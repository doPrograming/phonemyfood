var temp = new Array();
var temp2 = new Array();

function serachItems(){
    $.ajax({
        type: "get",
        url: "/phonemyfood/user/item/search_item_value",
        data: {
            key: $.trim($(".search_item").val())
        },
        success: function(t) {
            $(".food-list ul").empty(), 0 != t.length ? $.each(t, function(t, e) {
                $(".food-list ul").append($("<li>").text(e.name).addClass("select_item").attr("id", e.id))
            }) : $(".food-list ul").append($("<li>").text("No Record Found"))
        }
    })
}
$(document).on("keyup", ".search_item", function() {
	serachItems();
}),
$(document).on('input','.search_item', function() {
	serachItems();
}),

$(document).on("keyup", ".find-items", function() {
    $.ajax({
        type: "get",
        url: "/phonemyfood/user/item/search_item_value",
        data: { key: $(".find-items").val() }
	}).done(function(t){
        $(".food-list ul").empty(), 0 != t.length ? $.each(t, function(t, e) {
            $(".food-list ul").append("<li id='"+e.id+"'><input type='hidden' class='demoClass' value='"+e.id+"' required='required'><span>"+e.name+"</span></li>");
        }) : $(".food-list ul").append($("<li>").text("No Record Found"))
        getAlreadySelectedItems();
        $("#foodlist ul li").click(function(event) {
			selectItem(this,temp2);
		});
    }); 
}),
 
$(document).on("click", ".category_msg", function() {
    $.ajax({
        type: "get",
        url: "/phonemyfood/user/category/get_category_byId",
        data: {
            category_id: $(this).parent().parent().attr("id")
        },
        success: function(t) {
            $("#messageModal").html(t), $("#category_message").modal()
        }
    })
}),

$(document).on("click", ".add_category", function(e) {
    e.preventDefault();
    if($(".category_name").val() != ""){
    	$.ajax({
            type: "post",
            url: "/phonemyfood/user/category/addCategory",
            data: {
                name: $(".category_name").val(),
                id: $(".category_id").val()
            },
            success: function() {
                $(this).addClass("done");
                location.reload();
            },
            error: function() {
            	toastr.error("Category already Exist.");
            }
        })
    } else {
    	toastr.error("Please Enter a Category Name.");
    }
    /*$(".category_name").attr("disabled", "disabled"), $("span").each(function() {
        $(this).text() == $(".category_name").val() && $(this).text() != "" && (t = confirm("Category Name Already Exists, Add ( Y / N)"))
    }), t && $.*/
}),

$(".delete_category").on('click',function() {
	if(""!=$(".category_id").val())	{
		$.ajax({
	        type: "get",	
	        url: "/phonemyfood/user/category/categoryDeleteModal",
	        data:{
	        "category_id" : $(".category_id").val()
	        }
		})
		.done(function(data){
			$("#deleteConfirm").html(data);
			$("#deleteConfirm").modal();
		});
	}
	else{
		toastr.warning("Please select Category for delete.")
	}
}),

$(document).on("click", "#delete_category_yes", function() {
	$.ajax({
        type: "get",
        url: "/phonemyfood/user/category/delete",
        data: {
        	cid: $(".category_id").val()
        },
        success: function(t) {
        	if(t=="Deleted Successfully"){
        		$("#"+$(".category_id").val()).remove()
        		$(".category_name").val('');
        		$(".category_name").attr("disabled",false);
	        	toastr.success(t);
        	}
        	else{
        		toastr.warning(t);
        	}
        }
    })
}),

$(document).on("click", ".cat_select", function() {
    $(".cat_select").removeClass("selected-ingrediends");
    $(this).addClass("selected-ingrediends");
    $(".category_name").val($(this).find("span").text());
    $(".category_name").focus();
    $(".category_id").val($(this).attr("id"));
    $(".delete_category").css("background", "linear-gradient(to bottom, #ce5051 50%, #860404 50%)");
    $(".delete_category").css("border", "2px solid #7a0000");
    $(".add_category").removeClass("fa fa-plus").addClass("add_category fa fa-pencil").text(" "+"Edit");
}),

$(document).on("click", ".select_item", function() {
	$(".select_item").removeClass("item-selected"), $(this).addClass("item-selected"), $(".selected_item_id").val($(".item-selected").attr("id")), $.ajax({
        type: "post",
        url: "/phonemyfood/user/item/get_selected_item",
        data: {
            i_id: $(".item-selected").attr("id")
        },
        success: function(t) {
        	$("#updateItemId").empty(),$("#updateItemId").html(t);
        	$(this).addClass("done");
        	stoppedTypingItem();
        }
    })
});

function clear_item_btn(){
	$("#itemAddForm input[type='text']").val("");
	$("#itemAddForm input").prop('checked', false);
	$("#itemAddForm textarea").val("");	
}

$(document).ready(function() {
    $(document).on("blur", ".ingredient_price", function() {
        value = $(this).val(), "" != value && (t = parseFloat(value).toFixed(2), $(this).val(t))
    });
    var t = !1,
        e = $(".ingredients-sec :input");
    $(document).on("change", e, function() {
        t = !0
    }), $(document).on("focusout", e, function(e) {
        t && (t = !1, e.preventDefault(), $(".ingrediends-button").trigger("click")), e.preventDefault()
    }),
    $(document).on("click", "#add_menu_button", function() {
    	var form = this.closest("form");
    	var userMenuVo=$(form).serialize();
    	       $.ajax({
           		type: "POST",
        		url: "/phonemyfood/user/menu/add_menu",
        		data: userMenuVo,
                success: function() {
                	toastr.success("User Menu Saved")
                	var child=$('.sidebar ul').children();
                	for(var i=0;i<child.length;i++){
                		if($(child[i]).attr("class")=="selected")
                			$(child[i]).attr("class","");
                	}
                    $(".loginbox").hide(),
                    $(".aboutbox").hide(),
                    $(".specialsbox").hide(),
                    $(".digitalmenubox").hide()
                },
                error:function(data){
                	toastr.error(data.responseText)
                }
        	})
    }),
    $(document).on("click", ".save_rewards", function() {
    	var form = this.closest("form");
    	var serializedData=$(form).serialize();
    	       $.ajax({
           		type: "post",
        		url: "/phonemyfood/user/rewards/update_rewards",
        		data: serializedData,
                success: function() {
                	var child=$('.sidebar ul').children();
                	for(var i=0;i<child.length;i++){
                		if($(child[i]).attr("class")=="selected")
                			$(child[i]).attr("class","");
                	}
                	$(".loginbox").hide(),
                    $(".aboutbox").hide(),
                    $(".specialsbox").hide(),
                    $(".digitalmenubox").hide()
                    toastr.success("Rewards Saved")
                    
                }
        	})
    }),
    $(document).on("click", ".delivery_update", function() {
    	var form = this.closest("form");
    	var serializedData=$(form).serialize();
    	       $.ajax({
           		type: "POST",
        		url: "/phonemyfood/user/delivery/save_userdeliveries",
        		data: serializedData,
                success: function() {
                	var child=$('.sidebar ul').children();
                	for(var i=0;i<child.length;i++){
                		if($(child[i]).attr("class")=="selected")
                			$(child[i]).attr("class","");
                	}
                	$(".loginbox").hide(),
                    $(".aboutbox").hide(),
                    $(".specialsbox").hide(),
                    $(".digitalmenubox").hide()
                    toastr.success("Deliveries Saved")
                }
        	})
    })

}),

$(document).on("click", ".item_ingredient_select", function() {
    var t = [];
    "item_ingredient_select selected" == $(this).attr("class") ? ($(this).find('input').prop("disabled", true), $(this).attr("class", "item_ingredient_select")) : ($(this).find('input').prop("disabled", false), $(this).attr("class", "item_ingredient_select selected"))
}),
$(document).on("click", ".item_ingredient_option", function() {
	null != $(".item-selected").attr("id") ? $.ajax({
		 type: "post",
		 url: "/phonemyfood/user/item/ingredientsbyuser",
		 data: {
             i_id: $(".selected_item_id").val()
         },
         success: function(i) {
             $("#allModalDialogue").empty(),$("#allModalDialogue").html(i), $("#itemingredientModal").modal(),$(this).addClass("done")
         }
	 }) : toastr.info("Please create Item first.")
}),
$(document).on("click", ".combo_option_modal", function() {
	/* alert($(this).attr('value')); */
	var price = $(this).siblings("input").val();
	var str = $(this).siblings("input").attr("class").split(' ')[0];
	var name = $(".name"+str[str.length -1]).val();
	if($(this).attr('id') != "0" && price != "" && name != ""){
        $.ajax({
       		type: "get",
    		url: "/phonemyfood/user/combo/getComboById",
    		data: {
                c_id: $(this).attr('id')
            },
            success: function(i) {
                $("#allModalDialogue").empty(),$("#allModalDialogue").html(i), $("#combosoptionmodal").modal(),$(this).addClass("done")
            }
    	})
	}
	else toastr.error("Please enter name and price and insert/update first.")
}),
$(document).on("click", ".option1_select", function() {
	var t = [];
    "option1_select selected" == $(this).attr("class") ? ($(this).find('input').prop("disabled", true),$(this).attr("class", "option1_select")) : ($(this).find('input').prop("disabled", false), $(this).attr("class", "option1_select selected"), $(".first_hidden_option1").val(t))
}),
$(".hide_item").click(function() {
    "true" == $(this).find("input").attr("data-id") ? $(this).find("input").attr("value", "true") : $(this).find("input").attr("value", "false"), $(this).find("input").attr("data-id", "false")
}),/*
$(function() {
    $(".datetimepicker").timepicki()
})*/
 $(function() {
    $(".datetimepicker").clockpicker({
    	twelvehour: true
	});
}), $(document).ready(function() {
    /*$(document).on("click", ".menu_image", function() {
        "true" == $(this).attr("data-id") ? ("mon" == $(this).attr("day_type") ? ($(".cross_mon").attr("class", "menu_closeicon disabled cross_mon"), $(".menu_mon").hide(), $(".menu_mon").val(""), $(".arrow_mon").hide(), $(".menu_mon").removeAttr("required"), $(".mon").html("Closed")) : "tue" == $(this).attr("day_type") ? ($(".cross_tue").attr("class", "menu_closeicon disabled cross_tue"), $(".menu_tue").hide(), $(".menu_tue").val(""), $(".arrow_tue").hide(), $(".menu_tue").removeAttr("required"), $(".tue").html("Closed")) : "wed" == $(this).attr("day_type") ? ($(".cross_wed").attr("class", "menu_closeicon disabled cross_wed"), $(".menu_wed").hide(), $(".menu_wed").val(""), $(".arrow_wed").hide(), $(".menu_wed").removeAttr("required"), $(".wed").html("Closed")) : "thu" == $(this).attr("day_type") ? ($(".cross_thu").attr("class", "menu_closeicon disabled cross_thu"), $(".menu_thu").hide(), $(".menu_thu").val(""), $(".arrow_thu").hide(), $(".menu_thu").removeAttr("required"), $(".thu").html("Closed")) : "fri" == $(this).attr("day_type") ? ($(".cross_fri").attr("class", "menu_closeicon disabled cross_fri"), $(".menu_fri").hide(), $(".menu_fri").val(""), $(".arrow_fri").hide(), $(".menu_fri").removeAttr("required"), $(".fri").html("Closed")) : "sat" == $(this).attr("day_type") ? ($(".cross_sat").attr("class", "menu_closeicon disabled cross_sat"), $(".menu_sat").hide(), $(".menu_sat").val(""), $(".arrow_sat").hide(), $(".menu_sat").removeAttr("required"), $(".sat").html("Closed")) : "sun" == $(this).attr("day_type") && ($(".cross_sun").attr("class", "menu_closeicon disabled cross_sun"), $(".menu_sun").hide(), $(".menu_sun").val(""), $(".arrow_sun").hide(), $(".menu_sun").removeAttr("required"), $(".sun").html("Closed")), $(this).attr("data-id", "false")) : ("mon" == $(this).attr("day_type") ? ($(".cross_mon").attr("class", "menu_closeicon cross_mon"), $(".menu_mon").show(), $(".arrow_mon").show(), $(".menu_mon").attr("required", "required"), $(".mon").html("")) : "tue" == $(this).attr("day_type") ? ($(".cross_tue").attr("class", "menu_closeicon cross_tue"), $(".menu_tue").show(), $(".arrow_tue").show(), $(".menu_tue").attr("required", "required"), $(".tue").html("")) : "wed" == $(this).attr("day_type") ? ($(".cross_wed").attr("class", "menu_closeicon cross_wed"), $(".menu_wed").show(), $(".arrow_wed").show(), $(".menu_wed").attr("required", "required"), $(".wed").html("")) : "thu" == $(this).attr("day_type") ? ($(".cross_thu").attr("class", "menu_closeicon cross_thu"), $(".menu_thu").show(), $(".arrow_thu").show(), $(".menu_thu").attr("required", "required"), $(".thu").html("")) : "fri" == $(this).attr("day_type") ? ($(".cross_fri").attr("class", "menu_closeicon cross_fri"), $(".menu_fri").show(), $(".arrow_fri").show(), $(".menu_fri").attr("required", "required"), $(".fri").html("")) : "sat" == $(this).attr("day_type") ? ($(".cross_sat").attr("class", "menu_closeicon cross_sat"), $(".menu_sat").show(), $(".arrow_sat").show(), $(".menu_sat").attr("required", "required"), $(".sat").html("")) : "sun" == $(this).attr("day_type") && ($(".cross_sun").attr("class", "menu_closeicon cross_sun"), $(".menu_sun").show(), $(".arrow_sun").show(), $(".menu_sun").attr("required", "required"), $(".sun").html("")), $(this).attr("data-id", "true"))
    }),*/ $(document).on("click", ".close_window", function() {
        $(".add-menu").hide()
    }), $(document).on("click", ".close_menu", function() {
    	var child=$('.sidebar ul').children();
    	for(var i=0;i<child.length;i++){
    		if($(child[i]).attr("class")=="selected")
    			$(child[i]).attr("class","");
    	}
        $(".loginbox").hide(),
        $(".aboutbox").hide(),
        $(".specialsbox").hide(),
        $(".digitalmenubox").hide()
    })
}), $(document).ready(function() {
    $(document).on("click", ".menu_image1", function() {
        "true" == $(this).attr("data-id") ? ("mon" == $(this).attr("day_type") && ($(this).parent().parent().find(".cross_mon").attr("class", "edit_menu_closeicon disabled cross_mon"), $(this).parent().parent().find(".menu_mon").hide(), $(this).parent().parent().find(".arrow_mon").hide(), $(this).parent().parent().find(".mon_start_value").val(""), $(this).parent().parent().find(".mon_last_value").val(""), $(this).parent().parent().find(".mon").html("Closed")), $(this).attr("data-id", "false")) : "mon" == $(this).attr("day_type") ? ($(this).parent().parent().find(".cross_mon").attr("class", "edit_menu_closeicon cross_mon"), $(this).parent().parent().find(".menu_mon").show(), $(this).parent().parent().find(".arrow_mon").show(), $(this).parent().parent().find(".mon").html(""), $(this).parent().parent().find(".mon_start_value").val($(this).parent().parent().find(".mon_start_value").attr("value")), $(this).parent().parent().find(".mon_last_value").val($(this).parent().parent().find(".mon_last_value").attr("value")), $(this).attr("data-id", "true")) : ($(this).parent().parent().toggle(), $(this).parent().parent().siblings().toggle())
    }), $(document).on("click", ".delete_menu_checkbox", function() {
        "false" == $(this).attr("date-id") ? ($(".delete_user_menu").removeAttr("disabled"), $(this).attr("date-id", "true")) : ($(".delete_user_menu").attr("disabled", "disabled"), $(this).attr("date-id", "false"))
    }), $(document).on("click", ".delete_user_menu", function() {
        confirm("Are you sure to delete this Menu.") && ($(".edit_menu_box").hide(), $.ajax({
            type: "delete",
            url: "menus",
            data: {
                i_id: $(".delete_menu_button").val()
            },
            success: function() {
                $(this).addClass("done")
            }
        }))
    })
}),

$(document).on("change", ".select_menu_order", function() {
if("Please select menu" == $(".select_menu_order")[0].value){
	toastr.warning("Select Menu First")
	$(".select_menu_order")[1].value="Please select category";
}
    "Please select menu" != $(".select_menu_order")[0].value && "Please select category" != $(".select_menu_order")[1].value && $.ajax({
        type: "get",
        url: "/phonemyfood/user/menus/get_category_item",
        data: {
            menu_id: $($(".select_menu_order")[0]).find("option:selected").attr("id"),
            category_id: $($(".select_menu_order")[1]).find("option:selected").attr("id")
        },
        success: function(t) {
            $(".menuorderlist ul").empty(), 0 != t.length && $.each(t, function(t, e) {
                $(".menuorderlist ul").append($("<li>").text(e.name).val(e.id))
            })
        }
    })
}),

$(document).on("change", ".select_menu_builder", function() {
	getAlreadySelectedItems();
});

function getAlreadySelectedItems(incoming){
	"" != $(".select_menu_builder")[0].value && "" != $(".select_menu_builder")[1].value && 
	$.ajax({
        type: "get",
        url: "/phonemyfood/user/menu/get_already_selected_items",
        data: {
            menu_id: $($(".select_menu_builder")[0]).find("option:selected").attr("id"),
            category_id: $($(".select_menu_builder")[1]).find("option:selected").attr("id")
        },
        success: function(t) {
        	if(t){
        		$.each(t.items, function(i, obj) {
        			temp.push(obj.id.toString());
        		});
        		loadSelectedItems(t,temp);
        		temp2=temp;
        		temp=[];
        		$("#createdId").val(t.id);
        	}
        	else{
        		toastr.info("This Is A New Item")
        		$("#createdId").val(0);
	            $("ul.items li").each(function(n) {
	            	var obj1 = $(this);
	            	obj1.attr("class", "");
	            	obj1.find("input").prop('disabled', true);
	            });
        	}
        },
		error: function(e) {
			toastr.info("This Is A New Item")
			$.ajax({
				type : "GET",
				url : "/phonemyfood/user/menu/select_unselected_items",
				success: function(t) {
					$("#createdId").val(0);
					loadSelectedItems(t);
				}
			})
		}
    })
}

function loadSelectedItems(t,ids){
	var filtered = new Array();
	$("ul.items li").each(function(n) {
    	var obj1 = $(this);
    	obj1.attr("class", ""),obj1.find("input").prop('disabled', true)
    	$.each(t.items, function(i, obj) {
    		  //use obj.id and obj.name here, for example:
    		if(obj.id.toString() == obj1.attr("id")){
        		obj.id.toString().includes(obj1.attr("id")) ? (obj1.attr("class", "selected"),obj1.find("input").prop('disabled', false)) : (obj1.attr("class", ""),obj1.find("input").prop('disabled', true))
    		}
    	});
        if(obj1.hasClass("selected")){
			filtered.push(obj1.attr("id"));
        }
    });
	$.extend(filtered,ids)
  //  $("#createdId").val(t.id);
    var joined = filtered.join(",");
	$("#listItemId").val(joined);
}
									/*add below code to user_edit_menu.js*/
 $(document).ready(function() {
    $("#sortable").sortable(),
    $("#sortable").disableSelection(),
    
    $(document).on("change", "#select_menu", function() {
        $(".builder_menu").val($(this).find("option:selected").attr("id"))
    }),
    
    $(document).on("change", "#select_category", function() {
    	if("" != $("#select_menu").val()){
	       $(".builder_category").val($(this).find("option:selected").attr("id"));
	       if("Add Category" == $(this).find("option:selected").html()){
	    	   $("#category").modal();
	       }
	       
    	}
    	else
    		{
	    		toastr.warning("Please select Menu first.")    		
	    		$("#select_category").val("Please select category")
    		}
    }),
    
    $(document).on("click", ".menu_builder", function() {
        var t = [];
        $(".items li.selected").each(function() {
            t.push(this.id)
        }), "" != $(".builder_menu").val() && "" != $(".builder_category").val() && "" != t ? $.ajax({
            type: "post",
            url: "/phonemyfood/user/menus/add_menu_builder",
            data: {
                menu_id: $(".builder_menu").val(),
                c_id: $(".builder_category").val(),
                i_ids: t
            },
            success: function(t) {
                $(".select_menu_builder").val("Please select menu"), $("#select_category").val("Please select category"), $(".items li").attr("class", ""), toastr.success(t[0]), $(this).addClass("done")
            }
        }) : "" == $(".builder_menu").val() ? toastr.warning("Please select Menu first") : "" == $(".builder_category").val() ? toastr.warning("Please select Category") : "" == t && toastr.warning("Please select Items")
    }),  $(document).on("click", ".menu_order", function() {
        if ("Please select menu" != $(".select_menu_order")[0].value && "Please select category" != $(".select_menu_order")[1].value) {
            var t = [];
            $(".menuorderlist ul li").each(function() {
                t.push(this.value)
            })
            var joined = t.join(",");
            $.ajax({
                type: "post",
                url: "/phonemyfood/user/menus/save_menu_order",
                data: {
                    menu_id: $($(".select_menu_order")[0]).find("option:selected").attr("id"),
                    category_id: $($(".select_menu_order")[1]).find("option:selected").attr("id"),
                    i_ids: joined
                },
                success: function(t) {
                    toastr.success(t)
                }
            })
        } else toastr.warning("Please select Menu and Category first.")
    }),   
    
$(document).on("click", ".category_button", function() {
	if($(".new_category").val() != ""){
		$.ajax({
            type: "get",
            url: "/phonemyfood/user/category/add_new_category",
            data: {
                c_name: $(".new_category").val()
            },
            success: function(t) {
            	if(t=="exist"){
            		toastr.warning("Category name already exist");
            	}
            	else{
            		$.ajax({
	                    type: "get",
	                    url: "/phonemyfood/user/category/add_new_category",
	                    data: {
	                        c_name: $(".new_category").val()
	                    },
	                    success: function(t) {
	                    	if(t!=null){
	                    		toastr.success("category added"),
	                    		$("select#select_category option").eq(-1).before("<option value="+t.id+">"+t.name+"</option>").attr("id", t.id).text(t.name).attr("class", ""),
	                    		$("select#select_category option").eq(-1).attr("class", "add_category_color"),
	                    		$(".new_category").val(""),
	                    		$(".new_category").attr("placeholder", "Add Category name"),
	                    		location.reload();
	                    	}
	                    }
	                })
            	}
            }
        })
	}
	else{
		toastr.error("Please enter category name first.");
	}
 }),
    
    $(document).on("click", ".close-add-cat-popup", function() {
        $("#category").modal("hide")
    })
}) ,
										/*add above code to user_edit_menu.js*/
$(document).ready(function() {
    var t = !1,
        e = $("#reward_form :input");
    $(document).on("change", e, function() {
        t = !0
    })/*, $(document).on("focusout", e, function(e) {
        t && (t = !1, e.preventDefault(), $("#reward_form").trigger("submit")), e.preventDefault()
    })*/
})

function deleteUploadImage(obj) {
	$(obj).parent().hide();
	$.ajax({
		type : 'POST',
		url : '/phonemyfood/user/Image/delete_profile_image',
		data : {
			'i_id' : obj.id
		},
		success : function(data) {
			if (data === true) {
				toastr.success("Selected File deleted successfully.")
			} else {
				toastr.error("Failed to delete the  file")
			}
			//I assume you want to do something on controller action execution success?
		}
	});
}

function deleteSelectedItem(deleteButton){
	var itemId = $(deleteButton).closest("form").find("input[name=itemId]").val();
	if('0'!=itemId)	{
	$.ajax({
		type : 'get',
		url : contextURL + "user/item/itemDeleteModal",
		data : {
			'itemId' : itemId
		}
	}).done(function(data){
		$("#itemDeleteModal").html(data);
		$("#itemDeleteModal").modal();
	});
}else{
	toastr.warning("Please Select Item To Delete")
}
	
}

$(document).on("click", "#item_delete_yes", function() {
	var itemId = $(this).parent().find("input").val();
	$.ajax({
		type : 'get',
		url : contextURL + "user/item/deleteItem",
		data : {
			'itemId' : itemId
		}
	}).done(function(data){
		clear_item_btn();
		disableItemAllFields();
		$('#foodlist #items_list').find("#"+itemId).remove();
		toastr.success("Item Deleted Successfully");
	})
})


function disableItemAllFields(){
	$('.item_description').attr("disabled", "disabled");
	$('.add-item-contents input').attr("disabled", "disabled");
	$('.additemfields input').attr("disabled", "disabled");
	$('#item_user_menu').attr("disabled", "disabled");
}

