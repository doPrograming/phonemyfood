function change_format(t) {
    "PM" == t.value ? t.value = "AM" : t.value = "PM"
}

function change_min(t) {
    $(".min_time").val(t.value)
}

function css_browser_selector(t) {
    var e = t.toLowerCase(),
        i = function(t) {
            return e.indexOf(t) > -1
        },
        n = "gecko",
        s = "webkit",
        a = "safari",
        o = "opera",
        r = "mobile",
        l = document.documentElement,
        u = [!/opera|webtv/i.test(e) && /msie\s(\d)/.test(e) ? "ie ie" + RegExp.$1 : i("firefox/2") ? n + " ff2" : i("firefox/3.5") ? n + " ff3 ff3_5" : i("firefox/3.6") ? n + " ff3 ff3_6" : i("firefox/3") ? n + " ff3" : i("gecko/") ? n : i("opera") ? o + (/version\/(\d+)/.test(e) ? " " + o + RegExp.$1 : /opera(\s|\/)(\d+)/.test(e) ? " " + o + RegExp.$2 : "") : i("konqueror") ? "konqueror" : i("blackberry") ? r + " blackberry" : i("android") ? r + " android" : i("chrome") ? s + " chrome" : i("iron") ? s + " iron" : i("applewebkit/") ? s + " " + a + (/version\/(\d+)/.test(e) ? " " + a + RegExp.$1 : "") : i("mozilla/") ? n : "", i("j2me") ? r + " j2me" : i("iphone") ? r + " iphone" : i("ipod") ? r + " ipod" : i("ipad") ? r + " ipad" : i("mac") ? "mac" : i("darwin") ? "mac" : i("webtv") ? "webtv" : i("win") ? "win" + (i("windows nt 6.0") ? " vista" : "") : i("freebsd") ? "freebsd" : i("x11") || i("linux") ? "linux" : "", "js"];
    return c = u.join(" "), l.className += " " + c, c
}

function startTimers() {
    "true" == $(".active_popup_window").val() && (warningTimer = setTimeout("idleWarning()", timoutWarning), timeoutTimer = setTimeout("idleTimeout()", timoutNow))
}

function resetTimers() {
    "true" == $(".active_popup_window").val() && "true" == $(".timeout_popup").val() && (clearTimeout(warningTimer), clearTimeout(timeoutTimer), startTimers(), $("#timeout").modal("hide"))
}

function idleWarning() {
    $("#timeout").modal(), $(".timeout_popup").val("false")
}

function idleTimeout() {
    window.location = logoutUrl
}! function(t, e) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function(t) {
        if (!t.document) throw new Error("jQuery requires a window with a document");
        return e(t)
    } : e(t)
}("undefined" != typeof window ? window : this, function(t, e) {
    function i(t) {
        var e = !!t && "length" in t && t.length,
            i = pt.type(t);
        return "function" !== i && !pt.isWindow(t) && ("array" === i || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
    }

    function n(t, e, i) {
        if (pt.isFunction(e)) return pt.grep(t, function(t, n) {
            return !!e.call(t, n, t) !== i
        });
        if (e.nodeType) return pt.grep(t, function(t) {
            return t === e !== i
        });
        if ("string" == typeof e) {
            if ($t.test(e)) return pt.filter(e, t, i);
            e = pt.filter(e, t)
        }
        return pt.grep(t, function(t) {
            return pt.inArray(t, e) > -1 !== i
        })
    }

    function s(t, e) {
        do {
            t = t[e]
        } while (t && 1 !== t.nodeType);
        return t
    }

    function a(t) {
        var e = {};
        return pt.each(t.match(Tt) || [], function(t, i) {
            e[i] = !0
        }), e
    }

    function o() {
        nt.addEventListener ? (nt.removeEventListener("DOMContentLoaded", r), t.removeEventListener("load", r)) : (nt.detachEvent("onreadystatechange", r), t.detachEvent("onload", r))
    }

    function r() {
        (nt.addEventListener || "load" === t.event.type || "complete" === nt.readyState) && (o(), pt.ready())
    }

    function l(t, e, i) {
        if (i === undefined && 1 === t.nodeType) {
            var n = "data-" + e.replace(Pt, "-$1").toLowerCase();
            if ("string" == typeof(i = t.getAttribute(n))) {
                try {
                    i = "true" === i || "false" !== i && ("null" === i ? null : +i + "" === i ? +i : Mt.test(i) ? pt.parseJSON(i) : i)
                } catch (t) {}
                pt.data(t, e, i)
            } else i = undefined
        }
        return i
    }

    function c(t) {
        var e;
        for (e in t)
            if (("data" !== e || !pt.isEmptyObject(t[e])) && "toJSON" !== e) return !1;
        return !0
    }

    function u(t, e, i, n) {
        if (Et(t)) {
            var s, a, o = pt.expando,
                r = t.nodeType,
                l = r ? pt.cache : t,
                c = r ? t[o] : t[o] && o;
            if (c && l[c] && (n || l[c].data) || i !== undefined || "string" != typeof e) return c || (c = r ? t[o] = it.pop() || pt.guid++ : o), l[c] || (l[c] = r ? {} : {
                toJSON: pt.noop
            }), "object" != typeof e && "function" != typeof e || (n ? l[c] = pt.extend(l[c], e) : l[c].data = pt.extend(l[c].data, e)), a = l[c], n || (a.data || (a.data = {}), a = a.data), i !== undefined && (a[pt.camelCase(e)] = i), "string" == typeof e ? null == (s = a[e]) && (s = a[pt.camelCase(e)]) : s = a, s
        }
    }

    function d(t, e, i) {
        if (Et(t)) {
            var n, s, a = t.nodeType,
                o = a ? pt.cache : t,
                r = a ? t[pt.expando] : pt.expando;
            if (o[r]) {
                if (e && (n = i ? o[r] : o[r].data)) {
                    pt.isArray(e) ? e = e.concat(pt.map(e, pt.camelCase)) : e in n ? e = [e] : (e = pt.camelCase(e), e = e in n ? [e] : e.split(" ")), s = e.length;
                    for (; s--;) delete n[e[s]];
                    if (i ? !c(n) : !pt.isEmptyObject(n)) return
                }(i || (delete o[r].data, c(o[r]))) && (a ? pt.cleanData([t], !0) : dt.deleteExpando || o != o.window ? delete o[r] : o[r] = undefined)
            }
        }
    }

    function h(t, e, i, n) {
        var s, a = 1,
            o = 20,
            r = n ? function() {
                return n.cur()
            } : function() {
                return pt.css(t, e, "")
            },
            l = r(),
            c = i && i[3] || (pt.cssNumber[e] ? "" : "px"),
            u = (pt.cssNumber[e] || "px" !== c && +l) && Ft.exec(pt.css(t, e));
        if (u && u[3] !== c) {
            c = c || u[3], i = i || [], u = +l || 1;
            do {
                a = a || ".5", u /= a, pt.style(t, e, u + c)
            } while (a !== (a = r() / l) && 1 !== a && --o)
        }
        return i && (u = +u || +l || 0, s = i[1] ? u + (i[1] + 1) * i[2] : +i[2], n && (n.unit = c, n.start = u, n.end = s)), s
    }

    function p(t) {
        var e = Bt.split("|"),
            i = t.createDocumentFragment();
        if (i.createElement)
            for (; e.length;) i.createElement(e.pop());
        return i
    }

    function f(t, e) {
        var i, n, s = 0,
            a = "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e || "*") : "undefined" != typeof t.querySelectorAll ? t.querySelectorAll(e || "*") : undefined;
        if (!a)
            for (a = [], i = t.childNodes || t; null != (n = i[s]); s++) !e || pt.nodeName(n, e) ? a.push(n) : pt.merge(a, f(n, e));
        return e === undefined || e && pt.nodeName(t, e) ? pt.merge([t], a) : a
    }

    function m(t, e) {
        for (var i, n = 0; null != (i = t[n]); n++) pt._data(i, "globalEval", !e || pt._data(e[n], "globalEval"))
    }

    function g(t) {
        Ot.test(t.type) && (t.defaultChecked = t.checked)
    }

    function _(t, e, i, n, s) {
        for (var a, o, r, l, c, u, d, h = t.length, _ = p(e), v = [], b = 0; b < h; b++)
            if ((o = t[b]) || 0 === o)
                if ("object" === pt.type(o)) pt.merge(v, o.nodeType ? [o] : o);
                else if (Ut.test(o)) {
            for (l = l || _.appendChild(e.createElement("div")), c = (Rt.exec(o) || ["", ""])[1].toLowerCase(), d = qt[c] || qt._default, l.innerHTML = d[1] + pt.htmlPrefilter(o) + d[2], a = d[0]; a--;) l = l.lastChild;
            if (!dt.leadingWhitespace && Wt.test(o) && v.push(e.createTextNode(Wt.exec(o)[0])), !dt.tbody)
                for (o = "table" !== c || Yt.test(o) ? "<table>" !== d[1] || Yt.test(o) ? 0 : l : l.firstChild, a = o && o.childNodes.length; a--;) pt.nodeName(u = o.childNodes[a], "tbody") && !u.childNodes.length && o.removeChild(u);
            for (pt.merge(v, l.childNodes), l.textContent = ""; l.firstChild;) l.removeChild(l.firstChild);
            l = _.lastChild
        } else v.push(e.createTextNode(o));
        for (l && _.removeChild(l), dt.appendChecked || pt.grep(f(v, "input"), g), b = 0; o = v[b++];)
            if (n && pt.inArray(o, n) > -1) s && s.push(o);
            else if (r = pt.contains(o.ownerDocument, o), l = f(_.appendChild(o), "script"), r && m(l), i)
            for (a = 0; o = l[a++];) zt.test(o.type || "") && i.push(o);
        return l = null, _
    }

    function v() {
        return !0
    }

    function b() {
        return !1
    }

    function y() {
        try {
            return nt.activeElement
        } catch (t) {}
    }

    function w(t, e, i, n, s, a) {
        var o, r;
        if ("object" == typeof e) {
            "string" != typeof i && (n = n || i, i = undefined);
            for (r in e) w(t, r, i, n, e[r], a);
            return t
        }
        if (null == n && null == s ? (s = i, n = i = undefined) : null == s && ("string" == typeof i ? (s = n, n = undefined) : (s = n, n = i, i = undefined)), !1 === s) s = b;
        else if (!s) return t;
        return 1 === a && (o = s, s = function(t) {
            return pt().off(t), o.apply(this, arguments)
        }, s.guid = o.guid || (o.guid = pt.guid++)), t.each(function() {
            pt.event.add(this, e, s, n, i)
        })
    }

    function x(t, e) {
        return pt.nodeName(t, "table") && pt.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
    }

    function $(t) {
        return t.type = (null !== pt.find.attr(t, "type")) + "/" + t.type, t
    }

    function k(t) {
        var e = ne.exec(t.type);
        return e ? t.type = e[1] : t.removeAttribute("type"), t
    }

    function C(t, e) {
        if (1 === e.nodeType && pt.hasData(t)) {
            var i, n, s, a = pt._data(t),
                o = pt._data(e, a),
                r = a.events;
            if (r) {
                delete o.handle, o.events = {};
                for (i in r)
                    for (n = 0, s = r[i].length; n < s; n++) pt.event.add(e, i, r[i][n])
            }
            o.data && (o.data = pt.extend({}, o.data))
        }
    }

    function D(t, e) {
        var i, n, s;
        if (1 === e.nodeType) {
            if (i = e.nodeName.toLowerCase(), !dt.noCloneEvent && e[pt.expando]) {
                s = pt._data(e);
                for (n in s.events) pt.removeEvent(e, n, s.handle);
                e.removeAttribute(pt.expando)
            }
            "script" === i && e.text !== t.text ? ($(e).text = t.text, k(e)) : "object" === i ? (e.parentNode && (e.outerHTML = t.outerHTML), dt.html5Clone && t.innerHTML && !pt.trim(e.innerHTML) && (e.innerHTML = t.innerHTML)) : "input" === i && Ot.test(t.type) ? (e.defaultChecked = e.checked = t.checked, e.value !== t.value && (e.value = t.value)) : "option" === i ? e.defaultSelected = e.selected = t.defaultSelected : "input" !== i && "textarea" !== i || (e.defaultValue = t.defaultValue)
        }
    }

    function S(t, e, i, n) {
        e = at.apply([], e);
        var s, a, o, r, l, c, u = 0,
            d = t.length,
            h = d - 1,
            p = e[0],
            m = pt.isFunction(p);
        if (m || d > 1 && "string" == typeof p && !dt.checkClone && ie.test(p)) return t.each(function(s) {
            var a = t.eq(s);
            m && (e[0] = p.call(this, s, a.html())), S(a, e, i, n)
        });
        if (d && (c = _(e, t[0].ownerDocument, !1, t, n), s = c.firstChild, 1 === c.childNodes.length && (c = s), s || n)) {
            for (r = pt.map(f(c, "script"), $), o = r.length; u < d; u++) a = c, u !== h && (a = pt.clone(a, !0, !0), o && pt.merge(r, f(a, "script"))), i.call(t[u], a, u);
            if (o)
                for (l = r[r.length - 1].ownerDocument, pt.map(r, k), u = 0; u < o; u++) a = r[u], zt.test(a.type || "") && !pt._data(a, "globalEval") && pt.contains(l, a) && (a.src ? pt._evalUrl && pt._evalUrl(a.src) : pt.globalEval((a.text || a.textContent || a.innerHTML || "").replace(se, "")));
            c = s = null
        }
        return t
    }

    function T(t, e, i) {
        for (var n, s = e ? pt.filter(e, t) : t, a = 0; null != (n = s[a]); a++) i || 1 !== n.nodeType || pt.cleanData(f(n)), n.parentNode && (i && pt.contains(n.ownerDocument, n) && m(f(n, "script")), n.parentNode.removeChild(n));
        return t
    }

    function I(t, e) {
        var i = pt(e.createElement(t)).appendTo(e.body),
            n = pt.css(i[0], "display");
        return i.detach(), n
    }

    function A(t) {
        var e = nt,
            i = le[t];
        return i || (i = I(t, e), "none" !== i && i || (re = (re || pt("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement), e = (re[0].contentWindow || re[0].contentDocument).document, e.write(), e.close(), i = I(t, e), re.detach()), le[t] = i), i
    }

    function E(t, e) {
        return {
            get: function() {
                return t() ? void delete this.get : (this.get = e).apply(this, arguments)
            }
        }
    }

    function M(t) {
        if (t in $e) return t;
        for (var e = t.charAt(0).toUpperCase() + t.slice(1), i = xe.length; i--;)
            if ((t = xe[i] + e) in $e) return t
    }

    function P(t, e) {
        for (var i, n, s, a = [], o = 0, r = t.length; o < r; o++) n = t[o], n.style && (a[o] = pt._data(n, "olddisplay"), i = n.style.display, e ? (a[o] || "none" !== i || (n.style.display = ""), "" === n.style.display && Lt(n) && (a[o] = pt._data(n, "olddisplay", A(n.nodeName)))) : (s = Lt(n), (i && "none" !== i || !s) && pt._data(n, "olddisplay", s ? i : pt.css(n, "display"))));
        for (o = 0; o < r; o++) n = t[o], n.style && (e && "none" !== n.style.display && "" !== n.style.display || (n.style.display = e ? a[o] || "" : "none"));
        return t
    }

    function N(t, e, i) {
        var n = be.exec(e);
        return n ? Math.max(0, n[1] - (i || 0)) + (n[2] || "px") : e
    }

    function F(t, e, i, n, s) {
        for (var a = i === (n ? "border" : "content") ? 4 : "width" === e ? 1 : 0, o = 0; a < 4; a += 2) "margin" === i && (o += pt.css(t, i + jt[a], !0, s)), n ? ("content" === i && (o -= pt.css(t, "padding" + jt[a], !0, s)), "margin" !== i && (o -= pt.css(t, "border" + jt[a] + "Width", !0, s))) : (o += pt.css(t, "padding" + jt[a], !0, s), "padding" !== i && (o += pt.css(t, "border" + jt[a] + "Width", !0, s)));
        return o
    }

    function j(t, e, i) {
        var n = !0,
            s = "width" === e ? t.offsetWidth : t.offsetHeight,
            a = pe(t),
            o = dt.boxSizing && "border-box" === pt.css(t, "boxSizing", !1, a);
        if (s <= 0 || null == s) {
            if (s = fe(t, e, a), (s < 0 || null == s) && (s = t.style[e]), ue.test(s)) return s;
            n = o && (dt.boxSizingReliable() || s === t.style[e]), s = parseFloat(s) || 0
        }
        return s + F(t, e, i || (o ? "border" : "content"), n, a) + "px"
    }

    function L(t, e, i, n, s) {
        return new L.prototype.init(t, e, i, n, s)
    }

    function H() {
        return t.setTimeout(function() {
            ke = undefined
        }), ke = pt.now()
    }

    function O(t, e) {
        var i, n = {
                height: t
            },
            s = 0;
        for (e = e ? 1 : 0; s < 4; s += 2 - e) i = jt[s], n["margin" + i] = n["padding" + i] = t;
        return e && (n.opacity = n.width = t), n
    }

    function R(t, e, i) {
        for (var n, s = (B.tweeners[e] || []).concat(B.tweeners["*"]), a = 0, o = s.length; a < o; a++)
            if (n = s[a].call(i, e, t)) return n
    }

    function z(t, e, i) {
        var n, s, a, o, r, l, c, u = this,
            d = {},
            h = t.style,
            p = t.nodeType && Lt(t),
            f = pt._data(t, "fxshow");
        i.queue || (r = pt._queueHooks(t, "fx"), null == r.unqueued && (r.unqueued = 0, l = r.empty.fire, r.empty.fire = function() {
            r.unqueued || l()
        }), r.unqueued++, u.always(function() {
            u.always(function() {
                r.unqueued--, pt.queue(t, "fx").length || r.empty.fire()
            })
        })), 1 === t.nodeType && ("height" in e || "width" in e) && (i.overflow = [h.overflow, h.overflowX, h.overflowY], c = pt.css(t, "display"), "inline" === ("none" === c ? pt._data(t, "olddisplay") || A(t.nodeName) : c) && "none" === pt.css(t, "float") && (dt.inlineBlockNeedsLayout && "inline" !== A(t.nodeName) ? h.zoom = 1 : h.display = "inline-block")), i.overflow && (h.overflow = "hidden", dt.shrinkWrapBlocks() || u.always(function() {
            h.overflow = i.overflow[0], h.overflowX = i.overflow[1], h.overflowY = i.overflow[2]
        }));
        for (n in e)
            if (s = e[n], De.exec(s)) {
                if (delete e[n], a = a || "toggle" === s, s === (p ? "hide" : "show")) {
                    if ("show" !== s || !f || f[n] === undefined) continue;
                    p = !0
                }
                d[n] = f && f[n] || pt.style(t, n)
            } else c = undefined;
        if (pt.isEmptyObject(d)) "inline" === ("none" === c ? A(t.nodeName) : c) && (h.display = c);
        else {
            f ? "hidden" in f && (p = f.hidden) : f = pt._data(t, "fxshow", {}), a && (f.hidden = !p), p ? pt(t).show() : u.done(function() {
                pt(t).hide()
            }), u.done(function() {
                var e;
                pt._removeData(t, "fxshow");
                for (e in d) pt.style(t, e, d[e])
            });
            for (n in d) o = R(p ? f[n] : 0, n, u), n in f || (f[n] = o.start, p && (o.end = o.start, o.start = "width" === n || "height" === n ? 1 : 0))
        }
    }

    function W(t, e) {
        var i, n, s, a, o;
        for (i in t)
            if (n = pt.camelCase(i), s = e[n], a = t[i], pt.isArray(a) && (s = a[1], a = t[i] = a[0]), i !== n && (t[n] = a, delete t[i]), (o = pt.cssHooks[n]) && "expand" in o) {
                a = o.expand(a), delete t[n];
                for (i in a) i in t || (t[i] = a[i], e[i] = s)
            } else e[n] = s
    }

    function B(t, e, i) {
        var n, s, a = 0,
            o = B.prefilters.length,
            r = pt.Deferred().always(function() {
                delete l.elem
            }),
            l = function() {
                if (s) return !1;
                for (var e = ke || H(), i = Math.max(0, c.startTime + c.duration - e), n = i / c.duration || 0, a = 1 - n, o = 0, l = c.tweens.length; o < l; o++) c.tweens[o].run(a);
                return r.notifyWith(t, [c, a, i]), a < 1 && l ? i : (r.resolveWith(t, [c]), !1)
            },
            c = r.promise({
                elem: t,
                props: pt.extend({}, e),
                opts: pt.extend(!0, {
                    specialEasing: {},
                    easing: pt.easing._default
                }, i),
                originalProperties: e,
                originalOptions: i,
                startTime: ke || H(),
                duration: i.duration,
                tweens: [],
                createTween: function(e, i) {
                    var n = pt.Tween(t, c.opts, e, i, c.opts.specialEasing[e] || c.opts.easing);
                    return c.tweens.push(n), n
                },
                stop: function(e) {
                    var i = 0,
                        n = e ? c.tweens.length : 0;
                    if (s) return this;
                    for (s = !0; i < n; i++) c.tweens[i].run(1);
                    return e ? (r.notifyWith(t, [c, 1, 0]), r.resolveWith(t, [c, e])) : r.rejectWith(t, [c, e]), this
                }
            }),
            u = c.props;
        for (W(u, c.opts.specialEasing); a < o; a++)
            if (n = B.prefilters[a].call(c, t, u, c.opts)) return pt.isFunction(n.stop) && (pt._queueHooks(c.elem, c.opts.queue).stop = pt.proxy(n.stop, n)), n;
        return pt.map(u, R, c), pt.isFunction(c.opts.start) && c.opts.start.call(t, c), pt.fx.timer(pt.extend(l, {
            elem: t,
            anim: c,
            queue: c.opts.queue
        })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }

    function q(t) {
        return pt.attr(t, "class") || ""
    }

    function U(t) {
        return function(e, i) {
            "string" != typeof e && (i = e, e = "*");
            var n, s = 0,
                a = e.toLowerCase().match(Tt) || [];
            if (pt.isFunction(i))
                for (; n = a[s++];) "+" === n.charAt(0) ? (n = n.slice(1) || "*", (t[n] = t[n] || []).unshift(i)) : (t[n] = t[n] || []).push(i)
        }
    }

    function Y(t, e, i, n) {
        function s(r) {
            var l;
            return a[r] = !0, pt.each(t[r] || [], function(t, r) {
                var c = r(e, i, n);
                return "string" != typeof c || o || a[c] ? o ? !(l = c) : void 0 : (e.dataTypes.unshift(c), s(c), !1)
            }), l
        }
        var a = {},
            o = t === Ge;
        return s(e.dataTypes[0]) || !a["*"] && s("*")
    }

    function V(t, e) {
        var i, n, s = pt.ajaxSettings.flatOptions || {};
        for (n in e) e[n] !== undefined && ((s[n] ? t : i || (i = {}))[n] = e[n]);
        return i && pt.extend(!0, t, i), t
    }

    function K(t, e, i) {
        for (var n, s, a, o, r = t.contents, l = t.dataTypes;
            "*" === l[0];) l.shift(), s === undefined && (s = t.mimeType || e.getResponseHeader("Content-Type"));
        if (s)
            for (o in r)
                if (r[o] && r[o].test(s)) {
                    l.unshift(o);
                    break
                }
        if (l[0] in i) a = l[0];
        else {
            for (o in i) {
                if (!l[0] || t.converters[o + " " + l[0]]) {
                    a = o;
                    break
                }
                n || (n = o)
            }
            a = a || n
        }
        if (a) return a !== l[0] && l.unshift(a), i[a]
    }

    function X(t, e, i, n) {
        var s, a, o, r, l, c = {},
            u = t.dataTypes.slice();
        if (u[1])
            for (o in t.converters) c[o.toLowerCase()] = t.converters[o];
        for (a = u.shift(); a;)
            if (t.responseFields[a] && (i[t.responseFields[a]] = e), !l && n && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = a, a = u.shift())
                if ("*" === a) a = l;
                else if ("*" !== l && l !== a) {
            if (!(o = c[l + " " + a] || c["* " + a]))
                for (s in c)
                    if (r = s.split(" "), r[1] === a && (o = c[l + " " + r[0]] || c["* " + r[0]])) {
                        !0 === o ? o = c[s] : !0 !== c[s] && (a = r[0], u.unshift(r[1]));
                        break
                    }
            if (!0 !== o)
                if (o && t["throws"]) e = o(e);
                else try {
                    e = o(e)
                } catch (t) {
                    return {
                        state: "parsererror",
                        error: o ? t : "No conversion from " + l + " to " + a
                    }
                }
        }
        return {
            state: "success",
            data: e
        }
    }

    function Q(t) {
        return t.style && t.style.display || pt.css(t, "display")
    }

    function G(t) {
        if (!pt.contains(t.ownerDocument || nt, t)) return !0;
        for (; t && 1 === t.nodeType;) {
            if ("none" === Q(t) || "hidden" === t.type) return !0;
            t = t.parentNode
        }
        return !1
    }

    function J(t, e, i, n) {
        var s;
        if (pt.isArray(e)) pt.each(e, function(e, s) {
            i || ii.test(t) ? n(t, s) : J(t + "[" + ("object" == typeof s && null != s ? e : "") + "]", s, i, n)
        });
        else if (i || "object" !== pt.type(e)) n(t, e);
        else
            for (s in e) J(t + "[" + s + "]", e[s], i, n)
    }

    function Z() {
        try {
            return new t.XMLHttpRequest
        } catch (t) {}
    }

    function tt() {
        try {
            return new t.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {}
    }

    function et(t) {
        return pt.isWindow(t) ? t : 9 === t.nodeType && (t.defaultView || t.parentWindow)
    }
    var it = [],
        nt = t.document,
        st = it.slice,
        at = it.concat,
        ot = it.push,
        rt = it.indexOf,
        lt = {},
        ct = lt.toString,
        ut = lt.hasOwnProperty,
        dt = {},
        ht = "1.12.4",
        pt = function(t, e) {
            return new pt.fn.init(t, e)
        },
        ft = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        mt = /^-ms-/,
        gt = /-([\da-z])/gi,
        _t = function(t, e) {
            return e.toUpperCase()
        };
    pt.fn = pt.prototype = {
        jquery: ht,
        constructor: pt,
        selector: "",
        length: 0,
        toArray: function() {
            return st.call(this)
        },
        get: function(t) {
            return null != t ? t < 0 ? this[t + this.length] : this[t] : st.call(this)
        },
        pushStack: function(t) {
            var e = pt.merge(this.constructor(), t);
            return e.prevObject = this, e.context = this.context, e
        },
        each: function(t) {
            return pt.each(this, t)
        },
        map: function(t) {
            return this.pushStack(pt.map(this, function(e, i) {
                return t.call(e, i, e)
            }))
        },
        slice: function() {
            return this.pushStack(st.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(t) {
            var e = this.length,
                i = +t + (t < 0 ? e : 0);
            return this.pushStack(i >= 0 && i < e ? [this[i]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: ot,
        sort: it.sort,
        splice: it.splice
    }, pt.extend = pt.fn.extend = function() {
        var t, e, i, n, s, a, o = arguments[0] || {},
            r = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof o && (c = o, o = arguments[r] || {}, r++), "object" == typeof o || pt.isFunction(o) || (o = {}), r === l && (o = this, r--); r < l; r++)
            if (null != (s = arguments[r]))
                for (n in s) t = o[n], i = s[n], o !== i && (c && i && (pt.isPlainObject(i) || (e = pt.isArray(i))) ? (e ? (e = !1, a = t && pt.isArray(t) ? t : []) : a = t && pt.isPlainObject(t) ? t : {}, o[n] = pt.extend(c, a, i)) : i !== undefined && (o[n] = i));
        return o
    }, pt.extend({
        expando: "jQuery" + (ht + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(t) {
            throw new Error(t)
        },
        noop: function() {},
        isFunction: function(t) {
            return "function" === pt.type(t)
        },
        isArray: Array.isArray || function(t) {
            return "array" === pt.type(t)
        },
        isWindow: function(t) {
            return null != t && t == t.window
        },
        isNumeric: function(t) {
            var e = t && t.toString();
            return !pt.isArray(t) && e - parseFloat(e) + 1 >= 0
        },
        isEmptyObject: function(t) {
            var e;
            for (e in t) return !1;
            return !0
        },
        isPlainObject: function(t) {
            var e;
            if (!t || "object" !== pt.type(t) || t.nodeType || pt.isWindow(t)) return !1;
            try {
                if (t.constructor && !ut.call(t, "constructor") && !ut.call(t.constructor.prototype, "isPrototypeOf")) return !1
            } catch (t) {
                return !1
            }
            if (!dt.ownFirst)
                for (e in t) return ut.call(t, e);
            for (e in t);
            return e === undefined || ut.call(t, e)
        },
        type: function(t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? lt[ct.call(t)] || "object" : typeof t
        },
        globalEval: function(e) {
            e && pt.trim(e) && (t.execScript || function(e) {
                t.eval.call(t, e)
            })(e)
        },
        camelCase: function(t) {
            return t.replace(mt, "ms-").replace(gt, _t)
        },
        nodeName: function(t, e) {
            return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
        },
        each: function(t, e) {
            var n, s = 0;
            if (i(t))
                for (n = t.length; s < n && !1 !== e.call(t[s], s, t[s]); s++);
            else
                for (s in t)
                    if (!1 === e.call(t[s], s, t[s])) break;
            return t
        },
        trim: function(t) {
            return null == t ? "" : (t + "").replace(ft, "")
        },
        makeArray: function(t, e) {
            var n = e || [];
            return null != t && (i(Object(t)) ? pt.merge(n, "string" == typeof t ? [t] : t) : ot.call(n, t)), n
        },
        inArray: function(t, e, i) {
            var n;
            if (e) {
                if (rt) return rt.call(e, t, i);
                for (n = e.length, i = i ? i < 0 ? Math.max(0, n + i) : i : 0; i < n; i++)
                    if (i in e && e[i] === t) return i
            }
            return -1
        },
        merge: function(t, e) {
            for (var i = +e.length, n = 0, s = t.length; n < i;) t[s++] = e[n++];
            if (i !== i)
                for (; e[n] !== undefined;) t[s++] = e[n++];
            return t.length = s, t
        },
        grep: function(t, e, i) {
            for (var n = [], s = 0, a = t.length, o = !i; s < a; s++) !e(t[s], s) !== o && n.push(t[s]);
            return n
        },
        map: function(t, e, n) {
            var s, a, o = 0,
                r = [];
            if (i(t))
                for (s = t.length; o < s; o++) null != (a = e(t[o], o, n)) && r.push(a);
            else
                for (o in t) null != (a = e(t[o], o, n)) && r.push(a);
            return at.apply([], r)
        },
        guid: 1,
        proxy: function(t, e) {
            var i, n, s;
            return "string" == typeof e && (s = t[e], e = t, t = s), pt.isFunction(t) ? (i = st.call(arguments, 2), n = function() {
                return t.apply(e || this, i.concat(st.call(arguments)))
            }, n.guid = t.guid = t.guid || pt.guid++, n) : undefined
        },
        now: function() {
            return +new Date
        },
        support: dt
    }), "function" == typeof Symbol && (pt.fn[Symbol.iterator] = it[Symbol.iterator]), pt.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(t, e) {
        lt["[object " + e + "]"] = e.toLowerCase()
    });
    var vt = function(t) {
        function e(t, e, i, n) {
            var s, a, o, r, l, c, d, p, f = e && e.ownerDocument,
                m = e ? e.nodeType : 9;
            if (i = i || [], "string" != typeof t || !t || 1 !== m && 9 !== m && 11 !== m) return i;
            if (!n && ((e ? e.ownerDocument || e : R) !== M && E(e), e = e || M, N)) {
                if (11 !== m && (c = _t.exec(t)))
                    if (s = c[1]) {
                        if (9 === m) {
                            if (!(o = e.getElementById(s))) return i;
                            if (o.id === s) return i.push(o), i
                        } else if (f && (o = f.getElementById(s)) && H(e, o) && o.id === s) return i.push(o), i
                    } else {
                        if (c[2]) return J.apply(i, e.getElementsByTagName(t)), i;
                        if ((s = c[3]) && w.getElementsByClassName && e.getElementsByClassName) return J.apply(i, e.getElementsByClassName(s)), i
                    }
                if (w.qsa && !U[t + " "] && (!F || !F.test(t))) {
                    if (1 !== m) f = e, p = t;
                    else if ("object" !== e.nodeName.toLowerCase()) {
                        for ((r = e.getAttribute("id")) ? r = r.replace(bt, "\\$&") : e.setAttribute("id", r = O), d = C(t), a = d.length, l = ht.test(r) ? "#" + r : "[id='" + r + "']"; a--;) d[a] = l + " " + h(d[a]);
                        p = d.join(","), f = vt.test(t) && u(e.parentNode) || e
                    }
                    if (p) try {
                        return J.apply(i, f.querySelectorAll(p)), i
                    } catch (t) {} finally {
                        r === O && e.removeAttribute("id")
                    }
                }
            }
            return S(t.replace(rt, "$1"), e, i, n)
        }

        function i() {
            function t(i, n) {
                return e.push(i + " ") > x.cacheLength && delete t[e.shift()], t[i + " "] = n
            }
            var e = [];
            return t
        }

        function n(t) {
            return t[O] = !0, t
        }

        function s(t) {
            var e = M.createElement("div");
            try {
                return !!t(e)
            } catch (t) {
                return !1
            } finally {
                e.parentNode && e.parentNode.removeChild(e), e = null
            }
        }

        function a(t, e) {
            for (var i = t.split("|"), n = i.length; n--;) x.attrHandle[i[n]] = e
        }

        function o(t, e) {
            var i = e && t,
                n = i && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || V) - (~t.sourceIndex || V);
            if (n) return n;
            if (i)
                for (; i = i.nextSibling;)
                    if (i === e) return -1;
            return t ? 1 : -1
        }

        function r(t) {
            return function(e) {
                return "input" === e.nodeName.toLowerCase() && e.type === t
            }
        }

        function l(t) {
            return function(e) {
                var i = e.nodeName.toLowerCase();
                return ("input" === i || "button" === i) && e.type === t
            }
        }

        function c(t) {
            return n(function(e) {
                return e = +e, n(function(i, n) {
                    for (var s, a = t([], i.length, e), o = a.length; o--;) i[s = a[o]] && (i[s] = !(n[s] = i[s]))
                })
            })
        }

        function u(t) {
            return t && "undefined" != typeof t.getElementsByTagName && t
        }

        function d() {}

        function h(t) {
            for (var e = 0, i = t.length, n = ""; e < i; e++) n += t[e].value;
            return n
        }

        function p(t, e, i) {
            var n = e.dir,
                s = i && "parentNode" === n,
                a = W++;
            return e.first ? function(e, i, a) {
                for (; e = e[n];)
                    if (1 === e.nodeType || s) return t(e, i, a)
            } : function(e, i, o) {
                var r, l, c, u = [z, a];
                if (o) {
                    for (; e = e[n];)
                        if ((1 === e.nodeType || s) && t(e, i, o)) return !0
                } else
                    for (; e = e[n];)
                        if (1 === e.nodeType || s) {
                            if (c = e[O] || (e[O] = {}), l = c[e.uniqueID] || (c[e.uniqueID] = {}), (r = l[n]) && r[0] === z && r[1] === a) return u[2] = r[2];
                            if (l[n] = u, u[2] = t(e, i, o)) return !0
                        }
            }
        }

        function f(t) {
            return t.length > 1 ? function(e, i, n) {
                for (var s = t.length; s--;)
                    if (!t[s](e, i, n)) return !1;
                return !0
            } : t[0]
        }

        function m(t, i, n) {
            for (var s = 0, a = i.length; s < a; s++) e(t, i[s], n);
            return n
        }

        function g(t, e, i, n, s) {
            for (var a, o = [], r = 0, l = t.length, c = null != e; r < l; r++)(a = t[r]) && (i && !i(a, n, s) || (o.push(a), c && e.push(r)));
            return o
        }

        function _(t, e, i, s, a, o) {
            return s && !s[O] && (s = _(s)), a && !a[O] && (a = _(a, o)), n(function(n, o, r, l) {
                var c, u, d, h = [],
                    p = [],
                    f = o.length,
                    _ = n || m(e || "*", r.nodeType ? [r] : r, []),
                    v = !t || !n && e ? _ : g(_, h, t, r, l),
                    b = i ? a || (n ? t : f || s) ? [] : o : v;
                if (i && i(v, b, r, l), s)
                    for (c = g(b, p), s(c, [], r, l), u = c.length; u--;)(d = c[u]) && (b[p[u]] = !(v[p[u]] = d));
                if (n) {
                    if (a || t) {
                        if (a) {
                            for (c = [], u = b.length; u--;)(d = b[u]) && c.push(v[u] = d);
                            a(null, b = [], c, l)
                        }
                        for (u = b.length; u--;)(d = b[u]) && (c = a ? tt(n, d) : h[u]) > -1 && (n[c] = !(o[c] = d))
                    }
                } else b = g(b === o ? b.splice(f, b.length) : b), a ? a(null, o, b, l) : J.apply(o, b)
            })
        }

        function v(t) {
            for (var e, i, n, s = t.length, a = x.relative[t[0].type], o = a || x.relative[" "], r = a ? 1 : 0, l = p(function(t) {
                    return t === e
                }, o, !0), c = p(function(t) {
                    return tt(e, t) > -1
                }, o, !0), u = [function(t, i, n) {
                    var s = !a && (n || i !== T) || ((e = i).nodeType ? l(t, i, n) : c(t, i, n));
                    return e = null, s
                }]; r < s; r++)
                if (i = x.relative[t[r].type]) u = [p(f(u), i)];
                else {
                    if (i = x.filter[t[r].type].apply(null, t[r].matches), i[O]) {
                        for (n = ++r; n < s && !x.relative[t[n].type]; n++);
                        return _(r > 1 && f(u), r > 1 && h(t.slice(0, r - 1).concat({
                            value: " " === t[r - 2].type ? "*" : ""
                        })).replace(rt, "$1"), i, r < n && v(t.slice(r, n)), n < s && v(t = t.slice(n)), n < s && h(t))
                    }
                    u.push(i)
                }
            return f(u)
        }

        function b(t, i) {
            var s = i.length > 0,
                a = t.length > 0,
                o = function(n, o, r, l, c) {
                    var u, d, h, p = 0,
                        f = "0",
                        m = n && [],
                        _ = [],
                        v = T,
                        b = n || a && x.find.TAG("*", c),
                        y = z += null == v ? 1 : Math.random() || .1,
                        w = b.length;
                    for (c && (T = o === M || o || c); f !== w && null != (u = b[f]); f++) {
                        if (a && u) {
                            for (d = 0, o || u.ownerDocument === M || (E(u), r = !N); h = t[d++];)
                                if (h(u, o || M, r)) {
                                    l.push(u);
                                    break
                                }
                            c && (z = y)
                        }
                        s && ((u = !h && u) && p--, n && m.push(u))
                    }
                    if (p += f, s && f !== p) {
                        for (d = 0; h = i[d++];) h(m, _, o, r);
                        if (n) {
                            if (p > 0)
                                for (; f--;) m[f] || _[f] || (_[f] = Q.call(l));
                            _ = g(_)
                        }
                        J.apply(l, _), c && !n && _.length > 0 && p + i.length > 1 && e.uniqueSort(l)
                    }
                    return c && (z = y, T = v), m
                };
            return s ? n(o) : o
        }
        var y, w, x, $, k, C, D, S, T, I, A, E, M, P, N, F, j, L, H, O = "sizzle" + 1 * new Date,
            R = t.document,
            z = 0,
            W = 0,
            B = i(),
            q = i(),
            U = i(),
            Y = function(t, e) {
                return t === e && (A = !0), 0
            },
            V = 1 << 31,
            K = {}.hasOwnProperty,
            X = [],
            Q = X.pop,
            G = X.push,
            J = X.push,
            Z = X.slice,
            tt = function(t, e) {
                for (var i = 0, n = t.length; i < n; i++)
                    if (t[i] === e) return i;
                return -1
            },
            et = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            it = "[\\x20\\t\\r\\n\\f]",
            nt = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            st = "\\[" + it + "*(" + nt + ")(?:" + it + "*([*^$|!~]?=)" + it + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + nt + "))|)" + it + "*\\]",
            at = ":(" + nt + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + st + ")*)|.*)\\)|)",
            ot = new RegExp(it + "+", "g"),
            rt = new RegExp("^" + it + "+|((?:^|[^\\\\])(?:\\\\.)*)" + it + "+$", "g"),
            lt = new RegExp("^" + it + "*," + it + "*"),
            ct = new RegExp("^" + it + "*([>+~]|" + it + ")" + it + "*"),
            ut = new RegExp("=" + it + "*([^\\]'\"]*?)" + it + "*\\]", "g"),
            dt = new RegExp(at),
            ht = new RegExp("^" + nt + "$"),
            pt = {
                ID: new RegExp("^#(" + nt + ")"),
                CLASS: new RegExp("^\\.(" + nt + ")"),
                TAG: new RegExp("^(" + nt + "|[*])"),
                ATTR: new RegExp("^" + st),
                PSEUDO: new RegExp("^" + at),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + it + "*(even|odd|(([+-]|)(\\d*)n|)" + it + "*(?:([+-]|)" + it + "*(\\d+)|))" + it + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + et + ")$", "i"),
                needsContext: new RegExp("^" + it + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + it + "*((?:-\\d)?\\d*)" + it + "*\\)|)(?=[^-]|$)", "i")
            },
            ft = /^(?:input|select|textarea|button)$/i,
            mt = /^h\d$/i,
            gt = /^[^{]+\{\s*\[native \w/,
            _t = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            vt = /[+~]/,
            bt = /'|\\/g,
            yt = new RegExp("\\\\([\\da-f]{1,6}" + it + "?|(" + it + ")|.)", "ig"),
            wt = function(t, e, i) {
                var n = "0x" + e - 65536;
                return n !== n || i ? e : n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
            },
            xt = function() {
                E()
            };
        try {
            J.apply(X = Z.call(R.childNodes), R.childNodes), X[R.childNodes.length].nodeType
        } catch (t) {
            J = {
                apply: X.length ? function(t, e) {
                    G.apply(t, Z.call(e))
                } : function(t, e) {
                    for (var i = t.length, n = 0; t[i++] = e[n++];);
                    t.length = i - 1
                }
            }
        }
        w = e.support = {}, k = e.isXML = function(t) {
            var e = t && (t.ownerDocument || t).documentElement;
            return !!e && "HTML" !== e.nodeName
        }, E = e.setDocument = function(t) {
            var e, i, n = t ? t.ownerDocument || t : R;
            return n !== M && 9 === n.nodeType && n.documentElement ? (M = n, P = M.documentElement, N = !k(M), (i = M.defaultView) && i.top !== i && (i.addEventListener ? i.addEventListener("unload", xt, !1) : i.attachEvent && i.attachEvent("onunload", xt)), w.attributes = s(function(t) {
                return t.className = "i", !t.getAttribute("className")
            }), w.getElementsByTagName = s(function(t) {
                return t.appendChild(M.createComment("")), !t.getElementsByTagName("*").length
            }), w.getElementsByClassName = gt.test(M.getElementsByClassName), w.getById = s(function(t) {
                return P.appendChild(t).id = O, !M.getElementsByName || !M.getElementsByName(O).length
            }), w.getById ? (x.find.ID = function(t, e) {
                if ("undefined" != typeof e.getElementById && N) {
                    var i = e.getElementById(t);
                    return i ? [i] : []
                }
            }, x.filter.ID = function(t) {
                var e = t.replace(yt, wt);
                return function(t) {
                    return t.getAttribute("id") === e
                }
            }) : (delete x.find.ID, x.filter.ID = function(t) {
                var e = t.replace(yt, wt);
                return function(t) {
                    var i = "undefined" != typeof t.getAttributeNode && t.getAttributeNode("id");
                    return i && i.value === e
                }
            }), x.find.TAG = w.getElementsByTagName ? function(t, e) {
                return "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t) : w.qsa ? e.querySelectorAll(t) : void 0
            } : function(t, e) {
                var i, n = [],
                    s = 0,
                    a = e.getElementsByTagName(t);
                if ("*" === t) {
                    for (; i = a[s++];) 1 === i.nodeType && n.push(i);
                    return n
                }
                return a
            }, x.find.CLASS = w.getElementsByClassName && function(t, e) {
                if ("undefined" != typeof e.getElementsByClassName && N) return e.getElementsByClassName(t)
            }, j = [], F = [], (w.qsa = gt.test(M.querySelectorAll)) && (s(function(t) {
                P.appendChild(t).innerHTML = "<a id='" + O + "'></a><select id='" + O + "-\r\\' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && F.push("[*^$]=" + it + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || F.push("\\[" + it + "*(?:value|" + et + ")"), t.querySelectorAll("[id~=" + O + "-]").length || F.push("~="), t.querySelectorAll(":checked").length || F.push(":checked"), t.querySelectorAll("a#" + O + "+*").length || F.push(".#.+[+~]")
            }), s(function(t) {
                var e = M.createElement("input");
                e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && F.push("name" + it + "*[*^$|!~]?="), t.querySelectorAll(":enabled").length || F.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), F.push(",.*:")
            })), (w.matchesSelector = gt.test(L = P.matches || P.webkitMatchesSelector || P.mozMatchesSelector || P.oMatchesSelector || P.msMatchesSelector)) && s(function(t) {
                w.disconnectedMatch = L.call(t, "div"), L.call(t, "[s!='']:x"), j.push("!=", at)
            }), F = F.length && new RegExp(F.join("|")), j = j.length && new RegExp(j.join("|")), e = gt.test(P.compareDocumentPosition), H = e || gt.test(P.contains) ? function(t, e) {
                var i = 9 === t.nodeType ? t.documentElement : t,
                    n = e && e.parentNode;
                return t === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(n)))
            } : function(t, e) {
                if (e)
                    for (; e = e.parentNode;)
                        if (e === t) return !0;
                return !1
            }, Y = e ? function(t, e) {
                if (t === e) return A = !0, 0;
                var i = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return i || (i = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1, 1 & i || !w.sortDetached && e.compareDocumentPosition(t) === i ? t === M || t.ownerDocument === R && H(R, t) ? -1 : e === M || e.ownerDocument === R && H(R, e) ? 1 : I ? tt(I, t) - tt(I, e) : 0 : 4 & i ? -1 : 1)
            } : function(t, e) {
                if (t === e) return A = !0, 0;
                var i, n = 0,
                    s = t.parentNode,
                    a = e.parentNode,
                    r = [t],
                    l = [e];
                if (!s || !a) return t === M ? -1 : e === M ? 1 : s ? -1 : a ? 1 : I ? tt(I, t) - tt(I, e) : 0;
                if (s === a) return o(t, e);
                for (i = t; i = i.parentNode;) r.unshift(i);
                for (i = e; i = i.parentNode;) l.unshift(i);
                for (; r[n] === l[n];) n++;
                return n ? o(r[n], l[n]) : r[n] === R ? -1 : l[n] === R ? 1 : 0
            }, M) : M
        }, e.matches = function(t, i) {
            return e(t, null, null, i)
        }, e.matchesSelector = function(t, i) {
            if ((t.ownerDocument || t) !== M && E(t), i = i.replace(ut, "='$1']"), w.matchesSelector && N && !U[i + " "] && (!j || !j.test(i)) && (!F || !F.test(i))) try {
                var n = L.call(t, i);
                if (n || w.disconnectedMatch || t.document && 11 !== t.document.nodeType) return n
            } catch (t) {}
            return e(i, M, null, [t]).length > 0
        }, e.contains = function(t, e) {
            return (t.ownerDocument || t) !== M && E(t), H(t, e)
        }, e.attr = function(t, e) {
            (t.ownerDocument || t) !== M && E(t);
            var i = x.attrHandle[e.toLowerCase()],
                n = i && K.call(x.attrHandle, e.toLowerCase()) ? i(t, e, !N) : undefined;
            return n !== undefined ? n : w.attributes || !N ? t.getAttribute(e) : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
        }, e.error = function(t) {
            throw new Error("Syntax error, unrecognized expression: " + t)
        }, e.uniqueSort = function(t) {
            var e, i = [],
                n = 0,
                s = 0;
            if (A = !w.detectDuplicates, I = !w.sortStable && t.slice(0), t.sort(Y), A) {
                for (; e = t[s++];) e === t[s] && (n = i.push(s));
                for (; n--;) t.splice(i[n], 1)
            }
            return I = null, t
        }, $ = e.getText = function(t) {
            var e, i = "",
                n = 0,
                s = t.nodeType;
            if (s) {
                if (1 === s || 9 === s || 11 === s) {
                    if ("string" == typeof t.textContent) return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling) i += $(t)
                } else if (3 === s || 4 === s) return t.nodeValue
            } else
                for (; e = t[n++];) i += $(e);
            return i
        }, x = e.selectors = {
            cacheLength: 50,
            createPseudo: n,
            match: pt,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(t) {
                    return t[1] = t[1].replace(yt, wt), t[3] = (t[3] || t[4] || t[5] || "").replace(yt, wt), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
                },
                CHILD: function(t) {
                    return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
                },
                PSEUDO: function(t) {
                    var e, i = !t[6] && t[2];
                    return pt.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : i && dt.test(i) && (e = C(i, !0)) && (e = i.indexOf(")", i.length - e) - i.length) && (t[0] = t[0].slice(0, e), t[2] = i.slice(0, e)), t.slice(0, 3))
                }
            },
            filter: {
                TAG: function(t) {
                    var e = t.replace(yt, wt).toLowerCase();
                    return "*" === t ? function() {
                        return !0
                    } : function(t) {
                        return t.nodeName && t.nodeName.toLowerCase() === e
                    }
                },
                CLASS: function(t) {
                    var e = B[t + " "];
                    return e || (e = new RegExp("(^|" + it + ")" + t + "(" + it + "|$)")) && B(t, function(t) {
                        return e.test("string" == typeof t.className && t.className || "undefined" != typeof t.getAttribute && t.getAttribute("class") || "")
                    })
                },
                ATTR: function(t, i, n) {
                    return function(s) {
                        var a = e.attr(s, t);
                        return null == a ? "!=" === i : !i || (a += "", "=" === i ? a === n : "!=" === i ? a !== n : "^=" === i ? n && 0 === a.indexOf(n) : "*=" === i ? n && a.indexOf(n) > -1 : "$=" === i ? n && a.slice(-n.length) === n : "~=" === i ? (" " + a.replace(ot, " ") + " ").indexOf(n) > -1 : "|=" === i && (a === n || a.slice(0, n.length + 1) === n + "-"))
                    }
                },
                CHILD: function(t, e, i, n, s) {
                    var a = "nth" !== t.slice(0, 3),
                        o = "last" !== t.slice(-4),
                        r = "of-type" === e;
                    return 1 === n && 0 === s ? function(t) {
                        return !!t.parentNode
                    } : function(e, i, l) {
                        var c, u, d, h, p, f, m = a !== o ? "nextSibling" : "previousSibling",
                            g = e.parentNode,
                            _ = r && e.nodeName.toLowerCase(),
                            v = !l && !r,
                            b = !1;
                        if (g) {
                            if (a) {
                                for (; m;) {
                                    for (h = e; h = h[m];)
                                        if (r ? h.nodeName.toLowerCase() === _ : 1 === h.nodeType) return !1;
                                    f = m = "only" === t && !f && "nextSibling"
                                }
                                return !0
                            }
                            if (f = [o ? g.firstChild : g.lastChild], o && v) {
                                for (h = g, d = h[O] || (h[O] = {}), u = d[h.uniqueID] || (d[h.uniqueID] = {}), c = u[t] || [], p = c[0] === z && c[1], b = p && c[2], h = p && g.childNodes[p]; h = ++p && h && h[m] || (b = p = 0) || f.pop();)
                                    if (1 === h.nodeType && ++b && h === e) {
                                        u[t] = [z, p, b];
                                        break
                                    }
                            } else if (v && (h = e, d = h[O] || (h[O] = {}), u = d[h.uniqueID] || (d[h.uniqueID] = {}), c = u[t] || [], p = c[0] === z && c[1], b = p), !1 === b)
                                for (;
                                    (h = ++p && h && h[m] || (b = p = 0) || f.pop()) && ((r ? h.nodeName.toLowerCase() !== _ : 1 !== h.nodeType) || !++b || (v && (d = h[O] || (h[O] = {}), u = d[h.uniqueID] || (d[h.uniqueID] = {}), u[t] = [z, b]), h !== e)););
                            return (b -= s) === n || b % n == 0 && b / n >= 0
                        }
                    }
                },
                PSEUDO: function(t, i) {
                    var s, a = x.pseudos[t] || x.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
                    return a[O] ? a(i) : a.length > 1 ? (s = [t, t, "", i], x.setFilters.hasOwnProperty(t.toLowerCase()) ? n(function(t, e) {
                        for (var n, s = a(t, i), o = s.length; o--;) n = tt(t, s[o]), t[n] = !(e[n] = s[o])
                    }) : function(t) {
                        return a(t, 0, s)
                    }) : a
                }
            },
            pseudos: {
                not: n(function(t) {
                    var e = [],
                        i = [],
                        s = D(t.replace(rt, "$1"));
                    return s[O] ? n(function(t, e, i, n) {
                        for (var a, o = s(t, null, n, []), r = t.length; r--;)(a = o[r]) && (t[r] = !(e[r] = a))
                    }) : function(t, n, a) {
                        return e[0] = t, s(e, null, a, i), e[0] = null, !i.pop()
                    }
                }),
                has: n(function(t) {
                    return function(i) {
                        return e(t, i).length > 0
                    }
                }),
                contains: n(function(t) {
                    return t = t.replace(yt, wt),
                        function(e) {
                            return (e.textContent || e.innerText || $(e)).indexOf(t) > -1
                        }
                }),
                lang: n(function(t) {
                    return ht.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(yt, wt).toLowerCase(),
                        function(e) {
                            var i;
                            do {
                                if (i = N ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (i = i.toLowerCase()) === t || 0 === i.indexOf(t + "-")
                            } while ((e = e.parentNode) && 1 === e.nodeType);
                            return !1
                        }
                }),
                target: function(e) {
                    var i = t.location && t.location.hash;
                    return i && i.slice(1) === e.id
                },
                root: function(t) {
                    return t === P
                },
                focus: function(t) {
                    return t === M.activeElement && (!M.hasFocus || M.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                },
                enabled: function(t) {
                    return !1 === t.disabled
                },
                disabled: function(t) {
                    return !0 === t.disabled
                },
                checked: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && !!t.checked || "option" === e && !!t.selected
                },
                selected: function(t) {
                    return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected
                },
                empty: function(t) {
                    for (t = t.firstChild; t; t = t.nextSibling)
                        if (t.nodeType < 6) return !1;
                    return !0
                },
                parent: function(t) {
                    return !x.pseudos.empty(t)
                },
                header: function(t) {
                    return mt.test(t.nodeName)
                },
                input: function(t) {
                    return ft.test(t.nodeName)
                },
                button: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && "button" === t.type || "button" === e
                },
                text: function(t) {
                    var e;
                    return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                },
                first: c(function() {
                    return [0]
                }),
                last: c(function(t, e) {
                    return [e - 1]
                }),
                eq: c(function(t, e, i) {
                    return [i < 0 ? i + e : i]
                }),
                even: c(function(t, e) {
                    for (var i = 0; i < e; i += 2) t.push(i);
                    return t
                }),
                odd: c(function(t, e) {
                    for (var i = 1; i < e; i += 2) t.push(i);
                    return t
                }),
                lt: c(function(t, e, i) {
                    for (var n = i < 0 ? i + e : i; --n >= 0;) t.push(n);
                    return t
                }),
                gt: c(function(t, e, i) {
                    for (var n = i < 0 ? i + e : i; ++n < e;) t.push(n);
                    return t
                })
            }
        }, x.pseudos.nth = x.pseudos.eq;
        for (y in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) x.pseudos[y] = r(y);
        for (y in {
                submit: !0,
                reset: !0
            }) x.pseudos[y] = l(y);
        return d.prototype = x.filters = x.pseudos, x.setFilters = new d, C = e.tokenize = function(t, i) {
            var n, s, a, o, r, l, c, u = q[t + " "];
            if (u) return i ? 0 : u.slice(0);
            for (r = t, l = [], c = x.preFilter; r;) {
                n && !(s = lt.exec(r)) || (s && (r = r.slice(s[0].length) || r), l.push(a = [])), n = !1, (s = ct.exec(r)) && (n = s.shift(), a.push({
                    value: n,
                    type: s[0].replace(rt, " ")
                }), r = r.slice(n.length));
                for (o in x.filter) !(s = pt[o].exec(r)) || c[o] && !(s = c[o](s)) || (n = s.shift(), a.push({
                    value: n,
                    type: o,
                    matches: s
                }), r = r.slice(n.length));
                if (!n) break
            }
            return i ? r.length : r ? e.error(t) : q(t, l).slice(0)
        }, D = e.compile = function(t, e) {
            var i, n = [],
                s = [],
                a = U[t + " "];
            if (!a) {
                for (e || (e = C(t)), i = e.length; i--;) a = v(e[i]), a[O] ? n.push(a) : s.push(a);
                a = U(t, b(s, n)), a.selector = t
            }
            return a
        }, S = e.select = function(t, e, i, n) {
            var s, a, o, r, l, c = "function" == typeof t && t,
                d = !n && C(t = c.selector || t);
            if (i = i || [], 1 === d.length) {
                if (a = d[0] = d[0].slice(0), a.length > 2 && "ID" === (o = a[0]).type && w.getById && 9 === e.nodeType && N && x.relative[a[1].type]) {
                    if (!(e = (x.find.ID(o.matches[0].replace(yt, wt), e) || [])[0])) return i;
                    c && (e = e.parentNode), t = t.slice(a.shift().value.length)
                }
                for (s = pt.needsContext.test(t) ? 0 : a.length; s-- && (o = a[s], !x.relative[r = o.type]);)
                    if ((l = x.find[r]) && (n = l(o.matches[0].replace(yt, wt), vt.test(a[0].type) && u(e.parentNode) || e))) {
                        if (a.splice(s, 1), !(t = n.length && h(a))) return J.apply(i, n), i;
                        break
                    }
            }
            return (c || D(t, d))(n, e, !N, i, !e || vt.test(t) && u(e.parentNode) || e), i
        }, w.sortStable = O.split("").sort(Y).join("") === O, w.detectDuplicates = !!A, E(), w.sortDetached = s(function(t) {
            return 1 & t.compareDocumentPosition(M.createElement("div"))
        }), s(function(t) {
            return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
        }) || a("type|href|height|width", function(t, e, i) {
            if (!i) return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }), w.attributes && s(function(t) {
            return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
        }) || a("value", function(t, e, i) {
            if (!i && "input" === t.nodeName.toLowerCase()) return t.defaultValue
        }), s(function(t) {
            return null == t.getAttribute("disabled")
        }) || a(et, function(t, e, i) {
            var n;
            if (!i) return !0 === t[e] ? e.toLowerCase() : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
        }), e
    }(t);
    pt.find = vt, pt.expr = vt.selectors, pt.expr[":"] = pt.expr.pseudos, pt.uniqueSort = pt.unique = vt.uniqueSort, pt.text = vt.getText, pt.isXMLDoc = vt.isXML, pt.contains = vt.contains;
    var bt = function(t, e, i) {
            for (var n = [], s = i !== undefined;
                (t = t[e]) && 9 !== t.nodeType;)
                if (1 === t.nodeType) {
                    if (s && pt(t).is(i)) break;
                    n.push(t)
                }
            return n
        },
        yt = function(t, e) {
            for (var i = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && i.push(t);
            return i
        },
        wt = pt.expr.match.needsContext,
        xt = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
        $t = /^.[^:#\[\.,]*$/;
    pt.filter = function(t, e, i) {
        var n = e[0];
        return i && (t = ":not(" + t + ")"), 1 === e.length && 1 === n.nodeType ? pt.find.matchesSelector(n, t) ? [n] : [] : pt.find.matches(t, pt.grep(e, function(t) {
            return 1 === t.nodeType
        }))
    }, pt.fn.extend({
        find: function(t) {
            var e, i = [],
                n = this,
                s = n.length;
            if ("string" != typeof t) return this.pushStack(pt(t).filter(function() {
                for (e = 0; e < s; e++)
                    if (pt.contains(n[e], this)) return !0
            }));
            for (e = 0; e < s; e++) pt.find(t, n[e], i);
            return i = this.pushStack(s > 1 ? pt.unique(i) : i), i.selector = this.selector ? this.selector + " " + t : t, i
        },
        filter: function(t) {
            return this.pushStack(n(this, t || [], !1))
        },
        not: function(t) {
            return this.pushStack(n(this, t || [], !0))
        },
        is: function(t) {
            return !!n(this, "string" == typeof t && wt.test(t) ? pt(t) : t || [], !1).length
        }
    });
    var kt, Ct = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
    (pt.fn.init = function(t, e, i) {
        var n, s;
        if (!t) return this;
        if (i = i || kt, "string" == typeof t) {
            if (!(n = "<" === t.charAt(0) && ">" === t.charAt(t.length - 1) && t.length >= 3 ? [null, t, null] : Ct.exec(t)) || !n[1] && e) return !e || e.jquery ? (e || i).find(t) : this.constructor(e).find(t);
            if (n[1]) {
                if (e = e instanceof pt ? e[0] : e, pt.merge(this, pt.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : nt, !0)), xt.test(n[1]) && pt.isPlainObject(e))
                    for (n in e) pt.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
                return this
            }
            if ((s = nt.getElementById(n[2])) && s.parentNode) {
                if (s.id !== n[2]) return kt.find(t);
                this.length = 1, this[0] = s
            }
            return this.context = nt, this.selector = t, this
        }
        return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : pt.isFunction(t) ? "undefined" != typeof i.ready ? i.ready(t) : t(pt) : (t.selector !== undefined && (this.selector = t.selector, this.context = t.context), pt.makeArray(t, this))
    }).prototype = pt.fn, kt = pt(nt);
    var Dt = /^(?:parents|prev(?:Until|All))/,
        St = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    pt.fn.extend({
        has: function(t) {
            var e, i = pt(t, this),
                n = i.length;
            return this.filter(function() {
                for (e = 0; e < n; e++)
                    if (pt.contains(this, i[e])) return !0
            })
        },
        closest: function(t, e) {
            for (var i, n = 0, s = this.length, a = [], o = wt.test(t) || "string" != typeof t ? pt(t, e || this.context) : 0; n < s; n++)
                for (i = this[n]; i && i !== e; i = i.parentNode)
                    if (i.nodeType < 11 && (o ? o.index(i) > -1 : 1 === i.nodeType && pt.find.matchesSelector(i, t))) {
                        a.push(i);
                        break
                    }
            return this.pushStack(a.length > 1 ? pt.uniqueSort(a) : a)
        },
        index: function(t) {
            return t ? "string" == typeof t ? pt.inArray(this[0], pt(t)) : pt.inArray(t.jquery ? t[0] : t, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(t, e) {
            return this.pushStack(pt.uniqueSort(pt.merge(this.get(), pt(t, e))))
        },
        addBack: function(t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }
    }), pt.each({
        parent: function(t) {
            var e = t.parentNode;
            return e && 11 !== e.nodeType ? e : null
        },
        parents: function(t) {
            return bt(t, "parentNode")
        },
        parentsUntil: function(t, e, i) {
            return bt(t, "parentNode", i)
        },
        next: function(t) {
            return s(t, "nextSibling")
        },
        prev: function(t) {
            return s(t, "previousSibling")
        },
        nextAll: function(t) {
            return bt(t, "nextSibling")
        },
        prevAll: function(t) {
            return bt(t, "previousSibling")
        },
        nextUntil: function(t, e, i) {
            return bt(t, "nextSibling", i)
        },
        prevUntil: function(t, e, i) {
            return bt(t, "previousSibling", i)
        },
        siblings: function(t) {
            return yt((t.parentNode || {}).firstChild, t)
        },
        children: function(t) {
            return yt(t.firstChild)
        },
        contents: function(t) {
            return pt.nodeName(t, "iframe") ? t.contentDocument || t.contentWindow.document : pt.merge([], t.childNodes)
        }
    }, function(t, e) {
        pt.fn[t] = function(i, n) {
            var s = pt.map(this, e, i);
            return "Until" !== t.slice(-5) && (n = i), n && "string" == typeof n && (s = pt.filter(n, s)), this.length > 1 && (St[t] || (s = pt.uniqueSort(s)), Dt.test(t) && (s = s.reverse())), this.pushStack(s)
        }
    });
    var Tt = /\S+/g;
    pt.Callbacks = function(t) {
        t = "string" == typeof t ? a(t) : pt.extend({}, t);
        var e, i, n, s, o = [],
            r = [],
            l = -1,
            c = function() {
                for (s = t.once, n = e = !0; r.length; l = -1)
                    for (i = r.shift(); ++l < o.length;) !1 === o[l].apply(i[0], i[1]) && t.stopOnFalse && (l = o.length, i = !1);
                t.memory || (i = !1), e = !1, s && (o = i ? [] : "")
            },
            u = {
                add: function() {
                    return o && (i && !e && (l = o.length - 1, r.push(i)), function e(i) {
                        pt.each(i, function(i, n) {
                            pt.isFunction(n) ? t.unique && u.has(n) || o.push(n) : n && n.length && "string" !== pt.type(n) && e(n)
                        })
                    }(arguments), i && !e && c()), this
                },
                remove: function() {
                    return pt.each(arguments, function(t, e) {
                        for (var i;
                            (i = pt.inArray(e, o, i)) > -1;) o.splice(i, 1), i <= l && l--
                    }), this
                },
                has: function(t) {
                    return t ? pt.inArray(t, o) > -1 : o.length > 0
                },
                empty: function() {
                    return o && (o = []), this
                },
                disable: function() {
                    return s = r = [], o = i = "", this
                },
                disabled: function() {
                    return !o
                },
                lock: function() {
                    return s = !0, i || u.disable(), this
                },
                locked: function() {
                    return !!s
                },
                fireWith: function(t, i) {
                    return s || (i = i || [], i = [t, i.slice ? i.slice() : i], r.push(i), e || c()), this
                },
                fire: function() {
                    return u.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!n
                }
            };
        return u
    }, pt.extend({
        Deferred: function(t) {
            var e = [
                    ["resolve", "done", pt.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", pt.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", pt.Callbacks("memory")]
                ],
                i = "pending",
                n = {
                    state: function() {
                        return i
                    },
                    always: function() {
                        return s.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var t = arguments;
                        return pt.Deferred(function(i) {
                            pt.each(e, function(e, a) {
                                var o = pt.isFunction(t[e]) && t[e];
                                s[a[1]](function() {
                                    var t = o && o.apply(this, arguments);
                                    t && pt.isFunction(t.promise) ? t.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[a[0] + "With"](this === n ? i.promise() : this, o ? [t] : arguments)
                                })
                            }), t = null
                        }).promise()
                    },
                    promise: function(t) {
                        return null != t ? pt.extend(t, n) : n
                    }
                },
                s = {};
            return n.pipe = n.then, pt.each(e, function(t, a) {
                var o = a[2],
                    r = a[3];
                n[a[1]] = o.add, r && o.add(function() {
                    i = r
                }, e[1 ^ t][2].disable, e[2][2].lock), s[a[0]] = function() {
                    return s[a[0] + "With"](this === s ? n : this, arguments), this
                }, s[a[0] + "With"] = o.fireWith
            }), n.promise(s), t && t.call(s, s), s
        },
        when: function(t) {
            var e, i, n, s = 0,
                a = st.call(arguments),
                o = a.length,
                r = 1 !== o || t && pt.isFunction(t.promise) ? o : 0,
                l = 1 === r ? t : pt.Deferred(),
                c = function(t, i, n) {
                    return function(s) {
                        i[t] = this, n[t] = arguments.length > 1 ? st.call(arguments) : s, n === e ? l.notifyWith(i, n) : --r || l.resolveWith(i, n)
                    }
                };
            if (o > 1)
                for (e = new Array(o), i = new Array(o), n = new Array(o); s < o; s++) a[s] && pt.isFunction(a[s].promise) ? a[s].promise().progress(c(s, i, e)).done(c(s, n, a)).fail(l.reject) : --r;
            return r || l.resolveWith(n, a), l.promise()
        }
    });
    var It;
    pt.fn.ready = function(t) {
        return pt.ready.promise().done(t), this
    }, pt.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(t) {
            t ? pt.readyWait++ : pt.ready(!0)
        },
        ready: function(t) {
            (!0 === t ? --pt.readyWait : pt.isReady) || (pt.isReady = !0, !0 !== t && --pt.readyWait > 0 || (It.resolveWith(nt, [pt]), pt.fn.triggerHandler && (pt(nt).triggerHandler("ready"), pt(nt).off("ready"))))
        }
    }), pt.ready.promise = function(e) {
        if (!It)
            if (It = pt.Deferred(), "complete" === nt.readyState || "loading" !== nt.readyState && !nt.documentElement.doScroll) t.setTimeout(pt.ready);
            else if (nt.addEventListener) nt.addEventListener("DOMContentLoaded", r), t.addEventListener("load", r);
        else {
            nt.attachEvent("onreadystatechange", r), t.attachEvent("onload", r);
            var i = !1;
            try {
                i = null == t.frameElement && nt.documentElement
            } catch (t) {}
            i && i.doScroll && function e() {
                if (!pt.isReady) {
                    try {
                        i.doScroll("left")
                    } catch (i) {
                        return t.setTimeout(e, 50)
                    }
                    o(), pt.ready()
                }
            }()
        }
        return It.promise(e)
    }, pt.ready.promise();
    var At;
    for (At in pt(dt)) break;
    dt.ownFirst = "0" === At, dt.inlineBlockNeedsLayout = !1, pt(function() {
            var t, e, i, n;
            (i = nt.getElementsByTagName("body")[0]) && i.style && (e = nt.createElement("div"), n = nt.createElement("div"), n.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", i.appendChild(n).appendChild(e), "undefined" != typeof e.style.zoom && (e.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", dt.inlineBlockNeedsLayout = t = 3 === e.offsetWidth, t && (i.style.zoom = 1)), i.removeChild(n))
        }),
        function() {
            var t = nt.createElement("div");
            dt.deleteExpando = !0;
            try {
                delete t.test
            } catch (t) {
                dt.deleteExpando = !1
            }
            t = null
        }();
    var Et = function(t) {
            var e = pt.noData[(t.nodeName + " ").toLowerCase()],
                i = +t.nodeType || 1;
            return (1 === i || 9 === i) && (!e || !0 !== e && t.getAttribute("classid") === e)
        },
        Mt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        Pt = /([A-Z])/g;
    pt.extend({
            cache: {},
            noData: {
                "applet ": !0,
                "embed ": !0,
                "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
            },
            hasData: function(t) {
                return !!(t = t.nodeType ? pt.cache[t[pt.expando]] : t[pt.expando]) && !c(t)
            },
            data: function(t, e, i) {
                return u(t, e, i)
            },
            removeData: function(t, e) {
                return d(t, e)
            },
            _data: function(t, e, i) {
                return u(t, e, i, !0)
            },
            _removeData: function(t, e) {
                return d(t, e, !0)
            }
        }), pt.fn.extend({
            data: function(t, e) {
                var i, n, s, a = this[0],
                    o = a && a.attributes;
                if (t === undefined) {
                    if (this.length && (s = pt.data(a), 1 === a.nodeType && !pt._data(a, "parsedAttrs"))) {
                        for (i = o.length; i--;) o[i] && (n = o[i].name, 0 === n.indexOf("data-") && (n = pt.camelCase(n.slice(5)), l(a, n, s[n])));
                        pt._data(a, "parsedAttrs", !0)
                    }
                    return s
                }
                return "object" == typeof t ? this.each(function() {
                    pt.data(this, t)
                }) : arguments.length > 1 ? this.each(function() {
                    pt.data(this, t, e)
                }) : a ? l(a, t, pt.data(a, t)) : undefined
            },
            removeData: function(t) {
                return this.each(function() {
                    pt.removeData(this, t)
                })
            }
        }), pt.extend({
            queue: function(t, e, i) {
                var n;
                if (t) return e = (e || "fx") + "queue", n = pt._data(t, e), i && (!n || pt.isArray(i) ? n = pt._data(t, e, pt.makeArray(i)) : n.push(i)), n || []
            },
            dequeue: function(t, e) {
                e = e || "fx";
                var i = pt.queue(t, e),
                    n = i.length,
                    s = i.shift(),
                    a = pt._queueHooks(t, e),
                    o = function() {
                        pt.dequeue(t, e)
                    };
                "inprogress" === s && (s = i.shift(), n--), s && ("fx" === e && i.unshift("inprogress"), delete a.stop, s.call(t, o, a)), !n && a && a.empty.fire()
            },
            _queueHooks: function(t, e) {
                var i = e + "queueHooks";
                return pt._data(t, i) || pt._data(t, i, {
                    empty: pt.Callbacks("once memory").add(function() {
                        pt._removeData(t, e + "queue"), pt._removeData(t, i)
                    })
                })
            }
        }), pt.fn.extend({
            queue: function(t, e) {
                var i = 2;
                return "string" != typeof t && (e = t, t = "fx", i--), arguments.length < i ? pt.queue(this[0], t) : e === undefined ? this : this.each(function() {
                    var i = pt.queue(this, t, e);
                    pt._queueHooks(this, t), "fx" === t && "inprogress" !== i[0] && pt.dequeue(this, t)
                })
            },
            dequeue: function(t) {
                return this.each(function() {
                    pt.dequeue(this, t)
                })
            },
            clearQueue: function(t) {
                return this.queue(t || "fx", [])
            },
            promise: function(t, e) {
                var i, n = 1,
                    s = pt.Deferred(),
                    a = this,
                    o = this.length,
                    r = function() {
                        --n || s.resolveWith(a, [a])
                    };
                for ("string" != typeof t && (e = t, t = undefined), t = t || "fx"; o--;)(i = pt._data(a[o], t + "queueHooks")) && i.empty && (n++, i.empty.add(r));
                return r(), s.promise(e)
            }
        }),
        function() {
            var t;
            dt.shrinkWrapBlocks = function() {
                if (null != t) return t;
                t = !1;
                var e, i, n;
                return (i = nt.getElementsByTagName("body")[0]) && i.style ? (e = nt.createElement("div"), n = nt.createElement("div"), n.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", i.appendChild(n).appendChild(e), "undefined" != typeof e.style.zoom && (e.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", e.appendChild(nt.createElement("div")).style.width = "5px", t = 3 !== e.offsetWidth), i.removeChild(n), t) : void 0
            }
        }();
    var Nt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Ft = new RegExp("^(?:([+-])=|)(" + Nt + ")([a-z%]*)$", "i"),
        jt = ["Top", "Right", "Bottom", "Left"],
        Lt = function(t, e) {
            return t = e || t, "none" === pt.css(t, "display") || !pt.contains(t.ownerDocument, t)
        },
        Ht = function(t, e, i, n, s, a, o) {
            var r = 0,
                l = t.length,
                c = null == i;
            if ("object" === pt.type(i)) {
                s = !0;
                for (r in i) Ht(t, e, r, i[r], !0, a, o)
            } else if (n !== undefined && (s = !0, pt.isFunction(n) || (o = !0), c && (o ? (e.call(t, n), e = null) : (c = e, e = function(t, e, i) {
                    return c.call(pt(t), i)
                })), e))
                for (; r < l; r++) e(t[r], i, o ? n : n.call(t[r], r, e(t[r], i)));
            return s ? t : c ? e.call(t) : l ? e(t[0], i) : a
        },
        Ot = /^(?:checkbox|radio)$/i,
        Rt = /<([\w:-]+)/,
        zt = /^$|\/(?:java|ecma)script/i,
        Wt = /^\s+/,
        Bt = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";
    ! function() {
        var t = nt.createElement("div"),
            e = nt.createDocumentFragment(),
            i = nt.createElement("input");
        t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", dt.leadingWhitespace = 3 === t.firstChild.nodeType, dt.tbody = !t.getElementsByTagName("tbody").length, dt.htmlSerialize = !!t.getElementsByTagName("link").length, dt.html5Clone = "<:nav></:nav>" !== nt.createElement("nav").cloneNode(!0).outerHTML, i.type = "checkbox", i.checked = !0, e.appendChild(i), dt.appendChecked = i.checked, t.innerHTML = "<textarea>x</textarea>", dt.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue, e.appendChild(t), i = nt.createElement("input"), i.setAttribute("type", "radio"), i.setAttribute("checked", "checked"), i.setAttribute("name", "t"), t.appendChild(i), dt.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, dt.noCloneEvent = !!t.addEventListener, t[pt.expando] = 1, dt.attributes = !t.getAttribute(pt.expando)
    }();
    var qt = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        legend: [1, "<fieldset>", "</fieldset>"],
        area: [1, "<map>", "</map>"],
        param: [1, "<object>", "</object>"],
        thead: [1, "<table>", "</table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: dt.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
    };
    qt.optgroup = qt.option, qt.tbody = qt.tfoot = qt.colgroup = qt.caption = qt.thead, qt.th = qt.td;
    var Ut = /<|&#?\w+;/,
        Yt = /<tbody/i;
    ! function() {
        var e, i, n = nt.createElement("div");
        for (e in {
                submit: !0,
                change: !0,
                focusin: !0
            }) i = "on" + e, (dt[e] = i in t) || (n.setAttribute(i, "t"), dt[e] = !1 === n.attributes[i].expando);
        n = null
    }();
    var Vt = /^(?:input|select|textarea)$/i,
        Kt = /^key/,
        Xt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        Qt = /^(?:focusinfocus|focusoutblur)$/,
        Gt = /^([^.]*)(?:\.(.+)|)/;
    pt.event = {
        global: {},
        add: function(t, e, i, n, s) {
            var a, o, r, l, c, u, d, h, p, f, m, g = pt._data(t);
            if (g) {
                for (i.handler && (l = i, i = l.handler, s = l.selector), i.guid || (i.guid = pt.guid++), (o = g.events) || (o = g.events = {}), (u = g.handle) || (u = g.handle = function(t) {
                        return void 0 === pt || t && pt.event.triggered === t.type ? undefined : pt.event.dispatch.apply(u.elem, arguments)
                    }, u.elem = t), e = (e || "").match(Tt) || [""], r = e.length; r--;) a = Gt.exec(e[r]) || [], p = m = a[1], f = (a[2] || "").split(".").sort(), p && (c = pt.event.special[p] || {}, p = (s ? c.delegateType : c.bindType) || p, c = pt.event.special[p] || {}, d = pt.extend({
                    type: p,
                    origType: m,
                    data: n,
                    handler: i,
                    guid: i.guid,
                    selector: s,
                    needsContext: s && pt.expr.match.needsContext.test(s),
                    namespace: f.join(".")
                }, l), (h = o[p]) || (h = o[p] = [], h.delegateCount = 0, c.setup && !1 !== c.setup.call(t, n, f, u) || (t.addEventListener ? t.addEventListener(p, u, !1) : t.attachEvent && t.attachEvent("on" + p, u))), c.add && (c.add.call(t, d), d.handler.guid || (d.handler.guid = i.guid)), s ? h.splice(h.delegateCount++, 0, d) : h.push(d), pt.event.global[p] = !0);
                t = null
            }
        },
        remove: function(t, e, i, n, s) {
            var a, o, r, l, c, u, d, h, p, f, m, g = pt.hasData(t) && pt._data(t);
            if (g && (u = g.events)) {
                for (e = (e || "").match(Tt) || [""], c = e.length; c--;)
                    if (r = Gt.exec(e[c]) || [], p = m = r[1], f = (r[2] || "").split(".").sort(), p) {
                        for (d = pt.event.special[p] || {}, p = (n ? d.delegateType : d.bindType) || p, h = u[p] || [], r = r[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = a = h.length; a--;) o = h[a], !s && m !== o.origType || i && i.guid !== o.guid || r && !r.test(o.namespace) || n && n !== o.selector && ("**" !== n || !o.selector) || (h.splice(a, 1), o.selector && h.delegateCount--, d.remove && d.remove.call(t, o));
                        l && !h.length && (d.teardown && !1 !== d.teardown.call(t, f, g.handle) || pt.removeEvent(t, p, g.handle), delete u[p])
                    } else
                        for (p in u) pt.event.remove(t, p + e[c], i, n, !0);
                pt.isEmptyObject(u) && (delete g.handle, pt._removeData(t, "events"))
            }
        },
        trigger: function(e, i, n, s) {
            var a, o, r, l, c, u, d, h = [n || nt],
                p = ut.call(e, "type") ? e.type : e,
                f = ut.call(e, "namespace") ? e.namespace.split(".") : [];
            if (r = u = n = n || nt, 3 !== n.nodeType && 8 !== n.nodeType && !Qt.test(p + pt.event.triggered) && (p.indexOf(".") > -1 && (f = p.split("."), p = f.shift(), f.sort()), o = p.indexOf(":") < 0 && "on" + p, e = e[pt.expando] ? e : new pt.Event(p, "object" == typeof e && e), e.isTrigger = s ? 2 : 3, e.namespace = f.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = undefined, e.target || (e.target = n), i = null == i ? [e] : pt.makeArray(i, [e]), c = pt.event.special[p] || {}, s || !c.trigger || !1 !== c.trigger.apply(n, i))) {
                if (!s && !c.noBubble && !pt.isWindow(n)) {
                    for (l = c.delegateType || p, Qt.test(l + p) || (r = r.parentNode); r; r = r.parentNode) h.push(r), u = r;
                    u === (n.ownerDocument || nt) && h.push(u.defaultView || u.parentWindow || t)
                }
                for (d = 0;
                    (r = h[d++]) && !e.isPropagationStopped();) e.type = d > 1 ? l : c.bindType || p, a = (pt._data(r, "events") || {})[e.type] && pt._data(r, "handle"), a && a.apply(r, i), (a = o && r[o]) && a.apply && Et(r) && (e.result = a.apply(r, i), !1 === e.result && e.preventDefault());
                if (e.type = p, !s && !e.isDefaultPrevented() && (!c._default || !1 === c._default.apply(h.pop(), i)) && Et(n) && o && n[p] && !pt.isWindow(n)) {
                    u = n[o], u && (n[o] = null), pt.event.triggered = p;
                    try {
                        n[p]()
                    } catch (t) {}
                    pt.event.triggered = undefined, u && (n[o] = u)
                }
                return e.result
            }
        },
        dispatch: function(t) {
            t = pt.event.fix(t);
            var e, i, n, s, a, o = [],
                r = st.call(arguments),
                l = (pt._data(this, "events") || {})[t.type] || [],
                c = pt.event.special[t.type] || {};
            if (r[0] = t, t.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, t)) {
                for (o = pt.event.handlers.call(this, t, l), e = 0;
                    (s = o[e++]) && !t.isPropagationStopped();)
                    for (t.currentTarget = s.elem, i = 0;
                        (a = s.handlers[i++]) && !t.isImmediatePropagationStopped();) t.rnamespace && !t.rnamespace.test(a.namespace) || (t.handleObj = a, t.data = a.data, (n = ((pt.event.special[a.origType] || {}).handle || a.handler).apply(s.elem, r)) !== undefined && !1 === (t.result = n) && (t.preventDefault(), t.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, t), t.result
            }
        },
        handlers: function(t, e) {
            var i, n, s, a, o = [],
                r = e.delegateCount,
                l = t.target;
            if (r && l.nodeType && ("click" !== t.type || isNaN(t.button) || t.button < 1))
                for (; l != this; l = l.parentNode || this)
                    if (1 === l.nodeType && (!0 !== l.disabled || "click" !== t.type)) {
                        for (n = [], i = 0; i < r; i++) a = e[i], s = a.selector + " ", n[s] === undefined && (n[s] = a.needsContext ? pt(s, this).index(l) > -1 : pt.find(s, this, null, [l]).length), n[s] && n.push(a);
                        n.length && o.push({
                            elem: l,
                            handlers: n
                        })
                    }
            return r < e.length && o.push({
                elem: this,
                handlers: e.slice(r)
            }), o
        },
        fix: function(t) {
            if (t[pt.expando]) return t;
            var e, i, n, s = t.type,
                a = t,
                o = this.fixHooks[s];
            for (o || (this.fixHooks[s] = o = Xt.test(s) ? this.mouseHooks : Kt.test(s) ? this.keyHooks : {}), n = o.props ? this.props.concat(o.props) : this.props, t = new pt.Event(a), e = n.length; e--;) i = n[e], t[i] = a[i];
            return t.target || (t.target = a.srcElement || nt), 3 === t.target.nodeType && (t.target = t.target.parentNode), t.metaKey = !!t.metaKey, o.filter ? o.filter(t, a) : t
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(t, e) {
                return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(t, e) {
                var i, n, s, a = e.button,
                    o = e.fromElement;
                return null == t.pageX && null != e.clientX && (n = t.target.ownerDocument || nt, s = n.documentElement, i = n.body, t.pageX = e.clientX + (s && s.scrollLeft || i && i.scrollLeft || 0) - (s && s.clientLeft || i && i.clientLeft || 0), t.pageY = e.clientY + (s && s.scrollTop || i && i.scrollTop || 0) - (s && s.clientTop || i && i.clientTop || 0)), !t.relatedTarget && o && (t.relatedTarget = o === t.target ? e.toElement : o), t.which || a === undefined || (t.which = 1 & a ? 1 : 2 & a ? 3 : 4 & a ? 2 : 0), t
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== y() && this.focus) try {
                        return this.focus(), !1
                    } catch (t) {}
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === y() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if (pt.nodeName(this, "input") && "checkbox" === this.type && this.click) return this.click(), !1
                },
                _default: function(t) {
                    return pt.nodeName(t.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(t) {
                    t.result !== undefined && t.originalEvent && (t.originalEvent.returnValue = t.result)
                }
            }
        },
        simulate: function(t, e, i) {
            var n = pt.extend(new pt.Event, i, {
                type: t,
                isSimulated: !0
            });
            pt.event.trigger(n, null, e), n.isDefaultPrevented() && i.preventDefault()
        }
    }, pt.removeEvent = nt.removeEventListener ? function(t, e, i) {
        t.removeEventListener && t.removeEventListener(e, i)
    } : function(t, e, i) {
        var n = "on" + e;
        t.detachEvent && ("undefined" == typeof t[n] && (t[n] = null), t.detachEvent(n, i))
    }, pt.Event = function(t, e) {
        if (!(this instanceof pt.Event)) return new pt.Event(t, e);
        t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || t.defaultPrevented === undefined && !1 === t.returnValue ? v : b) : this.type = t, e && pt.extend(this, e), this.timeStamp = t && t.timeStamp || pt.now(), this[pt.expando] = !0
    }, pt.Event.prototype = {
        constructor: pt.Event,
        isDefaultPrevented: b,
        isPropagationStopped: b,
        isImmediatePropagationStopped: b,
        preventDefault: function() {
            var t = this.originalEvent;
            this.isDefaultPrevented = v, t && (t.preventDefault ? t.preventDefault() : t.returnValue = !1)
        },
        stopPropagation: function() {
            var t = this.originalEvent;
            this.isPropagationStopped = v, t && !this.isSimulated && (t.stopPropagation && t.stopPropagation(), t.cancelBubble = !0)
        },
        stopImmediatePropagation: function() {
            var t = this.originalEvent;
            this.isImmediatePropagationStopped = v, t && t.stopImmediatePropagation && t.stopImmediatePropagation(), this.stopPropagation()
        }
    }, pt.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(t, e) {
        pt.event.special[t] = {
            delegateType: e,
            bindType: e,
            handle: function(t) {
                var i, n = this,
                    s = t.relatedTarget,
                    a = t.handleObj;
                return s && (s === n || pt.contains(n, s)) || (t.type = a.origType, i = a.handler.apply(this, arguments), t.type = e), i
            }
        }
    }), dt.submit || (pt.event.special.submit = {
        setup: function() {
            if (pt.nodeName(this, "form")) return !1;
            pt.event.add(this, "click._submit keypress._submit", function(t) {
                var e = t.target,
                    i = pt.nodeName(e, "input") || pt.nodeName(e, "button") ? pt.prop(e, "form") : undefined;
                i && !pt._data(i, "submit") && (pt.event.add(i, "submit._submit", function(t) {
                    t._submitBubble = !0
                }), pt._data(i, "submit", !0))
            })
        },
        postDispatch: function(t) {
            t._submitBubble && (delete t._submitBubble, this.parentNode && !t.isTrigger && pt.event.simulate("submit", this.parentNode, t))
        },
        teardown: function() {
            if (pt.nodeName(this, "form")) return !1;
            pt.event.remove(this, "._submit")
        }
    }), dt.change || (pt.event.special.change = {
        setup: function() {
            if (Vt.test(this.nodeName)) return "checkbox" !== this.type && "radio" !== this.type || (pt.event.add(this, "propertychange._change", function(t) {
                "checked" === t.originalEvent.propertyName && (this._justChanged = !0)
            }), pt.event.add(this, "click._change", function(t) {
                this._justChanged && !t.isTrigger && (this._justChanged = !1), pt.event.simulate("change", this, t)
            })), !1;
            pt.event.add(this, "beforeactivate._change", function(t) {
                var e = t.target;
                Vt.test(e.nodeName) && !pt._data(e, "change") && (pt.event.add(e, "change._change", function(t) {
                    !this.parentNode || t.isSimulated || t.isTrigger || pt.event.simulate("change", this.parentNode, t)
                }), pt._data(e, "change", !0))
            })
        },
        handle: function(t) {
            var e = t.target;
            if (this !== e || t.isSimulated || t.isTrigger || "radio" !== e.type && "checkbox" !== e.type) return t.handleObj.handler.apply(this, arguments)
        },
        teardown: function() {
            return pt.event.remove(this, "._change"), !Vt.test(this.nodeName)
        }
    }), dt.focusin || pt.each({
        focus: "focusin",
        blur: "focusout"
    }, function(t, e) {
        var i = function(t) {
            pt.event.simulate(e, t.target, pt.event.fix(t))
        };
        pt.event.special[e] = {
            setup: function() {
                var n = this.ownerDocument || this,
                    s = pt._data(n, e);
                s || n.addEventListener(t, i, !0), pt._data(n, e, (s || 0) + 1)
            },
            teardown: function() {
                var n = this.ownerDocument || this,
                    s = pt._data(n, e) - 1;
                s ? pt._data(n, e, s) : (n.removeEventListener(t, i, !0), pt._removeData(n, e))
            }
        }
    }), pt.fn.extend({
        on: function(t, e, i, n) {
            return w(this, t, e, i, n)
        },
        one: function(t, e, i, n) {
            return w(this, t, e, i, n, 1)
        },
        off: function(t, e, i) {
            var n, s;
            if (t && t.preventDefault && t.handleObj) return n = t.handleObj, pt(t.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), this;
            if ("object" == typeof t) {
                for (s in t) this.off(s, e, t[s]);
                return this
            }
            return !1 !== e && "function" != typeof e || (i = e, e = undefined), !1 === i && (i = b), this.each(function() {
                pt.event.remove(this, t, i, e)
            })
        },
        trigger: function(t, e) {
            return this.each(function() {
                pt.event.trigger(t, e, this)
            })
        },
        triggerHandler: function(t, e) {
            var i = this[0];
            if (i) return pt.event.trigger(t, e, i, !0)
        }
    });
    var Jt = / jQuery\d+="(?:null|\d+)"/g,
        Zt = new RegExp("<(?:" + Bt + ")[\\s/>]", "i"),
        te = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
        ee = /<script|<style|<link/i,
        ie = /checked\s*(?:[^=]|=\s*.checked.)/i,
        ne = /^true\/(.*)/,
        se = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        ae = p(nt),
        oe = ae.appendChild(nt.createElement("div"));
    pt.extend({
        htmlPrefilter: function(t) {
            return t.replace(te, "<$1></$2>")
        },
        clone: function(t, e, i) {
            var n, s, a, o, r, l = pt.contains(t.ownerDocument, t);
            if (dt.html5Clone || pt.isXMLDoc(t) || !Zt.test("<" + t.nodeName + ">") ? a = t.cloneNode(!0) : (oe.innerHTML = t.outerHTML, oe.removeChild(a = oe.firstChild)), !(dt.noCloneEvent && dt.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || pt.isXMLDoc(t)))
                for (n = f(a), r = f(t), o = 0; null != (s = r[o]); ++o) n[o] && D(s, n[o]);
            if (e)
                if (i)
                    for (r = r || f(t), n = n || f(a), o = 0; null != (s = r[o]); o++) C(s, n[o]);
                else C(t, a);
            return n = f(a, "script"), n.length > 0 && m(n, !l && f(t, "script")), n = r = s = null, a
        },
        cleanData: function(t, e) {
            for (var i, n, s, a, o = 0, r = pt.expando, l = pt.cache, c = dt.attributes, u = pt.event.special; null != (i = t[o]); o++)
                if ((e || Et(i)) && (s = i[r], a = s && l[s])) {
                    if (a.events)
                        for (n in a.events) u[n] ? pt.event.remove(i, n) : pt.removeEvent(i, n, a.handle);
                    l[s] && (delete l[s], c || "undefined" == typeof i.removeAttribute ? i[r] = undefined : i.removeAttribute(r), it.push(s))
                }
        }
    }), pt.fn.extend({
        domManip: S,
        detach: function(t) {
            return T(this, t, !0)
        },
        remove: function(t) {
            return T(this, t)
        },
        text: function(t) {
            return Ht(this, function(t) {
                return t === undefined ? pt.text(this) : this.empty().append((this[0] && this[0].ownerDocument || nt).createTextNode(t))
            }, null, t, arguments.length)
        },
        append: function() {
            return S(this, arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    x(this, t).appendChild(t)
                }
            })
        },
        prepend: function() {
            return S(this, arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = x(this, t);
                    e.insertBefore(t, e.firstChild)
                }
            })
        },
        before: function() {
            return S(this, arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this)
            })
        },
        after: function() {
            return S(this, arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
            })
        },
        empty: function() {
            for (var t, e = 0; null != (t = this[e]); e++) {
                for (1 === t.nodeType && pt.cleanData(f(t, !1)); t.firstChild;) t.removeChild(t.firstChild);
                t.options && pt.nodeName(t, "select") && (t.options.length = 0)
            }
            return this
        },
        clone: function(t, e) {
            return t = null != t && t, e = null == e ? t : e, this.map(function() {
                return pt.clone(this, t, e)
            })
        },
        html: function(t) {
            return Ht(this, function(t) {
                var e = this[0] || {},
                    i = 0,
                    n = this.length;
                if (t === undefined) return 1 === e.nodeType ? e.innerHTML.replace(Jt, "") : undefined;
                if ("string" == typeof t && !ee.test(t) && (dt.htmlSerialize || !Zt.test(t)) && (dt.leadingWhitespace || !Wt.test(t)) && !qt[(Rt.exec(t) || ["", ""])[1].toLowerCase()]) {
                    t = pt.htmlPrefilter(t);
                    try {
                        for (; i < n; i++) e = this[i] || {}, 1 === e.nodeType && (pt.cleanData(f(e, !1)), e.innerHTML = t);
                        e = 0
                    } catch (t) {}
                }
                e && this.empty().append(t)
            }, null, t, arguments.length)
        },
        replaceWith: function() {
            var t = [];
            return S(this, arguments, function(e) {
                var i = this.parentNode;
                pt.inArray(this, t) < 0 && (pt.cleanData(f(this)), i && i.replaceChild(e, this))
            }, t)
        }
    }), pt.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(t, e) {
        pt.fn[t] = function(t) {
            for (var i, n = 0, s = [], a = pt(t), o = a.length - 1; n <= o; n++) i = n === o ? this : this.clone(!0), pt(a[n])[e](i), ot.apply(s, i.get());
            return this.pushStack(s)
        }
    });
    var re, le = {
            HTML: "block",
            BODY: "block"
        },
        ce = /^margin/,
        ue = new RegExp("^(" + Nt + ")(?!px)[a-z%]+$", "i"),
        de = function(t, e, i, n) {
            var s, a, o = {};
            for (a in e) o[a] = t.style[a], t.style[a] = e[a];
            s = i.apply(t, n || []);
            for (a in e) t.style[a] = o[a];
            return s
        },
        he = nt.documentElement;
    ! function() {
        function e() {
            var e, u, d = nt.documentElement;
            d.appendChild(l), c.style.cssText = "-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", i = s = r = !1, n = o = !0, t.getComputedStyle && (u = t.getComputedStyle(c), i = "1%" !== (u || {}).top, r = "2px" === (u || {}).marginLeft, s = "4px" === (u || {
                width: "4px"
            }).width, c.style.marginRight = "50%", n = "4px" === (u || {
                marginRight: "4px"
            }).marginRight, e = c.appendChild(nt.createElement("div")), e.style.cssText = c.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", e.style.marginRight = e.style.width = "0", c.style.width = "1px", o = !parseFloat((t.getComputedStyle(e) || {}).marginRight), c.removeChild(e)), c.style.display = "none", a = 0 === c.getClientRects().length, a && (c.style.display = "", c.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", c.childNodes[0].style.borderCollapse = "separate", e = c.getElementsByTagName("td"), e[0].style.cssText = "margin:0;border:0;padding:0;display:none", (a = 0 === e[0].offsetHeight) && (e[0].style.display = "", e[1].style.display = "none", a = 0 === e[0].offsetHeight)), d.removeChild(l)
        }
        var i, n, s, a, o, r, l = nt.createElement("div"),
            c = nt.createElement("div");
        c.style && (c.style.cssText = "float:left;opacity:.5", dt.opacity = "0.5" === c.style.opacity, dt.cssFloat = !!c.style.cssFloat, c.style.backgroundClip = "content-box", c.cloneNode(!0).style.backgroundClip = "", dt.clearCloneStyle = "content-box" === c.style.backgroundClip, l = nt.createElement("div"), l.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", c.innerHTML = "", l.appendChild(c), dt.boxSizing = "" === c.style.boxSizing || "" === c.style.MozBoxSizing || "" === c.style.WebkitBoxSizing, pt.extend(dt, {
            reliableHiddenOffsets: function() {
                return null == i && e(), a
            },
            boxSizingReliable: function() {
                return null == i && e(), s
            },
            pixelMarginRight: function() {
                return null == i && e(), n
            },
            pixelPosition: function() {
                return null == i && e(), i
            },
            reliableMarginRight: function() {
                return null == i && e(), o
            },
            reliableMarginLeft: function() {
                return null == i && e(), r
            }
        }))
    }();
    var pe, fe, me = /^(top|right|bottom|left)$/;
    t.getComputedStyle ? (pe = function(e) {
        var i = e.ownerDocument.defaultView;
        return i && i.opener || (i = t), i.getComputedStyle(e)
    }, fe = function(t, e, i) {
        var n, s, a, o, r = t.style;
        return i = i || pe(t), o = i ? i.getPropertyValue(e) || i[e] : undefined, "" !== o && o !== undefined || pt.contains(t.ownerDocument, t) || (o = pt.style(t, e)), i && !dt.pixelMarginRight() && ue.test(o) && ce.test(e) && (n = r.width, s = r.minWidth, a = r.maxWidth, r.minWidth = r.maxWidth = r.width = o, o = i.width, r.width = n, r.minWidth = s, r.maxWidth = a), o === undefined ? o : o + ""
    }) : he.currentStyle && (pe = function(t) {
        return t.currentStyle
    }, fe = function(t, e, i) {
        var n, s, a, o, r = t.style;
        return i = i || pe(t), o = i ? i[e] : undefined, null == o && r && r[e] && (o = r[e]), ue.test(o) && !me.test(e) && (n = r.left, s = t.runtimeStyle, a = s && s.left, a && (s.left = t.currentStyle.left), r.left = "fontSize" === e ? "1em" : o, o = r.pixelLeft + "px", r.left = n, a && (s.left = a)), o === undefined ? o : o + "" || "auto"
    });
    var ge = /alpha\([^)]*\)/i,
        _e = /opacity\s*=\s*([^)]*)/i,
        ve = /^(none|table(?!-c[ea]).+)/,
        be = new RegExp("^(" + Nt + ")(.*)$", "i"),
        ye = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        we = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        xe = ["Webkit", "O", "Moz", "ms"],
        $e = nt.createElement("div").style;
    pt.extend({
        cssHooks: {
            opacity: {
                get: function(t, e) {
                    if (e) {
                        var i = fe(t, "opacity");
                        return "" === i ? "1" : i
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": dt.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(t, e, i, n) {
            if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                var s, a, o, r = pt.camelCase(e),
                    l = t.style;
                if (e = pt.cssProps[r] || (pt.cssProps[r] = M(r) || r), o = pt.cssHooks[e] || pt.cssHooks[r], i === undefined) return o && "get" in o && (s = o.get(t, !1, n)) !== undefined ? s : l[e];
                if (a = typeof i, "string" === a && (s = Ft.exec(i)) && s[1] && (i = h(t, e, s), a = "number"), null != i && i === i && ("number" === a && (i += s && s[3] || (pt.cssNumber[r] ? "" : "px")), dt.clearCloneStyle || "" !== i || 0 !== e.indexOf("background") || (l[e] = "inherit"), !(o && "set" in o && (i = o.set(t, i, n)) === undefined))) try {
                    l[e] = i
                } catch (t) {}
            }
        },
        css: function(t, e, i, n) {
            var s, a, o, r = pt.camelCase(e);
            return e = pt.cssProps[r] || (pt.cssProps[r] = M(r) || r), o = pt.cssHooks[e] || pt.cssHooks[r], o && "get" in o && (a = o.get(t, !0, i)), a === undefined && (a = fe(t, e, n)), "normal" === a && e in we && (a = we[e]), "" === i || i ? (s = parseFloat(a), !0 === i || isFinite(s) ? s || 0 : a) : a
        }
    }), pt.each(["height", "width"], function(t, e) {
        pt.cssHooks[e] = {
            get: function(t, i, n) {
                if (i) return ve.test(pt.css(t, "display")) && 0 === t.offsetWidth ? de(t, ye, function() {
                    return j(t, e, n)
                }) : j(t, e, n)
            },
            set: function(t, i, n) {
                var s = n && pe(t);
                return N(t, i, n ? F(t, e, n, dt.boxSizing && "border-box" === pt.css(t, "boxSizing", !1, s), s) : 0)
            }
        }
    }), dt.opacity || (pt.cssHooks.opacity = {
        get: function(t, e) {
            return _e.test((e && t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : e ? "1" : ""
        },
        set: function(t, e) {
            var i = t.style,
                n = t.currentStyle,
                s = pt.isNumeric(e) ? "alpha(opacity=" + 100 * e + ")" : "",
                a = n && n.filter || i.filter || "";
            i.zoom = 1, (e >= 1 || "" === e) && "" === pt.trim(a.replace(ge, "")) && i.removeAttribute && (i.removeAttribute("filter"), "" === e || n && !n.filter) || (i.filter = ge.test(a) ? a.replace(ge, s) : a + " " + s)
        }
    }), pt.cssHooks.marginRight = E(dt.reliableMarginRight, function(t, e) {
        if (e) return de(t, {
            display: "inline-block"
        }, fe, [t, "marginRight"])
    }), pt.cssHooks.marginLeft = E(dt.reliableMarginLeft, function(t, e) {
        if (e) return (parseFloat(fe(t, "marginLeft")) || (pt.contains(t.ownerDocument, t) ? t.getBoundingClientRect().left - de(t, {
            marginLeft: 0
        }, function() {
            return t.getBoundingClientRect().left
        }) : 0)) + "px"
    }), pt.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(t, e) {
        pt.cssHooks[t + e] = {
            expand: function(i) {
                for (var n = 0, s = {}, a = "string" == typeof i ? i.split(" ") : [i]; n < 4; n++) s[t + jt[n] + e] = a[n] || a[n - 2] || a[0];
                return s
            }
        }, ce.test(t) || (pt.cssHooks[t + e].set = N)
    }), pt.fn.extend({
        css: function(t, e) {
            return Ht(this, function(t, e, i) {
                var n, s, a = {},
                    o = 0;
                if (pt.isArray(e)) {
                    for (n = pe(t), s = e.length; o < s; o++) a[e[o]] = pt.css(t, e[o], !1, n);
                    return a
                }
                return i !== undefined ? pt.style(t, e, i) : pt.css(t, e)
            }, t, e, arguments.length > 1)
        },
        show: function() {
            return P(this, !0)
        },
        hide: function() {
            return P(this)
        },
        toggle: function(t) {
            return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function() {
                Lt(this) ? pt(this).show() : pt(this).hide()
            })
        }
    }), pt.Tween = L, L.prototype = {
        constructor: L,
        init: function(t, e, i, n, s, a) {
            this.elem = t, this.prop = i, this.easing = s || pt.easing._default, this.options = e, this.start = this.now = this.cur(), this.end = n, this.unit = a || (pt.cssNumber[i] ? "" : "px")
        },
        cur: function() {
            var t = L.propHooks[this.prop];
            return t && t.get ? t.get(this) : L.propHooks._default.get(this)
        },
        run: function(t) {
            var e, i = L.propHooks[this.prop];
            return this.options.duration ? this.pos = e = pt.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : L.propHooks._default.set(this), this
        }
    }, L.prototype.init.prototype = L.prototype, L.propHooks = {
        _default: {
            get: function(t) {
                var e;
                return 1 !== t.elem.nodeType || null != t.elem[t.prop] && null == t.elem.style[t.prop] ? t.elem[t.prop] : (e = pt.css(t.elem, t.prop, ""), e && "auto" !== e ? e : 0)
            },
            set: function(t) {
                pt.fx.step[t.prop] ? pt.fx.step[t.prop](t) : 1 !== t.elem.nodeType || null == t.elem.style[pt.cssProps[t.prop]] && !pt.cssHooks[t.prop] ? t.elem[t.prop] = t.now : pt.style(t.elem, t.prop, t.now + t.unit)
            }
        }
    }, L.propHooks.scrollTop = L.propHooks.scrollLeft = {
        set: function(t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
        }
    }, pt.easing = {
        linear: function(t) {
            return t
        },
        swing: function(t) {
            return .5 - Math.cos(t * Math.PI) / 2
        },
        _default: "swing"
    }, pt.fx = L.prototype.init, pt.fx.step = {};
    var ke, Ce, De = /^(?:toggle|show|hide)$/,
        Se = /queueHooks$/;
    pt.Animation = pt.extend(B, {
            tweeners: {
                "*": [function(t, e) {
                    var i = this.createTween(t, e);
                    return h(i.elem, t, Ft.exec(e), i), i
                }]
            },
            tweener: function(t, e) {
                pt.isFunction(t) ? (e = t, t = ["*"]) : t = t.match(Tt);
                for (var i, n = 0, s = t.length; n < s; n++) i = t[n], B.tweeners[i] = B.tweeners[i] || [], B.tweeners[i].unshift(e)
            },
            prefilters: [z],
            prefilter: function(t, e) {
                e ? B.prefilters.unshift(t) : B.prefilters.push(t)
            }
        }), pt.speed = function(t, e, i) {
            var n = t && "object" == typeof t ? pt.extend({}, t) : {
                complete: i || !i && e || pt.isFunction(t) && t,
                duration: t,
                easing: i && e || e && !pt.isFunction(e) && e
            };
            return n.duration = pt.fx.off ? 0 : "number" == typeof n.duration ? n.duration : n.duration in pt.fx.speeds ? pt.fx.speeds[n.duration] : pt.fx.speeds._default, null != n.queue && !0 !== n.queue || (n.queue = "fx"), n.old = n.complete, n.complete = function() {
                pt.isFunction(n.old) && n.old.call(this), n.queue && pt.dequeue(this, n.queue)
            }, n
        }, pt.fn.extend({
            fadeTo: function(t, e, i, n) {
                return this.filter(Lt).css("opacity", 0).show().end().animate({
                    opacity: e
                }, t, i, n)
            },
            animate: function(t, e, i, n) {
                var s = pt.isEmptyObject(t),
                    a = pt.speed(e, i, n),
                    o = function() {
                        var e = B(this, pt.extend({}, t), a);
                        (s || pt._data(this, "finish")) && e.stop(!0)
                    };
                return o.finish = o, s || !1 === a.queue ? this.each(o) : this.queue(a.queue, o)
            },
            stop: function(t, e, i) {
                var n = function(t) {
                    var e = t.stop;
                    delete t.stop, e(i)
                };
                return "string" != typeof t && (i = e, e = t, t = undefined), e && !1 !== t && this.queue(t || "fx", []), this.each(function() {
                    var e = !0,
                        s = null != t && t + "queueHooks",
                        a = pt.timers,
                        o = pt._data(this);
                    if (s) o[s] && o[s].stop && n(o[s]);
                    else
                        for (s in o) o[s] && o[s].stop && Se.test(s) && n(o[s]);
                    for (s = a.length; s--;) a[s].elem !== this || null != t && a[s].queue !== t || (a[s].anim.stop(i), e = !1, a.splice(s, 1));
                    !e && i || pt.dequeue(this, t)
                })
            },
            finish: function(t) {
                return !1 !== t && (t = t || "fx"), this.each(function() {
                    var e, i = pt._data(this),
                        n = i[t + "queue"],
                        s = i[t + "queueHooks"],
                        a = pt.timers,
                        o = n ? n.length : 0;
                    for (i.finish = !0, pt.queue(this, t, []), s && s.stop && s.stop.call(this, !0), e = a.length; e--;) a[e].elem === this && a[e].queue === t && (a[e].anim.stop(!0), a.splice(e, 1));
                    for (e = 0; e < o; e++) n[e] && n[e].finish && n[e].finish.call(this);
                    delete i.finish
                })
            }
        }), pt.each(["toggle", "show", "hide"], function(t, e) {
            var i = pt.fn[e];
            pt.fn[e] = function(t, n, s) {
                return null == t || "boolean" == typeof t ? i.apply(this, arguments) : this.animate(O(e, !0), t, n, s)
            }
        }), pt.each({
            slideDown: O("show"),
            slideUp: O("hide"),
            slideToggle: O("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(t, e) {
            pt.fn[t] = function(t, i, n) {
                return this.animate(e, t, i, n)
            }
        }), pt.timers = [], pt.fx.tick = function() {
            var t, e = pt.timers,
                i = 0;
            for (ke = pt.now(); i < e.length; i++)(t = e[i])() || e[i] !== t || e.splice(i--, 1);
            e.length || pt.fx.stop(), ke = undefined
        }, pt.fx.timer = function(t) {
            pt.timers.push(t), t() ? pt.fx.start() : pt.timers.pop()
        }, pt.fx.interval = 13, pt.fx.start = function() {
            Ce || (Ce = t.setInterval(pt.fx.tick, pt.fx.interval))
        }, pt.fx.stop = function() {
            t.clearInterval(Ce), Ce = null
        }, pt.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, pt.fn.delay = function(e, i) {
            return e = pt.fx ? pt.fx.speeds[e] || e : e, i = i || "fx", this.queue(i, function(i, n) {
                var s = t.setTimeout(i, e);
                n.stop = function() {
                    t.clearTimeout(s)
                }
            })
        },
        function() {
            var t, e = nt.createElement("input"),
                i = nt.createElement("div"),
                n = nt.createElement("select"),
                s = n.appendChild(nt.createElement("option"));
            i = nt.createElement("div"), i.setAttribute("className", "t"), i.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", t = i.getElementsByTagName("a")[0], e.setAttribute("type", "checkbox"), i.appendChild(e), t = i.getElementsByTagName("a")[0], t.style.cssText = "top:1px", dt.getSetAttribute = "t" !== i.className, dt.style = /top/.test(t.getAttribute("style")), dt.hrefNormalized = "/a" === t.getAttribute("href"), dt.checkOn = !!e.value, dt.optSelected = s.selected, dt.enctype = !!nt.createElement("form").enctype, n.disabled = !0, dt.optDisabled = !s.disabled, e = nt.createElement("input"), e.setAttribute("value", ""), dt.input = "" === e.getAttribute("value"), e.value = "t", e.setAttribute("type", "radio"), dt.radioValue = "t" === e.value
        }();
    var Te = /\r/g,
        Ie = /[\x20\t\r\n\f]+/g;
    pt.fn.extend({
        val: function(t) {
            var e, i, n, s = this[0]; {
                if (arguments.length) return n = pt.isFunction(t), this.each(function(i) {
                    var s;
                    1 === this.nodeType && (s = n ? t.call(this, i, pt(this).val()) : t, null == s ? s = "" : "number" == typeof s ? s += "" : pt.isArray(s) && (s = pt.map(s, function(t) {
                        return null == t ? "" : t + ""
                    })), (e = pt.valHooks[this.type] || pt.valHooks[this.nodeName.toLowerCase()]) && "set" in e && e.set(this, s, "value") !== undefined || (this.value = s))
                });
                if (s) return (e = pt.valHooks[s.type] || pt.valHooks[s.nodeName.toLowerCase()]) && "get" in e && (i = e.get(s, "value")) !== undefined ? i : (i = s.value, "string" == typeof i ? i.replace(Te, "") : null == i ? "" : i)
            }
        }
    }), pt.extend({
        valHooks: {
            option: {
                get: function(t) {
                    var e = pt.find.attr(t, "value");
                    return null != e ? e : pt.trim(pt.text(t)).replace(Ie, " ")
                }
            },
            select: {
                get: function(t) {
                    for (var e, i, n = t.options, s = t.selectedIndex, a = "select-one" === t.type || s < 0, o = a ? null : [], r = a ? s + 1 : n.length, l = s < 0 ? r : a ? s : 0; l < r; l++)
                        if (i = n[l], (i.selected || l === s) && (dt.optDisabled ? !i.disabled : null === i.getAttribute("disabled")) && (!i.parentNode.disabled || !pt.nodeName(i.parentNode, "optgroup"))) {
                            if (e = pt(i).val(), a) return e;
                            o.push(e)
                        }
                    return o
                },
                set: function(t, e) {
                    for (var i, n, s = t.options, a = pt.makeArray(e), o = s.length; o--;)
                        if (n = s[o], pt.inArray(pt.valHooks.option.get(n), a) > -1) try {
                            n.selected = i = !0
                        } catch (t) {
                            n.scrollHeight
                        } else n.selected = !1;
                    return i || (t.selectedIndex = -1), s
                }
            }
        }
    }), pt.each(["radio", "checkbox"], function() {
        pt.valHooks[this] = {
            set: function(t, e) {
                if (pt.isArray(e)) return t.checked = pt.inArray(pt(t).val(), e) > -1
            }
        }, dt.checkOn || (pt.valHooks[this].get = function(t) {
            return null === t.getAttribute("value") ? "on" : t.value
        })
    });
    var Ae, Ee, Me = pt.expr.attrHandle,
        Pe = /^(?:checked|selected)$/i,
        Ne = dt.getSetAttribute,
        Fe = dt.input;
    pt.fn.extend({
        attr: function(t, e) {
            return Ht(this, pt.attr, t, e, arguments.length > 1)
        },
        removeAttr: function(t) {
            return this.each(function() {
                pt.removeAttr(this, t)
            })
        }
    }), pt.extend({
        attr: function(t, e, i) {
            var n, s, a = t.nodeType;
            if (3 !== a && 8 !== a && 2 !== a) return "undefined" == typeof t.getAttribute ? pt.prop(t, e, i) : (1 === a && pt.isXMLDoc(t) || (e = e.toLowerCase(), s = pt.attrHooks[e] || (pt.expr.match.bool.test(e) ? Ee : Ae)), i !== undefined ? null === i ? void pt.removeAttr(t, e) : s && "set" in s && (n = s.set(t, i, e)) !== undefined ? n : (t.setAttribute(e, i + ""), i) : s && "get" in s && null !== (n = s.get(t, e)) ? n : (n = pt.find.attr(t, e), null == n ? undefined : n))
        },
        attrHooks: {
            type: {
                set: function(t, e) {
                    if (!dt.radioValue && "radio" === e && pt.nodeName(t, "input")) {
                        var i = t.value;
                        return t.setAttribute("type", e), i && (t.value = i), e
                    }
                }
            }
        },
        removeAttr: function(t, e) {
            var i, n, s = 0,
                a = e && e.match(Tt);
            if (a && 1 === t.nodeType)
                for (; i = a[s++];) n = pt.propFix[i] || i, pt.expr.match.bool.test(i) ? Fe && Ne || !Pe.test(i) ? t[n] = !1 : t[pt.camelCase("default-" + i)] = t[n] = !1 : pt.attr(t, i, ""), t.removeAttribute(Ne ? i : n)
        }
    }), Ee = {
        set: function(t, e, i) {
            return !1 === e ? pt.removeAttr(t, i) : Fe && Ne || !Pe.test(i) ? t.setAttribute(!Ne && pt.propFix[i] || i, i) : t[pt.camelCase("default-" + i)] = t[i] = !0, i
        }
    }, pt.each(pt.expr.match.bool.source.match(/\w+/g), function(t, e) {
        var i = Me[e] || pt.find.attr;
        Fe && Ne || !Pe.test(e) ? Me[e] = function(t, e, n) {
            var s, a;
            return n || (a = Me[e], Me[e] = s, s = null != i(t, e, n) ? e.toLowerCase() : null, Me[e] = a), s
        } : Me[e] = function(t, e, i) {
            if (!i) return t[pt.camelCase("default-" + e)] ? e.toLowerCase() : null
        }
    }), Fe && Ne || (pt.attrHooks.value = {
        set: function(t, e, i) {
            if (!pt.nodeName(t, "input")) return Ae && Ae.set(t, e, i);
            t.defaultValue = e
        }
    }), Ne || (Ae = {
        set: function(t, e, i) {
            var n = t.getAttributeNode(i);
            if (n || t.setAttributeNode(n = t.ownerDocument.createAttribute(i)), n.value = e += "", "value" === i || e === t.getAttribute(i)) return e
        }
    }, Me.id = Me.name = Me.coords = function(t, e, i) {
        var n;
        if (!i) return (n = t.getAttributeNode(e)) && "" !== n.value ? n.value : null
    }, pt.valHooks.button = {
        get: function(t, e) {
            var i = t.getAttributeNode(e);
            if (i && i.specified) return i.value
        },
        set: Ae.set
    }, pt.attrHooks.contenteditable = {
        set: function(t, e, i) {
            Ae.set(t, "" !== e && e, i)
        }
    }, pt.each(["width", "height"], function(t, e) {
        pt.attrHooks[e] = {
            set: function(t, i) {
                if ("" === i) return t.setAttribute(e, "auto"), i
            }
        }
    })), dt.style || (pt.attrHooks.style = {
        get: function(t) {
            return t.style.cssText || undefined
        },
        set: function(t, e) {
            return t.style.cssText = e + ""
        }
    });
    var je = /^(?:input|select|textarea|button|object)$/i,
        Le = /^(?:a|area)$/i;
    pt.fn.extend({
        prop: function(t, e) {
            return Ht(this, pt.prop, t, e, arguments.length > 1)
        },
        removeProp: function(t) {
            return t = pt.propFix[t] || t, this.each(function() {
                try {
                    this[t] = undefined, delete this[t]
                } catch (t) {}
            })
        }
    }), pt.extend({
        prop: function(t, e, i) {
            var n, s, a = t.nodeType;
            if (3 !== a && 8 !== a && 2 !== a) return 1 === a && pt.isXMLDoc(t) || (e = pt.propFix[e] || e, s = pt.propHooks[e]), i !== undefined ? s && "set" in s && (n = s.set(t, i, e)) !== undefined ? n : t[e] = i : s && "get" in s && null !== (n = s.get(t, e)) ? n : t[e]
        },
        propHooks: {
            tabIndex: {
                get: function(t) {
                    var e = pt.find.attr(t, "tabindex");
                    return e ? parseInt(e, 10) : je.test(t.nodeName) || Le.test(t.nodeName) && t.href ? 0 : -1
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        }
    }), dt.hrefNormalized || pt.each(["href", "src"], function(t, e) {
        pt.propHooks[e] = {
            get: function(t) {
                return t.getAttribute(e, 4)
            }
        }
    }), dt.optSelected || (pt.propHooks.selected = {
        get: function(t) {
            var e = t.parentNode;
            return e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex), null
        },
        set: function(t) {
            var e = t.parentNode;
            e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex)
        }
    }), pt.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        pt.propFix[this.toLowerCase()] = this
    }), dt.enctype || (pt.propFix.enctype = "encoding");
    var He = /[\t\r\n\f]/g;
    pt.fn.extend({
        addClass: function(t) {
            var e, i, n, s, a, o, r, l = 0;
            if (pt.isFunction(t)) return this.each(function(e) {
                pt(this).addClass(t.call(this, e, q(this)))
            });
            if ("string" == typeof t && t)
                for (e = t.match(Tt) || []; i = this[l++];)
                    if (s = q(i), n = 1 === i.nodeType && (" " + s + " ").replace(He, " ")) {
                        for (o = 0; a = e[o++];) n.indexOf(" " + a + " ") < 0 && (n += a + " ");
                        r = pt.trim(n), s !== r && pt.attr(i, "class", r)
                    }
            return this
        },
        removeClass: function(t) {
            var e, i, n, s, a, o, r, l = 0;
            if (pt.isFunction(t)) return this.each(function(e) {
                pt(this).removeClass(t.call(this, e, q(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof t && t)
                for (e = t.match(Tt) || []; i = this[l++];)
                    if (s = q(i), n = 1 === i.nodeType && (" " + s + " ").replace(He, " ")) {
                        for (o = 0; a = e[o++];)
                            for (; n.indexOf(" " + a + " ") > -1;) n = n.replace(" " + a + " ", " ");
                        r = pt.trim(n), s !== r && pt.attr(i, "class", r)
                    }
            return this
        },
        toggleClass: function(t, e) {
            var i = typeof t;
            return "boolean" == typeof e && "string" === i ? e ? this.addClass(t) : this.removeClass(t) : pt.isFunction(t) ? this.each(function(i) {
                pt(this).toggleClass(t.call(this, i, q(this), e), e)
            }) : this.each(function() {
                var e, n, s, a;
                if ("string" === i)
                    for (n = 0, s = pt(this), a = t.match(Tt) || []; e = a[n++];) s.hasClass(e) ? s.removeClass(e) : s.addClass(e);
                else t !== undefined && "boolean" !== i || (e = q(this), e && pt._data(this, "__className__", e), pt.attr(this, "class", e || !1 === t ? "" : pt._data(this, "__className__") || ""))
            })
        },
        hasClass: function(t) {
            var e, i, n = 0;
            for (e = " " + t + " "; i = this[n++];)
                if (1 === i.nodeType && (" " + q(i) + " ").replace(He, " ").indexOf(e) > -1) return !0;
            return !1
        }
    }), pt.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(t, e) {
        pt.fn[e] = function(t, i) {
            return arguments.length > 0 ? this.on(e, null, t, i) : this.trigger(e)
        }
    }), pt.fn.extend({
        hover: function(t, e) {
            return this.mouseenter(t).mouseleave(e || t)
        }
    });
    var Oe = t.location,
        Re = pt.now(),
        ze = /\?/,
        We = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    pt.parseJSON = function(e) {
        if (t.JSON && t.JSON.parse) return t.JSON.parse(e + "");
        var i, n = null,
            s = pt.trim(e + "");
        return s && !pt.trim(s.replace(We, function(t, e, s, a) {
            return i && e && (n = 0), 0 === n ? t : (i = s || e, n += !a - !s, "")
        })) ? Function("return " + s)() : pt.error("Invalid JSON: " + e)
    }, pt.parseXML = function(e) {
        var i, n;
        if (!e || "string" != typeof e) return null;
        try {
            t.DOMParser ? (n = new t.DOMParser, i = n.parseFromString(e, "text/xml")) : (i = new t.ActiveXObject("Microsoft.XMLDOM"), i.async = "false", i.loadXML(e))
        } catch (t) {
            i = undefined
        }
        return i && i.documentElement && !i.getElementsByTagName("parsererror").length || pt.error("Invalid XML: " + e), i
    };
    var Be = /#.*$/,
        qe = /([?&])_=[^&]*/,
        Ue = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Ye = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Ve = /^(?:GET|HEAD)$/,
        Ke = /^\/\//,
        Xe = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
        Qe = {},
        Ge = {},
        Je = "*/".concat("*"),
        Ze = Oe.href,
        ti = Xe.exec(Ze.toLowerCase()) || [];
    pt.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Ze,
            type: "GET",
            isLocal: Ye.test(ti[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Je,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": pt.parseJSON,
                "text xml": pt.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(t, e) {
            return e ? V(V(t, pt.ajaxSettings), e) : V(pt.ajaxSettings, t)
        },
        ajaxPrefilter: U(Qe),
        ajaxTransport: U(Ge),
        ajax: function(e, i) {
            function n(e, i, n, s) {
                var a, d, v, b, w, $ = i;
                2 !== y && (y = 2, l && t.clearTimeout(l), u = undefined, r = s || "", x.readyState = e > 0 ? 4 : 0, a = e >= 200 && e < 300 || 304 === e, n && (b = K(h, x, n)), b = X(h, b, x, a), a ? (h.ifModified && (w = x.getResponseHeader("Last-Modified"), w && (pt.lastModified[o] = w), (w = x.getResponseHeader("etag")) && (pt.etag[o] = w)), 204 === e || "HEAD" === h.type ? $ = "nocontent" : 304 === e ? $ = "notmodified" : ($ = b.state, d = b.data, v = b.error, a = !v)) : (v = $, !e && $ || ($ = "error", e < 0 && (e = 0))), x.status = e, x.statusText = (i || $) + "", a ? m.resolveWith(p, [d, $, x]) : m.rejectWith(p, [x, $, v]), x.statusCode(_), _ = undefined, c && f.trigger(a ? "ajaxSuccess" : "ajaxError", [x, h, a ? d : v]), g.fireWith(p, [x, $]), c && (f.trigger("ajaxComplete", [x, h]), --pt.active || pt.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (i = e, e = undefined), i = i || {};
            var s, a, o, r, l, c, u, d, h = pt.ajaxSetup({}, i),
                p = h.context || h,
                f = h.context && (p.nodeType || p.jquery) ? pt(p) : pt.event,
                m = pt.Deferred(),
                g = pt.Callbacks("once memory"),
                _ = h.statusCode || {},
                v = {},
                b = {},
                y = 0,
                w = "canceled",
                x = {
                    readyState: 0,
                    getResponseHeader: function(t) {
                        var e;
                        if (2 === y) {
                            if (!d)
                                for (d = {}; e = Ue.exec(r);) d[e[1].toLowerCase()] = e[2];
                            e = d[t.toLowerCase()]
                        }
                        return null == e ? null : e
                    },
                    getAllResponseHeaders: function() {
                        return 2 === y ? r : null
                    },
                    setRequestHeader: function(t, e) {
                        var i = t.toLowerCase();
                        return y || (t = b[i] = b[i] || t, v[t] = e), this
                    },
                    overrideMimeType: function(t) {
                        return y || (h.mimeType = t), this
                    },
                    statusCode: function(t) {
                        var e;
                        if (t)
                            if (y < 2)
                                for (e in t) _[e] = [_[e], t[e]];
                            else x.always(t[x.status]);
                        return this
                    },
                    abort: function(t) {
                        var e = t || w;
                        return u && u.abort(e), n(0, e), this
                    }
                };
            if (m.promise(x).complete = g.add, x.success = x.done, x.error = x.fail, h.url = ((e || h.url || Ze) + "").replace(Be, "").replace(Ke, ti[1] + "//"), h.type = i.method || i.type || h.method || h.type, h.dataTypes = pt.trim(h.dataType || "*").toLowerCase().match(Tt) || [""], null == h.crossDomain && (s = Xe.exec(h.url.toLowerCase()), h.crossDomain = !(!s || s[1] === ti[1] && s[2] === ti[2] && (s[3] || ("http:" === s[1] ? "80" : "443")) === (ti[3] || ("http:" === ti[1] ? "80" : "443")))), h.data && h.processData && "string" != typeof h.data && (h.data = pt.param(h.data, h.traditional)), Y(Qe, h, i, x), 2 === y) return x;
            c = pt.event && h.global, c && 0 == pt.active++ && pt.event.trigger("ajaxStart"), h.type = h.type.toUpperCase(), h.hasContent = !Ve.test(h.type), o = h.url, h.hasContent || (h.data && (o = h.url += (ze.test(o) ? "&" : "?") + h.data, delete h.data), !1 === h.cache && (h.url = qe.test(o) ? o.replace(qe, "$1_=" + Re++) : o + (ze.test(o) ? "&" : "?") + "_=" + Re++)), h.ifModified && (pt.lastModified[o] && x.setRequestHeader("If-Modified-Since", pt.lastModified[o]), pt.etag[o] && x.setRequestHeader("If-None-Match", pt.etag[o])), (h.data && h.hasContent && !1 !== h.contentType || i.contentType) && x.setRequestHeader("Content-Type", h.contentType), x.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + Je + "; q=0.01" : "") : h.accepts["*"]);
            for (a in h.headers) x.setRequestHeader(a, h.headers[a]);
            if (h.beforeSend && (!1 === h.beforeSend.call(p, x, h) || 2 === y)) return x.abort();
            w = "abort";
            for (a in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) x[a](h[a]);
            if (u = Y(Ge, h, i, x)) {
                if (x.readyState = 1, c && f.trigger("ajaxSend", [x, h]), 2 === y) return x;
                h.async && h.timeout > 0 && (l = t.setTimeout(function() {
                    x.abort("timeout")
                }, h.timeout));
                try {
                    y = 1, u.send(v, n)
                } catch (t) {
                    if (!(y < 2)) throw t;
                    n(-1, t)
                }
            } else n(-1, "No Transport");
            return x
        },
        getJSON: function(t, e, i) {
            return pt.get(t, e, i, "json")
        },
        getScript: function(t, e) {
            return pt.get(t, undefined, e, "script")
        }
    }), pt.each(["get", "post"], function(t, e) {
        pt[e] = function(t, i, n, s) {
            return pt.isFunction(i) && (s = s || n, n = i, i = undefined), pt.ajax(pt.extend({
                url: t,
                type: e,
                dataType: s,
                data: i,
                success: n
            }, pt.isPlainObject(t) && t))
        }
    }), pt._evalUrl = function(t) {
        return pt.ajax({
            url: t,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            "throws": !0
        })
    }, pt.fn.extend({
        wrapAll: function(t) {
            if (pt.isFunction(t)) return this.each(function(e) {
                pt(this).wrapAll(t.call(this, e))
            });
            if (this[0]) {
                var e = pt(t, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && e.insertBefore(this[0]), e.map(function() {
                    for (var t = this; t.firstChild && 1 === t.firstChild.nodeType;) t = t.firstChild;
                    return t
                }).append(this)
            }
            return this
        },
        wrapInner: function(t) {
            return pt.isFunction(t) ? this.each(function(e) {
                pt(this).wrapInner(t.call(this, e))
            }) : this.each(function() {
                var e = pt(this),
                    i = e.contents();
                i.length ? i.wrapAll(t) : e.append(t)
            })
        },
        wrap: function(t) {
            var e = pt.isFunction(t);
            return this.each(function(i) {
                pt(this).wrapAll(e ? t.call(this, i) : t)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                pt.nodeName(this, "body") || pt(this).replaceWith(this.childNodes)
            }).end()
        }
    }), pt.expr.filters.hidden = function(t) {
        return dt.reliableHiddenOffsets() ? t.offsetWidth <= 0 && t.offsetHeight <= 0 && !t.getClientRects().length : G(t)
    }, pt.expr.filters.visible = function(t) {
        return !pt.expr.filters.hidden(t)
    };
    var ei = /%20/g,
        ii = /\[\]$/,
        ni = /\r?\n/g,
        si = /^(?:submit|button|image|reset|file)$/i,
        ai = /^(?:input|select|textarea|keygen)/i;
    pt.param = function(t, e) {
        var i, n = [],
            s = function(t, e) {
                e = pt.isFunction(e) ? e() : null == e ? "" : e, n[n.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
            };
        if (e === undefined && (e = pt.ajaxSettings && pt.ajaxSettings.traditional), pt.isArray(t) || t.jquery && !pt.isPlainObject(t)) pt.each(t, function() {
            s(this.name, this.value)
        });
        else
            for (i in t) J(i, t[i], e, s);
        return n.join("&").replace(ei, "+")
    }, pt.fn.extend({
        serialize: function() {
            return pt.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var t = pt.prop(this, "elements");
                return t ? pt.makeArray(t) : this
            }).filter(function() {
                var t = this.type;
                return this.name && !pt(this).is(":disabled") && ai.test(this.nodeName) && !si.test(t) && (this.checked || !Ot.test(t))
            }).map(function(t, e) {
                var i = pt(this).val();
                return null == i ? null : pt.isArray(i) ? pt.map(i, function(t) {
                    return {
                        name: e.name,
                        value: t.replace(ni, "\r\n")
                    }
                }) : {
                    name: e.name,
                    value: i.replace(ni, "\r\n")
                }
            }).get()
        }
    }), pt.ajaxSettings.xhr = t.ActiveXObject !== undefined ? function() {
        return this.isLocal ? tt() : nt.documentMode > 8 ? Z() : /^(get|post|head|put|delete|options)$/i.test(this.type) && Z() || tt()
    } : Z;
    var oi = 0,
        ri = {},
        li = pt.ajaxSettings.xhr();
    t.attachEvent && t.attachEvent("onunload", function() {
        for (var t in ri) ri[t](undefined, !0)
    }), dt.cors = !!li && "withCredentials" in li, li = dt.ajax = !!li, li && pt.ajaxTransport(function(e) {
        if (!e.crossDomain || dt.cors) {
            var i;
            return {
                send: function(n, s) {
                    var a, o = e.xhr(),
                        r = ++oi;
                    if (o.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                        for (a in e.xhrFields) o[a] = e.xhrFields[a];
                    e.mimeType && o.overrideMimeType && o.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                    for (a in n) n[a] !== undefined && o.setRequestHeader(a, n[a] + "");
                    o.send(e.hasContent && e.data || null), i = function(t, n) {
                        var a, l, c;
                        if (i && (n || 4 === o.readyState))
                            if (delete ri[r], i = undefined, o.onreadystatechange = pt.noop, n) 4 !== o.readyState && o.abort();
                            else {
                                c = {}, a = o.status, "string" == typeof o.responseText && (c.text = o.responseText);
                                try {
                                    l = o.statusText
                                } catch (t) {
                                    l = ""
                                }
                                a || !e.isLocal || e.crossDomain ? 1223 === a && (a = 204) : a = c.text ? 200 : 404
                            }
                        c && s(a, l, c, o.getAllResponseHeaders())
                    }, e.async ? 4 === o.readyState ? t.setTimeout(i) : o.onreadystatechange = ri[r] = i : i()
                },
                abort: function() {
                    i && i(undefined, !0)
                }
            }
        }
    }), pt.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(t) {
                return pt.globalEval(t), t
            }
        }
    }), pt.ajaxPrefilter("script", function(t) {
        t.cache === undefined && (t.cache = !1), t.crossDomain && (t.type = "GET", t.global = !1)
    }), pt.ajaxTransport("script", function(t) {
        if (t.crossDomain) {
            var e, i = nt.head || pt("head")[0] || nt.documentElement;
            return {
                send: function(n, s) {
                    e = nt.createElement("script"), e.async = !0, t.scriptCharset && (e.charset = t.scriptCharset), e.src = t.url, e.onload = e.onreadystatechange = function(t, i) {
                        (i || !e.readyState || /loaded|complete/.test(e.readyState)) && (e.onload = e.onreadystatechange = null, e.parentNode && e.parentNode.removeChild(e), e = null, i || s(200, "success"))
                    }, i.insertBefore(e, i.firstChild)
                },
                abort: function() {
                    e && e.onload(undefined, !0)
                }
            }
        }
    });
    var ci = [],
        ui = /(=)\?(?=&|$)|\?\?/;
    pt.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var t = ci.pop() || pt.expando + "_" + Re++;
            return this[t] = !0, t
        }
    }), pt.ajaxPrefilter("json jsonp", function(e, i, n) {
        var s, a, o, r = !1 !== e.jsonp && (ui.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && ui.test(e.data) && "data");
        if (r || "jsonp" === e.dataTypes[0]) return s = e.jsonpCallback = pt.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, r ? e[r] = e[r].replace(ui, "$1" + s) : !1 !== e.jsonp && (e.url += (ze.test(e.url) ? "&" : "?") + e.jsonp + "=" + s), e.converters["script json"] = function() {
            return o || pt.error(s + " was not called"), o[0]
        }, e.dataTypes[0] = "json", a = t[s], t[s] = function() {
            o = arguments
        }, n.always(function() {
            a === undefined ? pt(t).removeProp(s) : t[s] = a, e[s] && (e.jsonpCallback = i.jsonpCallback, ci.push(s)), o && pt.isFunction(a) && a(o[0]), o = a = undefined
        }), "script"
    }), pt.parseHTML = function(t, e, i) {
        if (!t || "string" != typeof t) return null;
        "boolean" == typeof e && (i = e, e = !1), e = e || nt;
        var n = xt.exec(t),
            s = !i && [];
        return n ? [e.createElement(n[1])] : (n = _([t], e, s), s && s.length && pt(s).remove(), pt.merge([], n.childNodes))
    };
    var di = pt.fn.load;
    pt.fn.load = function(t, e, i) {
            if ("string" != typeof t && di) return di.apply(this, arguments);
            var n, s, a, o = this,
                r = t.indexOf(" ");
            return r > -1 && (n = pt.trim(t.slice(r, t.length)), t = t.slice(0, r)), pt.isFunction(e) ? (i = e, e = undefined) : e && "object" == typeof e && (s = "POST"), o.length > 0 && pt.ajax({
                url: t,
                type: s || "GET",
                dataType: "html",
                data: e
            }).done(function(t) {
                a = arguments, o.html(n ? pt("<div>").append(pt.parseHTML(t)).find(n) : t)
            }).always(i && function(t, e) {
                o.each(function() {
                    i.apply(this, a || [t.responseText, e, t])
                })
            }), this
        }, pt.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(t, e) {
            pt.fn[e] = function(t) {
                return this.on(e, t)
            }
        }), pt.expr.filters.animated = function(t) {
            return pt.grep(pt.timers, function(e) {
                return t === e.elem
            }).length
        }, pt.offset = {
            setOffset: function(t, e, i) {
                var n, s, a, o, r, l, c, u = pt.css(t, "position"),
                    d = pt(t),
                    h = {};
                "static" === u && (t.style.position = "relative"), r = d.offset(), a = pt.css(t, "top"), l = pt.css(t, "left"), c = ("absolute" === u || "fixed" === u) && pt.inArray("auto", [a, l]) > -1, c ? (n = d.position(), o = n.top, s = n.left) : (o = parseFloat(a) || 0, s = parseFloat(l) || 0), pt.isFunction(e) && (e = e.call(t, i, pt.extend({}, r))), null != e.top && (h.top = e.top - r.top + o), null != e.left && (h.left = e.left - r.left + s), "using" in e ? e.using.call(t, h) : d.css(h)
            }
        }, pt.fn.extend({
            offset: function(t) {
                if (arguments.length) return t === undefined ? this : this.each(function(e) {
                    pt.offset.setOffset(this, t, e)
                });
                var e, i, n = {
                        top: 0,
                        left: 0
                    },
                    s = this[0],
                    a = s && s.ownerDocument;
                if (a) return e = a.documentElement, pt.contains(e, s) ? ("undefined" != typeof s.getBoundingClientRect && (n = s.getBoundingClientRect()), i = et(a), {
                    top: n.top + (i.pageYOffset || e.scrollTop) - (e.clientTop || 0),
                    left: n.left + (i.pageXOffset || e.scrollLeft) - (e.clientLeft || 0)
                }) : n
            },
            position: function() {
                if (this[0]) {
                    var t, e, i = {
                            top: 0,
                            left: 0
                        },
                        n = this[0];
                    return "fixed" === pt.css(n, "position") ? e = n.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), pt.nodeName(t[0], "html") || (i = t.offset()), i.top += pt.css(t[0], "borderTopWidth", !0), i.left += pt.css(t[0], "borderLeftWidth", !0)), {
                        top: e.top - i.top - pt.css(n, "marginTop", !0),
                        left: e.left - i.left - pt.css(n, "marginLeft", !0)
                    }
                }
            },
            offsetParent: function() {
                return this.map(function() {
                    for (var t = this.offsetParent; t && !pt.nodeName(t, "html") && "static" === pt.css(t, "position");) t = t.offsetParent;
                    return t || he
                })
            }
        }), pt.each({
            scrollLeft: "pageXOffset",
            scrollTop: "pageYOffset"
        }, function(t, e) {
            var i = /Y/.test(e);
            pt.fn[t] = function(n) {
                return Ht(this, function(t, n, s) {
                    var a = et(t);
                    if (s === undefined) return a ? e in a ? a[e] : a.document.documentElement[n] : t[n];
                    a ? a.scrollTo(i ? pt(a).scrollLeft() : s, i ? s : pt(a).scrollTop()) : t[n] = s
                }, t, n, arguments.length, null)
            }
        }),
        pt.each(["top", "left"], function(t, e) {
            pt.cssHooks[e] = E(dt.pixelPosition, function(t, i) {
                if (i) return i = fe(t, e), ue.test(i) ? pt(t).position()[e] + "px" : i
            })
        }), pt.each({
            Height: "height",
            Width: "width"
        }, function(t, e) {
            pt.each({
                padding: "inner" + t,
                content: e,
                "": "outer" + t
            }, function(i, n) {
                pt.fn[n] = function(n, s) {
                    var a = arguments.length && (i || "boolean" != typeof n),
                        o = i || (!0 === n || !0 === s ? "margin" : "border");
                    return Ht(this, function(e, i, n) {
                        var s;
                        return pt.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (s = e.documentElement, Math.max(e.body["scroll" + t], s["scroll" + t], e.body["offset" + t], s["offset" + t], s["client" + t])) : n === undefined ? pt.css(e, i, o) : pt.style(e, i, n, o)
                    }, e, a ? n : undefined, a, null)
                }
            })
        }), pt.fn.extend({
            bind: function(t, e, i) {
                return this.on(t, null, e, i)
            },
            unbind: function(t, e) {
                return this.off(t, null, e)
            },
            delegate: function(t, e, i, n) {
                return this.on(e, t, i, n)
            },
            undelegate: function(t, e, i) {
                return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", i)
            }
        }), pt.fn.size = function() {
            return this.length
        }, pt.fn.andSelf = pt.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
            return pt
        });
    var hi = t.jQuery,
        pi = t.$;
    return pt.noConflict = function(e) {
        return t.$ === pt && (t.$ = pi), e && t.jQuery === pt && (t.jQuery = hi), pt
    }, e || (t.jQuery = t.$ = pt), pt
}),
function(t, e) {
    "use strict";
    t.rails !== e && t.error("jquery-ujs has already been loaded!");
    var i, n = t(document);
    t.rails = i = {
        linkClickSelector: "a[data-confirm], a[data-method], a[data-remote]:not([disabled]), a[data-disable-with], a[data-disable]",
        buttonClickSelector: "button[data-remote]:not([form]):not(form button), button[data-confirm]:not([form]):not(form button)",
        inputChangeSelector: "select[data-remote], input[data-remote], textarea[data-remote]",
        formSubmitSelector: "form",
        formInputClickSelector: "form input[type=submit], form input[type=image], form button[type=submit], form button:not([type]), input[type=submit][form], input[type=image][form], button[type=submit][form], button[form]:not([type])",
        disableSelector: "input[data-disable-with]:enabled, button[data-disable-with]:enabled, textarea[data-disable-with]:enabled, input[data-disable]:enabled, button[data-disable]:enabled, textarea[data-disable]:enabled",
        enableSelector: "input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled, input[data-disable]:disabled, button[data-disable]:disabled, textarea[data-disable]:disabled",
        requiredInputSelector: "input[name][required]:not([disabled]), textarea[name][required]:not([disabled])",
        fileInputSelector: "input[name][type=file]:not([disabled])",
        linkDisableSelector: "a[data-disable-with], a[data-disable]",
        buttonDisableSelector: "button[data-remote][data-disable-with], button[data-remote][data-disable]",
        csrfToken: function() {
            return t("meta[name=csrf-token]").attr("content")
        },
        csrfParam: function() {
            return t("meta[name=csrf-param]").attr("content")
        },
        CSRFProtection: function(t) {
            var e = i.csrfToken();
            e && t.setRequestHeader("X-CSRF-Token", e)
        },
        refreshCSRFTokens: function() {
            t('form input[name="' + i.csrfParam() + '"]').val(i.csrfToken())
        },
        fire: function(e, i, n) {
            var s = t.Event(i);
            return e.trigger(s, n), !1 !== s.result
        },
        confirm: function(t) {
            return confirm(t)
        },
        ajax: function(e) {
            return t.ajax(e)
        },
        href: function(t) {
            return t[0].href
        },
        isRemote: function(t) {
            return t.data("remote") !== e && !1 !== t.data("remote")
        },
        handleRemote: function(n) {
            var s, a, o, r, l, c;
            if (i.fire(n, "ajax:before")) {
                if (r = n.data("with-credentials") || null, l = n.data("type") || t.ajaxSettings && t.ajaxSettings.dataType, n.is("form")) {
                    s = n.data("ujs:submit-button-formmethod") || n.attr("method"), a = n.data("ujs:submit-button-formaction") || n.attr("action"), o = t(n[0]).serializeArray();
                    var u = n.data("ujs:submit-button");
                    u && (o.push(u), n.data("ujs:submit-button", null)), n.data("ujs:submit-button-formmethod", null), n.data("ujs:submit-button-formaction", null)
                } else n.is(i.inputChangeSelector) ? (s = n.data("method"), a = n.data("url"), o = n.serialize(), n.data("params") && (o = o + "&" + n.data("params"))) : n.is(i.buttonClickSelector) ? (s = n.data("method") || "get", a = n.data("url"), o = n.serialize(), n.data("params") && (o = o + "&" + n.data("params"))) : (s = n.data("method"), a = i.href(n), o = n.data("params") || null);
                return c = {
                    type: s || "GET",
                    data: o,
                    dataType: l,
                    beforeSend: function(t, s) {
                        if (s.dataType === e && t.setRequestHeader("accept", "*/*;q=0.5, " + s.accepts.script), !i.fire(n, "ajax:beforeSend", [t, s])) return !1;
                        n.trigger("ajax:send", t)
                    },
                    success: function(t, e, i) {
                        n.trigger("ajax:success", [t, e, i])
                    },
                    complete: function(t, e) {
                        n.trigger("ajax:complete", [t, e])
                    },
                    error: function(t, e, i) {
                        n.trigger("ajax:error", [t, e, i])
                    },
                    crossDomain: i.isCrossDomain(a)
                }, r && (c.xhrFields = {
                    withCredentials: r
                }), a && (c.url = a), i.ajax(c)
            }
            return !1
        },
        isCrossDomain: function(t) {
            var e = document.createElement("a");
            e.href = location.href;
            var i = document.createElement("a");
            try {
                return i.href = t, i.href = i.href, !((!i.protocol || ":" === i.protocol) && !i.host || e.protocol + "//" + e.host == i.protocol + "//" + i.host)
            } catch (t) {
                return !0
            }
        },
        handleMethod: function(n) {
            var s = i.href(n),
                a = n.data("method"),
                o = n.attr("target"),
                r = i.csrfToken(),
                l = i.csrfParam(),
                c = t('<form method="post" action="' + s + '"></form>'),
                u = '<input name="_method" value="' + a + '" type="hidden" />';
            l === e || r === e || i.isCrossDomain(s) || (u += '<input name="' + l + '" value="' + r + '" type="hidden" />'), o && c.attr("target", o), c.hide().append(u).appendTo("body"), c.submit()
        },
        formElements: function(e, i) {
            return e.is("form") ? t(e[0].elements).filter(i) : e.find(i)
        },
        disableFormElements: function(e) {
            i.formElements(e, i.disableSelector).each(function() {
                i.disableFormElement(t(this))
            })
        },
        disableFormElement: function(t) {
            var i, n;
            i = t.is("button") ? "html" : "val", n = t.data("disable-with"), n !== e && (t.data("ujs:enable-with", t[i]()), t[i](n)), t.prop("disabled", !0), t.data("ujs:disabled", !0)
        },
        enableFormElements: function(e) {
            i.formElements(e, i.enableSelector).each(function() {
                i.enableFormElement(t(this))
            })
        },
        enableFormElement: function(t) {
            var i = t.is("button") ? "html" : "val";
            t.data("ujs:enable-with") !== e && (t[i](t.data("ujs:enable-with")), t.removeData("ujs:enable-with")), t.prop("disabled", !1), t.removeData("ujs:disabled")
        },
        allowAction: function(t) {
            var e, n = t.data("confirm"),
                s = !1;
            if (!n) return !0;
            if (i.fire(t, "confirm")) {
                try {
                    s = i.confirm(n)
                } catch (t) {
                    (console.error || console.log).call(console, t.stack || t)
                }
                e = i.fire(t, "confirm:complete", [s])
            }
            return s && e
        },
        blankInputs: function(e, i, n) {
            var s, a, o, r, l = t(),
                c = i || "input,textarea",
                u = e.find(c),
                d = {};
            return u.each(function() {
                s = t(this), s.is("input[type=radio]") ? (r = s.attr("name"), d[r] || (0 === e.find('input[type=radio]:checked[name="' + r + '"]').length && (o = e.find('input[type=radio][name="' + r + '"]'), l = l.add(o)), d[r] = r)) : (a = s.is("input[type=checkbox],input[type=radio]") ? s.is(":checked") : !!s.val()) === n && (l = l.add(s))
            }), !!l.length && l
        },
        nonBlankInputs: function(t, e) {
            return i.blankInputs(t, e, !0)
        },
        stopEverything: function(e) {
            return t(e.target).trigger("ujs:everythingStopped"), e.stopImmediatePropagation(), !1
        },
        disableElement: function(t) {
            var n = t.data("disable-with");
            n !== e && (t.data("ujs:enable-with", t.html()), t.html(n)), t.bind("click.railsDisable", function(t) {
                return i.stopEverything(t)
            }), t.data("ujs:disabled", !0)
        },
        enableElement: function(t) {
            t.data("ujs:enable-with") !== e && (t.html(t.data("ujs:enable-with")), t.removeData("ujs:enable-with")), t.unbind("click.railsDisable"), t.removeData("ujs:disabled")
        }
    }, i.fire(n, "rails:attachBindings") && (t.ajaxPrefilter(function(t, e, n) {
        t.crossDomain || i.CSRFProtection(n)
    }), t(window).on("pageshow.rails", function() {
        t(t.rails.enableSelector).each(function() {
            var e = t(this);
            e.data("ujs:disabled") && t.rails.enableFormElement(e)
        }), t(t.rails.linkDisableSelector).each(function() {
            var e = t(this);
            e.data("ujs:disabled") && t.rails.enableElement(e)
        })
    }), n.on("ajax:complete", i.linkDisableSelector, function() {
        i.enableElement(t(this))
    }), n.on("ajax:complete", i.buttonDisableSelector, function() {
        i.enableFormElement(t(this))
    }), n.on("click.rails", i.linkClickSelector, function(e) {
        var n = t(this),
            s = n.data("method"),
            a = n.data("params"),
            o = e.metaKey || e.ctrlKey;
        if (!i.allowAction(n)) return i.stopEverything(e);
        if (!o && n.is(i.linkDisableSelector) && i.disableElement(n), i.isRemote(n)) {
            if (o && (!s || "GET" === s) && !a) return !0;
            var r = i.handleRemote(n);
            return !1 === r ? i.enableElement(n) : r.fail(function() {
                i.enableElement(n)
            }), !1
        }
        return s ? (i.handleMethod(n), !1) : void 0
    }), n.on("click.rails", i.buttonClickSelector, function(e) {
        var n = t(this);
        if (!i.allowAction(n) || !i.isRemote(n)) return i.stopEverything(e);
        n.is(i.buttonDisableSelector) && i.disableFormElement(n);
        var s = i.handleRemote(n);
        return !1 === s ? i.enableFormElement(n) : s.fail(function() {
            i.enableFormElement(n)
        }), !1
    }), n.on("change.rails", i.inputChangeSelector, function(e) {
        var n = t(this);
        return i.allowAction(n) && i.isRemote(n) ? (i.handleRemote(n), !1) : i.stopEverything(e)
    }), n.on("submit.rails", i.formSubmitSelector, function(n) {
        var s, a, o = t(this),
            r = i.isRemote(o);
        if (!i.allowAction(o)) return i.stopEverything(n);
        if (o.attr("novalidate") === e)
            if (o.data("ujs:formnovalidate-button") === e) {
                if ((s = i.blankInputs(o, i.requiredInputSelector, !1)) && i.fire(o, "ajax:aborted:required", [s])) return i.stopEverything(n)
            } else o.data("ujs:formnovalidate-button", e);
        if (r) {
            if (a = i.nonBlankInputs(o, i.fileInputSelector)) {
                setTimeout(function() {
                    i.disableFormElements(o)
                }, 13);
                var l = i.fire(o, "ajax:aborted:file", [a]);
                return l || setTimeout(function() {
                    i.enableFormElements(o)
                }, 13), l
            }
            return i.handleRemote(o), !1
        }
        setTimeout(function() {
            i.disableFormElements(o)
        }, 13)
    }), n.on("click.rails", i.formInputClickSelector, function(e) {
        var n = t(this);
        if (!i.allowAction(n)) return i.stopEverything(e);
        var s = n.attr("name"),
            a = s ? {
                name: s,
                value: n.val()
            } : null,
            o = n.closest("form");
        0 === o.length && (o = t("#" + n.attr("form"))), o.data("ujs:submit-button", a), o.data("ujs:formnovalidate-button", n.attr("formnovalidate")), o.data("ujs:submit-button-formaction", n.attr("formaction")), o.data("ujs:submit-button-formmethod", n.attr("formmethod"))
    }), n.on("ajax:send.rails", i.formSubmitSelector, function(e) {
        this === e.target && i.disableFormElements(t(this))
    }), n.on("ajax:complete.rails", i.formSubmitSelector, function(e) {
        this === e.target && i.enableFormElements(t(this))
    }), t(function() {
        i.refreshCSRFTokens()
    }))
}(jQuery),
function(t, e) {
    function i(e, i) {
        var s, a, o, r = e.nodeName.toLowerCase();
        return "area" === r ? (s = e.parentNode, a = s.name, !(!e.href || !a || "map" !== s.nodeName.toLowerCase()) && (!!(o = t("img[usemap=#" + a + "]")[0]) && n(o))) : (/input|select|textarea|button|object/.test(r) ? !e.disabled : "a" === r ? e.href || i : i) && n(e)
    }

    function n(e) {
        return t.expr.filters.visible(e) && !t(e).parents().andSelf().filter(function() {
            return "hidden" === t.css(this, "visibility")
        }).length
    }
    var s = 0,
        a = /^ui-id-\d+$/;
    t.ui = t.ui || {}, t.ui.version || (t.extend(t.ui, {
        version: "1.9.1",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            NUMPAD_ADD: 107,
            NUMPAD_DECIMAL: 110,
            NUMPAD_DIVIDE: 111,
            NUMPAD_ENTER: 108,
            NUMPAD_MULTIPLY: 106,
            NUMPAD_SUBTRACT: 109,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    }), t.fn.extend({
        _focus: t.fn.focus,
        focus: function(e, i) {
            return "number" == typeof e ? this.each(function() {
                var n = this;
                setTimeout(function() {
                    t(n).focus(), i && i.call(n)
                }, e)
            }) : this._focus.apply(this, arguments)
        },
        scrollParent: function() {
            var e;
            return e = t.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function() {
                return /(relative|absolute|fixed)/.test(t.css(this, "position")) && /(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
            }).eq(0) : this.parents().filter(function() {
                return /(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
            }).eq(0), /fixed/.test(this.css("position")) || !e.length ? t(document) : e
        },
        zIndex: function(i) {
            if (i !== e) return this.css("zIndex", i);
            if (this.length)
                for (var n, s, a = t(this[0]); a.length && a[0] !== document;) {
                    if (("absolute" === (n = a.css("position")) || "relative" === n || "fixed" === n) && (s = parseInt(a.css("zIndex"), 10), !isNaN(s) && 0 !== s)) return s;
                    a = a.parent()
                }
            return 0
        },
        uniqueId: function() {
            return this.each(function() {
                this.id || (this.id = "ui-id-" + ++s)
            })
        },
        removeUniqueId: function() {
            return this.each(function() {
                a.test(this.id) && t(this).removeAttr("id")
            })
        }
    }), t("<a>").outerWidth(1).jquery || t.each(["Width", "Height"], function(i, n) {
        function s(e, i, n, s) {
            return t.each(a, function() {
                i -= parseFloat(t.css(e, "padding" + this)) || 0, n && (i -= parseFloat(t.css(e, "border" + this + "Width")) || 0), s && (i -= parseFloat(t.css(e, "margin" + this)) || 0)
            }), i
        }
        var a = "Width" === n ? ["Left", "Right"] : ["Top", "Bottom"],
            o = n.toLowerCase(),
            r = {
                innerWidth: t.fn.innerWidth,
                innerHeight: t.fn.innerHeight,
                outerWidth: t.fn.outerWidth,
                outerHeight: t.fn.outerHeight
            };
        t.fn["inner" + n] = function(i) {
            return i === e ? r["inner" + n].call(this) : this.each(function() {
                t(this).css(o, s(this, i) + "px")
            })
        }, t.fn["outer" + n] = function(e, i) {
            return "number" != typeof e ? r["outer" + n].call(this, e) : this.each(function() {
                t(this).css(o, s(this, e, !0, i) + "px")
            })
        }
    }), t.extend(t.expr[":"], {
        data: t.expr.createPseudo ? t.expr.createPseudo(function(e) {
            return function(i) {
                return !!t.data(i, e)
            }
        }) : function(e, i, n) {
            return !!t.data(e, n[3])
        },
        focusable: function(e) {
            return i(e, !isNaN(t.attr(e, "tabindex")))
        },
        tabbable: function(e) {
            var n = t.attr(e, "tabindex"),
                s = isNaN(n);
            return (s || n >= 0) && i(e, !s)
        }
    }), t(function() {
        var e = document.body,
            i = e.appendChild(i = document.createElement("div"));
        i.offsetHeight, t.extend(i.style, {
            minHeight: "100px",
            height: "auto",
            padding: 0,
            borderWidth: 0
        }), t.support.minHeight = 100 === i.offsetHeight, t.support.selectstart = "onselectstart" in i, e.removeChild(i).style.display = "none"
    }), function() {
        var e = /msie ([\w.]+)/.exec(navigator.userAgent.toLowerCase()) || [];
        t.ui.ie = !!e.length, t.ui.ie6 = 6 === parseFloat(e[1], 10)
    }(), t.fn.extend({
        disableSelection: function() {
            return this.bind((t.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function(t) {
                t.preventDefault()
            })
        },
        enableSelection: function() {
            return this.unbind(".ui-disableSelection")
        }
    }), t.extend(t.ui, {
        plugin: {
            add: function(e, i, n) {
                var s, a = t.ui[e].prototype;
                for (s in n) a.plugins[s] = a.plugins[s] || [], a.plugins[s].push([i, n[s]])
            },
            call: function(t, e, i) {
                var n, s = t.plugins[e];
                if (s && t.element[0].parentNode && 11 !== t.element[0].parentNode.nodeType)
                    for (n = 0; n < s.length; n++) t.options[s[n][0]] && s[n][1].apply(t.element, i)
            }
        },
        contains: t.contains,
        hasScroll: function(e, i) {
            if ("hidden" === t(e).css("overflow")) return !1;
            var n = i && "left" === i ? "scrollLeft" : "scrollTop",
                s = !1;
            return e[n] > 0 || (e[n] = 1, s = e[n] > 0, e[n] = 0, s)
        },
        isOverAxis: function(t, e, i) {
            return t > e && t < e + i
        },
        isOver: function(e, i, n, s, a, o) {
            return t.ui.isOverAxis(e, n, a) && t.ui.isOverAxis(i, s, o)
        }
    }))
}(jQuery),
function(t, e) {
    var i = 0,
        n = Array.prototype.slice,
        s = t.cleanData;
    t.cleanData = function(e) {
        for (var i, n = 0; null != (i = e[n]); n++) try {
            t(i).triggerHandler("remove")
        } catch (t) {}
        s(e)
    }, t.widget = function(e, i, n) {
        var s, a, o, r, l = e.split(".")[0];
        e = e.split(".")[1], s = l + "-" + e, n || (n = i, i = t.Widget), t.expr[":"][s.toLowerCase()] = function(e) {
            return !!t.data(e, s)
        }, t[l] = t[l] || {}, a = t[l][e], o = t[l][e] = function(t, e) {
            if (!this._createWidget) return new o(t, e);
            arguments.length && this._createWidget(t, e)
        }, t.extend(o, a, {
            version: n.version,
            _proto: t.extend({}, n),
            _childConstructors: []
        }), r = new i, r.options = t.widget.extend({}, r.options), t.each(n, function(e, s) {
            t.isFunction(s) && (n[e] = function() {
                var t = function() {
                        return i.prototype[e].apply(this, arguments)
                    },
                    n = function(t) {
                        return i.prototype[e].apply(this, t)
                    };
                return function() {
                    var e, i = this._super,
                        a = this._superApply;
                    return this._super = t, this._superApply = n, e = s.apply(this, arguments), this._super = i, this._superApply = a, e
                }
            }())
        }), o.prototype = t.widget.extend(r, {
            widgetEventPrefix: r.widgetEventPrefix || e
        }, n, {
            constructor: o,
            namespace: l,
            widgetName: e,
            widgetBaseClass: s,
            widgetFullName: s
        }), a ? (t.each(a._childConstructors, function(e, i) {
            var n = i.prototype;
            t.widget(n.namespace + "." + n.widgetName, o, i._proto)
        }), delete a._childConstructors) : i._childConstructors.push(o), t.widget.bridge(e, o)
    }, t.widget.extend = function(i) {
        for (var s, a, o = n.call(arguments, 1), r = 0, l = o.length; r < l; r++)
            for (s in o[r]) a = o[r][s], o[r].hasOwnProperty(s) && a !== e && (t.isPlainObject(a) ? i[s] = t.isPlainObject(i[s]) ? t.widget.extend({}, i[s], a) : t.widget.extend({}, a) : i[s] = a);
        return i
    }, t.widget.bridge = function(i, s) {
        var a = s.prototype.widgetFullName;
        t.fn[i] = function(o) {
            var r = "string" == typeof o,
                l = n.call(arguments, 1),
                c = this;
            return o = !r && l.length ? t.widget.extend.apply(null, [o].concat(l)) : o, r ? this.each(function() {
                var n, s = t.data(this, a);
                return s ? t.isFunction(s[o]) && "_" !== o.charAt(0) ? (n = s[o].apply(s, l), n !== s && n !== e ? (c = n && n.jquery ? c.pushStack(n.get()) : n, !1) : void 0) : t.error("no such method '" + o + "' for " + i + " widget instance") : t.error("cannot call methods on " + i + " prior to initialization; attempted to call method '" + o + "'")
            }) : this.each(function() {
                var e = t.data(this, a);
                e ? e.option(o || {})._init() : new s(o, this)
            }), c
        }
    }, t.Widget = function() {}, t.Widget._childConstructors = [], t.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {
            disabled: !1,
            create: null
        },
        _createWidget: function(e, n) {
            n = t(n || this.defaultElement || this)[0], this.element = t(n), this.uuid = i++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = t.widget.extend({}, this.options, this._getCreateOptions(), e), this.bindings = t(), this.hoverable = t(), this.focusable = t(), n !== this && (t.data(n, this.widgetName, this), t.data(n, this.widgetFullName, this), this._on(this.element, {
                remove: function(t) {
                    t.target === n && this.destroy()
                }
            }), this.document = t(n.style ? n.ownerDocument : n.document || n), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
        },
        _getCreateOptions: t.noop,
        _getCreateEventData: t.noop,
        _create: t.noop,
        _init: t.noop,
        destroy: function() {
            this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
        },
        _destroy: t.noop,
        widget: function() {
            return this.element
        },
        option: function(i, n) {
            var s, a, o, r = i;
            if (0 === arguments.length) return t.widget.extend({}, this.options);
            if ("string" == typeof i)
                if (r = {}, s = i.split("."), i = s.shift(), s.length) {
                    for (a = r[i] = t.widget.extend({}, this.options[i]), o = 0; o < s.length - 1; o++) a[s[o]] = a[s[o]] || {}, a = a[s[o]];
                    if (i = s.pop(), n === e) return a[i] === e ? null : a[i];
                    a[i] = n
                } else {
                    if (n === e) return this.options[i] === e ? null : this.options[i];
                    r[i] = n
                }
            return this._setOptions(r), this
        },
        _setOptions: function(t) {
            var e;
            for (e in t) this._setOption(e, t[e]);
            return this
        },
        _setOption: function(t, e) {
            return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!e).attr("aria-disabled", e), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
        },
        enable: function() {
            return this._setOption("disabled", !1)
        },
        disable: function() {
            return this._setOption("disabled", !0)
        },
        _on: function(e, i) {
            var n, s = this;
            i ? (e = n = t(e), this.bindings = this.bindings.add(e)) : (i = e, e = this.element, n = this.widget()), t.each(i, function(i, a) {
                function o() {
                    if (!0 !== s.options.disabled && !t(this).hasClass("ui-state-disabled")) return ("string" == typeof a ? s[a] : a).apply(s, arguments)
                }
                "string" != typeof a && (o.guid = a.guid = a.guid || o.guid || t.guid++);
                var r = i.match(/^(\w+)\s*(.*)$/),
                    l = r[1] + s.eventNamespace,
                    c = r[2];
                c ? n.delegate(c, l, o) : e.bind(l, o)
            })
        },
        _off: function(t, e) {
            e = (e || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, t.unbind(e).undelegate(e)
        },
        _delay: function(t, e) {
            function i() {
                return ("string" == typeof t ? n[t] : t).apply(n, arguments)
            }
            var n = this;
            return setTimeout(i, e || 0)
        },
        _hoverable: function(e) {
            this.hoverable = this.hoverable.add(e), this._on(e, {
                mouseenter: function(e) {
                    t(e.currentTarget).addClass("ui-state-hover")
                },
                mouseleave: function(e) {
                    t(e.currentTarget).removeClass("ui-state-hover")
                }
            })
        },
        _focusable: function(e) {
            this.focusable = this.focusable.add(e), this._on(e, {
                focusin: function(e) {
                    t(e.currentTarget).addClass("ui-state-focus")
                },
                focusout: function(e) {
                    t(e.currentTarget).removeClass("ui-state-focus")
                }
            })
        },
        _trigger: function(e, i, n) {
            var s, a, o = this.options[e];
            if (n = n || {}, i = t.Event(i), i.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), i.target = this.element[0], a = i.originalEvent)
                for (s in a) s in i || (i[s] = a[s]);
            return this.element.trigger(i, n), !(t.isFunction(o) && !1 === o.apply(this.element[0], [i].concat(n)) || i.isDefaultPrevented())
        }
    }, t.each({
        show: "fadeIn",
        hide: "fadeOut"
    }, function(e, i) {
        t.Widget.prototype["_" + e] = function(n, s, a) {
            "string" == typeof s && (s = {
                effect: s
            });
            var o, r = s ? !0 === s || "number" == typeof s ? i : s.effect || i : e;
            s = s || {}, "number" == typeof s && (s = {
                duration: s
            }), o = !t.isEmptyObject(s), s.complete = a, s.delay && n.delay(s.delay), o && t.effects && (t.effects.effect[r] || !1 !== t.uiBackCompat && t.effects[r]) ? n[e](s) : r !== e && n[r] ? n[r](s.duration, s.easing, a) : n.queue(function(i) {
                t(this)[e](), a && a.call(n[0]), i()
            })
        }
    }), !1 !== t.uiBackCompat && (t.Widget.prototype._getCreateOptions = function() {
        return t.metadata && t.metadata.get(this.element[0])[this.widgetName]
    })
}(jQuery),
function(t) {
    var e = !1;
    t(document).mouseup(function() {
        e = !1
    }), t.widget("ui.mouse", {
        version: "1.9.1",
        options: {
            cancel: "input,textarea,button,select,option",
            distance: 1,
            delay: 0
        },
        _mouseInit: function() {
            var e = this;
            this.element.bind("mousedown." + this.widgetName, function(t) {
                return e._mouseDown(t)
            }).bind("click." + this.widgetName, function(i) {
                if (!0 === t.data(i.target, e.widgetName + ".preventClickEvent")) return t.removeData(i.target, e.widgetName + ".preventClickEvent"), i.stopImmediatePropagation(), !1
            }), this.started = !1
        },
        _mouseDestroy: function() {
            this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
        },
        _mouseDown: function(i) {
            if (!e) {
                this._mouseStarted && this._mouseUp(i), this._mouseDownEvent = i;
                var n = this,
                    s = 1 === i.which,
                    a = !("string" != typeof this.options.cancel || !i.target.nodeName) && t(i.target).closest(this.options.cancel).length;
                return !(s && !a && this._mouseCapture(i)) || (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
                    n.mouseDelayMet = !0
                }, this.options.delay)), this._mouseDistanceMet(i) && this._mouseDelayMet(i) && (this._mouseStarted = !1 !== this._mouseStart(i), !this._mouseStarted) ? (i.preventDefault(), !0) : (!0 === t.data(i.target, this.widgetName + ".preventClickEvent") && t.removeData(i.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function(t) {
                    return n._mouseMove(t)
                }, this._mouseUpDelegate = function(t) {
                    return n._mouseUp(t)
                }, t(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), i.preventDefault(), e = !0, !0))
            }
        },
        _mouseMove: function(e) {
            return !t.ui.ie || document.documentMode >= 9 || e.button ? this._mouseStarted ? (this._mouseDrag(e), e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = !1 !== this._mouseStart(this._mouseDownEvent, e), this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted) : this._mouseUp(e)
        },
        _mouseUp: function(e) {
            return t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, e.target === this._mouseDownEvent.target && t.data(e.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(e)), !1
        },
        _mouseDistanceMet: function(t) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance
        },
        _mouseDelayMet: function() {
            return this.mouseDelayMet
        },
        _mouseStart: function() {},
        _mouseDrag: function() {},
        _mouseStop: function() {},
        _mouseCapture: function() {
            return !0
        }
    })
}(jQuery),
function(t) {
    t.widget("ui.draggable", t.ui.mouse, {
        version: "1.9.1",
        widgetEventPrefix: "drag",
        options: {
            addClasses: !0,
            appendTo: "parent",
            axis: !1,
            connectToSortable: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            iframeFix: !1,
            opacity: !1,
            refreshPositions: !1,
            revert: !1,
            revertDuration: 500,
            scope: "default",
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: !1,
            snapMode: "both",
            snapTolerance: 20,
            stack: !1,
            zIndex: !1
        },
        _create: function() {
            "original" != this.options.helper || /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative"), this.options.addClasses && this.element.addClass("ui-draggable"), this.options.disabled && this.element.addClass("ui-draggable-disabled"), this._mouseInit()
        },
        _destroy: function() {
            this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"), this._mouseDestroy()
        },
        _mouseCapture: function(e) {
            var i = this.options;
            return !(this.helper || i.disabled || t(e.target).is(".ui-resizable-handle")) && (this.handle = this._getHandle(e), !!this.handle && (t(!0 === i.iframeFix ? "iframe" : i.iframeFix).each(function() {
                t('<div class="ui-draggable-iframeFix" style="background: #fff;"></div>').css({
                    width: this.offsetWidth + "px",
                    height: this.offsetHeight + "px",
                    position: "absolute",
                    opacity: "0.001",
                    zIndex: 1e3
                }).css(t(this).offset()).appendTo("body")
            }), !0))
        },
        _mouseStart: function(e) {
            var i = this.options;
            return this.helper = this._createHelper(e), this.helper.addClass("ui-draggable-dragging"), this._cacheHelperProportions(), t.ui.ddmanager && (t.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(), this.offset = this.positionAbs = this.element.offset(), this.offset = {
                top: this.offset.top - this.margins.top,
                left: this.offset.left - this.margins.left
            }, t.extend(this.offset, {
                click: {
                    left: e.pageX - this.offset.left,
                    top: e.pageY - this.offset.top
                },
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            }), this.originalPosition = this.position = this._generatePosition(e), this.originalPageX = e.pageX, this.originalPageY = e.pageY, i.cursorAt && this._adjustOffsetFromHelper(i.cursorAt), i.containment && this._setContainment(), !1 === this._trigger("start", e) ? (this._clear(), !1) : (this._cacheHelperProportions(), t.ui.ddmanager && !i.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this._mouseDrag(e, !0), t.ui.ddmanager && t.ui.ddmanager.dragStart(this, e), !0)
        },
        _mouseDrag: function(e, i) {
            if (this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), !i) {
                var n = this._uiHash();
                if (!1 === this._trigger("drag", e, n)) return this._mouseUp({}), !1;
                this.position = n.position
            }
            return this.options.axis && "y" == this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" == this.options.axis || (this.helper[0].style.top = this.position.top + "px"), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), !1
        },
        _mouseStop: function(e) {
            var i = !1;
            t.ui.ddmanager && !this.options.dropBehaviour && (i = t.ui.ddmanager.drop(this, e)), this.dropped && (i = this.dropped, this.dropped = !1);
            for (var n = this.element[0], s = !1; n && (n = n.parentNode);) n == document && (s = !0);
            if (!s && "original" === this.options.helper) return !1;
            if ("invalid" == this.options.revert && !i || "valid" == this.options.revert && i || !0 === this.options.revert || t.isFunction(this.options.revert) && this.options.revert.call(this.element, i)) {
                var a = this;
                t(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
                    !1 !== a._trigger("stop", e) && a._clear()
                })
            } else !1 !== this._trigger("stop", e) && this._clear();
            return !1
        },
        _mouseUp: function(e) {
            return t("div.ui-draggable-iframeFix").each(function() {
                this.parentNode.removeChild(this)
            }), t.ui.ddmanager && t.ui.ddmanager.dragStop(this, e), t.ui.mouse.prototype._mouseUp.call(this, e)
        },
        cancel: function() {
            return this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear(), this
        },
        _getHandle: function(e) {
            var i = !this.options.handle || !t(this.options.handle, this.element).length;
            return t(this.options.handle, this.element).find("*").andSelf().each(function() {
                this == e.target && (i = !0)
            }), i
        },
        _createHelper: function(e) {
            var i = this.options,
                n = t.isFunction(i.helper) ? t(i.helper.apply(this.element[0], [e])) : "clone" == i.helper ? this.element.clone().removeAttr("id") : this.element;
            return n.parents("body").length || n.appendTo("parent" == i.appendTo ? this.element[0].parentNode : i.appendTo), n[0] == this.element[0] || /(fixed|absolute)/.test(n.css("position")) || n.css("position", "absolute"), n
        },
        _adjustOffsetFromHelper: function(e) {
            "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
                left: +e[0],
                top: +e[1] || 0
            }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top)
        },
        _getParentOffset: function() {
            this.offsetParent = this.helper.offsetParent();
            var e = this.offsetParent.offset();
            return "absolute" == this.cssPosition && this.scrollParent[0] != document && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] == document.body || this.offsetParent[0].tagName && "html" == this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {
                top: 0,
                left: 0
            }), {
                top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function() {
            if ("relative" == this.cssPosition) {
                var t = this.element.position();
                return {
                    top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                }
            }
            return {
                top: 0,
                left: 0
            }
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.element.css("marginLeft"), 10) || 0,
                top: parseInt(this.element.css("marginTop"), 10) || 0,
                right: parseInt(this.element.css("marginRight"), 10) || 0,
                bottom: parseInt(this.element.css("marginBottom"), 10) || 0
            }
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            }
        },
        _setContainment: function() {
            var e = this.options;
            if ("parent" == e.containment && (e.containment = this.helper[0].parentNode), "document" != e.containment && "window" != e.containment || (this.containment = ["document" == e.containment ? 0 : t(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, "document" == e.containment ? 0 : t(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, ("document" == e.containment ? 0 : t(window).scrollLeft()) + t("document" == e.containment ? document : window).width() - this.helperProportions.width - this.margins.left, ("document" == e.containment ? 0 : t(window).scrollTop()) + (t("document" == e.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), /^(document|window|parent)$/.test(e.containment) || e.containment.constructor == Array) e.containment.constructor == Array && (this.containment = e.containment);
            else {
                var i = t(e.containment),
                    n = i[0];
                if (!n) return;
                var s = (i.offset(), "hidden" != t(n).css("overflow"));
                this.containment = [(parseInt(t(n).css("borderLeftWidth"), 10) || 0) + (parseInt(t(n).css("paddingLeft"), 10) || 0), (parseInt(t(n).css("borderTopWidth"), 10) || 0) + (parseInt(t(n).css("paddingTop"), 10) || 0), (s ? Math.max(n.scrollWidth, n.offsetWidth) : n.offsetWidth) - (parseInt(t(n).css("borderLeftWidth"), 10) || 0) - (parseInt(t(n).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (s ? Math.max(n.scrollHeight, n.offsetHeight) : n.offsetHeight) - (parseInt(t(n).css("borderTopWidth"), 10) || 0) - (parseInt(t(n).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relative_container = i
            }
        },
        _convertPositionTo: function(e, i) {
            i || (i = this.position);
            var n = "absolute" == e ? 1 : -1,
                s = (this.options, "absolute" != this.cssPosition || this.scrollParent[0] != document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent),
                a = /(html|body)/i.test(s[0].tagName);
            return {
                top: i.top + this.offset.relative.top * n + this.offset.parent.top * n - ("fixed" == this.cssPosition ? -this.scrollParent.scrollTop() : a ? 0 : s.scrollTop()) * n,
                left: i.left + this.offset.relative.left * n + this.offset.parent.left * n - ("fixed" == this.cssPosition ? -this.scrollParent.scrollLeft() : a ? 0 : s.scrollLeft()) * n
            }
        },
        _generatePosition: function(e) {
            var i = this.options,
                n = "absolute" != this.cssPosition || this.scrollParent[0] != document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                s = /(html|body)/i.test(n[0].tagName),
                a = e.pageX,
                o = e.pageY;
            if (this.originalPosition) {
                var r;
                if (this.containment) {
                    if (this.relative_container) {
                        var l = this.relative_container.offset();
                        r = [this.containment[0] + l.left, this.containment[1] + l.top, this.containment[2] + l.left, this.containment[3] + l.top]
                    } else r = this.containment;
                    e.pageX - this.offset.click.left < r[0] && (a = r[0] + this.offset.click.left), e.pageY - this.offset.click.top < r[1] && (o = r[1] + this.offset.click.top), e.pageX - this.offset.click.left > r[2] && (a = r[2] + this.offset.click.left), e.pageY - this.offset.click.top > r[3] && (o = r[3] + this.offset.click.top)
                }
                if (i.grid) {
                    var c = i.grid[1] ? this.originalPageY + Math.round((o - this.originalPageY) / i.grid[1]) * i.grid[1] : this.originalPageY;
                    o = r && (c - this.offset.click.top < r[1] || c - this.offset.click.top > r[3]) ? c - this.offset.click.top < r[1] ? c + i.grid[1] : c - i.grid[1] : c;
                    var u = i.grid[0] ? this.originalPageX + Math.round((a - this.originalPageX) / i.grid[0]) * i.grid[0] : this.originalPageX;
                    a = r && (u - this.offset.click.left < r[0] || u - this.offset.click.left > r[2]) ? u - this.offset.click.left < r[0] ? u + i.grid[0] : u - i.grid[0] : u
                }
            }
            return {
                top: o - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" == this.cssPosition ? -this.scrollParent.scrollTop() : s ? 0 : n.scrollTop()),
                left: a - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" == this.cssPosition ? -this.scrollParent.scrollLeft() : s ? 0 : n.scrollLeft())
            }
        },
        _clear: function() {
            this.helper.removeClass("ui-draggable-dragging"), this.helper[0] == this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1
        },
        _trigger: function(e, i, n) {
            return n = n || this._uiHash(), t.ui.plugin.call(this, e, [i, n]), "drag" == e && (this.positionAbs = this._convertPositionTo("absolute")), t.Widget.prototype._trigger.call(this, e, i, n)
        },
        plugins: {},
        _uiHash: function() {
            return {
                helper: this.helper,
                position: this.position,
                originalPosition: this.originalPosition,
                offset: this.positionAbs
            }
        }
    }), t.ui.plugin.add("draggable", "connectToSortable", {
        start: function(e, i) {
            var n = t(this).data("draggable"),
                s = n.options,
                a = t.extend({}, i, {
                    item: n.element
                });
            n.sortables = [], t(s.connectToSortable).each(function() {
                var i = t.data(this, "sortable");
                i && !i.options.disabled && (n.sortables.push({
                    instance: i,
                    shouldRevert: i.options.revert
                }), i.refreshPositions(), i._trigger("activate", e, a))
            })
        },
        stop: function(e, i) {
            var n = t(this).data("draggable"),
                s = t.extend({}, i, {
                    item: n.element
                });
            t.each(n.sortables, function() {
                this.instance.isOver ? (this.instance.isOver = 0, n.cancelHelperRemoval = !0, this.instance.cancelHelperRemoval = !1, this.shouldRevert && (this.instance.options.revert = !0), this.instance._mouseStop(e), this.instance.options.helper = this.instance.options._helper, "original" == n.options.helper && this.instance.currentItem.css({
                    top: "auto",
                    left: "auto"
                })) : (this.instance.cancelHelperRemoval = !1, this.instance._trigger("deactivate", e, s))
            })
        },
        drag: function(e, i) {
            var n = t(this).data("draggable"),
                s = this;
            t.each(n.sortables, function() {
                var a = !1,
                    o = this;
                this.instance.positionAbs = n.positionAbs, this.instance.helperProportions = n.helperProportions, this.instance.offset.click = n.offset.click, this.instance._intersectsWith(this.instance.containerCache) && (a = !0, t.each(n.sortables, function() {
                    return this.instance.positionAbs = n.positionAbs, this.instance.helperProportions = n.helperProportions, this.instance.offset.click = n.offset.click, this != o && this.instance._intersectsWith(this.instance.containerCache) && t.ui.contains(o.instance.element[0], this.instance.element[0]) && (a = !1), a
                })), a ? (this.instance.isOver || (this.instance.isOver = 1, this.instance.currentItem = t(s).clone().removeAttr("id").appendTo(this.instance.element).data("sortable-item", !0), this.instance.options._helper = this.instance.options.helper, this.instance.options.helper = function() {
                    return i.helper[0]
                }, e.target = this.instance.currentItem[0], this.instance._mouseCapture(e, !0), this.instance._mouseStart(e, !0, !0), this.instance.offset.click.top = n.offset.click.top, this.instance.offset.click.left = n.offset.click.left, this.instance.offset.parent.left -= n.offset.parent.left - this.instance.offset.parent.left, this.instance.offset.parent.top -= n.offset.parent.top - this.instance.offset.parent.top, n._trigger("toSortable", e), n.dropped = this.instance.element, n.currentItem = n.element, this.instance.fromOutside = n), this.instance.currentItem && this.instance._mouseDrag(e)) : this.instance.isOver && (this.instance.isOver = 0, this.instance.cancelHelperRemoval = !0, this.instance.options.revert = !1, this.instance._trigger("out", e, this.instance._uiHash(this.instance)), this.instance._mouseStop(e, !0), this.instance.options.helper = this.instance.options._helper, this.instance.currentItem.remove(), this.instance.placeholder && this.instance.placeholder.remove(), n._trigger("fromSortable", e), n.dropped = !1)
            })
        }
    }), t.ui.plugin.add("draggable", "cursor", {
        start: function() {
            var e = t("body"),
                i = t(this).data("draggable").options;
            e.css("cursor") && (i._cursor = e.css("cursor")), e.css("cursor", i.cursor)
        },
        stop: function() {
            var e = t(this).data("draggable").options;
            e._cursor && t("body").css("cursor", e._cursor)
        }
    }), t.ui.plugin.add("draggable", "opacity", {
        start: function(e, i) {
            var n = t(i.helper),
                s = t(this).data("draggable").options;
            n.css("opacity") && (s._opacity = n.css("opacity")), n.css("opacity", s.opacity)
        },
        stop: function(e, i) {
            var n = t(this).data("draggable").options;
            n._opacity && t(i.helper).css("opacity", n._opacity)
        }
    }), t.ui.plugin.add("draggable", "scroll", {
        start: function() {
            var e = t(this).data("draggable");
            e.scrollParent[0] != document && "HTML" != e.scrollParent[0].tagName && (e.overflowOffset = e.scrollParent.offset())
        },
        drag: function(e) {
            var i = t(this).data("draggable"),
                n = i.options,
                s = !1;
            i.scrollParent[0] != document && "HTML" != i.scrollParent[0].tagName ? (n.axis && "x" == n.axis || (i.overflowOffset.top + i.scrollParent[0].offsetHeight - e.pageY < n.scrollSensitivity ? i.scrollParent[0].scrollTop = s = i.scrollParent[0].scrollTop + n.scrollSpeed : e.pageY - i.overflowOffset.top < n.scrollSensitivity && (i.scrollParent[0].scrollTop = s = i.scrollParent[0].scrollTop - n.scrollSpeed)), n.axis && "y" == n.axis || (i.overflowOffset.left + i.scrollParent[0].offsetWidth - e.pageX < n.scrollSensitivity ? i.scrollParent[0].scrollLeft = s = i.scrollParent[0].scrollLeft + n.scrollSpeed : e.pageX - i.overflowOffset.left < n.scrollSensitivity && (i.scrollParent[0].scrollLeft = s = i.scrollParent[0].scrollLeft - n.scrollSpeed))) : (n.axis && "x" == n.axis || (e.pageY - t(document).scrollTop() < n.scrollSensitivity ? s = t(document).scrollTop(t(document).scrollTop() - n.scrollSpeed) : t(window).height() - (e.pageY - t(document).scrollTop()) < n.scrollSensitivity && (s = t(document).scrollTop(t(document).scrollTop() + n.scrollSpeed))), n.axis && "y" == n.axis || (e.pageX - t(document).scrollLeft() < n.scrollSensitivity ? s = t(document).scrollLeft(t(document).scrollLeft() - n.scrollSpeed) : t(window).width() - (e.pageX - t(document).scrollLeft()) < n.scrollSensitivity && (s = t(document).scrollLeft(t(document).scrollLeft() + n.scrollSpeed)))), !1 !== s && t.ui.ddmanager && !n.dropBehaviour && t.ui.ddmanager.prepareOffsets(i, e)
        }
    }), t.ui.plugin.add("draggable", "snap", {
        start: function() {
            var e = t(this).data("draggable"),
                i = e.options;
            e.snapElements = [], t(i.snap.constructor != String ? i.snap.items || ":data(draggable)" : i.snap).each(function() {
                var i = t(this),
                    n = i.offset();
                this != e.element[0] && e.snapElements.push({
                    item: this,
                    width: i.outerWidth(),
                    height: i.outerHeight(),
                    top: n.top,
                    left: n.left
                })
            })
        },
        drag: function(e, i) {
            for (var n = t(this).data("draggable"), s = n.options, a = s.snapTolerance, o = i.offset.left, r = o + n.helperProportions.width, l = i.offset.top, c = l + n.helperProportions.height, u = n.snapElements.length - 1; u >= 0; u--) {
                var d = n.snapElements[u].left,
                    h = d + n.snapElements[u].width,
                    p = n.snapElements[u].top,
                    f = p + n.snapElements[u].height;
                if (d - a < o && o < h + a && p - a < l && l < f + a || d - a < o && o < h + a && p - a < c && c < f + a || d - a < r && r < h + a && p - a < l && l < f + a || d - a < r && r < h + a && p - a < c && c < f + a) {
                    if ("inner" != s.snapMode) {
                        var m = Math.abs(p - c) <= a,
                            g = Math.abs(f - l) <= a,
                            _ = Math.abs(d - r) <= a,
                            v = Math.abs(h - o) <= a;
                        m && (i.position.top = n._convertPositionTo("relative", {
                            top: p - n.helperProportions.height,
                            left: 0
                        }).top - n.margins.top), g && (i.position.top = n._convertPositionTo("relative", {
                            top: f,
                            left: 0
                        }).top - n.margins.top), _ && (i.position.left = n._convertPositionTo("relative", {
                            top: 0,
                            left: d - n.helperProportions.width
                        }).left - n.margins.left), v && (i.position.left = n._convertPositionTo("relative", {
                            top: 0,
                            left: h
                        }).left - n.margins.left)
                    }
                    var b = m || g || _ || v;
                    if ("outer" != s.snapMode) {
                        var m = Math.abs(p - l) <= a,
                            g = Math.abs(f - c) <= a,
                            _ = Math.abs(d - o) <= a,
                            v = Math.abs(h - r) <= a;
                        m && (i.position.top = n._convertPositionTo("relative", {
                            top: p,
                            left: 0
                        }).top - n.margins.top), g && (i.position.top = n._convertPositionTo("relative", {
                            top: f - n.helperProportions.height,
                            left: 0
                        }).top - n.margins.top), _ && (i.position.left = n._convertPositionTo("relative", {
                            top: 0,
                            left: d
                        }).left - n.margins.left), v && (i.position.left = n._convertPositionTo("relative", {
                            top: 0,
                            left: h - n.helperProportions.width
                        }).left - n.margins.left)
                    }!n.snapElements[u].snapping && (m || g || _ || v || b) && n.options.snap.snap && n.options.snap.snap.call(n.element, e, t.extend(n._uiHash(), {
                        snapItem: n.snapElements[u].item
                    })), n.snapElements[u].snapping = m || g || _ || v || b
                } else n.snapElements[u].snapping && n.options.snap.release && n.options.snap.release.call(n.element, e, t.extend(n._uiHash(), {
                    snapItem: n.snapElements[u].item
                })), n.snapElements[u].snapping = !1
            }
        }
    }), t.ui.plugin.add("draggable", "stack", {
        start: function() {
            var e = t(this).data("draggable").options,
                i = t.makeArray(t(e.stack)).sort(function(e, i) {
                    return (parseInt(t(e).css("zIndex"), 10) || 0) - (parseInt(t(i).css("zIndex"), 10) || 0)
                });
            if (i.length) {
                var n = parseInt(i[0].style.zIndex) || 0;
                t(i).each(function(t) {
                    this.style.zIndex = n + t
                }), this[0].style.zIndex = n + i.length
            }
        }
    }), t.ui.plugin.add("draggable", "zIndex", {
        start: function(e, i) {
            var n = t(i.helper),
                s = t(this).data("draggable").options;
            n.css("zIndex") && (s._zIndex = n.css("zIndex")), n.css("zIndex", s.zIndex)
        },
        stop: function(e, i) {
            var n = t(this).data("draggable").options;
            n._zIndex && t(i.helper).css("zIndex", n._zIndex)
        }
    })
}(jQuery),
function(t) {
    t.widget("ui.droppable", {
        version: "1.9.1",
        widgetEventPrefix: "drop",
        options: {
            accept: "*",
            activeClass: !1,
            addClasses: !0,
            greedy: !1,
            hoverClass: !1,
            scope: "default",
            tolerance: "intersect"
        },
        _create: function() {
            var e = this.options,
                i = e.accept;
            this.isover = 0, this.isout = 1, this.accept = t.isFunction(i) ? i : function(t) {
                return t.is(i)
            }, this.proportions = {
                width: this.element[0].offsetWidth,
                height: this.element[0].offsetHeight
            }, t.ui.ddmanager.droppables[e.scope] = t.ui.ddmanager.droppables[e.scope] || [], t.ui.ddmanager.droppables[e.scope].push(this), e.addClasses && this.element.addClass("ui-droppable")
        },
        _destroy: function() {
            for (var e = t.ui.ddmanager.droppables[this.options.scope], i = 0; i < e.length; i++) e[i] == this && e.splice(i, 1);
            this.element.removeClass("ui-droppable ui-droppable-disabled")
        },
        _setOption: function(e, i) {
            "accept" == e && (this.accept = t.isFunction(i) ? i : function(t) {
                return t.is(i)
            }), t.Widget.prototype._setOption.apply(this, arguments)
        },
        _activate: function(e) {
            var i = t.ui.ddmanager.current;
            this.options.activeClass && this.element.addClass(this.options.activeClass), i && this._trigger("activate", e, this.ui(i))
        },
        _deactivate: function(e) {
            var i = t.ui.ddmanager.current;
            this.options.activeClass && this.element.removeClass(this.options.activeClass), i && this._trigger("deactivate", e, this.ui(i))
        },
        _over: function(e) {
            var i = t.ui.ddmanager.current;
            i && (i.currentItem || i.element)[0] != this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this.options.hoverClass && this.element.addClass(this.options.hoverClass), this._trigger("over", e, this.ui(i)))
        },
        _out: function(e) {
            var i = t.ui.ddmanager.current;
            i && (i.currentItem || i.element)[0] != this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("out", e, this.ui(i)))
        },
        _drop: function(e, i) {
            var n = i || t.ui.ddmanager.current;
            if (!n || (n.currentItem || n.element)[0] == this.element[0]) return !1;
            var s = !1;
            return this.element.find(":data(droppable)").not(".ui-draggable-dragging").each(function() {
                var e = t.data(this, "droppable");
                if (e.options.greedy && !e.options.disabled && e.options.scope == n.options.scope && e.accept.call(e.element[0], n.currentItem || n.element) && t.ui.intersect(n, t.extend(e, {
                        offset: e.element.offset()
                    }), e.options.tolerance)) return s = !0, !1
            }), !s && (!!this.accept.call(this.element[0], n.currentItem || n.element) && (this.options.activeClass && this.element.removeClass(this.options.activeClass), this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("drop", e, this.ui(n)), this.element))
        },
        ui: function(t) {
            return {
                draggable: t.currentItem || t.element,
                helper: t.helper,
                position: t.position,
                offset: t.positionAbs
            }
        }
    }), t.ui.intersect = function(e, i, n) {
        if (!i.offset) return !1;
        var s = (e.positionAbs || e.position.absolute).left,
            a = s + e.helperProportions.width,
            o = (e.positionAbs || e.position.absolute).top,
            r = o + e.helperProportions.height,
            l = i.offset.left,
            c = l + i.proportions.width,
            u = i.offset.top,
            d = u + i.proportions.height;
        switch (n) {
            case "fit":
                return l <= s && a <= c && u <= o && r <= d;
            case "intersect":
                return l < s + e.helperProportions.width / 2 && a - e.helperProportions.width / 2 < c && u < o + e.helperProportions.height / 2 && r - e.helperProportions.height / 2 < d;
            case "pointer":
                var h = (e.positionAbs || e.position.absolute).left + (e.clickOffset || e.offset.click).left,
                    p = (e.positionAbs || e.position.absolute).top + (e.clickOffset || e.offset.click).top;
                return t.ui.isOver(p, h, u, l, i.proportions.height, i.proportions.width);
            case "touch":
                return (o >= u && o <= d || r >= u && r <= d || o < u && r > d) && (s >= l && s <= c || a >= l && a <= c || s < l && a > c);
            default:
                return !1
        }
    }, t.ui.ddmanager = {
        current: null,
        droppables: {
            "default": []
        },
        prepareOffsets: function(e, i) {
            var n = t.ui.ddmanager.droppables[e.options.scope] || [],
                s = i ? i.type : null,
                a = (e.currentItem || e.element).find(":data(droppable)").andSelf();
            t: for (var o = 0; o < n.length; o++)
                if (!(n[o].options.disabled || e && !n[o].accept.call(n[o].element[0], e.currentItem || e.element))) {
                    for (var r = 0; r < a.length; r++)
                        if (a[r] == n[o].element[0]) {
                            n[o].proportions.height = 0;
                            continue t
                        }
                    n[o].visible = "none" != n[o].element.css("display"), n[o].visible && ("mousedown" == s && n[o]._activate.call(n[o], i), n[o].offset = n[o].element.offset(), n[o].proportions = {
                        width: n[o].element[0].offsetWidth,
                        height: n[o].element[0].offsetHeight
                    })
                }
        },
        drop: function(e, i) {
            var n = !1;
            return t.each(t.ui.ddmanager.droppables[e.options.scope] || [], function() {
                this.options && (!this.options.disabled && this.visible && t.ui.intersect(e, this, this.options.tolerance) && (n = this._drop.call(this, i) || n), !this.options.disabled && this.visible && this.accept.call(this.element[0], e.currentItem || e.element) && (this.isout = 1, this.isover = 0, this._deactivate.call(this, i)))
            }), n
        },
        dragStart: function(e, i) {
            e.element.parentsUntil("body").bind("scroll.droppable", function() {
                e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, i)
            })
        },
        drag: function(e, i) {
            e.options.refreshPositions && t.ui.ddmanager.prepareOffsets(e, i), t.each(t.ui.ddmanager.droppables[e.options.scope] || [], function() {
                if (!this.options.disabled && !this.greedyChild && this.visible) {
                    var n = t.ui.intersect(e, this, this.options.tolerance),
                        s = n || 1 != this.isover ? n && 0 == this.isover ? "isover" : null : "isout";
                    if (s) {
                        var a;
                        if (this.options.greedy) {
                            var o = this.options.scope,
                                r = this.element.parents(":data(droppable)").filter(function() {
                                    return t.data(this, "droppable").options.scope === o
                                });
                            r.length && (a = t.data(r[0], "droppable"), a.greedyChild = "isover" == s ? 1 : 0)
                        }
                        a && "isover" == s && (a.isover = 0, a.isout = 1, a._out.call(a, i)), this[s] = 1, this["isout" == s ? "isover" : "isout"] = 0, this["isover" == s ? "_over" : "_out"].call(this, i), a && "isout" == s && (a.isout = 0, a.isover = 1, a._over.call(a, i))
                    }
                }
            })
        },
        dragStop: function(e, i) {
            e.element.parentsUntil("body").unbind("scroll.droppable"), e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, i)
        }
    }
}(jQuery),
function(t) {
    t.widget("ui.resizable", t.ui.mouse, {
        version: "1.9.1",
        widgetEventPrefix: "resize",
        options: {
            alsoResize: !1,
            animate: !1,
            animateDuration: "slow",
            animateEasing: "swing",
            aspectRatio: !1,
            autoHide: !1,
            containment: !1,
            ghost: !1,
            grid: !1,
            handles: "e,s,se",
            helper: !1,
            maxHeight: null,
            maxWidth: null,
            minHeight: 10,
            minWidth: 10,
            zIndex: 1e3
        },
        _create: function() {
            var e = this,
                i = this.options;
            if (this.element.addClass("ui-resizable"), t.extend(this, {
                    _aspectRatio: !!i.aspectRatio,
                    aspectRatio: i.aspectRatio,
                    originalElement: this.element,
                    _proportionallyResizeElements: [],
                    _helper: i.helper || i.ghost || i.animate ? i.helper || "ui-resizable-helper" : null
                }), this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i) && (this.element.wrap(t('<div class="ui-wrapper" style="overflow: hidden;"></div>').css({
                    position: this.element.css("position"),
                    width: this.element.outerWidth(),
                    height: this.element.outerHeight(),
                    top: this.element.css("top"),
                    left: this.element.css("left")
                })), this.element = this.element.parent().data("resizable", this.element.data("resizable")), this.elementIsWrapper = !0, this.element.css({
                    marginLeft: this.originalElement.css("marginLeft"),
                    marginTop: this.originalElement.css("marginTop"),
                    marginRight: this.originalElement.css("marginRight"),
                    marginBottom: this.originalElement.css("marginBottom")
                }), this.originalElement.css({
                    marginLeft: 0,
                    marginTop: 0,
                    marginRight: 0,
                    marginBottom: 0
                }), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
                    position: "static",
                    zoom: 1,
                    display: "block"
                })), this.originalElement.css({
                    margin: this.originalElement.css("margin")
                }), this._proportionallyResize()), this.handles = i.handles || (t(".ui-resizable-handle", this.element).length ? {
                    n: ".ui-resizable-n",
                    e: ".ui-resizable-e",
                    s: ".ui-resizable-s",
                    w: ".ui-resizable-w",
                    se: ".ui-resizable-se",
                    sw: ".ui-resizable-sw",
                    ne: ".ui-resizable-ne",
                    nw: ".ui-resizable-nw"
                } : "e,s,se"), this.handles.constructor == String) {
                "all" == this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw");
                var n = this.handles.split(",");
                this.handles = {};
                for (var s = 0; s < n.length; s++) {
                    var a = t.trim(n[s]),
                        o = "ui-resizable-" + a,
                        r = t('<div class="ui-resizable-handle ' + o + '"></div>');
                    r.css({
                        zIndex: i.zIndex
                    }), "se" == a && r.addClass("ui-icon ui-icon-gripsmall-diagonal-se"), this.handles[a] = ".ui-resizable-" + a, this.element.append(r)
                }
            }
            this._renderAxis = function(e) {
                e = e || this.element;
                for (var i in this.handles) {
                    if (this.handles[i].constructor == String && (this.handles[i] = t(this.handles[i], this.element).show()), this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i)) {
                        var n = t(this.handles[i], this.element),
                            s = 0;
                        s = /sw|ne|nw|se|n|s/.test(i) ? n.outerHeight() : n.outerWidth();
                        var a = ["padding", /ne|nw|n/.test(i) ? "Top" : /se|sw|s/.test(i) ? "Bottom" : /^e$/.test(i) ? "Right" : "Left"].join("");
                        e.css(a, s), this._proportionallyResize()
                    }
                    t(this.handles[i]).length
                }
            }, this._renderAxis(this.element), this._handles = t(".ui-resizable-handle", this.element).disableSelection(), this._handles.mouseover(function() {
                if (!e.resizing) {
                    if (this.className) var t = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i);
                    e.axis = t && t[1] ? t[1] : "se"
                }
            }), i.autoHide && (this._handles.hide(), t(this.element).addClass("ui-resizable-autohide").mouseenter(function() {
                i.disabled || (t(this).removeClass("ui-resizable-autohide"), e._handles.show())
            }).mouseleave(function() {
                i.disabled || e.resizing || (t(this).addClass("ui-resizable-autohide"), e._handles.hide())
            })), this._mouseInit()
        },
        _destroy: function() {
            this._mouseDestroy();
            var e = function(e) {
                t(e).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()
            };
            if (this.elementIsWrapper) {
                e(this.element);
                var i = this.element;
                this.originalElement.css({
                    position: i.css("position"),
                    width: i.outerWidth(),
                    height: i.outerHeight(),
                    top: i.css("top"),
                    left: i.css("left")
                }).insertAfter(i), i.remove()
            }
            return this.originalElement.css("resize", this.originalResizeStyle), e(this.originalElement), this
        },
        _mouseCapture: function(e) {
            var i = !1;
            for (var n in this.handles) t(this.handles[n])[0] == e.target && (i = !0);
            return !this.options.disabled && i
        },
        _mouseStart: function(i) {
            var n = this.options,
                s = this.element.position(),
                a = this.element;
            this.resizing = !0, this.documentScroll = {
                top: t(document).scrollTop(),
                left: t(document).scrollLeft()
            }, (a.is(".ui-draggable") || /absolute/.test(a.css("position"))) && a.css({
                position: "absolute",
                top: s.top,
                left: s.left
            }), this._renderProxy();
            var o = e(this.helper.css("left")),
                r = e(this.helper.css("top"));
            n.containment && (o += t(n.containment).scrollLeft() || 0, r += t(n.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
                left: o,
                top: r
            }, this.size = this._helper ? {
                width: a.outerWidth(),
                height: a.outerHeight()
            } : {
                width: a.width(),
                height: a.height()
            }, this.originalSize = this._helper ? {
                width: a.outerWidth(),
                height: a.outerHeight()
            } : {
                width: a.width(),
                height: a.height()
            }, this.originalPosition = {
                left: o,
                top: r
            }, this.sizeDiff = {
                width: a.outerWidth() - a.width(),
                height: a.outerHeight() - a.height()
            }, this.originalMousePosition = {
                left: i.pageX,
                top: i.pageY
            }, this.aspectRatio = "number" == typeof n.aspectRatio ? n.aspectRatio : this.originalSize.width / this.originalSize.height || 1;
            var l = t(".ui-resizable-" + this.axis).css("cursor");
            return t("body").css("cursor", "auto" == l ? this.axis + "-resize" : l), a.addClass("ui-resizable-resizing"), this._propagate("start", i), !0
        },
        _mouseDrag: function(t) {
            var e = this.helper,
                i = (this.options, this.originalMousePosition),
                n = this.axis,
                s = t.pageX - i.left || 0,
                a = t.pageY - i.top || 0,
                o = this._change[n];
            if (!o) return !1;
            var r = o.apply(this, [t, s, a]);
            return this._updateVirtualBoundaries(t.shiftKey), (this._aspectRatio || t.shiftKey) && (r = this._updateRatio(r, t)), r = this._respectSize(r, t), this._propagate("resize", t), e.css({
                top: this.position.top + "px",
                left: this.position.left + "px",
                width: this.size.width + "px",
                height: this.size.height + "px"
            }), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), this._updateCache(r), this._trigger("resize", t, this.ui()), !1
        },
        _mouseStop: function(e) {
            this.resizing = !1;
            var i = this.options,
                n = this;
            if (this._helper) {
                var s = this._proportionallyResizeElements,
                    a = s.length && /textarea/i.test(s[0].nodeName),
                    o = a && t.ui.hasScroll(s[0], "left") ? 0 : n.sizeDiff.height,
                    r = a ? 0 : n.sizeDiff.width,
                    l = {
                        width: n.helper.width() - r,
                        height: n.helper.height() - o
                    },
                    c = parseInt(n.element.css("left"), 10) + (n.position.left - n.originalPosition.left) || null,
                    u = parseInt(n.element.css("top"), 10) + (n.position.top - n.originalPosition.top) || null;
                i.animate || this.element.css(t.extend(l, {
                    top: u,
                    left: c
                })), n.helper.height(n.size.height), n.helper.width(n.size.width), this._helper && !i.animate && this._proportionallyResize()
            }
            return t("body").css("cursor", "auto"), this.element.removeClass("ui-resizable-resizing"), this._propagate("stop", e), this._helper && this.helper.remove(), !1
        },
        _updateVirtualBoundaries: function(t) {
            var e, n, s, a, o, r = this.options;
            o = {
                minWidth: i(r.minWidth) ? r.minWidth : 0,
                maxWidth: i(r.maxWidth) ? r.maxWidth : Infinity,
                minHeight: i(r.minHeight) ? r.minHeight : 0,
                maxHeight: i(r.maxHeight) ? r.maxHeight : Infinity
            }, (this._aspectRatio || t) && (e = o.minHeight * this.aspectRatio, s = o.minWidth / this.aspectRatio, n = o.maxHeight * this.aspectRatio, a = o.maxWidth / this.aspectRatio, e > o.minWidth && (o.minWidth = e), s > o.minHeight && (o.minHeight = s), n < o.maxWidth && (o.maxWidth = n), a < o.maxHeight && (o.maxHeight = a)), this._vBoundaries = o
        },
        _updateCache: function(t) {
            this.options;
            this.offset = this.helper.offset(), i(t.left) && (this.position.left = t.left), i(t.top) && (this.position.top = t.top), i(t.height) && (this.size.height = t.height), i(t.width) && (this.size.width = t.width)
        },
        _updateRatio: function(t) {
            var e = (this.options, this.position),
                n = this.size,
                s = this.axis;
            return i(t.height) ? t.width = t.height * this.aspectRatio : i(t.width) && (t.height = t.width / this.aspectRatio), "sw" == s && (t.left = e.left + (n.width - t.width), t.top = null), "nw" == s && (t.top = e.top + (n.height - t.height), t.left = e.left + (n.width - t.width)), t
        },
        _respectSize: function(t, e) {
            var n = (this.helper, this._vBoundaries),
                s = (this._aspectRatio || e.shiftKey, this.axis),
                a = i(t.width) && n.maxWidth && n.maxWidth < t.width,
                o = i(t.height) && n.maxHeight && n.maxHeight < t.height,
                r = i(t.width) && n.minWidth && n.minWidth > t.width,
                l = i(t.height) && n.minHeight && n.minHeight > t.height;
            r && (t.width = n.minWidth), l && (t.height = n.minHeight), a && (t.width = n.maxWidth), o && (t.height = n.maxHeight);
            var c = this.originalPosition.left + this.originalSize.width,
                u = this.position.top + this.size.height,
                d = /sw|nw|w/.test(s),
                h = /nw|ne|n/.test(s);
            r && d && (t.left = c - n.minWidth), a && d && (t.left = c - n.maxWidth), l && h && (t.top = u - n.minHeight), o && h && (t.top = u - n.maxHeight);
            var p = !t.width && !t.height;
            return p && !t.left && t.top ? t.top = null : p && !t.top && t.left && (t.left = null), t
        },
        _proportionallyResize: function() {
            this.options;
            if (this._proportionallyResizeElements.length)
                for (var e = this.helper || this.element, i = 0; i < this._proportionallyResizeElements.length; i++) {
                    var n = this._proportionallyResizeElements[i];
                    if (!this.borderDif) {
                        var s = [n.css("borderTopWidth"), n.css("borderRightWidth"), n.css("borderBottomWidth"), n.css("borderLeftWidth")],
                            a = [n.css("paddingTop"), n.css("paddingRight"), n.css("paddingBottom"), n.css("paddingLeft")];
                        this.borderDif = t.map(s, function(t, e) {
                            return (parseInt(t, 10) || 0) + (parseInt(a[e], 10) || 0)
                        })
                    }
                    n.css({
                        height: e.height() - this.borderDif[0] - this.borderDif[2] || 0,
                        width: e.width() - this.borderDif[1] - this.borderDif[3] || 0
                    })
                }
        },
        _renderProxy: function() {
            var e = this.element,
                i = this.options;
            if (this.elementOffset = e.offset(), this._helper) {
                this.helper = this.helper || t('<div style="overflow:hidden;"></div>');
                var n = t.ui.ie6 ? 1 : 0,
                    s = t.ui.ie6 ? 2 : -1;
                this.helper.addClass(this._helper).css({
                    width: this.element.outerWidth() + s,
                    height: this.element.outerHeight() + s,
                    position: "absolute",
                    left: this.elementOffset.left - n + "px",
                    top: this.elementOffset.top - n + "px",
                    zIndex: ++i.zIndex
                }), this.helper.appendTo("body").disableSelection()
            } else this.helper = this.element
        },
        _change: {
            e: function(t, e) {
                return {
                    width: this.originalSize.width + e
                }
            },
            w: function(t, e) {
                var i = (this.options, this.originalSize);
                return {
                    left: this.originalPosition.left + e,
                    width: i.width - e
                }
            },
            n: function(t, e, i) {
                var n = (this.options, this.originalSize);
                return {
                    top: this.originalPosition.top + i,
                    height: n.height - i
                }
            },
            s: function(t, e, i) {
                return {
                    height: this.originalSize.height + i
                }
            },
            se: function(e, i, n) {
                return t.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [e, i, n]))
            },
            sw: function(e, i, n) {
                return t.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [e, i, n]))
            },
            ne: function(e, i, n) {
                return t.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [e, i, n]))
            },
            nw: function(e, i, n) {
                return t.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [e, i, n]))
            }
        },
        _propagate: function(e, i) {
            t.ui.plugin.call(this, e, [i, this.ui()]), "resize" != e && this._trigger(e, i, this.ui())
        },
        plugins: {},
        ui: function() {
            return {
                originalElement: this.originalElement,
                element: this.element,
                helper: this.helper,
                position: this.position,
                size: this.size,
                originalSize: this.originalSize,
                originalPosition: this.originalPosition
            }
        }
    }), t.ui.plugin.add("resizable", "alsoResize", {
        start: function() {
            var e = t(this).data("resizable"),
                i = e.options,
                n = function(e) {
                    t(e).each(function() {
                        var e = t(this);
                        e.data("resizable-alsoresize", {
                            width: parseInt(e.width(), 10),
                            height: parseInt(e.height(), 10),
                            left: parseInt(e.css("left"), 10),
                            top: parseInt(e.css("top"), 10)
                        })
                    })
                };
            "object" != typeof i.alsoResize || i.alsoResize.parentNode ? n(i.alsoResize) : i.alsoResize.length ? (i.alsoResize = i.alsoResize[0], n(i.alsoResize)) : t.each(i.alsoResize, function(t) {
                n(t)
            })
        },
        resize: function(e, i) {
            var n = t(this).data("resizable"),
                s = n.options,
                a = n.originalSize,
                o = n.originalPosition,
                r = {
                    height: n.size.height - a.height || 0,
                    width: n.size.width - a.width || 0,
                    top: n.position.top - o.top || 0,
                    left: n.position.left - o.left || 0
                },
                l = function(e, n) {
                    t(e).each(function() {
                        var e = t(this),
                            s = t(this).data("resizable-alsoresize"),
                            a = {},
                            o = n && n.length ? n : e.parents(i.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
                        t.each(o, function(t, e) {
                            var i = (s[e] || 0) + (r[e] || 0);
                            i && i >= 0 && (a[e] = i || null)
                        }), e.css(a)
                    })
                };
            "object" != typeof s.alsoResize || s.alsoResize.nodeType ? l(s.alsoResize) : t.each(s.alsoResize, function(t, e) {
                l(t, e)
            })
        },
        stop: function() {
            t(this).removeData("resizable-alsoresize")
        }
    }), t.ui.plugin.add("resizable", "animate", {
        stop: function(e) {
            var i = t(this).data("resizable"),
                n = i.options,
                s = i._proportionallyResizeElements,
                a = s.length && /textarea/i.test(s[0].nodeName),
                o = a && t.ui.hasScroll(s[0], "left") ? 0 : i.sizeDiff.height,
                r = a ? 0 : i.sizeDiff.width,
                l = {
                    width: i.size.width - r,
                    height: i.size.height - o
                },
                c = parseInt(i.element.css("left"), 10) + (i.position.left - i.originalPosition.left) || null,
                u = parseInt(i.element.css("top"), 10) + (i.position.top - i.originalPosition.top) || null;
            i.element.animate(t.extend(l, u && c ? {
                top: u,
                left: c
            } : {}), {
                duration: n.animateDuration,
                easing: n.animateEasing,
                step: function() {
                    var n = {
                        width: parseInt(i.element.css("width"), 10),
                        height: parseInt(i.element.css("height"), 10),
                        top: parseInt(i.element.css("top"), 10),
                        left: parseInt(i.element.css("left"), 10)
                    };
                    s && s.length && t(s[0]).css({
                        width: n.width,
                        height: n.height
                    }), i._updateCache(n), i._propagate("resize", e)
                }
            })
        }
    }), t.ui.plugin.add("resizable", "containment", {
        start: function() {
            var i = t(this).data("resizable"),
                n = i.options,
                s = i.element,
                a = n.containment,
                o = a instanceof t ? a.get(0) : /parent/.test(a) ? s.parent().get(0) : a;
            if (o)
                if (i.containerElement = t(o), /document/.test(a) || a == document) i.containerOffset = {
                    left: 0,
                    top: 0
                }, i.containerPosition = {
                    left: 0,
                    top: 0
                }, i.parentData = {
                    element: t(document),
                    left: 0,
                    top: 0,
                    width: t(document).width(),
                    height: t(document).height() || document.body.parentNode.scrollHeight
                };
                else {
                    var r = t(o),
                        l = [];
                    t(["Top", "Right", "Left", "Bottom"]).each(function(t, i) {
                        l[t] = e(r.css("padding" + i))
                    }), i.containerOffset = r.offset(), i.containerPosition = r.position(), i.containerSize = {
                        height: r.innerHeight() - l[3],
                        width: r.innerWidth() - l[1]
                    };
                    var c = i.containerOffset,
                        u = i.containerSize.height,
                        d = i.containerSize.width,
                        h = t.ui.hasScroll(o, "left") ? o.scrollWidth : d,
                        p = t.ui.hasScroll(o) ? o.scrollHeight : u;
                    i.parentData = {
                        element: o,
                        left: c.left,
                        top: c.top,
                        width: h,
                        height: p
                    }
                }
        },
        resize: function(e) {
            var i = t(this).data("resizable"),
                n = i.options,
                s = (i.containerSize, i.containerOffset),
                a = (i.size, i.position),
                o = i._aspectRatio || e.shiftKey,
                r = {
                    top: 0,
                    left: 0
                },
                l = i.containerElement;
            l[0] != document && /static/.test(l.css("position")) && (r = s), a.left < (i._helper ? s.left : 0) && (i.size.width = i.size.width + (i._helper ? i.position.left - s.left : i.position.left - r.left), o && (i.size.height = i.size.width / i.aspectRatio), i.position.left = n.helper ? s.left : 0), a.top < (i._helper ? s.top : 0) && (i.size.height = i.size.height + (i._helper ? i.position.top - s.top : i.position.top), o && (i.size.width = i.size.height * i.aspectRatio), i.position.top = i._helper ? s.top : 0), i.offset.left = i.parentData.left + i.position.left, i.offset.top = i.parentData.top + i.position.top;
            var c = Math.abs((i._helper, i.offset.left - r.left + i.sizeDiff.width)),
                u = Math.abs((i._helper ? i.offset.top - r.top : i.offset.top - s.top) + i.sizeDiff.height),
                d = i.containerElement.get(0) == i.element.parent().get(0),
                h = /relative|absolute/.test(i.containerElement.css("position"));
            d && h && (c -= i.parentData.left), c + i.size.width >= i.parentData.width && (i.size.width = i.parentData.width - c, o && (i.size.height = i.size.width / i.aspectRatio)), u + i.size.height >= i.parentData.height && (i.size.height = i.parentData.height - u, o && (i.size.width = i.size.height * i.aspectRatio))
        },
        stop: function() {
            var e = t(this).data("resizable"),
                i = e.options,
                n = (e.position, e.containerOffset),
                s = e.containerPosition,
                a = e.containerElement,
                o = t(e.helper),
                r = o.offset(),
                l = o.outerWidth() - e.sizeDiff.width,
                c = o.outerHeight() - e.sizeDiff.height;
            e._helper && !i.animate && /relative/.test(a.css("position")) && t(this).css({
                left: r.left - s.left - n.left,
                width: l,
                height: c
            }), e._helper && !i.animate && /static/.test(a.css("position")) && t(this).css({
                left: r.left - s.left - n.left,
                width: l,
                height: c
            })
        }
    }), t.ui.plugin.add("resizable", "ghost", {
        start: function() {
            var e = t(this).data("resizable"),
                i = e.options,
                n = e.size;
            e.ghost = e.originalElement.clone(), e.ghost.css({
                opacity: .25,
                display: "block",
                position: "relative",
                height: n.height,
                width: n.width,
                margin: 0,
                left: 0,
                top: 0
            }).addClass("ui-resizable-ghost").addClass("string" == typeof i.ghost ? i.ghost : ""), e.ghost.appendTo(e.helper)
        },
        resize: function() {
            var e = t(this).data("resizable");
            e.options;
            e.ghost && e.ghost.css({
                position: "relative",
                height: e.size.height,
                width: e.size.width
            })
        },
        stop: function() {
            var e = t(this).data("resizable");
            e.options;
            e.ghost && e.helper && e.helper.get(0).removeChild(e.ghost.get(0))
        }
    }), t.ui.plugin.add("resizable", "grid", {
        resize: function(e) {
            var i = t(this).data("resizable"),
                n = i.options,
                s = i.size,
                a = i.originalSize,
                o = i.originalPosition,
                r = i.axis;
            n._aspectRatio || e.shiftKey;
            n.grid = "number" == typeof n.grid ? [n.grid, n.grid] : n.grid;
            var l = Math.round((s.width - a.width) / (n.grid[0] || 1)) * (n.grid[0] || 1),
                c = Math.round((s.height - a.height) / (n.grid[1] || 1)) * (n.grid[1] || 1);
            /^(se|s|e)$/.test(r) ? (i.size.width = a.width + l, i.size.height = a.height + c) : /^(ne)$/.test(r) ? (i.size.width = a.width + l, i.size.height = a.height + c, i.position.top = o.top - c) : /^(sw)$/.test(r) ? (i.size.width = a.width + l, i.size.height = a.height + c, i.position.left = o.left - l) : (i.size.width = a.width + l, i.size.height = a.height + c, i.position.top = o.top - c, i.position.left = o.left - l)
        }
    });
    var e = function(t) {
            return parseInt(t, 10) || 0
        },
        i = function(t) {
            return !isNaN(parseInt(t, 10))
        }
}(jQuery),
function(t) {
    t.widget("ui.selectable", t.ui.mouse, {
        version: "1.9.1",
        options: {
            appendTo: "body",
            autoRefresh: !0,
            distance: 0,
            filter: "*",
            tolerance: "touch"
        },
        _create: function() {
            var e = this;
            this.element.addClass("ui-selectable"), this.dragged = !1;
            var i;
            this.refresh = function() {
                i = t(e.options.filter, e.element[0]), i.addClass("ui-selectee"), i.each(function() {
                    var e = t(this),
                        i = e.offset();
                    t.data(this, "selectable-item", {
                        element: this,
                        $element: e,
                        left: i.left,
                        top: i.top,
                        right: i.left + e.outerWidth(),
                        bottom: i.top + e.outerHeight(),
                        startselected: !1,
                        selected: e.hasClass("ui-selected"),
                        selecting: e.hasClass("ui-selecting"),
                        unselecting: e.hasClass("ui-unselecting")
                    })
                })
            }, this.refresh(), this.selectees = i.addClass("ui-selectee"), this._mouseInit(), this.helper = t("<div class='ui-selectable-helper'></div>")
        },
        _destroy: function() {
            this.selectees.removeClass("ui-selectee").removeData("selectable-item"), this.element.removeClass("ui-selectable ui-selectable-disabled"), this._mouseDestroy()
        },
        _mouseStart: function(e) {
            var i = this;
            if (this.opos = [e.pageX, e.pageY], !this.options.disabled) {
                var n = this.options;
                this.selectees = t(n.filter, this.element[0]), this._trigger("start", e), t(n.appendTo).append(this.helper), this.helper.css({
                        left: e.clientX,
                        top: e.clientY,
                        width: 0,
                        height: 0
                    }),
                    n.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function() {
                        var n = t.data(this, "selectable-item");
                        n.startselected = !0, e.metaKey || e.ctrlKey || (n.$element.removeClass("ui-selected"), n.selected = !1, n.$element.addClass("ui-unselecting"), n.unselecting = !0, i._trigger("unselecting", e, {
                            unselecting: n.element
                        }))
                    }), t(e.target).parents().andSelf().each(function() {
                        var n = t.data(this, "selectable-item");
                        if (n) {
                            var s = !e.metaKey && !e.ctrlKey || !n.$element.hasClass("ui-selected");
                            return n.$element.removeClass(s ? "ui-unselecting" : "ui-selected").addClass(s ? "ui-selecting" : "ui-unselecting"), n.unselecting = !s, n.selecting = s, n.selected = s, s ? i._trigger("selecting", e, {
                                selecting: n.element
                            }) : i._trigger("unselecting", e, {
                                unselecting: n.element
                            }), !1
                        }
                    })
            }
        },
        _mouseDrag: function(e) {
            var i = this;
            if (this.dragged = !0, !this.options.disabled) {
                var n = this.options,
                    s = this.opos[0],
                    a = this.opos[1],
                    o = e.pageX,
                    r = e.pageY;
                if (s > o) {
                    var l = o;
                    o = s, s = l
                }
                if (a > r) {
                    var l = r;
                    r = a, a = l
                }
                return this.helper.css({
                    left: s,
                    top: a,
                    width: o - s,
                    height: r - a
                }), this.selectees.each(function() {
                    var l = t.data(this, "selectable-item");
                    if (l && l.element != i.element[0]) {
                        var c = !1;
                        "touch" == n.tolerance ? c = !(l.left > o || l.right < s || l.top > r || l.bottom < a) : "fit" == n.tolerance && (c = l.left > s && l.right < o && l.top > a && l.bottom < r), c ? (l.selected && (l.$element.removeClass("ui-selected"), l.selected = !1), l.unselecting && (l.$element.removeClass("ui-unselecting"), l.unselecting = !1), l.selecting || (l.$element.addClass("ui-selecting"), l.selecting = !0, i._trigger("selecting", e, {
                            selecting: l.element
                        }))) : (l.selecting && ((e.metaKey || e.ctrlKey) && l.startselected ? (l.$element.removeClass("ui-selecting"), l.selecting = !1, l.$element.addClass("ui-selected"), l.selected = !0) : (l.$element.removeClass("ui-selecting"), l.selecting = !1, l.startselected && (l.$element.addClass("ui-unselecting"), l.unselecting = !0), i._trigger("unselecting", e, {
                            unselecting: l.element
                        }))), l.selected && (e.metaKey || e.ctrlKey || l.startselected || (l.$element.removeClass("ui-selected"), l.selected = !1, l.$element.addClass("ui-unselecting"), l.unselecting = !0, i._trigger("unselecting", e, {
                            unselecting: l.element
                        }))))
                    }
                }), !1
            }
        },
        _mouseStop: function(e) {
            var i = this;
            this.dragged = !1;
            this.options;
            return t(".ui-unselecting", this.element[0]).each(function() {
                var n = t.data(this, "selectable-item");
                n.$element.removeClass("ui-unselecting"), n.unselecting = !1, n.startselected = !1, i._trigger("unselected", e, {
                    unselected: n.element
                })
            }), t(".ui-selecting", this.element[0]).each(function() {
                var n = t.data(this, "selectable-item");
                n.$element.removeClass("ui-selecting").addClass("ui-selected"), n.selecting = !1, n.selected = !0, n.startselected = !0, i._trigger("selected", e, {
                    selected: n.element
                })
            }), this._trigger("stop", e), this.helper.remove(), !1
        }
    })
}(jQuery),
function(t) {
    t.widget("ui.sortable", t.ui.mouse, {
        version: "1.9.1",
        widgetEventPrefix: "sort",
        ready: !1,
        options: {
            appendTo: "parent",
            axis: !1,
            connectWith: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            dropOnEmpty: !0,
            forcePlaceholderSize: !1,
            forceHelperSize: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            items: "> *",
            opacity: !1,
            placeholder: !1,
            revert: !1,
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            scope: "default",
            tolerance: "intersect",
            zIndex: 1e3
        },
        _create: function() {
            var t = this.options;
            this.containerCache = {}, this.element.addClass("ui-sortable"), this.refresh(), this.floating = !!this.items.length && ("x" === t.axis || /left|right/.test(this.items[0].item.css("float")) || /inline|table-cell/.test(this.items[0].item.css("display"))), this.offset = this.element.offset(), this._mouseInit(), this.ready = !0
        },
        _destroy: function() {
            this.element.removeClass("ui-sortable ui-sortable-disabled"), this._mouseDestroy();
            for (var t = this.items.length - 1; t >= 0; t--) this.items[t].item.removeData(this.widgetName + "-item");
            return this
        },
        _setOption: function(e, i) {
            "disabled" === e ? (this.options[e] = i, this.widget().toggleClass("ui-sortable-disabled", !!i)) : t.Widget.prototype._setOption.apply(this, arguments)
        },
        _mouseCapture: function(e, i) {
            var n = this;
            if (this.reverting) return !1;
            if (this.options.disabled || "static" == this.options.type) return !1;
            this._refreshItems(e);
            var s = null;
            t(e.target).parents().each(function() {
                if (t.data(this, n.widgetName + "-item") == n) return s = t(this), !1
            });
            if (t.data(e.target, n.widgetName + "-item") == n && (s = t(e.target)), !s) return !1;
            if (this.options.handle && !i) {
                var a = !1;
                if (t(this.options.handle, s).find("*").andSelf().each(function() {
                        this == e.target && (a = !0)
                    }), !a) return !1
            }
            return this.currentItem = s, this._removeCurrentsFromItems(), !0
        },
        _mouseStart: function(e, i, n) {
            var s = this.options;
            if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(e), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), this.offset = this.currentItem.offset(), this.offset = {
                    top: this.offset.top - this.margins.top,
                    left: this.offset.left - this.margins.left
                }, t.extend(this.offset, {
                    click: {
                        left: e.pageX - this.offset.left,
                        top: e.pageY - this.offset.top
                    },
                    parent: this._getParentOffset(),
                    relative: this._getRelativeOffset()
                }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(e), this.originalPageX = e.pageX, this.originalPageY = e.pageY, s.cursorAt && this._adjustOffsetFromHelper(s.cursorAt), this.domPosition = {
                    prev: this.currentItem.prev()[0],
                    parent: this.currentItem.parent()[0]
                }, this.helper[0] != this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), s.containment && this._setContainment(), s.cursor && (t("body").css("cursor") && (this._storedCursor = t("body").css("cursor")), t("body").css("cursor", s.cursor)), s.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), this.helper.css("opacity", s.opacity)), s.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", s.zIndex)), this.scrollParent[0] != document && "HTML" != this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", e, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !n)
                for (var a = this.containers.length - 1; a >= 0; a--) this.containers[a]._trigger("activate", e, this._uiHash(this));
            return t.ui.ddmanager && (t.ui.ddmanager.current = this), t.ui.ddmanager && !s.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this.dragging = !0, this.helper.addClass("ui-sortable-helper"), this._mouseDrag(e), !0
        },
        _mouseDrag: function(e) {
            if (this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll) {
                var i = this.options,
                    n = !1;
                this.scrollParent[0] != document && "HTML" != this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - e.pageY < i.scrollSensitivity ? this.scrollParent[0].scrollTop = n = this.scrollParent[0].scrollTop + i.scrollSpeed : e.pageY - this.overflowOffset.top < i.scrollSensitivity && (this.scrollParent[0].scrollTop = n = this.scrollParent[0].scrollTop - i.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - e.pageX < i.scrollSensitivity ? this.scrollParent[0].scrollLeft = n = this.scrollParent[0].scrollLeft + i.scrollSpeed : e.pageX - this.overflowOffset.left < i.scrollSensitivity && (this.scrollParent[0].scrollLeft = n = this.scrollParent[0].scrollLeft - i.scrollSpeed)) : (e.pageY - t(document).scrollTop() < i.scrollSensitivity ? n = t(document).scrollTop(t(document).scrollTop() - i.scrollSpeed) : t(window).height() - (e.pageY - t(document).scrollTop()) < i.scrollSensitivity && (n = t(document).scrollTop(t(document).scrollTop() + i.scrollSpeed)), e.pageX - t(document).scrollLeft() < i.scrollSensitivity ? n = t(document).scrollLeft(t(document).scrollLeft() - i.scrollSpeed) : t(window).width() - (e.pageX - t(document).scrollLeft()) < i.scrollSensitivity && (n = t(document).scrollLeft(t(document).scrollLeft() + i.scrollSpeed))), !1 !== n && t.ui.ddmanager && !i.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e)
            }
            this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" == this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" == this.options.axis || (this.helper[0].style.top = this.position.top + "px");
            for (var s = this.items.length - 1; s >= 0; s--) {
                var a = this.items[s],
                    o = a.item[0],
                    r = this._intersectsWithPointer(a);
                if (r && (a.instance === this.currentContainer && !(o == this.currentItem[0] || this.placeholder[1 == r ? "next" : "prev"]()[0] == o || t.contains(this.placeholder[0], o) || "semi-dynamic" == this.options.type && t.contains(this.element[0], o)))) {
                    if (this.direction = 1 == r ? "down" : "up", "pointer" != this.options.tolerance && !this._intersectsWithSides(a)) break;
                    this._rearrange(e, a), this._trigger("change", e, this._uiHash());
                    break
                }
            }
            return this._contactContainers(e), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), this._trigger("sort", e, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1
        },
        _mouseStop: function(e, i) {
            if (e) {
                if (t.ui.ddmanager && !this.options.dropBehaviour && t.ui.ddmanager.drop(this, e), this.options.revert) {
                    var n = this,
                        s = this.placeholder.offset();
                    this.reverting = !0, t(this.helper).animate({
                        left: s.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] == document.body ? 0 : this.offsetParent[0].scrollLeft),
                        top: s.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] == document.body ? 0 : this.offsetParent[0].scrollTop)
                    }, parseInt(this.options.revert, 10) || 500, function() {
                        n._clear(e)
                    })
                } else this._clear(e, i);
                return !1
            }
        },
        cancel: function() {
            if (this.dragging) {
                this._mouseUp({
                    target: null
                }), "original" == this.options.helper ? this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper") : this.currentItem.show();
                for (var e = this.containers.length - 1; e >= 0; e--) this.containers[e]._trigger("deactivate", null, this._uiHash(this)), this.containers[e].containerCache.over && (this.containers[e]._trigger("out", null, this._uiHash(this)), this.containers[e].containerCache.over = 0)
            }
            return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" != this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), t.extend(this, {
                helper: null,
                dragging: !1,
                reverting: !1,
                _noFinalSort: null
            }), this.domPosition.prev ? t(this.domPosition.prev).after(this.currentItem) : t(this.domPosition.parent).prepend(this.currentItem)), this
        },
        serialize: function(e) {
            var i = this._getItemsAsjQuery(e && e.connected),
                n = [];
            return e = e || {}, t(i).each(function() {
                var i = (t(e.item || this).attr(e.attribute || "id") || "").match(e.expression || /(.+)[-=_](.+)/);
                i && n.push((e.key || i[1] + "[]") + "=" + (e.key && e.expression ? i[1] : i[2]))
            }), !n.length && e.key && n.push(e.key + "="), n.join("&")
        },
        toArray: function(e) {
            var i = this._getItemsAsjQuery(e && e.connected),
                n = [];
            return e = e || {}, i.each(function() {
                n.push(t(e.item || this).attr(e.attribute || "id") || "")
            }), n
        },
        _intersectsWith: function(t) {
            var e = this.positionAbs.left,
                i = e + this.helperProportions.width,
                n = this.positionAbs.top,
                s = n + this.helperProportions.height,
                a = t.left,
                o = a + t.width,
                r = t.top,
                l = r + t.height,
                c = this.offset.click.top,
                u = this.offset.click.left,
                d = n + c > r && n + c < l && e + u > a && e + u < o;
            return "pointer" == this.options.tolerance || this.options.forcePointerForContainers || "pointer" != this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > t[this.floating ? "width" : "height"] ? d : a < e + this.helperProportions.width / 2 && i - this.helperProportions.width / 2 < o && r < n + this.helperProportions.height / 2 && s - this.helperProportions.height / 2 < l
        },
        _intersectsWithPointer: function(e) {
            var i = "x" === this.options.axis || t.ui.isOverAxis(this.positionAbs.top + this.offset.click.top, e.top, e.height),
                n = "y" === this.options.axis || t.ui.isOverAxis(this.positionAbs.left + this.offset.click.left, e.left, e.width),
                s = i && n,
                a = this._getDragVerticalDirection(),
                o = this._getDragHorizontalDirection();
            return !!s && (this.floating ? o && "right" == o || "down" == a ? 2 : 1 : a && ("down" == a ? 2 : 1))
        },
        _intersectsWithSides: function(e) {
            var i = t.ui.isOverAxis(this.positionAbs.top + this.offset.click.top, e.top + e.height / 2, e.height),
                n = t.ui.isOverAxis(this.positionAbs.left + this.offset.click.left, e.left + e.width / 2, e.width),
                s = this._getDragVerticalDirection(),
                a = this._getDragHorizontalDirection();
            return this.floating && a ? "right" == a && n || "left" == a && !n : s && ("down" == s && i || "up" == s && !i)
        },
        _getDragVerticalDirection: function() {
            var t = this.positionAbs.top - this.lastPositionAbs.top;
            return 0 != t && (t > 0 ? "down" : "up")
        },
        _getDragHorizontalDirection: function() {
            var t = this.positionAbs.left - this.lastPositionAbs.left;
            return 0 != t && (t > 0 ? "right" : "left")
        },
        refresh: function(t) {
            return this._refreshItems(t), this.refreshPositions(), this
        },
        _connectWith: function() {
            var t = this.options;
            return t.connectWith.constructor == String ? [t.connectWith] : t.connectWith
        },
        _getItemsAsjQuery: function(e) {
            var i = [],
                n = [],
                s = this._connectWith();
            if (s && e)
                for (var a = s.length - 1; a >= 0; a--)
                    for (var o = t(s[a]), r = o.length - 1; r >= 0; r--) {
                        var l = t.data(o[r], this.widgetName);
                        l && l != this && !l.options.disabled && n.push([t.isFunction(l.options.items) ? l.options.items.call(l.element) : t(l.options.items, l.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), l])
                    }
            n.push([t.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
                options: this.options,
                item: this.currentItem
            }) : t(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]);
            for (var a = n.length - 1; a >= 0; a--) n[a][0].each(function() {
                i.push(this)
            });
            return t(i)
        },
        _removeCurrentsFromItems: function() {
            var e = this.currentItem.find(":data(" + this.widgetName + "-item)");
            this.items = t.grep(this.items, function(t) {
                for (var i = 0; i < e.length; i++)
                    if (e[i] == t.item[0]) return !1;
                return !0
            })
        },
        _refreshItems: function(e) {
            this.items = [], this.containers = [this];
            var i = this.items,
                n = [
                    [t.isFunction(this.options.items) ? this.options.items.call(this.element[0], e, {
                        item: this.currentItem
                    }) : t(this.options.items, this.element), this]
                ],
                s = this._connectWith();
            if (s && this.ready)
                for (var a = s.length - 1; a >= 0; a--)
                    for (var o = t(s[a]), r = o.length - 1; r >= 0; r--) {
                        var l = t.data(o[r], this.widgetName);
                        l && l != this && !l.options.disabled && (n.push([t.isFunction(l.options.items) ? l.options.items.call(l.element[0], e, {
                            item: this.currentItem
                        }) : t(l.options.items, l.element), l]), this.containers.push(l))
                    }
            for (var a = n.length - 1; a >= 0; a--)
                for (var c = n[a][1], u = n[a][0], r = 0, d = u.length; r < d; r++) {
                    var h = t(u[r]);
                    h.data(this.widgetName + "-item", c), i.push({
                        item: h,
                        instance: c,
                        width: 0,
                        height: 0,
                        left: 0,
                        top: 0
                    })
                }
        },
        refreshPositions: function(e) {
            this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
            for (var i = this.items.length - 1; i >= 0; i--) {
                var n = this.items[i];
                if (n.instance == this.currentContainer || !this.currentContainer || n.item[0] == this.currentItem[0]) {
                    var s = this.options.toleranceElement ? t(this.options.toleranceElement, n.item) : n.item;
                    e || (n.width = s.outerWidth(), n.height = s.outerHeight());
                    var a = s.offset();
                    n.left = a.left, n.top = a.top
                }
            }
            if (this.options.custom && this.options.custom.refreshContainers) this.options.custom.refreshContainers.call(this);
            else
                for (var i = this.containers.length - 1; i >= 0; i--) {
                    var a = this.containers[i].element.offset();
                    this.containers[i].containerCache.left = a.left, this.containers[i].containerCache.top = a.top, this.containers[i].containerCache.width = this.containers[i].element.outerWidth(), this.containers[i].containerCache.height = this.containers[i].element.outerHeight()
                }
            return this
        },
        _createPlaceholder: function(e) {
            e = e || this;
            var i = e.options;
            if (!i.placeholder || i.placeholder.constructor == String) {
                var n = i.placeholder;
                i.placeholder = {
                    element: function() {
                        var i = t(document.createElement(e.currentItem[0].nodeName)).addClass(n || e.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper")[0];
                        return n || (i.style.visibility = "hidden"), i
                    },
                    update: function(t, s) {
                        n && !i.forcePlaceholderSize || (s.height() || s.height(e.currentItem.innerHeight() - parseInt(e.currentItem.css("paddingTop") || 0, 10) - parseInt(e.currentItem.css("paddingBottom") || 0, 10)), s.width() || s.width(e.currentItem.innerWidth() - parseInt(e.currentItem.css("paddingLeft") || 0, 10) - parseInt(e.currentItem.css("paddingRight") || 0, 10)))
                    }
                }
            }
            e.placeholder = t(i.placeholder.element.call(e.element, e.currentItem)), e.currentItem.after(e.placeholder), i.placeholder.update(e, e.placeholder)
        },
        _contactContainers: function(e) {
            for (var i = null, n = null, s = this.containers.length - 1; s >= 0; s--)
                if (!t.contains(this.currentItem[0], this.containers[s].element[0]))
                    if (this._intersectsWith(this.containers[s].containerCache)) {
                        if (i && t.contains(this.containers[s].element[0], i.element[0])) continue;
                        i = this.containers[s], n = s
                    } else this.containers[s].containerCache.over && (this.containers[s]._trigger("out", e, this._uiHash(this)), this.containers[s].containerCache.over = 0);
            if (i)
                if (1 === this.containers.length) this.containers[n]._trigger("over", e, this._uiHash(this)), this.containers[n].containerCache.over = 1;
                else {
                    for (var a = 1e4, o = null, r = this.containers[n].floating ? "left" : "top", l = this.containers[n].floating ? "width" : "height", c = this.positionAbs[r] + this.offset.click[r], u = this.items.length - 1; u >= 0; u--)
                        if (t.contains(this.containers[n].element[0], this.items[u].item[0]) && this.items[u].item[0] != this.currentItem[0]) {
                            var d = this.items[u].item.offset()[r],
                                h = !1;
                            Math.abs(d - c) > Math.abs(d + this.items[u][l] - c) && (h = !0, d += this.items[u][l]), Math.abs(d - c) < a && (a = Math.abs(d - c), o = this.items[u], this.direction = h ? "up" : "down")
                        }
                    if (!o && !this.options.dropOnEmpty) return;
                    this.currentContainer = this.containers[n], o ? this._rearrange(e, o, null, !0) : this._rearrange(e, null, this.containers[n].element, !0), this._trigger("change", e, this._uiHash()), this.containers[n]._trigger("change", e, this._uiHash(this)), this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[n]._trigger("over", e, this._uiHash(this)), this.containers[n].containerCache.over = 1
                }
        },
        _createHelper: function(e) {
            var i = this.options,
                n = t.isFunction(i.helper) ? t(i.helper.apply(this.element[0], [e, this.currentItem])) : "clone" == i.helper ? this.currentItem.clone() : this.currentItem;
            return n.parents("body").length || t("parent" != i.appendTo ? i.appendTo : this.currentItem[0].parentNode)[0].appendChild(n[0]), n[0] == this.currentItem[0] && (this._storedCSS = {
                width: this.currentItem[0].style.width,
                height: this.currentItem[0].style.height,
                position: this.currentItem.css("position"),
                top: this.currentItem.css("top"),
                left: this.currentItem.css("left")
            }), ("" == n[0].style.width || i.forceHelperSize) && n.width(this.currentItem.width()), ("" == n[0].style.height || i.forceHelperSize) && n.height(this.currentItem.height()), n
        },
        _adjustOffsetFromHelper: function(e) {
            "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
                left: +e[0],
                top: +e[1] || 0
            }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top)
        },
        _getParentOffset: function() {
            this.offsetParent = this.helper.offsetParent();
            var e = this.offsetParent.offset();
            return "absolute" == this.cssPosition && this.scrollParent[0] != document && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] == document.body || this.offsetParent[0].tagName && "html" == this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {
                top: 0,
                left: 0
            }), {
                top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function() {
            if ("relative" == this.cssPosition) {
                var t = this.currentItem.position();
                return {
                    top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                }
            }
            return {
                top: 0,
                left: 0
            }
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
                top: parseInt(this.currentItem.css("marginTop"), 10) || 0
            }
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            }
        },
        _setContainment: function() {
            var e = this.options;
            if ("parent" == e.containment && (e.containment = this.helper[0].parentNode), "document" != e.containment && "window" != e.containment || (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, t("document" == e.containment ? document : window).width() - this.helperProportions.width - this.margins.left, (t("document" == e.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), !/^(document|window|parent)$/.test(e.containment)) {
                var i = t(e.containment)[0],
                    n = t(e.containment).offset(),
                    s = "hidden" != t(i).css("overflow");
                this.containment = [n.left + (parseInt(t(i).css("borderLeftWidth"), 10) || 0) + (parseInt(t(i).css("paddingLeft"), 10) || 0) - this.margins.left, n.top + (parseInt(t(i).css("borderTopWidth"), 10) || 0) + (parseInt(t(i).css("paddingTop"), 10) || 0) - this.margins.top, n.left + (s ? Math.max(i.scrollWidth, i.offsetWidth) : i.offsetWidth) - (parseInt(t(i).css("borderLeftWidth"), 10) || 0) - (parseInt(t(i).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, n.top + (s ? Math.max(i.scrollHeight, i.offsetHeight) : i.offsetHeight) - (parseInt(t(i).css("borderTopWidth"), 10) || 0) - (parseInt(t(i).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top]
            }
        },
        _convertPositionTo: function(e, i) {
            i || (i = this.position);
            var n = "absolute" == e ? 1 : -1,
                s = (this.options, "absolute" != this.cssPosition || this.scrollParent[0] != document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent),
                a = /(html|body)/i.test(s[0].tagName);
            return {
                top: i.top + this.offset.relative.top * n + this.offset.parent.top * n - ("fixed" == this.cssPosition ? -this.scrollParent.scrollTop() : a ? 0 : s.scrollTop()) * n,
                left: i.left + this.offset.relative.left * n + this.offset.parent.left * n - ("fixed" == this.cssPosition ? -this.scrollParent.scrollLeft() : a ? 0 : s.scrollLeft()) * n
            }
        },
        _generatePosition: function(e) {
            var i = this.options,
                n = "absolute" != this.cssPosition || this.scrollParent[0] != document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                s = /(html|body)/i.test(n[0].tagName);
            "relative" != this.cssPosition || this.scrollParent[0] != document && this.scrollParent[0] != this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset());
            var a = e.pageX,
                o = e.pageY;
            if (this.originalPosition && (this.containment && (e.pageX - this.offset.click.left < this.containment[0] && (a = this.containment[0] + this.offset.click.left), e.pageY - this.offset.click.top < this.containment[1] && (o = this.containment[1] + this.offset.click.top), e.pageX - this.offset.click.left > this.containment[2] && (a = this.containment[2] + this.offset.click.left), e.pageY - this.offset.click.top > this.containment[3] && (o = this.containment[3] + this.offset.click.top)), i.grid)) {
                var r = this.originalPageY + Math.round((o - this.originalPageY) / i.grid[1]) * i.grid[1];
                o = this.containment && (r - this.offset.click.top < this.containment[1] || r - this.offset.click.top > this.containment[3]) ? r - this.offset.click.top < this.containment[1] ? r + i.grid[1] : r - i.grid[1] : r;
                var l = this.originalPageX + Math.round((a - this.originalPageX) / i.grid[0]) * i.grid[0];
                a = this.containment && (l - this.offset.click.left < this.containment[0] || l - this.offset.click.left > this.containment[2]) ? l - this.offset.click.left < this.containment[0] ? l + i.grid[0] : l - i.grid[0] : l
            }
            return {
                top: o - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" == this.cssPosition ? -this.scrollParent.scrollTop() : s ? 0 : n.scrollTop()),
                left: a - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" == this.cssPosition ? -this.scrollParent.scrollLeft() : s ? 0 : n.scrollLeft())
            }
        },
        _rearrange: function(t, e, i, n) {
            i ? i[0].appendChild(this.placeholder[0]) : e.item[0].parentNode.insertBefore(this.placeholder[0], "down" == this.direction ? e.item[0] : e.item[0].nextSibling), this.counter = this.counter ? ++this.counter : 1;
            var s = this.counter;
            this._delay(function() {
                s == this.counter && this.refreshPositions(!n)
            })
        },
        _clear: function(e, i) {
            this.reverting = !1;
            var n = [];
            if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] == this.currentItem[0]) {
                for (var s in this._storedCSS) "auto" != this._storedCSS[s] && "static" != this._storedCSS[s] || (this._storedCSS[s] = "");
                this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
            } else this.currentItem.show();
            this.fromOutside && !i && n.push(function(t) {
                this._trigger("receive", t, this._uiHash(this.fromOutside))
            }), !this.fromOutside && this.domPosition.prev == this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent == this.currentItem.parent()[0] || i || n.push(function(t) {
                this._trigger("update", t, this._uiHash())
            }), this !== this.currentContainer && (i || (n.push(function(t) {
                this._trigger("remove", t, this._uiHash())
            }), n.push(function(t) {
                return function(e) {
                    t._trigger("receive", e, this._uiHash(this))
                }
            }.call(this, this.currentContainer)), n.push(function(t) {
                return function(e) {
                    t._trigger("update", e, this._uiHash(this))
                }
            }.call(this, this.currentContainer))));
            for (var s = this.containers.length - 1; s >= 0; s--) i || n.push(function(t) {
                return function(e) {
                    t._trigger("deactivate", e, this._uiHash(this))
                }
            }.call(this, this.containers[s])), this.containers[s].containerCache.over && (n.push(function(t) {
                return function(e) {
                    t._trigger("out", e, this._uiHash(this))
                }
            }.call(this, this.containers[s])), this.containers[s].containerCache.over = 0);
            if (this._storedCursor && t("body").css("cursor", this._storedCursor), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" == this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, this.cancelHelperRemoval) {
                if (!i) {
                    this._trigger("beforeStop", e, this._uiHash());
                    for (var s = 0; s < n.length; s++) n[s].call(this, e);
                    this._trigger("stop", e, this._uiHash())
                }
                return this.fromOutside = !1, !1
            }
            if (i || this._trigger("beforeStop", e, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.helper[0] != this.currentItem[0] && this.helper.remove(), this.helper = null, !i) {
                for (var s = 0; s < n.length; s++) n[s].call(this, e);
                this._trigger("stop", e, this._uiHash())
            }
            return this.fromOutside = !1, !0
        },
        _trigger: function() {
            !1 === t.Widget.prototype._trigger.apply(this, arguments) && this.cancel()
        },
        _uiHash: function(e) {
            var i = e || this;
            return {
                helper: i.helper,
                placeholder: i.placeholder || t([]),
                position: i.position,
                originalPosition: i.originalPosition,
                offset: i.positionAbs,
                item: i.currentItem,
                sender: e ? e.element : null
            }
        }
    })
}(jQuery), jQuery.effects || function(t, e) {
        var i = !1 !== t.uiBackCompat,
            n = "ui-effects-";
        t.effects = {
                effect: {}
            },
            function(e, i) {
                function n(t, e, i) {
                    var n = h[e.type] || {};
                    return null == t ? i || !e.def ? null : e.def : (t = n.floor ? ~~t : parseFloat(t), isNaN(t) ? e.def : n.mod ? (t + n.mod) % n.mod : 0 > t ? 0 : n.max < t ? n.max : t)
                }

                function s(t) {
                    var i = u(),
                        n = i._rgba = [];
                    return t = t.toLowerCase(), m(c, function(e, s) {
                        var a, o = s.re.exec(t),
                            r = o && s.parse(o),
                            l = s.space || "rgba";
                        if (r) return a = i[l](r), i[d[l].cache] = a[d[l].cache], n = i._rgba = a._rgba, !1
                    }), n.length ? ("0,0,0,0" === n.join() && e.extend(n, o.transparent), i) : o[t]
                }

                function a(t, e, i) {
                    return i = (i + 1) % 1, 6 * i < 1 ? t + (e - t) * i * 6 : 2 * i < 1 ? e : 3 * i < 2 ? t + (e - t) * (2 / 3 - i) * 6 : t
                }
                var o, r = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor".split(" "),
                    l = /^([\-+])=\s*(\d+\.?\d*)/,
                    c = [{
                        re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
                        parse: function(t) {
                            return [t[1], t[2], t[3], t[4]]
                        }
                    }, {
                        re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
                        parse: function(t) {
                            return [2.55 * t[1], 2.55 * t[2], 2.55 * t[3], t[4]]
                        }
                    }, {
                        re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
                        parse: function(t) {
                            return [parseInt(t[1], 16), parseInt(t[2], 16), parseInt(t[3], 16)]
                        }
                    }, {
                        re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
                        parse: function(t) {
                            return [parseInt(t[1] + t[1], 16), parseInt(t[2] + t[2], 16), parseInt(t[3] + t[3], 16)]
                        }
                    }, {
                        re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
                        space: "hsla",
                        parse: function(t) {
                            return [t[1], t[2] / 100, t[3] / 100, t[4]]
                        }
                    }],
                    u = e.Color = function(t, i, n, s) {
                        return new e.Color.fn.parse(t, i, n, s)
                    },
                    d = {
                        rgba: {
                            props: {
                                red: {
                                    idx: 0,
                                    type: "byte"
                                },
                                green: {
                                    idx: 1,
                                    type: "byte"
                                },
                                blue: {
                                    idx: 2,
                                    type: "byte"
                                }
                            }
                        },
                        hsla: {
                            props: {
                                hue: {
                                    idx: 0,
                                    type: "degrees"
                                },
                                saturation: {
                                    idx: 1,
                                    type: "percent"
                                },
                                lightness: {
                                    idx: 2,
                                    type: "percent"
                                }
                            }
                        }
                    },
                    h = {
                        "byte": {
                            floor: !0,
                            max: 255
                        },
                        percent: {
                            max: 1
                        },
                        degrees: {
                            mod: 360,
                            floor: !0
                        }
                    },
                    p = u.support = {},
                    f = e("<p>")[0],
                    m = e.each;
                f.style.cssText = "background-color:rgba(1,1,1,.5)", p.rgba = f.style.backgroundColor.indexOf("rgba") > -1, m(d, function(t, e) {
                    e.cache = "_" + t, e.props.alpha = {
                        idx: 3,
                        type: "percent",
                        def: 1
                    }
                }), u.fn = e.extend(u.prototype, {
                    parse: function(a, r, l, c) {
                        if (a === i) return this._rgba = [null, null, null, null], this;
                        (a.jquery || a.nodeType) && (a = e(a).css(r), r = i);
                        var h = this,
                            p = e.type(a),
                            f = this._rgba = [];
                        return r !== i && (a = [a, r, l, c], p = "array"), "string" === p ? this.parse(s(a) || o._default) : "array" === p ? (m(d.rgba.props, function(t, e) {
                            f[e.idx] = n(a[e.idx], e)
                        }), this) : "object" === p ? (a instanceof u ? m(d, function(t, e) {
                            a[e.cache] && (h[e.cache] = a[e.cache].slice())
                        }) : m(d, function(e, i) {
                            var s = i.cache;
                            m(i.props, function(t, e) {
                                if (!h[s] && i.to) {
                                    if ("alpha" === t || null == a[t]) return;
                                    h[s] = i.to(h._rgba)
                                }
                                h[s][e.idx] = n(a[t], e, !0)
                            }), h[s] && t.inArray(null, h[s].slice(0, 3)) < 0 && (h[s][3] = 1, i.from && (h._rgba = i.from(h[s])))
                        }), this) : void 0
                    },
                    is: function(t) {
                        var e = u(t),
                            i = !0,
                            n = this;
                        return m(d, function(t, s) {
                            var a, o = e[s.cache];
                            return o && (a = n[s.cache] || s.to && s.to(n._rgba) || [], m(s.props, function(t, e) {
                                if (null != o[e.idx]) return i = o[e.idx] === a[e.idx]
                            })), i
                        }), i
                    },
                    _space: function() {
                        var t = [],
                            e = this;
                        return m(d, function(i, n) {
                            e[n.cache] && t.push(i)
                        }), t.pop()
                    },
                    transition: function(t, e) {
                        var i = u(t),
                            s = i._space(),
                            a = d[s],
                            o = 0 === this.alpha() ? u("transparent") : this,
                            r = o[a.cache] || a.to(o._rgba),
                            l = r.slice();
                        return i = i[a.cache], m(a.props, function(t, s) {
                            var a = s.idx,
                                o = r[a],
                                c = i[a],
                                u = h[s.type] || {};
                            null !== c && (null === o ? l[a] = c : (u.mod && (c - o > u.mod / 2 ? o += u.mod : o - c > u.mod / 2 && (o -= u.mod)), l[a] = n((c - o) * e + o, s)))
                        }), this[s](l)
                    },
                    blend: function(t) {
                        if (1 === this._rgba[3]) return this;
                        var i = this._rgba.slice(),
                            n = i.pop(),
                            s = u(t)._rgba;
                        return u(e.map(i, function(t, e) {
                            return (1 - n) * s[e] + n * t
                        }))
                    },
                    toRgbaString: function() {
                        var t = "rgba(",
                            i = e.map(this._rgba, function(t, e) {
                                return null == t ? e > 2 ? 1 : 0 : t
                            });
                        return 1 === i[3] && (i.pop(), t = "rgb("), t + i.join() + ")"
                    },
                    toHslaString: function() {
                        var t = "hsla(",
                            i = e.map(this.hsla(), function(t, e) {
                                return null == t && (t = e > 2 ? 1 : 0), e && e < 3 && (t = Math.round(100 * t) + "%"), t
                            });
                        return 1 === i[3] && (i.pop(), t = "hsl("), t + i.join() + ")"
                    },
                    toHexString: function(t) {
                        var i = this._rgba.slice(),
                            n = i.pop();
                        return t && i.push(~~(255 * n)), "#" + e.map(i, function(t) {
                            return t = (t || 0).toString(16), 1 === t.length ? "0" + t : t
                        }).join("")
                    },
                    toString: function() {
                        return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
                    }
                }), u.fn.parse.prototype = u.fn, d.hsla.to = function(t) {
                    if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
                    var e, i, n = t[0] / 255,
                        s = t[1] / 255,
                        a = t[2] / 255,
                        o = t[3],
                        r = Math.max(n, s, a),
                        l = Math.min(n, s, a),
                        c = r - l,
                        u = r + l,
                        d = .5 * u;
                    return e = l === r ? 0 : n === r ? 60 * (s - a) / c + 360 : s === r ? 60 * (a - n) / c + 120 : 60 * (n - s) / c + 240, i = 0 === d || 1 === d ? d : d <= .5 ? c / u : c / (2 - u), [Math.round(e) % 360, i, d, null == o ? 1 : o]
                }, d.hsla.from = function(t) {
                    if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
                    var e = t[0] / 360,
                        i = t[1],
                        n = t[2],
                        s = t[3],
                        o = n <= .5 ? n * (1 + i) : n + i - n * i,
                        r = 2 * n - o;
                    return [Math.round(255 * a(r, o, e + 1 / 3)), Math.round(255 * a(r, o, e)), Math.round(255 * a(r, o, e - 1 / 3)), s]
                }, m(d, function(t, s) {
                    var a = s.props,
                        o = s.cache,
                        r = s.to,
                        c = s.from;
                    u.fn[t] = function(t) {
                        if (r && !this[o] && (this[o] = r(this._rgba)), t === i) return this[o].slice();
                        var s, l = e.type(t),
                            d = "array" === l || "object" === l ? t : arguments,
                            h = this[o].slice();
                        return m(a, function(t, e) {
                            var i = d["object" === l ? t : e.idx];
                            null == i && (i = h[e.idx]), h[e.idx] = n(i, e)
                        }), c ? (s = u(c(h)), s[o] = h, s) : u(h)
                    }, m(a, function(i, n) {
                        u.fn[i] || (u.fn[i] = function(s) {
                            var a, o = e.type(s),
                                r = "alpha" === i ? this._hsla ? "hsla" : "rgba" : t,
                                c = this[r](),
                                u = c[n.idx];
                            return "undefined" === o ? u : ("function" === o && (s = s.call(this, u), o = e.type(s)), null == s && n.empty ? this : ("string" === o && (a = l.exec(s)) && (s = u + parseFloat(a[2]) * ("+" === a[1] ? 1 : -1)), c[n.idx] = s, this[r](c)))
                        })
                    })
                }), m(r, function(t, i) {
                    e.cssHooks[i] = {
                        set: function(t, n) {
                            var a, o, r = "";
                            if ("string" !== e.type(n) || (a = s(n))) {
                                if (n = u(a || n), !p.rgba && 1 !== n._rgba[3]) {
                                    for (o = "backgroundColor" === i ? t.parentNode : t;
                                        ("" === r || "transparent" === r) && o && o.style;) try {
                                        r = e.css(o, "backgroundColor"), o = o.parentNode
                                    } catch (t) {}
                                    n = n.blend(r && "transparent" !== r ? r : "_default")
                                }
                                n = n.toRgbaString()
                            }
                            try {
                                t.style[i] = n
                            } catch (t) {}
                        }
                    }, e.fx.step[i] = function(t) {
                        t.colorInit || (t.start = u(t.elem, i), t.end = u(t.end), t.colorInit = !0), e.cssHooks[i].set(t.elem, t.start.transition(t.end, t.pos))
                    }
                }), e.cssHooks.borderColor = {
                    expand: function(t) {
                        var e = {};
                        return m(["Top", "Right", "Bottom", "Left"], function(i, n) {
                            e["border" + n + "Color"] = t
                        }), e
                    }
                }, o = e.Color.names = {
                    aqua: "#00ffff",
                    black: "#000000",
                    blue: "#0000ff",
                    fuchsia: "#ff00ff",
                    gray: "#808080",
                    green: "#008000",
                    lime: "#00ff00",
                    maroon: "#800000",
                    navy: "#000080",
                    olive: "#808000",
                    purple: "#800080",
                    red: "#ff0000",
                    silver: "#c0c0c0",
                    teal: "#008080",
                    white: "#ffffff",
                    yellow: "#ffff00",
                    transparent: [null, null, null, 0],
                    _default: "#ffffff"
                }
            }(jQuery),
            function() {
                function i() {
                    var e, i, n = this.ownerDocument.defaultView ? this.ownerDocument.defaultView.getComputedStyle(this, null) : this.currentStyle,
                        s = {};
                    if (n && n.length && n[0] && n[n[0]])
                        for (i = n.length; i--;) e = n[i],
                            "string" == typeof n[e] && (s[t.camelCase(e)] = n[e]);
                    else
                        for (e in n) "string" == typeof n[e] && (s[e] = n[e]);
                    return s
                }

                function n(e, i) {
                    var n, s, o = {};
                    for (n in i) s = i[n], e[n] !== s && (a[n] || !t.fx.step[n] && isNaN(parseFloat(s)) || (o[n] = s));
                    return o
                }
                var s = ["add", "remove", "toggle"],
                    a = {
                        border: 1,
                        borderBottom: 1,
                        borderColor: 1,
                        borderLeft: 1,
                        borderRight: 1,
                        borderTop: 1,
                        borderWidth: 1,
                        margin: 1,
                        padding: 1
                    };
                t.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function(e, i) {
                    t.fx.step[i] = function(t) {
                        ("none" !== t.end && !t.setAttr || 1 === t.pos && !t.setAttr) && (jQuery.style(t.elem, i, t.end), t.setAttr = !0)
                    }
                }), t.effects.animateClass = function(e, a, o, r) {
                    var l = t.speed(a, o, r);
                    return this.queue(function() {
                        var a, o = t(this),
                            r = o.attr("class") || "",
                            c = l.children ? o.find("*").andSelf() : o;
                        c = c.map(function() {
                            return {
                                el: t(this),
                                start: i.call(this)
                            }
                        }), a = function() {
                            t.each(s, function(t, i) {
                                e[i] && o[i + "Class"](e[i])
                            })
                        }, a(), c = c.map(function() {
                            return this.end = i.call(this.el[0]), this.diff = n(this.start, this.end), this
                        }), o.attr("class", r), c = c.map(function() {
                            var e = this,
                                i = t.Deferred(),
                                n = jQuery.extend({}, l, {
                                    queue: !1,
                                    complete: function() {
                                        i.resolve(e)
                                    }
                                });
                            return this.el.animate(this.diff, n), i.promise()
                        }), t.when.apply(t, c.get()).done(function() {
                            a(), t.each(arguments, function() {
                                var e = this.el;
                                t.each(this.diff, function(t) {
                                    e.css(t, "")
                                })
                            }), l.complete.call(o[0])
                        })
                    })
                }, t.fn.extend({
                    _addClass: t.fn.addClass,
                    addClass: function(e, i, n, s) {
                        return i ? t.effects.animateClass.call(this, {
                            add: e
                        }, i, n, s) : this._addClass(e)
                    },
                    _removeClass: t.fn.removeClass,
                    removeClass: function(e, i, n, s) {
                        return i ? t.effects.animateClass.call(this, {
                            remove: e
                        }, i, n, s) : this._removeClass(e)
                    },
                    _toggleClass: t.fn.toggleClass,
                    toggleClass: function(i, n, s, a, o) {
                        return "boolean" == typeof n || n === e ? s ? t.effects.animateClass.call(this, n ? {
                            add: i
                        } : {
                            remove: i
                        }, s, a, o) : this._toggleClass(i, n) : t.effects.animateClass.call(this, {
                            toggle: i
                        }, n, s, a)
                    },
                    switchClass: function(e, i, n, s, a) {
                        return t.effects.animateClass.call(this, {
                            add: i,
                            remove: e
                        }, n, s, a)
                    }
                })
            }(),
            function() {
                function s(e, i, n, s) {
                    return t.isPlainObject(e) && (i = e, e = e.effect), e = {
                        effect: e
                    }, null == i && (i = {}), t.isFunction(i) && (s = i, n = null, i = {}), ("number" == typeof i || t.fx.speeds[i]) && (s = n, n = i, i = {}), t.isFunction(n) && (s = n, n = null), i && t.extend(e, i), n = n || i.duration, e.duration = t.fx.off ? 0 : "number" == typeof n ? n : n in t.fx.speeds ? t.fx.speeds[n] : t.fx.speeds._default, e.complete = s || i.complete, e
                }

                function a(e) {
                    return !(e && "number" != typeof e && !t.fx.speeds[e]) || "string" == typeof e && !t.effects.effect[e] && (!i || !t.effects[e])
                }
                t.extend(t.effects, {
                    version: "1.9.1",
                    save: function(t, e) {
                        for (var i = 0; i < e.length; i++) null !== e[i] && t.data(n + e[i], t[0].style[e[i]])
                    },
                    restore: function(t, i) {
                        var s, a;
                        for (a = 0; a < i.length; a++) null !== i[a] && (s = t.data(n + i[a]), s === e && (s = ""), t.css(i[a], s))
                    },
                    setMode: function(t, e) {
                        return "toggle" === e && (e = t.is(":hidden") ? "show" : "hide"), e
                    },
                    getBaseline: function(t, e) {
                        var i, n;
                        switch (t[0]) {
                            case "top":
                                i = 0;
                                break;
                            case "middle":
                                i = .5;
                                break;
                            case "bottom":
                                i = 1;
                                break;
                            default:
                                i = t[0] / e.height
                        }
                        switch (t[1]) {
                            case "left":
                                n = 0;
                                break;
                            case "center":
                                n = .5;
                                break;
                            case "right":
                                n = 1;
                                break;
                            default:
                                n = t[1] / e.width
                        }
                        return {
                            x: n,
                            y: i
                        }
                    },
                    createWrapper: function(e) {
                        if (e.parent().is(".ui-effects-wrapper")) return e.parent();
                        var i = {
                                width: e.outerWidth(!0),
                                height: e.outerHeight(!0),
                                "float": e.css("float")
                            },
                            n = t("<div></div>").addClass("ui-effects-wrapper").css({
                                fontSize: "100%",
                                background: "transparent",
                                border: "none",
                                margin: 0,
                                padding: 0
                            }),
                            s = {
                                width: e.width(),
                                height: e.height()
                            },
                            a = document.activeElement;
                        try {
                            a.id
                        } catch (t) {
                            a = document.body
                        }
                        return e.wrap(n), (e[0] === a || t.contains(e[0], a)) && t(a).focus(), n = e.parent(), "static" === e.css("position") ? (n.css({
                            position: "relative"
                        }), e.css({
                            position: "relative"
                        })) : (t.extend(i, {
                            position: e.css("position"),
                            zIndex: e.css("z-index")
                        }), t.each(["top", "left", "bottom", "right"], function(t, n) {
                            i[n] = e.css(n), isNaN(parseInt(i[n], 10)) && (i[n] = "auto")
                        }), e.css({
                            position: "relative",
                            top: 0,
                            left: 0,
                            right: "auto",
                            bottom: "auto"
                        })), e.css(s), n.css(i).show()
                    },
                    removeWrapper: function(e) {
                        var i = document.activeElement;
                        return e.parent().is(".ui-effects-wrapper") && (e.parent().replaceWith(e), (e[0] === i || t.contains(e[0], i)) && t(i).focus()), e
                    },
                    setTransition: function(e, i, n, s) {
                        return s = s || {}, t.each(i, function(t, i) {
                            var a = e.cssUnit(i);
                            a[0] > 0 && (s[i] = a[0] * n + a[1])
                        }), s
                    }
                }), t.fn.extend({
                    effect: function() {
                        function e(e) {
                            function i() {
                                t.isFunction(a) && a.call(s[0]), t.isFunction(e) && e()
                            }
                            var s = t(this),
                                a = n.complete,
                                o = n.mode;
                            (s.is(":hidden") ? "hide" === o : "show" === o) ? i(): r.call(s[0], n, i)
                        }
                        var n = s.apply(this, arguments),
                            a = n.mode,
                            o = n.queue,
                            r = t.effects.effect[n.effect],
                            l = !r && i && t.effects[n.effect];
                        return t.fx.off || !r && !l ? a ? this[a](n.duration, n.complete) : this.each(function() {
                            n.complete && n.complete.call(this)
                        }) : r ? !1 === o ? this.each(e) : this.queue(o || "fx", e) : l.call(this, {
                            options: n,
                            duration: n.duration,
                            callback: n.complete,
                            mode: n.mode
                        })
                    },
                    _show: t.fn.show,
                    show: function(t) {
                        if (a(t)) return this._show.apply(this, arguments);
                        var e = s.apply(this, arguments);
                        return e.mode = "show", this.effect.call(this, e)
                    },
                    _hide: t.fn.hide,
                    hide: function(t) {
                        if (a(t)) return this._hide.apply(this, arguments);
                        var e = s.apply(this, arguments);
                        return e.mode = "hide", this.effect.call(this, e)
                    },
                    __toggle: t.fn.toggle,
                    toggle: function(e) {
                        if (a(e) || "boolean" == typeof e || t.isFunction(e)) return this.__toggle.apply(this, arguments);
                        var i = s.apply(this, arguments);
                        return i.mode = "toggle", this.effect.call(this, i)
                    },
                    cssUnit: function(e) {
                        var i = this.css(e),
                            n = [];
                        return t.each(["em", "px", "%", "pt"], function(t, e) {
                            i.indexOf(e) > 0 && (n = [parseFloat(i), e])
                        }), n
                    }
                })
            }(),
            function() {
                var e = {};
                t.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(t, i) {
                    e[i] = function(e) {
                        return Math.pow(e, t + 2)
                    }
                }), t.extend(e, {
                    Sine: function(t) {
                        return 1 - Math.cos(t * Math.PI / 2)
                    },
                    Circ: function(t) {
                        return 1 - Math.sqrt(1 - t * t)
                    },
                    Elastic: function(t) {
                        return 0 === t || 1 === t ? t : -Math.pow(2, 8 * (t - 1)) * Math.sin((80 * (t - 1) - 7.5) * Math.PI / 15)
                    },
                    Back: function(t) {
                        return t * t * (3 * t - 2)
                    },
                    Bounce: function(t) {
                        for (var e, i = 4; t < ((e = Math.pow(2, --i)) - 1) / 11;);
                        return 1 / Math.pow(4, 3 - i) - 7.5625 * Math.pow((3 * e - 2) / 22 - t, 2)
                    }
                }), t.each(e, function(e, i) {
                    t.easing["easeIn" + e] = i, t.easing["easeOut" + e] = function(t) {
                        return 1 - i(1 - t)
                    }, t.easing["easeInOut" + e] = function(t) {
                        return t < .5 ? i(2 * t) / 2 : 1 - i(-2 * t + 2) / 2
                    }
                })
            }()
    }(jQuery),
    function(t) {
        var e = 0,
            i = {},
            n = {};
        i.height = i.paddingTop = i.paddingBottom = i.borderTopWidth = i.borderBottomWidth = "hide", n.height = n.paddingTop = n.paddingBottom = n.borderTopWidth = n.borderBottomWidth = "show", t.widget("ui.accordion", {
            version: "1.9.1",
            options: {
                active: 0,
                animate: {},
                collapsible: !1,
                event: "click",
                header: "> li > :first-child,> :not(li):even",
                heightStyle: "auto",
                icons: {
                    activeHeader: "ui-icon-triangle-1-s",
                    header: "ui-icon-triangle-1-e"
                },
                activate: null,
                beforeActivate: null
            },
            _create: function() {
                var i = this.accordionId = "ui-accordion-" + (this.element.attr("id") || ++e),
                    n = this.options;
                this.prevShow = this.prevHide = t(), this.element.addClass("ui-accordion ui-widget ui-helper-reset"), this.headers = this.element.find(n.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"), this._hoverable(this.headers), this._focusable(this.headers), this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").hide(), n.collapsible || !1 !== n.active && null != n.active || (n.active = 0), n.active < 0 && (n.active += this.headers.length), this.active = this._findActive(n.active).addClass("ui-accordion-header-active ui-state-active").toggleClass("ui-corner-all ui-corner-top"), this.active.next().addClass("ui-accordion-content-active").show(), this._createIcons(), this.refresh(), this.element.attr("role", "tablist"), this.headers.attr("role", "tab").each(function(e) {
                    var n = t(this),
                        s = n.attr("id"),
                        a = n.next(),
                        o = a.attr("id");
                    s || (s = i + "-header-" + e, n.attr("id", s)), o || (o = i + "-panel-" + e, a.attr("id", o)), n.attr("aria-controls", o), a.attr("aria-labelledby", s)
                }).next().attr("role", "tabpanel"), this.headers.not(this.active).attr({
                    "aria-selected": "false",
                    tabIndex: -1
                }).next().attr({
                    "aria-expanded": "false",
                    "aria-hidden": "true"
                }).hide(), this.active.length ? this.active.attr({
                    "aria-selected": "true",
                    tabIndex: 0
                }).next().attr({
                    "aria-expanded": "true",
                    "aria-hidden": "false"
                }) : this.headers.eq(0).attr("tabIndex", 0), this._on(this.headers, {
                    keydown: "_keydown"
                }), this._on(this.headers.next(), {
                    keydown: "_panelKeyDown"
                }), this._setupEvents(n.event)
            },
            _getCreateEventData: function() {
                return {
                    header: this.active,
                    content: this.active.length ? this.active.next() : t()
                }
            },
            _createIcons: function() {
                var e = this.options.icons;
                e && (t("<span>").addClass("ui-accordion-header-icon ui-icon " + e.header).prependTo(this.headers), this.active.children(".ui-accordion-header-icon").removeClass(e.header).addClass(e.activeHeader), this.headers.addClass("ui-accordion-icons"))
            },
            _destroyIcons: function() {
                this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()
            },
            _destroy: function() {
                var t;
                this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"), this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").each(function() {
                    /^ui-accordion/.test(this.id) && this.removeAttribute("id")
                }), this._destroyIcons(), t = this.headers.next().css("display", "").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").each(function() {
                    /^ui-accordion/.test(this.id) && this.removeAttribute("id")
                }), "content" !== this.options.heightStyle && t.css("height", "")
            },
            _setOption: function(t, e) {
                if ("active" === t) return void this._activate(e);
                "event" === t && (this.options.event && this._off(this.headers, this.options.event), this._setupEvents(e)), this._super(t, e), "collapsible" !== t || e || !1 !== this.options.active || this._activate(0), "icons" === t && (this._destroyIcons(), e && this._createIcons()), "disabled" === t && this.headers.add(this.headers.next()).toggleClass("ui-state-disabled", !!e)
            },
            _keydown: function(e) {
                if (!e.altKey && !e.ctrlKey) {
                    var i = t.ui.keyCode,
                        n = this.headers.length,
                        s = this.headers.index(e.target),
                        a = !1;
                    switch (e.keyCode) {
                        case i.RIGHT:
                        case i.DOWN:
                            a = this.headers[(s + 1) % n];
                            break;
                        case i.LEFT:
                        case i.UP:
                            a = this.headers[(s - 1 + n) % n];
                            break;
                        case i.SPACE:
                        case i.ENTER:
                            this._eventHandler(e);
                            break;
                        case i.HOME:
                            a = this.headers[0];
                            break;
                        case i.END:
                            a = this.headers[n - 1]
                    }
                    a && (t(e.target).attr("tabIndex", -1), t(a).attr("tabIndex", 0), a.focus(), e.preventDefault())
                }
            },
            _panelKeyDown: function(e) {
                e.keyCode === t.ui.keyCode.UP && e.ctrlKey && t(e.currentTarget).prev().focus()
            },
            refresh: function() {
                var e, i, n = this.options.heightStyle,
                    s = this.element.parent();
                "fill" === n ? (t.support.minHeight || (i = s.css("overflow"), s.css("overflow", "hidden")), e = s.height(), this.element.siblings(":visible").each(function() {
                    var i = t(this),
                        n = i.css("position");
                    "absolute" !== n && "fixed" !== n && (e -= i.outerHeight(!0))
                }), i && s.css("overflow", i), this.headers.each(function() {
                    e -= t(this).outerHeight(!0)
                }), this.headers.next().each(function() {
                    t(this).height(Math.max(0, e - t(this).innerHeight() + t(this).height()))
                }).css("overflow", "auto")) : "auto" === n && (e = 0, this.headers.next().each(function() {
                    e = Math.max(e, t(this).height("").height())
                }).height(e))
            },
            _activate: function(e) {
                var i = this._findActive(e)[0];
                i !== this.active[0] && (i = i || this.active[0], this._eventHandler({
                    target: i,
                    currentTarget: i,
                    preventDefault: t.noop
                }))
            },
            _findActive: function(e) {
                return "number" == typeof e ? this.headers.eq(e) : t()
            },
            _setupEvents: function(e) {
                var i = {};
                e && (t.each(e.split(" "), function(t, e) {
                    i[e] = "_eventHandler"
                }), this._on(this.headers, i))
            },
            _eventHandler: function(e) {
                var i = this.options,
                    n = this.active,
                    s = t(e.currentTarget),
                    a = s[0] === n[0],
                    o = a && i.collapsible,
                    r = o ? t() : s.next(),
                    l = n.next(),
                    c = {
                        oldHeader: n,
                        oldPanel: l,
                        newHeader: o ? t() : s,
                        newPanel: r
                    };
                e.preventDefault(), a && !i.collapsible || !1 === this._trigger("beforeActivate", e, c) || (i.active = !o && this.headers.index(s), this.active = a ? t() : s, this._toggle(c), n.removeClass("ui-accordion-header-active ui-state-active"), i.icons && n.children(".ui-accordion-header-icon").removeClass(i.icons.activeHeader).addClass(i.icons.header), a || (s.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"), i.icons && s.children(".ui-accordion-header-icon").removeClass(i.icons.header).addClass(i.icons.activeHeader), s.next().addClass("ui-accordion-content-active")))
            },
            _toggle: function(e) {
                var i = e.newPanel,
                    n = this.prevShow.length ? this.prevShow : e.oldPanel;
                this.prevShow.add(this.prevHide).stop(!0, !0), this.prevShow = i, this.prevHide = n, this.options.animate ? this._animate(i, n, e) : (n.hide(), i.show(), this._toggleComplete(e)), n.attr({
                    "aria-expanded": "false",
                    "aria-hidden": "true"
                }), n.prev().attr("aria-selected", "false"), i.length && n.length ? n.prev().attr("tabIndex", -1) : i.length && this.headers.filter(function() {
                    return 0 === t(this).attr("tabIndex")
                }).attr("tabIndex", -1), i.attr({
                    "aria-expanded": "true",
                    "aria-hidden": "false"
                }).prev().attr({
                    "aria-selected": "true",
                    tabIndex: 0
                })
            },
            _animate: function(t, e, s) {
                var a, o, r, l = this,
                    c = 0,
                    u = t.length && (!e.length || t.index() < e.index()),
                    d = this.options.animate || {},
                    h = u && d.down || d,
                    p = function() {
                        l._toggleComplete(s)
                    };
                return "number" == typeof h && (r = h), "string" == typeof h && (o = h), o = o || h.easing || d.easing, r = r || h.duration || d.duration, e.length ? t.length ? (a = t.show().outerHeight(), e.animate(i, {
                    duration: r,
                    easing: o,
                    step: function(t, e) {
                        e.now = Math.round(t)
                    }
                }), void t.hide().animate(n, {
                    duration: r,
                    easing: o,
                    complete: p,
                    step: function(t, i) {
                        i.now = Math.round(t), "height" !== i.prop ? c += i.now : "content" !== l.options.heightStyle && (i.now = Math.round(a - e.outerHeight() - c), c = 0)
                    }
                })) : e.animate(i, r, o, p) : t.animate(n, r, o, p)
            },
            _toggleComplete: function(t) {
                var e = t.oldPanel;
                e.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"), e.length && (e.parent()[0].className = e.parent()[0].className), this._trigger("activate", null, t)
            }
        }), !1 !== t.uiBackCompat && (function(t, e) {
            t.extend(e.options, {
                navigation: !1,
                navigationFilter: function() {
                    return this.href.toLowerCase() === location.href.toLowerCase()
                }
            });
            var i = e._create;
            e._create = function() {
                if (this.options.navigation) {
                    var e = this,
                        n = this.element.find(this.options.header),
                        s = n.next(),
                        a = n.add(s).find("a").filter(this.options.navigationFilter)[0];
                    a && n.add(s).each(function(i) {
                        if (t.contains(this, a)) return e.options.active = Math.floor(i / 2), !1
                    })
                }
                i.call(this)
            }
        }(jQuery, jQuery.ui.accordion.prototype), function(t, e) {
            t.extend(e.options, {
                heightStyle: null,
                autoHeight: !0,
                clearStyle: !1,
                fillSpace: !1
            });
            var i = e._create,
                n = e._setOption;
            t.extend(e, {
                _create: function() {
                    this.options.heightStyle = this.options.heightStyle || this._mergeHeightStyle(), i.call(this)
                },
                _setOption: function(t) {
                    "autoHeight" !== t && "clearStyle" !== t && "fillSpace" !== t || (this.options.heightStyle = this._mergeHeightStyle()), n.apply(this, arguments)
                },
                _mergeHeightStyle: function() {
                    var t = this.options;
                    return t.fillSpace ? "fill" : t.clearStyle ? "content" : t.autoHeight ? "auto" : void 0
                }
            })
        }(jQuery, jQuery.ui.accordion.prototype), function(t, e) {
            t.extend(e.options.icons, {
                activeHeader: null,
                headerSelected: "ui-icon-triangle-1-s"
            });
            var i = e._createIcons;
            e._createIcons = function() {
                this.options.icons && (this.options.icons.activeHeader = this.options.icons.activeHeader || this.options.icons.headerSelected), i.call(this)
            }
        }(jQuery, jQuery.ui.accordion.prototype), function(t, e) {
            e.activate = e._activate;
            var i = e._findActive;
            e._findActive = function(t) {
                return -1 === t && (t = !1), t && "number" != typeof t && -1 === (t = this.headers.index(this.headers.filter(t))) && (t = !1), i.call(this, t)
            }
        }(jQuery, jQuery.ui.accordion.prototype), jQuery.ui.accordion.prototype.resize = jQuery.ui.accordion.prototype.refresh, function(t, e) {
            t.extend(e.options, {
                change: null,
                changestart: null
            });
            var i = e._trigger;
            e._trigger = function(t, e, n) {
                var s = i.apply(this, arguments);
                return !!s && ("beforeActivate" === t ? s = i.call(this, "changestart", e, {
                    oldHeader: n.oldHeader,
                    oldContent: n.oldPanel,
                    newHeader: n.newHeader,
                    newContent: n.newPanel
                }) : "activate" === t && (s = i.call(this, "change", e, {
                    oldHeader: n.oldHeader,
                    oldContent: n.oldPanel,
                    newHeader: n.newHeader,
                    newContent: n.newPanel
                })), s)
            }
        }(jQuery, jQuery.ui.accordion.prototype), function(t, e) {
            t.extend(e.options, {
                animate: null,
                animated: "slide"
            });
            var i = e._create;
            e._create = function() {
                var t = this.options;
                null === t.animate && (t.animated ? "slide" === t.animated ? t.animate = 300 : "bounceslide" === t.animated ? t.animate = {
                    duration: 200,
                    down: {
                        easing: "easeOutBounce",
                        duration: 1e3
                    }
                } : t.animate = t.animated : t.animate = !1), i.call(this)
            }
        }(jQuery, jQuery.ui.accordion.prototype))
    }(jQuery),
    function(t) {
        var e = 0;
        t.widget("ui.autocomplete", {
            version: "1.9.1",
            defaultElement: "<input>",
            options: {
                appendTo: "body",
                autoFocus: !1,
                delay: 300,
                minLength: 1,
                position: {
                    my: "left top",
                    at: "left bottom",
                    collision: "none"
                },
                source: null,
                change: null,
                close: null,
                focus: null,
                open: null,
                response: null,
                search: null,
                select: null
            },
            pending: 0,
            _create: function() {
                var e, i, n;
                this.isMultiLine = this._isMultiLine(), this.valueMethod = this.element[this.element.is("input,textarea") ? "val" : "text"], this.isNewMenu = !0, this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off"), this._on(this.element, {
                    keydown: function(s) {
                        if (this.element.prop("readOnly")) return e = !0, n = !0, void(i = !0);
                        e = !1, n = !1, i = !1;
                        var a = t.ui.keyCode;
                        switch (s.keyCode) {
                            case a.PAGE_UP:
                                e = !0, this._move("previousPage", s);
                                break;
                            case a.PAGE_DOWN:
                                e = !0, this._move("nextPage", s);
                                break;
                            case a.UP:
                                e = !0, this._keyEvent("previous", s);
                                break;
                            case a.DOWN:
                                e = !0, this._keyEvent("next", s);
                                break;
                            case a.ENTER:
                            case a.NUMPAD_ENTER:
                                this.menu.active && (e = !0, s.preventDefault(), this.menu.select(s));
                                break;
                            case a.TAB:
                                this.menu.active && this.menu.select(s);
                                break;
                            case a.ESCAPE:
                                this.menu.element.is(":visible") && (this._value(this.term), this.close(s), s.preventDefault());
                                break;
                            default:
                                i = !0, this._searchTimeout(s)
                        }
                    },
                    keypress: function(n) {
                        if (e) return e = !1, void n.preventDefault();
                        if (!i) {
                            var s = t.ui.keyCode;
                            switch (n.keyCode) {
                                case s.PAGE_UP:
                                    this._move("previousPage", n);
                                    break;
                                case s.PAGE_DOWN:
                                    this._move("nextPage", n);
                                    break;
                                case s.UP:
                                    this._keyEvent("previous", n);
                                    break;
                                case s.DOWN:
                                    this._keyEvent("next", n)
                            }
                        }
                    },
                    input: function(t) {
                        if (n) return n = !1, void t.preventDefault();
                        this._searchTimeout(t)
                    },
                    focus: function() {
                        this.selectedItem = null, this.previous = this._value()
                    },
                    blur: function(t) {
                        if (this.cancelBlur) return void delete this.cancelBlur;
                        clearTimeout(this.searching), this.close(t), this._change(t)
                    }
                }), this._initSource(), this.menu = t("<ul>").addClass("ui-autocomplete").appendTo(this.document.find(this.options.appendTo || "body")[0]).menu({
                    input: t(),
                    role: null
                }).zIndex(this.element.zIndex() + 1).hide().data("menu"), this._on(this.menu.element, {
                    mousedown: function(e) {
                        e.preventDefault(), this.cancelBlur = !0, this._delay(function() {
                            delete this.cancelBlur
                        });
                        var i = this.menu.element[0];
                        t(e.target).closest(".ui-menu-item").length || this._delay(function() {
                            var e = this;
                            this.document.one("mousedown", function(n) {
                                n.target === e.element[0] || n.target === i || t.contains(i, n.target) || e.close()
                            })
                        })
                    },
                    menufocus: function(e, i) {
                        if (this.isNewMenu && (this.isNewMenu = !1, e.originalEvent && /^mouse/.test(e.originalEvent.type))) return this.menu.blur(), void this.document.one("mousemove", function() {
                            t(e.target).trigger(e.originalEvent)
                        });
                        var n = i.item.data("ui-autocomplete-item") || i.item.data("item.autocomplete");
                        !1 !== this._trigger("focus", e, {
                            item: n
                        }) ? e.originalEvent && /^key/.test(e.originalEvent.type) && this._value(n.value) : this.liveRegion.text(n.value)
                    },
                    menuselect: function(t, e) {
                        var i = e.item.data("ui-autocomplete-item") || e.item.data("item.autocomplete"),
                            n = this.previous;
                        this.element[0] !== this.document[0].activeElement && (this.element.focus(), this.previous = n, this._delay(function() {
                            this.previous = n, this.selectedItem = i
                        })), !1 !== this._trigger("select", t, {
                            item: i
                        }) && this._value(i.value), this.term = this._value(), this.close(t), this.selectedItem = i
                    }
                }), this.liveRegion = t("<span>", {
                    role: "status",
                    "aria-live": "polite"
                }).addClass("ui-helper-hidden-accessible").insertAfter(this.element), t.fn.bgiframe && this.menu.element.bgiframe(), this._on(this.window, {
                    beforeunload: function() {
                        this.element.removeAttr("autocomplete")
                    }
                })
            },
            _destroy: function() {
                clearTimeout(this.searching), this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"), this.menu.element.remove(), this.liveRegion.remove()
            },
            _setOption: function(t, e) {
                this._super(t, e), "source" === t && this._initSource(), "appendTo" === t && this.menu.element.appendTo(this.document.find(e || "body")[0]), "disabled" === t && e && this.xhr && this.xhr.abort()
            },
            _isMultiLine: function() {
                return !!this.element.is("textarea") || !this.element.is("input") && this.element.prop("isContentEditable")
            },
            _initSource: function() {
                var e, i, n = this;
                t.isArray(this.options.source) ? (e = this.options.source, this.source = function(i, n) {
                    n(t.ui.autocomplete.filter(e, i.term))
                }) : "string" == typeof this.options.source ? (i = this.options.source, this.source = function(e, s) {
                    n.xhr && n.xhr.abort(), n.xhr = t.ajax({
                        url: i,
                        data: e,
                        dataType: "json",
                        success: function(t) {
                            s(t)
                        },
                        error: function() {
                            s([])
                        }
                    })
                }) : this.source = this.options.source
            },
            _searchTimeout: function(t) {
                clearTimeout(this.searching), this.searching = this._delay(function() {
                    this.term !== this._value() && (this.selectedItem = null, this.search(null, t))
                }, this.options.delay)
            },
            search: function(t, e) {
                return t = null != t ? t : this._value(), this.term = this._value(), t.length < this.options.minLength ? this.close(e) : !1 !== this._trigger("search", e) ? this._search(t) : void 0
            },
            _search: function(t) {
                this.pending++, this.element.addClass("ui-autocomplete-loading"), this.cancelSearch = !1, this.source({
                    term: t
                }, this._response())
            },
            _response: function() {
                var t = this,
                    i = ++e;
                return function(n) {
                    i === e && t.__response(n), --t.pending || t.element.removeClass("ui-autocomplete-loading")
                }
            },
            __response: function(t) {
                t && (t = this._normalize(t)), this._trigger("response", null, {
                    content: t
                }), !this.options.disabled && t && t.length && !this.cancelSearch ? (this._suggest(t), this._trigger("open")) : this._close()
            },
            close: function(t) {
                this.cancelSearch = !0, this._close(t)
            },
            _close: function(t) {
                this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), this.isNewMenu = !0, this._trigger("close", t))
            },
            _change: function(t) {
                this.previous !== this._value() && this._trigger("change", t, {
                    item: this.selectedItem
                })
            },
            _normalize: function(e) {
                return e.length && e[0].label && e[0].value ? e : t.map(e, function(e) {
                    return "string" == typeof e ? {
                        label: e,
                        value: e
                    } : t.extend({
                        label: e.label || e.value,
                        value: e.value || e.label
                    }, e)
                })
            },
            _suggest: function(e) {
                var i = this.menu.element.empty().zIndex(this.element.zIndex() + 1);
                this._renderMenu(i, e), this.menu.refresh(), i.show(), this._resizeMenu(), i.position(t.extend({ of: this.element
                }, this.options.position)), this.options.autoFocus && this.menu.next()
            },
            _resizeMenu: function() {
                var t = this.menu.element;
                t.outerWidth(Math.max(t.width("").outerWidth() + 1, this.element.outerWidth()))
            },
            _renderMenu: function(e, i) {
                var n = this;
                t.each(i, function(t, i) {
                    n._renderItemData(e, i)
                })
            },
            _renderItemData: function(t, e) {
                return this._renderItem(t, e).data("ui-autocomplete-item", e)
            },
            _renderItem: function(e, i) {
                return t("<li>").append(t("<a>").text(i.label)).appendTo(e)
            },
            _move: function(t, e) {
                return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(t) || this.menu.isLastItem() && /^next/.test(t) ? (this._value(this.term), void this.menu.blur()) : void this.menu[t](e) : void this.search(null, e)
            },
            widget: function() {
                return this.menu.element
            },
            _value: function() {
                return this.valueMethod.apply(this.element, arguments)
            },
            _keyEvent: function(t, e) {
                this.isMultiLine && !this.menu.element.is(":visible") || (this._move(t, e), e.preventDefault())
            }
        }), t.extend(t.ui.autocomplete, {
            escapeRegex: function(t) {
                return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
            },
            filter: function(e, i) {
                var n = new RegExp(t.ui.autocomplete.escapeRegex(i), "i");
                return t.grep(e, function(t) {
                    return n.test(t.label || t.value || t)
                })
            }
        }), t.widget("ui.autocomplete", t.ui.autocomplete, {
            options: {
                messages: {
                    noResults: "No search results.",
                    results: function(t) {
                        return t + (t > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate."
                    }
                }
            },
            __response: function(t) {
                var e;
                this._superApply(arguments), this.options.disabled || this.cancelSearch || (e = t && t.length ? this.options.messages.results(t.length) : this.options.messages.noResults, this.liveRegion.text(e))
            }
        })
    }(jQuery),
    function(t) {
        var e, i, n, s, a = "ui-button ui-widget ui-state-default ui-corner-all",
            o = "ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",
            r = function() {
                var e = t(this).find(":ui-button");
                setTimeout(function() {
                    e.button("refresh")
                }, 1)
            },
            l = function(e) {
                var i = e.name,
                    n = e.form,
                    s = t([]);
                return i && (s = n ? t(n).find("[name='" + i + "']") : t("[name='" + i + "']", e.ownerDocument).filter(function() {
                    return !this.form
                })), s
            };
        t.widget("ui.button", {
            version: "1.9.1",
            defaultElement: "<button>",
            options: {
                disabled: null,
                text: !0,
                label: null,
                icons: {
                    primary: null,
                    secondary: null
                }
            },
            _create: function() {
                this.element.closest("form").unbind("reset" + this.eventNamespace).bind("reset" + this.eventNamespace, r), "boolean" != typeof this.options.disabled ? this.options.disabled = !!this.element.prop("disabled") : this.element.prop("disabled", this.options.disabled), this._determineButtonType(), this.hasTitle = !!this.buttonElement.attr("title");
                var o = this,
                    c = this.options,
                    u = "checkbox" === this.type || "radio" === this.type,
                    d = "ui-state-hover" + (u ? "" : " ui-state-active"),
                    h = "ui-state-focus";
                null === c.label && (c.label = "input" === this.type ? this.buttonElement.val() : this.buttonElement.html()), this.buttonElement.addClass(a).attr("role", "button").bind("mouseenter" + this.eventNamespace, function() {
                    c.disabled || (t(this).addClass("ui-state-hover"), this === e && t(this).addClass("ui-state-active"))
                }).bind("mouseleave" + this.eventNamespace, function() {
                    c.disabled || t(this).removeClass(d)
                }).bind("click" + this.eventNamespace, function(t) {
                    c.disabled && (t.preventDefault(), t.stopImmediatePropagation())
                }), this.element.bind("focus" + this.eventNamespace, function() {
                    o.buttonElement.addClass(h)
                }).bind("blur" + this.eventNamespace, function() {
                    o.buttonElement.removeClass(h)
                }), u && (this.element.bind("change" + this.eventNamespace, function() {
                    s || o.refresh()
                }), this.buttonElement.bind("mousedown" + this.eventNamespace, function(t) {
                    c.disabled || (s = !1, i = t.pageX, n = t.pageY)
                }).bind("mouseup" + this.eventNamespace, function(t) {
                    c.disabled || i === t.pageX && n === t.pageY || (s = !0)
                })), "checkbox" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function() {
                    if (c.disabled || s) return !1;
                    t(this).toggleClass("ui-state-active"), o.buttonElement.attr("aria-pressed", o.element[0].checked)
                }) : "radio" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function() {
                    if (c.disabled || s) return !1;
                    t(this).addClass("ui-state-active"), o.buttonElement.attr("aria-pressed", "true");
                    var e = o.element[0];
                    l(e).not(e).map(function() {
                        return t(this).button("widget")[0]
                    }).removeClass("ui-state-active").attr("aria-pressed", "false")
                }) : (this.buttonElement.bind("mousedown" + this.eventNamespace, function() {
                    if (c.disabled) return !1;
                    t(this).addClass("ui-state-active"), e = this, o.document.one("mouseup", function() {
                        e = null
                    })
                }).bind("mouseup" + this.eventNamespace, function() {
                    if (c.disabled) return !1;
                    t(this).removeClass("ui-state-active")
                }).bind("keydown" + this.eventNamespace, function(e) {
                    if (c.disabled) return !1;
                    e.keyCode !== t.ui.keyCode.SPACE && e.keyCode !== t.ui.keyCode.ENTER || t(this).addClass("ui-state-active")
                }).bind("keyup" + this.eventNamespace, function() {
                    t(this).removeClass("ui-state-active")
                }), this.buttonElement.is("a") && this.buttonElement.keyup(function(e) {
                    e.keyCode === t.ui.keyCode.SPACE && t(this).click()
                })), this._setOption("disabled", c.disabled), this._resetButton()
            },
            _determineButtonType: function() {
                var t, e, i;
                this.element.is("[type=checkbox]") ? this.type = "checkbox" : this.element.is("[type=radio]") ? this.type = "radio" : this.element.is("input") ? this.type = "input" : this.type = "button", "checkbox" === this.type || "radio" === this.type ? (t = this.element.parents().last(), e = "label[for='" + this.element.attr("id") + "']", this.buttonElement = t.find(e), this.buttonElement.length || (t = t.length ? t.siblings() : this.element.siblings(), this.buttonElement = t.filter(e), this.buttonElement.length || (this.buttonElement = t.find(e))), this.element.addClass("ui-helper-hidden-accessible"), i = this.element.is(":checked"), i && this.buttonElement.addClass("ui-state-active"), this.buttonElement.prop("aria-pressed", i)) : this.buttonElement = this.element
            },
            widget: function() {
                return this.buttonElement
            },
            _destroy: function() {
                this.element.removeClass("ui-helper-hidden-accessible"), this.buttonElement.removeClass(a + " ui-state-hover ui-state-active  " + o).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()), this.hasTitle || this.buttonElement.removeAttr("title")
            },
            _setOption: function(t, e) {
                if (this._super(t, e), "disabled" === t) return void(e ? this.element.prop("disabled", !0) : this.element.prop("disabled", !1));
                this._resetButton()
            },
            refresh: function() {
                var e = this.element.is(":disabled") || this.element.hasClass("ui-button-disabled");
                e !== this.options.disabled && this._setOption("disabled", e), "radio" === this.type ? l(this.element[0]).each(function() {
                    t(this).is(":checked") ? t(this).button("widget").addClass("ui-state-active").attr("aria-pressed", "true") : t(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false")
                }) : "checkbox" === this.type && (this.element.is(":checked") ? this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true") : this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false"))
            },
            _resetButton: function() {
                if ("input" === this.type) return void(this.options.label && this.element.val(this.options.label));
                var e = this.buttonElement.removeClass(o),
                    i = t("<span></span>", this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(e.empty()).text(),
                    n = this.options.icons,
                    s = n.primary && n.secondary,
                    a = [];
                n.primary || n.secondary ? (this.options.text && a.push("ui-button-text-icon" + (s ? "s" : n.primary ? "-primary" : "-secondary")), n.primary && e.prepend("<span class='ui-button-icon-primary ui-icon " + n.primary + "'></span>"), n.secondary && e.append("<span class='ui-button-icon-secondary ui-icon " + n.secondary + "'></span>"), this.options.text || (a.push(s ? "ui-button-icons-only" : "ui-button-icon-only"), this.hasTitle || e.attr("title", t.trim(i)))) : a.push("ui-button-text-only"), e.addClass(a.join(" "))
            }
        }), t.widget("ui.buttonset", {
            version: "1.9.1",
            options: {
                items: "button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(button)"
            },
            _create: function() {
                this.element.addClass("ui-buttonset")
            },
            _init: function() {
                this.refresh()
            },
            _setOption: function(t, e) {
                "disabled" === t && this.buttons.button("option", t, e), this._super(t, e)
            },
            refresh: function() {
                var e = "rtl" === this.element.css("direction");
                this.buttons = this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function() {
                    return t(this).button("widget")[0]
                }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(e ? "ui-corner-right" : "ui-corner-left").end().filter(":last").addClass(e ? "ui-corner-left" : "ui-corner-right").end().end()
            },
            _destroy: function() {
                this.element.removeClass("ui-buttonset"), this.buttons.map(function() {
                    return t(this).button("widget")[0]
                }).removeClass("ui-corner-left ui-corner-right").end().button("destroy")
            }
        })
    }(jQuery),
    function($, undefined) {
        function Datepicker() {
            this.debug = !1, this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {
                closeText: "Done",
                prevText: "Prev",
                nextText: "Next",
                currentText: "Today",
                monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                weekHeader: "Wk",
                dateFormat: "mm/dd/yy",
                firstDay: 0,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, this._defaults = {
                showOn: "focus",
                showAnim: "fadeIn",
                showOptions: {},
                defaultDate: null,
                appendText: "",
                buttonText: "...",
                buttonImage: "",
                buttonImageOnly: !1,
                hideIfNoPrevNext: !1,
                navigationAsDateFormat: !1,
                gotoCurrent: !1,
                changeMonth: !1,
                changeYear: !1,
                yearRange: "c-10:c+10",
                showOtherMonths: !1,
                selectOtherMonths: !1,
                showWeek: !1,
                calculateWeek: this.iso8601Week,
                shortYearCutoff: "+10",
                minDate: null,
                maxDate: null,
                duration: "fast",
                beforeShowDay: null,
                beforeShow: null,
                onSelect: null,
                onChangeMonthYear: null,
                onClose: null,
                numberOfMonths: 1,
                showCurrentAtPos: 0,
                stepMonths: 1,
                stepBigMonths: 12,
                altField: "",
                altFormat: "",
                constrainInput: !0,
                showButtonPanel: !1,
                autoSize: !1,
                disabled: !1
            }, $.extend(this._defaults, this.regional[""]), this.dpDiv = bindHover($('<div id="' + this._mainDivId + '" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>'))
        }

        function bindHover(t) {
            var e = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
            return t.delegate(e, "mouseout", function() {
                $(this).removeClass("ui-state-hover"), -1 != this.className.indexOf("ui-datepicker-prev") && $(this).removeClass("ui-datepicker-prev-hover"), -1 != this.className.indexOf("ui-datepicker-next") && $(this).removeClass("ui-datepicker-next-hover")
            }).delegate(e, "mouseover", function() {
                $.datepicker._isDisabledDatepicker(instActive.inline ? t.parent()[0] : instActive.input[0]) || ($(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), $(this).addClass("ui-state-hover"), -1 != this.className.indexOf("ui-datepicker-prev") && $(this).addClass("ui-datepicker-prev-hover"), -1 != this.className.indexOf("ui-datepicker-next") && $(this).addClass("ui-datepicker-next-hover"))
            })
        }

        function extendRemove(t, e) {
            $.extend(t, e);
            for (var i in e) null != e[i] && e[i] != undefined || (t[i] = e[i]);
            return t
        }
        $.extend($.ui, {
            datepicker: {
                version: "1.9.1"
            }
        });
        var PROP_NAME = "datepicker",
            dpuuid = (new Date).getTime(),
            instActive;
        $.extend(Datepicker.prototype, {
            markerClassName: "hasDatepicker",
            maxRows: 4,
            log: function() {
                this.debug && console.log.apply("", arguments)
            },
            _widgetDatepicker: function() {
                return this.dpDiv
            },
            setDefaults: function(t) {
                return extendRemove(this._defaults, t || {}), this
            },
            _attachDatepicker: function(target, settings) {
                var inlineSettings = null;
                for (var attrName in this._defaults) {
                    var attrValue = target.getAttribute("date:" + attrName);
                    if (attrValue) {
                        inlineSettings = inlineSettings || {};
                        try {
                            inlineSettings[attrName] = eval(attrValue)
                        } catch (t) {
                            inlineSettings[attrName] = attrValue
                        }
                    }
                }
                var nodeName = target.nodeName.toLowerCase(),
                    inline = "div" == nodeName || "span" == nodeName;
                target.id || (this.uuid += 1, target.id = "dp" + this.uuid);
                var inst = this._newInst($(target), inline);
                inst.settings = $.extend({}, settings || {}, inlineSettings || {}), "input" == nodeName ? this._connectDatepicker(target, inst) : inline && this._inlineDatepicker(target, inst)
            },
            _newInst: function(t, e) {
                return {
                    id: t[0].id.replace(/([^A-Za-z0-9_-])/g, "\\\\$1"),
                    input: t,
                    selectedDay: 0,
                    selectedMonth: 0,
                    selectedYear: 0,
                    drawMonth: 0,
                    drawYear: 0,
                    inline: e,
                    dpDiv: e ? bindHover($('<div class="' + this._inlineClass + ' ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>')) : this.dpDiv
                }
            },
            _connectDatepicker: function(t, e) {
                var i = $(t);
                e.append = $([]), e.trigger = $([]), i.hasClass(this.markerClassName) || (this._attachments(i, e), i.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp).bind("setData.datepicker", function(t, i, n) {
                    e.settings[i] = n
                }).bind("getData.datepicker", function(t, i) {
                    return this._get(e, i)
                }), this._autoSize(e), $.data(t, PROP_NAME, e), e.settings.disabled && this._disableDatepicker(t))
            },
            _attachments: function(t, e) {
                var i = this._get(e, "appendText"),
                    n = this._get(e, "isRTL");
                e.append && e.append.remove(), i && (e.append = $('<span class="' + this._appendClass + '">' + i + "</span>"), t[n ? "before" : "after"](e.append)), t.unbind("focus", this._showDatepicker), e.trigger && e.trigger.remove();
                var s = this._get(e, "showOn");
                if ("focus" != s && "both" != s || t.focus(this._showDatepicker), "button" == s || "both" == s) {
                    var a = this._get(e, "buttonText"),
                        o = this._get(e, "buttonImage");
                    e.trigger = $(this._get(e, "buttonImageOnly") ? $("<img/>").addClass(this._triggerClass).attr({
                        src: o,
                        alt: a,
                        title: a
                    }) : $('<button type="button"></button>').addClass(this._triggerClass).html("" == o ? a : $("<img/>").attr({
                        src: o,
                        alt: a,
                        title: a
                    }))), t[n ? "before" : "after"](e.trigger), e.trigger.click(function() {
                        return $.datepicker._datepickerShowing && $.datepicker._lastInput == t[0] ? $.datepicker._hideDatepicker() : $.datepicker._datepickerShowing && $.datepicker._lastInput != t[0] ? ($.datepicker._hideDatepicker(), $.datepicker._showDatepicker(t[0])) : $.datepicker._showDatepicker(t[0]), !1
                    })
                }
            },
            _autoSize: function(t) {
                if (this._get(t, "autoSize") && !t.inline) {
                    var e = new Date(2009, 11, 20),
                        i = this._get(t, "dateFormat");
                    if (i.match(/[DM]/)) {
                        var n = function(t) {
                            for (var e = 0, i = 0, n = 0; n < t.length; n++) t[n].length > e && (e = t[n].length, i = n);
                            return i
                        };
                        e.setMonth(n(this._get(t, i.match(/MM/) ? "monthNames" : "monthNamesShort"))), e.setDate(n(this._get(t, i.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - e.getDay())
                    }
                    t.input.attr("size", this._formatDate(t, e).length)
                }
            },
            _inlineDatepicker: function(t, e) {
                var i = $(t);
                i.hasClass(this.markerClassName) || (i.addClass(this.markerClassName).append(e.dpDiv).bind("setData.datepicker", function(t, i, n) {
                    e.settings[i] = n
                }).bind("getData.datepicker", function(t, i) {
                    return this._get(e, i)
                }), $.data(t, PROP_NAME, e), this._setDate(e, this._getDefaultDate(e), !0), this._updateDatepicker(e), this._updateAlternate(e), e.settings.disabled && this._disableDatepicker(t), e.dpDiv.css("display", "block"))
            },
            _dialogDatepicker: function(t, e, i, n, s) {
                var a = this._dialogInst;
                if (!a) {
                    this.uuid += 1;
                    var o = "dp" + this.uuid;
                    this._dialogInput = $('<input type="text" id="' + o + '" style="position: absolute; top: -100px; width: 0px;"/>'), this._dialogInput.keydown(this._doKeyDown), $("body").append(this._dialogInput), a = this._dialogInst = this._newInst(this._dialogInput, !1), a.settings = {}, $.data(this._dialogInput[0], PROP_NAME, a)
                }
                if (extendRemove(a.settings, n || {}), e = e && e.constructor == Date ? this._formatDate(a, e) : e, this._dialogInput.val(e), this._pos = s ? s.length ? s : [s.pageX, s.pageY] : null, !this._pos) {
                    var r = document.documentElement.clientWidth,
                        l = document.documentElement.clientHeight,
                        c = document.documentElement.scrollLeft || document.body.scrollLeft,
                        u = document.documentElement.scrollTop || document.body.scrollTop;
                    this._pos = [r / 2 - 100 + c, l / 2 - 150 + u]
                }
                return this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), a.settings.onSelect = i, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), $.blockUI && $.blockUI(this.dpDiv), $.data(this._dialogInput[0], PROP_NAME, a), this
            },
            _destroyDatepicker: function(t) {
                var e = $(t),
                    i = $.data(t, PROP_NAME);
                if (e.hasClass(this.markerClassName)) {
                    var n = t.nodeName.toLowerCase();
                    $.removeData(t, PROP_NAME), "input" == n ? (i.append.remove(), i.trigger.remove(), e.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)) : "div" != n && "span" != n || e.removeClass(this.markerClassName).empty()
                }
            },
            _enableDatepicker: function(t) {
                var e = $(t),
                    i = $.data(t, PROP_NAME);
                if (e.hasClass(this.markerClassName)) {
                    var n = t.nodeName.toLowerCase();
                    if ("input" == n) t.disabled = !1, i.trigger.filter("button").each(function() {
                        this.disabled = !1
                    }).end().filter("img").css({
                        opacity: "1.0",
                        cursor: ""
                    });
                    else if ("div" == n || "span" == n) {
                        var s = e.children("." + this._inlineClass);
                        s.children().removeClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)
                    }
                    this._disabledInputs = $.map(this._disabledInputs, function(e) {
                        return e == t ? null : e
                    })
                }
            },
            _disableDatepicker: function(t) {
                var e = $(t),
                    i = $.data(t, PROP_NAME);
                if (e.hasClass(this.markerClassName)) {
                    var n = t.nodeName.toLowerCase();
                    if ("input" == n) t.disabled = !0, i.trigger.filter("button").each(function() {
                        this.disabled = !0
                    }).end().filter("img").css({
                        opacity: "0.5",
                        cursor: "default"
                    });
                    else if ("div" == n || "span" == n) {
                        var s = e.children("." + this._inlineClass);
                        s.children().addClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)
                    }
                    this._disabledInputs = $.map(this._disabledInputs, function(e) {
                        return e == t ? null : e
                    }), this._disabledInputs[this._disabledInputs.length] = t
                }
            },
            _isDisabledDatepicker: function(t) {
                if (!t) return !1;
                for (var e = 0; e < this._disabledInputs.length; e++)
                    if (this._disabledInputs[e] == t) return !0;
                return !1
            },
            _getInst: function(t) {
                try {
                    return $.data(t, PROP_NAME)
                } catch (t) {
                    throw "Missing instance data for this datepicker"
                }
            },
            _optionDatepicker: function(t, e, i) {
                var n = this._getInst(t);
                if (2 == arguments.length && "string" == typeof e) return "defaults" == e ? $.extend({}, $.datepicker._defaults) : n ? "all" == e ? $.extend({}, n.settings) : this._get(n, e) : null;
                var s = e || {};
                if ("string" == typeof e && (s = {}, s[e] = i), n) {
                    this._curInst == n && this._hideDatepicker();
                    var a = this._getDateDatepicker(t, !0),
                        o = this._getMinMaxDate(n, "min"),
                        r = this._getMinMaxDate(n, "max");
                    extendRemove(n.settings, s), null !== o && s.dateFormat !== undefined && s.minDate === undefined && (n.settings.minDate = this._formatDate(n, o)), null !== r && s.dateFormat !== undefined && s.maxDate === undefined && (n.settings.maxDate = this._formatDate(n, r)), this._attachments($(t), n), this._autoSize(n), this._setDate(n, a), this._updateAlternate(n), this._updateDatepicker(n)
                }
            },
            _changeDatepicker: function(t, e, i) {
                this._optionDatepicker(t, e, i)
            },
            _refreshDatepicker: function(t) {
                var e = this._getInst(t);
                e && this._updateDatepicker(e)
            },
            _setDateDatepicker: function(t, e) {
                var i = this._getInst(t);
                i && (this._setDate(i, e), this._updateDatepicker(i), this._updateAlternate(i))
            },
            _getDateDatepicker: function(t, e) {
                var i = this._getInst(t);
                return i && !i.inline && this._setDateFromField(i, e), i ? this._getDate(i) : null
            },
            _doKeyDown: function(t) {
                var e = $.datepicker._getInst(t.target),
                    i = !0,
                    n = e.dpDiv.is(".ui-datepicker-rtl");
                if (e._keyEvent = !0, $.datepicker._datepickerShowing) switch (t.keyCode) {
                    case 9:
                        $.datepicker._hideDatepicker(), i = !1;
                        break;
                    case 13:
                        var s = $("td." + $.datepicker._dayOverClass + ":not(." + $.datepicker._currentClass + ")", e.dpDiv);
                        s[0] && $.datepicker._selectDay(t.target, e.selectedMonth, e.selectedYear, s[0]);
                        var a = $.datepicker._get(e, "onSelect");
                        if (a) {
                            var o = $.datepicker._formatDate(e);
                            a.apply(e.input ? e.input[0] : null, [o, e])
                        } else $.datepicker._hideDatepicker();
                        return !1;
                    case 27:
                        $.datepicker._hideDatepicker();
                        break;
                    case 33:
                        $.datepicker._adjustDate(t.target, t.ctrlKey ? -$.datepicker._get(e, "stepBigMonths") : -$.datepicker._get(e, "stepMonths"), "M");
                        break;
                    case 34:
                        $.datepicker._adjustDate(t.target, t.ctrlKey ? +$.datepicker._get(e, "stepBigMonths") : +$.datepicker._get(e, "stepMonths"), "M");
                        break;
                    case 35:
                        (t.ctrlKey || t.metaKey) && $.datepicker._clearDate(t.target), i = t.ctrlKey || t.metaKey;
                        break;
                    case 36:
                        (t.ctrlKey || t.metaKey) && $.datepicker._gotoToday(t.target), i = t.ctrlKey || t.metaKey;
                        break;
                    case 37:
                        (t.ctrlKey || t.metaKey) && $.datepicker._adjustDate(t.target, n ? 1 : -1, "D"), i = t.ctrlKey || t.metaKey, t.originalEvent.altKey && $.datepicker._adjustDate(t.target, t.ctrlKey ? -$.datepicker._get(e, "stepBigMonths") : -$.datepicker._get(e, "stepMonths"), "M");
                        break;
                    case 38:
                        (t.ctrlKey || t.metaKey) && $.datepicker._adjustDate(t.target, -7, "D"), i = t.ctrlKey || t.metaKey;
                        break;
                    case 39:
                        (t.ctrlKey || t.metaKey) && $.datepicker._adjustDate(t.target, n ? -1 : 1, "D"), i = t.ctrlKey || t.metaKey, t.originalEvent.altKey && $.datepicker._adjustDate(t.target, t.ctrlKey ? +$.datepicker._get(e, "stepBigMonths") : +$.datepicker._get(e, "stepMonths"), "M");
                        break;
                    case 40:
                        (t.ctrlKey || t.metaKey) && $.datepicker._adjustDate(t.target, 7, "D"), i = t.ctrlKey || t.metaKey;
                        break;
                    default:
                        i = !1
                } else 36 == t.keyCode && t.ctrlKey ? $.datepicker._showDatepicker(this) : i = !1;
                i && (t.preventDefault(), t.stopPropagation())
            },
            _doKeyPress: function(t) {
                var e = $.datepicker._getInst(t.target);
                if ($.datepicker._get(e, "constrainInput")) {
                    var i = $.datepicker._possibleChars($.datepicker._get(e, "dateFormat")),
                        n = String.fromCharCode(t.charCode == undefined ? t.keyCode : t.charCode);
                    return t.ctrlKey || t.metaKey || n < " " || !i || i.indexOf(n) > -1
                }
            },
            _doKeyUp: function(t) {
                var e = $.datepicker._getInst(t.target);
                if (e.input.val() != e.lastVal) try {
                    $.datepicker.parseDate($.datepicker._get(e, "dateFormat"), e.input ? e.input.val() : null, $.datepicker._getFormatConfig(e)) && ($.datepicker._setDateFromField(e), $.datepicker._updateAlternate(e), $.datepicker._updateDatepicker(e))
                } catch (t) {
                    $.datepicker.log(t)
                }
                return !0
            },
            _showDatepicker: function(t) {
                if (t = t.target || t, "input" != t.nodeName.toLowerCase() && (t = $("input", t.parentNode)[0]), !$.datepicker._isDisabledDatepicker(t) && $.datepicker._lastInput != t) {
                    var e = $.datepicker._getInst(t);
                    $.datepicker._curInst && $.datepicker._curInst != e && ($.datepicker._curInst.dpDiv.stop(!0, !0), e && $.datepicker._datepickerShowing && $.datepicker._hideDatepicker($.datepicker._curInst.input[0]));
                    var i = $.datepicker._get(e, "beforeShow"),
                        n = i ? i.apply(t, [t, e]) : {};
                    if (!1 !== n) {
                        extendRemove(e.settings, n), e.lastVal = null, $.datepicker._lastInput = t, $.datepicker._setDateFromField(e), $.datepicker._inDialog && (t.value = ""), $.datepicker._pos || ($.datepicker._pos = $.datepicker._findPos(t), $.datepicker._pos[1] += t.offsetHeight);
                        var s = !1;
                        $(t).parents().each(function() {
                            return !(s |= "fixed" == $(this).css("position"))
                        });
                        var a = {
                            left: $.datepicker._pos[0],
                            top: $.datepicker._pos[1]
                        };
                        if ($.datepicker._pos = null, e.dpDiv.empty(), e.dpDiv.css({
                                position: "absolute",
                                display: "block",
                                top: "-1000px"
                            }), $.datepicker._updateDatepicker(e), a = $.datepicker._checkOffset(e, a, s), e.dpDiv.css({
                                position: $.datepicker._inDialog && $.blockUI ? "static" : s ? "fixed" : "absolute",
                                display: "none",
                                left: a.left + "px",
                                top: a.top + "px"
                            }), !e.inline) {
                            var o = $.datepicker._get(e, "showAnim"),
                                r = $.datepicker._get(e, "duration"),
                                l = function() {
                                    var t = e.dpDiv.find("iframe.ui-datepicker-cover");
                                    if (t.length) {
                                        var i = $.datepicker._getBorders(e.dpDiv);
                                        t.css({
                                            left: -i[0],
                                            top: -i[1],
                                            width: e.dpDiv.outerWidth(),
                                            height: e.dpDiv.outerHeight()
                                        })
                                    }
                                };
                            e.dpDiv.zIndex($(t).zIndex() + 1), $.datepicker._datepickerShowing = !0, $.effects && ($.effects.effect[o] || $.effects[o]) ? e.dpDiv.show(o, $.datepicker._get(e, "showOptions"), r, l) : e.dpDiv[o || "show"](o ? r : null, l), o && r || l(), e.input.is(":visible") && !e.input.is(":disabled") && e.input.focus(), $.datepicker._curInst = e
                        }
                    }
                }
            },
            _updateDatepicker: function(t) {
                this.maxRows = 4;
                var e = $.datepicker._getBorders(t.dpDiv);
                instActive = t, t.dpDiv.empty().append(this._generateHTML(t)), this._attachHandlers(t);
                var i = t.dpDiv.find("iframe.ui-datepicker-cover");
                i.length && i.css({
                    left: -e[0],
                    top: -e[1],
                    width: t.dpDiv.outerWidth(),
                    height: t.dpDiv.outerHeight()
                }), t.dpDiv.find("." + this._dayOverClass + " a").mouseover();
                var n = this._getNumberOfMonths(t),
                    s = n[1];
                if (t.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), s > 1 && t.dpDiv.addClass("ui-datepicker-multi-" + s).css("width", 17 * s + "em"), t.dpDiv[(1 != n[0] || 1 != n[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), t.dpDiv[(this._get(t, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), t == $.datepicker._curInst && $.datepicker._datepickerShowing && t.input && t.input.is(":visible") && !t.input.is(":disabled") && t.input[0] != document.activeElement && t.input.focus(), t.yearshtml) {
                    var a = t.yearshtml;
                    setTimeout(function() {
                        a === t.yearshtml && t.yearshtml && t.dpDiv.find("select.ui-datepicker-year:first").replaceWith(t.yearshtml), a = t.yearshtml = null
                    }, 0)
                }
            },
            _getBorders: function(t) {
                var e = function(t) {
                    return {
                        thin: 1,
                        medium: 2,
                        thick: 3
                    } [t] || t
                };
                return [parseFloat(e(t.css("border-left-width"))), parseFloat(e(t.css("border-top-width")))]
            },
            _checkOffset: function(t, e, i) {
                var n = t.dpDiv.outerWidth(),
                    s = t.dpDiv.outerHeight(),
                    a = t.input ? t.input.outerWidth() : 0,
                    o = t.input ? t.input.outerHeight() : 0,
                    r = document.documentElement.clientWidth + (i ? 0 : $(document).scrollLeft()),
                    l = document.documentElement.clientHeight + (i ? 0 : $(document).scrollTop());
                return e.left -= this._get(t, "isRTL") ? n - a : 0, e.left -= i && e.left == t.input.offset().left ? $(document).scrollLeft() : 0, e.top -= i && e.top == t.input.offset().top + o ? $(document).scrollTop() : 0, e.left -= Math.min(e.left, e.left + n > r && r > n ? Math.abs(e.left + n - r) : 0), e.top -= Math.min(e.top, e.top + s > l && l > s ? Math.abs(s + o) : 0), e
            },
            _findPos: function(t) {
                for (var e = this._getInst(t), i = this._get(e, "isRTL"); t && ("hidden" == t.type || 1 != t.nodeType || $.expr.filters.hidden(t));) t = t[i ? "previousSibling" : "nextSibling"];
                var n = $(t).offset();
                return [n.left, n.top]
            },
            _hideDatepicker: function(t) {
                var e = this._curInst;
                if (e && (!t || e == $.data(t, PROP_NAME)) && this._datepickerShowing) {
                    var i = this._get(e, "showAnim"),
                        n = this._get(e, "duration"),
                        s = function() {
                            $.datepicker._tidyDialog(e)
                        };
                    $.effects && ($.effects.effect[i] || $.effects[i]) ? e.dpDiv.hide(i, $.datepicker._get(e, "showOptions"), n, s) : e.dpDiv["slideDown" == i ? "slideUp" : "fadeIn" == i ? "fadeOut" : "hide"](i ? n : null, s), i || s(), this._datepickerShowing = !1;
                    var a = this._get(e, "onClose");
                    a && a.apply(e.input ? e.input[0] : null, [e.input ? e.input.val() : "", e]), this._lastInput = null, this._inDialog && (this._dialogInput.css({
                        position: "absolute",
                        left: "0",
                        top: "-100px"
                    }), $.blockUI && ($.unblockUI(), $("body").append(this.dpDiv))), this._inDialog = !1
                }
            },
            _tidyDialog: function(t) {
                t.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")
            },
            _checkExternalClick: function(t) {
                if ($.datepicker._curInst) {
                    var e = $(t.target),
                        i = $.datepicker._getInst(e[0]);
                    (e[0].id == $.datepicker._mainDivId || 0 != e.parents("#" + $.datepicker._mainDivId).length || e.hasClass($.datepicker.markerClassName) || e.closest("." + $.datepicker._triggerClass).length || !$.datepicker._datepickerShowing || $.datepicker._inDialog && $.blockUI) && (!e.hasClass($.datepicker.markerClassName) || $.datepicker._curInst == i) || $.datepicker._hideDatepicker()
                }
            },
            _adjustDate: function(t, e, i) {
                var n = $(t),
                    s = this._getInst(n[0]);
                this._isDisabledDatepicker(n[0]) || (this._adjustInstDate(s, e + ("M" == i ? this._get(s, "showCurrentAtPos") : 0), i), this._updateDatepicker(s))
            },
            _gotoToday: function(t) {
                var e = $(t),
                    i = this._getInst(e[0]);
                if (this._get(i, "gotoCurrent") && i.currentDay) i.selectedDay = i.currentDay, i.drawMonth = i.selectedMonth = i.currentMonth, i.drawYear = i.selectedYear = i.currentYear;
                else {
                    var n = new Date;
                    i.selectedDay = n.getDate(), i.drawMonth = i.selectedMonth = n.getMonth(), i.drawYear = i.selectedYear = n.getFullYear()
                }
                this._notifyChange(i), this._adjustDate(e)
            },
            _selectMonthYear: function(t, e, i) {
                var n = $(t),
                    s = this._getInst(n[0]);
                s["selected" + ("M" == i ? "Month" : "Year")] = s["draw" + ("M" == i ? "Month" : "Year")] = parseInt(e.options[e.selectedIndex].value, 10), this._notifyChange(s), this._adjustDate(n)
            },
            _selectDay: function(t, e, i, n) {
                var s = $(t);
                if (!$(n).hasClass(this._unselectableClass) && !this._isDisabledDatepicker(s[0])) {
                    var a = this._getInst(s[0]);
                    a.selectedDay = a.currentDay = $("a", n).html(), a.selectedMonth = a.currentMonth = e, a.selectedYear = a.currentYear = i, this._selectDate(t, this._formatDate(a, a.currentDay, a.currentMonth, a.currentYear))
                }
            },
            _clearDate: function(t) {
                var e = $(t);
                this._getInst(e[0]);
                this._selectDate(e, "")
            },
            _selectDate: function(t, e) {
                var i = $(t),
                    n = this._getInst(i[0]);
                e = null != e ? e : this._formatDate(n), n.input && n.input.val(e), this._updateAlternate(n);
                var s = this._get(n, "onSelect");
                s ? s.apply(n.input ? n.input[0] : null, [e, n]) : n.input && n.input.trigger("change"), n.inline ? this._updateDatepicker(n) : (this._hideDatepicker(), this._lastInput = n.input[0], "object" != typeof n.input[0] && n.input.focus(), this._lastInput = null)
            },
            _updateAlternate: function(t) {
                var e = this._get(t, "altField");
                if (e) {
                    var i = this._get(t, "altFormat") || this._get(t, "dateFormat"),
                        n = this._getDate(t),
                        s = this.formatDate(i, n, this._getFormatConfig(t));
                    $(e).each(function() {
                        $(this).val(s)
                    })
                }
            },
            noWeekends: function(t) {
                var e = t.getDay();
                return [e > 0 && e < 6, ""]
            },
            iso8601Week: function(t) {
                var e = new Date(t.getTime());
                e.setDate(e.getDate() + 4 - (e.getDay() || 7));
                var i = e.getTime();
                return e.setMonth(0), e.setDate(1), Math.floor(Math.round((i - e) / 864e5) / 7) + 1
            },
            parseDate: function(t, e, i) {
                if (null == t || null == e) throw "Invalid arguments";
                if ("" == (e = "object" == typeof e ? e.toString() : e + "")) return null;
                var n = (i ? i.shortYearCutoff : null) || this._defaults.shortYearCutoff;
                n = "string" != typeof n ? n : (new Date).getFullYear() % 100 + parseInt(n, 10);
                for (var s = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort, a = (i ? i.dayNames : null) || this._defaults.dayNames, o = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort, r = (i ? i.monthNames : null) || this._defaults.monthNames, l = -1, c = -1, u = -1, d = -1, h = !1, p = function(e) {
                        var i = v + 1 < t.length && t.charAt(v + 1) == e;
                        return i && v++, i
                    }, f = function(t) {
                        var i = p(t),
                            n = "@" == t ? 14 : "!" == t ? 20 : "y" == t && i ? 4 : "o" == t ? 3 : 2,
                            s = new RegExp("^\\d{1," + n + "}"),
                            a = e.substring(_).match(s);
                        if (!a) throw "Missing number at position " + _;
                        return _ += a[0].length, parseInt(a[0], 10)
                    }, m = function(t, i, n) {
                        var s = $.map(p(t) ? n : i, function(t, e) {
                                return [
                                    [e, t]
                                ]
                            }).sort(function(t, e) {
                                return -(t[1].length - e[1].length)
                            }),
                            a = -1;
                        if ($.each(s, function(t, i) {
                                var n = i[1];
                                if (e.substr(_, n.length).toLowerCase() == n.toLowerCase()) return a = i[0], _ += n.length, !1
                            }), -1 != a) return a + 1;
                        throw "Unknown name at position " + _
                    }, g = function() {
                        if (e.charAt(_) != t.charAt(v)) throw "Unexpected literal at position " + _;
                        _++
                    }, _ = 0, v = 0; v < t.length; v++)
                    if (h) "'" != t.charAt(v) || p("'") ? g() : h = !1;
                    else switch (t.charAt(v)) {
                        case "d":
                            u = f("d");
                            break;
                        case "D":
                            m("D", s, a);
                            break;
                        case "o":
                            d = f("o");
                            break;
                        case "m":
                            c = f("m");
                            break;
                        case "M":
                            c = m("M", o, r);
                            break;
                        case "y":
                            l = f("y");
                            break;
                        case "@":
                            var b = new Date(f("@"));
                            l = b.getFullYear(), c = b.getMonth() + 1, u = b.getDate();
                            break;
                        case "!":
                            var b = new Date((f("!") - this._ticksTo1970) / 1e4);
                            l = b.getFullYear(), c = b.getMonth() + 1, u = b.getDate();
                            break;
                        case "'":
                            p("'") ? g() : h = !0;
                            break;
                        default:
                            g()
                    }
                if (_ < e.length) {
                    var y = e.substr(_);
                    if (!/^\s+/.test(y)) throw "Extra/unparsed characters found in date: " + y
                }
                if (-1 == l ? l = (new Date).getFullYear() : l < 100 && (l += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (l <= n ? 0 : -100)), d > -1)
                    for (c = 1, u = d;;) {
                        var w = this._getDaysInMonth(l, c - 1);
                        if (u <= w) break;
                        c++, u -= w
                    }
                var b = this._daylightSavingAdjust(new Date(l, c - 1, u));
                if (b.getFullYear() != l || b.getMonth() + 1 != c || b.getDate() != u) throw "Invalid date";
                return b
            },
            ATOM: "yy-mm-dd",
            COOKIE: "D, dd M yy",
            ISO_8601: "yy-mm-dd",
            RFC_822: "D, d M y",
            RFC_850: "DD, dd-M-y",
            RFC_1036: "D, d M y",
            RFC_1123: "D, d M yy",
            RFC_2822: "D, d M yy",
            RSS: "D, d M y",
            TICKS: "!",
            TIMESTAMP: "@",
            W3C: "yy-mm-dd",
            _ticksTo1970: 24 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)) * 60 * 60 * 1e7,
            formatDate: function(t, e, i) {
                if (!e) return "";
                var n = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort,
                    s = (i ? i.dayNames : null) || this._defaults.dayNames,
                    a = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort,
                    o = (i ? i.monthNames : null) || this._defaults.monthNames,
                    r = function(e) {
                        var i = h + 1 < t.length && t.charAt(h + 1) == e;
                        return i && h++, i
                    },
                    l = function(t, e, i) {
                        var n = "" + e;
                        if (r(t))
                            for (; n.length < i;) n = "0" + n;
                        return n
                    },
                    c = function(t, e, i, n) {
                        return r(t) ? n[e] : i[e]
                    },
                    u = "",
                    d = !1;
                if (e)
                    for (var h = 0; h < t.length; h++)
                        if (d) "'" != t.charAt(h) || r("'") ? u += t.charAt(h) : d = !1;
                        else switch (t.charAt(h)) {
                            case "d":
                                u += l("d", e.getDate(), 2);
                                break;
                            case "D":
                                u += c("D", e.getDay(), n, s);
                                break;
                            case "o":
                                u += l("o", Math.round((new Date(e.getFullYear(), e.getMonth(), e.getDate()).getTime() - new Date(e.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                                break;
                            case "m":
                                u += l("m", e.getMonth() + 1, 2);
                                break;
                            case "M":
                                u += c("M", e.getMonth(), a, o);
                                break;
                            case "y":
                                u += r("y") ? e.getFullYear() : (e.getYear() % 100 < 10 ? "0" : "") + e.getYear() % 100;
                                break;
                            case "@":
                                u += e.getTime();
                                break;
                            case "!":
                                u += 1e4 * e.getTime() + this._ticksTo1970;
                                break;
                            case "'":
                                r("'") ? u += "'" : d = !0;
                                break;
                            default:
                                u += t.charAt(h)
                        }
                return u
            },
            _possibleChars: function(t) {
                for (var e = "", i = !1, n = function(e) {
                        var i = s + 1 < t.length && t.charAt(s + 1) == e;
                        return i && s++, i
                    }, s = 0; s < t.length; s++)
                    if (i) "'" != t.charAt(s) || n("'") ? e += t.charAt(s) : i = !1;
                    else switch (t.charAt(s)) {
                        case "d":
                        case "m":
                        case "y":
                        case "@":
                            e += "0123456789";
                            break;
                        case "D":
                        case "M":
                            return null;
                        case "'":
                            n("'") ? e += "'" : i = !0;
                            break;
                        default:
                            e += t.charAt(s)
                    }
                return e
            },
            _get: function(t, e) {
                return t.settings[e] !== undefined ? t.settings[e] : this._defaults[e]
            },
            _setDateFromField: function(t, e) {
                if (t.input.val() != t.lastVal) {
                    var i, n, s = this._get(t, "dateFormat"),
                        a = t.lastVal = t.input ? t.input.val() : null;
                    i = n = this._getDefaultDate(t);
                    var o = this._getFormatConfig(t);
                    try {
                        i = this.parseDate(s, a, o) || n
                    } catch (t) {
                        this.log(t), a = e ? "" : a
                    }
                    t.selectedDay = i.getDate(), t.drawMonth = t.selectedMonth = i.getMonth(), t.drawYear = t.selectedYear = i.getFullYear(), t.currentDay = a ? i.getDate() : 0, t.currentMonth = a ? i.getMonth() : 0, t.currentYear = a ? i.getFullYear() : 0, this._adjustInstDate(t)
                }
            },
            _getDefaultDate: function(t) {
                return this._restrictMinMax(t, this._determineDate(t, this._get(t, "defaultDate"), new Date))
            },
            _determineDate: function(t, e, i) {
                var n = function(t) {
                        var e = new Date;
                        return e.setDate(e.getDate() + t), e
                    },
                    s = function(e) {
                        try {
                            return $.datepicker.parseDate($.datepicker._get(t, "dateFormat"), e, $.datepicker._getFormatConfig(t))
                        } catch (t) {}
                        for (var i = (e.toLowerCase().match(/^c/) ? $.datepicker._getDate(t) : null) || new Date, n = i.getFullYear(), s = i.getMonth(), a = i.getDate(), o = /([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, r = o.exec(e); r;) {
                            switch (r[2] || "d") {
                                case "d":
                                case "D":
                                    a += parseInt(r[1], 10);
                                    break;
                                case "w":
                                case "W":
                                    a += 7 * parseInt(r[1], 10);
                                    break;
                                case "m":
                                case "M":
                                    s += parseInt(r[1], 10), a = Math.min(a, $.datepicker._getDaysInMonth(n, s));
                                    break;
                                case "y":
                                case "Y":
                                    n += parseInt(r[1], 10), a = Math.min(a, $.datepicker._getDaysInMonth(n, s))
                            }
                            r = o.exec(e)
                        }
                        return new Date(n, s, a)
                    },
                    a = null == e || "" === e ? i : "string" == typeof e ? s(e) : "number" == typeof e ? isNaN(e) ? i : n(e) : new Date(e.getTime());
                return a = a && "Invalid Date" == a.toString() ? i : a, a && (a.setHours(0), a.setMinutes(0), a.setSeconds(0), a.setMilliseconds(0)), this._daylightSavingAdjust(a)
            },
            _daylightSavingAdjust: function(t) {
                return t ? (t.setHours(t.getHours() > 12 ? t.getHours() + 2 : 0), t) : null
            },
            _setDate: function(t, e, i) {
                var n = !e,
                    s = t.selectedMonth,
                    a = t.selectedYear,
                    o = this._restrictMinMax(t, this._determineDate(t, e, new Date));
                t.selectedDay = t.currentDay = o.getDate(), t.drawMonth = t.selectedMonth = t.currentMonth = o.getMonth(), t.drawYear = t.selectedYear = t.currentYear = o.getFullYear(), s == t.selectedMonth && a == t.selectedYear || i || this._notifyChange(t), this._adjustInstDate(t), t.input && t.input.val(n ? "" : this._formatDate(t))
            },
            _getDate: function(t) {
                return !t.currentYear || t.input && "" == t.input.val() ? null : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay))
            },
            _attachHandlers: function(t) {
                var e = this._get(t, "stepMonths"),
                    i = "#" + t.id.replace(/\\\\/g, "\\");
                t.dpDiv.find("[data-handler]").map(function() {
                    var t = {
                        prev: function() {
                            window["DP_jQuery_" + dpuuid].datepicker._adjustDate(i, -e, "M")
                        },
                        next: function() {
                            window["DP_jQuery_" + dpuuid].datepicker._adjustDate(i, +e, "M")
                        },
                        hide: function() {
                            window["DP_jQuery_" + dpuuid].datepicker._hideDatepicker()
                        },
                        today: function() {
                            window["DP_jQuery_" + dpuuid].datepicker._gotoToday(i)
                        },
                        selectDay: function() {
                            return window["DP_jQuery_" + dpuuid].datepicker._selectDay(i, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1
                        },
                        selectMonth: function() {
                            return window["DP_jQuery_" + dpuuid].datepicker._selectMonthYear(i, this, "M"), !1
                        },
                        selectYear: function() {
                            return window["DP_jQuery_" + dpuuid].datepicker._selectMonthYear(i, this, "Y"), !1
                        }
                    };
                    $(this).bind(this.getAttribute("data-event"), t[this.getAttribute("data-handler")])
                })
            },
            _generateHTML: function(t) {
                var e = new Date;
                e = this._daylightSavingAdjust(new Date(e.getFullYear(), e.getMonth(), e.getDate()));
                var i = this._get(t, "isRTL"),
                    n = this._get(t, "showButtonPanel"),
                    s = this._get(t, "hideIfNoPrevNext"),
                    a = this._get(t, "navigationAsDateFormat"),
                    o = this._getNumberOfMonths(t),
                    r = this._get(t, "showCurrentAtPos"),
                    l = this._get(t, "stepMonths"),
                    c = 1 != o[0] || 1 != o[1],
                    u = this._daylightSavingAdjust(t.currentDay ? new Date(t.currentYear, t.currentMonth, t.currentDay) : new Date(9999, 9, 9)),
                    d = this._getMinMaxDate(t, "min"),
                    h = this._getMinMaxDate(t, "max"),
                    p = t.drawMonth - r,
                    f = t.drawYear;
                if (p < 0 && (p += 12, f--), h) {
                    var m = this._daylightSavingAdjust(new Date(h.getFullYear(), h.getMonth() - o[0] * o[1] + 1, h.getDate()));
                    for (m = d && m < d ? d : m; this._daylightSavingAdjust(new Date(f, p, 1)) > m;) --p < 0 && (p = 11, f--)
                }
                t.drawMonth = p, t.drawYear = f;
                var g = this._get(t, "prevText");
                g = a ? this.formatDate(g, this._daylightSavingAdjust(new Date(f, p - l, 1)), this._getFormatConfig(t)) : g;
                var _ = this._canAdjustMonth(t, -1, f, p) ? '<a class="ui-datepicker-prev ui-corner-all" data-handler="prev" data-event="click" title="' + g + '"><span class="ui-icon ui-icon-circle-triangle-' + (i ? "e" : "w") + '">' + g + "</span></a>" : s ? "" : '<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="' + g + '"><span class="ui-icon ui-icon-circle-triangle-' + (i ? "e" : "w") + '">' + g + "</span></a>",
                    v = this._get(t, "nextText");
                v = a ? this.formatDate(v, this._daylightSavingAdjust(new Date(f, p + l, 1)), this._getFormatConfig(t)) : v;
                var b = this._canAdjustMonth(t, 1, f, p) ? '<a class="ui-datepicker-next ui-corner-all" data-handler="next" data-event="click" title="' + v + '"><span class="ui-icon ui-icon-circle-triangle-' + (i ? "w" : "e") + '">' + v + "</span></a>" : s ? "" : '<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="' + v + '"><span class="ui-icon ui-icon-circle-triangle-' + (i ? "w" : "e") + '">' + v + "</span></a>",
                    y = this._get(t, "currentText"),
                    w = this._get(t, "gotoCurrent") && t.currentDay ? u : e;
                y = a ? this.formatDate(y, w, this._getFormatConfig(t)) : y;
                var x = t.inline ? "" : '<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" data-handler="hide" data-event="click">' + this._get(t, "closeText") + "</button>",
                    k = n ? '<div class="ui-datepicker-buttonpane ui-widget-content">' + (i ? x : "") + (this._isInRange(t, w) ? '<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" data-handler="today" data-event="click">' + y + "</button>" : "") + (i ? "" : x) + "</div>" : "",
                    C = parseInt(this._get(t, "firstDay"), 10);
                C = isNaN(C) ? 0 : C;
                for (var D = this._get(t, "showWeek"), S = this._get(t, "dayNames"), T = (this._get(t, "dayNamesShort"), this._get(t, "dayNamesMin")), I = this._get(t, "monthNames"), A = this._get(t, "monthNamesShort"), E = this._get(t, "beforeShowDay"), M = this._get(t, "showOtherMonths"), P = this._get(t, "selectOtherMonths"), N = (this._get(t, "calculateWeek") || this.iso8601Week, this._getDefaultDate(t)), F = "", j = 0; j < o[0]; j++) {
                    var L = "";
                    this.maxRows = 4;
                    for (var H = 0; H < o[1]; H++) {
                        var O = this._daylightSavingAdjust(new Date(f, p, t.selectedDay)),
                            R = " ui-corner-all",
                            z = "";
                        if (c) {
                            if (z += '<div class="ui-datepicker-group', o[1] > 1) switch (H) {
                                case 0:
                                    z += " ui-datepicker-group-first", R = " ui-corner-" + (i ? "right" : "left");
                                    break;
                                case o[1] - 1:
                                    z += " ui-datepicker-group-last", R = " ui-corner-" + (i ? "left" : "right");
                                    break;
                                default:
                                    z += " ui-datepicker-group-middle", R = ""
                            }
                            z += '">'
                        }
                        z += '<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix' + R + '">' + (/all|left/.test(R) && 0 == j ? i ? b : _ : "") + (/all|right/.test(R) && 0 == j ? i ? _ : b : "") + this._generateMonthYearHeader(t, p, f, d, h, j > 0 || H > 0, I, A) + '</div><table class="ui-datepicker-calendar"><thead><tr>';
                        for (var W = D ? '<th class="ui-datepicker-week-col">' + this._get(t, "weekHeader") + "</th>" : "", B = 0; B < 7; B++) {
                            var q = (B + C) % 7;
                            W += "<th" + ((B + C + 6) % 7 >= 5 ? ' class="ui-datepicker-week-end"' : "") + '><span title="' + S[q] + '">' + T[q] + "</span></th>"
                        }
                        z += W + "</tr></thead><tbody>";
                        var U = this._getDaysInMonth(f, p);
                        f == t.selectedYear && p == t.selectedMonth && (t.selectedDay = Math.min(t.selectedDay, U));
                        var Y = (this._getFirstDayOfMonth(f, p) - C + 7) % 7,
                            V = Math.ceil((Y + U) / 7),
                            K = c && this.maxRows > V ? this.maxRows : V;
                        this.maxRows = K;
                        for (var X = this._daylightSavingAdjust(new Date(f, p, 1 - Y)), Q = 0; Q < K; Q++) {
                            z += "<tr>";
                            for (var G = D ? '<td class="ui-datepicker-week-col">' + this._get(t, "calculateWeek")(X) + "</td>" : "", B = 0; B < 7; B++) {
                                var J = E ? E.apply(t.input ? t.input[0] : null, [X]) : [!0, ""],
                                    Z = X.getMonth() != p,
                                    tt = Z && !P || !J[0] || d && X < d || h && X > h;
                                G += '<td class="' + ((B + C + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (Z ? " ui-datepicker-other-month" : "") + (X.getTime() == O.getTime() && p == t.selectedMonth && t._keyEvent || N.getTime() == X.getTime() && N.getTime() == O.getTime() ? " " + this._dayOverClass : "") + (tt ? " " + this._unselectableClass + " ui-state-disabled" : "") + (Z && !M ? "" : " " + J[1] + (X.getTime() == u.getTime() ? " " + this._currentClass : "") + (X.getTime() == e.getTime() ? " ui-datepicker-today" : "")) + '"' + (Z && !M || !J[2] ? "" : ' title="' + J[2] + '"') + (tt ? "" : ' data-handler="selectDay" data-event="click" data-month="' + X.getMonth() + '" data-year="' + X.getFullYear() + '"') + ">" + (Z && !M ? "&#xa0;" : tt ? '<span class="ui-state-default">' + X.getDate() + "</span>" : '<a class="ui-state-default' + (X.getTime() == e.getTime() ? " ui-state-highlight" : "") + (X.getTime() == u.getTime() ? " ui-state-active" : "") + (Z ? " ui-priority-secondary" : "") + '" href="#">' + X.getDate() + "</a>") + "</td>", X.setDate(X.getDate() + 1), X = this._daylightSavingAdjust(X)
                            }
                            z += G + "</tr>"
                        }
                        p++, p > 11 && (p = 0, f++), z += "</tbody></table>" + (c ? "</div>" + (o[0] > 0 && H == o[1] - 1 ? '<div class="ui-datepicker-row-break"></div>' : "") : ""), L += z
                    }
                    F += L
                }
                return F += k + ($.ui.ie6 && !t.inline ? '<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>' : ""), t._keyEvent = !1, F
            },
            _generateMonthYearHeader: function(t, e, i, n, s, a, o, r) {
                var l = this._get(t, "changeMonth"),
                    c = this._get(t, "changeYear"),
                    u = this._get(t, "showMonthAfterYear"),
                    d = '<div class="ui-datepicker-title">',
                    h = "";
                if (a || !l) h += '<span class="ui-datepicker-month">' + o[e] + "</span>";
                else {
                    var p = n && n.getFullYear() == i,
                        f = s && s.getFullYear() == i;
                    h += '<select class="ui-datepicker-month" data-handler="selectMonth" data-event="change">';
                    for (var m = 0; m < 12; m++)(!p || m >= n.getMonth()) && (!f || m <= s.getMonth()) && (h += '<option value="' + m + '"' + (m == e ? ' selected="selected"' : "") + ">" + r[m] + "</option>");
                    h += "</select>"
                }
                if (u || (d += h + (!a && l && c ? "" : "&#xa0;")), !t.yearshtml)
                    if (t.yearshtml = "", a || !c) d += '<span class="ui-datepicker-year">' + i + "</span>";
                    else {
                        var g = this._get(t, "yearRange").split(":"),
                            _ = (new Date).getFullYear(),
                            v = function(t) {
                                var e = t.match(/c[+-].*/) ? i + parseInt(t.substring(1), 10) : t.match(/[+-].*/) ? _ + parseInt(t, 10) : parseInt(t, 10);
                                return isNaN(e) ? _ : e
                            },
                            b = v(g[0]),
                            y = Math.max(b, v(g[1] || ""));
                        for (b = n ? Math.max(b, n.getFullYear()) : b, y = s ? Math.min(y, s.getFullYear()) : y, t.yearshtml += '<select class="ui-datepicker-year" data-handler="selectYear" data-event="change">'; b <= y; b++) t.yearshtml += '<option value="' + b + '"' + (b == i ? ' selected="selected"' : "") + ">" + b + "</option>";
                        t.yearshtml += "</select>", d += t.yearshtml, t.yearshtml = null
                    }
                return d += this._get(t, "yearSuffix"), u && (d += (!a && l && c ? "" : "&#xa0;") + h), d += "</div>"
            },
            _adjustInstDate: function(t, e, i) {
                var n = t.drawYear + ("Y" == i ? e : 0),
                    s = t.drawMonth + ("M" == i ? e : 0),
                    a = Math.min(t.selectedDay, this._getDaysInMonth(n, s)) + ("D" == i ? e : 0),
                    o = this._restrictMinMax(t, this._daylightSavingAdjust(new Date(n, s, a)));
                t.selectedDay = o.getDate(), t.drawMonth = t.selectedMonth = o.getMonth(), t.drawYear = t.selectedYear = o.getFullYear(), "M" != i && "Y" != i || this._notifyChange(t)
            },
            _restrictMinMax: function(t, e) {
                var i = this._getMinMaxDate(t, "min"),
                    n = this._getMinMaxDate(t, "max"),
                    s = i && e < i ? i : e;
                return s = n && s > n ? n : s
            },
            _notifyChange: function(t) {
                var e = this._get(t, "onChangeMonthYear");
                e && e.apply(t.input ? t.input[0] : null, [t.selectedYear, t.selectedMonth + 1, t])
            },
            _getNumberOfMonths: function(t) {
                var e = this._get(t, "numberOfMonths");
                return null == e ? [1, 1] : "number" == typeof e ? [1, e] : e
            },
            _getMinMaxDate: function(t, e) {
                return this._determineDate(t, this._get(t, e + "Date"), null)
            },
            _getDaysInMonth: function(t, e) {
                return 32 - this._daylightSavingAdjust(new Date(t, e, 32)).getDate()
            },
            _getFirstDayOfMonth: function(t, e) {
                return new Date(t, e, 1).getDay()
            },
            _canAdjustMonth: function(t, e, i, n) {
                var s = this._getNumberOfMonths(t),
                    a = this._daylightSavingAdjust(new Date(i, n + (e < 0 ? e : s[0] * s[1]), 1));
                return e < 0 && a.setDate(this._getDaysInMonth(a.getFullYear(), a.getMonth())), this._isInRange(t, a)
            },
            _isInRange: function(t, e) {
                var i = this._getMinMaxDate(t, "min"),
                    n = this._getMinMaxDate(t, "max");
                return (!i || e.getTime() >= i.getTime()) && (!n || e.getTime() <= n.getTime())
            },
            _getFormatConfig: function(t) {
                var e = this._get(t, "shortYearCutoff");
                return e = "string" != typeof e ? e : (new Date).getFullYear() % 100 + parseInt(e, 10), {
                    shortYearCutoff: e,
                    dayNamesShort: this._get(t, "dayNamesShort"),
                    dayNames: this._get(t, "dayNames"),
                    monthNamesShort: this._get(t, "monthNamesShort"),
                    monthNames: this._get(t, "monthNames")
                }
            },
            _formatDate: function(t, e, i, n) {
                e || (t.currentDay = t.selectedDay, t.currentMonth = t.selectedMonth, t.currentYear = t.selectedYear);
                var s = e ? "object" == typeof e ? e : this._daylightSavingAdjust(new Date(n, i, e)) : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
                return this.formatDate(this._get(t, "dateFormat"), s, this._getFormatConfig(t))
            }
        }), $.fn.datepicker = function(t) {
            if (!this.length) return this;
            $.datepicker.initialized || ($(document).mousedown($.datepicker._checkExternalClick).find(document.body).append($.datepicker.dpDiv), $.datepicker.initialized = !0);
            var e = Array.prototype.slice.call(arguments, 1);
            return "string" != typeof t || "isDisabled" != t && "getDate" != t && "widget" != t ? "option" == t && 2 == arguments.length && "string" == typeof arguments[1] ? $.datepicker["_" + t + "Datepicker"].apply($.datepicker, [this[0]].concat(e)) : this.each(function() {
                "string" == typeof t ? $.datepicker["_" + t + "Datepicker"].apply($.datepicker, [this].concat(e)) : $.datepicker._attachDatepicker(this, t)
            }) : $.datepicker["_" + t + "Datepicker"].apply($.datepicker, [this[0]].concat(e))
        }, $.datepicker = new Datepicker, $.datepicker.initialized = !1, $.datepicker.uuid = (new Date).getTime(), $.datepicker.version = "1.9.1", window["DP_jQuery_" + dpuuid] = $
    }(jQuery),
    function(t, e) {
        var i = "ui-dialog ui-widget ui-widget-content ui-corner-all ",
            n = {
                buttons: !0,
                height: !0,
                maxHeight: !0,
                maxWidth: !0,
                minHeight: !0,
                minWidth: !0,
                width: !0
            },
            s = {
                maxHeight: !0,
                maxWidth: !0,
                minHeight: !0,
                minWidth: !0
            };
        t.widget("ui.dialog", {
            version: "1.9.1",
            options: {
                autoOpen: !0,
                buttons: {},
                closeOnEscape: !0,
                closeText: "close",
                dialogClass: "",
                draggable: !0,
                hide: null,
                height: "auto",
                maxHeight: !1,
                maxWidth: !1,
                minHeight: 150,
                minWidth: 150,
                modal: !1,
                position: {
                    my: "center",
                    at: "center",
                    of: window,
                    collision: "fit",
                    using: function(e) {
                        var i = t(this).css(e).offset().top;
                        i < 0 && t(this).css("top", e.top - i)
                    }
                },
                resizable: !0,
                show: null,
                stack: !0,
                title: "",
                width: 300,
                zIndex: 1e3
            },
            _create: function() {
                this.originalTitle = this.element.attr("title"), "string" != typeof this.originalTitle && (this.originalTitle = ""), this.oldPosition = {
                    parent: this.element.parent(),
                    index: this.element.parent().children().index(this.element)
                }, this.options.title = this.options.title || this.originalTitle;
                var e, n, s, a, o, r = this,
                    l = this.options,
                    c = l.title || "&#160;";
                e = (this.uiDialog = t("<div>")).addClass(i + l.dialogClass).css({
                    display: "none",
                    outline: 0,
                    zIndex: l.zIndex
                }).attr("tabIndex", -1).keydown(function(e) {
                    l.closeOnEscape && !e.isDefaultPrevented() && e.keyCode && e.keyCode === t.ui.keyCode.ESCAPE && (r.close(e), e.preventDefault())
                }).mousedown(function(t) {
                    r.moveToTop(!1, t)
                }).appendTo("body"), this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(e), n = (this.uiDialogTitlebar = t("<div>")).addClass("ui-dialog-titlebar  ui-widget-header  ui-corner-all  ui-helper-clearfix").bind("mousedown", function() {
                    e.focus()
                }).prependTo(e), s = t("<a href='#'></a>").addClass("ui-dialog-titlebar-close  ui-corner-all").attr("role", "button").click(function(t) {
                    t.preventDefault(), r.close(t)
                }).appendTo(n), (this.uiDialogTitlebarCloseText = t("<span>")).addClass("ui-icon ui-icon-closethick").text(l.closeText).appendTo(s), a = t("<span>").uniqueId().addClass("ui-dialog-title").html(c).prependTo(n), o = (this.uiDialogButtonPane = t("<div>")).addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"), (this.uiButtonSet = t("<div>")).addClass("ui-dialog-buttonset").appendTo(o), e.attr({
                    role: "dialog",
                    "aria-labelledby": a.attr("id")
                }), n.find("*").add(n).disableSelection(), this._hoverable(s), this._focusable(s), l.draggable && t.fn.draggable && this._makeDraggable(), l.resizable && t.fn.resizable && this._makeResizable(), this._createButtons(l.buttons), this._isOpen = !1, t.fn.bgiframe && e.bgiframe(), this._on(e, {
                    keydown: function(i) {
                        if (l.modal && i.keyCode === t.ui.keyCode.TAB) {
                            var n = t(":tabbable", e),
                                s = n.filter(":first"),
                                a = n.filter(":last");
                            return i.target !== a[0] || i.shiftKey ? i.target === s[0] && i.shiftKey ? (a.focus(1), !1) : void 0 : (s.focus(1), !1)
                        }
                    }
                })
            },
            _init: function() {
                this.options.autoOpen && this.open()
            },
            _destroy: function() {
                var t, e = this.oldPosition;
                this.overlay && this.overlay.destroy(), this.uiDialog.hide(), this.element.removeClass("ui-dialog-content ui-widget-content").hide().appendTo("body"), this.uiDialog.remove(), this.originalTitle && this.element.attr("title", this.originalTitle), t = e.parent.children().eq(e.index), t.length && t[0] !== this.element[0] ? t.before(this.element) : e.parent.append(this.element)
            },
            widget: function() {
                return this.uiDialog
            },
            close: function(e) {
                var i, n, s = this;
                if (this._isOpen && !1 !== this._trigger("beforeClose", e)) return this._isOpen = !1, this.overlay && this.overlay.destroy(), this.options.hide ? this._hide(this.uiDialog, this.options.hide, function() {
                    s._trigger("close", e)
                }) : (this.uiDialog.hide(), this._trigger("close", e)), t.ui.dialog.overlay.resize(), this.options.modal && (i = 0, t(".ui-dialog").each(function() {
                    this !== s.uiDialog[0] && (n = t(this).css("z-index"), isNaN(n) || (i = Math.max(i, n)))
                }), t.ui.dialog.maxZ = i), this
            },
            isOpen: function() {
                return this._isOpen
            },
            moveToTop: function(e, i) {
                var n, s = this.options;
                return s.modal && !e || !s.stack && !s.modal ? this._trigger("focus", i) : (s.zIndex > t.ui.dialog.maxZ && (t.ui.dialog.maxZ = s.zIndex), this.overlay && (t.ui.dialog.maxZ += 1, t.ui.dialog.overlay.maxZ = t.ui.dialog.maxZ, this.overlay.$el.css("z-index", t.ui.dialog.overlay.maxZ)), n = {
                    scrollTop: this.element.scrollTop(),
                    scrollLeft: this.element.scrollLeft()
                }, t.ui.dialog.maxZ += 1, this.uiDialog.css("z-index", t.ui.dialog.maxZ), this.element.attr(n), this._trigger("focus", i), this)
            },
            open: function() {
                if (!this._isOpen) {
                    var e, i = this.options,
                        n = this.uiDialog;
                    return this._size(), this._position(i.position), n.show(i.show), this.overlay = i.modal ? new t.ui.dialog.overlay(this) : null, this.moveToTop(!0), e = this.element.find(":tabbable"), e.length || (e = this.uiDialogButtonPane.find(":tabbable"), e.length || (e = n)), e.eq(0).focus(), this._isOpen = !0, this._trigger("open"), this
                }
            },
            _createButtons: function(e) {
                var i = this,
                    n = !1;
                this.uiDialogButtonPane.remove(), this.uiButtonSet.empty(), "object" == typeof e && null !== e && t.each(e, function() {
                    return !(n = !0)
                }), n ? (t.each(e, function(e, n) {
                    n = t.isFunction(n) ? {
                        click: n,
                        text: e
                    } : n;
                    var s = t("<button type='button'></button>").attr(n, !0).unbind("click").click(function() {
                        n.click.apply(i.element[0], arguments)
                    }).appendTo(i.uiButtonSet);
                    t.fn.button && s.button()
                }), this.uiDialog.addClass("ui-dialog-buttons"), this.uiDialogButtonPane.appendTo(this.uiDialog)) : this.uiDialog.removeClass("ui-dialog-buttons")
            },
            _makeDraggable: function() {
                function e(t) {
                    return {
                        position: t.position,
                        offset: t.offset
                    }
                }
                var i = this,
                    n = this.options;
                this.uiDialog.draggable({
                    cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
                    handle: ".ui-dialog-titlebar",
                    containment: "document",
                    start: function(n, s) {
                        t(this).addClass("ui-dialog-dragging"), i._trigger("dragStart", n, e(s))
                    },
                    drag: function(t, n) {
                        i._trigger("drag", t, e(n))
                    },
                    stop: function(s, a) {
                        n.position = [a.position.left - i.document.scrollLeft(), a.position.top - i.document.scrollTop()], t(this).removeClass("ui-dialog-dragging"), i._trigger("dragStop", s, e(a)), t.ui.dialog.overlay.resize()
                    }
                })
            },
            _makeResizable: function(i) {
                function n(t) {
                    return {
                        originalPosition: t.originalPosition,
                        originalSize: t.originalSize,
                        position: t.position,
                        size: t.size
                    }
                }
                i = i === e ? this.options.resizable : i;
                var s = this,
                    a = this.options,
                    o = this.uiDialog.css("position"),
                    r = "string" == typeof i ? i : "n,e,s,w,se,sw,ne,nw";
                this.uiDialog.resizable({
                    cancel: ".ui-dialog-content",
                    containment: "document",
                    alsoResize: this.element,
                    maxWidth: a.maxWidth,
                    maxHeight: a.maxHeight,
                    minWidth: a.minWidth,
                    minHeight: this._minHeight(),
                    handles: r,
                    start: function(e, i) {
                        t(this).addClass("ui-dialog-resizing"), s._trigger("resizeStart", e, n(i))
                    },
                    resize: function(t, e) {
                        s._trigger("resize", t, n(e))
                    },
                    stop: function(e, i) {
                        t(this).removeClass("ui-dialog-resizing"), a.height = t(this).height(), a.width = t(this).width(), s._trigger("resizeStop", e, n(i)), t.ui.dialog.overlay.resize()
                    }
                }).css("position", o).find(".ui-resizable-se").addClass("ui-icon ui-icon-grip-diagonal-se")
            },
            _minHeight: function() {
                var t = this.options;
                return "auto" === t.height ? t.minHeight : Math.min(t.minHeight, t.height)
            },
            _position: function(e) {
                var i, n = [],
                    s = [0, 0];
                e ? (("string" == typeof e || "object" == typeof e && "0" in e) && (n = e.split ? e.split(" ") : [e[0], e[1]], 1 === n.length && (n[1] = n[0]), t.each(["left", "top"], function(t, e) {
                    +n[t] === n[t] && (s[t] = n[t], n[t] = e)
                }), e = {
                    my: n[0] + (s[0] < 0 ? s[0] : "+" + s[0]) + " " + n[1] + (s[1] < 0 ? s[1] : "+" + s[1]),
                    at: n.join(" ")
                }), e = t.extend({}, t.ui.dialog.prototype.options.position, e)) : e = t.ui.dialog.prototype.options.position, i = this.uiDialog.is(":visible"), i || this.uiDialog.show(), this.uiDialog.position(e), i || this.uiDialog.hide()
            },
            _setOptions: function(e) {
                var i = this,
                    a = {},
                    o = !1;
                t.each(e, function(t, e) {
                    i._setOption(t, e), t in n && (o = !0), t in s && (a[t] = e)
                }), o && this._size(), this.uiDialog.is(":data(resizable)") && this.uiDialog.resizable("option", a)
            },
            _setOption: function(e, n) {
                var s, a, o = this.uiDialog;
                switch (e) {
                    case "buttons":
                        this._createButtons(n);
                        break;
                    case "closeText":
                        this.uiDialogTitlebarCloseText.text("" + n);
                        break;
                    case "dialogClass":
                        o.removeClass(this.options.dialogClass).addClass(i + n);
                        break;
                    case "disabled":
                        n ? o.addClass("ui-dialog-disabled") : o.removeClass("ui-dialog-disabled");
                        break;
                    case "draggable":
                        s = o.is(":data(draggable)"), s && !n && o.draggable("destroy"), !s && n && this._makeDraggable();
                        break;
                    case "position":
                        this._position(n);
                        break;
                    case "resizable":
                        a = o.is(":data(resizable)"), a && !n && o.resizable("destroy"), a && "string" == typeof n && o.resizable("option", "handles", n), a || !1 === n || this._makeResizable(n);
                        break;
                    case "title":
                        t(".ui-dialog-title", this.uiDialogTitlebar).html("" + (n || "&#160;"))
                }
                this._super(e, n)
            },
            _size: function() {
                var e, i, n, s = this.options,
                    a = this.uiDialog.is(":visible");
                this.element.show().css({
                    width: "auto",
                    minHeight: 0,
                    height: 0
                }), s.minWidth > s.width && (s.width = s.minWidth), e = this.uiDialog.css({
                    height: "auto",
                    width: s.width
                }).outerHeight(), i = Math.max(0, s.minHeight - e), "auto" === s.height ? t.support.minHeight ? this.element.css({
                    minHeight: i,
                    height: "auto"
                }) : (this.uiDialog.show(), n = this.element.css("height", "auto").height(), a || this.uiDialog.hide(), this.element.height(Math.max(n, i))) : this.element.height(Math.max(s.height - e, 0)), this.uiDialog.is(":data(resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight())
            }
        }), t.extend(t.ui.dialog, {
            uuid: 0,
            maxZ: 0,
            getTitleId: function(t) {
                var e = t.attr("id");
                return e || (this.uuid += 1, e = this.uuid), "ui-dialog-title-" + e
            },
            overlay: function(e) {
                this.$el = t.ui.dialog.overlay.create(e)
            }
        }), t.extend(t.ui.dialog.overlay, {
            instances: [],
            oldInstances: [],
            maxZ: 0,
            events: t.map("focus,mousedown,mouseup,keydown,keypress,click".split(","), function(t) {
                return t + ".dialog-overlay"
            }).join(" "),
            create: function(e) {
                0 === this.instances.length && (setTimeout(function() {
                    t.ui.dialog.overlay.instances.length && t(document).bind(t.ui.dialog.overlay.events, function(e) {
                        if (t(e.target).zIndex() < t.ui.dialog.overlay.maxZ) return !1
                    })
                }, 1), t(window).bind("resize.dialog-overlay", t.ui.dialog.overlay.resize));
                var i = this.oldInstances.pop() || t("<div>").addClass("ui-widget-overlay");
                return t(document).bind("keydown.dialog-overlay", function(n) {
                    var s = t.ui.dialog.overlay.instances;
                    0 !== s.length && s[s.length - 1] === i && e.options.closeOnEscape && !n.isDefaultPrevented() && n.keyCode && n.keyCode === t.ui.keyCode.ESCAPE && (e.close(n), n.preventDefault())
                }), i.appendTo(document.body).css({
                    width: this.width(),
                    height: this.height()
                }), t.fn.bgiframe && i.bgiframe(), this.instances.push(i), i
            },
            destroy: function(e) {
                var i = t.inArray(e, this.instances),
                    n = 0; - 1 !== i && this.oldInstances.push(this.instances.splice(i, 1)[0]), 0 === this.instances.length && t([document, window]).unbind(".dialog-overlay"), e.height(0).width(0).remove(), t.each(this.instances, function() {
                    n = Math.max(n, this.css("z-index"))
                }), this.maxZ = n
            },
            height: function() {
                var e, i;
                return t.ui.ie ? (e = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight), i = Math.max(document.documentElement.offsetHeight, document.body.offsetHeight), e < i ? t(window).height() + "px" : e + "px") : t(document).height() + "px"
            },
            width: function() {
                var e, i;
                return t.ui.ie ? (e = Math.max(document.documentElement.scrollWidth, document.body.scrollWidth), i = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth), e < i ? t(window).width() + "px" : e + "px") : t(document).width() + "px"
            },
            resize: function() {
                var e = t([]);
                t.each(t.ui.dialog.overlay.instances, function() {
                    e = e.add(this)
                }), e.css({
                    width: 0,
                    height: 0
                }).css({
                    width: t.ui.dialog.overlay.width(),
                    height: t.ui.dialog.overlay.height()
                })
            }
        }), t.extend(t.ui.dialog.overlay.prototype, {
            destroy: function() {
                t.ui.dialog.overlay.destroy(this.$el)
            }
        })
    }(jQuery),
    function(t) {
        var e = /up|down|vertical/,
            i = /up|left|vertical|horizontal/;
        t.effects.effect.blind = function(n, s) {
            var a, o, r, l = t(this),
                c = ["position", "top", "bottom", "left", "right", "height", "width"],
                u = t.effects.setMode(l, n.mode || "hide"),
                d = n.direction || "up",
                h = e.test(d),
                p = h ? "height" : "width",
                f = h ? "top" : "left",
                m = i.test(d),
                g = {},
                _ = "show" === u;
            l.parent().is(".ui-effects-wrapper") ? t.effects.save(l.parent(), c) : t.effects.save(l, c), l.show(), a = t.effects.createWrapper(l).css({
                overflow: "hidden"
            }), o = a[p](), r = parseFloat(a.css(f)) || 0, g[p] = _ ? o : 0, m || (l.css(h ? "bottom" : "right", 0).css(h ? "top" : "left", "auto").css({
                position: "absolute"
            }), g[f] = _ ? r : o + r), _ && (a.css(p, 0), m || a.css(f, r + o)), a.animate(g, {
                duration: n.duration,
                easing: n.easing,
                queue: !1,
                complete: function() {
                    "hide" === u && l.hide(), t.effects.restore(l, c), t.effects.removeWrapper(l), s()
                }
            })
        }
    }(jQuery),
    function(t) {
        t.effects.effect.bounce = function(e, i) {
            var n, s, a, o = t(this),
                r = ["position", "top", "bottom", "left", "right", "height", "width"],
                l = t.effects.setMode(o, e.mode || "effect"),
                c = "hide" === l,
                u = "show" === l,
                d = e.direction || "up",
                h = e.distance,
                p = e.times || 5,
                f = 2 * p + (u || c ? 1 : 0),
                m = e.duration / f,
                g = e.easing,
                _ = "up" === d || "down" === d ? "top" : "left",
                v = "up" === d || "left" === d,
                b = o.queue(),
                y = b.length;
            for ((u || c) && r.push("opacity"), t.effects.save(o, r), o.show(), t.effects.createWrapper(o), h || (h = o["top" === _ ? "outerHeight" : "outerWidth"]() / 3), u && (a = {
                    opacity: 1
                }, a[_] = 0, o.css("opacity", 0).css(_, v ? 2 * -h : 2 * h).animate(a, m, g)), c && (h /= Math.pow(2, p - 1)), a = {}, a[_] = 0, n = 0; n < p; n++) s = {}, s[_] = (v ? "-=" : "+=") + h, o.animate(s, m, g).animate(a, m, g), h = c ? 2 * h : h / 2;
            c && (s = {
                opacity: 0
            }, s[_] = (v ? "-=" : "+=") + h, o.animate(s, m, g)), o.queue(function() {
                c && o.hide(), t.effects.restore(o, r), t.effects.removeWrapper(o), i()
            }), y > 1 && b.splice.apply(b, [1, 0].concat(b.splice(y, f + 1))), o.dequeue()
        }
    }(jQuery),
    function(t) {
        t.effects.effect.clip = function(e, i) {
            var n, s, a, o = t(this),
                r = ["position", "top", "bottom", "left", "right", "height", "width"],
                l = t.effects.setMode(o, e.mode || "hide"),
                c = "show" === l,
                u = e.direction || "vertical",
                d = "vertical" === u,
                h = d ? "height" : "width",
                p = d ? "top" : "left",
                f = {};
            t.effects.save(o, r), o.show(), n = t.effects.createWrapper(o).css({
                overflow: "hidden"
            }), s = "IMG" === o[0].tagName ? n : o, a = s[h](), c && (s.css(h, 0), s.css(p, a / 2)), f[h] = c ? a : 0, f[p] = c ? 0 : a / 2, s.animate(f, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: function() {
                    c || o.hide(), t.effects.restore(o, r), t.effects.removeWrapper(o), i()
                }
            })
        }
    }(jQuery),
    function(t) {
        t.effects.effect.drop = function(e, i) {
            var n, s = t(this),
                a = ["position", "top", "bottom", "left", "right", "opacity", "height", "width"],
                o = t.effects.setMode(s, e.mode || "hide"),
                r = "show" === o,
                l = e.direction || "left",
                c = "up" === l || "down" === l ? "top" : "left",
                u = "up" === l || "left" === l ? "pos" : "neg",
                d = {
                    opacity: r ? 1 : 0
                };
            t.effects.save(s, a), s.show(), t.effects.createWrapper(s), n = e.distance || s["top" === c ? "outerHeight" : "outerWidth"](!0) / 2, r && s.css("opacity", 0).css(c, "pos" === u ? -n : n), d[c] = (r ? "pos" === u ? "+=" : "-=" : "pos" === u ? "-=" : "+=") + n, s.animate(d, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: function() {
                    "hide" === o && s.hide(), t.effects.restore(s, a), t.effects.removeWrapper(s), i()
                }
            })
        }
    }(jQuery),
    function(t) {
        t.effects.effect.explode = function(e, i) {
            function n() {
                b.push(this), b.length === d * h && s()
            }

            function s() {
                p.css({
                    visibility: "visible"
                }), t(b).remove(), m || p.hide(), i()
            }
            var a, o, r, l, c, u, d = e.pieces ? Math.round(Math.sqrt(e.pieces)) : 3,
                h = d,
                p = t(this),
                f = t.effects.setMode(p, e.mode || "hide"),
                m = "show" === f,
                g = p.show().css("visibility", "hidden").offset(),
                _ = Math.ceil(p.outerWidth() / h),
                v = Math.ceil(p.outerHeight() / d),
                b = [];
            for (a = 0; a < d; a++)
                for (l = g.top + a * v, u = a - (d - 1) / 2, o = 0; o < h; o++) r = g.left + o * _, c = o - (h - 1) / 2, p.clone().appendTo("body").wrap("<div></div>").css({
                    position: "absolute",
                    visibility: "visible",
                    left: -o * _,
                    top: -a * v
                }).parent().addClass("ui-effects-explode").css({
                    position: "absolute",
                    overflow: "hidden",
                    width: _,
                    height: v,
                    left: r + (m ? c * _ : 0),
                    top: l + (m ? u * v : 0),
                    opacity: m ? 0 : 1
                }).animate({
                    left: r + (m ? 0 : c * _),
                    top: l + (m ? 0 : u * v),
                    opacity: m ? 1 : 0
                }, e.duration || 500, e.easing, n)
        }
    }(jQuery),
    function(t) {
        t.effects.effect.fade = function(e, i) {
            var n = t(this),
                s = t.effects.setMode(n, e.mode || "toggle");
            n.animate({
                opacity: s
            }, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: i
            })
        }
    }(jQuery),
    function(t) {
        t.effects.effect.fold = function(e, i) {
            var n, s, a = t(this),
                o = ["position", "top", "bottom", "left", "right", "height", "width"],
                r = t.effects.setMode(a, e.mode || "hide"),
                l = "show" === r,
                c = "hide" === r,
                u = e.size || 15,
                d = /([0-9]+)%/.exec(u),
                h = !!e.horizFirst,
                p = l !== h,
                f = p ? ["width", "height"] : ["height", "width"],
                m = e.duration / 2,
                g = {},
                _ = {};
            t.effects.save(a, o), a.show(), n = t.effects.createWrapper(a).css({
                overflow: "hidden"
            }), s = p ? [n.width(), n.height()] : [n.height(), n.width()], d && (u = parseInt(d[1], 10) / 100 * s[c ? 0 : 1]), l && n.css(h ? {
                height: 0,
                width: u
            } : {
                height: u,
                width: 0
            }), g[f[0]] = l ? s[0] : u, _[f[1]] = l ? s[1] : 0, n.animate(g, m, e.easing).animate(_, m, e.easing, function() {
                c && a.hide(), t.effects.restore(a, o), t.effects.removeWrapper(a), i()
            })
        }
    }(jQuery),
    function(t) {
        t.effects.effect.highlight = function(e, i) {
            var n = t(this),
                s = ["backgroundImage", "backgroundColor", "opacity"],
                a = t.effects.setMode(n, e.mode || "show"),
                o = {
                    backgroundColor: n.css("backgroundColor")
                };
            "hide" === a && (o.opacity = 0), t.effects.save(n, s), n.show().css({
                backgroundImage: "none",
                backgroundColor: e.color || "#ffff99"
            }).animate(o, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: function() {
                    "hide" === a && n.hide(), t.effects.restore(n, s), i()
                }
            })
        }
    }(jQuery),
    function(t) {
        t.effects.effect.pulsate = function(e, i) {
            var n, s = t(this),
                a = t.effects.setMode(s, e.mode || "show"),
                o = "show" === a,
                r = "hide" === a,
                l = o || "hide" === a,
                c = 2 * (e.times || 5) + (l ? 1 : 0),
                u = e.duration / c,
                d = 0,
                h = s.queue(),
                p = h.length;
            for (!o && s.is(":visible") || (s.css("opacity", 0).show(), d = 1), n = 1; n < c; n++) s.animate({
                opacity: d
            }, u, e.easing), d = 1 - d;
            s.animate({
                opacity: d
            }, u, e.easing), s.queue(function() {
                r && s.hide(), i()
            }), p > 1 && h.splice.apply(h, [1, 0].concat(h.splice(p, c + 1))), s.dequeue()
        }
    }(jQuery),
    function(t) {
        t.effects.effect.puff = function(e, i) {
            var n = t(this),
                s = t.effects.setMode(n, e.mode || "hide"),
                a = "hide" === s,
                o = parseInt(e.percent, 10) || 150,
                r = o / 100,
                l = {
                    height: n.height(),
                    width: n.width()
                };
            t.extend(e, {
                effect: "scale",
                queue: !1,
                fade: !0,
                mode: s,
                complete: i,
                percent: a ? o : 100,
                from: a ? l : {
                    height: l.height * r,
                    width: l.width * r
                }
            }), n.effect(e)
        }, t.effects.effect.scale = function(e, i) {
            var n = t(this),
                s = t.extend(!0, {}, e),
                a = t.effects.setMode(n, e.mode || "effect"),
                o = parseInt(e.percent, 10) || (0 === parseInt(e.percent, 10) ? 0 : "hide" === a ? 0 : 100),
                r = e.direction || "both",
                l = e.origin,
                c = {
                    height: n.height(),
                    width: n.width(),
                    outerHeight: n.outerHeight(),
                    outerWidth: n.outerWidth()
                },
                u = {
                    y: "horizontal" !== r ? o / 100 : 1,
                    x: "vertical" !== r ? o / 100 : 1
                };
            s.effect = "size", s.queue = !1, s.complete = i, "effect" !== a && (s.origin = l || ["middle", "center"], s.restore = !0), s.from = e.from || ("show" === a ? {
                height: 0,
                width: 0
            } : c), s.to = {
                height: c.height * u.y,
                width: c.width * u.x,
                outerHeight: c.outerHeight * u.y,
                outerWidth: c.outerWidth * u.x
            }, s.fade && ("show" === a && (s.from.opacity = 0, s.to.opacity = 1), "hide" === a && (s.from.opacity = 1, s.to.opacity = 0)), n.effect(s)
        }, t.effects.effect.size = function(e, i) {
            var n, s, a, o = t(this),
                r = ["position", "top", "bottom", "left", "right", "width", "height", "overflow", "opacity"],
                l = ["position", "top", "bottom", "left", "right", "overflow", "opacity"],
                c = ["width", "height", "overflow"],
                u = ["fontSize"],
                d = ["borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom"],
                h = ["borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight"],
                p = t.effects.setMode(o, e.mode || "effect"),
                f = e.restore || "effect" !== p,
                m = e.scale || "both",
                g = e.origin || ["middle", "center"],
                _ = o.css("position"),
                v = f ? r : l,
                b = {
                    height: 0,
                    width: 0
                };
            "show" === p && o.show(), n = {
                height: o.height(),
                width: o.width(),
                outerHeight: o.outerHeight(),
                outerWidth: o.outerWidth()
            }, "toggle" === e.mode && "show" === p ? (o.from = e.to || b, o.to = e.from || n) : (o.from = e.from || ("show" === p ? b : n), o.to = e.to || ("hide" === p ? b : n)), a = {
                from: {
                    y: o.from.height / n.height,
                    x: o.from.width / n.width
                },
                to: {
                    y: o.to.height / n.height,
                    x: o.to.width / n.width
                }
            }, "box" !== m && "both" !== m || (a.from.y !== a.to.y && (v = v.concat(d), o.from = t.effects.setTransition(o, d, a.from.y, o.from), o.to = t.effects.setTransition(o, d, a.to.y, o.to)), a.from.x !== a.to.x && (v = v.concat(h), o.from = t.effects.setTransition(o, h, a.from.x, o.from), o.to = t.effects.setTransition(o, h, a.to.x, o.to))), "content" !== m && "both" !== m || a.from.y !== a.to.y && (v = v.concat(u).concat(c), o.from = t.effects.setTransition(o, u, a.from.y, o.from), o.to = t.effects.setTransition(o, u, a.to.y, o.to)), t.effects.save(o, v), o.show(), t.effects.createWrapper(o), o.css("overflow", "hidden").css(o.from), g && (s = t.effects.getBaseline(g, n), o.from.top = (n.outerHeight - o.outerHeight()) * s.y, o.from.left = (n.outerWidth - o.outerWidth()) * s.x, o.to.top = (n.outerHeight - o.to.outerHeight) * s.y, o.to.left = (n.outerWidth - o.to.outerWidth) * s.x), o.css(o.from), "content" !== m && "both" !== m || (d = d.concat(["marginTop", "marginBottom"]).concat(u), h = h.concat(["marginLeft", "marginRight"]), c = r.concat(d).concat(h), o.find("*[width]").each(function() {
                var i = t(this),
                    n = {
                        height: i.height(),
                        width: i.width()
                    };
                f && t.effects.save(i, c), i.from = {
                    height: n.height * a.from.y,
                    width: n.width * a.from.x
                }, i.to = {
                    height: n.height * a.to.y,
                    width: n.width * a.to.x
                }, a.from.y !== a.to.y && (i.from = t.effects.setTransition(i, d, a.from.y, i.from), i.to = t.effects.setTransition(i, d, a.to.y, i.to)), a.from.x !== a.to.x && (i.from = t.effects.setTransition(i, h, a.from.x, i.from), i.to = t.effects.setTransition(i, h, a.to.x, i.to)), i.css(i.from), i.animate(i.to, e.duration, e.easing, function() {
                    f && t.effects.restore(i, c)
                })
            })), o.animate(o.to, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: function() {
                    0 === o.to.opacity && o.css("opacity", o.from.opacity), "hide" === p && o.hide(), t.effects.restore(o, v), f || ("static" === _ ? o.css({
                        position: "relative",
                        top: o.to.top,
                        left: o.to.left
                    }) : t.each(["top", "left"], function(t, e) {
                        o.css(e, function(e, i) {
                            var n = parseInt(i, 10),
                                s = t ? o.to.left : o.to.top;
                            return "auto" === i ? s + "px" : n + s + "px"
                        })
                    })), t.effects.removeWrapper(o), i()
                }
            })
        }
    }(jQuery),
    function(t) {
        t.effects.effect.shake = function(e, i) {
            var n, s = t(this),
                a = ["position", "top", "bottom", "left", "right", "height", "width"],
                o = t.effects.setMode(s, e.mode || "effect"),
                r = e.direction || "left",
                l = e.distance || 20,
                c = e.times || 3,
                u = 2 * c + 1,
                d = Math.round(e.duration / u),
                h = "up" === r || "down" === r ? "top" : "left",
                p = "up" === r || "left" === r,
                f = {},
                m = {},
                g = {},
                _ = s.queue(),
                v = _.length;
            for (t.effects.save(s, a), s.show(), t.effects.createWrapper(s), f[h] = (p ? "-=" : "+=") + l, m[h] = (p ? "+=" : "-=") + 2 * l, g[h] = (p ? "-=" : "+=") + 2 * l, s.animate(f, d, e.easing), n = 1; n < c; n++) s.animate(m, d, e.easing).animate(g, d, e.easing);
            s.animate(m, d, e.easing).animate(f, d / 2, e.easing).queue(function() {
                "hide" === o && s.hide(), t.effects.restore(s, a), t.effects.removeWrapper(s), i()
            }), v > 1 && _.splice.apply(_, [1, 0].concat(_.splice(v, u + 1))), s.dequeue()
        }
    }(jQuery),
    function(t) {
        t.effects.effect.slide = function(e, i) {
            var n, s = t(this),
                a = ["position", "top", "bottom", "left", "right", "width", "height"],
                o = t.effects.setMode(s, e.mode || "show"),
                r = "show" === o,
                l = e.direction || "left",
                c = "up" === l || "down" === l ? "top" : "left",
                u = "up" === l || "left" === l,
                d = {};
            t.effects.save(s, a), s.show(), n = e.distance || s["top" === c ? "outerHeight" : "outerWidth"](!0), t.effects.createWrapper(s).css({
                overflow: "hidden"
            }), r && s.css(c, u ? isNaN(n) ? "-" + n : -n : n), d[c] = (r ? u ? "+=" : "-=" : u ? "-=" : "+=") + n, s.animate(d, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: function() {
                    "hide" === o && s.hide(), t.effects.restore(s, a), t.effects.removeWrapper(s), i()
                }
            })
        }
    }(jQuery),
    function(t) {
        t.effects.effect.transfer = function(e, i) {
            var n = t(this),
                s = t(e.to),
                a = "fixed" === s.css("position"),
                o = t("body"),
                r = a ? o.scrollTop() : 0,
                l = a ? o.scrollLeft() : 0,
                c = s.offset(),
                u = {
                    top: c.top - r,
                    left: c.left - l,
                    height: s.innerHeight(),
                    width: s.innerWidth()
                },
                d = n.offset(),
                h = t('<div class="ui-effects-transfer"></div>').appendTo(document.body).addClass(e.className).css({
                    top: d.top - r,
                    left: d.left - l,
                    height: n.innerHeight(),
                    width: n.innerWidth(),
                    position: a ? "fixed" : "absolute"
                }).animate(u, e.duration, e.easing, function() {
                    h.remove(), i()
                })
        }
    }(jQuery),
    function(t) {
        var e = !1;
        t.widget("ui.menu", {
            version: "1.9.1",
            defaultElement: "<ul>",
            delay: 300,
            options: {
                icons: {
                    submenu: "ui-icon-carat-1-e"
                },
                menus: "ul",
                position: {
                    my: "left top",
                    at: "right top"
                },
                role: "menu",
                blur: null,
                focus: null,
                select: null
            },
            _create: function() {
                this.activeMenu = this.element, this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content ui-corner-all").toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length).attr({
                    role: this.options.role,
                    tabIndex: 0
                }).bind("click" + this.eventNamespace, t.proxy(function(t) {
                    this.options.disabled && t.preventDefault()
                }, this)), this.options.disabled && this.element.addClass("ui-state-disabled").attr("aria-disabled", "true"), this._on({
                    "mousedown .ui-menu-item > a": function(t) {
                        t.preventDefault()
                    },
                    "click .ui-state-disabled > a": function(t) {
                        t.preventDefault()
                    },
                    "click .ui-menu-item:has(a)": function(i) {
                        var n = t(i.target).closest(".ui-menu-item");
                        !e && n.not(".ui-state-disabled").length && (e = !0, this.select(i), n.has(".ui-menu").length ? this.expand(i) : this.element.is(":focus") || (this.element.trigger("focus", [!0]), this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)))
                    },
                    "mouseenter .ui-menu-item": function(e) {
                        var i = t(e.currentTarget);
                        i.siblings().children(".ui-state-active").removeClass("ui-state-active"), this.focus(e, i)
                    },
                    mouseleave: "collapseAll",
                    "mouseleave .ui-menu": "collapseAll",
                    focus: function(t, e) {
                        var i = this.active || this.element.children(".ui-menu-item").eq(0);
                        e || this.focus(t, i)
                    },
                    blur: function(e) {
                        this._delay(function() {
                            t.contains(this.element[0], this.document[0].activeElement) || this.collapseAll(e)
                        })
                    },
                    keydown: "_keydown"
                }), this.refresh(), this._on(this.document, {
                    click: function(i) {
                        t(i.target).closest(".ui-menu").length || this.collapseAll(i), e = !1
                    }
                })
            },
            _destroy: function() {
                this.element.removeAttr("aria-activedescendant").find(".ui-menu").andSelf().removeClass("ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(), this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").children("a").removeUniqueId().removeClass("ui-corner-all ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function() {
                    var e = t(this);
                    e.data("ui-menu-submenu-carat") && e.remove()
                }), this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")
            },
            _keydown: function(e) {
                function i(t) {
                    return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
                }
                var n, s, a, o, r, l = !0;
                switch (e.keyCode) {
                    case t.ui.keyCode.PAGE_UP:
                        this.previousPage(e);
                        break;
                    case t.ui.keyCode.PAGE_DOWN:
                        this.nextPage(e);
                        break;
                    case t.ui.keyCode.HOME:
                        this._move("first", "first", e);
                        break;
                    case t.ui.keyCode.END:
                        this._move("last", "last", e);
                        break;
                    case t.ui.keyCode.UP:
                        this.previous(e);
                        break;
                    case t.ui.keyCode.DOWN:
                        this.next(e);
                        break;
                    case t.ui.keyCode.LEFT:
                        this.collapse(e);
                        break;
                    case t.ui.keyCode.RIGHT:
                        this.active && !this.active.is(".ui-state-disabled") && this.expand(e);
                        break;
                    case t.ui.keyCode.ENTER:
                    case t.ui.keyCode.SPACE:
                        this._activate(e);
                        break;
                    case t.ui.keyCode.ESCAPE:
                        this.collapse(e);
                        break;
                    default:
                        l = !1, s = this.previousFilter || "", a = String.fromCharCode(e.keyCode), o = !1, clearTimeout(this.filterTimer), a === s ? o = !0 : a = s + a, r = new RegExp("^" + i(a), "i"), n = this.activeMenu.children(".ui-menu-item").filter(function() {
                            return r.test(t(this).children("a").text())
                        }), n = o && -1 !== n.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : n, n.length || (a = String.fromCharCode(e.keyCode), r = new RegExp("^" + i(a), "i"), n = this.activeMenu.children(".ui-menu-item").filter(function() {
                            return r.test(t(this).children("a").text())
                        })), n.length ? (this.focus(e, n), n.length > 1 ? (this.previousFilter = a, this.filterTimer = this._delay(function() {
                            delete this.previousFilter
                        }, 1e3)) : delete this.previousFilter) : delete this.previousFilter
                }
                l && e.preventDefault()
            },
            _activate: function(t) {
                this.active.is(".ui-state-disabled") || (this.active.children("a[aria-haspopup='true']").length ? this.expand(t) : this.select(t))
            },
            refresh: function() {
                var e, i = this.options.icons.submenu,
                    n = this.element.find(this.options.menus + ":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-corner-all").hide().attr({
                        role: this.options.role,
                        "aria-hidden": "true",
                        "aria-expanded": "false"
                    });
                e = n.add(this.element), e.children(":not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role", "presentation").children("a").uniqueId().addClass("ui-corner-all").attr({
                    tabIndex: -1,
                    role: this._itemRole()
                }), e.children(":not(.ui-menu-item)").each(function() {
                    var e = t(this);
                    /[^\-\u2014\u2013\s]/.test(e.text()) || e.addClass("ui-widget-content ui-menu-divider")
                }), e.children(".ui-state-disabled").attr("aria-disabled", "true"), n.each(function() {
                    var e = t(this),
                        n = e.prev("a"),
                        s = t("<span>").addClass("ui-menu-icon ui-icon " + i).data("ui-menu-submenu-carat", !0);
                    n.attr("aria-haspopup", "true").prepend(s), e.attr("aria-labelledby", n.attr("id"))
                }), this.active && !t.contains(this.element[0], this.active[0]) && this.blur()
            },
            _itemRole: function() {
                return {
                    menu: "menuitem",
                    listbox: "option"
                } [this.options.role]
            },
            focus: function(t, e) {
                var i, n;
                this.blur(t, t && "focus" === t.type), this._scrollIntoView(e), this.active = e.first(), n = this.active.children("a").addClass("ui-state-focus"), this.options.role && this.element.attr("aria-activedescendant", n.attr("id")), this.active.parent().closest(".ui-menu-item").children("a:first").addClass("ui-state-active"), t && "keydown" === t.type ? this._close() : this.timer = this._delay(function() {
                    this._close()
                }, this.delay), i = e.children(".ui-menu"), i.length && /^mouse/.test(t.type) && this._startOpening(i), this.activeMenu = e.parent(), this._trigger("focus", t, {
                    item: e
                })
            },
            _scrollIntoView: function(e) {
                var i, n, s, a, o, r;
                this._hasScroll() && (i = parseFloat(t.css(this.activeMenu[0], "borderTopWidth")) || 0, n = parseFloat(t.css(this.activeMenu[0], "paddingTop")) || 0, s = e.offset().top - this.activeMenu.offset().top - i - n, a = this.activeMenu.scrollTop(), o = this.activeMenu.height(), r = e.height(), s < 0 ? this.activeMenu.scrollTop(a + s) : s + r > o && this.activeMenu.scrollTop(a + s - o + r))
            },
            blur: function(t, e) {
                e || clearTimeout(this.timer), this.active && (this.active.children("a").removeClass("ui-state-focus"), this.active = null, this._trigger("blur", t, {
                    item: this.active
                }))
            },
            _startOpening: function(t) {
                clearTimeout(this.timer), "true" === t.attr("aria-hidden") && (this.timer = this._delay(function() {
                    this._close(), this._open(t)
                }, this.delay))
            },
            _open: function(e) {
                var i = t.extend({ of: this.active
                }, this.options.position);
                clearTimeout(this.timer), this.element.find(".ui-menu").not(e.parents(".ui-menu")).hide().attr("aria-hidden", "true"), e.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(i)
            },
            collapseAll: function(e, i) {
                clearTimeout(this.timer), this.timer = this._delay(function() {
                    var n = i ? this.element : t(e && e.target).closest(this.element.find(".ui-menu"));
                    n.length || (n = this.element), this._close(n), this.blur(e), this.activeMenu = n
                }, this.delay)
            },
            _close: function(t) {
                t || (t = this.active ? this.active.parent() : this.element), t.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false").end().find("a.ui-state-active").removeClass("ui-state-active")
            },
            collapse: function(t) {
                var e = this.active && this.active.parent().closest(".ui-menu-item", this.element);
                e && e.length && (this._close(), this.focus(t, e))
            },
            expand: function(t) {
                var e = this.active && this.active.children(".ui-menu ").children(".ui-menu-item").first();
                e && e.length && (this._open(e.parent()), this._delay(function() {
                    this.focus(t, e)
                }))
            },
            next: function(t) {
                this._move("next", "first", t)
            },
            previous: function(t) {
                this._move("prev", "last", t)
            },
            isFirstItem: function() {
                return this.active && !this.active.prevAll(".ui-menu-item").length
            },
            isLastItem: function() {
                return this.active && !this.active.nextAll(".ui-menu-item").length
            },
            _move: function(t, e, i) {
                var n;
                this.active && (n = "first" === t || "last" === t ? this.active["first" === t ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[t + "All"](".ui-menu-item").eq(0)), n && n.length && this.active || (n = this.activeMenu.children(".ui-menu-item")[e]()), this.focus(i, n)
            },
            nextPage: function(e) {
                var i, n, s;
                if (!this.active) return void this.next(e);
                this.isLastItem() || (this._hasScroll() ? (n = this.active.offset().top, s = this.element.height(), this.active.nextAll(".ui-menu-item").each(function() {
                    return i = t(this), i.offset().top - n - s < 0
                }), this.focus(e, i)) : this.focus(e, this.activeMenu.children(".ui-menu-item")[this.active ? "last" : "first"]()))
            },
            previousPage: function(e) {
                var i, n, s;
                if (!this.active) return void this.next(e);
                this.isFirstItem() || (this._hasScroll() ? (n = this.active.offset().top, s = this.element.height(), this.active.prevAll(".ui-menu-item").each(function() {
                    return i = t(this), i.offset().top - n + s > 0
                }), this.focus(e, i)) : this.focus(e, this.activeMenu.children(".ui-menu-item").first()))
            },
            _hasScroll: function() {
                return this.element.outerHeight() < this.element.prop("scrollHeight")
            },
            select: function(e) {
                this.active = this.active || t(e.target).closest(".ui-menu-item");
                var i = {
                    item: this.active
                };
                this.active.has(".ui-menu").length || this.collapseAll(e, !0), this._trigger("select", e, i)
            }
        })
    }(jQuery),
    function(t, e) {
        function i(t, e, i) {
            return [parseInt(t[0], 10) * (h.test(t[0]) ? e / 100 : 1), parseInt(t[1], 10) * (h.test(t[1]) ? i / 100 : 1)]
        }

        function n(e, i) {
            return parseInt(t.css(e, i), 10) || 0
        }
        t.ui = t.ui || {};
        var s, a = Math.max,
            o = Math.abs,
            r = Math.round,
            l = /left|center|right/,
            c = /top|center|bottom/,
            u = /[\+\-]\d+%?/,
            d = /^\w+/,
            h = /%$/,
            p = t.fn.position;
        t.position = {
                scrollbarWidth: function() {
                    if (s !== e) return s;
                    var i, n, a = t("<div style='display:block;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
                        o = a.children()[0];
                    return t("body").append(a), i = o.offsetWidth, a.css("overflow", "scroll"), n = o.offsetWidth, i === n && (n = a[0].clientWidth), a.remove(), s = i - n
                },
                getScrollInfo: function(e) {
                    var i = e.isWindow ? "" : e.element.css("overflow-x"),
                        n = e.isWindow ? "" : e.element.css("overflow-y"),
                        s = "scroll" === i || "auto" === i && e.width < e.element[0].scrollWidth,
                        a = "scroll" === n || "auto" === n && e.height < e.element[0].scrollHeight;
                    return {
                        width: s ? t.position.scrollbarWidth() : 0,
                        height: a ? t.position.scrollbarWidth() : 0
                    }
                },
                getWithinInfo: function(e) {
                    var i = t(e || window),
                        n = t.isWindow(i[0]);
                    return {
                        element: i,
                        isWindow: n,
                        offset: i.offset() || {
                            left: 0,
                            top: 0
                        },
                        scrollLeft: i.scrollLeft(),
                        scrollTop: i.scrollTop(),
                        width: n ? i.width() : i.outerWidth(),
                        height: n ? i.height() : i.outerHeight()
                    }
                }
            }, t.fn.position = function(e) {
                if (!e || !e.of) return p.apply(this, arguments);
                e = t.extend({}, e);
                var s, h, f, m, g, _ = t(e.of),
                    v = t.position.getWithinInfo(e.within),
                    b = t.position.getScrollInfo(v),
                    y = _[0],
                    w = (e.collision || "flip").split(" "),
                    x = {};
                return 9 === y.nodeType ? (h = _.width(), f = _.height(), m = {
                    top: 0,
                    left: 0
                }) : t.isWindow(y) ? (h = _.width(), f = _.height(), m = {
                    top: _.scrollTop(),
                    left: _.scrollLeft()
                }) : y.preventDefault ? (e.at = "left top", h = f = 0, m = {
                    top: y.pageY,
                    left: y.pageX
                }) : (h = _.outerWidth(), f = _.outerHeight(), m = _.offset()), g = t.extend({}, m), t.each(["my", "at"], function() {
                    var t, i, n = (e[this] || "").split(" ");
                    1 === n.length && (n = l.test(n[0]) ? n.concat(["center"]) : c.test(n[0]) ? ["center"].concat(n) : ["center", "center"]), n[0] = l.test(n[0]) ? n[0] : "center", n[1] = c.test(n[1]) ? n[1] : "center", t = u.exec(n[0]), i = u.exec(n[1]), x[this] = [t ? t[0] : 0, i ? i[0] : 0], e[this] = [d.exec(n[0])[0], d.exec(n[1])[0]]
                }), 1 === w.length && (w[1] = w[0]), "right" === e.at[0] ? g.left += h : "center" === e.at[0] && (g.left += h / 2), "bottom" === e.at[1] ? g.top += f : "center" === e.at[1] && (g.top += f / 2), s = i(x.at, h, f), g.left += s[0], g.top += s[1], this.each(function() {
                    var l, c, u = t(this),
                        d = u.outerWidth(),
                        p = u.outerHeight(),
                        y = n(this, "marginLeft"),
                        $ = n(this, "marginTop"),
                        k = d + y + n(this, "marginRight") + b.width,
                        C = p + $ + n(this, "marginBottom") + b.height,
                        D = t.extend({}, g),
                        S = i(x.my, u.outerWidth(), u.outerHeight());
                    "right" === e.my[0] ? D.left -= d : "center" === e.my[0] && (D.left -= d / 2), "bottom" === e.my[1] ? D.top -= p : "center" === e.my[1] && (D.top -= p / 2), D.left += S[0], D.top += S[1], t.support.offsetFractions || (D.left = r(D.left), D.top = r(D.top)), l = {
                        marginLeft: y,
                        marginTop: $
                    }, t.each(["left", "top"], function(i, n) {
                        t.ui.position[w[i]] && t.ui.position[w[i]][n](D, {
                            targetWidth: h,
                            targetHeight: f,
                            elemWidth: d,
                            elemHeight: p,
                            collisionPosition: l,
                            collisionWidth: k,
                            collisionHeight: C,
                            offset: [s[0] + S[0], s[1] + S[1]],
                            my: e.my,
                            at: e.at,
                            within: v,
                            elem: u
                        })
                    }), t.fn.bgiframe && u.bgiframe(), e.using && (c = function(t) {
                        var i = m.left - D.left,
                            n = i + h - d,
                            s = m.top - D.top,
                            r = s + f - p,
                            l = {
                                target: {
                                    element: _,
                                    left: m.left,
                                    top: m.top,
                                    width: h,
                                    height: f
                                },
                                element: {
                                    element: u,
                                    left: D.left,
                                    top: D.top,
                                    width: d,
                                    height: p
                                },
                                horizontal: n < 0 ? "left" : i > 0 ? "right" : "center",
                                vertical: r < 0 ? "top" : s > 0 ? "bottom" : "middle"
                            };
                        h < d && o(i + n) < h && (l.horizontal = "center"), f < p && o(s + r) < f && (l.vertical = "middle"), a(o(i), o(n)) > a(o(s), o(r)) ? l.important = "horizontal" : l.important = "vertical", e.using.call(this, t, l)
                    }), u.offset(t.extend(D, {
                        using: c
                    }))
                })
            }, t.ui.position = {
                fit: {
                    left: function(t, e) {
                        var i, n = e.within,
                            s = n.isWindow ? n.scrollLeft : n.offset.left,
                            o = n.width,
                            r = t.left - e.collisionPosition.marginLeft,
                            l = s - r,
                            c = r + e.collisionWidth - o - s;
                        e.collisionWidth > o ? l > 0 && c <= 0 ? (i = t.left + l + e.collisionWidth - o - s, t.left += l - i) : t.left = c > 0 && l <= 0 ? s : l > c ? s + o - e.collisionWidth : s : l > 0 ? t.left += l : c > 0 ? t.left -= c : t.left = a(t.left - r, t.left)
                    },
                    top: function(t, e) {
                        var i, n = e.within,
                            s = n.isWindow ? n.scrollTop : n.offset.top,
                            o = e.within.height,
                            r = t.top - e.collisionPosition.marginTop,
                            l = s - r,
                            c = r + e.collisionHeight - o - s;
                        e.collisionHeight > o ? l > 0 && c <= 0 ? (i = t.top + l + e.collisionHeight - o - s, t.top += l - i) : t.top = c > 0 && l <= 0 ? s : l > c ? s + o - e.collisionHeight : s : l > 0 ? t.top += l : c > 0 ? t.top -= c : t.top = a(t.top - r, t.top)
                    }
                },
                flip: {
                    left: function(t, e) {
                        var i, n, s = e.within,
                            a = s.offset.left + s.scrollLeft,
                            r = s.width,
                            l = s.isWindow ? s.scrollLeft : s.offset.left,
                            c = t.left - e.collisionPosition.marginLeft,
                            u = c - l,
                            d = c + e.collisionWidth - r - l,
                            h = "left" === e.my[0] ? -e.elemWidth : "right" === e.my[0] ? e.elemWidth : 0,
                            p = "left" === e.at[0] ? e.targetWidth : "right" === e.at[0] ? -e.targetWidth : 0,
                            f = -2 * e.offset[0];
                        u < 0 ? ((i = t.left + h + p + f + e.collisionWidth - r - a) < 0 || i < o(u)) && (t.left += h + p + f) : d > 0 && ((n = t.left - e.collisionPosition.marginLeft + h + p + f - l) > 0 || o(n) < d) && (t.left += h + p + f)
                    },
                    top: function(t, e) {
                        var i, n, s = e.within,
                            a = s.offset.top + s.scrollTop,
                            r = s.height,
                            l = s.isWindow ? s.scrollTop : s.offset.top,
                            c = t.top - e.collisionPosition.marginTop,
                            u = c - l,
                            d = c + e.collisionHeight - r - l,
                            h = "top" === e.my[1],
                            p = h ? -e.elemHeight : "bottom" === e.my[1] ? e.elemHeight : 0,
                            f = "top" === e.at[1] ? e.targetHeight : "bottom" === e.at[1] ? -e.targetHeight : 0,
                            m = -2 * e.offset[1];
                        u < 0 ? (n = t.top + p + f + m + e.collisionHeight - r - a, t.top + p + f + m > u && (n < 0 || n < o(u)) && (t.top += p + f + m)) : d > 0 && (i = t.top - e.collisionPosition.marginTop + p + f + m - l, t.top + p + f + m > d && (i > 0 || o(i) < d) && (t.top += p + f + m))
                    }
                },
                flipfit: {
                    left: function() {
                        t.ui.position.flip.left.apply(this, arguments), t.ui.position.fit.left.apply(this, arguments)
                    },
                    top: function() {
                        t.ui.position.flip.top.apply(this, arguments), t.ui.position.fit.top.apply(this, arguments)
                    }
                }
            },
            function() {
                var e, i, n, s, a, o = document.getElementsByTagName("body")[0],
                    r = document.createElement("div");
                e = document.createElement(o ? "div" : "body"), n = {
                    visibility: "hidden",
                    width: 0,
                    height: 0,
                    border: 0,
                    margin: 0,
                    background: "none"
                }, o && t.extend(n, {
                    position: "absolute",
                    left: "-1000px",
                    top: "-1000px"
                });
                for (a in n) e.style[a] = n[a];
                e.appendChild(r), i = o || document.documentElement, i.insertBefore(e, i.firstChild), r.style.cssText = "position: absolute; left: 10.7432222px;", s = t(r).offset().left, t.support.offsetFractions = s > 10 && s < 11, e.innerHTML = "", i.removeChild(e)
            }(), !1 !== t.uiBackCompat && function(t) {
                var i = t.fn.position;
                t.fn.position = function(n) {
                    if (!n || !n.offset) return i.call(this, n);
                    var s = n.offset.split(" "),
                        a = n.at.split(" ");
                    return 1 === s.length && (s[1] = s[0]), /^\d/.test(s[0]) && (s[0] = "+" + s[0]), /^\d/.test(s[1]) && (s[1] = "+" + s[1]), 1 === a.length && (/left|center|right/.test(a[0]) ? a[1] = "center" : (a[1] = a[0], a[0] = "center")), i.call(this, t.extend(n, {
                        at: a[0] + s[0] + " " + a[1] + s[1],
                        offset: e
                    }))
                }
            }(jQuery)
    }(jQuery),
    function(t, e) {
        t.widget("ui.progressbar", {
            version: "1.9.1",
            options: {
                value: 0,
                max: 100
            },
            min: 0,
            _create: function() {
                this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({
                    role: "progressbar",
                    "aria-valuemin": this.min,
                    "aria-valuemax": this.options.max,
                    "aria-valuenow": this._value()
                }), this.valueDiv = t("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element), this.oldValue = this._value(), this._refreshValue()
            },
            _destroy: function() {
                this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), this.valueDiv.remove()
            },
            value: function(t) {
                return t === e ? this._value() : (this._setOption("value", t), this)
            },
            _setOption: function(t, e) {
                "value" === t && (this.options.value = e, this._refreshValue(), this._value() === this.options.max && this._trigger("complete")), this._super(t, e)
            },
            _value: function() {
                var t = this.options.value;
                return "number" != typeof t && (t = 0), Math.min(this.options.max, Math.max(this.min, t))
            },
            _percentage: function() {
                return 100 * this._value() / this.options.max
            },
            _refreshValue: function() {
                var t = this.value(),
                    e = this._percentage();
                this.oldValue !== t && (this.oldValue = t, this._trigger("change")), this.valueDiv.toggle(t > this.min).toggleClass("ui-corner-right", t === this.options.max).width(e.toFixed(0) + "%"), this.element.attr("aria-valuenow", t)
            }
        })
    }(jQuery),
    function(t) {
        var e = 5;
        t.widget("ui.slider", t.ui.mouse, {
            version: "1.9.1",
            widgetEventPrefix: "slide",
            options: {
                animate: !1,
                distance: 0,
                max: 100,
                min: 0,
                orientation: "horizontal",
                range: !1,
                step: 1,
                value: 0,
                values: null
            },
            _create: function() {
                var i, n, s = this.options,
                    a = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),
                    o = [];
                for (this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, this._detectOrientation(), this._mouseInit(), this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget ui-widget-content ui-corner-all" + (s.disabled ? " ui-slider-disabled ui-disabled" : "")), this.range = t([]), s.range && (!0 === s.range && (s.values || (s.values = [this._valueMin(), this._valueMin()]), s.values.length && 2 !== s.values.length && (s.values = [s.values[0], s.values[0]])), this.range = t("<div></div>").appendTo(this.element).addClass("ui-slider-range ui-widget-header" + ("min" === s.range || "max" === s.range ? " ui-slider-range-" + s.range : ""))), n = s.values && s.values.length || 1, i = a.length; i < n; i++) o.push("<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>");
                this.handles = a.add(t(o.join("")).appendTo(this.element)), this.handle = this.handles.eq(0), this.handles.add(this.range).filter("a").click(function(t) {
                    t.preventDefault()
                }).mouseenter(function() {
                    s.disabled || t(this).addClass("ui-state-hover")
                }).mouseleave(function() {
                    t(this).removeClass("ui-state-hover")
                }).focus(function() {
                    s.disabled ? t(this).blur() : (t(".ui-slider .ui-state-focus").removeClass("ui-state-focus"), t(this).addClass("ui-state-focus"))
                }).blur(function() {
                    t(this).removeClass("ui-state-focus")
                }), this.handles.each(function(e) {
                    t(this).data("ui-slider-handle-index", e)
                }), this._on(this.handles, {
                    keydown: function(i) {
                        var n, s, a, o = t(i.target).data("ui-slider-handle-index");
                        switch (i.keyCode) {
                            case t.ui.keyCode.HOME:
                            case t.ui.keyCode.END:
                            case t.ui.keyCode.PAGE_UP:
                            case t.ui.keyCode.PAGE_DOWN:
                            case t.ui.keyCode.UP:
                            case t.ui.keyCode.RIGHT:
                            case t.ui.keyCode.DOWN:
                            case t.ui.keyCode.LEFT:
                                if (i.preventDefault(), !this._keySliding && (this._keySliding = !0, t(i.target).addClass("ui-state-active"), !1 === this._start(i, o))) return
                        }
                        switch (a = this.options.step, n = s = this.options.values && this.options.values.length ? this.values(o) : this.value(), i.keyCode) {
                            case t.ui.keyCode.HOME:
                                s = this._valueMin();
                                break;
                            case t.ui.keyCode.END:
                                s = this._valueMax();
                                break;
                            case t.ui.keyCode.PAGE_UP:
                                s = this._trimAlignValue(n + (this._valueMax() - this._valueMin()) / e);
                                break;
                            case t.ui.keyCode.PAGE_DOWN:
                                s = this._trimAlignValue(n - (this._valueMax() - this._valueMin()) / e);
                                break;
                            case t.ui.keyCode.UP:
                            case t.ui.keyCode.RIGHT:
                                if (n === this._valueMax()) return;
                                s = this._trimAlignValue(n + a);
                                break;
                            case t.ui.keyCode.DOWN:
                            case t.ui.keyCode.LEFT:
                                if (n === this._valueMin()) return;
                                s = this._trimAlignValue(n - a)
                        }
                        this._slide(i, o, s)
                    },
                    keyup: function(e) {
                        var i = t(e.target).data("ui-slider-handle-index");
                        this._keySliding && (this._keySliding = !1, this._stop(e, i), this._change(e, i), t(e.target).removeClass("ui-state-active"))
                    }
                }), this._refreshValue(), this._animateOff = !1
            },
            _destroy: function() {
                this.handles.remove(), this.range.remove(), this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-slider-disabled ui-widget ui-widget-content ui-corner-all"), this._mouseDestroy()
            },
            _mouseCapture: function(e) {
                var i, n, s, a, o, r, l, c = this,
                    u = this.options;
                return !u.disabled && (this.elementSize = {
                    width: this.element.outerWidth(),
                    height: this.element.outerHeight()
                }, this.elementOffset = this.element.offset(), i = {
                    x: e.pageX,
                    y: e.pageY
                }, n = this._normValueFromMouse(i), s = this._valueMax() - this._valueMin() + 1, this.handles.each(function(e) {
                    var i = Math.abs(n - c.values(e));
                    s > i && (s = i, a = t(this), o = e)
                }), !0 === u.range && this.values(1) === u.min && (o += 1, a = t(this.handles[o])), !1 !== this._start(e, o) && (this._mouseSliding = !0, this._handleIndex = o, a.addClass("ui-state-active").focus(), r = a.offset(), l = !t(e.target).parents().andSelf().is(".ui-slider-handle"), this._clickOffset = l ? {
                    left: 0,
                    top: 0
                } : {
                    left: e.pageX - r.left - a.width() / 2,
                    top: e.pageY - r.top - a.height() / 2 - (parseInt(a.css("borderTopWidth"), 10) || 0) - (parseInt(a.css("borderBottomWidth"), 10) || 0) + (parseInt(a.css("marginTop"), 10) || 0)
                }, this.handles.hasClass("ui-state-hover") || this._slide(e, o, n), this._animateOff = !0, !0))
            },
            _mouseStart: function() {
                return !0
            },
            _mouseDrag: function(t) {
                var e = {
                        x: t.pageX,
                        y: t.pageY
                    },
                    i = this._normValueFromMouse(e);
                return this._slide(t, this._handleIndex, i), !1
            },
            _mouseStop: function(t) {
                return this.handles.removeClass("ui-state-active"), this._mouseSliding = !1, this._stop(t, this._handleIndex), this._change(t, this._handleIndex), this._handleIndex = null, this._clickOffset = null, this._animateOff = !1, !1
            },
            _detectOrientation: function() {
                this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal"
            },
            _normValueFromMouse: function(t) {
                var e, i, n, s, a;
                return "horizontal" === this.orientation ? (e = this.elementSize.width, i = t.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (e = this.elementSize.height, i = t.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), n = i / e, n > 1 && (n = 1), n < 0 && (n = 0), "vertical" === this.orientation && (n = 1 - n), s = this._valueMax() - this._valueMin(), a = this._valueMin() + n * s, this._trimAlignValue(a)
            },
            _start: function(t, e) {
                var i = {
                    handle: this.handles[e],
                    value: this.value()
                };
                return this.options.values && this.options.values.length && (i.value = this.values(e), i.values = this.values()), this._trigger("start", t, i)
            },
            _slide: function(t, e, i) {
                var n, s, a;
                this.options.values && this.options.values.length ? (n = this.values(e ? 0 : 1), 2 === this.options.values.length && !0 === this.options.range && (0 === e && i > n || 1 === e && i < n) && (i = n), i !== this.values(e) && (s = this.values(), s[e] = i, a = this._trigger("slide", t, {
                    handle: this.handles[e],
                    value: i,
                    values: s
                }), n = this.values(e ? 0 : 1), !1 !== a && this.values(e, i, !0))) : i !== this.value() && !1 !== (a = this._trigger("slide", t, {
                    handle: this.handles[e],
                    value: i
                })) && this.value(i)
            },
            _stop: function(t, e) {
                var i = {
                    handle: this.handles[e],
                    value: this.value()
                };
                this.options.values && this.options.values.length && (i.value = this.values(e), i.values = this.values()), this._trigger("stop", t, i)
            },
            _change: function(t, e) {
                if (!this._keySliding && !this._mouseSliding) {
                    var i = {
                        handle: this.handles[e],
                        value: this.value()
                    };
                    this.options.values && this.options.values.length && (i.value = this.values(e), i.values = this.values()), this._trigger("change", t, i)
                }
            },
            value: function(t) {
                return arguments.length ? (this.options.value = this._trimAlignValue(t), this._refreshValue(), void this._change(null, 0)) : this._value()
            },
            values: function(e, i) {
                var n, s, a;
                if (arguments.length > 1) return this.options.values[e] = this._trimAlignValue(i), this._refreshValue(), void this._change(null, e);
                if (!arguments.length) return this._values();
                if (!t.isArray(arguments[0])) return this.options.values && this.options.values.length ? this._values(e) : this.value();
                for (n = this.options.values, s = arguments[0], a = 0; a < n.length; a += 1) n[a] = this._trimAlignValue(s[a]), this._change(null, a);
                this._refreshValue()
            },
            _setOption: function(e, i) {
                var n, s = 0;
                switch (t.isArray(this.options.values) && (s = this.options.values.length), t.Widget.prototype._setOption.apply(this, arguments), e) {
                    case "disabled":
                        i ? (this.handles.filter(".ui-state-focus").blur(), this.handles.removeClass("ui-state-hover"), this.handles.prop("disabled", !0), this.element.addClass("ui-disabled")) : (this.handles.prop("disabled", !1), this.element.removeClass("ui-disabled"));
                        break;
                    case "orientation":
                        this._detectOrientation(), this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation), this._refreshValue();
                        break;
                    case "value":
                        this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
                        break;
                    case "values":
                        for (this._animateOff = !0, this._refreshValue(), n = 0; n < s; n += 1) this._change(null, n);
                        this._animateOff = !1;
                        break;
                    case "min":
                    case "max":
                        this._animateOff = !0, this._refreshValue(), this._animateOff = !1
                }
            },
            _value: function() {
                var t = this.options.value;
                return t = this._trimAlignValue(t)
            },
            _values: function(t) {
                var e, i, n;
                if (arguments.length) return e = this.options.values[t], e = this._trimAlignValue(e);
                for (i = this.options.values.slice(), n = 0; n < i.length; n += 1) i[n] = this._trimAlignValue(i[n]);
                return i
            },
            _trimAlignValue: function(t) {
                if (t <= this._valueMin()) return this._valueMin();
                if (t >= this._valueMax()) return this._valueMax();
                var e = this.options.step > 0 ? this.options.step : 1,
                    i = (t - this._valueMin()) % e,
                    n = t - i;
                return 2 * Math.abs(i) >= e && (n += i > 0 ? e : -e), parseFloat(n.toFixed(5))
            },
            _valueMin: function() {
                return this.options.min
            },
            _valueMax: function() {
                return this.options.max
            },
            _refreshValue: function() {
                var e, i, n, s, a, o = this.options.range,
                    r = this.options,
                    l = this,
                    c = !this._animateOff && r.animate,
                    u = {};
                this.options.values && this.options.values.length ? this.handles.each(function(n) {
                    i = (l.values(n) - l._valueMin()) / (l._valueMax() - l._valueMin()) * 100, u["horizontal" === l.orientation ? "left" : "bottom"] = i + "%", t(this).stop(1, 1)[c ? "animate" : "css"](u, r.animate), !0 === l.options.range && ("horizontal" === l.orientation ? (0 === n && l.range.stop(1, 1)[c ? "animate" : "css"]({
                        left: i + "%"
                    }, r.animate), 1 === n && l.range[c ? "animate" : "css"]({
                        width: i - e + "%"
                    }, {
                        queue: !1,
                        duration: r.animate
                    })) : (0 === n && l.range.stop(1, 1)[c ? "animate" : "css"]({
                        bottom: i + "%"
                    }, r.animate), 1 === n && l.range[c ? "animate" : "css"]({
                        height: i - e + "%"
                    }, {
                        queue: !1,
                        duration: r.animate
                    }))), e = i
                }) : (n = this.value(), s = this._valueMin(), a = this._valueMax(), i = a !== s ? (n - s) / (a - s) * 100 : 0, u["horizontal" === this.orientation ? "left" : "bottom"] = i + "%", this.handle.stop(1, 1)[c ? "animate" : "css"](u, r.animate), "min" === o && "horizontal" === this.orientation && this.range.stop(1, 1)[c ? "animate" : "css"]({
                    width: i + "%"
                }, r.animate), "max" === o && "horizontal" === this.orientation && this.range[c ? "animate" : "css"]({
                    width: 100 - i + "%"
                }, {
                    queue: !1,
                    duration: r.animate
                }), "min" === o && "vertical" === this.orientation && this.range.stop(1, 1)[c ? "animate" : "css"]({
                    height: i + "%"
                }, r.animate), "max" === o && "vertical" === this.orientation && this.range[c ? "animate" : "css"]({
                    height: 100 - i + "%"
                }, {
                    queue: !1,
                    duration: r.animate
                }))
            }
        })
    }(jQuery),
    function(t) {
        function e(t) {
            return function() {
                var e = this.element.val();
                t.apply(this, arguments), this._refresh(), e !== this.element.val() && this._trigger("change")
            }
        }
        t.widget("ui.spinner", {
            version: "1.9.1",
            defaultElement: "<input>",
            widgetEventPrefix: "spin",
            options: {
                culture: null,
                icons: {
                    down: "ui-icon-triangle-1-s",
                    up: "ui-icon-triangle-1-n"
                },
                incremental: !0,
                max: null,
                min: null,
                numberFormat: null,
                page: 10,
                step: 1,
                change: null,
                spin: null,
                start: null,
                stop: null
            },
            _create: function() {
                this._setOption("max", this.options.max), this._setOption("min", this.options.min), this._setOption("step", this.options.step), this._value(this.element.val(), !0), this._draw(), this._on(this._events), this._refresh(), this._on(this.window, {
                    beforeunload: function() {
                        this.element.removeAttr("autocomplete")
                    }
                })
            },
            _getCreateOptions: function() {
                var e = {},
                    i = this.element;
                return t.each(["min", "max", "step"], function(t, n) {
                    var s = i.attr(n);
                    s !== undefined && s.length && (e[n] = s)
                }), e
            },
            _events: {
                keydown: function(t) {
                    this._start(t) && this._keydown(t) && t.preventDefault()
                },
                keyup: "_stop",
                focus: function() {
                    this.previous = this.element.val()
                },
                blur: function(t) {
                    if (this.cancelBlur) return void delete this.cancelBlur;
                    this._refresh(), this.previous !== this.element.val() && this._trigger("change", t)
                },
                mousewheel: function(t, e) {
                    if (e) {
                        if (!this.spinning && !this._start(t)) return !1;
                        this._spin((e > 0 ? 1 : -1) * this.options.step, t), clearTimeout(this.mousewheelTimer), this.mousewheelTimer = this._delay(function() {
                            this.spinning && this._stop(t)
                        }, 100), t.preventDefault()
                    }
                },
                "mousedown .ui-spinner-button": function(e) {
                    function i() {
                        this.element[0] === this.document[0].activeElement || (this.element.focus(), this.previous = n, this._delay(function() {
                            this.previous = n
                        }))
                    }
                    var n;
                    n = this.element[0] === this.document[0].activeElement ? this.previous : this.element.val(), e.preventDefault(), i.call(this), this.cancelBlur = !0, this._delay(function() {
                        delete this.cancelBlur, i.call(this)
                    }), !1 !== this._start(e) && this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e)
                },
                "mouseup .ui-spinner-button": "_stop",
                "mouseenter .ui-spinner-button": function(e) {
                    if (t(e.currentTarget).hasClass("ui-state-active")) return !1 !== this._start(e) && void this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e)
                },
                "mouseleave .ui-spinner-button": "_stop"
            },
            _draw: function() {
                var t = this.uiSpinner = this.element.addClass("ui-spinner-input").attr("autocomplete", "off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());
                this.element.attr("role", "spinbutton"), this.buttons = t.find(".ui-spinner-button").attr("tabIndex", -1).button().removeClass("ui-corner-all"), this.buttons.height() > Math.ceil(.5 * t.height()) && t.height() > 0 && t.height(t.height()), this.options.disabled && this.disable()
            },
            _keydown: function(e) {
                var i = this.options,
                    n = t.ui.keyCode;
                switch (e.keyCode) {
                    case n.UP:
                        return this._repeat(null, 1, e), !0;
                    case n.DOWN:
                        return this._repeat(null, -1, e), !0;
                    case n.PAGE_UP:
                        return this._repeat(null, i.page, e), !0;
                    case n.PAGE_DOWN:
                        return this._repeat(null, -i.page, e), !0
                }
                return !1
            },
            _uiSpinnerHtml: function() {
                return "<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>"
            },
            _buttonHtml: function() {
                return "<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon " + this.options.icons.up + "'>&#9650;</span></a><a class='ui-spinner-button ui-spinner-down ui-corner-br'><span class='ui-icon " + this.options.icons.down + "'>&#9660;</span></a>"
            },
            _start: function(t) {
                return !(!this.spinning && !1 === this._trigger("start", t)) && (this.counter || (this.counter = 1), this.spinning = !0, !0)
            },
            _repeat: function(t, e, i) {
                t = t || 500, clearTimeout(this.timer), this.timer = this._delay(function() {
                    this._repeat(40, e, i)
                }, t), this._spin(e * this.options.step, i)
            },
            _spin: function(t, e) {
                var i = this.value() || 0;
                this.counter || (this.counter = 1), i = this._adjustValue(i + t * this._increment(this.counter)), this.spinning && !1 === this._trigger("spin", e, {
                    value: i
                }) || (this._value(i), this.counter++)
            },
            _increment: function(e) {
                var i = this.options.incremental;
                return i ? t.isFunction(i) ? i(e) : Math.floor(e * e * e / 5e4 - e * e / 500 + 17 * e / 200 + 1) : 1
            },
            _precision: function() {
                var t = this._precisionOf(this.options.step);
                return null !== this.options.min && (t = Math.max(t, this._precisionOf(this.options.min))), t
            },
            _precisionOf: function(t) {
                var e = t.toString(),
                    i = e.indexOf(".");
                return -1 === i ? 0 : e.length - i - 1
            },
            _adjustValue: function(t) {
                var e, i, n = this.options;
                return e = null !== n.min ? n.min : 0, i = t - e, i = Math.round(i / n.step) * n.step, t = e + i, t = parseFloat(t.toFixed(this._precision())), null !== n.max && t > n.max ? n.max : null !== n.min && t < n.min ? n.min : t
            },
            _stop: function(t) {
                this.spinning && (clearTimeout(this.timer), clearTimeout(this.mousewheelTimer), this.counter = 0, this.spinning = !1, this._trigger("stop", t))
            },
            _setOption: function(t, e) {
                if ("culture" === t || "numberFormat" === t) {
                    var i = this._parse(this.element.val());
                    return this.options[t] = e, void this.element.val(this._format(i))
                }
                "max" !== t && "min" !== t && "step" !== t || "string" == typeof e && (e = this._parse(e)), this._super(t, e), "disabled" === t && (e ? (this.element.prop("disabled", !0), this.buttons.button("disable")) : (this.element.prop("disabled", !1), this.buttons.button("enable")))
            },
            _setOptions: e(function(t) {
                this._super(t), this._value(this.element.val())
            }),
            _parse: function(t) {
                return "string" == typeof t && "" !== t && (t = window.Globalize && this.options.numberFormat ? Globalize.parseFloat(t, 10, this.options.culture) : +t), "" === t || isNaN(t) ? null : t
            },
            _format: function(t) {
                return "" === t ? "" : window.Globalize && this.options.numberFormat ? Globalize.format(t, this.options.numberFormat, this.options.culture) : t
            },
            _refresh: function() {
                this.element.attr({
                    "aria-valuemin": this.options.min,
                    "aria-valuemax": this.options.max,
                    "aria-valuenow": this._parse(this.element.val())
                })
            },
            _value: function(t, e) {
                var i;
                "" !== t && null !== (i = this._parse(t)) && (e || (i = this._adjustValue(i)), t = this._format(i)), this.element.val(t), this._refresh()
            },
            _destroy: function() {
                this.element.removeClass("ui-spinner-input").prop("disabled", !1).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), this.uiSpinner.replaceWith(this.element)
            },
            stepUp: e(function(t) {
                this._stepUp(t)
            }),
            _stepUp: function(t) {
                this._spin((t || 1) * this.options.step)
            },
            stepDown: e(function(t) {
                this._stepDown(t)
            }),
            _stepDown: function(t) {
                this._spin((t || 1) * -this.options.step)
            },
            pageUp: e(function(t) {
                this._stepUp((t || 1) * this.options.page)
            }),
            pageDown: e(function(t) {
                this._stepDown((t || 1) * this.options.page)
            }),
            value: function(t) {
                if (!arguments.length) return this._parse(this.element.val());
                e(this._value).call(this, t)
            },
            widget: function() {
                return this.uiSpinner
            }
        })
    }(jQuery),
    function(t, e) {
        function i() {
            return ++s
        }

        function n(t) {
            return t.hash.length > 1 && t.href.replace(a, "") === location.href.replace(a, "")
        }
        var s = 0,
            a = /#.*$/;
        t.widget("ui.tabs", {
            version: "1.9.1",
            delay: 300,
            options: {
                active: null,
                collapsible: !1,
                event: "click",
                heightStyle: "content",
                hide: null,
                show: null,
                activate: null,
                beforeActivate: null,
                beforeLoad: null,
                load: null
            },
            _create: function() {
                var e = this,
                    i = this.options,
                    n = i.active,
                    s = location.hash.substring(1);
                this.running = !1, this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible", i.collapsible).delegate(".ui-tabs-nav > li", "mousedown" + this.eventNamespace, function(e) {
                    t(this).is(".ui-state-disabled") && e.preventDefault()
                }).delegate(".ui-tabs-anchor", "focus" + this.eventNamespace, function() {
                    t(this).closest("li").is(".ui-state-disabled") && this.blur()
                }), this._processTabs(), null === n && (s && this.tabs.each(function(e, i) {
                    if (t(i).attr("aria-controls") === s) return n = e, !1
                }), null === n && (n = this.tabs.index(this.tabs.filter(".ui-tabs-active"))), null !== n && -1 !== n || (n = !!this.tabs.length && 0)), !1 !== n && -1 === (n = this.tabs.index(this.tabs.eq(n))) && (n = !i.collapsible && 0), i.active = n, !i.collapsible && !1 === i.active && this.anchors.length && (i.active = 0), t.isArray(i.disabled) && (i.disabled = t.unique(i.disabled.concat(t.map(this.tabs.filter(".ui-state-disabled"), function(t) {
                    return e.tabs.index(t)
                }))).sort()), !1 !== this.options.active && this.anchors.length ? this.active = this._findActive(this.options.active) : this.active = t(), this._refresh(), this.active.length && this.load(i.active)
            },
            _getCreateEventData: function() {
                return {
                    tab: this.active,
                    panel: this.active.length ? this._getPanelForTab(this.active) : t()
                }
            },
            _tabKeydown: function(e) {
                var i = t(this.document[0].activeElement).closest("li"),
                    n = this.tabs.index(i),
                    s = !0;
                if (!this._handlePageNav(e)) {
                    switch (e.keyCode) {
                        case t.ui.keyCode.RIGHT:
                        case t.ui.keyCode.DOWN:
                            n++;
                            break;
                        case t.ui.keyCode.UP:
                        case t.ui.keyCode.LEFT:
                            s = !1, n--;
                            break;
                        case t.ui.keyCode.END:
                            n = this.anchors.length - 1;
                            break;
                        case t.ui.keyCode.HOME:
                            n = 0;
                            break;
                        case t.ui.keyCode.SPACE:
                            return e.preventDefault(), clearTimeout(this.activating), void this._activate(n);
                        case t.ui.keyCode.ENTER:
                            return e.preventDefault(), clearTimeout(this.activating), void this._activate(n !== this.options.active && n);
                        default:
                            return
                    }
                    e.preventDefault(), clearTimeout(this.activating), n = this._focusNextTab(n, s), e.ctrlKey || (i.attr("aria-selected", "false"), this.tabs.eq(n).attr("aria-selected", "true"), this.activating = this._delay(function() {
                        this.option("active", n)
                    }, this.delay))
                }
            },
            _panelKeydown: function(e) {
                this._handlePageNav(e) || e.ctrlKey && e.keyCode === t.ui.keyCode.UP && (e.preventDefault(), this.active.focus())
            },
            _handlePageNav: function(e) {
                return e.altKey && e.keyCode === t.ui.keyCode.PAGE_UP ? (this._activate(this._focusNextTab(this.options.active - 1, !1)), !0) : e.altKey && e.keyCode === t.ui.keyCode.PAGE_DOWN ? (this._activate(this._focusNextTab(this.options.active + 1, !0)), !0) : void 0
            },
            _findNextTab: function(e, i) {
                function n() {
                    return e > s && (e = 0), e < 0 && (e = s), e
                }
                for (var s = this.tabs.length - 1; - 1 !== t.inArray(n(), this.options.disabled);) e = i ? e + 1 : e - 1;
                return e
            },
            _focusNextTab: function(t, e) {
                return t = this._findNextTab(t, e), this.tabs.eq(t).focus(), t
            },
            _setOption: function(t, e) {
                return "active" === t ? void this._activate(e) : "disabled" === t ? void this._setupDisabled(e) : (this._super(t, e), "collapsible" === t && (this.element.toggleClass("ui-tabs-collapsible", e), e || !1 !== this.options.active || this._activate(0)), "event" === t && this._setupEvents(e), void("heightStyle" === t && this._setupHeightStyle(e)))
            },
            _tabId: function(t) {
                return t.attr("aria-controls") || "ui-tabs-" + i()
            },
            _sanitizeSelector: function(t) {
                return t ? t.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&") : ""
            },
            refresh: function() {
                var e = this.options,
                    i = this.tablist.children(":has(a[href])");
                e.disabled = t.map(i.filter(".ui-state-disabled"), function(t) {
                    return i.index(t)
                }), this._processTabs(), !1 !== e.active && this.anchors.length ? this.active.length && !t.contains(this.tablist[0], this.active[0]) ? this.tabs.length === e.disabled.length ? (e.active = !1, this.active = t()) : this._activate(this._findNextTab(Math.max(0, e.active - 1), !1)) : e.active = this.tabs.index(this.active) : (e.active = !1, this.active = t()), this._refresh()
            },
            _refresh: function() {
                this._setupDisabled(this.options.disabled), this._setupEvents(this.options.event), this._setupHeightStyle(this.options.heightStyle), this.tabs.not(this.active).attr({
                    "aria-selected": "false",
                    tabIndex: -1
                }), this.panels.not(this._getPanelForTab(this.active)).hide().attr({
                    "aria-expanded": "false",
                    "aria-hidden": "true"
                }), this.active.length ? (this.active.addClass("ui-tabs-active ui-state-active").attr({
                    "aria-selected": "true",
                    tabIndex: 0
                }), this._getPanelForTab(this.active).show().attr({
                    "aria-expanded": "true",
                    "aria-hidden": "false"
                })) : this.tabs.eq(0).attr("tabIndex", 0)
            },
            _processTabs: function() {
                var e = this;
                this.tablist = this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role", "tablist"), this.tabs = this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({
                    role: "tab",
                    tabIndex: -1
                }), this.anchors = this.tabs.map(function() {
                    return t("a", this)[0]
                }).addClass("ui-tabs-anchor").attr({
                    role: "presentation",
                    tabIndex: -1
                }), this.panels = t(), this.anchors.each(function(i, s) {
                    var a, o, r, l = t(s).uniqueId().attr("id"),
                        c = t(s).closest("li"),
                        u = c.attr("aria-controls");
                    n(s) ? (a = s.hash, o = e.element.find(e._sanitizeSelector(a))) : (r = e._tabId(c), a = "#" + r, o = e.element.find(a), o.length || (o = e._createPanel(r), o.insertAfter(e.panels[i - 1] || e.tablist)), o.attr("aria-live", "polite")), o.length && (e.panels = e.panels.add(o)), u && c.data("ui-tabs-aria-controls", u), c.attr({
                        "aria-controls": a.substring(1),
                        "aria-labelledby": l
                    }), o.attr("aria-labelledby", l)
                }), this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role", "tabpanel")
            },
            _getList: function() {
                return this.element.find("ol,ul").eq(0)
            },
            _createPanel: function(e) {
                return t("<div>").attr("id", e).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy", !0)
            },
            _setupDisabled: function(e) {
                t.isArray(e) && (e.length ? e.length === this.anchors.length && (e = !0) : e = !1);
                for (var i, n = 0; i = this.tabs[n]; n++) !0 === e || -1 !== t.inArray(n, e) ? t(i).addClass("ui-state-disabled").attr("aria-disabled", "true") : t(i).removeClass("ui-state-disabled").removeAttr("aria-disabled");
                this.options.disabled = e
            },
            _setupEvents: function(e) {
                var i = {
                    click: function(t) {
                        t.preventDefault()
                    }
                };
                e && t.each(e.split(" "), function(t, e) {
                    i[e] = "_eventHandler"
                }), this._off(this.anchors.add(this.tabs).add(this.panels)), this._on(this.anchors, i), this._on(this.tabs, {
                    keydown: "_tabKeydown"
                }), this._on(this.panels, {
                    keydown: "_panelKeydown"
                }), this._focusable(this.tabs), this._hoverable(this.tabs)
            },
            _setupHeightStyle: function(e) {
                var i, n, s = this.element.parent();
                "fill" === e ? (t.support.minHeight || (n = s.css("overflow"), s.css("overflow", "hidden")), i = s.height(), this.element.siblings(":visible").each(function() {
                    var e = t(this),
                        n = e.css("position");
                    "absolute" !== n && "fixed" !== n && (i -= e.outerHeight(!0))
                }), n && s.css("overflow", n), this.element.children().not(this.panels).each(function() {
                    i -= t(this).outerHeight(!0)
                }), this.panels.each(function() {
                    t(this).height(Math.max(0, i - t(this).innerHeight() + t(this).height()))
                }).css("overflow", "auto")) : "auto" === e && (i = 0, this.panels.each(function() {
                    i = Math.max(i, t(this).height("").height())
                }).height(i))
            },
            _eventHandler: function(e) {
                var i = this.options,
                    n = this.active,
                    s = t(e.currentTarget),
                    a = s.closest("li"),
                    o = a[0] === n[0],
                    r = o && i.collapsible,
                    l = r ? t() : this._getPanelForTab(a),
                    c = n.length ? this._getPanelForTab(n) : t(),
                    u = {
                        oldTab: n,
                        oldPanel: c,
                        newTab: r ? t() : a,
                        newPanel: l
                    };
                e.preventDefault(), a.hasClass("ui-state-disabled") || a.hasClass("ui-tabs-loading") || this.running || o && !i.collapsible || !1 === this._trigger("beforeActivate", e, u) || (i.active = !r && this.tabs.index(a), this.active = o ? t() : a, this.xhr && this.xhr.abort(), c.length || l.length || t.error("jQuery UI Tabs: Mismatching fragment identifier."), l.length && this.load(this.tabs.index(a), e), this._toggle(e, u))
            },
            _toggle: function(e, i) {
                function n() {
                    a.running = !1, a._trigger("activate", e, i)
                }

                function s() {
                    i.newTab.closest("li").addClass("ui-tabs-active ui-state-active"), o.length && a.options.show ? a._show(o, a.options.show, n) : (o.show(), n())
                }
                var a = this,
                    o = i.newPanel,
                    r = i.oldPanel;
                this.running = !0, r.length && this.options.hide ? this._hide(r, this.options.hide, function() {
                    i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), s()
                }) : (i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), r.hide(), s()), r.attr({
                    "aria-expanded": "false",
                    "aria-hidden": "true"
                }), i.oldTab.attr("aria-selected", "false"), o.length && r.length ? i.oldTab.attr("tabIndex", -1) : o.length && this.tabs.filter(function() {
                    return 0 === t(this).attr("tabIndex")
                }).attr("tabIndex", -1), o.attr({
                    "aria-expanded": "true",
                    "aria-hidden": "false"
                }), i.newTab.attr({
                    "aria-selected": "true",
                    tabIndex: 0
                })
            },
            _activate: function(e) {
                var i, n = this._findActive(e);
                n[0] !== this.active[0] && (n.length || (n = this.active), i = n.find(".ui-tabs-anchor")[0], this._eventHandler({
                    target: i,
                    currentTarget: i,
                    preventDefault: t.noop
                }))
            },
            _findActive: function(e) {
                return !1 === e ? t() : this.tabs.eq(e)
            },
            _getIndex: function(t) {
                return "string" == typeof t && (t = this.anchors.index(this.anchors.filter("[href$='" + t + "']"))), t
            },
            _destroy: function() {
                this.xhr && this.xhr.abort(), this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible"), this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role"), this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeData("href.tabs").removeData("load.tabs").removeUniqueId(), this.tabs.add(this.panels).each(function() {
                    t.data(this, "ui-tabs-destroy") ? t(this).remove() : t(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role")
                }), this.tabs.each(function() {
                    var e = t(this),
                        i = e.data("ui-tabs-aria-controls");
                    i ? e.attr("aria-controls", i) : e.removeAttr("aria-controls")
                }), "content" !== this.options.heightStyle && this.panels.css("height", "")
            },
            enable: function(i) {
                var n = this.options.disabled;
                !1 !== n && (i === e ? n = !1 : (i = this._getIndex(i), n = t.isArray(n) ? t.map(n, function(t) {
                    return t !== i ? t : null
                }) : t.map(this.tabs, function(t, e) {
                    return e !== i ? e : null
                })), this._setupDisabled(n))
            },
            disable: function(i) {
                var n = this.options.disabled;
                if (!0 !== n) {
                    if (i === e) n = !0;
                    else {
                        if (i = this._getIndex(i), -1 !== t.inArray(i, n)) return;
                        n = t.isArray(n) ? t.merge([i], n).sort() : [i]
                    }
                    this._setupDisabled(n)
                }
            },
            load: function(e, i) {
                e = this._getIndex(e);
                var s = this,
                    a = this.tabs.eq(e),
                    o = a.find(".ui-tabs-anchor"),
                    r = this._getPanelForTab(a),
                    l = {
                        tab: a,
                        panel: r
                    };
                n(o[0]) || (this.xhr = t.ajax(this._ajaxSettings(o, i, l)), this.xhr && "canceled" !== this.xhr.statusText && (a.addClass("ui-tabs-loading"), r.attr("aria-busy", "true"), this.xhr.success(function(t) {
                    setTimeout(function() {
                        r.html(t), s._trigger("load", i, l)
                    }, 1)
                }).complete(function(t, e) {
                    setTimeout(function() {
                        "abort" === e && s.panels.stop(!1, !0), a.removeClass("ui-tabs-loading"), r.removeAttr("aria-busy"), t === s.xhr && delete s.xhr
                    }, 1)
                })))
            },
            _ajaxSettings: function(e, i, n) {
                var s = this;
                return {
                    url: e.attr("href"),
                    beforeSend: function(e, a) {
                        return s._trigger("beforeLoad", i, t.extend({
                            jqXHR: e,
                            ajaxSettings: a
                        }, n))
                    }
                }
            },
            _getPanelForTab: function(e) {
                var i = t(e).attr("aria-controls");
                return this.element.find(this._sanitizeSelector("#" + i))
            }
        }), !1 !== t.uiBackCompat && (t.ui.tabs.prototype._ui = function(t, e) {
            return {
                tab: t,
                panel: e,
                index: this.anchors.index(t)
            }
        }, t.widget("ui.tabs", t.ui.tabs, {
            url: function(t, e) {
                this.anchors.eq(t).attr("href", e)
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            options: {
                ajaxOptions: null,
                cache: !1
            },
            _create: function() {
                this._super();
                var e = this;
                this._on({
                    tabsbeforeload: function(i, n) {
                        if (t.data(n.tab[0], "cache.tabs")) return void i.preventDefault();
                        n.jqXHR.success(function() {
                            e.options.cache && t.data(n.tab[0], "cache.tabs", !0)
                        })
                    }
                })
            },
            _ajaxSettings: function(e, i, n) {
                var s = this.options.ajaxOptions;
                return t.extend({}, s, {
                    error: function(t, e) {
                        try {
                            s.error(t, e, n.tab.closest("li").index(), n.tab[0])
                        } catch (t) {}
                    }
                }, this._superApply(arguments))
            },
            _setOption: function(t, e) {
                "cache" === t && !1 === e && this.anchors.removeData("cache.tabs"), this._super(t, e)
            },
            _destroy: function() {
                this.anchors.removeData("cache.tabs"), this._super()
            },
            url: function(t) {
                this.anchors.eq(t).removeData("cache.tabs"), this._superApply(arguments)
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            abort: function() {
                this.xhr && this.xhr.abort()
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            options: {
                spinner: "<em>Loading&#8230;</em>"
            },
            _create: function() {
                this._super(), this._on({
                    tabsbeforeload: function(t, e) {
                        if (t.target === this.element[0] && this.options.spinner) {
                            var i = e.tab.find("span"),
                                n = i.html();
                            i.html(this.options.spinner), e.jqXHR.complete(function() {
                                i.html(n)
                            })
                        }
                    }
                })
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            options: {
                enable: null,
                disable: null
            },
            enable: function(e) {
                var i, n = this.options;
                (e && !0 === n.disabled || t.isArray(n.disabled) && -1 !== t.inArray(e, n.disabled)) && (i = !0), this._superApply(arguments), i && this._trigger("enable", null, this._ui(this.anchors[e], this.panels[e]))
            },
            disable: function(e) {
                var i, n = this.options;
                (e && !1 === n.disabled || t.isArray(n.disabled) && -1 === t.inArray(e, n.disabled)) && (i = !0), this._superApply(arguments), i && this._trigger("disable", null, this._ui(this.anchors[e], this.panels[e]))
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            options: {
                add: null,
                remove: null,
                tabTemplate: "<li><a href='#{href}'><span>#{label}</span></a></li>"
            },
            add: function(i, n, s) {
                s === e && (s = this.anchors.length);
                var a, o, r = this.options,
                    l = t(r.tabTemplate.replace(/#\{href\}/g, i).replace(/#\{label\}/g, n)),
                    c = i.indexOf("#") ? this._tabId(l) : i.replace("#", "");
                return l.addClass("ui-state-default ui-corner-top").data("ui-tabs-destroy", !0), l.attr("aria-controls", c), a = s >= this.tabs.length, o = this.element.find("#" + c), o.length || (o = this._createPanel(c), a ? s > 0 ? o.insertAfter(this.panels.eq(-1)) : o.appendTo(this.element) : o.insertBefore(this.panels[s])), o.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").hide(), a ? l.appendTo(this.tablist) : l.insertBefore(this.tabs[s]), r.disabled = t.map(r.disabled, function(t) {
                    return t >= s ? ++t : t
                }), this.refresh(), 1 === this.tabs.length && !1 === r.active && this.option("active", 0), this._trigger("add", null, this._ui(this.anchors[s], this.panels[s])), this
            },
            remove: function(e) {
                e = this._getIndex(e);
                var i = this.options,
                    n = this.tabs.eq(e).remove(),
                    s = this._getPanelForTab(n).remove();
                return n.hasClass("ui-tabs-active") && this.anchors.length > 2 && this._activate(e + (e + 1 < this.anchors.length ? 1 : -1)), i.disabled = t.map(t.grep(i.disabled, function(t) {
                    return t !== e
                }), function(t) {
                    return t >= e ? --t : t
                }), this.refresh(), this._trigger("remove", null, this._ui(n.find("a")[0], s[0])), this
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            length: function() {
                return this.anchors.length
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            options: {
                idPrefix: "ui-tabs-"
            },
            _tabId: function(e) {
                var n = e.is("li") ? e.find("a[href]") : e;
                return n = n[0], t(n).closest("li").attr("aria-controls") || n.title && n.title.replace(/\s/g, "_").replace(/[^\w\u00c0-\uFFFF\-]/g, "") || this.options.idPrefix + i()
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            options: {
                panelTemplate: "<div></div>"
            },
            _createPanel: function(e) {
                return t(this.options.panelTemplate).attr("id", e).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy", !0)
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            _create: function() {
                var t = this.options;
                null === t.active && t.selected !== e && (t.active = -1 !== t.selected && t.selected), this._super(), t.selected = t.active, !1 === t.selected && (t.selected = -1)
            },
            _setOption: function(t, e) {
                if ("selected" !== t) return this._super(t, e);
                var i = this.options;
                this._super("active", -1 !== e && e), i.selected = i.active, !1 === i.selected && (i.selected = -1)
            },
            _eventHandler: function() {
                this._superApply(arguments), this.options.selected = this.options.active, !1 === this.options.selected && (this.options.selected = -1)
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            options: {
                show: null,
                select: null
            },
            _create: function() {
                this._super(), !1 !== this.options.active && this._trigger("show", null, this._ui(this.active.find(".ui-tabs-anchor")[0], this._getPanelForTab(this.active)[0]))
            },
            _trigger: function(t, e, i) {
                var n = this._superApply(arguments);
                return !!n && ("beforeActivate" === t && i.newTab.length ? n = this._super("select", e, {
                    tab: i.newTab.find(".ui-tabs-anchor")[0],
                    panel: i.newPanel[0],
                    index: i.newTab.closest("li").index()
                }) : "activate" === t && i.newTab.length && (n = this._super("show", e, {
                    tab: i.newTab.find(".ui-tabs-anchor")[0],
                    panel: i.newPanel[0],
                    index: i.newTab.closest("li").index()
                })), n)
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            select: function(t) {
                if (-1 === (t = this._getIndex(t))) {
                    if (!this.options.collapsible || -1 === this.options.selected) return;
                    t = this.options.selected
                }
                this.anchors.eq(t).trigger(this.options.event + this.eventNamespace)
            }
        }), function() {
            var e = 0;
            t.widget("ui.tabs", t.ui.tabs, {
                options: {
                    cookie: null
                },
                _create: function() {
                    var t, e = this.options;
                    null == e.active && e.cookie && (t = parseInt(this._cookie(), 10), -1 === t && (t = !1), e.active = t), this._super()
                },
                _cookie: function(i) {
                    var n = [this.cookie || (this.cookie = this.options.cookie.name || "ui-tabs-" + ++e)];
                    return arguments.length && (n.push(!1 === i ? -1 : i), n.push(this.options.cookie)), t.cookie.apply(null, n)
                },
                _refresh: function() {
                    this._super(), this.options.cookie && this._cookie(this.options.active, this.options.cookie)
                },
                _eventHandler: function() {
                    this._superApply(arguments), this.options.cookie && this._cookie(this.options.active, this.options.cookie)
                },
                _destroy: function() {
                    this._super(), this.options.cookie && this._cookie(null, this.options.cookie)
                }
            })
        }(), t.widget("ui.tabs", t.ui.tabs, {
            _trigger: function(e, i, n) {
                var s = t.extend({}, n);
                return "load" === e && (s.panel = s.panel[0], s.tab = s.tab.find(".ui-tabs-anchor")[0]), this._super(e, i, s)
            }
        }), t.widget("ui.tabs", t.ui.tabs, {
            options: {
                fx: null
            },
            _getFx: function() {
                var e, i, n = this.options.fx;
                return n && (t.isArray(n) ? (e = n[0], i = n[1]) : e = i = n), n ? {
                    show: i,
                    hide: e
                } : null
            },
            _toggle: function(t, e) {
                function i() {
                    s.running = !1, s._trigger("activate", t, e)
                }

                function n() {
                    e.newTab.closest("li").addClass("ui-tabs-active ui-state-active"), a.length && r.show ? a.animate(r.show, r.show.duration, function() {
                        i()
                    }) : (a.show(), i())
                }
                var s = this,
                    a = e.newPanel,
                    o = e.oldPanel,
                    r = this._getFx();
                if (!r) return this._super(t, e);
                s.running = !0, o.length && r.hide ? o.animate(r.hide, r.hide.duration, function() {
                    e.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), n()
                }) : (e.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), o.hide(), n())
            }
        }))
    }(jQuery),
    function(t) {
        function e(e, i) {
            var n = (e.attr("aria-describedby") || "").split(/\s+/);
            n.push(i), e.data("ui-tooltip-id", i).attr("aria-describedby", t.trim(n.join(" ")))
        }

        function i(e) {
            var i = e.data("ui-tooltip-id"),
                n = (e.attr("aria-describedby") || "").split(/\s+/),
                s = t.inArray(i, n); - 1 !== s && n.splice(s, 1), e.removeData("ui-tooltip-id"), n = t.trim(n.join(" ")), n ? e.attr("aria-describedby", n) : e.removeAttr("aria-describedby")
        }
        var n = 0;
        t.widget("ui.tooltip", {
            version: "1.9.1",
            options: {
                content: function() {
                    return t(this).attr("title")
                },
                hide: !0,
                items: "[title]:not([disabled])",
                position: {
                    my: "left top+15",
                    at: "left bottom",
                    collision: "flipfit flipfit"
                },
                show: !0,
                tooltipClass: null,
                track: !1,
                close: null,
                open: null
            },
            _create: function() {
                this._on({
                    mouseover: "open",
                    focusin: "open"
                }), this.tooltips = {}, this.parents = {}, this.options.disabled && this._disable()
            },
            _setOption: function(e, i) {
                var n = this;
                if ("disabled" === e) return this[i ? "_disable" : "_enable"](), void(this.options[e] = i);
                this._super(e, i), "content" === e && t.each(this.tooltips, function(t, e) {
                    n._updateContent(e)
                })
            },
            _disable: function() {
                var e = this;
                t.each(this.tooltips, function(i, n) {
                    var s = t.Event("blur");
                    s.target = s.currentTarget = n[0], e.close(s, !0)
                }), this.element.find(this.options.items).andSelf().each(function() {
                    var e = t(this);
                    e.is("[title]") && e.data("ui-tooltip-title", e.attr("title")).attr("title", "")
                })
            },
            _enable: function() {
                this.element.find(this.options.items).andSelf().each(function() {
                    var e = t(this);
                    e.data("ui-tooltip-title") && e.attr("title", e.data("ui-tooltip-title"))
                })
            },
            open: function(e) {
                var i = this,
                    n = t(e ? e.target : this.element).closest(this.options.items);
                if (n.length) {
                    if (this.options.track && n.data("ui-tooltip-id")) return this._find(n).position(t.extend({ of: n
                    }, this.options.position)), void this._off(this.document, "mousemove");
                    n.attr("title") && n.data("ui-tooltip-title", n.attr("title")), n.data("tooltip-open", !0), e && "mouseover" === e.type && n.parents().each(function() {
                        var e;
                        t(this).data("tooltip-open") && (e = t.Event("blur"), e.target = e.currentTarget = this, i.close(e, !0)), this.title && (t(this).uniqueId(), i.parents[this.id] = {
                            element: this,
                            title: this.title
                        }, this.title = "")
                    }), this._updateContent(n, e)
                }
            },
            _updateContent: function(t, e) {
                var i, n = this.options.content,
                    s = this;
                if ("string" == typeof n) return this._open(e, t, n);
                (i = n.call(t[0], function(i) {
                    t.data("tooltip-open") && s._delay(function() {
                        this._open(e, t, i)
                    })
                })) && this._open(e, t, i)
            },
            _open: function(i, n, s) {
                function a(t) {
                    c.of = t, o.is(":hidden") || o.position(c)
                }
                var o, r, l, c = t.extend({}, this.options.position);
                if (s) {
                    if (o = this._find(n), o.length) return void o.find(".ui-tooltip-content").html(s);
                    n.is("[title]") && (i && "mouseover" === i.type ? n.attr("title", "") : n.removeAttr("title")), o = this._tooltip(n), e(n, o.attr("id")), o.find(".ui-tooltip-content").html(s), this.options.track && i && /^mouse/.test(i.originalEvent.type) ? (this._on(this.document, {
                        mousemove: a
                    }), a(i)) : o.position(t.extend({ of: n
                    }, this.options.position)), o.hide(), this._show(o, this.options.show), this.options.show && this.options.show.delay && (l = setInterval(function() {
                        o.is(":visible") && (a(c.of), clearInterval(l))
                    }, t.fx.interval)), this._trigger("open", i, {
                        tooltip: o
                    }), r = {
                        keyup: function(e) {
                            if (e.keyCode === t.ui.keyCode.ESCAPE) {
                                var i = t.Event(e);
                                i.currentTarget = n[0], this.close(i, !0)
                            }
                        },
                        remove: function() {
                            this._removeTooltip(o)
                        }
                    }, i && "mouseover" !== i.type || (r.mouseleave = "close"), i && "focusin" !== i.type || (r.focusout = "close"), this._on(n, r)
                }
            },
            close: function(e) {
                var n = this,
                    s = t(e ? e.currentTarget : this.element),
                    a = this._find(s);
                this.closing || (s.data("ui-tooltip-title") && s.attr("title", s.data("ui-tooltip-title")), i(s), a.stop(!0), this._hide(a, this.options.hide, function() {
                    n._removeTooltip(t(this))
                }), s.removeData("tooltip-open"), this._off(s, "mouseleave focusout keyup"), s[0] !== this.element[0] && this._off(s, "remove"), this._off(this.document, "mousemove"), e && "mouseleave" === e.type && t.each(this.parents, function(t, e) {
                    e.element.title = e.title, delete n.parents[t]
                }), this.closing = !0, this._trigger("close", e, {
                    tooltip: a
                }), this.closing = !1)
            },
            _tooltip: function(e) {
                var i = "ui-tooltip-" + n++,
                    s = t("<div>").attr({
                        id: i,
                        role: "tooltip"
                    }).addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content " + (this.options.tooltipClass || ""));
                return t("<div>").addClass("ui-tooltip-content").appendTo(s), s.appendTo(this.document[0].body), t.fn.bgiframe && s.bgiframe(), this.tooltips[i] = e, s
            },
            _find: function(e) {
                var i = e.data("ui-tooltip-id");
                return i ? t("#" + i) : t()
            },
            _removeTooltip: function(t) {
                t.remove(), delete this.tooltips[t.attr("id")]
            },
            _destroy: function() {
                var e = this;
                t.each(this.tooltips, function(i, n) {
                    var s = t.Event("blur");
                    s.target = s.currentTarget = n[0], e.close(s, !0), t("#" + i).remove(), n.data("ui-tooltip-title") && (n.attr("title", n.data("ui-tooltip-title")), n.removeData("ui-tooltip-title"))
                })
            }
        })
    }(jQuery), $(document).on("keyup", "#check_hours", function() {
        $(".min_time").val("00"), $(".min_tab_select").val("00")
    }),
    function(t) {
        t.fn.timepicki = function(e) {
            var i = {
                    format_output: function(t, e, i) {
                        return n.show_meridian ? (t = Math.min(Math.max(parseInt(t), 1), 12), t < 10 && (t = "0" + t), e = Math.min(Math.max(parseInt(e), 0), 59), e < 10 && (e = "0" + e), t + ":" + e + " " + i) : (t = Math.min(Math.max(parseInt(t), 0), 23), t < 10 && (t = "0" + t), e = Math.min(Math.max(parseInt(e), 0), 59), e < 10 && (e = "0" + e), e = Math.min(Math.max(parseInt(e), 0), 59), t + ":" + e)
                    },
                    increase_direction: "up",
                    custom_classes: "",
                    min_hour_value: 1,
                    max_hour_value: 12,
                    show_meridian: !0,
                    step_size_hours: "1",
                    step_size_minutes: "1",
                    overflow_minutes: !1,
                    disable_keyboard_mobile: !1,
                    reset: !1,
                    on_change: null
                },
                n = t.extend({}, i, e);
            return this.each(function() {
                function e(e) {
                    return t.contains(h[0], e[0]) || h.is(e)
                }

                function i(t, e) {
                    var i = f.find(".ti_tx input").val(),
                        s = f.find(".mi_tx input").val(),
                        o = "";
                    n.show_meridian && (o = f.find(".mer_tx input").val()), 0 === i.length || 0 === s.length || n.show_meridian && 0 === o.length || (u.attr("data-timepicki-tim", i), u.attr("data-timepicki-mini", s), n.show_meridian ? (u.attr("data-timepicki-meri", o), u.val(n.format_output(i, s, o))) : u.val(n.format_output(i, s))), null !== n.on_change && n.on_change(u[0]), e && a()
                }

                function s() {
                    o(n.start_time), f.fadeIn();
                    var e = f.find("input:visible").first();
                    e.focus();
                    var i = function(n) {
                        if (9 === n.which && n.shiftKey) {
                            e.off("keydown", i);
                            var s = t(":input:visible:not(.timepicki-input)"),
                                a = s.index(u);
                            s.get(a - 1).focus()
                        }
                    };
                    e.on("keydown", i)
                }

                function a() {
                    f.fadeOut()
                }

                function o(t) {
                    var e, i, s;
                    u.is("[data-timepicki-tim]") ? (e = Number(u.attr("data-timepicki-tim")), i = Number(u.attr("data-timepicki-mini")), n.show_meridian && (s = u.attr("data-timepicki-meri"))) : "object" == typeof t ? (e = Number(t[0]), i = Number(t[1]), n.show_meridian && (s = t[2])) : (new Date, e = "", i = "", s = "AM", n.show_meridian && (0 == e ? e = 12 : 12 == e ? s = "PM" : e > 12 && (e -= 12, s = "PM"))), e < 10 ? f.find(".ti_tx input").val("0" + e) : f.find(".ti_tx input").val(e), i < 10 ? f.find(".mi_tx input").val("0" + i) : f.find(".mi_tx input").val(i), n.show_meridian && (s < 10 ? f.find(".mer_tx input").val("0" + s) : f.find(".mer_tx input").val(s))
                }

                function r(t, e) {
                    var i = "time",
                        s = Number(f.find("." + i + " .ti_tx input").val()),
                        a = Number(n.min_hour_value),
                        o = Number(n.max_hour_value),
                        r = Number(n.step_size_hours);
                    if (t && t.hasClass("action-next") || "next" === e)
                        if (s + r > o) {
                            var l = a;
                            l = l < 10 ? "0" + l : String(l), f.find("." + i + " .ti_tx input").val(l)
                        } else s += r, s < 10 && (s = "0" + s), f.find("." + i + " .ti_tx input").val(s);
                    else if (t && t.hasClass("action-prev") || "prev" === e) {
                        var c = Number(n.min_hour_value);
                        if (s - r < c) {
                            var u = o;
                            u = u < 10 ? "0" + u : String(u), f.find("." + i + " .ti_tx input").val(u)
                        } else s -= r, s < 10 && (s = "0" + s), f.find("." + i + " .ti_tx input").val(s)
                    }
                }

                function l(t, e) {
                    var i = "mins",
                        s = Number(f.find("." + i + " .mi_tx input").val()),
                        a = 59,
                        o = Number(n.step_size_minutes);
                    t && t.hasClass("action-next") || "next" === e ? s + o > a ? (f.find("." + i + " .mi_tx input").val("00"), n.overflow_minutes && r(null, "next")) : (s += o, s < 10 ? f.find("." + i + " .mi_tx input").val("0" + s) : f.find("." + i + " .mi_tx input").val(s)) : (t && t.hasClass("action-prev") || "prev" === e) && (s - o <= -1 ? (f.find("." + i + " .mi_tx input").val(a + 1 - o), n.overflow_minutes && r(null, "prev")) : (s -= o, s < 10 ? f.find("." + i + " .mi_tx input").val("0" + s) : f.find("." + i + " .mi_tx input").val(s)))
                }

                function c(t, e) {
                    var i = "meridian",
                        n = null;
                    n = f.find("." + i + " .mer_tx input").val(), t && t.hasClass("action-next") || "next" === e ? "AM" == n ? f.find("." + i + " .mer_tx input").val("PM") : f.find("." + i + " .mer_tx input").val("AM") : (t && t.hasClass("action-prev") || "prev" === e) && ("AM" == n ? f.find("." + i + " .mer_tx input").val("PM") : f.find("." + i + " .mer_tx input").val("AM"))
                }
                var u = t(this),
                    d = u.outerHeight();
                d += 10, t(u).wrap("<div class='time_pick'>");
                var h = t(this).parents(".time_pick"),
                    p = (n.increase_direction, n.increase_direction, t("<div class='timepicker_wrap " + n.custom_classes + "'><div class='arrow_top'></div><div class='time'><div class='ti_tx'><input type='text' class='timepicki-input'  id = 'check_hours'" + (n.disable_keyboard_mobile ? "readonly" : "") + "></div></div><span class ='add_symbol'>:</span><div class='mins'><div class='mi_tx'><input type='hidden' class='timepicki-input min_time'><select class= 'min_tab_select' list='cityname' onchange = 'change_min(this);'><option >00</option><option >15</option><option >30</option><option >45</option></select></div></div>"));
                n.show_meridian && p.append("<div class='meridian'><div class='mer_tx'><input type='button' class='timepicki-input time_picker_format' onclick = 'change_format(this);' ></div></div>"), n.reset && p.append("<div><a href='#' class='reset_time'>Reset</a></div>"), h.append(p);
                var f = t(this).next(".timepicker_wrap"),
                    m = (f.find("div"), h.find("input"));
                t(".reset_time").on("click", function() {
                    u.val(""), a()
                }), t(".timepicki-input").keydown(function(e) {
                    function s() {
                        var t = /^\d+$/.test(l.val()),
                            i = "" === l.val();
                        if (r.hasClass("time"))
                            if (t) {
                                var s = n.show_meridian ? Math.min(Math.max(parseInt(l.val()), 1), 12) : Math.min(Math.max(parseInt(l.val()), 0), 23);
                                l.val(s)
                            } else i || l.val(c);
                        else if (r.hasClass("mins"))
                            if (t) {
                                var a = Math.min(Math.max(parseInt(l.val()), 0), 59);
                                l.val(a)
                            } else i || l.val(c);
                        else r.hasClass("meridian") && e.preventDefault()
                    }

                    function o() {
                        done || (s(), done = !0)
                    }
                    if (13 == e.keyCode) return e.preventDefault(), i(), void a();
                    var r = t(this).parent().parent(),
                        l = t(this),
                        c = l.val();
                    done = !1, setTimeout(o, 0)
                }), t(document).on("click", function(n) {
                    if (!t(n.target).is(f) && "block" == f.css("display") && !t(n.target).is(t(".reset_time")))
                        if (t(n.target).is(u)) {
                            var a = 0;
                            f.css({
                                top: d + "px",
                                left: a + "px"
                            }), s()
                        } else i(n, !e(t(n.target)))
                }), u.on("focus", s), m.on("focus", function() {
                    var e = t(this);
                    e.is(u) || e.select()
                }), m.on("keydown", function(e) {
                    var i, s = t(this);
                    38 === e.which ? i = "down" === n.increase_direction ? "prev" : "next" : 40 === e.which && (i = "down" === n.increase_direction ? "next" : "prev"), s.closest(".timepicker_wrap .time").length ? r(null, i) : s.closest(".timepicker_wrap .mins").length ? l(null, i) : s.closest(".timepicker_wrap .meridian").length && n.show_meridian && c(null, i)
                }), m.on("blur", function() {
                    setTimeout(function() {
                        var n = t(document.activeElement);
                        n.is(":input") && !e(n) && (i(), a())
                    }, 0)
                });
                var g = f.find(".action-next"),
                    _ = f.find(".action-prev");
                t(_).add(g).on("click", function() {
                    var e = t(this);
                    "time" == e.parent().attr("class") ? r(e) : "mins" == e.parent().attr("class") ? l(e) : n.show_meridian && c(e)
                })
            })
        }
    }(jQuery),
    function(t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e, window, document)
        }) : "object" == typeof exports ? module.exports = function(e, i) {
            return e || (e = window), i || (i = "undefined" != typeof window ? require("jquery") : require("jquery")(e)), t(i, e, e.document)
        } : t(jQuery, window, document)
    }(function(t, e, i, n) {
        function s(e) {
            var i, n, a = {};
            t.each(e, function(t) {
                (i = t.match(/^([^A-Z]+?)([A-Z])/)) && -1 !== "a aa ai ao as b fn i m o s ".indexOf(i[1] + " ") && (n = t.replace(i[0], i[2].toLowerCase()), a[n] = t, "o" === i[1] && s(e[t]))
            }), e._hungarianMap = a
        }

        function a(e, i, o) {
            e._hungarianMap || s(e);
            var r;
            t.each(i, function(s) {
                (r = e._hungarianMap[s]) === n || !o && i[r] !== n || ("o" === r.charAt(0) ? (i[r] || (i[r] = {}), t.extend(!0, i[r], i[s]), a(e[r], i[r], o)) : i[r] = i[s])
            })
        }

        function o(t) {
            var e = Yt.defaults.oLanguage,
                i = t.sZeroRecords;
            !t.sEmptyTable && i && "No data available in table" === e.sEmptyTable && Et(t, t, "sZeroRecords", "sEmptyTable"), !t.sLoadingRecords && i && "Loading..." === e.sLoadingRecords && Et(t, t, "sZeroRecords", "sLoadingRecords"), t.sInfoThousands && (t.sThousands = t.sInfoThousands), (t = t.sDecimal) && Rt(t)
        }

        function r(t) {
            if (ce(t, "ordering", "bSort"), ce(t, "orderMulti", "bSortMulti"), ce(t, "orderClasses", "bSortClasses"), ce(t, "orderCellsTop", "bSortCellsTop"), ce(t, "order", "aaSorting"), ce(t, "orderFixed", "aaSortingFixed"), ce(t, "paging", "bPaginate"), ce(t, "pagingType", "sPaginationType"), ce(t, "pageLength", "iDisplayLength"), ce(t, "searching", "bFilter"), "boolean" == typeof t.sScrollX && (t.sScrollX = t.sScrollX ? "100%" : ""), "boolean" == typeof t.scrollX && (t.scrollX = t.scrollX ? "100%" : ""), t = t.aoSearchCols)
                for (var e = 0, i = t.length; e < i; e++) t[e] && a(Yt.models.oSearch, t[e])
        }

        function l(e) {
            ce(e, "orderable", "bSortable"), ce(e, "orderData", "aDataSort"), ce(e, "orderSequence", "asSorting"), ce(e, "orderDataType", "sortDataType");
            var i = e.aDataSort;
            "number" == typeof i && !t.isArray(i) && (e.aDataSort = [i])
        }

        function c(i) {
            if (!Yt.__browser) {
                var n = {};
                Yt.__browser = n;
                var s = t("<div/>").css({
                        position: "fixed",
                        top: 0,
                        left: -1 * t(e).scrollLeft(),
                        height: 1,
                        width: 1,
                        overflow: "hidden"
                    }).append(t("<div/>").css({
                        position: "absolute",
                        top: 1,
                        left: 1,
                        width: 100,
                        overflow: "scroll"
                    }).append(t("<div/>").css({
                        width: "100%",
                        height: 10
                    }))).appendTo("body"),
                    a = s.children(),
                    o = a.children();
                n.barWidth = a[0].offsetWidth - a[0].clientWidth, n.bScrollOversize = 100 === o[0].offsetWidth && 100 !== a[0].clientWidth, n.bScrollbarLeft = 1 !== Math.round(o.offset().left), n.bBounding = !!s[0].getBoundingClientRect().width, s.remove()
            }
            t.extend(i.oBrowser, Yt.__browser), i.oScroll.iBarWidth = Yt.__browser.barWidth
        }

        function u(t, e, i, s, a, o) {
            var r, l = !1;
            for (i !== n && (r = i, l = !0); s !== a;) t.hasOwnProperty(s) && (r = l ? e(r, t[s], s, t) : t[s], l = !0, s += o);
            return r
        }

        function d(e, n) {
            var s = Yt.defaults.column,
                a = e.aoColumns.length,
                s = t.extend({}, Yt.models.oColumn, s, {
                    nTh: n || i.createElement("th"),
                    sTitle: s.sTitle ? s.sTitle : n ? n.innerHTML : "",
                    aDataSort: s.aDataSort ? s.aDataSort : [a],
                    mData: s.mData ? s.mData : a,
                    idx: a
                });
            e.aoColumns.push(s), s = e.aoPreSearchCols, s[a] = t.extend({}, Yt.models.oSearch, s[a]), h(e, a, t(n).data())
        }

        function h(e, i, s) {
            var i = e.aoColumns[i],
                o = e.oClasses,
                r = t(i.nTh);
            if (!i.sWidthOrig) {
                i.sWidthOrig = r.attr("width") || null;
                var c = (r.attr("style") || "").match(/width:\s*(\d+[pxem%]+)/);
                c && (i.sWidthOrig = c[1])
            }
            s !== n && null !== s && (l(s), a(Yt.defaults.column, s), s.mDataProp !== n && !s.mData && (s.mData = s.mDataProp), s.sType && (i._sManualType = s.sType), s.className && !s.sClass && (s.sClass = s.className), t.extend(i, s), Et(i, s, "sWidth", "sWidthOrig"), s.iDataSort !== n && (i.aDataSort = [s.iDataSort]), Et(i, s, "aDataSort"));
            var u = i.mData,
                d = C(u),
                h = i.mRender ? C(i.mRender) : null,
                s = function(t) {
                    return "string" == typeof t && -1 !== t.indexOf("@")
                };
            i._bAttrSrc = t.isPlainObject(u) && (s(u.sort) || s(u.type) || s(u.filter)), i._setter = null, i.fnGetData = function(t, e, i) {
                var s = d(t, e, n, i);
                return h && e ? h(s, e, t, i) : s
            }, i.fnSetData = function(t, e, i) {
                return D(u)(t, e, i)
            }, "number" != typeof u && (e._rowReadObject = !0), e.oFeatures.bSort || (i.bSortable = !1, r.addClass(o.sSortableNone)), e = -1 !== t.inArray("asc", i.asSorting), s = -1 !== t.inArray("desc", i.asSorting), i.bSortable && (e || s) ? e && !s ? (i.sSortingClass = o.sSortableAsc, i.sSortingClassJUI = o.sSortJUIAscAllowed) : !e && s ? (i.sSortingClass = o.sSortableDesc, i.sSortingClassJUI = o.sSortJUIDescAllowed) : (i.sSortingClass = o.sSortable, i.sSortingClassJUI = o.sSortJUI) : (i.sSortingClass = o.sSortableNone, i.sSortingClassJUI = "")
        }

        function p(t) {
            if (!1 !== t.oFeatures.bAutoWidth) {
                var e = t.aoColumns;
                mt(t);
                for (var i = 0, n = e.length; i < n; i++) e[i].nTh.style.width = e[i].sWidth
            }
            e = t.oScroll, ("" !== e.sY || "" !== e.sX) && pt(t), Ft(t, null, "column-sizing", [t])
        }

        function f(t, e) {
            var i = _(t, "bVisible");
            return "number" == typeof i[e] ? i[e] : null
        }

        function m(e, i) {
            var n = _(e, "bVisible"),
                n = t.inArray(i, n);
            return -1 !== n ? n : null
        }

        function g(e) {
            var i = 0;
            return t.each(e.aoColumns, function(e, n) {
                n.bVisible && "none" !== t(n.nTh).css("display") && i++
            }), i
        }

        function _(e, i) {
            var n = [];
            return t.map(e.aoColumns, function(t, e) {
                t[i] && n.push(e)
            }), n
        }

        function v(t) {
            var e, i, s, a, o, r, l, c, u, d = t.aoColumns,
                h = t.aoData,
                p = Yt.ext.type.detect;
            for (e = 0, i = d.length; e < i; e++)
                if (l = d[e], u = [], !l.sType && l._sManualType) l.sType = l._sManualType;
                else if (!l.sType) {
                for (s = 0, a = p.length; s < a; s++) {
                    for (o = 0, r = h.length; o < r && (u[o] === n && (u[o] = x(t, o, e, "type")), (c = p[s](u[o], t)) || s === p.length - 1) && "html" !== c; o++);
                    if (c) {
                        l.sType = c;
                        break
                    }
                }
                l.sType || (l.sType = "string")
            }
        }

        function b(e, i, s, a) {
            var o, r, l, c, u, h, p = e.aoColumns;
            if (i)
                for (o = i.length - 1; 0 <= o; o--) {
                    h = i[o];
                    var f = h.targets !== n ? h.targets : h.aTargets;
                    for (t.isArray(f) || (f = [f]), r = 0, l = f.length; r < l; r++)
                        if ("number" == typeof f[r] && 0 <= f[r]) {
                            for (; p.length <= f[r];) d(e);
                            a(f[r], h)
                        } else if ("number" == typeof f[r] && 0 > f[r]) a(p.length + f[r], h);
                    else if ("string" == typeof f[r])
                        for (c = 0, u = p.length; c < u; c++)("_all" == f[r] || t(p[c].nTh).hasClass(f[r])) && a(c, h)
                }
            if (s)
                for (o = 0, e = s.length; o < e; o++) a(o, s[o])
        }

        function y(e, i, s, a) {
            var o = e.aoData.length,
                r = t.extend(!0, {}, Yt.models.oRow, {
                    src: s ? "dom" : "data",
                    idx: o
                });
            r._aData = i, e.aoData.push(r);
            for (var l = e.aoColumns, c = 0, u = l.length; c < u; c++) l[c].sType = null;
            return e.aiDisplayMaster.push(o), i = e.rowIdFn(i), i !== n && (e.aIds[i] = r), (s || !e.oFeatures.bDeferRender) && M(e, o, s, a), o
        }

        function w(e, i) {
            var n;
            return i instanceof t || (i = t(i)), i.map(function(t, i) {
                return n = E(e, i), y(e, n.data, i, n.cells)
            })
        }

        function x(t, e, i, s) {
            var a = t.iDraw,
                o = t.aoColumns[i],
                r = t.aoData[e]._aData,
                l = o.sDefaultContent,
                c = o.fnGetData(r, s, {
                    settings: t,
                    row: e,
                    col: i
                });
            if (c === n) return t.iDrawError != a && null === l && (At(t, 0, "Requested unknown parameter " + ("function" == typeof o.mData ? "{function}" : "'" + o.mData + "'") + " for row " + e + ", column " + i, 4), t.iDrawError = a), l;
            if (c !== r && null !== c || null === l || s === n) {
                if ("function" == typeof c) return c.call(r)
            } else c = l;
            return null === c && "display" == s ? "" : c
        }

        function $(t, e, i, n) {
            t.aoColumns[i].fnSetData(t.aoData[e]._aData, n, {
                settings: t,
                row: e,
                col: i
            })
        }

        function k(e) {
            return t.map(e.match(/(\\.|[^\.])+/g) || [""], function(t) {
                return t.replace(/\\\./g, ".")
            })
        }

        function C(e) {
            if (t.isPlainObject(e)) {
                var i = {};
                return t.each(e, function(t, e) {
                        e && (i[t] = C(e))
                    }),
                    function(t, e, s, a) {
                        var o = i[e] || i._;
                        return o !== n ? o(t, e, s, a) : t
                    }
            }
            if (null === e) return function(t) {
                return t
            };
            if ("function" == typeof e) return function(t, i, n, s) {
                return e(t, i, n, s)
            };
            if ("string" == typeof e && (-1 !== e.indexOf(".") || -1 !== e.indexOf("[") || -1 !== e.indexOf("("))) {
                var s = function(e, i, a) {
                    var o, r;
                    if ("" !== a) {
                        r = k(a);
                        for (var l = 0, c = r.length; l < c; l++) {
                            if (a = r[l].match(ue), o = r[l].match(de), a) {
                                if (r[l] = r[l].replace(ue, ""), "" !== r[l] && (e = e[r[l]]), o = [], r.splice(0, l + 1), r = r.join("."), t.isArray(e))
                                    for (l = 0, c = e.length; l < c; l++) o.push(s(e[l], i, r));
                                e = a[0].substring(1, a[0].length - 1), e = "" === e ? o : o.join(e);
                                break
                            }
                            if (o) r[l] = r[l].replace(de, ""), e = e[r[l]]();
                            else {
                                if (null === e || e[r[l]] === n) return n;
                                e = e[r[l]]
                            }
                        }
                    }
                    return e
                };
                return function(t, i) {
                    return s(t, i, e)
                }
            }
            return function(t) {
                return t[e]
            }
        }

        function D(e) {
            if (t.isPlainObject(e)) return D(e._);
            if (null === e) return function() {};
            if ("function" == typeof e) return function(t, i, n) {
                e(t, "set", i, n)
            };
            if ("string" == typeof e && (-1 !== e.indexOf(".") || -1 !== e.indexOf("[") || -1 !== e.indexOf("("))) {
                var i = function(e, s, a) {
                    var o, a = k(a);
                    o = a[a.length - 1];
                    for (var r, l, c = 0, u = a.length - 1; c < u; c++) {
                        if (r = a[c].match(ue), l = a[c].match(de), r) {
                            if (a[c] = a[c].replace(ue, ""), e[a[c]] = [], o = a.slice(), o.splice(0, c + 1), r = o.join("."), t.isArray(s))
                                for (l = 0, u = s.length; l < u; l++) o = {}, i(o, s[l], r), e[a[c]].push(o);
                            else e[a[c]] = s;
                            return
                        }
                        l && (a[c] = a[c].replace(de, ""), e = e[a[c]](s)), null !== e[a[c]] && e[a[c]] !== n || (e[a[c]] = {}), e = e[a[c]]
                    }
                    o.match(de) ? e[o.replace(de, "")](s) : e[o.replace(ue, "")] = s
                };
                return function(t, n) {
                    return i(t, n, e)
                }
            }
            return function(t, i) {
                t[e] = i
            }
        }

        function S(t) {
            return se(t.aoData, "_aData")
        }

        function T(t) {
            t.aoData.length = 0, t.aiDisplayMaster.length = 0, t.aiDisplay.length = 0, t.aIds = {}
        }

        function I(t, e, i) {
            for (var s = -1, a = 0, o = t.length; a < o; a++) t[a] == e ? s = a : t[a] > e && t[a]--; - 1 != s && i === n && t.splice(s, 1)
        }

        function A(t, e, i, s) {
            var a, o = t.aoData[e],
                r = function(i, n) {
                    for (; i.childNodes.length;) i.removeChild(i.firstChild);
                    i.innerHTML = x(t, e, n, "display")
                };
            if ("dom" !== i && (i && "auto" !== i || "dom" !== o.src)) {
                var l = o.anCells;
                if (l)
                    if (s !== n) r(l[s], s);
                    else
                        for (i = 0, a = l.length; i < a; i++) r(l[i], i)
            } else o._aData = E(t, o, s, s === n ? n : o._aData).data;
            if (o._aSortData = null, o._aFilterData = null, r = t.aoColumns, s !== n) r[s].sType = null;
            else {
                for (i = 0, a = r.length; i < a; i++) r[i].sType = null;
                P(t, o)
            }
        }

        function E(e, i, s, a) {
            var o, r, l, c = [],
                u = i.firstChild,
                d = 0,
                h = e.aoColumns,
                p = e._rowReadObject,
                a = a !== n ? a : p ? {} : [],
                f = function(t, e) {
                    if ("string" == typeof t) {
                        var i = t.indexOf("@"); - 1 !== i && (i = t.substring(i + 1), D(t)(a, e.getAttribute(i)))
                    }
                },
                m = function(e) {
                    s !== n && s !== d || (r = h[d], l = t.trim(e.innerHTML), r && r._bAttrSrc ? (D(r.mData._)(a, l), f(r.mData.sort, e), f(r.mData.type, e), f(r.mData.filter, e)) : p ? (r._setter || (r._setter = D(r.mData)), r._setter(a, l)) : a[d] = l), d++
                };
            if (u)
                for (; u;) o = u.nodeName.toUpperCase(), "TD" != o && "TH" != o || (m(u), c.push(u)), u = u.nextSibling;
            else
                for (c = i.anCells, u = 0, o = c.length; u < o; u++) m(c[u]);
            return (i = i.firstChild ? i : i.nTr) && (i = i.getAttribute("id")) && D(e.rowId)(a, i), {
                data: a,
                cells: c
            }
        }

        function M(e, n, s, a) {
            var o, r, l, c, u, d = e.aoData[n],
                h = d._aData,
                p = [];
            if (null === d.nTr) {
                for (o = s || i.createElement("tr"), d.nTr = o, d.anCells = p, o._DT_RowIndex = n, P(e, d), c = 0, u = e.aoColumns.length; c < u; c++) l = e.aoColumns[c], r = s ? a[c] : i.createElement(l.sCellType), r._DT_CellIndex = {
                    row: n,
                    column: c
                }, p.push(r), s && !l.mRender && l.mData === c || t.isPlainObject(l.mData) && l.mData._ === c + ".display" || (r.innerHTML = x(e, n, c, "display")), l.sClass && (r.className += " " + l.sClass), l.bVisible && !s ? o.appendChild(r) : !l.bVisible && s && r.parentNode.removeChild(r), l.fnCreatedCell && l.fnCreatedCell.call(e.oInstance, r, x(e, n, c), h, n, c);
                Ft(e, "aoRowCreatedCallback", null, [o, h, n])
            }
            d.nTr.setAttribute("role", "row")
        }

        function P(e, i) {
            var n = i.nTr,
                s = i._aData;
            if (n) {
                var a = e.rowIdFn(s);
                a && (n.id = a), s.DT_RowClass && (a = s.DT_RowClass.split(" "), i.__rowc = i.__rowc ? le(i.__rowc.concat(a)) : a, t(n).removeClass(i.__rowc.join(" ")).addClass(s.DT_RowClass)), s.DT_RowAttr && t(n).attr(s.DT_RowAttr), s.DT_RowData && t(n).data(s.DT_RowData)
            }
        }

        function N(e) {
            var i, n, s, a, o, r = e.nTHead,
                l = e.nTFoot,
                c = 0 === t("th, td", r).length,
                u = e.oClasses,
                d = e.aoColumns;
            for (c && (a = t("<tr/>").appendTo(r)), i = 0, n = d.length; i < n; i++) o = d[i], s = t(o.nTh).addClass(o.sClass), c && s.appendTo(a), e.oFeatures.bSort && (s.addClass(o.sSortingClass), !1 !== o.bSortable && (s.attr("tabindex", e.iTabIndex).attr("aria-controls", e.sTableId), kt(e, o.nTh, i))), o.sTitle != s[0].innerHTML && s.html(o.sTitle), Lt(e, "header")(e, s, o, u);
            if (c && O(e.aoHeader, r), t(r).find(">tr").attr("role", "row"), t(r).find(">tr>th, >tr>td").addClass(u.sHeaderTH), t(l).find(">tr>th, >tr>td").addClass(u.sFooterTH), null !== l)
                for (e = e.aoFooter[0], i = 0, n = e.length; i < n; i++) o = d[i], o.nTf = e[i].cell, o.sClass && t(o.nTf).addClass(o.sClass)
        }

        function F(e, i, s) {
            var a, o, r, l, c = [],
                u = [],
                d = e.aoColumns.length;
            if (i) {
                for (s === n && (s = !1), a = 0, o = i.length; a < o; a++) {
                    for (c[a] = i[a].slice(), c[a].nTr = i[a].nTr, r = d - 1; 0 <= r; r--) !e.aoColumns[r].bVisible && !s && c[a].splice(r, 1);
                    u.push([])
                }
                for (a = 0, o = c.length; a < o; a++) {
                    if (e = c[a].nTr)
                        for (; r = e.firstChild;) e.removeChild(r);
                    for (r = 0, i = c[a].length; r < i; r++)
                        if (l = d = 1, u[a][r] === n) {
                            for (e.appendChild(c[a][r].cell), u[a][r] = 1; c[a + d] !== n && c[a][r].cell == c[a + d][r].cell;) u[a + d][r] = 1, d++;
                            for (; c[a][r + l] !== n && c[a][r].cell == c[a][r + l].cell;) {
                                for (s = 0; s < d; s++) u[a + s][r + l] = 1;
                                l++
                            }
                            t(c[a][r].cell).attr("rowspan", d).attr("colspan", l)
                        }
                }
            }
        }

        function j(e) {
            var i = Ft(e, "aoPreDrawCallback", "preDraw", [e]);
            if (-1 !== t.inArray(!1, i)) dt(e, !1);
            else {
                var i = [],
                    s = 0,
                    a = e.asStripeClasses,
                    o = a.length,
                    r = e.oLanguage,
                    l = e.iInitDisplayStart,
                    c = "ssp" == Ht(e),
                    u = e.aiDisplay;
                e.bDrawing = !0, l !== n && -1 !== l && (e._iDisplayStart = c ? l : l >= e.fnRecordsDisplay() ? 0 : l, e.iInitDisplayStart = -1);
                var l = e._iDisplayStart,
                    d = e.fnDisplayEnd();
                if (e.bDeferLoading) e.bDeferLoading = !1, e.iDraw++, dt(e, !1);
                else if (c) {
                    if (!e.bDestroying && !W(e)) return
                } else e.iDraw++;
                if (0 !== u.length)
                    for (r = c ? e.aoData.length : d, c = c ? 0 : l; c < r; c++) {
                        var h = u[c],
                            p = e.aoData[h];
                        if (null === p.nTr && M(e, h), h = p.nTr, 0 !== o) {
                            var f = a[s % o];
                            p._sRowStripe != f && (t(h).removeClass(p._sRowStripe).addClass(f), p._sRowStripe = f)
                        }
                        Ft(e, "aoRowCallback", null, [h, p._aData, s, c]), i.push(h), s++
                    } else s = r.sZeroRecords, 1 == e.iDraw && "ajax" == Ht(e) ? s = r.sLoadingRecords : r.sEmptyTable && 0 === e.fnRecordsTotal() && (s = r.sEmptyTable), i[0] = t("<tr/>", {
                        "class": o ? a[0] : ""
                    }).append(t("<td />", {
                        valign: "top",
                        colSpan: g(e),
                        "class": e.oClasses.sRowEmpty
                    }).html(s))[0];
                Ft(e, "aoHeaderCallback", "header", [t(e.nTHead).children("tr")[0], S(e), l, d, u]), Ft(e, "aoFooterCallback", "footer", [t(e.nTFoot).children("tr")[0], S(e), l, d, u]), a = t(e.nTBody), a.children().detach(), a.append(t(i)), Ft(e, "aoDrawCallback", "draw", [e]), e.bSorted = !1, e.bFiltered = !1, e.bDrawing = !1
            }
        }

        function L(t, e) {
            var i = t.oFeatures,
                n = i.bFilter;
            i.bSort && wt(t), n ? V(t, t.oPreviousSearch) : t.aiDisplay = t.aiDisplayMaster.slice(), !0 !== e && (t._iDisplayStart = 0), t._drawHold = e, j(t), t._drawHold = !1
        }

        function H(e) {
            var i = e.oClasses,
                n = t(e.nTable),
                n = t("<div/>").insertBefore(n),
                s = e.oFeatures,
                a = t("<div/>", {
                    id: e.sTableId + "_wrapper",
                    "class": i.sWrapper + (e.nTFoot ? "" : " " + i.sNoFooter)
                });
            e.nHolding = n[0], e.nTableWrapper = a[0], e.nTableReinsertBefore = e.nTable.nextSibling;
            for (var o, r, l, c, u, d, h = e.sDom.split(""), p = 0; p < h.length; p++) {
                if (o = null, "<" == (r = h[p])) {
                    if (l = t("<div/>")[0], "'" == (c = h[p + 1]) || '"' == c) {
                        for (u = "", d = 2; h[p + d] != c;) u += h[p + d], d++;
                        "H" == u ? u = i.sJUIHeader : "F" == u && (u = i.sJUIFooter), -1 != u.indexOf(".") ? (c = u.split("."), l.id = c[0].substr(1, c[0].length - 1), l.className = c[1]) : "#" == u.charAt(0) ? l.id = u.substr(1, u.length - 1) : l.className = u, p += d
                    }
                    a.append(l), a = t(l)
                } else if (">" == r) a = a.parent();
                else if ("l" == r && s.bPaginate && s.bLengthChange) o = rt(e);
                else if ("f" == r && s.bFilter) o = Y(e);
                else if ("r" == r && s.bProcessing) o = ut(e);
                else if ("t" == r) o = ht(e);
                else if ("i" == r && s.bInfo) o = et(e);
                else if ("p" == r && s.bPaginate) o = lt(e);
                else if (0 !== Yt.ext.feature.length)
                    for (l = Yt.ext.feature, d = 0, c = l.length; d < c; d++)
                        if (r == l[d].cFeature) {
                            o = l[d].fnInit(e);
                            break
                        }
                o && (l = e.aanFeatures, l[r] || (l[r] = []), l[r].push(o), a.append(o))
            }
            n.replaceWith(a), e.nHolding = null
        }

        function O(e, i) {
            var n, s, a, o, r, l, c, u, d, h, p = t(i).children("tr");
            for (e.splice(0, e.length), a = 0, l = p.length; a < l; a++) e.push([]);
            for (a = 0, l = p.length; a < l; a++)
                for (n = p[a], s = n.firstChild; s;) {
                    if ("TD" == s.nodeName.toUpperCase() || "TH" == s.nodeName.toUpperCase()) {
                        for (u = 1 * s.getAttribute("colspan"), d = 1 * s.getAttribute("rowspan"), u = u && 0 !== u && 1 !== u ? u : 1, d = d && 0 !== d && 1 !== d ? d : 1, o = 0, r = e[a]; r[o];) o++;
                        for (c = o, h = 1 === u, r = 0; r < u; r++)
                            for (o = 0; o < d; o++) e[a + o][c + r] = {
                                cell: s,
                                unique: h
                            }, e[a + o].nTr = n
                    }
                    s = s.nextSibling
                }
        }

        function R(t, e, i) {
            var n = [];
            i || (i = t.aoHeader, e && (i = [], O(i, e)));
            for (var e = 0, s = i.length; e < s; e++)
                for (var a = 0, o = i[e].length; a < o; a++) !i[e][a].unique || n[a] && t.bSortCellsTop || (n[a] = i[e][a].cell);
            return n
        }

        function z(e, i, n) {
            if (Ft(e, "aoServerParams", "serverParams", [i]), i && t.isArray(i)) {
                var s = {},
                    a = /(.*?)\[\]$/;
                t.each(i, function(t, e) {
                    var i = e.name.match(a);
                    i ? (i = i[0], s[i] || (s[i] = []), s[i].push(e.value)) : s[e.name] = e.value
                }), i = s
            }
            var o, r = e.ajax,
                l = e.oInstance,
                c = function(t) {
                    Ft(e, null, "xhr", [e, t, e.jqXHR]), n(t)
                };
            if (t.isPlainObject(r) && r.data) {
                o = r.data;
                var u = t.isFunction(o) ? o(i, e) : o,
                    i = t.isFunction(o) && u ? u : t.extend(!0, i, u);
                delete r.data
            }
            u = {
                data: i,
                success: function(t) {
                    var i = t.error || t.sError;
                    i && At(e, 0, i), e.json = t, c(t)
                },
                dataType: "json",
                cache: !1,
                type: e.sServerMethod,
                error: function(i, n) {
                    var s = Ft(e, null, "xhr", [e, null, e.jqXHR]); - 1 === t.inArray(!0, s) && ("parsererror" == n ? At(e, 0, "Invalid JSON response", 1) : 4 === i.readyState && At(e, 0, "Ajax error", 7)), dt(e, !1)
                }
            }, e.oAjaxData = i, Ft(e, null, "preXhr", [e, i]), e.fnServerData ? e.fnServerData.call(l, e.sAjaxSource, t.map(i, function(t, e) {
                return {
                    name: e,
                    value: t
                }
            }), c, e) : e.sAjaxSource || "string" == typeof r ? e.jqXHR = t.ajax(t.extend(u, {
                url: r || e.sAjaxSource
            })) : t.isFunction(r) ? e.jqXHR = r.call(l, i, c, e) : (e.jqXHR = t.ajax(t.extend(u, r)), r.data = o)
        }

        function W(t) {
            return !t.bAjaxDataGet || (t.iDraw++, dt(t, !0), z(t, B(t), function(e) {
                q(t, e)
            }), !1)
        }

        function B(e) {
            var i, n, s, a, o = e.aoColumns,
                r = o.length,
                l = e.oFeatures,
                c = e.oPreviousSearch,
                u = e.aoPreSearchCols,
                d = [],
                h = yt(e);
            i = e._iDisplayStart, n = !1 !== l.bPaginate ? e._iDisplayLength : -1;
            var p = function(t, e) {
                d.push({
                    name: t,
                    value: e
                })
            };
            p("sEcho", e.iDraw), p("iColumns", r), p("sColumns", se(o, "sName").join(",")), p("iDisplayStart", i), p("iDisplayLength", n);
            var f = {
                draw: e.iDraw,
                columns: [],
                order: [],
                start: i,
                length: n,
                search: {
                    value: c.sSearch,
                    regex: c.bRegex
                }
            };
            for (i = 0; i < r; i++) s = o[i], a = u[i], n = "function" == typeof s.mData ? "function" : s.mData, f.columns.push({
                data: n,
                name: s.sName,
                searchable: s.bSearchable,
                orderable: s.bSortable,
                search: {
                    value: a.sSearch,
                    regex: a.bRegex
                }
            }), p("mDataProp_" + i, n), l.bFilter && (p("sSearch_" + i, a.sSearch), p("bRegex_" + i, a.bRegex), p("bSearchable_" + i, s.bSearchable)), l.bSort && p("bSortable_" + i, s.bSortable);
            return l.bFilter && (p("sSearch", c.sSearch), p("bRegex", c.bRegex)), l.bSort && (t.each(h, function(t, e) {
                f.order.push({
                    column: e.col,
                    dir: e.dir
                }), p("iSortCol_" + t, e.col), p("sSortDir_" + t, e.dir)
            }), p("iSortingCols", h.length)), o = Yt.ext.legacy.ajax, null === o ? e.sAjaxSource ? d : f : o ? d : f
        }

        function q(t, e) {
            var i = U(t, e),
                s = e.sEcho !== n ? e.sEcho : e.draw,
                a = e.iTotalRecords !== n ? e.iTotalRecords : e.recordsTotal,
                o = e.iTotalDisplayRecords !== n ? e.iTotalDisplayRecords : e.recordsFiltered;
            if (s) {
                if (1 * s < t.iDraw) return;
                t.iDraw = 1 * s
            }
            for (T(t), t._iRecordsTotal = parseInt(a, 10), t._iRecordsDisplay = parseInt(o, 10), s = 0, a = i.length; s < a; s++) y(t, i[s]);
            t.aiDisplay = t.aiDisplayMaster.slice(), t.bAjaxDataGet = !1, j(t), t._bInitComplete || at(t, e), t.bAjaxDataGet = !0, dt(t, !1)
        }

        function U(e, i) {
            var s = t.isPlainObject(e.ajax) && e.ajax.dataSrc !== n ? e.ajax.dataSrc : e.sAjaxDataProp;
            return "data" === s ? i.aaData || i[s] : "" !== s ? C(s)(i) : i
        }

        function Y(e) {
            var n = e.oClasses,
                s = e.sTableId,
                a = e.oLanguage,
                o = e.oPreviousSearch,
                r = e.aanFeatures,
                l = '<input type="search" class="' + n.sFilterInput + '"/>',
                c = a.sSearch,
                c = c.match(/_INPUT_/) ? c.replace("_INPUT_", l) : c + l,
                n = t("<div/>", {
                    id: r.f ? null : s + "_filter",
                    "class": n.sFilter
                }).append(t("<label/>").append(c)),
                r = function() {
                    var t = this.value ? this.value : "";
                    t != o.sSearch && (V(e, {
                        sSearch: t,
                        bRegex: o.bRegex,
                        bSmart: o.bSmart,
                        bCaseInsensitive: o.bCaseInsensitive
                    }), e._iDisplayStart = 0, j(e))
                },
                l = null !== e.searchDelay ? e.searchDelay : "ssp" === Ht(e) ? 400 : 0,
                u = t("input", n).val(o.sSearch).attr("placeholder", a.sSearchPlaceholder).on("keyup.DT search.DT input.DT paste.DT cut.DT", l ? ge(r, l) : r).on("keypress.DT", function(t) {
                    if (13 == t.keyCode) return !1
                }).attr("aria-controls", s);
            return t(e.nTable).on("search.dt.DT", function(t, n) {
                if (e === n) try {
                    u[0] !== i.activeElement && u.val(o.sSearch)
                } catch (t) {}
            }), n[0]
        }

        function V(t, e, i) {
            var s = t.oPreviousSearch,
                a = t.aoPreSearchCols,
                o = function(t) {
                    s.sSearch = t.sSearch, s.bRegex = t.bRegex, s.bSmart = t.bSmart, s.bCaseInsensitive = t.bCaseInsensitive
                };
            if (v(t), "ssp" != Ht(t)) {
                for (Q(t, e.sSearch, i, e.bEscapeRegex !== n ? !e.bEscapeRegex : e.bRegex, e.bSmart, e.bCaseInsensitive), o(e), e = 0; e < a.length; e++) X(t, a[e].sSearch, e, a[e].bEscapeRegex !== n ? !a[e].bEscapeRegex : a[e].bRegex, a[e].bSmart, a[e].bCaseInsensitive);
                K(t)
            } else o(e);
            t.bFiltered = !0, Ft(t, null, "search", [t])
        }

        function K(e) {
            for (var i, n, s = Yt.ext.search, a = e.aiDisplay, o = 0, r = s.length; o < r; o++) {
                for (var l = [], c = 0, u = a.length; c < u; c++) n = a[c], i = e.aoData[n], s[o](e, i._aFilterData, n, i._aData, c) && l.push(n);
                a.length = 0, t.merge(a, l)
            }
        }

        function X(t, e, i, n, s, a) {
            if ("" !== e) {
                for (var o = [], r = t.aiDisplay, n = G(e, n, s, a), s = 0; s < r.length; s++) e = t.aoData[r[s]]._aFilterData[i], n.test(e) && o.push(r[s]);
                t.aiDisplay = o
            }
        }

        function Q(t, e, i, n, s, a) {
            var o, n = G(e, n, s, a),
                a = t.oPreviousSearch.sSearch,
                r = t.aiDisplayMaster,
                s = [];
            if (0 !== Yt.ext.search.length && (i = !0), o = J(t), 0 >= e.length) t.aiDisplay = r.slice();
            else {
                for ((o || i || a.length > e.length || 0 !== e.indexOf(a) || t.bSorted) && (t.aiDisplay = r.slice()), e = t.aiDisplay, i = 0; i < e.length; i++) n.test(t.aoData[e[i]]._sFilterRow) && s.push(e[i]);
                t.aiDisplay = s
            }
        }

        function G(e, i, n, s) {
            return e = i ? e : he(e), n && (e = "^(?=.*?" + t.map(e.match(/"[^"]+"|[^ ]+/g) || [""], function(t) {
                if ('"' === t.charAt(0)) var e = t.match(/^"(.*)"$/),
                    t = e ? e[1] : t;
                return t.replace('"', "")
            }).join(")(?=.*?") + ").*$"), RegExp(e, s ? "i" : "")
        }

        function J(t) {
            var e, i, n, s, a, o, r, l, c = t.aoColumns,
                u = Yt.ext.type.search;
            for (e = !1, i = 0, s = t.aoData.length; i < s; i++)
                if (l = t.aoData[i], !l._aFilterData) {
                    for (o = [], n = 0, a = c.length; n < a; n++) e = c[n], e.bSearchable ? (r = x(t, i, n, "filter"), u[e.sType] && (r = u[e.sType](r)), null === r && (r = ""), "string" != typeof r && r.toString && (r = r.toString())) : r = "", r.indexOf && -1 !== r.indexOf("&") && (pe.innerHTML = r, r = fe ? pe.textContent : pe.innerText), r.replace && (r = r.replace(/[\r\n]/g, "")), o.push(r);
                    l._aFilterData = o, l._sFilterRow = o.join("  "), e = !0
                }
            return e
        }

        function Z(t) {
            return {
                search: t.sSearch,
                smart: t.bSmart,
                regex: t.bRegex,
                caseInsensitive: t.bCaseInsensitive
            }
        }

        function tt(t) {
            return {
                sSearch: t.search,
                bSmart: t.smart,
                bRegex: t.regex,
                bCaseInsensitive: t.caseInsensitive
            }
        }

        function et(e) {
            var i = e.sTableId,
                n = e.aanFeatures.i,
                s = t("<div/>", {
                    "class": e.oClasses.sInfo,
                    id: n ? null : i + "_info"
                });
            return n || (e.aoDrawCallback.push({
                fn: it,
                sName: "information"
            }), s.attr("role", "status").attr("aria-live", "polite"), t(e.nTable).attr("aria-describedby", i + "_info")), s[0]
        }

        function it(e) {
            var i = e.aanFeatures.i;
            if (0 !== i.length) {
                var n = e.oLanguage,
                    s = e._iDisplayStart + 1,
                    a = e.fnDisplayEnd(),
                    o = e.fnRecordsTotal(),
                    r = e.fnRecordsDisplay(),
                    l = r ? n.sInfo : n.sInfoEmpty;
                r !== o && (l += " " + n.sInfoFiltered), l += n.sInfoPostFix, l = nt(e, l), n = n.fnInfoCallback, null !== n && (l = n.call(e.oInstance, e, s, a, o, r, l)), t(i).html(l)
            }
        }

        function nt(t, e) {
            var i = t.fnFormatNumber,
                n = t._iDisplayStart + 1,
                s = t._iDisplayLength,
                a = t.fnRecordsDisplay(),
                o = -1 === s;
            return e.replace(/_START_/g, i.call(t, n)).replace(/_END_/g, i.call(t, t.fnDisplayEnd())).replace(/_MAX_/g, i.call(t, t.fnRecordsTotal())).replace(/_TOTAL_/g, i.call(t, a)).replace(/_PAGE_/g, i.call(t, o ? 1 : Math.ceil(n / s))).replace(/_PAGES_/g, i.call(t, o ? 1 : Math.ceil(a / s)))
        }

        function st(t) {
            var e, i, n, s = t.iInitDisplayStart,
                a = t.aoColumns;
            i = t.oFeatures;
            var o = t.bDeferLoading;
            if (t.bInitialised) {
                for (H(t), N(t), F(t, t.aoHeader), F(t, t.aoFooter), dt(t, !0), i.bAutoWidth && mt(t), e = 0, i = a.length; e < i; e++) n = a[e], n.sWidth && (n.nTh.style.width = bt(n.sWidth));
                Ft(t, null, "preInit", [t]), L(t), a = Ht(t), ("ssp" != a || o) && ("ajax" == a ? z(t, [], function(i) {
                    var n = U(t, i);
                    for (e = 0; e < n.length; e++) y(t, n[e]);
                    t.iInitDisplayStart = s, L(t), dt(t, !1), at(t, i)
                }, t) : (dt(t, !1), at(t)))
            } else setTimeout(function() {
                st(t)
            }, 200)
        }

        function at(t, e) {
            t._bInitComplete = !0, (e || t.oInit.aaData) && p(t), Ft(t, null, "plugin-init", [t, e]), Ft(t, "aoInitComplete", "init", [t, e])
        }

        function ot(t, e) {
            var i = parseInt(e, 10);
            t._iDisplayLength = i, jt(t), Ft(t, null, "length", [t, i])
        }

        function rt(e) {
            for (var i = e.oClasses, n = e.sTableId, s = e.aLengthMenu, a = t.isArray(s[0]), o = a ? s[0] : s, s = a ? s[1] : s, a = t("<select/>", {
                    name: n + "_length",
                    "aria-controls": n,
                    "class": i.sLengthSelect
                }), r = 0, l = o.length; r < l; r++) a[0][r] = new Option(s[r], o[r]);
            var c = t("<div><label/></div>").addClass(i.sLength);
            return e.aanFeatures.l || (c[0].id = n + "_length"), c.children().append(e.oLanguage.sLengthMenu.replace("_MENU_", a[0].outerHTML)), t("select", c).val(e._iDisplayLength).on("change.DT", function() {
                ot(e, t(this).val()), j(e)
            }), t(e.nTable).on("length.dt.DT", function(i, n, s) {
                e === n && t("select", c).val(s)
            }), c[0]
        }

        function lt(e) {
            var i = e.sPaginationType,
                n = Yt.ext.pager[i],
                s = "function" == typeof n,
                a = function(t) {
                    j(t)
                },
                i = t("<div/>").addClass(e.oClasses.sPaging + i)[0],
                o = e.aanFeatures;
            return s || n.fnInit(e, i, a), o.p || (i.id = e.sTableId + "_paginate", e.aoDrawCallback.push({
                fn: function(t) {
                    if (s) {
                        var e, i = t._iDisplayStart,
                            r = t._iDisplayLength,
                            l = t.fnRecordsDisplay(),
                            c = -1 === r,
                            i = c ? 0 : Math.ceil(i / r),
                            r = c ? 1 : Math.ceil(l / r),
                            l = n(i, r),
                            c = 0;
                        for (e = o.p.length; c < e; c++) Lt(t, "pageButton")(t, o.p[c], c, l, i, r)
                    } else n.fnUpdate(t, a)
                },
                sName: "pagination"
            })), i
        }

        function ct(t, e, i) {
            var n = t._iDisplayStart,
                s = t._iDisplayLength,
                a = t.fnRecordsDisplay();
            return 0 === a || -1 === s ? n = 0 : "number" == typeof e ? (n = e * s) > a && (n = 0) : "first" == e ? n = 0 : "previous" == e ? 0 > (n = 0 <= s ? n - s : 0) && (n = 0) : "next" == e ? n + s < a && (n += s) : "last" == e ? n = Math.floor((a - 1) / s) * s : At(t, 0, "Unknown paging action: " + e, 5), e = t._iDisplayStart !== n, t._iDisplayStart = n, e && (Ft(t, null, "page", [t]), i && j(t)), e
        }

        function ut(e) {
            return t("<div/>", {
                id: e.aanFeatures.r ? null : e.sTableId + "_processing",
                "class": e.oClasses.sProcessing
            }).html(e.oLanguage.sProcessing).insertBefore(e.nTable)[0]
        }

        function dt(e, i) {
            e.oFeatures.bProcessing && t(e.aanFeatures.r).css("display", i ? "block" : "none"), Ft(e, null, "processing", [e, i])
        }

        function ht(e) {
            var i = t(e.nTable);
            i.attr("role", "grid");
            var n = e.oScroll;
            if ("" === n.sX && "" === n.sY) return e.nTable;
            var s = n.sX,
                a = n.sY,
                o = e.oClasses,
                r = i.children("caption"),
                l = r.length ? r[0]._captionSide : null,
                c = t(i[0].cloneNode(!1)),
                u = t(i[0].cloneNode(!1)),
                d = i.children("tfoot");
            d.length || (d = null), c = t("<div/>", {
                "class": o.sScrollWrapper
            }).append(t("<div/>", {
                "class": o.sScrollHead
            }).css({
                overflow: "hidden",
                position: "relative",
                border: 0,
                width: s ? s ? bt(s) : null : "100%"
            }).append(t("<div/>", {
                "class": o.sScrollHeadInner
            }).css({
                "box-sizing": "content-box",
                width: n.sXInner || "100%"
            }).append(c.removeAttr("id").css("margin-left", 0).append("top" === l ? r : null).append(i.children("thead"))))).append(t("<div/>", {
                "class": o.sScrollBody
            }).css({
                position: "relative",
                overflow: "auto",
                width: s ? bt(s) : null
            }).append(i)), d && c.append(t("<div/>", {
                "class": o.sScrollFoot
            }).css({
                overflow: "hidden",
                border: 0,
                width: s ? s ? bt(s) : null : "100%"
            }).append(t("<div/>", {
                "class": o.sScrollFootInner
            }).append(u.removeAttr("id").css("margin-left", 0).append("bottom" === l ? r : null).append(i.children("tfoot")))));
            var i = c.children(),
                h = i[0],
                o = i[1],
                p = d ? i[2] : null;
            return s && t(o).on("scroll.DT", function() {
                var t = this.scrollLeft;
                h.scrollLeft = t, d && (p.scrollLeft = t)
            }), t(o).css(a && n.bCollapse ? "max-height" : "height", a), e.nScrollHead = h, e.nScrollBody = o, e.nScrollFoot = p, e.aoDrawCallback.push({
                fn: pt,
                sName: "scrolling"
            }), c[0]
        }

        function pt(e) {
            var i, s, a, o, r, l = e.oScroll,
                c = l.sX,
                u = l.sXInner,
                d = l.sY,
                l = l.iBarWidth,
                h = t(e.nScrollHead),
                m = h[0].style,
                g = h.children("div"),
                _ = g[0].style,
                v = g.children("table"),
                g = e.nScrollBody,
                b = t(g),
                y = g.style,
                w = t(e.nScrollFoot).children("div"),
                x = w.children("table"),
                $ = t(e.nTHead),
                k = t(e.nTable),
                C = k[0],
                D = C.style,
                S = e.nTFoot ? t(e.nTFoot) : null,
                T = e.oBrowser,
                I = T.bScrollOversize,
                A = se(e.aoColumns, "nTh"),
                E = [],
                M = [],
                P = [],
                N = [],
                F = function(t) {
                    t = t.style, t.paddingTop = "0", t.paddingBottom = "0", t.borderTopWidth = "0", t.borderBottomWidth = "0", t.height = 0
                };
            s = g.scrollHeight > g.clientHeight, e.scrollBarVis !== s && e.scrollBarVis !== n ? (e.scrollBarVis = s, p(e)) : (e.scrollBarVis = s, k.children("thead, tfoot").remove(), S && (a = S.clone().prependTo(k), i = S.find("tr"), a = a.find("tr")), o = $.clone().prependTo(k), $ = $.find("tr"), s = o.find("tr"), o.find("th, td").removeAttr("tabindex"), c || (y.width = "100%", h[0].style.width = "100%"), t.each(R(e, o), function(t, i) {
                r = f(e, t), i.style.width = e.aoColumns[r].sWidth
            }), S && ft(function(t) {
                t.style.width = ""
            }, a), h = k.outerWidth(), "" === c ? (D.width = "100%", I && (k.find("tbody").height() > g.offsetHeight || "scroll" == b.css("overflow-y")) && (D.width = bt(k.outerWidth() - l)), h = k.outerWidth()) : "" !== u && (D.width = bt(u), h = k.outerWidth()), ft(F, s), ft(function(e) {
                P.push(e.innerHTML), E.push(bt(t(e).css("width")))
            }, s), ft(function(e, i) {
                -1 !== t.inArray(e, A) && (e.style.width = E[i])
            }, $), t(s).height(0), S && (ft(F, a), ft(function(e) {
                N.push(e.innerHTML), M.push(bt(t(e).css("width")))
            }, a), ft(function(t, e) {
                t.style.width = M[e]
            }, i), t(a).height(0)), ft(function(t, e) {
                t.innerHTML = '<div class="dataTables_sizing" style="height:0;overflow:hidden;">' + P[e] + "</div>", t.style.width = E[e]
            }, s), S && ft(function(t, e) {
                t.innerHTML = '<div class="dataTables_sizing" style="height:0;overflow:hidden;">' + N[e] + "</div>", t.style.width = M[e]
            }, a), k.outerWidth() < h ? (i = g.scrollHeight > g.offsetHeight || "scroll" == b.css("overflow-y") ? h + l : h, I && (g.scrollHeight > g.offsetHeight || "scroll" == b.css("overflow-y")) && (D.width = bt(i - l)), ("" === c || "" !== u) && At(e, 1, "Possible column misalignment", 6)) : i = "100%", y.width = bt(i), m.width = bt(i), S && (e.nScrollFoot.style.width = bt(i)), !d && I && (y.height = bt(C.offsetHeight + l)), c = k.outerWidth(), v[0].style.width = bt(c), _.width = bt(c), u = k.height() > g.clientHeight || "scroll" == b.css("overflow-y"), d = "padding" + (T.bScrollbarLeft ? "Left" : "Right"), _[d] = u ? l + "px" : "0px", S && (x[0].style.width = bt(c), w[0].style.width = bt(c), w[0].style[d] = u ? l + "px" : "0px"), k.children("colgroup").insertBefore(k.children("thead")), b.scroll(), !e.bSorted && !e.bFiltered || e._drawHold || (g.scrollTop = 0))
        }

        function ft(t, e, i) {
            for (var n, s, a = 0, o = 0, r = e.length; o < r;) {
                for (n = e[o].firstChild, s = i ? i[o].firstChild : null; n;) 1 === n.nodeType && (i ? t(n, s, a) : t(n, a), a++), n = n.nextSibling, s = i ? s.nextSibling : null;
                o++
            }
        }

        function mt(i) {
            var n, s, a = i.nTable,
                o = i.aoColumns,
                r = i.oScroll,
                l = r.sY,
                c = r.sX,
                u = r.sXInner,
                d = o.length,
                h = _(i, "bVisible"),
                m = t("th", i.nTHead),
                v = a.getAttribute("width"),
                b = a.parentNode,
                y = !1,
                w = i.oBrowser,
                r = w.bScrollOversize;
            for ((n = a.style.width) && -1 !== n.indexOf("%") && (v = n), n = 0; n < h.length; n++) s = o[h[n]], null !== s.sWidth && (s.sWidth = gt(s.sWidthOrig, b), y = !0);
            if (r || !y && !c && !l && d == g(i) && d == m.length)
                for (n = 0; n < d; n++) null !== (h = f(i, n)) && (o[h].sWidth = bt(m.eq(n).width()));
            else {
                d = t(a).clone().css("visibility", "hidden").removeAttr("id"), d.find("tbody tr").remove();
                var x = t("<tr/>").appendTo(d.find("tbody"));
                for (d.find("thead, tfoot").remove(), d.append(t(i.nTHead).clone()).append(t(i.nTFoot).clone()), d.find("tfoot th, tfoot td").css("width", ""), m = R(i, d.find("thead")[0]), n = 0; n < h.length; n++) s = o[h[n]], m[n].style.width = null !== s.sWidthOrig && "" !== s.sWidthOrig ? bt(s.sWidthOrig) : "", s.sWidthOrig && c && t(m[n]).append(t("<div/>").css({
                    width: s.sWidthOrig,
                    margin: 0,
                    padding: 0,
                    border: 0,
                    height: 1
                }));
                if (i.aoData.length)
                    for (n = 0; n < h.length; n++) y = h[n], s = o[y], t(_t(i, y)).clone(!1).append(s.sContentPadding).appendTo(x);
                for (t("[name]", d).removeAttr("name"), s = t("<div/>").css(c || l ? {
                        position: "absolute",
                        top: 0,
                        left: 0,
                        height: 1,
                        right: 0,
                        overflow: "hidden"
                    } : {}).append(d).appendTo(b), c && u ? d.width(u) : c ? (d.css("width", "auto"), d.removeAttr("width"), d.width() < b.clientWidth && v && d.width(b.clientWidth)) : l ? d.width(b.clientWidth) : v && d.width(v), n = l = 0; n < h.length; n++) b = t(m[n]), u = b.outerWidth() - b.width(), b = w.bBounding ? Math.ceil(m[n].getBoundingClientRect().width) : b.outerWidth(), l += b, o[h[n]].sWidth = bt(b - u);
                a.style.width = bt(l), s.remove()
            }
            v && (a.style.width = bt(v)), !v && !c || i._reszEvt || (a = function() {
                t(e).on("resize.DT-" + i.sInstance, ge(function() {
                    p(i)
                }))
            }, r ? setTimeout(a, 1e3) : a(), i._reszEvt = !0)
        }

        function gt(e, n) {
            if (!e) return 0;
            var s = t("<div/>").css("width", bt(e)).appendTo(n || i.body),
                a = s[0].offsetWidth;
            return s.remove(), a
        }

        function _t(e, i) {
            var n = vt(e, i);
            if (0 > n) return null;
            var s = e.aoData[n];
            return s.nTr ? s.anCells[i] : t("<td/>").html(x(e, n, i, "display"))[0]
        }

        function vt(t, e) {
            for (var i, n = -1, s = -1, a = 0, o = t.aoData.length; a < o; a++) i = x(t, a, e, "display") + "", i = i.replace(me, ""), i = i.replace(/&nbsp;/g, " "), i.length > n && (n = i.length, s = a);
            return s
        }

        function bt(t) {
            return null === t ? "0px" : "number" == typeof t ? 0 > t ? "0px" : t + "px" : t.match(/\d$/) ? t + "px" : t
        }

        function yt(e) {
            var i, s, a, o, r, l, c = [],
                u = e.aoColumns;
            i = e.aaSortingFixed, s = t.isPlainObject(i);
            var d = [];
            for (a = function(e) {
                    e.length && !t.isArray(e[0]) ? d.push(e) : t.merge(d, e)
                }, t.isArray(i) && a(i), s && i.pre && a(i.pre), a(e.aaSorting), s && i.post && a(i.post), e = 0; e < d.length; e++)
                for (l = d[e][0], a = u[l].aDataSort, i = 0, s = a.length; i < s; i++) o = a[i], r = u[o].sType || "string", d[e]._idx === n && (d[e]._idx = t.inArray(d[e][1], u[o].asSorting)), c.push({
                    src: l,
                    col: o,
                    dir: d[e][1],
                    index: d[e]._idx,
                    type: r,
                    formatter: Yt.ext.type.order[r + "-pre"]
                });
            return c
        }

        function wt(t) {
            var e, i, n, s, a = [],
                o = Yt.ext.type.order,
                r = t.aoData,
                l = 0,
                c = t.aiDisplayMaster;
            for (v(t), s = yt(t), e = 0, i = s.length; e < i; e++) n = s[e], n.formatter && l++, Dt(t, n.col);
            if ("ssp" != Ht(t) && 0 !== s.length) {
                for (e = 0, i = c.length; e < i; e++) a[c[e]] = e;
                l === s.length ? c.sort(function(t, e) {
                    var i, n, o, l, c = s.length,
                        u = r[t]._aSortData,
                        d = r[e]._aSortData;
                    for (o = 0; o < c; o++)
                        if (l = s[o], i = u[l.col], n = d[l.col], 0 !== (i = i < n ? -1 : i > n ? 1 : 0)) return "asc" === l.dir ? i : -i;
                    return i = a[t], n = a[e], i < n ? -1 : i > n ? 1 : 0
                }) : c.sort(function(t, e) {
                    var i, n, l, c, u = s.length,
                        d = r[t]._aSortData,
                        h = r[e]._aSortData;
                    for (l = 0; l < u; l++)
                        if (c = s[l], i = d[c.col], n = h[c.col], c = o[c.type + "-" + c.dir] || o["string-" + c.dir], 0 !== (i = c(i, n))) return i;
                    return i = a[t], n = a[e], i < n ? -1 : i > n ? 1 : 0
                })
            }
            t.bSorted = !0
        }

        function xt(t) {
            for (var e, i, n = t.aoColumns, s = yt(t), t = t.oLanguage.oAria, a = 0, o = n.length; a < o; a++) {
                i = n[a];
                var r = i.asSorting;
                e = i.sTitle.replace(/<.*?>/g, "");
                var l = i.nTh;
                l.removeAttribute("aria-sort"), i.bSortable && (0 < s.length && s[0].col == a ? (l.setAttribute("aria-sort", "asc" == s[0].dir ? "ascending" : "descending"), i = r[s[0].index + 1] || r[0]) : i = r[0], e += "asc" === i ? t.sSortAscending : t.sSortDescending), l.setAttribute("aria-label", e)
            }
        }

        function $t(e, i, s, a) {
            var o = e.aaSorting,
                r = e.aoColumns[i].asSorting,
                l = function(e, i) {
                    var s = e._idx;
                    return s === n && (s = t.inArray(e[1], r)), s + 1 < r.length ? s + 1 : i ? null : 0
                };
            "number" == typeof o[0] && (o = e.aaSorting = [o]), s && e.oFeatures.bSortMulti ? (s = t.inArray(i, se(o, "0")), -1 !== s ? (i = l(o[s], !0), null === i && 1 === o.length && (i = 0), null === i ? o.splice(s, 1) : (o[s][1] = r[i], o[s]._idx = i)) : (o.push([i, r[0], 0]), o[o.length - 1]._idx = 0)) : o.length && o[0][0] == i ? (i = l(o[0]), o.length = 1, o[0][1] = r[i], o[0]._idx = i) : (o.length = 0, o.push([i, r[0]]), o[0]._idx = 0), L(e), "function" == typeof a && a(e)
        }

        function kt(t, e, i, n) {
            var s = t.aoColumns[i];
            Pt(e, {}, function(e) {
                !1 !== s.bSortable && (t.oFeatures.bProcessing ? (dt(t, !0), setTimeout(function() {
                    $t(t, i, e.shiftKey, n), "ssp" !== Ht(t) && dt(t, !1)
                }, 0)) : $t(t, i, e.shiftKey, n))
            })
        }

        function Ct(e) {
            var i, n, s = e.aLastSort,
                a = e.oClasses.sSortColumn,
                o = yt(e),
                r = e.oFeatures;
            if (r.bSort && r.bSortClasses) {
                for (r = 0, i = s.length; r < i; r++) n = s[r].src, t(se(e.aoData, "anCells", n)).removeClass(a + (2 > r ? r + 1 : 3));
                for (r = 0, i = o.length; r < i; r++) n = o[r].src, t(se(e.aoData, "anCells", n)).addClass(a + (2 > r ? r + 1 : 3))
            }
            e.aLastSort = o
        }

        function Dt(t, e) {
            var i, n = t.aoColumns[e],
                s = Yt.ext.order[n.sSortDataType];
            s && (i = s.call(t.oInstance, t, e, m(t, e)));
            for (var a, o = Yt.ext.type.order[n.sType + "-pre"], r = 0, l = t.aoData.length; r < l; r++) n = t.aoData[r], n._aSortData || (n._aSortData = []), (!n._aSortData[e] || s) && (a = s ? i[r] : x(t, r, e, "sort"), n._aSortData[e] = o ? o(a) : a)
        }

        function St(e) {
            if (e.oFeatures.bStateSave && !e.bDestroying) {
                var i = {
                    time: +new Date,
                    start: e._iDisplayStart,
                    length: e._iDisplayLength,
                    order: t.extend(!0, [], e.aaSorting),
                    search: Z(e.oPreviousSearch),
                    columns: t.map(e.aoColumns, function(t, i) {
                        return {
                            visible: t.bVisible,
                            search: Z(e.aoPreSearchCols[i])
                        }
                    })
                };
                Ft(e, "aoStateSaveParams", "stateSaveParams", [e, i]), e.oSavedState = i, e.fnStateSaveCallback.call(e.oInstance, e, i)
            }
        }

        function Tt(e, i, s) {
            var a, o, r = e.aoColumns,
                i = function(i) {
                    if (i && i.time) {
                        var l = Ft(e, "aoStateLoadParams", "stateLoadParams", [e, i]);
                        if (-1 === t.inArray(!1, l) && !(0 < (l = e.iStateDuration) && i.time < +new Date - 1e3 * l || i.columns && r.length !== i.columns.length)) {
                            if (e.oLoadedState = t.extend(!0, {}, i), i.start !== n && (e._iDisplayStart = i.start, e.iInitDisplayStart = i.start), i.length !== n && (e._iDisplayLength = i.length), i.order !== n && (e.aaSorting = [], t.each(i.order, function(t, i) {
                                    e.aaSorting.push(i[0] >= r.length ? [0, i[1]] : i)
                                })), i.search !== n && t.extend(e.oPreviousSearch, tt(i.search)), i.columns)
                                for (a = 0, o = i.columns.length; a < o; a++) l = i.columns[a], l.visible !== n && (r[a].bVisible = l.visible), l.search !== n && t.extend(e.aoPreSearchCols[a], tt(l.search));
                            Ft(e, "aoStateLoaded", "stateLoaded", [e, i])
                        }
                    }
                    s()
                };
            if (e.oFeatures.bStateSave) {
                var l = e.fnStateLoadCallback.call(e.oInstance, e, i);
                l !== n && i(l)
            } else s()
        }

        function It(e) {
            var i = Yt.settings,
                e = t.inArray(e, se(i, "nTable"));
            return -1 !== e ? i[e] : null
        }

        function At(t, i, n, s) {
            if (n = "DataTables warning: " + (t ? "table id=" + t.sTableId + " - " : "") + n, s && (n += ". For more information about this error, please see http://datatables.net/tn/" + s), i) e.console && console.log && console.log(n);
            else if (i = Yt.ext, i = i.sErrMode || i.errMode, t && Ft(t, null, "error", [t, s, n]), "alert" == i) alert(n);
            else {
                if ("throw" == i) throw Error(n);
                "function" == typeof i && i(t, s, n)
            }
        }

        function Et(e, i, s, a) {
            t.isArray(s) ? t.each(s, function(n, s) {
                t.isArray(s) ? Et(e, i, s[0], s[1]) : Et(e, i, s)
            }) : (a === n && (a = s), i[s] !== n && (e[a] = i[s]))
        }

        function Mt(e, i, n) {
            var s, a;
            for (a in i) i.hasOwnProperty(a) && (s = i[a], t.isPlainObject(s) ? (t.isPlainObject(e[a]) || (e[a] = {}), t.extend(!0, e[a], s)) : e[a] = n && "data" !== a && "aaData" !== a && t.isArray(s) ? s.slice() : s);
            return e
        }

        function Pt(e, i, n) {
            t(e).on("click.DT", i, function(t) {
                e.blur(), n(t)
            }).on("keypress.DT", i, function(t) {
                13 === t.which && (t.preventDefault(), n(t))
            }).on("selectstart.DT", function() {
                return !1
            })
        }

        function Nt(t, e, i, n) {
            i && t[e].push({
                fn: i,
                sName: n
            })
        }

        function Ft(e, i, n, s) {
            var a = [];
            return i && (a = t.map(e[i].slice().reverse(), function(t) {
                return t.fn.apply(e.oInstance, s)
            })), null !== n && (i = t.Event(n + ".dt"), t(e.nTable).trigger(i, s), a.push(i.result)), a
        }

        function jt(t) {
            var e = t._iDisplayStart,
                i = t.fnDisplayEnd(),
                n = t._iDisplayLength;
            e >= i && (e = i - n), e -= e % n, (-1 === n || 0 > e) && (e = 0), t._iDisplayStart = e
        }

        function Lt(e, i) {
            var n = e.renderer,
                s = Yt.ext.renderer[i];
            return t.isPlainObject(n) && n[i] ? s[n[i]] || s._ : "string" == typeof n ? s[n] || s._ : s._
        }

        function Ht(t) {
            return t.oFeatures.bServerSide ? "ssp" : t.ajax || t.sAjaxSource ? "ajax" : "dom"
        }

        function Ot(t, e) {
            var i = [],
                i = Pe.numbers_length,
                n = Math.floor(i / 2);
            return e <= i ? i = oe(0, e) : t <= n ? (i = oe(0, i - 2), i.push("ellipsis"), i.push(e - 1)) : (t >= e - 1 - n ? i = oe(e - (i - 2), e) : (i = oe(t - n + 2, t + n - 1), i.push("ellipsis"), i.push(e - 1)), i.splice(0, 0, "ellipsis"), i.splice(0, 0, 0)), i.DT_el = "span", i
        }

        function Rt(e) {
            t.each({
                num: function(t) {
                    return Ne(t, e)
                },
                "num-fmt": function(t) {
                    return Ne(t, e, Jt)
                },
                "html-num": function(t) {
                    return Ne(t, e, Xt)
                },
                "html-num-fmt": function(t) {
                    return Ne(t, e, Xt, Jt)
                }
            }, function(t, i) {
                Wt.type.order[t + e + "-pre"] = i, t.match(/^html\-/) && (Wt.type.search[t + e] = Wt.type.search.html)
            })
        }

        function zt(t) {
            return function() {
                var e = [It(this[Yt.ext.iApiIndex])].concat(Array.prototype.slice.call(arguments));
                return Yt.ext.internal[t].apply(this, e)
            }
        }
        var Wt, Bt, qt, Ut, Yt = function(e) {
                this.$ = function(t, e) {
                    return this.api(!0).$(t, e)
                }, this._ = function(t, e) {
                    return this.api(!0).rows(t, e).data()
                }, this.api = function(t) {
                    return new Bt(t ? It(this[Wt.iApiIndex]) : this)
                }, this.fnAddData = function(e, i) {
                    var s = this.api(!0),
                        a = t.isArray(e) && (t.isArray(e[0]) || t.isPlainObject(e[0])) ? s.rows.add(e) : s.row.add(e);
                    return (i === n || i) && s.draw(), a.flatten().toArray()
                }, this.fnAdjustColumnSizing = function(t) {
                    var e = this.api(!0).columns.adjust(),
                        i = e.settings()[0],
                        s = i.oScroll;
                    t === n || t ? e.draw(!1) : ("" !== s.sX || "" !== s.sY) && pt(i)
                }, this.fnClearTable = function(t) {
                    var e = this.api(!0).clear();
                    (t === n || t) && e.draw()
                }, this.fnClose = function(t) {
                    this.api(!0).row(t).child.hide()
                }, this.fnDeleteRow = function(t, e, i) {
                    var s = this.api(!0),
                        t = s.rows(t),
                        a = t.settings()[0],
                        o = a.aoData[t[0][0]];
                    return t.remove(), e && e.call(this, a, o), (i === n || i) && s.draw(), o
                }, this.fnDestroy = function(t) {
                    this.api(!0).destroy(t)
                }, this.fnDraw = function(t) {
                    this.api(!0).draw(t)
                }, this.fnFilter = function(t, e, i, s, a, o) {
                    a = this.api(!0), null === e || e === n ? a.search(t, i, s, o) : a.column(e).search(t, i, s, o), a.draw()
                }, this.fnGetData = function(t, e) {
                    var i = this.api(!0);
                    if (t !== n) {
                        var s = t.nodeName ? t.nodeName.toLowerCase() : "";
                        return e !== n || "td" == s || "th" == s ? i.cell(t, e).data() : i.row(t).data() || null
                    }
                    return i.data().toArray()
                }, this.fnGetNodes = function(t) {
                    var e = this.api(!0);
                    return t !== n ? e.row(t).node() : e.rows().nodes().flatten().toArray()
                }, this.fnGetPosition = function(t) {
                    var e = this.api(!0),
                        i = t.nodeName.toUpperCase();
                    return "TR" == i ? e.row(t).index() : "TD" == i || "TH" == i ? (t = e.cell(t).index(), [t.row, t.columnVisible, t.column]) : null
                }, this.fnIsOpen = function(t) {
                    return this.api(!0).row(t).child.isShown()
                }, this.fnOpen = function(t, e, i) {
                    return this.api(!0).row(t).child(e, i).show().child()[0]
                }, this.fnPageChange = function(t, e) {
                    var i = this.api(!0).page(t);
                    (e === n || e) && i.draw(!1)
                }, this.fnSetColumnVis = function(t, e, i) {
                    t = this.api(!0).column(t).visible(e), (i === n || i) && t.columns.adjust().draw()
                }, this.fnSettings = function() {
                    return It(this[Wt.iApiIndex])
                }, this.fnSort = function(t) {
                    this.api(!0).order(t).draw()
                }, this.fnSortListener = function(t, e, i) {
                    this.api(!0).order.listener(t, e, i)
                }, this.fnUpdate = function(t, e, i, s, a) {
                    var o = this.api(!0);
                    return i === n || null === i ? o.row(e).data(t) : o.cell(e, i).data(t), (a === n || a) && o.columns.adjust(), (s === n || s) && o.draw(), 0
                }, this.fnVersionCheck = Wt.fnVersionCheck;
                var i = this,
                    s = e === n,
                    u = this.length;
                s && (e = {}), this.oApi = this.internal = Wt.internal;
                for (var p in Yt.ext.internal) p && (this[p] = zt(p));
                return this.each(function() {
                    var p, f = {},
                        m = 1 < u ? Mt(f, e, !0) : e,
                        g = 0,
                        f = this.getAttribute("id"),
                        _ = !1,
                        v = Yt.defaults,
                        x = t(this);
                    if ("table" != this.nodeName.toLowerCase()) At(null, 0, "Non-table node initialisation (" + this.nodeName + ")", 2);
                    else {
                        r(v), l(v.column), a(v, v, !0), a(v.column, v.column, !0), a(v, t.extend(m, x.data()));
                        var $ = Yt.settings,
                            g = 0;
                        for (p = $.length; g < p; g++) {
                            var k = $[g];
                            if (k.nTable == this || k.nTHead.parentNode == this || k.nTFoot && k.nTFoot.parentNode == this) {
                                var D = m.bRetrieve !== n ? m.bRetrieve : v.bRetrieve;
                                if (s || D) return k.oInstance;
                                if (m.bDestroy !== n ? m.bDestroy : v.bDestroy) {
                                    k.oInstance.fnDestroy();
                                    break
                                }
                                return void At(k, 0, "Cannot reinitialise DataTable", 3)
                            }
                            if (k.sTableId == this.id) {
                                $.splice(g, 1);
                                break
                            }
                        }
                        null !== f && "" !== f || (this.id = f = "DataTables_Table_" + Yt.ext._unique++);
                        var S = t.extend(!0, {}, Yt.models.oSettings, {
                            sDestroyWidth: x[0].style.width,
                            sInstance: f,
                            sTableId: f
                        });
                        S.nTable = this, S.oApi = i.internal, S.oInit = m, $.push(S), S.oInstance = 1 === i.length ? i : x.dataTable(), r(m), m.oLanguage && o(m.oLanguage), m.aLengthMenu && !m.iDisplayLength && (m.iDisplayLength = t.isArray(m.aLengthMenu[0]) ? m.aLengthMenu[0][0] : m.aLengthMenu[0]), m = Mt(t.extend(!0, {}, v), m), Et(S.oFeatures, m, "bPaginate bLengthChange bFilter bSort bSortMulti bInfo bProcessing bAutoWidth bSortClasses bServerSide bDeferRender".split(" ")), Et(S, m, ["asStripeClasses", "ajax", "fnServerData", "fnFormatNumber", "sServerMethod", "aaSorting", "aaSortingFixed", "aLengthMenu", "sPaginationType", "sAjaxSource", "sAjaxDataProp", "iStateDuration", "sDom", "bSortCellsTop", "iTabIndex", "fnStateLoadCallback", "fnStateSaveCallback", "renderer", "searchDelay", "rowId", ["iCookieDuration", "iStateDuration"],
                            ["oSearch", "oPreviousSearch"],
                            ["aoSearchCols", "aoPreSearchCols"],
                            ["iDisplayLength", "_iDisplayLength"],
                            ["bJQueryUI", "bJUI"]
                        ]), Et(S.oScroll, m, [
                            ["sScrollX", "sX"],
                            ["sScrollXInner", "sXInner"],
                            ["sScrollY", "sY"],
                            ["bScrollCollapse", "bCollapse"]
                        ]), Et(S.oLanguage, m, "fnInfoCallback"), Nt(S, "aoDrawCallback", m.fnDrawCallback, "user"), Nt(S, "aoServerParams", m.fnServerParams, "user"), Nt(S, "aoStateSaveParams", m.fnStateSaveParams, "user"), Nt(S, "aoStateLoadParams", m.fnStateLoadParams, "user"), Nt(S, "aoStateLoaded", m.fnStateLoaded, "user"), Nt(S, "aoRowCallback", m.fnRowCallback, "user"), Nt(S, "aoRowCreatedCallback", m.fnCreatedRow, "user"), Nt(S, "aoHeaderCallback", m.fnHeaderCallback, "user"), Nt(S, "aoFooterCallback", m.fnFooterCallback, "user"), Nt(S, "aoInitComplete", m.fnInitComplete, "user"), Nt(S, "aoPreDrawCallback", m.fnPreDrawCallback, "user"), S.rowIdFn = C(m.rowId), c(S);
                        var T = S.oClasses;
                        m.bJQueryUI ? (t.extend(T, Yt.ext.oJUIClasses, m.oClasses), m.sDom === v.sDom && "lfrtip" === v.sDom && (S.sDom = '<"H"lfr>t<"F"ip>'), S.renderer ? t.isPlainObject(S.renderer) && !S.renderer.header && (S.renderer.header = "jqueryui") : S.renderer = "jqueryui") : t.extend(T, Yt.ext.classes, m.oClasses), x.addClass(T.sTable), S.iInitDisplayStart === n && (S.iInitDisplayStart = m.iDisplayStart, S._iDisplayStart = m.iDisplayStart), null !== m.iDeferLoading && (S.bDeferLoading = !0, f = t.isArray(m.iDeferLoading), S._iRecordsDisplay = f ? m.iDeferLoading[0] : m.iDeferLoading, S._iRecordsTotal = f ? m.iDeferLoading[1] : m.iDeferLoading);
                        var I = S.oLanguage;
                        t.extend(!0, I, m.oLanguage), I.sUrl && (t.ajax({
                            dataType: "json",
                            url: I.sUrl,
                            success: function(e) {
                                o(e), a(v.oLanguage, e), t.extend(!0, I, e), st(S)
                            },
                            error: function() {
                                st(S)
                            }
                        }), _ = !0), null === m.asStripeClasses && (S.asStripeClasses = [T.sStripeOdd, T.sStripeEven]);
                        var f = S.asStripeClasses,
                            A = x.children("tbody").find("tr").eq(0);
                        if (-1 !== t.inArray(!0, t.map(f, function(t) {
                                return A.hasClass(t)
                            })) && (t("tbody tr", this).removeClass(f.join(" ")), S.asDestroyStripes = f.slice()), f = [], $ = this.getElementsByTagName("thead"), 0 !== $.length && (O(S.aoHeader, $[0]), f = R(S)), null === m.aoColumns)
                            for ($ = [], g = 0, p = f.length; g < p; g++) $.push(null);
                        else $ = m.aoColumns;
                        for (g = 0, p = $.length; g < p; g++) d(S, f ? f[g] : null);
                        if (b(S, m.aoColumnDefs, $, function(t, e) {
                                h(S, t, e)
                            }), A.length) {
                            var E = function(t, e) {
                                return null !== t.getAttribute("data-" + e) ? e : null
                            };
                            t(A[0]).children("th, td").each(function(t, e) {
                                var i = S.aoColumns[t];
                                if (i.mData === t) {
                                    var s = E(e, "sort") || E(e, "order"),
                                        a = E(e, "filter") || E(e, "search");
                                    null === s && null === a || (i.mData = {
                                        _: t + ".display",
                                        sort: null !== s ? t + ".@data-" + s : n,
                                        type: null !== s ? t + ".@data-" + s : n,
                                        filter: null !== a ? t + ".@data-" + a : n
                                    }, h(S, t))
                                }
                            })
                        }
                        var M = S.oFeatures,
                            f = function() {
                                if (m.aaSorting === n) {
                                    var e = S.aaSorting;
                                    for (g = 0, p = e.length; g < p; g++) e[g][1] = S.aoColumns[g].asSorting[0]
                                }
                                Ct(S), M.bSort && Nt(S, "aoDrawCallback", function() {
                                    if (S.bSorted) {
                                        var e = yt(S),
                                            i = {};
                                        t.each(e, function(t, e) {
                                            i[e.src] = e.dir
                                        }), Ft(S, null, "order", [S, e, i]), xt(S)
                                    }
                                }), Nt(S, "aoDrawCallback", function() {
                                    (S.bSorted || "ssp" === Ht(S) || M.bDeferRender) && Ct(S)
                                }, "sc");
                                var e = x.children("caption").each(function() {
                                        this._captionSide = t(this).css("caption-side")
                                    }),
                                    i = x.children("thead");
                                if (0 === i.length && (i = t("<thead/>").appendTo(x)), S.nTHead = i[0], i = x.children("tbody"), 0 === i.length && (i = t("<tbody/>").appendTo(x)), S.nTBody = i[0], i = x.children("tfoot"), 0 === i.length && e.length > 0 && ("" !== S.oScroll.sX || "" !== S.oScroll.sY) && (i = t("<tfoot/>").appendTo(x)), 0 === i.length || 0 === i.children().length ? x.addClass(T.sNoFooter) : i.length > 0 && (S.nTFoot = i[0], O(S.aoFooter, S.nTFoot)), m.aaData)
                                    for (g = 0; g < m.aaData.length; g++) y(S, m.aaData[g]);
                                else(S.bDeferLoading || "dom" == Ht(S)) && w(S, t(S.nTBody).children("tr"));
                                S.aiDisplay = S.aiDisplayMaster.slice(), S.bInitialised = !0, !1 === _ && st(S)
                            };
                        m.bStateSave ? (M.bStateSave = !0, Nt(S, "aoDrawCallback", St, "state_save"), Tt(S, m, f)) : f()
                    }
                }), i = null, this
            },
            Vt = {},
            Kt = /[\r\n]/g,
            Xt = /<.*?>/g,
            Qt = /^\d{2,4}[\.\/\-]\d{1,2}[\.\/\-]\d{1,2}([T ]{1}\d{1,2}[:\.]\d{2}([\.:]\d{2})?)?$/,
            Gt = RegExp("(\\/|\\.|\\*|\\+|\\?|\\||\\(|\\)|\\[|\\]|\\{|\\}|\\\\|\\$|\\^|\\-)", "g"),
            Jt = /[',$\xa3\u20ac\xa5%\u2009\u202F\u20BD\u20a9\u20BArfk]/gi,
            Zt = function(t) {
                return !t || !0 === t || "-" === t
            },
            te = function(t) {
                var e = parseInt(t, 10);
                return !isNaN(e) && isFinite(t) ? e : null
            },
            ee = function(t, e) {
                return Vt[e] || (Vt[e] = RegExp(he(e), "g")), "string" == typeof t && "." !== e ? t.replace(/\./g, "").replace(Vt[e], ".") : t
            },
            ie = function(t, e, i) {
                var n = "string" == typeof t;
                return !!Zt(t) || (e && n && (t = ee(t, e)), i && n && (t = t.replace(Jt, "")), !isNaN(parseFloat(t)) && isFinite(t))
            },
            ne = function(t, e, i) {
                return !!Zt(t) || (Zt(t) || "string" == typeof t ? !!ie(t.replace(Xt, ""), e, i) || null : null)
            },
            se = function(t, e, i) {
                var s = [],
                    a = 0,
                    o = t.length;
                if (i !== n)
                    for (; a < o; a++) t[a] && t[a][e] && s.push(t[a][e][i]);
                else
                    for (; a < o; a++) t[a] && s.push(t[a][e]);
                return s
            },
            ae = function(t, e, i, s) {
                var a = [],
                    o = 0,
                    r = e.length;
                if (s !== n)
                    for (; o < r; o++) t[e[o]][i] && a.push(t[e[o]][i][s]);
                else
                    for (; o < r; o++) a.push(t[e[o]][i]);
                return a
            },
            oe = function(t, e) {
                var i, s = [];
                e === n ? (e = 0, i = t) : (i = e, e = t);
                for (var a = e; a < i; a++) s.push(a);
                return s
            },
            re = function(t) {
                for (var e = [], i = 0, n = t.length; i < n; i++) t[i] && e.push(t[i]);
                return e
            },
            le = function(t) {
                var e;
                t: {
                    if (!(2 > t.length)) {
                        e = t.slice().sort();
                        for (var i = e[0], n = 1, s = e.length; n < s; n++) {
                            if (e[n] === i) {
                                e = !1;
                                break t
                            }
                            i = e[n]
                        }
                    }
                    e = !0
                }
                if (e) return t.slice();
                e = [];
                var a, s = t.length,
                    o = 0,
                    n = 0;
                t: for (; n < s; n++) {
                    for (i = t[n], a = 0; a < o; a++)
                        if (e[a] === i) continue t;
                    e.push(i), o++
                }
                return e
            };
        Yt.util = {
            throttle: function(t, e) {
                var i, s, a = e !== n ? e : 200;
                return function() {
                    var e = this,
                        o = +new Date,
                        r = arguments;
                    i && o < i + a ? (clearTimeout(s), s = setTimeout(function() {
                        i = n, t.apply(e, r)
                    }, a)) : (i = o, t.apply(e, r))
                }
            },
            escapeRegex: function(t) {
                return t.replace(Gt, "\\$1")
            }
        };
        var ce = function(t, e, i) {
                t[e] !== n && (t[i] = t[e])
            },
            ue = /\[.*?\]$/,
            de = /\(\)$/,
            he = Yt.util.escapeRegex,
            pe = t("<div>")[0],
            fe = pe.textContent !== n,
            me = /<.*?>/g,
            ge = Yt.util.throttle,
            _e = [],
            ve = Array.prototype,
            be = function(e) {
                var i, n, s = Yt.settings,
                    a = t.map(s, function(t) {
                        return t.nTable
                    });
                return e ? e.nTable && e.oApi ? [e] : e.nodeName && "table" === e.nodeName.toLowerCase() ? (i = t.inArray(e, a), -1 !== i ? [s[i]] : null) : e && "function" == typeof e.settings ? e.settings().toArray() : ("string" == typeof e ? n = t(e) : e instanceof t && (n = e), n ? n.map(function() {
                    return i = t.inArray(this, a), -1 !== i ? s[i] : null
                }).toArray() : void 0) : []
            };
        Bt = function(e, i) {
            if (!(this instanceof Bt)) return new Bt(e, i);
            var n = [],
                s = function(t) {
                    (t = be(t)) && (n = n.concat(t))
                };
            if (t.isArray(e))
                for (var a = 0, o = e.length; a < o; a++) s(e[a]);
            else s(e);
            this.context = le(n), i && t.merge(this, i), this.selector = {
                rows: null,
                cols: null,
                opts: null
            }, Bt.extend(this, this, _e)
        }, Yt.Api = Bt, t.extend(Bt.prototype, {
            any: function() {
                return 0 !== this.count()
            },
            concat: ve.concat,
            context: [],
            count: function() {
                return this.flatten().length
            },
            each: function(t) {
                for (var e = 0, i = this.length; e < i; e++) t.call(this, this[e], e, this);
                return this
            },
            eq: function(t) {
                var e = this.context;
                return e.length > t ? new Bt(e[t], this[t]) : null
            },
            filter: function(t) {
                var e = [];
                if (ve.filter) e = ve.filter.call(this, t, this);
                else
                    for (var i = 0, n = this.length; i < n; i++) t.call(this, this[i], i, this) && e.push(this[i]);
                return new Bt(this.context, e)
            },
            flatten: function() {
                var t = [];
                return new Bt(this.context, t.concat.apply(t, this.toArray()))
            },
            join: ve.join,
            indexOf: ve.indexOf || function(t, e) {
                for (var i = e || 0, n = this.length; i < n; i++)
                    if (this[i] === t) return i;
                return -1
            },
            iterator: function(t, e, i, s) {
                var a, o, r, l, c, u, d, h = [],
                    p = this.context,
                    f = this.selector;
                for ("string" == typeof t && (s = i, i = e, e = t, t = !1), o = 0, r = p.length; o < r; o++) {
                    var m = new Bt(p[o]);
                    if ("table" === e)(a = i.call(m, p[o], o)) !== n && h.push(a);
                    else if ("columns" === e || "rows" === e)(a = i.call(m, p[o], this[o], o)) !== n && h.push(a);
                    else if ("column" === e || "column-rows" === e || "row" === e || "cell" === e)
                        for (d = this[o], "column-rows" === e && (u = ke(p[o], f.opts)), l = 0, c = d.length; l < c; l++) a = d[l], (a = "cell" === e ? i.call(m, p[o], a.row, a.column, o, l) : i.call(m, p[o], a, o, l, u)) !== n && h.push(a)
                }
                return h.length || s ? (t = new Bt(p, t ? h.concat.apply([], h) : h), e = t.selector, e.rows = f.rows, e.cols = f.cols, e.opts = f.opts, t) : this
            },
            lastIndexOf: ve.lastIndexOf || function() {
                return this.indexOf.apply(this.toArray.reverse(), arguments)
            },
            length: 0,
            map: function(t) {
                var e = [];
                if (ve.map) e = ve.map.call(this, t, this);
                else
                    for (var i = 0, n = this.length; i < n; i++) e.push(t.call(this, this[i], i));
                return new Bt(this.context, e)
            },
            pluck: function(t) {
                return this.map(function(e) {
                    return e[t]
                })
            },
            pop: ve.pop,
            push: ve.push,
            reduce: ve.reduce || function(t, e) {
                return u(this, t, e, 0, this.length, 1)
            },
            reduceRight: ve.reduceRight || function(t, e) {
                return u(this, t, e, this.length - 1, -1, -1)
            },
            reverse: ve.reverse,
            selector: null,
            shift: ve.shift,
            slice: function() {
                return new Bt(this.context, this)
            },
            sort: ve.sort,
            splice: ve.splice,
            toArray: function() {
                return ve.slice.call(this)
            },
            to$: function() {
                return t(this)
            },
            toJQuery: function() {
                return t(this)
            },
            unique: function() {
                return new Bt(this.context, le(this))
            },
            unshift: ve.unshift
        }), Bt.extend = function(e, i, n) {
            if (n.length && i && (i instanceof Bt || i.__dt_wrapper)) {
                var s, a, o, r = function(t, e, i) {
                    return function() {
                        var n = e.apply(t, arguments);
                        return Bt.extend(n, n, i.methodExt), n
                    }
                };
                for (s = 0, a = n.length; s < a; s++) o = n[s], i[o.name] = "function" == typeof o.val ? r(e, o.val, o) : t.isPlainObject(o.val) ? {} : o.val, i[o.name].__dt_wrapper = !0, Bt.extend(e, i[o.name], o.propExt)
            }
        }, Bt.register = qt = function(e, i) {
            if (t.isArray(e))
                for (var n = 0, s = e.length; n < s; n++) Bt.register(e[n], i);
            else
                for (var a, o, r = e.split("."), l = _e, n = 0, s = r.length; n < s; n++) {
                    a = (o = -1 !== r[n].indexOf("()")) ? r[n].replace("()", "") : r[n];
                    var c;
                    t: {
                        c = 0;
                        for (var u = l.length; c < u; c++)
                            if (l[c].name === a) {
                                c = l[c];
                                break t
                            }
                        c = null
                    }
                    c || (c = {
                        name: a,
                        val: {},
                        methodExt: [],
                        propExt: []
                    }, l.push(c)), n === s - 1 ? c.val = i : l = o ? c.methodExt : c.propExt
                }
        }, Bt.registerPlural = Ut = function(e, i, s) {
            Bt.register(e, s), Bt.register(i, function() {
                var e = s.apply(this, arguments);
                return e === this ? this : e instanceof Bt ? e.length ? t.isArray(e[0]) ? new Bt(e.context, e[0]) : e[0] : n : e
            })
        }, qt("tables()", function(e) {
            var i;
            if (e) {
                i = Bt;
                var n = this.context;
                if ("number" == typeof e) e = [n[e]];
                else var s = t.map(n, function(t) {
                        return t.nTable
                    }),
                    e = t(s).filter(e).map(function() {
                        var e = t.inArray(this, s);
                        return n[e]
                    }).toArray();
                i = new i(e)
            } else i = this;
            return i
        }), qt("table()", function(t) {
            var t = this.tables(t),
                e = t.context;
            return e.length ? new Bt(e[0]) : t
        }), Ut("tables().nodes()", "table().node()", function() {
            return this.iterator("table", function(t) {
                return t.nTable
            }, 1)
        }), Ut("tables().body()", "table().body()", function() {
            return this.iterator("table", function(t) {
                return t.nTBody
            }, 1)
        }), Ut("tables().header()", "table().header()", function() {
            return this.iterator("table", function(t) {
                return t.nTHead
            }, 1)
        }), Ut("tables().footer()", "table().footer()", function() {
            return this.iterator("table", function(t) {
                return t.nTFoot
            }, 1)
        }), Ut("tables().containers()", "table().container()", function() {
            return this.iterator("table", function(t) {
                return t.nTableWrapper
            }, 1)
        }), qt("draw()", function(t) {
            return this.iterator("table", function(e) {
                "page" === t ? j(e) : ("string" == typeof t && (t = "full-hold" !== t), L(e, !1 === t))
            })
        }), qt("page()", function(t) {
            return t === n ? this.page.info().page : this.iterator("table", function(e) {
                ct(e, t)
            })
        }), qt("page.info()", function() {
            if (0 === this.context.length) return n;
            var t = this.context[0],
                e = t._iDisplayStart,
                i = t.oFeatures.bPaginate ? t._iDisplayLength : -1,
                s = t.fnRecordsDisplay(),
                a = -1 === i;
            return {
                page: a ? 0 : Math.floor(e / i),
                pages: a ? 1 : Math.ceil(s / i),
                start: e,
                end: t.fnDisplayEnd(),
                length: i,
                recordsTotal: t.fnRecordsTotal(),
                recordsDisplay: s,
                serverSide: "ssp" === Ht(t)
            }
        }), qt("page.len()", function(t) {
            return t === n ? 0 !== this.context.length ? this.context[0]._iDisplayLength : n : this.iterator("table", function(e) {
                ot(e, t)
            })
        });
        var ye = function(t, e, i) {
            if (i) {
                var n = new Bt(t);
                n.one("draw", function() {
                    i(n.ajax.json())
                })
            }
            if ("ssp" == Ht(t)) L(t, e);
            else {
                dt(t, !0);
                var s = t.jqXHR;
                s && 4 !== s.readyState && s.abort(), z(t, [], function(i) {
                    T(t);
                    for (var i = U(t, i), n = 0, s = i.length; n < s; n++) y(t, i[n]);
                    L(t, e), dt(t, !1)
                })
            }
        };
        qt("ajax.json()", function() {
            var t = this.context;
            if (0 < t.length) return t[0].json
        }), qt("ajax.params()", function() {
            var t = this.context;
            if (0 < t.length) return t[0].oAjaxData
        }), qt("ajax.reload()", function(t, e) {
            return this.iterator("table", function(i) {
                ye(i, !1 === e, t)
            })
        }), qt("ajax.url()", function(e) {
            var i = this.context;
            return e === n ? 0 === i.length ? n : (i = i[0], i.ajax ? t.isPlainObject(i.ajax) ? i.ajax.url : i.ajax : i.sAjaxSource) : this.iterator("table", function(i) {
                t.isPlainObject(i.ajax) ? i.ajax.url = e : i.ajax = e
            })
        }), qt("ajax.url().load()", function(t, e) {
            return this.iterator("table", function(i) {
                ye(i, !1 === e, t)
            })
        });
        var we = function(e, i, s, a, o) {
                var r, l, c, u, d, h, p = [];
                for (c = typeof i, i && "string" !== c && "function" !== c && i.length !== n || (i = [i]), c = 0, u = i.length; c < u; c++)
                    for (l = i[c] && i[c].split && !i[c].match(/[\[\(:]/) ? i[c].split(",") : [i[c]], d = 0, h = l.length; d < h; d++)(r = s("string" == typeof l[d] ? t.trim(l[d]) : l[d])) && r.length && (p = p.concat(r));
                if (e = Wt.selector[e], e.length)
                    for (c = 0, u = e.length; c < u; c++) p = e[c](a, o, p);
                return le(p)
            },
            xe = function(e) {
                return e || (e = {}), e.filter && e.search === n && (e.search = e.filter), t.extend({
                    search: "none",
                    order: "current",
                    page: "all"
                }, e)
            },
            $e = function(t) {
                for (var e = 0, i = t.length; e < i; e++)
                    if (0 < t[e].length) return t[0] = t[e], t[0].length = 1, t.length = 1, t.context = [t.context[e]], t;
                return t.length = 0, t
            },
            ke = function(e, i) {
                var n, s, a, o = [],
                    r = e.aiDisplay;
                n = e.aiDisplayMaster;
                var l = i.search;
                if (s = i.order, a = i.page, "ssp" == Ht(e)) return "removed" === l ? [] : oe(0, n.length);
                if ("current" == a)
                    for (n = e._iDisplayStart, s = e.fnDisplayEnd(); n < s; n++) o.push(r[n]);
                else if ("current" == s || "applied" == s) o = "none" == l ? n.slice() : "applied" == l ? r.slice() : t.map(n, function(e) {
                    return -1 === t.inArray(e, r) ? e : null
                });
                else if ("index" == s || "original" == s)
                    for (n = 0, s = e.aoData.length; n < s; n++) "none" == l ? o.push(n) : (-1 === (a = t.inArray(n, r)) && "removed" == l || 0 <= a && "applied" == l) && o.push(n);
                return o
            };
        qt("rows()", function(e, i) {
            e === n ? e = "" : t.isPlainObject(e) && (i = e, e = "");
            var i = xe(i),
                s = this.iterator("table", function(s) {
                    var a, o = i;
                    return we("row", e, function(e) {
                        var i = te(e);
                        if (null !== i && !o) return [i];
                        if (a || (a = ke(s, o)), null !== i && -1 !== t.inArray(i, a)) return [i];
                        if (null === e || e === n || "" === e) return a;
                        if ("function" == typeof e) return t.map(a, function(t) {
                            var i = s.aoData[t];
                            return e(t, i._aData, i.nTr) ? t : null
                        });
                        if (i = re(ae(s.aoData, a, "nTr")), e.nodeName) return e._DT_RowIndex !== n ? [e._DT_RowIndex] : e._DT_CellIndex ? [e._DT_CellIndex.row] : (i = t(e).closest("*[data-dt-row]"), i.length ? [i.data("dt-row")] : []);
                        if ("string" == typeof e && "#" === e.charAt(0)) {
                            var r = s.aIds[e.replace(/^#/, "")];
                            if (r !== n) return [r.idx]
                        }
                        return t(i).filter(e).map(function() {
                            return this._DT_RowIndex
                        }).toArray()
                    }, s, o)
                }, 1);
            return s.selector.rows = e, s.selector.opts = i, s
        }), qt("rows().nodes()", function() {
            return this.iterator("row", function(t, e) {
                return t.aoData[e].nTr || n
            }, 1)
        }), qt("rows().data()", function() {
            return this.iterator(!0, "rows", function(t, e) {
                return ae(t.aoData, e, "_aData")
            }, 1)
        }), Ut("rows().cache()", "row().cache()", function(t) {
            return this.iterator("row", function(e, i) {
                var n = e.aoData[i];
                return "search" === t ? n._aFilterData : n._aSortData
            }, 1)
        }), Ut("rows().invalidate()", "row().invalidate()", function(t) {
            return this.iterator("row", function(e, i) {
                A(e, i, t)
            })
        }), Ut("rows().indexes()", "row().index()", function() {
            return this.iterator("row", function(t, e) {
                return e
            }, 1)
        }), Ut("rows().ids()", "row().id()", function(t) {
            for (var e = [], i = this.context, n = 0, s = i.length; n < s; n++)
                for (var a = 0, o = this[n].length; a < o; a++) {
                    var r = i[n].rowIdFn(i[n].aoData[this[n][a]]._aData);
                    e.push((!0 === t ? "#" : "") + r)
                }
            return new Bt(i, e)
        }), Ut("rows().remove()", "row().remove()", function() {
            var t = this;
            return this.iterator("row", function(e, i, s) {
                var a, o, r, l, c, u = e.aoData,
                    d = u[i];
                for (u.splice(i, 1), a = 0, o = u.length; a < o; a++)
                    if (r = u[a], c = r.anCells, null !== r.nTr && (r.nTr._DT_RowIndex = a), null !== c)
                        for (r = 0, l = c.length; r < l; r++) c[r]._DT_CellIndex.row = a;
                I(e.aiDisplayMaster, i), I(e.aiDisplay, i), I(t[s], i, !1), jt(e), (i = e.rowIdFn(d._aData)) !== n && delete e.aIds[i]
            }), this.iterator("table", function(t) {
                for (var e = 0, i = t.aoData.length; e < i; e++) t.aoData[e].idx = e
            }), this
        }), qt("rows.add()", function(e) {
            var i = this.iterator("table", function(t) {
                    var i, n, s, a = [];
                    for (n = 0, s = e.length; n < s; n++) i = e[n], i.nodeName && "TR" === i.nodeName.toUpperCase() ? a.push(w(t, i)[0]) : a.push(y(t, i));
                    return a
                }, 1),
                n = this.rows(-1);
            return n.pop(), t.merge(n, i), n
        }), qt("row()", function(t, e) {
            return $e(this.rows(t, e))
        }), qt("row().data()", function(t) {
            var e = this.context;
            return t === n ? e.length && this.length ? e[0].aoData[this[0]]._aData : n : (e[0].aoData[this[0]]._aData = t, A(e[0], this[0], "data"), this)
        }), qt("row().node()", function() {
            var t = this.context;
            return t.length && this.length ? t[0].aoData[this[0]].nTr || null : null
        }), qt("row.add()", function(e) {
            e instanceof t && e.length && (e = e[0]);
            var i = this.iterator("table", function(t) {
                return e.nodeName && "TR" === e.nodeName.toUpperCase() ? w(t, e)[0] : y(t, e)
            });
            return this.row(i[0])
        });
        var Ce = function(t, e) {
                var i = t.context;
                i.length && (i = i[0].aoData[e !== n ? e : t[0]]) && i._details && (i._details.remove(), i._detailsShow = n, i._details = n)
            },
            De = function(t, e) {
                var i = t.context;
                if (i.length && t.length) {
                    var n = i[0].aoData[t[0]];
                    if (n._details) {
                        (n._detailsShow = e) ? n._details.insertAfter(n.nTr): n._details.detach();
                        var s = i[0],
                            a = new Bt(s),
                            o = s.aoData;
                        a.off("draw.dt.DT_details column-visibility.dt.DT_details destroy.dt.DT_details"), 0 < se(o, "_details").length && (a.on("draw.dt.DT_details", function(t, e) {
                            s === e && a.rows({
                                page: "current"
                            }).eq(0).each(function(t) {
                                t = o[t], t._detailsShow && t._details.insertAfter(t.nTr)
                            })
                        }), a.on("column-visibility.dt.DT_details", function(t, e) {
                            if (s === e)
                                for (var i, n = g(e), a = 0, r = o.length; a < r; a++) i = o[a], i._details && i._details.children("td[colspan]").attr("colspan", n)
                        }), a.on("destroy.dt.DT_details", function(t, e) {
                            if (s === e)
                                for (var i = 0, n = o.length; i < n; i++) o[i]._details && Ce(a, i)
                        }))
                    }
                }
            };
        qt("row().child()", function(e, i) {
            var s = this.context;
            if (e === n) return s.length && this.length ? s[0].aoData[this[0]]._details : n;
            if (!0 === e) this.child.show();
            else if (!1 === e) Ce(this);
            else if (s.length && this.length) {
                var a = s[0],
                    s = s[0].aoData[this[0]],
                    o = [],
                    r = function(e, i) {
                        if (t.isArray(e) || e instanceof t)
                            for (var n = 0, s = e.length; n < s; n++) r(e[n], i);
                        else e.nodeName && "tr" === e.nodeName.toLowerCase() ? o.push(e) : (n = t("<tr><td/></tr>").addClass(i), t("td", n).addClass(i).html(e)[0].colSpan = g(a), o.push(n[0]))
                    };
                r(e, i), s._details && s._details.detach(), s._details = t(o), s._detailsShow && s._details.insertAfter(s.nTr)
            }
            return this
        }), qt(["row().child.show()", "row().child().show()"], function() {
            return De(this, !0), this
        }), qt(["row().child.hide()", "row().child().hide()"], function() {
            return De(this, !1), this
        }), qt(["row().child.remove()", "row().child().remove()"], function() {
            return Ce(this), this
        }), qt("row().child.isShown()", function() {
            var t = this.context;
            return !(!t.length || !this.length) && (t[0].aoData[this[0]]._detailsShow || !1)
        });
        var Se = /^([^:]+):(name|visIdx|visible)$/,
            Te = function(t, e, i, n, s) {
                for (var i = [], n = 0, a = s.length; n < a; n++) i.push(x(t, s[n], e));
                return i
            };
        qt("columns()", function(e, i) {
            e === n ? e = "" : t.isPlainObject(e) && (i = e, e = "");
            var i = xe(i),
                s = this.iterator("table", function(n) {
                    var s = e,
                        a = i,
                        o = n.aoColumns,
                        r = se(o, "sName"),
                        l = se(o, "nTh");
                    return we("column", s, function(e) {
                        var i = te(e);
                        if ("" === e) return oe(o.length);
                        if (null !== i) return [i >= 0 ? i : o.length + i];
                        if ("function" == typeof e) {
                            var s = ke(n, a);
                            return t.map(o, function(t, i) {
                                return e(i, Te(n, i, 0, 0, s), l[i]) ? i : null
                            })
                        }
                        var c = "string" == typeof e ? e.match(Se) : "";
                        if (c) switch (c[2]) {
                            case "visIdx":
                            case "visible":
                                if ((i = parseInt(c[1], 10)) < 0) {
                                    var u = t.map(o, function(t, e) {
                                        return t.bVisible ? e : null
                                    });
                                    return [u[u.length + i]]
                                }
                                return [f(n, i)];
                            case "name":
                                return t.map(r, function(t, e) {
                                    return t === c[1] ? e : null
                                });
                            default:
                                return []
                        }
                        return e.nodeName && e._DT_CellIndex ? [e._DT_CellIndex.column] : (i = t(l).filter(e).map(function() {
                            return t.inArray(this, l)
                        }).toArray(), i.length || !e.nodeName ? i : (i = t(e).closest("*[data-dt-column]"), i.length ? [i.data("dt-column")] : []))
                    }, n, a)
                }, 1);
            return s.selector.cols = e, s.selector.opts = i, s
        }), Ut("columns().header()", "column().header()", function() {
            return this.iterator("column", function(t, e) {
                return t.aoColumns[e].nTh
            }, 1)
        }), Ut("columns().footer()", "column().footer()", function() {
            return this.iterator("column", function(t, e) {
                return t.aoColumns[e].nTf
            }, 1)
        }), Ut("columns().data()", "column().data()", function() {
            return this.iterator("column-rows", Te, 1)
        }), Ut("columns().dataSrc()", "column().dataSrc()", function() {
            return this.iterator("column", function(t, e) {
                return t.aoColumns[e].mData
            }, 1)
        }), Ut("columns().cache()", "column().cache()", function(t) {
            return this.iterator("column-rows", function(e, i, n, s, a) {
                return ae(e.aoData, a, "search" === t ? "_aFilterData" : "_aSortData", i)
            }, 1)
        }), Ut("columns().nodes()", "column().nodes()", function() {
            return this.iterator("column-rows", function(t, e, i, n, s) {
                return ae(t.aoData, s, "anCells", e)
            }, 1)
        }), Ut("columns().visible()", "column().visible()", function(e, i) {
            var s = this.iterator("column", function(i, s) {
                if (e === n) return i.aoColumns[s].bVisible;
                var a, o, r, l = i.aoColumns,
                    c = l[s],
                    u = i.aoData;
                if (e !== n && c.bVisible !== e) {
                    if (e) {
                        var d = t.inArray(!0, se(l, "bVisible"), s + 1);
                        for (a = 0, o = u.length; a < o; a++) r = u[a].nTr, l = u[a].anCells, r && r.insertBefore(l[s], l[d] || null)
                    } else t(se(i.aoData, "anCells", s)).detach();
                    c.bVisible = e, F(i, i.aoHeader), F(i, i.aoFooter), St(i)
                }
            });
            return e !== n && (this.iterator("column", function(t, n) {
                Ft(t, null, "column-visibility", [t, n, e, i])
            }), (i === n || i) && this.columns.adjust()), s
        }), Ut("columns().indexes()", "column().index()", function(t) {
            return this.iterator("column", function(e, i) {
                return "visible" === t ? m(e, i) : i
            }, 1)
        }), qt("columns.adjust()", function() {
            return this.iterator("table", function(t) {
                p(t)
            }, 1)
        }), qt("column.index()", function(t, e) {
            if (0 !== this.context.length) {
                var i = this.context[0];
                if ("fromVisible" === t || "toData" === t) return f(i, e);
                if ("fromData" === t || "toVisible" === t) return m(i, e)
            }
        }), qt("column()", function(t, e) {
            return $e(this.columns(t, e))
        }), qt("cells()", function(e, i, s) {
            if (t.isPlainObject(e) && (e.row === n ? (s = e, e = null) : (s = i, i = null)), t.isPlainObject(i) && (s = i, i = null), null === i || i === n) return this.iterator("table", function(i) {
                var a, o, r, l, c, u, d, h = e,
                    p = xe(s),
                    f = i.aoData,
                    m = ke(i, p),
                    g = re(ae(f, m, "anCells")),
                    _ = t([].concat.apply([], g)),
                    v = i.aoColumns.length;
                return we("cell", h, function(e) {
                    var s = "function" == typeof e;
                    if (null === e || e === n || s) {
                        for (o = [], r = 0, l = m.length; r < l; r++)
                            for (a = m[r], c = 0; c < v; c++) u = {
                                row: a,
                                column: c
                            }, s ? (d = f[a], e(u, x(i, a, c), d.anCells ? d.anCells[c] : null) && o.push(u)) : o.push(u);
                        return o
                    }
                    return t.isPlainObject(e) ? [e] : (s = _.filter(e).map(function(t, e) {
                        return {
                            row: e._DT_CellIndex.row,
                            column: e._DT_CellIndex.column
                        }
                    }).toArray(), s.length || !e.nodeName ? s : (d = t(e).closest("*[data-dt-row]"), d.length ? [{
                        row: d.data("dt-row"),
                        column: d.data("dt-column")
                    }] : []))
                }, i, p)
            });
            var a, o, r, l, c, u = this.columns(i, s),
                d = this.rows(e, s),
                h = this.iterator("table", function(t, e) {
                    for (a = [], o = 0, r = d[e].length; o < r; o++)
                        for (l = 0, c = u[e].length; l < c; l++) a.push({
                            row: d[e][o],
                            column: u[e][l]
                        });
                    return a
                }, 1);
            return t.extend(h.selector, {
                cols: i,
                rows: e,
                opts: s
            }), h
        }), Ut("cells().nodes()", "cell().node()", function() {
            return this.iterator("cell", function(t, e, i) {
                return (t = t.aoData[e]) && t.anCells ? t.anCells[i] : n
            }, 1)
        }), qt("cells().data()", function() {
            return this.iterator("cell", function(t, e, i) {
                return x(t, e, i)
            }, 1)
        }), Ut("cells().cache()", "cell().cache()", function(t) {
            return t = "search" === t ? "_aFilterData" : "_aSortData", this.iterator("cell", function(e, i, n) {
                return e.aoData[i][t][n]
            }, 1)
        }), Ut("cells().render()", "cell().render()", function(t) {
            return this.iterator("cell", function(e, i, n) {
                return x(e, i, n, t)
            }, 1)
        }), Ut("cells().indexes()", "cell().index()", function() {
            return this.iterator("cell", function(t, e, i) {
                return {
                    row: e,
                    column: i,
                    columnVisible: m(t, i)
                }
            }, 1)
        }), Ut("cells().invalidate()", "cell().invalidate()", function(t) {
            return this.iterator("cell", function(e, i, n) {
                A(e, i, t, n)
            })
        }), qt("cell()", function(t, e, i) {
            return $e(this.cells(t, e, i))
        }), qt("cell().data()", function(t) {
            var e = this.context,
                i = this[0];
            return t === n ? e.length && i.length ? x(e[0], i[0].row, i[0].column) : n : ($(e[0], i[0].row, i[0].column, t), A(e[0], i[0].row, "data", i[0].column), this)
        }), qt("order()", function(e, i) {
            var s = this.context;
            return e === n ? 0 !== s.length ? s[0].aaSorting : n : ("number" == typeof e ? e = [
                [e, i]
            ] : e.length && !t.isArray(e[0]) && (e = Array.prototype.slice.call(arguments)), this.iterator("table", function(t) {
                t.aaSorting = e.slice()
            }))
        }), qt("order.listener()", function(t, e, i) {
            return this.iterator("table", function(n) {
                kt(n, t, e, i)
            })
        }), qt("order.fixed()", function(e) {
            if (!e) {
                var i = this.context,
                    i = i.length ? i[0].aaSortingFixed : n;
                return t.isArray(i) ? {
                    pre: i
                } : i
            }
            return this.iterator("table", function(i) {
                i.aaSortingFixed = t.extend(!0, {}, e)
            })
        }), qt(["columns().order()", "column().order()"], function(e) {
            var i = this;
            return this.iterator("table", function(n, s) {
                var a = [];
                t.each(i[s], function(t, i) {
                    a.push([i, e])
                }), n.aaSorting = a
            })
        }), qt("search()", function(e, i, s, a) {
            var o = this.context;
            return e === n ? 0 !== o.length ? o[0].oPreviousSearch.sSearch : n : this.iterator("table", function(n) {
                n.oFeatures.bFilter && V(n, t.extend({}, n.oPreviousSearch, {
                    sSearch: e + "",
                    bRegex: null !== i && i,
                    bSmart: null === s || s,
                    bCaseInsensitive: null === a || a
                }), 1)
            })
        }), Ut("columns().search()", "column().search()", function(e, i, s, a) {
            return this.iterator("column", function(o, r) {
                var l = o.aoPreSearchCols;
                if (e === n) return l[r].sSearch;
                o.oFeatures.bFilter && (t.extend(l[r], {
                    sSearch: e + "",
                    bRegex: null !== i && i,
                    bSmart: null === s || s,
                    bCaseInsensitive: null === a || a
                }), V(o, o.oPreviousSearch, 1))
            })
        }), qt("state()", function() {
            return this.context.length ? this.context[0].oSavedState : null
        }), qt("state.clear()", function() {
            return this.iterator("table", function(t) {
                t.fnStateSaveCallback.call(t.oInstance, t, {})
            })
        }), qt("state.loaded()", function() {
            return this.context.length ? this.context[0].oLoadedState : null
        }), qt("state.save()", function() {
            return this.iterator("table", function(t) {
                St(t)
            })
        }), Yt.versionCheck = Yt.fnVersionCheck = function(t) {
            for (var e, i, n = Yt.version.split("."), t = t.split("."), s = 0, a = t.length; s < a; s++)
                if (e = parseInt(n[s], 10) || 0, i = parseInt(t[s], 10) || 0, e !== i) return e > i;
            return !0
        }, Yt.isDataTable = Yt.fnIsDataTable = function(e) {
            var i = t(e).get(0),
                n = !1;
            return e instanceof Yt.Api || (t.each(Yt.settings, function(e, s) {
                var a = s.nScrollHead ? t("table", s.nScrollHead)[0] : null,
                    o = s.nScrollFoot ? t("table", s.nScrollFoot)[0] : null;
                s.nTable !== i && a !== i && o !== i || (n = !0)
            }), n)
        }, Yt.tables = Yt.fnTables = function(e) {
            var i = !1;
            t.isPlainObject(e) && (i = e.api, e = e.visible);
            var n = t.map(Yt.settings, function(i) {
                if (!e || e && t(i.nTable).is(":visible")) return i.nTable
            });
            return i ? new Bt(n) : n
        }, Yt.camelToHungarian = a, qt("$()", function(e, i) {
            var n = this.rows(i).nodes(),
                n = t(n);
            return t([].concat(n.filter(e).toArray(), n.find(e).toArray()))
        }), t.each(["on", "one", "off"], function(e, i) {
            qt(i + "()", function() {
                var e = Array.prototype.slice.call(arguments);
                e[0] = t.map(e[0].split(/\s/), function(t) {
                    return t.match(/\.dt\b/) ? t : t + ".dt"
                }).join(" ");
                var n = t(this.tables().nodes());
                return n[i].apply(n, e), this
            })
        }), qt("clear()", function() {
            return this.iterator("table", function(t) {
                T(t)
            })
        }), qt("settings()", function() {
            return new Bt(this.context, this.context)
        }), qt("init()", function() {
            var t = this.context;
            return t.length ? t[0].oInit : null
        }), qt("data()", function() {
            return this.iterator("table", function(t) {
                return se(t.aoData, "_aData")
            }).flatten()
        }), qt("destroy()", function(i) {
            return i = i || !1, this.iterator("table", function(n) {
                var s, a = n.nTableWrapper.parentNode,
                    o = n.oClasses,
                    r = n.nTable,
                    l = n.nTBody,
                    c = n.nTHead,
                    u = n.nTFoot,
                    d = t(r),
                    l = t(l),
                    h = t(n.nTableWrapper),
                    p = t.map(n.aoData, function(t) {
                        return t.nTr
                    });
                n.bDestroying = !0, Ft(n, "aoDestroyCallback", "destroy", [n]), i || new Bt(n).columns().visible(!0), h.off(".DT").find(":not(tbody *)").off(".DT"), t(e).off(".DT-" + n.sInstance), r != c.parentNode && (d.children("thead").detach(), d.append(c)), u && r != u.parentNode && (d.children("tfoot").detach(), d.append(u)), n.aaSorting = [], n.aaSortingFixed = [], Ct(n), t(p).removeClass(n.asStripeClasses.join(" ")), t("th, td", c).removeClass(o.sSortable + " " + o.sSortableAsc + " " + o.sSortableDesc + " " + o.sSortableNone), n.bJUI && (t("th span." + o.sSortIcon + ", td span." + o.sSortIcon, c).detach(), t("th, td", c).each(function() {
                    var e = t("div." + o.sSortJUIWrapper, this);
                    t(this).append(e.contents()), e.detach()
                })), l.children().detach(), l.append(p), c = i ? "remove" : "detach", d[c](), h[c](), !i && a && (a.insertBefore(r, n.nTableReinsertBefore), d.css("width", n.sDestroyWidth).removeClass(o.sTable), (s = n.asDestroyStripes.length) && l.children().each(function(e) {
                    t(this).addClass(n.asDestroyStripes[e % s])
                })), -1 !== (a = t.inArray(n, Yt.settings)) && Yt.settings.splice(a, 1)
            })
        }), t.each(["column", "row", "cell"], function(t, e) {
            qt(e + "s().every()", function(t) {
                var i = this.selector.opts,
                    s = this;
                return this.iterator(e, function(a, o, r, l, c) {
                    t.call(s[e](o, "cell" === e ? r : i, "cell" === e ? i : n), o, r, l, c)
                })
            })
        }), qt("i18n()", function(e, i, s) {
            var a = this.context[0],
                e = C(e)(a.oLanguage);
            return e === n && (e = i), s !== n && t.isPlainObject(e) && (e = e[s] !== n ? e[s] : e._), e.replace("%d", s)
        }), Yt.version = "1.10.15", Yt.settings = [], Yt.models = {}, Yt.models.oSearch = {
            bCaseInsensitive: !0,
            sSearch: "",
            bRegex: !1,
            bSmart: !0
        }, Yt.models.oRow = {
            nTr: null,
            anCells: null,
            _aData: [],
            _aSortData: null,
            _aFilterData: null,
            _sFilterRow: null,
            _sRowStripe: "",
            src: null,
            idx: -1
        }, Yt.models.oColumn = {
            idx: null,
            aDataSort: null,
            asSorting: null,
            bSearchable: null,
            bSortable: null,
            bVisible: null,
            _sManualType: null,
            _bAttrSrc: !1,
            fnCreatedCell: null,
            fnGetData: null,
            fnSetData: null,
            mData: null,
            mRender: null,
            nTh: null,
            nTf: null,
            sClass: null,
            sContentPadding: null,
            sDefaultContent: null,
            sName: null,
            sSortDataType: "std",
            sSortingClass: null,
            sSortingClassJUI: null,
            sTitle: null,
            sType: null,
            sWidth: null,
            sWidthOrig: null
        }, Yt.defaults = {
            aaData: null,
            aaSorting: [
                [0, "asc"]
            ],
            aaSortingFixed: [],
            ajax: null,
            aLengthMenu: [10, 25, 50, 100],
            aoColumns: null,
            aoColumnDefs: null,
            aoSearchCols: [],
            asStripeClasses: null,
            bAutoWidth: !0,
            bDeferRender: !1,
            bDestroy: !1,
            bFilter: !0,
            bInfo: !0,
            bJQueryUI: !1,
            bLengthChange: !0,
            bPaginate: !0,
            bProcessing: !1,
            bRetrieve: !1,
            bScrollCollapse: !1,
            bServerSide: !1,
            bSort: !0,
            bSortMulti: !0,
            bSortCellsTop: !1,
            bSortClasses: !0,
            bStateSave: !1,
            fnCreatedRow: null,
            fnDrawCallback: null,
            fnFooterCallback: null,
            fnFormatNumber: function(t) {
                return t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, this.oLanguage.sThousands)
            },
            fnHeaderCallback: null,
            fnInfoCallback: null,
            fnInitComplete: null,
            fnPreDrawCallback: null,
            fnRowCallback: null,
            fnServerData: null,
            fnServerParams: null,
            fnStateLoadCallback: function(t) {
                try {
                    return JSON.parse((-1 === t.iStateDuration ? sessionStorage : localStorage).getItem("DataTables_" + t.sInstance + "_" + location.pathname))
                } catch (t) {}
            },
            fnStateLoadParams: null,
            fnStateLoaded: null,
            fnStateSaveCallback: function(t, e) {
                try {
                    (-1 === t.iStateDuration ? sessionStorage : localStorage).setItem("DataTables_" + t.sInstance + "_" + location.pathname, JSON.stringify(e))
                } catch (t) {}
            },
            fnStateSaveParams: null,
            iStateDuration: 7200,
            iDeferLoading: null,
            iDisplayLength: 10,
            iDisplayStart: 0,
            iTabIndex: 0,
            oClasses: {},
            oLanguage: {
                oAria: {
                    sSortAscending: ": activate to sort column ascending",
                    sSortDescending: ": activate to sort column descending"
                },
                oPaginate: {
                    sFirst: "First",
                    sLast: "Last",
                    sNext: "Next",
                    sPrevious: "Previous"
                },
                sEmptyTable: "No data available in table",
                sInfo: "Showing _START_ to _END_ of _TOTAL_ entries",
                sInfoEmpty: "Showing 0 to 0 of 0 entries",
                sInfoFiltered: "(filtered from _MAX_ total entries)",
                sInfoPostFix: "",
                sDecimal: "",
                sThousands: ",",
                sLengthMenu: "Show _MENU_ entries",
                sLoadingRecords: "Loading...",
                sProcessing: "Processing...",
                sSearch: "Search:",
                sSearchPlaceholder: "",
                sUrl: "",
                sZeroRecords: "No matching records found"
            },
            oSearch: t.extend({}, Yt.models.oSearch),
            sAjaxDataProp: "data",
            sAjaxSource: null,
            sDom: "lfrtip",
            searchDelay: null,
            sPaginationType: "simple_numbers",
            sScrollX: "",
            sScrollXInner: "",
            sScrollY: "",
            sServerMethod: "GET",
            renderer: null,
            rowId: "DT_RowId"
        }, s(Yt.defaults), Yt.defaults.column = {
            aDataSort: null,
            iDataSort: -1,
            asSorting: ["asc", "desc"],
            bSearchable: !0,
            bSortable: !0,
            bVisible: !0,
            fnCreatedCell: null,
            mData: null,
            mRender: null,
            sCellType: "td",
            sClass: "",
            sContentPadding: "",
            sDefaultContent: null,
            sName: "",
            sSortDataType: "std",
            sTitle: null,
            sType: null,
            sWidth: null
        }, s(Yt.defaults.column), Yt.models.oSettings = {
            oFeatures: {
                bAutoWidth: null,
                bDeferRender: null,
                bFilter: null,
                bInfo: null,
                bLengthChange: null,
                bPaginate: null,
                bProcessing: null,
                bServerSide: null,
                bSort: null,
                bSortMulti: null,
                bSortClasses: null,
                bStateSave: null
            },
            oScroll: {
                bCollapse: null,
                iBarWidth: 0,
                sX: null,
                sXInner: null,
                sY: null
            },
            oLanguage: {
                fnInfoCallback: null
            },
            oBrowser: {
                bScrollOversize: !1,
                bScrollbarLeft: !1,
                bBounding: !1,
                barWidth: 0
            },
            ajax: null,
            aanFeatures: [],
            aoData: [],
            aiDisplay: [],
            aiDisplayMaster: [],
            aIds: {},
            aoColumns: [],
            aoHeader: [],
            aoFooter: [],
            oPreviousSearch: {},
            aoPreSearchCols: [],
            aaSorting: null,
            aaSortingFixed: [],
            asStripeClasses: null,
            asDestroyStripes: [],
            sDestroyWidth: 0,
            aoRowCallback: [],
            aoHeaderCallback: [],
            aoFooterCallback: [],
            aoDrawCallback: [],
            aoRowCreatedCallback: [],
            aoPreDrawCallback: [],
            aoInitComplete: [],
            aoStateSaveParams: [],
            aoStateLoadParams: [],
            aoStateLoaded: [],
            sTableId: "",
            nTable: null,
            nTHead: null,
            nTFoot: null,
            nTBody: null,
            nTableWrapper: null,
            bDeferLoading: !1,
            bInitialised: !1,
            aoOpenRows: [],
            sDom: null,
            searchDelay: null,
            sPaginationType: "two_button",
            iStateDuration: 0,
            aoStateSave: [],
            aoStateLoad: [],
            oSavedState: null,
            oLoadedState: null,
            sAjaxSource: null,
            sAjaxDataProp: null,
            bAjaxDataGet: !0,
            jqXHR: null,
            json: n,
            oAjaxData: n,
            fnServerData: null,
            aoServerParams: [],
            sServerMethod: null,
            fnFormatNumber: null,
            aLengthMenu: null,
            iDraw: 0,
            bDrawing: !1,
            iDrawError: -1,
            _iDisplayLength: 10,
            _iDisplayStart: 0,
            _iRecordsTotal: 0,
            _iRecordsDisplay: 0,
            bJUI: null,
            oClasses: {},
            bFiltered: !1,
            bSorted: !1,
            bSortCellsTop: null,
            oInit: null,
            aoDestroyCallback: [],
            fnRecordsTotal: function() {
                return "ssp" == Ht(this) ? 1 * this._iRecordsTotal : this.aiDisplayMaster.length
            },
            fnRecordsDisplay: function() {
                return "ssp" == Ht(this) ? 1 * this._iRecordsDisplay : this.aiDisplay.length
            },
            fnDisplayEnd: function() {
                var t = this._iDisplayLength,
                    e = this._iDisplayStart,
                    i = e + t,
                    n = this.aiDisplay.length,
                    s = this.oFeatures,
                    a = s.bPaginate;
                return s.bServerSide ? !1 === a || -1 === t ? e + n : Math.min(e + t, this._iRecordsDisplay) : !a || i > n || -1 === t ? n : i
            },
            oInstance: null,
            sInstance: null,
            iTabIndex: 0,
            nScrollHead: null,
            nScrollFoot: null,
            aLastSort: [],
            oPlugins: {},
            rowIdFn: null,
            rowId: null
        }, Yt.ext = Wt = {
            buttons: {},
            classes: {},
            builder: "-source-",
            errMode: "alert",
            feature: [],
            search: [],
            selector: {
                cell: [],
                column: [],
                row: []
            },
            internal: {},
            legacy: {
                ajax: null
            },
            pager: {},
            renderer: {
                pageButton: {},
                header: {}
            },
            order: {},
            type: {
                detect: [],
                search: {},
                order: {}
            },
            _unique: 0,
            fnVersionCheck: Yt.fnVersionCheck,
            iApiIndex: 0,
            oJUIClasses: {},
            sVersion: Yt.version
        }, t.extend(Wt, {
            afnFiltering: Wt.search,
            aTypes: Wt.type.detect,
            ofnSearch: Wt.type.search,
            oSort: Wt.type.order,
            afnSortData: Wt.order,
            aoFeatures: Wt.feature,
            oApi: Wt.internal,
            oStdClasses: Wt.classes,
            oPagination: Wt.pager
        }), t.extend(Yt.ext.classes, {
            sTable: "dataTable",
            sNoFooter: "no-footer",
            sPageButton: "paginate_button",
            sPageButtonActive: "current",
            sPageButtonDisabled: "disabled",
            sStripeOdd: "odd",
            sStripeEven: "even",
            sRowEmpty: "dataTables_empty",
            sWrapper: "dataTables_wrapper",
            sFilter: "dataTables_filter",
            sInfo: "dataTables_info",
            sPaging: "dataTables_paginate paging_",
            sLength: "dataTables_length",
            sProcessing: "dataTables_processing",
            sSortAsc: "sorting_asc",
            sSortDesc: "sorting_desc",
            sSortable: "sorting",
            sSortableAsc: "sorting_asc_disabled",
            sSortableDesc: "sorting_desc_disabled",
            sSortableNone: "sorting_disabled",
            sSortColumn: "sorting_",
            sFilterInput: "",
            sLengthSelect: "",
            sScrollWrapper: "dataTables_scroll",
            sScrollHead: "dataTables_scrollHead",
            sScrollHeadInner: "dataTables_scrollHeadInner",
            sScrollBody: "dataTables_scrollBody",
            sScrollFoot: "dataTables_scrollFoot",
            sScrollFootInner: "dataTables_scrollFootInner",
            sHeaderTH: "",
            sFooterTH: "",
            sSortJUIAsc: "",
            sSortJUIDesc: "",
            sSortJUI: "",
            sSortJUIAscAllowed: "",
            sSortJUIDescAllowed: "",
            sSortJUIWrapper: "",
            sSortIcon: "",
            sJUIHeader: "",
            sJUIFooter: ""
        });
        var Ie = "",
            Ie = "",
            Ae = Ie + "ui-state-default",
            Ee = Ie + "css_right ui-icon ui-icon-",
            Me = Ie + "fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix";
        t.extend(Yt.ext.oJUIClasses, Yt.ext.classes, {
            sPageButton: "fg-button ui-button " + Ae,
            sPageButtonActive: "ui-state-disabled",
            sPageButtonDisabled: "ui-state-disabled",
            sPaging: "dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_",
            sSortAsc: Ae + " sorting_asc",
            sSortDesc: Ae + " sorting_desc",
            sSortable: Ae + " sorting",
            sSortableAsc: Ae + " sorting_asc_disabled",
            sSortableDesc: Ae + " sorting_desc_disabled",
            sSortableNone: Ae + " sorting_disabled",
            sSortJUIAsc: Ee + "triangle-1-n",
            sSortJUIDesc: Ee + "triangle-1-s",
            sSortJUI: Ee + "carat-2-n-s",
            sSortJUIAscAllowed: Ee + "carat-1-n",
            sSortJUIDescAllowed: Ee + "carat-1-s",
            sSortJUIWrapper: "DataTables_sort_wrapper",
            sSortIcon: "DataTables_sort_icon",
            sScrollHead: "dataTables_scrollHead " + Ae,
            sScrollFoot: "dataTables_scrollFoot " + Ae,
            sHeaderTH: Ae,
            sFooterTH: Ae,
            sJUIHeader: Me + " ui-corner-tl ui-corner-tr",
            sJUIFooter: Me + " ui-corner-bl ui-corner-br"
        });
        var Pe = Yt.ext.pager;
        t.extend(Pe, {
            simple: function() {
                return ["previous", "next"]
            },
            full: function() {
                return ["first", "previous", "next", "last"]
            },
            numbers: function(t, e) {
                return [Ot(t, e)]
            },
            simple_numbers: function(t, e) {
                return ["previous", Ot(t, e), "next"]
            },
            full_numbers: function(t, e) {
                return ["first", "previous", Ot(t, e), "next", "last"]
            },
            first_last_numbers: function(t, e) {
                return ["first", Ot(t, e), "last"]
            },
            _numbers: Ot,
            numbers_length: 7
        }), t.extend(!0, Yt.ext.renderer, {
            pageButton: {
                _: function(e, s, a, o, r, l) {
                    var c, u, d, h = e.oClasses,
                        p = e.oLanguage.oPaginate,
                        f = e.oLanguage.oAria.paginate || {},
                        m = 0,
                        g = function(i, n) {
                            var s, o, d, _, v = function(t) {
                                ct(e, t.data.action, !0)
                            };
                            for (s = 0, o = n.length; s < o; s++)
                                if (_ = n[s], t.isArray(_)) d = t("<" + (_.DT_el || "div") + "/>").appendTo(i), g(d, _);
                                else {
                                    switch (c = null, u = "", _) {
                                        case "ellipsis":
                                            i.append('<span class="ellipsis">&#x2026;</span>');
                                            break;
                                        case "first":
                                            c = p.sFirst, u = _ + (r > 0 ? "" : " " + h.sPageButtonDisabled);
                                            break;
                                        case "previous":
                                            c = p.sPrevious, u = _ + (r > 0 ? "" : " " + h.sPageButtonDisabled);
                                            break;
                                        case "next":
                                            c = p.sNext, u = _ + (r < l - 1 ? "" : " " + h.sPageButtonDisabled);
                                            break;
                                        case "last":
                                            c = p.sLast, u = _ + (r < l - 1 ? "" : " " + h.sPageButtonDisabled);
                                            break;
                                        default:
                                            c = _ + 1, u = r === _ ? h.sPageButtonActive : ""
                                    }
                                    null !== c && (d = t("<a>", {
                                        "class": h.sPageButton + " " + u,
                                        "aria-controls": e.sTableId,
                                        "aria-label": f[_],
                                        "data-dt-idx": m,
                                        tabindex: e.iTabIndex,
                                        id: 0 === a && "string" == typeof _ ? e.sTableId + "_" + _ : null
                                    }).html(c).appendTo(i), Pt(d, {
                                        action: _
                                    }, v), m++)
                                }
                        };
                    try {
                        d = t(s).find(i.activeElement).data("dt-idx")
                    } catch (t) {}
                    g(t(s).empty(), o), d !== n && t(s).find("[data-dt-idx=" + d + "]").focus()
                }
            }
        }), t.extend(Yt.ext.type.detect, [function(t, e) {
            var i = e.oLanguage.sDecimal;
            return ie(t, i) ? "num" + i : null
        }, function(t) {
            if (t && !(t instanceof Date) && !Qt.test(t)) return null;
            var e = Date.parse(t);
            return null !== e && !isNaN(e) || Zt(t) ? "date" : null
        }, function(t, e) {
            var i = e.oLanguage.sDecimal;
            return ie(t, i, !0) ? "num-fmt" + i : null
        }, function(t, e) {
            var i = e.oLanguage.sDecimal;
            return ne(t, i) ? "html-num" + i : null
        }, function(t, e) {
            var i = e.oLanguage.sDecimal;
            return ne(t, i, !0) ? "html-num-fmt" + i : null
        }, function(t) {
            return Zt(t) || "string" == typeof t && -1 !== t.indexOf("<") ? "html" : null
        }]), t.extend(Yt.ext.type.search, {
            html: function(t) {
                return Zt(t) ? t : "string" == typeof t ? t.replace(Kt, " ").replace(Xt, "") : ""
            },
            string: function(t) {
                return Zt(t) ? t : "string" == typeof t ? t.replace(Kt, " ") : t
            }
        });
        var Ne = function(t, e, i, n) {
            return 0 === t || t && "-" !== t ? (e && (t = ee(t, e)), t.replace && (i && (t = t.replace(i, "")), n && (t = t.replace(n, ""))), 1 * t) : -Infinity
        };
        t.extend(Wt.type.order, {
            "date-pre": function(t) {
                return Date.parse(t) || -Infinity
            },
            "html-pre": function(t) {
                return Zt(t) ? "" : t.replace ? t.replace(/<.*?>/g, "").toLowerCase() : t + ""
            },
            "string-pre": function(t) {
                return Zt(t) ? "" : "string" == typeof t ? t.toLowerCase() : t.toString ? t.toString() : ""
            },
            "string-asc": function(t, e) {
                return t < e ? -1 : t > e ? 1 : 0
            },
            "string-desc": function(t, e) {
                return t < e ? 1 : t > e ? -1 : 0
            }
        }), Rt(""), t.extend(!0, Yt.ext.renderer, {
            header: {
                _: function(e, i, n, s) {
                    t(e.nTable).on("order.dt.DT", function(t, a, o, r) {
                        e === a && (t = n.idx, i.removeClass(n.sSortingClass + " " + s.sSortAsc + " " + s.sSortDesc).addClass("asc" == r[t] ? s.sSortAsc : "desc" == r[t] ? s.sSortDesc : n.sSortingClass))
                    })
                },
                jqueryui: function(e, i, n, s) {
                    t("<div/>").addClass(s.sSortJUIWrapper).append(i.contents()).append(t("<span/>").addClass(s.sSortIcon + " " + n.sSortingClassJUI)).appendTo(i), t(e.nTable).on("order.dt.DT", function(t, a, o, r) {
                        e === a && (t = n.idx, i.removeClass(s.sSortAsc + " " + s.sSortDesc).addClass("asc" == r[t] ? s.sSortAsc : "desc" == r[t] ? s.sSortDesc : n.sSortingClass), i.find("span." + s.sSortIcon).removeClass(s.sSortJUIAsc + " " + s.sSortJUIDesc + " " + s.sSortJUI + " " + s.sSortJUIAscAllowed + " " + s.sSortJUIDescAllowed).addClass("asc" == r[t] ? s.sSortJUIAsc : "desc" == r[t] ? s.sSortJUIDesc : n.sSortingClassJUI))
                    })
                }
            }
        });
        var Fe = function(t) {
            return "string" == typeof t ? t.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;") : t
        };
        return Yt.render = {
            number: function(t, e, i, n, s) {
                return {
                    display: function(a) {
                        if ("number" != typeof a && "string" != typeof a) return a;
                        var o = 0 > a ? "-" : "",
                            r = parseFloat(a);
                        return isNaN(r) ? Fe(a) : (r = r.toFixed(i), a = Math.abs(r), r = parseInt(a, 10), a = i ? e + (a - r).toFixed(i).substring(2) : "", o + (n || "") + r.toString().replace(/\B(?=(\d{3})+(?!\d))/g, t) + a + (s || ""))
                    }
                }
            },
            text: function() {
                return {
                    display: Fe
                }
            }
        }, t.extend(Yt.ext.internal, {
            _fnExternApiFunc: zt,
            _fnBuildAjax: z,
            _fnAjaxUpdate: W,
            _fnAjaxParameters: B,
            _fnAjaxUpdateDraw: q,
            _fnAjaxDataSrc: U,
            _fnAddColumn: d,
            _fnColumnOptions: h,
            _fnAdjustColumnSizing: p,
            _fnVisibleToColumnIndex: f,
            _fnColumnIndexToVisible: m,
            _fnVisbleColumns: g,
            _fnGetColumns: _,
            _fnColumnTypes: v,
            _fnApplyColumnDefs: b,
            _fnHungarianMap: s,
            _fnCamelToHungarian: a,
            _fnLanguageCompat: o,
            _fnBrowserDetect: c,
            _fnAddData: y,
            _fnAddTr: w,
            _fnNodeToDataIndex: function(t, e) {
                return e._DT_RowIndex !== n ? e._DT_RowIndex : null
            },
            _fnNodeToColumnIndex: function(e, i, n) {
                return t.inArray(n, e.aoData[i].anCells)
            },
            _fnGetCellData: x,
            _fnSetCellData: $,
            _fnSplitObjNotation: k,
            _fnGetObjectDataFn: C,
            _fnSetObjectDataFn: D,
            _fnGetDataMaster: S,
            _fnClearTable: T,
            _fnDeleteIndex: I,
            _fnInvalidate: A,
            _fnGetRowElements: E,
            _fnCreateTr: M,
            _fnBuildHead: N,
            _fnDrawHead: F,
            _fnDraw: j,
            _fnReDraw: L,
            _fnAddOptionsHtml: H,
            _fnDetectHeader: O,
            _fnGetUniqueThs: R,
            _fnFeatureHtmlFilter: Y,
            _fnFilterComplete: V,
            _fnFilterCustom: K,
            _fnFilterColumn: X,
            _fnFilter: Q,
            _fnFilterCreateSearch: G,
            _fnEscapeRegex: he,
            _fnFilterData: J,
            _fnFeatureHtmlInfo: et,
            _fnUpdateInfo: it,
            _fnInfoMacros: nt,
            _fnInitialise: st,
            _fnInitComplete: at,
            _fnLengthChange: ot,
            _fnFeatureHtmlLength: rt,
            _fnFeatureHtmlPaginate: lt,
            _fnPageChange: ct,
            _fnFeatureHtmlProcessing: ut,
            _fnProcessingDisplay: dt,
            _fnFeatureHtmlTable: ht,
            _fnScrollDraw: pt,
            _fnApplyToChildren: ft,
            _fnCalculateColumnWidths: mt,
            _fnThrottle: ge,
            _fnConvertToWidth: gt,
            _fnGetWidestNode: _t,
            _fnGetMaxLenString: vt,
            _fnStringToCss: bt,
            _fnSortFlatten: yt,
            _fnSort: wt,
            _fnSortAria: xt,
            _fnSortListener: $t,
            _fnSortAttachListener: kt,
            _fnSortingClasses: Ct,
            _fnSortData: Dt,
            _fnSaveState: St,
            _fnLoadState: Tt,
            _fnSettingsFromNode: It,
            _fnLog: At,
            _fnMap: Et,
            _fnBindAction: Pt,
            _fnCallbackReg: Nt,
            _fnCallbackFire: Ft,
            _fnLengthOverflow: jt,
            _fnRenderer: Lt,
            _fnDataSource: Ht,
            _fnRowAttributes: P,
            _fnCalculateEnd: function() {}
        }), t.fn.dataTable = Yt, Yt.$ = t, t.fn.dataTableSettings = Yt.settings, t.fn.dataTableExt = Yt.ext, t.fn.DataTable = function(e) {
            return t(this).dataTable(e).api()
        }, t.each(Yt, function(e, i) {
            t.fn.DataTable[e] = i
        }), t.fn.dataTable
    }),
    function() {
        var t, e, i, n, s, a = {}.hasOwnProperty,
            o = function(t, e) {
                function i() {
                    this.constructor = t
                }
                for (var n in e) a.call(e, n) && (t[n] = e[n]);
                return i.prototype = e.prototype, t.prototype = new i, t.__super__ = e.prototype, t
            };
        n = function() {
            function t() {
                this.options_index = 0, this.parsed = []
            }
            return t.prototype.add_node = function(t) {
                return "OPTGROUP" === t.nodeName.toUpperCase() ? this.add_group(t) : this.add_option(t)
            }, t.prototype.add_group = function(t) {
                var e, i, n, s, a, o;
                for (e = this.parsed.length, this.parsed.push({
                        array_index: e,
                        group: !0,
                        label: this.escapeExpression(t.label),
                        title: t.title ? t.title : void 0,
                        children: 0,
                        disabled: t.disabled,
                        classes: t.className
                    }), a = t.childNodes, o = [], n = 0, s = a.length; s > n; n++) i = a[n], o.push(this.add_option(i, e, t.disabled));
                return o
            }, t.prototype.add_option = function(t, e, i) {
                return "OPTION" === t.nodeName.toUpperCase() ? ("" !== t.text ? (null != e && (this.parsed[e].children += 1), this.parsed.push({
                    array_index: this.parsed.length,
                    options_index: this.options_index,
                    value: t.value,
                    text: t.text,
                    html: t.innerHTML,
                    title: t.title ? t.title : void 0,
                    selected: t.selected,
                    disabled: !0 === i ? i : t.disabled,
                    group_array_index: e,
                    group_label: null != e ? this.parsed[e].label : null,
                    classes: t.className,
                    style: t.style.cssText
                })) : this.parsed.push({
                    array_index: this.parsed.length,
                    options_index: this.options_index,
                    empty: !0
                }), this.options_index += 1) : void 0
            }, t.prototype.escapeExpression = function(t) {
                var e, i;
                return null == t || !1 === t ? "" : /[\&\<\>\"\'\`]/.test(t) ? (e = {
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#x27;",
                    "`": "&#x60;"
                }, i = /&(?!\w+;)|[\<\>\"\'\`]/g, t.replace(i, function(t) {
                    return e[t] || "&amp;"
                })) : t
            }, t
        }(), n.select_to_array = function(t) {
            var e, i, s, a, o;
            for (i = new n, o = t.childNodes, s = 0, a = o.length; a > s; s++) e = o[s], i.add_node(e);
            return i.parsed
        }, e = function() {
            function t(e, i) {
                this.form_field = e, this.options = null != i ? i : {}, t.browser_is_supported() && (this.is_multiple = this.form_field.multiple, this.set_default_text(), this.set_default_values(), this.setup(), this.set_up_html(), this.register_observers(), this.on_ready())
            }
            return t.prototype.set_default_values = function() {
                var t = this;
                return this.click_test_action = function(e) {
                    return t.test_active_click(e)
                }, this.activate_action = function(e) {
                    return t.activate_field(e)
                }, this.active_field = !1, this.mouse_on_container = !1, this.results_showing = !1, this.result_highlighted = null, this.allow_single_deselect = null != this.options.allow_single_deselect && null != this.form_field.options[0] && "" === this.form_field.options[0].text && this.options.allow_single_deselect, this.disable_search_threshold = this.options.disable_search_threshold || 0, this.disable_search = this.options.disable_search || !1, this.enable_split_word_search = null == this.options.enable_split_word_search || this.options.enable_split_word_search, this.group_search = null == this.options.group_search || this.options.group_search, this.search_contains = this.options.search_contains || !1, this.single_backstroke_delete = null == this.options.single_backstroke_delete || this.options.single_backstroke_delete, this.max_selected_options = this.options.max_selected_options || 1 / 0, this.inherit_select_classes = this.options.inherit_select_classes || !1, this.display_selected_options = null == this.options.display_selected_options || this.options.display_selected_options, this.display_disabled_options = null == this.options.display_disabled_options || this.options.display_disabled_options, this.include_group_label_in_selected = this.options.include_group_label_in_selected || !1, this.max_shown_results = this.options.max_shown_results || Number.POSITIVE_INFINITY
            }, t.prototype.set_default_text = function() {
                return this.form_field.getAttribute("data-placeholder") ? this.default_text = this.form_field.getAttribute("data-placeholder") : this.is_multiple ? this.default_text = this.options.placeholder_text_multiple || this.options.placeholder_text || t.default_multiple_text : this.default_text = this.options.placeholder_text_single || this.options.placeholder_text || t.default_single_text, this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || t.default_no_result_text
            }, t.prototype.choice_label = function(t) {
                return this.include_group_label_in_selected && null != t.group_label ? "<b class='group-name'>" + t.group_label + "</b>" + t.html : t.html
            }, t.prototype.mouse_enter = function() {
                return this.mouse_on_container = !0
            }, t.prototype.mouse_leave = function() {
                return this.mouse_on_container = !1
            }, t.prototype.input_focus = function() {
                var t = this;
                if (this.is_multiple) {
                    if (!this.active_field) return setTimeout(function() {
                        return t.container_mousedown()
                    }, 50)
                } else if (!this.active_field) return this.activate_field()
            }, t.prototype.input_blur = function() {
                var t = this;
                return this.mouse_on_container ? void 0 : (this.active_field = !1, setTimeout(function() {
                    return t.blur_test()
                }, 100))
            }, t.prototype.results_option_build = function(t) {
                var e, i, n, s, a, o, r;
                for (e = "", s = 0, r = this.results_data, a = 0, o = r.length; o > a && (i = r[a], n = "", n = i.group ? this.result_add_group(i) : this.result_add_option(i), "" !== n && (s++, e += n), (null != t ? t.first : void 0) && (i.selected && this.is_multiple ? this.choice_build(i) : i.selected && !this.is_multiple && this.single_set_selected_text(this.choice_label(i))), !(s >= this.max_shown_results)); a++);
                return e
            }, t.prototype.result_add_option = function(t) {
                var e, i;
                return t.search_match && this.include_option_in_results(t) ? (e = [], t.disabled || t.selected && this.is_multiple || e.push("active-result"), !t.disabled || t.selected && this.is_multiple || e.push("disabled-result"), t.selected && e.push("result-selected"), null != t.group_array_index && e.push("group-option"), "" !== t.classes && e.push(t.classes), i = document.createElement("li"), i.className = e.join(" "), i.style.cssText = t.style, i.setAttribute("data-option-array-index", t.array_index), i.innerHTML = t.search_text, t.title && (i.title = t.title), this.outerHTML(i)) : ""
            }, t.prototype.result_add_group = function(t) {
                var e, i;
                return (t.search_match || t.group_match) && t.active_options > 0 ? (e = [], e.push("group-result"), t.classes && e.push(t.classes), i = document.createElement("li"), i.className = e.join(" "), i.innerHTML = t.search_text, t.title && (i.title = t.title), this.outerHTML(i)) : ""
            }, t.prototype.results_update_field = function() {
                return this.set_default_text(), this.is_multiple || this.results_reset_cleanup(), this.result_clear_highlight(), this.results_build(), this.results_showing ? this.winnow_results() : void 0
            }, t.prototype.reset_single_select_options = function() {
                var t, e, i, n, s;
                for (n = this.results_data, s = [], e = 0, i = n.length; i > e; e++) t = n[e], t.selected ? s.push(t.selected = !1) : s.push(void 0);
                return s
            }, t.prototype.results_toggle = function() {
                return this.results_showing ? this.results_hide() : this.results_show()
            }, t.prototype.results_search = function() {
                return this.results_showing ? this.winnow_results() : this.results_show()
            }, t.prototype.winnow_results = function() {
                var t, e, i, n, s, a, o, r, l, c, u, d;
                for (this.no_results_clear(), n = 0, a = this.get_search_text(), t = a.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), l = new RegExp(t, "i"), i = this.get_search_regex(t), d = this.results_data, c = 0, u = d.length; u > c; c++) e = d[c], e.search_match = !1, s = null, this.include_option_in_results(e) && (e.group && (e.group_match = !1, e.active_options = 0), null != e.group_array_index && this.results_data[e.group_array_index] && (s = this.results_data[e.group_array_index], 0 === s.active_options && s.search_match && (n += 1), s.active_options += 1), e.search_text = e.group ? e.label : e.html, (!e.group || this.group_search) && (e.search_match = this.search_string_match(e.search_text, i), e.search_match && !e.group && (n += 1), e.search_match ? (a.length && (o = e.search_text.search(l), r = e.search_text.substr(0, o + a.length) + "</em>" + e.search_text.substr(o + a.length), e.search_text = r.substr(0, o) + "<em>" + r.substr(o)), null != s && (s.group_match = !0)) : null != e.group_array_index && this.results_data[e.group_array_index].search_match && (e.search_match = !0)));
                return this.result_clear_highlight(), 1 > n && a.length ? (this.update_results_content(""), this.no_results(a)) : (this.update_results_content(this.results_option_build()), this.winnow_results_set_highlight())
            }, t.prototype.get_search_regex = function(t) {
                var e;
                return e = this.search_contains ? "" : "^", new RegExp(e + t, "i")
            }, t.prototype.search_string_match = function(t, e) {
                var i, n, s, a;
                if (e.test(t)) return !0;
                if (this.enable_split_word_search && (t.indexOf(" ") >= 0 || 0 === t.indexOf("[")) && (n = t.replace(/\[|\]/g, "").split(" "), n.length))
                    for (s = 0, a = n.length; a > s; s++)
                        if (i = n[s], e.test(i)) return !0
            }, t.prototype.choices_count = function() {
                var t, e, i, n;
                if (null != this.selected_option_count) return this.selected_option_count;
                for (this.selected_option_count = 0, n = this.form_field.options, e = 0, i = n.length; i > e; e++) t = n[e], t.selected && (this.selected_option_count += 1);
                return this.selected_option_count
            }, t.prototype.choices_click = function(t) {
                return t.preventDefault(), this.results_showing || this.is_disabled ? void 0 : this.results_show()
            }, t.prototype.keyup_checker = function(t) {
                var e, i;
                switch (e = null != (i = t.which) ? i : t.keyCode, this.search_field_scale(), e) {
                    case 8:
                        if (this.is_multiple && this.backstroke_length < 1 && this.choices_count() > 0) return this.keydown_backstroke();
                        if (!this.pending_backstroke) return this.result_clear_highlight(), this.results_search();
                        break;
                    case 13:
                        if (t.preventDefault(), this.results_showing) return this.result_select(t);
                        break;
                    case 27:
                        return this.results_showing && this.results_hide(), !0;
                    case 9:
                    case 38:
                    case 40:
                    case 16:
                    case 91:
                    case 17:
                    case 18:
                        break;
                    default:
                        return this.results_search()
                }
            }, t.prototype.clipboard_event_checker = function() {
                var t = this;
                return setTimeout(function() {
                    return t.results_search()
                }, 50)
            }, t.prototype.container_width = function() {
                return null != this.options.width ? this.options.width : this.form_field.offsetWidth + "px"
            }, t.prototype.include_option_in_results = function(t) {
                return !(this.is_multiple && !this.display_selected_options && t.selected) && (!(!this.display_disabled_options && t.disabled) && !t.empty)
            }, t.prototype.search_results_touchstart = function(t) {
                return this.touch_started = !0, this.search_results_mouseover(t)
            }, t.prototype.search_results_touchmove = function(t) {
                return this.touch_started = !1, this.search_results_mouseout(t)
            }, t.prototype.search_results_touchend = function(t) {
                return this.touch_started ? this.search_results_mouseup(t) : void 0
            }, t.prototype.outerHTML = function(t) {
                var e;
                return t.outerHTML ? t.outerHTML : (e = document.createElement("div"), e.appendChild(t), e.innerHTML)
            }, t.browser_is_supported = function() {
                return !/iP(od|hone)/i.test(window.navigator.userAgent) && ((!/Android/i.test(window.navigator.userAgent) || !/Mobile/i.test(window.navigator.userAgent)) && (!/IEMobile/i.test(window.navigator.userAgent) && (!/Windows Phone/i.test(window.navigator.userAgent) && (!/BlackBerry/i.test(window.navigator.userAgent) && (!/BB10/i.test(window.navigator.userAgent) && ("Microsoft Internet Explorer" !== window.navigator.appName || document.documentMode >= 8))))))
            }, t.default_multiple_text = "Select Some Options", t.default_single_text = "Select an Option", t.default_no_result_text = "No results match", t
        }(), t = jQuery, t.fn.extend({
            chosen: function(n) {
                return e.browser_is_supported() ? this.each(function() {
                    var e, s;
                    return e = t(this), s = e.data("chosen"), "destroy" === n ? void(s instanceof i && s.destroy()) : void(s instanceof i || e.data("chosen", new i(this, n)))
                }) : this
            }
        }), i = function(e) {
            function i() {
                return s = i.__super__.constructor.apply(this, arguments)
            }
            return o(i, e), i.prototype.setup = function() {
                return this.form_field_jq = t(this.form_field), this.current_selectedIndex = this.form_field.selectedIndex, this.is_rtl = this.form_field_jq.hasClass("chosen-rtl")
            }, i.prototype.set_up_html = function() {
                var e, i;
                return e = ["chosen-container"], e.push("chosen-container-" + (this.is_multiple ? "multi" : "single")), this.inherit_select_classes && this.form_field.className && e.push(this.form_field.className), this.is_rtl && e.push("chosen-rtl"), i = {
                    "class": e.join(" "),
                    style: "width: " + this.container_width() + ";",
                    title: this.form_field.title
                }, this.form_field.id.length && (i.id = this.form_field.id.replace(/[^\w]/g, "_") + "_chosen"), this.container = t("<div />", i), this.is_multiple ? this.container.html('<ul class="chosen-choices"><li class="search-field"><input type="text" value="' + this.default_text + '" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chosen-drop"><ul class="chosen-results"></ul></div>') : this.container.html('<a class="chosen-single chosen-default"><span>' + this.default_text + '</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" /></div><ul class="chosen-results"></ul></div>'), this.form_field_jq.hide().after(this.container), this.dropdown = this.container.find("div.chosen-drop").first(), this.search_field = this.container.find("input").first(), this.search_results = this.container.find("ul.chosen-results").first(), this.search_field_scale(), this.search_no_results = this.container.find("li.no-results").first(), this.is_multiple ? (this.search_choices = this.container.find("ul.chosen-choices").first(), this.search_container = this.container.find("li.search-field").first()) : (this.search_container = this.container.find("div.chosen-search").first(), this.selected_item = this.container.find(".chosen-single").first()), this.results_build(), this.set_tab_index(), this.set_label_behavior()
            }, i.prototype.on_ready = function() {
                return this.form_field_jq.trigger("chosen:ready", {
                    chosen: this
                })
            }, i.prototype.register_observers = function() {
                var t = this;
                return this.container.bind("touchstart.chosen", function(e) {
                    return t.container_mousedown(e), e.preventDefault()
                }), this.container.bind("touchend.chosen", function(e) {
                    return t.container_mouseup(e), e.preventDefault()
                }), this.container.bind("mousedown.chosen", function(e) {
                    t.container_mousedown(e)
                }), this.container.bind("mouseup.chosen", function(e) {
                    t.container_mouseup(e)
                }), this.container.bind("mouseenter.chosen", function(e) {
                    t.mouse_enter(e)
                }), this.container.bind("mouseleave.chosen", function(e) {
                    t.mouse_leave(e)
                }), this.search_results.bind("mouseup.chosen", function(e) {
                    t.search_results_mouseup(e)
                }), this.search_results.bind("mouseover.chosen", function(e) {
                    t.search_results_mouseover(e)
                }), this.search_results.bind("mouseout.chosen", function(e) {
                    t.search_results_mouseout(e)
                }), this.search_results.bind("mousewheel.chosen DOMMouseScroll.chosen", function(e) {
                    t.search_results_mousewheel(e)
                }), this.search_results.bind("touchstart.chosen", function(e) {
                    t.search_results_touchstart(e)
                }), this.search_results.bind("touchmove.chosen", function(e) {
                    t.search_results_touchmove(e)
                }), this.search_results.bind("touchend.chosen", function(e) {
                    t.search_results_touchend(e)
                }), this.form_field_jq.bind("chosen:updated.chosen", function(e) {
                    t.results_update_field(e)
                }), this.form_field_jq.bind("chosen:activate.chosen", function(e) {
                    t.activate_field(e)
                }), this.form_field_jq.bind("chosen:open.chosen", function(e) {
                    t.container_mousedown(e)
                }), this.form_field_jq.bind("chosen:close.chosen", function(e) {
                    t.input_blur(e)
                }), this.search_field.bind("blur.chosen", function(e) {
                    t.input_blur(e)
                }), this.search_field.bind("keyup.chosen", function(e) {
                    t.keyup_checker(e)
                }), this.search_field.bind("keydown.chosen", function(e) {
                    t.keydown_checker(e)
                }), this.search_field.bind("focus.chosen", function(e) {
                    t.input_focus(e)
                }), this.search_field.bind("cut.chosen", function(e) {
                    t.clipboard_event_checker(e)
                }), this.search_field.bind("paste.chosen", function(e) {
                    t.clipboard_event_checker(e)
                }), this.is_multiple ? this.search_choices.bind("click.chosen", function(e) {
                    t.choices_click(e)
                }) : this.container.bind("click.chosen", function(t) {
                    t.preventDefault()
                })
            }, i.prototype.destroy = function() {
                return t(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), this.search_field[0].tabIndex && (this.form_field_jq[0].tabIndex = this.search_field[0].tabIndex), this.container.remove(), this.form_field_jq.removeData("chosen"), this.form_field_jq.show()
            }, i.prototype.search_field_disabled = function() {
                return this.is_disabled = this.form_field_jq[0].disabled, this.is_disabled ? (this.container.addClass("chosen-disabled"), this.search_field[0].disabled = !0, this.is_multiple || this.selected_item.unbind("focus.chosen", this.activate_action), this.close_field()) : (this.container.removeClass("chosen-disabled"), this.search_field[0].disabled = !1, this.is_multiple ? void 0 : this.selected_item.bind("focus.chosen", this.activate_action))
            }, i.prototype.container_mousedown = function(e) {
                return this.is_disabled || (e && "mousedown" === e.type && !this.results_showing && e.preventDefault(), null != e && t(e.target).hasClass("search-choice-close")) ? void 0 : (this.active_field ? this.is_multiple || !e || t(e.target)[0] !== this.selected_item[0] && !t(e.target).parents("a.chosen-single").length || (e.preventDefault(), this.results_toggle()) : (this.is_multiple && this.search_field.val(""), t(this.container[0].ownerDocument).bind("click.chosen", this.click_test_action), this.results_show()), this.activate_field())
            }, i.prototype.container_mouseup = function(t) {
                return "ABBR" !== t.target.nodeName || this.is_disabled ? void 0 : this.results_reset(t)
            }, i.prototype.search_results_mousewheel = function(t) {
                var e;
                return t.originalEvent && (e = t.originalEvent.deltaY || -t.originalEvent.wheelDelta || t.originalEvent.detail), null != e ? (t.preventDefault(), "DOMMouseScroll" === t.type && (e *= 40), this.search_results.scrollTop(e + this.search_results.scrollTop())) : void 0
            }, i.prototype.blur_test = function() {
                return !this.active_field && this.container.hasClass("chosen-container-active") ? this.close_field() : void 0
            }, i.prototype.close_field = function() {
                return t(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), this.active_field = !1, this.results_hide(), this.container.removeClass("chosen-container-active"), this.clear_backstroke(), this.show_search_field_default(), this.search_field_scale()
            }, i.prototype.activate_field = function() {
                return this.container.addClass("chosen-container-active"), this.active_field = !0, this.search_field.val(this.search_field.val()), this.search_field.focus()
            }, i.prototype.test_active_click = function(e) {
                var i;
                return i = t(e.target).closest(".chosen-container"), i.length && this.container[0] === i[0] ? this.active_field = !0 : this.close_field()
            }, i.prototype.results_build = function() {
                return this.parsing = !0, this.selected_option_count = null, this.results_data = n.select_to_array(this.form_field), this.is_multiple ? this.search_choices.find("li.search-choice").remove() : this.is_multiple || (this.single_set_selected_text(), this.disable_search || this.form_field.options.length <= this.disable_search_threshold ? (this.search_field[0].readOnly = !0, this.container.addClass("chosen-container-single-nosearch")) : (this.search_field[0].readOnly = !1, this.container.removeClass("chosen-container-single-nosearch"))), this.update_results_content(this.results_option_build({
                    first: !0
                })), this.search_field_disabled(), this.show_search_field_default(), this.search_field_scale(), this.parsing = !1
            }, i.prototype.result_do_highlight = function(t) {
                var e, i, n, s, a;
                if (t.length) {
                    if (this.result_clear_highlight(), this.result_highlight = t, this.result_highlight.addClass("highlighted"), n = parseInt(this.search_results.css("maxHeight"), 10), a = this.search_results.scrollTop(), s = n + a, i = this.result_highlight.position().top + this.search_results.scrollTop(), (e = i + this.result_highlight.outerHeight()) >= s) return this.search_results.scrollTop(e - n > 0 ? e - n : 0);
                    if (a > i) return this.search_results.scrollTop(i)
                }
            }, i.prototype.result_clear_highlight = function() {
                return this.result_highlight && this.result_highlight.removeClass("highlighted"), this.result_highlight = null
            }, i.prototype.results_show = function() {
                return this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", {
                    chosen: this
                }), !1) : (this.container.addClass("chosen-with-drop"), this.results_showing = !0, this.search_field.focus(), this.search_field.val(this.search_field.val()), this.winnow_results(), this.form_field_jq.trigger("chosen:showing_dropdown", {
                    chosen: this
                }))
            }, i.prototype.update_results_content = function(t) {
                return this.search_results.html(t)
            }, i.prototype.results_hide = function() {
                return this.results_showing && (this.result_clear_highlight(), this.container.removeClass("chosen-with-drop"), this.form_field_jq.trigger("chosen:hiding_dropdown", {
                    chosen: this
                })), this.results_showing = !1
            }, i.prototype.set_tab_index = function() {
                var t;
                return this.form_field.tabIndex ? (t = this.form_field.tabIndex, this.form_field.tabIndex = -1, this.search_field[0].tabIndex = t) : void 0
            }, i.prototype.set_label_behavior = function() {
                var e = this;
                return this.form_field_label = this.form_field_jq.parents("label"), !this.form_field_label.length && this.form_field.id.length && (this.form_field_label = t("label[for='" + this.form_field.id + "']")), this.form_field_label.length > 0 ? this.form_field_label.bind("click.chosen", function(t) {
                    return e.is_multiple ? e.container_mousedown(t) : e.activate_field()
                }) : void 0
            }, i.prototype.show_search_field_default = function() {
                return this.is_multiple && this.choices_count() < 1 && !this.active_field ? (this.search_field.val(this.default_text), this.search_field.addClass("default")) : (this.search_field.val(""), this.search_field.removeClass("default"))
            }, i.prototype.search_results_mouseup = function(e) {
                var i;
                return i = t(e.target).hasClass("active-result") ? t(e.target) : t(e.target).parents(".active-result").first(), i.length ? (this.result_highlight = i, this.result_select(e), this.search_field.focus()) : void 0
            }, i.prototype.search_results_mouseover = function(e) {
                var i;
                return i = t(e.target).hasClass("active-result") ? t(e.target) : t(e.target).parents(".active-result").first(), i ? this.result_do_highlight(i) : void 0
            }, i.prototype.search_results_mouseout = function(e) {
                return t(e.target).hasClass("active-result") ? this.result_clear_highlight() : void 0
            }, i.prototype.choice_build = function(e) {
                var i, n, s = this;
                return i = t("<li />", {
                    "class": "search-choice"
                }).html("<span>" + this.choice_label(e) + "</span>"), e.disabled ? i.addClass("search-choice-disabled") : (n = t("<a />", {
                    "class": "search-choice-close",
                    "data-option-array-index": e.array_index
                }), n.bind("click.chosen", function(t) {
                    return s.choice_destroy_link_click(t)
                }), i.append(n)), this.search_container.before(i)
            }, i.prototype.choice_destroy_link_click = function(e) {
                return e.preventDefault(), e.stopPropagation(), this.is_disabled ? void 0 : this.choice_destroy(t(e.target))
            }, i.prototype.choice_destroy = function(t) {
                return this.result_deselect(t[0].getAttribute("data-option-array-index")) ? (this.show_search_field_default(), this.is_multiple && this.choices_count() > 0 && this.search_field.val().length < 1 && this.results_hide(), t.parents("li").first().remove(), this.search_field_scale()) : void 0
            }, i.prototype.results_reset = function() {
                return this.reset_single_select_options(), this.form_field.options[0].selected = !0, this.single_set_selected_text(), this.show_search_field_default(), this.results_reset_cleanup(), this.form_field_jq.trigger("change"), this.active_field ? this.results_hide() : void 0
            }, i.prototype.results_reset_cleanup = function() {
                return this.current_selectedIndex = this.form_field.selectedIndex, this.selected_item.find("abbr").remove()
            }, i.prototype.result_select = function(t) {
                var e, i;
                return this.result_highlight ? (e = this.result_highlight, this.result_clear_highlight(), this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", {
                    chosen: this
                }), !1) : (this.is_multiple ? e.removeClass("active-result") : this.reset_single_select_options(), e.addClass("result-selected"), i = this.results_data[e[0].getAttribute("data-option-array-index")], i.selected = !0, this.form_field.options[i.options_index].selected = !0, this.selected_option_count = null, this.is_multiple ? this.choice_build(i) : this.single_set_selected_text(this.choice_label(i)), (t.metaKey || t.ctrlKey) && this.is_multiple || this.results_hide(), this.show_search_field_default(), (this.is_multiple || this.form_field.selectedIndex !== this.current_selectedIndex) && this.form_field_jq.trigger("change", {
                    selected: this.form_field.options[i.options_index].value
                }), this.current_selectedIndex = this.form_field.selectedIndex, t.preventDefault(), this.search_field_scale())) : void 0
            }, i.prototype.single_set_selected_text = function(t) {
                return null == t && (t = this.default_text), t === this.default_text ? this.selected_item.addClass("chosen-default") : (this.single_deselect_control_build(), this.selected_item.removeClass("chosen-default")), this.selected_item.find("span").html(t)
            }, i.prototype.result_deselect = function(t) {
                var e;
                return e = this.results_data[t], !this.form_field.options[e.options_index].disabled && (e.selected = !1, this.form_field.options[e.options_index].selected = !1, this.selected_option_count = null, this.result_clear_highlight(), this.results_showing && this.winnow_results(), this.form_field_jq.trigger("change", {
                    deselected: this.form_field.options[e.options_index].value
                }), this.search_field_scale(), !0)
            }, i.prototype.single_deselect_control_build = function() {
                return this.allow_single_deselect ? (this.selected_item.find("abbr").length || this.selected_item.find("span").first().after('<abbr class="search-choice-close"></abbr>'), this.selected_item.addClass("chosen-single-with-deselect")) : void 0
            }, i.prototype.get_search_text = function() {
                return t("<div/>").text(t.trim(this.search_field.val())).html()
            }, i.prototype.winnow_results_set_highlight = function() {
                var t, e;
                return e = this.is_multiple ? [] : this.search_results.find(".result-selected.active-result"), t = e.length ? e.first() : this.search_results.find(".active-result").first(), null != t ? this.result_do_highlight(t) : void 0
            }, i.prototype.no_results = function(e) {
                var i;
                return i = t('<li class="no-results">' + this.results_none_found + ' "<span></span>"</li>'), i.find("span").first().html(e), this.search_results.append(i), this.form_field_jq.trigger("chosen:no_results", {
                    chosen: this
                })
            }, i.prototype.no_results_clear = function() {
                return this.search_results.find(".no-results").remove()
            }, i.prototype.keydown_arrow = function() {
                var t;
                return this.results_showing && this.result_highlight ? (t = this.result_highlight.nextAll("li.active-result").first()) ? this.result_do_highlight(t) : void 0 : this.results_show()
            }, i.prototype.keyup_arrow = function() {
                var t;
                return this.results_showing || this.is_multiple ? this.result_highlight ? (t = this.result_highlight.prevAll("li.active-result"), t.length ? this.result_do_highlight(t.first()) : (this.choices_count() > 0 && this.results_hide(), this.result_clear_highlight())) : void 0 : this.results_show()
            }, i.prototype.keydown_backstroke = function() {
                var t;
                return this.pending_backstroke ? (this.choice_destroy(this.pending_backstroke.find("a").first()), this.clear_backstroke()) : (t = this.search_container.siblings("li.search-choice").last(), t.length && !t.hasClass("search-choice-disabled") ? (this.pending_backstroke = t, this.single_backstroke_delete ? this.keydown_backstroke() : this.pending_backstroke.addClass("search-choice-focus")) : void 0)
            }, i.prototype.clear_backstroke = function() {
                return this.pending_backstroke && this.pending_backstroke.removeClass("search-choice-focus"),
                    this.pending_backstroke = null
            }, i.prototype.keydown_checker = function(t) {
                var e, i;
                switch (e = null != (i = t.which) ? i : t.keyCode, this.search_field_scale(), 8 !== e && this.pending_backstroke && this.clear_backstroke(), e) {
                    case 8:
                        this.backstroke_length = this.search_field.val().length;
                        break;
                    case 9:
                        this.results_showing && !this.is_multiple && this.result_select(t), this.mouse_on_container = !1;
                        break;
                    case 13:
                        this.results_showing && t.preventDefault();
                        break;
                    case 32:
                        this.disable_search && t.preventDefault();
                        break;
                    case 38:
                        t.preventDefault(), this.keyup_arrow();
                        break;
                    case 40:
                        t.preventDefault(), this.keydown_arrow()
                }
            }, i.prototype.search_field_scale = function() {
                var e, i, n, s, a, o, r, l;
                if (this.is_multiple) {
                    for (0, o = 0, s = "position:absolute; left: -1000px; top: -1000px; display:none;", a = ["font-size", "font-style", "font-weight", "font-family", "line-height", "text-transform", "letter-spacing"], r = 0, l = a.length; l > r; r++) n = a[r], s += n + ":" + this.search_field.css(n) + ";";
                    return e = t("<div />", {
                        style: s
                    }), e.text(this.search_field.val()), t("body").append(e), o = e.width() + 25, e.remove(), i = this.container.outerWidth(), o > i - 10 && (o = i - 10), this.search_field.css({
                        width: o + "px"
                    })
                }
            }, i
        }(e)
    }.call(this),
    function(t) {
        t(["jquery"], function(t) {
            return function() {
                function e(t, e, i) {
                    return f({
                        type: w.error,
                        iconClass: m().iconClasses.error,
                        message: t,
                        optionsOverride: i,
                        title: e
                    })
                }

                function i(e, i) {
                    return e || (e = m()), _ = t("#" + e.containerId), _.length ? _ : (i && (_ = d(e)), _)
                }

                function n(t, e, i) {
                    return f({
                        type: w.info,
                        iconClass: m().iconClasses.info,
                        message: t,
                        optionsOverride: i,
                        title: e
                    })
                }

                function s(t) {
                    v = t
                }

                function a(t, e, i) {
                    return f({
                        type: w.success,
                        iconClass: m().iconClasses.success,
                        message: t,
                        optionsOverride: i,
                        title: e
                    })
                }

                function o(t, e, i) {
                    return f({
                        type: w.warning,
                        iconClass: m().iconClasses.warning,
                        message: t,
                        optionsOverride: i,
                        title: e
                    })
                }

                function r(t, e) {
                    var n = m();
                    _ || i(n), u(t, n, e) || c(n)
                }

                function l(e) {
                    var n = m();
                    if (_ || i(n), e && 0 === t(":focus", e).length) return void g(e);
                    _.children().length && _.remove()
                }

                function c(e) {
                    for (var i = _.children(), n = i.length - 1; n >= 0; n--) u(t(i[n]), e)
                }

                function u(e, i, n) {
                    var s = !(!n || !n.force) && n.force;
                    return !(!e || !s && 0 !== t(":focus", e).length) && (e[i.hideMethod]({
                        duration: i.hideDuration,
                        easing: i.hideEasing,
                        complete: function() {
                            g(e)
                        }
                    }), !0)
                }

                function d(e) {
                    return _ = t("<div/>").attr("id", e.containerId).addClass(e.positionClass), _.appendTo(t(e.target)), _
                }

                function h() {
                    return {
                        tapToDismiss: !0,
                        toastClass: "toast",
                        containerId: "toast-container",
                        debug: !1,
                        showMethod: "fadeIn",
                        showDuration: 300,
                        showEasing: "swing",
                        onShown: undefined,
                        hideMethod: "fadeOut",
                        hideDuration: 1e3,
                        hideEasing: "swing",
                        onHidden: undefined,
                        closeMethod: !1,
                        closeDuration: !1,
                        closeEasing: !1,
                        closeOnHover: !0,
                        extendedTimeOut: 1e3,
                        iconClasses: {
                            error: "toast-error",
                            info: "toast-info",
                            success: "toast-success",
                            warning: "toast-warning"
                        },
                        iconClass: "toast-info",
                        positionClass: "toast-top-right",
                        timeOut: 5e3,
                        titleClass: "toast-title",
                        messageClass: "toast-message",
                        escapeHtml: !1,
                        target: "body",
                        closeHtml: '<button type="button">&times;</button>',
                        closeClass: "toast-close-button",
                        newestOnTop: !0,
                        preventDuplicates: !1,
                        progressBar: !1,
                        progressClass: "toast-progress",
                        rtl: !1
                    }
                }

                function p(t) {
                    v && v(t)
                }

                function f(e) {
                    function n(t) {
                        return null == t && (t = ""), t.replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/'/g, "&#39;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
                    }

                    function s() {
                        l(), u(), d(), h(), f(), v(), c(), a()
                    }

                    function a() {
                        var t = "";
                        switch (e.iconClass) {
                            case "toast-success":
                            case "toast-info":
                                t = "polite";
                                break;
                            default:
                                t = "assertive"
                        }
                        I.attr("aria-live", t)
                    }

                    function o() {
                        D.closeOnHover && I.hover(k, $), !D.onclick && D.tapToDismiss && I.click(x), D.closeButton && P && P.click(function(t) {
                            t.stopPropagation ? t.stopPropagation() : t.cancelBubble !== undefined && !0 !== t.cancelBubble && (t.cancelBubble = !0), D.onCloseClick && D.onCloseClick(t), x(!0)
                        }), D.onclick && I.click(function(t) {
                            D.onclick(t), x()
                        })
                    }

                    function r() {
                        I.hide(), I[D.showMethod]({
                            duration: D.showDuration,
                            easing: D.showEasing,
                            complete: D.onShown
                        }), D.timeOut > 0 && (T = setTimeout(x, D.timeOut), N.maxHideTime = parseFloat(D.timeOut), N.hideEta = (new Date).getTime() + N.maxHideTime, D.progressBar && (N.intervalId = setInterval(C, 10)))
                    }

                    function l() {
                        e.iconClass && I.addClass(D.toastClass).addClass(S)
                    }

                    function c() {
                        D.newestOnTop ? _.prepend(I) : _.append(I)
                    }

                    function u() {
                        if (e.title) {
                            var t = e.title;
                            D.escapeHtml && (t = n(e.title)), A.append(t).addClass(D.titleClass), I.append(A)
                        }
                    }

                    function d() {
                        if (e.message) {
                            var t = e.message;
                            D.escapeHtml && (t = n(e.message)), E.append(t).addClass(D.messageClass), I.append(E)
                        }
                    }

                    function h() {
                        D.closeButton && (P.addClass(D.closeClass).attr("role", "button"), I.prepend(P))
                    }

                    function f() {
                        D.progressBar && (M.addClass(D.progressClass), I.prepend(M))
                    }

                    function v() {
                        D.rtl && I.addClass("rtl")
                    }

                    function w(t, e) {
                        if (t.preventDuplicates) {
                            if (e.message === b) return !0;
                            b = e.message
                        }
                        return !1
                    }

                    function x(e) {
                        var i = e && !1 !== D.closeMethod ? D.closeMethod : D.hideMethod,
                            n = e && !1 !== D.closeDuration ? D.closeDuration : D.hideDuration,
                            s = e && !1 !== D.closeEasing ? D.closeEasing : D.hideEasing;
                        if (!t(":focus", I).length || e) return clearTimeout(N.intervalId), I[i]({
                            duration: n,
                            easing: s,
                            complete: function() {
                                g(I), clearTimeout(T), D.onHidden && "hidden" !== F.state && D.onHidden(), F.state = "hidden", F.endTime = new Date, p(F)
                            }
                        })
                    }

                    function $() {
                        (D.timeOut > 0 || D.extendedTimeOut > 0) && (T = setTimeout(x, D.extendedTimeOut), N.maxHideTime = parseFloat(D.extendedTimeOut), N.hideEta = (new Date).getTime() + N.maxHideTime)
                    }

                    function k() {
                        clearTimeout(T), N.hideEta = 0, I.stop(!0, !0)[D.showMethod]({
                            duration: D.showDuration,
                            easing: D.showEasing
                        })
                    }

                    function C() {
                        var t = (N.hideEta - (new Date).getTime()) / N.maxHideTime * 100;
                        M.width(t + "%")
                    }
                    var D = m(),
                        S = e.iconClass || D.iconClass;
                    if ("undefined" != typeof e.optionsOverride && (D = t.extend(D, e.optionsOverride), S = e.optionsOverride.iconClass || S), !w(D, e)) {
                        y++, _ = i(D, !0);
                        var T = null,
                            I = t("<div/>"),
                            A = t("<div/>"),
                            E = t("<div/>"),
                            M = t("<div/>"),
                            P = t(D.closeHtml),
                            N = {
                                intervalId: null,
                                hideEta: null,
                                maxHideTime: null
                            },
                            F = {
                                toastId: y,
                                state: "visible",
                                startTime: new Date,
                                options: D,
                                map: e
                            };
                        return s(), r(), o(), p(F), D.debug && console && console.log(F), I
                    }
                }

                function m() {
                    return t.extend({}, h(), x.options)
                }

                function g(t) {
                    _ || (_ = i()), t.is(":visible") || (t.remove(), t = null, 0 === _.children().length && (_.remove(), b = undefined))
                }
                var _, v, b, y = 0,
                    w = {
                        error: "error",
                        info: "info",
                        success: "success",
                        warning: "warning"
                    },
                    x = {
                        clear: r,
                        remove: l,
                        error: e,
                        getContainer: i,
                        info: n,
                        options: {},
                        subscribe: s,
                        success: a,
                        version: "2.1.3",
                        warning: o
                    };
                return x
            }()
        })
    }("function" == typeof define && define.amd ? define : function(t, e) {
        "undefined" != typeof module && module.exports ? module.exports = e(require("jquery")) : window.toastr = e(window.jQuery)
    }), toastr.options = {
        closeButton: !0,
        debug: !1,
        progressBar: !0,
        positionClass: "toast-top-right",
        showDuration: "300",
        hideDuration: "1000",
        timeOut: "5000",
        extendedTimeOut: "1000",
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    };
var showToast = function(t) {
    for (var e = 0; e < t.length; e++) {
        var i = t[e],
            n = {
                notice: "success",
                success: "success",
                alert: "error",
                error: "error",
                warning: "warning",
                info: "info"
            },
            s = {
                notice: {},
                alert: {
                    timeOut: "0",
                    extendedTimeOut: "0"
                },
                warning: {
                    timeOut: "0",
                    extendedTimeOut: "0"
                },
                info: {}
            };
        try {
            toastr[n[i[0]]](i[1], "", s[i[0]])
        } catch (t) {
            toastr.info(i[1], "", s[i[0]])
        }
    }
};
! function(t) {
    "use strict";
    t.ajaxPrefilter(function(t) {
        if (t.iframe) return "iframe"
    }), t.ajaxTransport("iframe", function(e, i, n) {
        function s() {
            u.each(function(e) {
                t(this).replaceWith(c[e]), u.splice(e, 1)
            }), o.remove(), r.bind("load", function() {
                r.remove()
            }), r.attr("src", "about:blank")
        }
        var a, o = null,
            r = null,
            l = "iframe-" + t.now(),
            c = t(e.files).filter(":file:enabled"),
            u = null;
        if (e.dataTypes.shift(), c.length) return o = t("<form enctype='multipart/form-data' method='post'></form>").hide().attr({
            action: e.url,
            target: l
        }), "string" == typeof e.data && e.data.length > 0 && t.error("data must not be serialized"), t.each(e.data || {}, function(e, i) {
            t.isPlainObject(i) && (e = i.name, i = i.value), t("<input type='hidden' />").attr({
                name: e,
                value: i
            }).appendTo(o)
        }), t("<input type='hidden' value='IFrame' name='X-Requested-With' />").appendTo(o), a = e.dataTypes[0] && e.accepts[e.dataTypes[0]] ? e.accepts[e.dataTypes[0]] + ("*" !== e.dataTypes[0] ? ", */*; q=0.01" : "") : e.accepts["*"], t("<input type='hidden' name='X-Http-Accept'>").attr("value", a).appendTo(o), u = c.after(function() {
            return t(this).clone().prop("disabled", !0)
        }).next(), c.appendTo(o), {
            send: function(e, i) {
                r = t("<iframe src='about:blank' name='" + l + "' id='" + l + "' style='display:none'></iframe>"), r.bind("load", function() {
                    r.unbind("load").bind("load", function() {
                        var t = this.contentWindow ? this.contentWindow.document : this.contentDocument ? this.contentDocument : this.document,
                            e = t.documentElement ? t.documentElement : t.body,
                            a = e.getElementsByTagName("textarea")[0],
                            o = a && a.getAttribute("data-type") || null,
                            r = a && a.getAttribute("data-status") || 200,
                            l = a && a.getAttribute("data-statusText") || "OK",
                            c = {
                                html: e.innerHTML,
                                text: o ? a.value : e ? e.textContent || e.innerText : null
                            };
                        s(), n.responseText || (n.responseText = c.text), i(r, l, c, o ? "Content-Type: " + o : null)
                    }), o[0].submit()
                }), t("body").append(o, r)
            },
            abort: function() {
                null !== r && (r.unbind("load").attr("src", "javascript:false;"), s())
            }
        }
    })
}(jQuery),
function(t) {
    var e;
    t.remotipart = e = {
        setup: function(i) {
            var n = i.data("ujs:submit-button"),
                s = t('meta[name="csrf-param"]').attr("content"),
                a = t('meta[name="csrf-token"]').attr("content"),
                o = i.find('input[name="' + s + '"]').length;
            i.one("ajax:beforeSend.remotipart", function(r, l, c) {
                return delete c.beforeSend, c.iframe = !0, c.files = t(t.rails.fileInputSelector, i), c.data = i.serializeArray(), n && c.data.push(n), c.files.each(function(t, e) {
                    for (var i = c.data.length - 1; i >= 0; i--) c.data[i].name == e.name && c.data.splice(i, 1)
                }), c.processData = !1, c.dataType === undefined && (c.dataType = "script *"), c.data.push({
                    name: "remotipart_submitted",
                    value: !0
                }), a && s && !o && c.data.push({
                    name: s,
                    value: a
                }), t.rails.fire(i, "ajax:remotipartSubmit", [l, c]) && (t.rails.ajax(c).complete(function(e) {
                    t.rails.fire(i, "ajax:remotipartComplete", [e])
                }), setTimeout(function() {
                    t.rails.disableFormElements(i)
                }, 20)), e.teardown(i), !1
            }).data("remotipartSubmitted", !0)
        },
        teardown: function(t) {
            t.unbind("ajax:beforeSend.remotipart").removeData("remotipartSubmitted")
        }
    }, t(document).on("ajax:aborted:file", "form", function() {
        var i = t(this);
        return e.setup(i), t.rails.handleRemote(i), !1
    })
}(jQuery);
var BinaryFile = function(t, e, i) {
        var n = t,
            s = e || 0,
            a = 0;
        this.getRawData = function() {
            return n
        }, "string" == typeof t ? (a = i || n.length, this.getByteAt = function(t) {
            return 255 & n.charCodeAt(t + s)
        }, this.getBytesAt = function(t, e) {
            for (var i = [], a = 0; a < e; a++) i[a] = 255 & n.charCodeAt(t + a + s);
            return i
        }) : "unknown" == typeof t && (a = i || IEBinary_getLength(n), this.getByteAt = function(t) {
            return IEBinary_getByteAt(n, t + s)
        }, this.getBytesAt = function(t, e) {
            return new VBArray(IEBinary_getBytesAt(n, t + s, e)).toArray()
        }), this.getLength = function() {
            return a
        }, this.getSByteAt = function(t) {
            var e = this.getByteAt(t);
            return e > 127 ? e - 256 : e
        }, this.getShortAt = function(t, e) {
            var i = e ? (this.getByteAt(t) << 8) + this.getByteAt(t + 1) : (this.getByteAt(t + 1) << 8) + this.getByteAt(t);
            return i < 0 && (i += 65536), i
        }, this.getSShortAt = function(t, e) {
            var i = this.getShortAt(t, e);
            return i > 32767 ? i - 65536 : i
        }, this.getLongAt = function(t, e) {
            var i = this.getByteAt(t),
                n = this.getByteAt(t + 1),
                s = this.getByteAt(t + 2),
                a = this.getByteAt(t + 3),
                o = e ? (((i << 8) + n << 8) + s << 8) + a : (((a << 8) + s << 8) + n << 8) + i;
            return o < 0 && (o += 4294967296), o
        }, this.getSLongAt = function(t, e) {
            var i = this.getLongAt(t, e);
            return i > 2147483647 ? i - 4294967296 : i
        }, this.getStringAt = function(t, e) {
            for (var i = [], n = this.getBytesAt(t, e), s = 0; s < e; s++) i[s] = String.fromCharCode(n[s]);
            return i.join("")
        }, this.getCharAt = function(t) {
            return String.fromCharCode(this.getByteAt(t))
        }, this.toBase64 = function() {
            return window.btoa(n)
        }, this.fromBase64 = function(t) {
            n = window.atob(t)
        }
    },
    BinaryAjax = function() {
        function t() {
            var t = null;
            return window.ActiveXObject ? t = new ActiveXObject("Microsoft.XMLHTTP") : window.XMLHttpRequest && (t = new XMLHttpRequest), t
        }

        function e(e, i, n) {
            var s = t();
            s ? (i && ("undefined" != typeof s.onload ? s.onload = function() {
                "200" == s.status ? i(this) : n && n(), s = null
            } : s.onreadystatechange = function() {
                4 == s.readyState && ("200" == s.status ? i(this) : n && n(), s = null)
            }), s.open("HEAD", e, !0), s.send(null)) : n && n()
        }

        function i(e, i, n, s, a, o) {
            var r = t();
            if (r) {
                var l = 0;
                s && !a && (l = s[0]);
                var c = 0;
                s && (c = s[1] - s[0] + 1), i && ("undefined" != typeof r.onload ? r.onload = function() {
                    "200" == r.status || "206" == r.status || "0" == r.status ? (r.binaryResponse = new BinaryFile(r.responseText, l, c), r.fileSize = o || r.getResponseHeader("Content-Length"), i(r)) : n && n(), r = null
                } : r.onreadystatechange = function() {
                    if (4 == r.readyState) {
                        if ("200" == r.status || "206" == r.status || "0" == r.status) {
                            var t = {
                                status: r.status,
                                binaryResponse: new BinaryFile("unknown" == typeof r.responseBody ? r.responseBody : r.responseText, l, c),
                                fileSize: o || r.getResponseHeader("Content-Length")
                            };
                            i(t)
                        } else n && n();
                        r = null
                    }
                }), r.open("GET", e, !0), r.overrideMimeType && r.overrideMimeType("text/plain; charset=x-user-defined"), s && a && r.setRequestHeader("Range", "bytes=" + s[0] + "-" + s[1]), r.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 1970 00:00:00 GMT"), r.send(null)
            } else n && n()
        }
        return function(t, n, s, a) {
            a ? e(t, function(e) {
                var o, r, l = parseInt(e.getResponseHeader("Content-Length"), 10),
                    c = e.getResponseHeader("Accept-Ranges");
                o = a[0], a[0] < 0 && (o += l), r = o + a[1] - 1, i(t, n, s, [o, r], "bytes" == c, l)
            }) : i(t, n, s)
        }
    }();
document.write("<script type='text/vbscript'>\r\nFunction IEBinary_getByteAt(strBinary, iOffset)\r\n\tIEBinary_getByteAt = AscB(MidB(strBinary, iOffset + 1, 1))\r\nEnd Function\r\nFunction IEBinary_getBytesAt(strBinary, iOffset, iLength)\r\n  Dim aBytes()\r\n  ReDim aBytes(iLength - 1)\r\n  For i = 0 To iLength - 1\r\n   aBytes(i) = IEBinary_getByteAt(strBinary, iOffset + i)\r\n  Next\r\n  IEBinary_getBytesAt = aBytes\r\nEnd Function\r\nFunction IEBinary_getLength(strBinary)\r\n\tIEBinary_getLength = LenB(strBinary)\r\nEnd Function\r\n</script>\r\n"),
    function(t) {
        function e(t, e) {
            this.file = t, this.options = n.extend({}, s, e), this._defaults = s, this._name = i, this.init()
        }
        var i = "canvasResize",
            n = {
                newsize: function(t, e, i, n, s) {
                    var a = s ? "h" : "";
                    if (i && t > i || n && e > n) {
                        var o = t / e;
                        (o >= 1 || 0 === n) && i && !s ? (t = i, e = i / o >> 0) : s && o <= i / n ? (t = i, e = i / o >> 0, a = "w") : (t = n * o >> 0, e = n)
                    }
                    return {
                        width: t,
                        height: e,
                        cropped: a
                    }
                },
                dataURLtoBlob: function(t) {
                    for (var e = t.split(",")[0].split(":")[1].split(";")[0], i = atob(t.split(",")[1]), n = new ArrayBuffer(i.length), s = new Uint8Array(n), a = 0; a < i.length; a++) s[a] = i.charCodeAt(a);
                    var o = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder;
                    return o ? (o = new(window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder), o.append(n), o.getBlob(e)) : o = new Blob([n], {
                        type: e
                    })
                },
                detectSubsampling: function(t) {
                    var e = t.width;
                    if (e * t.height > 1048576) {
                        var i = document.createElement("canvas");
                        i.width = i.height = 1;
                        var n = i.getContext("2d");
                        return n.drawImage(t, 1 - e, 0), 0 === n.getImageData(0, 0, 1, 1).data[3]
                    }
                    return !1
                },
                rotate: function(t, e) {
                    var i = {
                        1: {
                            90: 6,
                            180: 3,
                            270: 8
                        },
                        2: {
                            90: 7,
                            180: 4,
                            270: 5
                        },
                        3: {
                            90: 8,
                            180: 1,
                            270: 6
                        },
                        4: {
                            90: 5,
                            180: 2,
                            270: 7
                        },
                        5: {
                            90: 2,
                            180: 7,
                            270: 4
                        },
                        6: {
                            90: 3,
                            180: 8,
                            270: 1
                        },
                        7: {
                            90: 4,
                            180: 5,
                            270: 2
                        },
                        8: {
                            90: 1,
                            180: 6,
                            270: 3
                        }
                    };
                    return i[t][e] ? i[t][e] : t
                },
                transformCoordinate: function(t, e, i, n) {
                    switch (n) {
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                            t.width = i, t.height = e;
                            break;
                        default:
                            t.width = e, t.height = i
                    }
                    var s = t.getContext("2d");
                    switch (n) {
                        case 1:
                            break;
                        case 2:
                            s.translate(e, 0), s.scale(-1, 1);
                            break;
                        case 3:
                            s.translate(e, i), s.rotate(Math.PI);
                            break;
                        case 4:
                            s.translate(0, i), s.scale(1, -1);
                            break;
                        case 5:
                            s.rotate(.5 * Math.PI), s.scale(1, -1);
                            break;
                        case 6:
                            s.rotate(.5 * Math.PI), s.translate(0, -i);
                            break;
                        case 7:
                            s.rotate(.5 * Math.PI), s.translate(e, -i), s.scale(-1, 1);
                            break;
                        case 8:
                            s.rotate(-.5 * Math.PI), s.translate(-e, 0)
                    }
                },
                detectVerticalSquash: function(t, e, i) {
                    var n = document.createElement("canvas");
                    n.width = 1, n.height = i;
                    var s = n.getContext("2d");
                    s.drawImage(t, 0, 0);
                    for (var a = s.getImageData(0, 0, 1, i).data, o = 0, r = i, l = i; l > o;) {
                        0 === a[4 * (l - 1) + 3] ? r = l : o = l, l = r + o >> 1
                    }
                    var c = l / i;
                    return 0 === c ? 1 : c
                },
                callback: function(t) {
                    return t
                },
                extend: function() {
                    var t = arguments[0] || {},
                        e = 1,
                        i = arguments.length,
                        s = !1;
                    t.constructor === Boolean && (s = t, t = arguments[1] || {}), 1 === i && (t = this, e = 0);
                    for (var a; e < i; e++)
                        if (null !== (a = arguments[e]))
                            for (var o in a) t !== a[o] && (s && "object" == typeof a[o] && t[o] ? n.extend(t[o], a[o]) : a[o] !== undefined && (t[o] = a[o]));
                    return t
                }
            },
            s = {
                width: 300,
                height: 0,
                crop: !1,
                quality: 80,
                rotate: 0,
                callback: n.callback
            };
        e.prototype = {
            init: function() {
                var t = this,
                    e = this.file,
                    i = new FileReader;
                i.onloadend = function(i) {
                    var s = i.target.result,
                        a = atob(s.split(",")[1]),
                        o = new BinaryFile(a, 0, a.length),
                        r = EXIF.readFromBinaryFile(o),
                        l = new Image;
                    l.onload = function() {
                        var i = r.Orientation || 1;
                        i = n.rotate(i, t.options.rotate);
                        var s = i >= 5 && i <= 8 ? n.newsize(l.height, l.width, t.options.width, t.options.height, t.options.crop) : n.newsize(l.width, l.height, t.options.width, t.options.height, t.options.crop),
                            a = l.width,
                            o = l.height,
                            c = s.width,
                            u = s.height,
                            d = document.createElement("canvas"),
                            h = d.getContext("2d");
                        h.save(), n.transformCoordinate(d, c, u, i), n.detectSubsampling(l) && (a /= 2, o /= 2);
                        var p = 1024,
                            f = document.createElement("canvas");
                        f.width = f.height = p;
                        for (var m = f.getContext("2d"), g = n.detectVerticalSquash(l, a, o), _ = 0; _ < o;) {
                            for (var v = _ + p > o ? o - _ : p, b = 0; b < a;) {
                                var y = b + p > a ? a - b : p;
                                m.clearRect(0, 0, p, p), m.drawImage(l, -b, -_);
                                var w = Math.floor(b * c / a),
                                    x = Math.ceil(y * c / a),
                                    $ = Math.floor(_ * u / o / g),
                                    k = Math.ceil(v * u / o / g);
                                h.drawImage(f, 0, 0, y, v, w, $, x, k), b += p
                            }
                            _ += p
                        }
                        h.restore(), f = m = null;
                        var C = document.createElement("canvas");
                        C.width = "h" === s.cropped ? u : c, C.height = "w" === s.cropped ? c : u;
                        var D = "h" === s.cropped ? .5 * (u - c) : 0,
                            S = "w" === s.cropped ? .5 * (c - u) : 0;
                        if (newctx = C.getContext("2d"), newctx.drawImage(d, D, S, c, u), console.log(e, e.type), "image/png" === e.type) var T = C.toDataURL(e.type);
                        else var T = C.toDataURL("image/jpeg", .01 * t.options.quality);
                        t.options.callback(T, C.width, C.height)
                    }, l.src = s
                }, i.readAsDataURL(e)
            }
        }, t[i] = function(t, i) {
            if ("string" == typeof t) return n[t](i);
            new e(t, i)
        }
    }(window);
var EXIF = function() {
    function t(t) {
        return !!t.exifdata
    }

    function e(t, e) {
        BinaryAjax(t.src, function(n) {
            var s = i(n.binaryResponse);
            t.exifdata = s || {}, e && e.call(t)
        })
    }

    function i(t) {
        if (255 != t.getByteAt(0) || 216 != t.getByteAt(1)) return !1;
        for (var e, i = 2, n = t.getLength(); i < n;) {
            if (255 != t.getByteAt(i)) return d && console.log("Not a valid marker at offset " + i + ", found: " + t.getByteAt(i)), !1;
            if (22400 == (e = t.getByteAt(i + 1))) return d && console.log("Found 0xFFE1 marker"), a(t, i + 4, t.getShortAt(i + 2, !0) - 2);
            if (225 == e) return d && console.log("Found 0xFFE1 marker"), a(t, i + 4, t.getShortAt(i + 2, !0) - 2);
            i += 2 + t.getShortAt(i + 2, !0)
        }
    }

    function n(t, e, i, n, a) {
        var o, r, l, c = t.getShortAt(i, a),
            u = {};
        for (l = 0; l < c; l++) o = i + 12 * l + 2, r = n[t.getShortAt(o, a)], !r && d && console.log("Unknown tag: " + t.getShortAt(o, a)), u[r] = s(t, o, e, i, a);
        return u
    }

    function s(t, e, i, n, s) {
        var a, o, r, l, c, u, d = t.getShortAt(e + 2, s),
            h = t.getLongAt(e + 4, s),
            p = t.getLongAt(e + 8, s) + i;
        switch (d) {
            case 1:
            case 7:
                if (1 == h) return t.getByteAt(e + 8, s);
                for (a = h > 4 ? p : e + 8, o = [], l = 0; l < h; l++) o[l] = t.getByteAt(a + l);
                return o;
            case 2:
                return a = h > 4 ? p : e + 8, t.getStringAt(a, h - 1);
            case 3:
                if (1 == h) return t.getShortAt(e + 8, s);
                for (a = h > 2 ? p : e + 8, o = [], l = 0; l < h; l++) o[l] = t.getShortAt(a + 2 * l, s);
                return o;
            case 4:
                if (1 == h) return t.getLongAt(e + 8, s);
                o = [];
                for (var l = 0; l < h; l++) o[l] = t.getLongAt(p + 4 * l, s);
                return o;
            case 5:
                if (1 == h) return c = t.getLongAt(p, s), u = t.getLongAt(p + 4, s), r = new Number(c / u), r.numerator = c, r.denominator = u, r;
                for (o = [], l = 0; l < h; l++) c = t.getLongAt(p + 8 * l, s), u = t.getLongAt(p + 4 + 8 * l, s), o[l] = new Number(c / u), o[l].numerator = c, o[l].denominator = u;
                return o;
            case 9:
                if (1 == h) return t.getSLongAt(e + 8, s);
                for (o = [], l = 0; l < h; l++) o[l] = t.getSLongAt(p + 4 * l, s);
                return o;
            case 10:
                if (1 == h) return t.getSLongAt(p, s) / t.getSLongAt(p + 4, s);
                for (o = [], l = 0; l < h; l++) o[l] = t.getSLongAt(p + 8 * l, s) / t.getSLongAt(p + 4 + 8 * l, s);
                return o
        }
    }

    function a(t, e) {
        if ("Exif" != t.getStringAt(e, 4)) return d && console.log("Not valid EXIF data! " + t.getStringAt(e, 4)), !1;
        var i, s, a, o, r, l = e + 6;
        if (18761 == t.getShortAt(l)) i = !1;
        else {
            if (19789 != t.getShortAt(l)) return d && console.log("Not valid TIFF data! (no 0x4949 or 0x4D4D)"), !1;
            i = !0
        }
        if (42 != t.getShortAt(l + 2, i)) return d && console.log("Not valid TIFF data! (no 0x002A)"), !1;
        if (8 != t.getLongAt(l + 4, i)) return d && console.log("Not valid TIFF data! (First offset not 8)", t.getShortAt(l + 4, i)), !1;
        if (s = n(t, l, l + 8, p, i), s.ExifIFDPointer) {
            o = n(t, l, l + s.ExifIFDPointer, h, i);
            for (a in o) {
                switch (a) {
                    case "LightSource":
                    case "Flash":
                    case "MeteringMode":
                    case "ExposureProgram":
                    case "SensingMethod":
                    case "SceneCaptureType":
                    case "SceneType":
                    case "CustomRendered":
                    case "WhiteBalance":
                    case "GainControl":
                    case "Contrast":
                    case "Saturation":
                    case "Sharpness":
                    case "SubjectDistanceRange":
                    case "FileSource":
                        o[a] = m[a][o[a]];
                        break;
                    case "ExifVersion":
                    case "FlashpixVersion":
                        o[a] = String.fromCharCode(o[a][0], o[a][1], o[a][2], o[a][3]);
                        break;
                    case "ComponentsConfiguration":
                        o[a] = m.Components[o[a][0]] + m.Components[o[a][1]] + m.Components[o[a][2]] + m.Components[o[a][3]]
                }
                s[a] = o[a]
            }
        }
        if (s.GPSInfoIFDPointer) {
            r = n(t, l, l + s.GPSInfoIFDPointer, f, i);
            for (a in r) {
                switch (a) {
                    case "GPSVersionID":
                        r[a] = r[a][0] + "." + r[a][1] + "." + r[a][2] + "." + r[a][3]
                }
                s[a] = r[a]
            }
        }
        return s
    }

    function o(i, n) {
        return !!i.complete && (t(i) ? n && n.call(i) : e(i, n), !0)
    }

    function r(e, i) {
        if (t(e)) return e.exifdata[i]
    }

    function l(e) {
        if (!t(e)) return {};
        var i, n = e.exifdata,
            s = {};
        for (i in n) n.hasOwnProperty(i) && (s[i] = n[i]);
        return s
    }

    function c(e) {
        if (!t(e)) return "";
        var i, n = e.exifdata,
            s = "";
        for (i in n) n.hasOwnProperty(i) && ("object" == typeof n[i] ? n[i] instanceof Number ? s += i + " : " + n[i] + " [" + n[i].numerator + "/" + n[i].denominator + "]\r\n" : s += i + " : [" + n[i].length + " values]\r\n" : s += i + " : " + n[i] + "\r\n");
        return s
    }

    function u(t) {
        return i(t)
    }
    var d = !1,
        h = {
            36864: "ExifVersion",
            40960: "FlashpixVersion",
            40961: "ColorSpace",
            40962: "PixelXDimension",
            40963: "PixelYDimension",
            37121: "ComponentsConfiguration",
            37122: "CompressedBitsPerPixel",
            37500: "MakerNote",
            37510: "UserComment",
            40964: "RelatedSoundFile",
            36867: "DateTimeOriginal",
            36868: "DateTimeDigitized",
            37520: "SubsecTime",
            37521: "SubsecTimeOriginal",
            37522: "SubsecTimeDigitized",
            33434: "ExposureTime",
            33437: "FNumber",
            34850: "ExposureProgram",
            34852: "SpectralSensitivity",
            34855: "ISOSpeedRatings",
            34856: "OECF",
            37377: "ShutterSpeedValue",
            37378: "ApertureValue",
            37379: "BrightnessValue",
            37380: "ExposureBias",
            37381: "MaxApertureValue",
            37382: "SubjectDistance",
            37383: "MeteringMode",
            37384: "LightSource",
            37385: "Flash",
            37396: "SubjectArea",
            37386: "FocalLength",
            41483: "FlashEnergy",
            41484: "SpatialFrequencyResponse",
            41486: "FocalPlaneXResolution",
            41487: "FocalPlaneYResolution",
            41488: "FocalPlaneResolutionUnit",
            41492: "SubjectLocation",
            41493: "ExposureIndex",
            41495: "SensingMethod",
            41728: "FileSource",
            41729: "SceneType",
            41730: "CFAPattern",
            41985: "CustomRendered",
            41986: "ExposureMode",
            41987: "WhiteBalance",
            41988: "DigitalZoomRation",
            41989: "FocalLengthIn35mmFilm",
            41990: "SceneCaptureType",
            41991: "GainControl",
            41992: "Contrast",
            41993: "Saturation",
            41994: "Sharpness",
            41995: "DeviceSettingDescription",
            41996: "SubjectDistanceRange",
            40965: "InteroperabilityIFDPointer",
            42016: "ImageUniqueID"
        },
        p = {
            256: "ImageWidth",
            257: "ImageHeight",
            34665: "ExifIFDPointer",
            34853: "GPSInfoIFDPointer",
            40965: "InteroperabilityIFDPointer",
            258: "BitsPerSample",
            259: "Compression",
            262: "PhotometricInterpretation",
            274: "Orientation",
            277: "SamplesPerPixel",
            284: "PlanarConfiguration",
            530: "YCbCrSubSampling",
            531: "YCbCrPositioning",
            282: "XResolution",
            283: "YResolution",
            296: "ResolutionUnit",
            273: "StripOffsets",
            278: "RowsPerStrip",
            279: "StripByteCounts",
            513: "JPEGInterchangeFormat",
            514: "JPEGInterchangeFormatLength",
            301: "TransferFunction",
            318: "WhitePoint",
            319: "PrimaryChromaticities",
            529: "YCbCrCoefficients",
            532: "ReferenceBlackWhite",
            306: "DateTime",
            270: "ImageDescription",
            271: "Make",
            272: "Model",
            305: "Software",
            315: "Artist",
            33432: "Copyright"
        },
        f = {
            0: "GPSVersionID",
            1: "GPSLatitudeRef",
            2: "GPSLatitude",
            3: "GPSLongitudeRef",
            4: "GPSLongitude",
            5: "GPSAltitudeRef",
            6: "GPSAltitude",
            7: "GPSTimeStamp",
            8: "GPSSatellites",
            9: "GPSStatus",
            10: "GPSMeasureMode",
            11: "GPSDOP",
            12: "GPSSpeedRef",
            13: "GPSSpeed",
            14: "GPSTrackRef",
            15: "GPSTrack",
            16: "GPSImgDirectionRef",
            17: "GPSImgDirection",
            18: "GPSMapDatum",
            19: "GPSDestLatitudeRef",
            20: "GPSDestLatitude",
            21: "GPSDestLongitudeRef",
            22: "GPSDestLongitude",
            23: "GPSDestBearingRef",
            24: "GPSDestBearing",
            25: "GPSDestDistanceRef",
            26: "GPSDestDistance",
            27: "GPSProcessingMethod",
            28: "GPSAreaInformation",
            29: "GPSDateStamp",
            30: "GPSDifferential"
        },
        m = {
            ExposureProgram: {
                0: "Not defined",
                1: "Manual",
                2: "Normal program",
                3: "Aperture priority",
                4: "Shutter priority",
                5: "Creative program",
                6: "Action program",
                7: "Portrait mode",
                8: "Landscape mode"
            },
            MeteringMode: {
                0: "Unknown",
                1: "Average",
                2: "CenterWeightedAverage",
                3: "Spot",
                4: "MultiSpot",
                5: "Pattern",
                6: "Partial",
                255: "Other"
            },
            LightSource: {
                0: "Unknown",
                1: "Daylight",
                2: "Fluorescent",
                3: "Tungsten (incandescent light)",
                4: "Flash",
                9: "Fine weather",
                10: "Cloudy weather",
                11: "Shade",
                12: "Daylight fluorescent (D 5700 - 7100K)",
                13: "Day white fluorescent (N 4600 - 5400K)",
                14: "Cool white fluorescent (W 3900 - 4500K)",
                15: "White fluorescent (WW 3200 - 3700K)",
                17: "Standard light A",
                18: "Standard light B",
                19: "Standard light C",
                20: "D55",
                21: "D65",
                22: "D75",
                23: "D50",
                24: "ISO studio tungsten",
                255: "Other"
            },
            Flash: {
                0: "Flash did not fire",
                1: "Flash fired",
                5: "Strobe return light not detected",
                7: "Strobe return light detected",
                9: "Flash fired, compulsory flash mode",
                13: "Flash fired, compulsory flash mode, return light not detected",
                15: "Flash fired, compulsory flash mode, return light detected",
                16: "Flash did not fire, compulsory flash mode",
                24: "Flash did not fire, auto mode",
                25: "Flash fired, auto mode",
                29: "Flash fired, auto mode, return light not detected",
                31: "Flash fired, auto mode, return light detected",
                32: "No flash function",
                65: "Flash fired, red-eye reduction mode",
                69: "Flash fired, red-eye reduction mode, return light not detected",
                71: "Flash fired, red-eye reduction mode, return light detected",
                73: "Flash fired, compulsory flash mode, red-eye reduction mode",
                77: "Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected",
                79: "Flash fired, compulsory flash mode, red-eye reduction mode, return light detected",
                89: "Flash fired, auto mode, red-eye reduction mode",
                93: "Flash fired, auto mode, return light not detected, red-eye reduction mode",
                95: "Flash fired, auto mode, return light detected, red-eye reduction mode"
            },
            SensingMethod: {
                1: "Not defined",
                2: "One-chip color area sensor",
                3: "Two-chip color area sensor",
                4: "Three-chip color area sensor",
                5: "Color sequential area sensor",
                7: "Trilinear sensor",
                8: "Color sequential linear sensor"
            },
            SceneCaptureType: {
                0: "Standard",
                1: "Landscape",
                2: "Portrait",
                3: "Night scene"
            },
            SceneType: {
                1: "Directly photographed"
            },
            CustomRendered: {
                0: "Normal process",
                1: "Custom process"
            },
            WhiteBalance: {
                0: "Auto white balance",
                1: "Manual white balance"
            },
            GainControl: {
                0: "None",
                1: "Low gain up",
                2: "High gain up",
                3: "Low gain down",
                4: "High gain down"
            },
            Contrast: {
                0: "Normal",
                1: "Soft",
                2: "Hard"
            },
            Saturation: {
                0: "Normal",
                1: "Low saturation",
                2: "High saturation"
            },
            Sharpness: {
                0: "Normal",
                1: "Soft",
                2: "Hard"
            },
            SubjectDistanceRange: {
                0: "Unknown",
                1: "Macro",
                2: "Close view",
                3: "Distant view"
            },
            FileSource: {
                3: "DSC"
            },
            Components: {
                0: "",
                1: "Y",
                2: "Cb",
                3: "Cr",
                4: "R",
                5: "G",
                6: "B"
            }
        };
    return {
        readFromBinaryFile: u,
        pretty: c,
        getTag: r,
        getAllTags: l,
        getData: o,
        Tags: h,
        TiffTags: p,
        GPSTags: f,
        StringValues: m
    }
}();
if (function() {
        var t, e, i, n, s, a, o, r, l = [].slice,
            c = {}.hasOwnProperty,
            u = function(t, e) {
                function i() {
                    this.constructor = t
                }
                for (var n in e) c.call(e, n) && (t[n] = e[n]);
                return i.prototype = e.prototype, t.prototype = new i, t.__super__ = e.prototype, t
            };
        o = function() {}, e = function() {
            function t() {}
            return t.prototype.addEventListener = t.prototype.on, t.prototype.on = function(t, e) {
                return this._callbacks = this._callbacks || {}, this._callbacks[t] || (this._callbacks[t] = []), this._callbacks[t].push(e), this
            }, t.prototype.emit = function() {
                var t, e, i, n, s, a;
                if (n = arguments[0], t = 2 <= arguments.length ? l.call(arguments, 1) : [], this._callbacks = this._callbacks || {}, i = this._callbacks[n])
                    for (s = 0, a = i.length; s < a; s++) e = i[s], e.apply(this, t);
                return this
            }, t.prototype.removeListener = t.prototype.off, t.prototype.removeAllListeners = t.prototype.off, t.prototype.removeEventListener = t.prototype.off, t.prototype.off = function(t, e) {
                var i, n, s, a;
                if (!this._callbacks || 0 === arguments.length) return this._callbacks = {}, this;
                if (!(i = this._callbacks[t])) return this;
                if (1 === arguments.length) return delete this._callbacks[t], this;
                for (n = s = 0, a = i.length; s < a; n = ++s)
                    if (i[n] === e) {
                        i.splice(n, 1);
                        break
                    }
                return this
            }, t
        }(), t = function(t) {
            function i(t, e) {
                var s, a, o;
                if (this.element = t, this.version = i.version, this.defaultOptions.previewTemplate = this.defaultOptions.previewTemplate.replace(/\n*/g, ""), this.clickableElements = [], this.listeners = [], this.files = [], "string" == typeof this.element && (this.element = document.querySelector(this.element)), !this.element || null == this.element.nodeType) throw new Error("Invalid dropzone element.");
                if (this.element.dropzone) throw new Error("Dropzone already attached.");
                if (i.instances.push(this), this.element.dropzone = this, s = null != (o = i.optionsForElement(this.element)) ? o : {}, this.options = n({}, this.defaultOptions, s, null != e ? e : {}), this.options.forceFallback || !i.isBrowserSupported()) return this.options.fallback.call(this);
                if (null == this.options.url && (this.options.url = this.element.getAttribute("action")), !this.options.url) throw new Error("No URL provided.");
                if (this.options.acceptedFiles && this.options.acceptedMimeTypes) throw new Error("You can't provide both 'acceptedFiles' and 'acceptedMimeTypes'. 'acceptedMimeTypes' is deprecated.");
                this.options.acceptedMimeTypes && (this.options.acceptedFiles = this.options.acceptedMimeTypes, delete this.options.acceptedMimeTypes), this.options.method = this.options.method.toUpperCase(), (a = this.getExistingFallback()) && a.parentNode && a.parentNode.removeChild(a), !1 !== this.options.previewsContainer && (this.options.previewsContainer ? this.previewsContainer = i.getElement(this.options.previewsContainer, "previewsContainer") : this.previewsContainer = this.element), this.options.clickable && (!0 === this.options.clickable ? this.clickableElements = [this.element] : this.clickableElements = i.getElements(this.options.clickable, "clickable")), this.init()
            }
            var n, s;
            return u(i, t), i.prototype.Emitter = e, i.prototype.events = ["drop", "dragstart", "dragend", "dragenter", "dragover", "dragleave", "addedfile", "addedfiles", "removedfile", "thumbnail", "error", "errormultiple", "processing", "processingmultiple", "uploadprogress", "totaluploadprogress", "sending", "sendingmultiple", "success", "successmultiple", "canceled", "canceledmultiple", "complete", "completemultiple", "reset", "maxfilesexceeded", "maxfilesreached", "queuecomplete"], i.prototype.defaultOptions = {
                url: null,
                method: "post",
                withCredentials: !1,
                parallelUploads: 2,
                uploadMultiple: !1,
                maxFilesize: 256,
                paramName: "file",
                createImageThumbnails: !0,
                maxThumbnailFilesize: 10,
                thumbnailWidth: 120,
                thumbnailHeight: 120,
                filesizeBase: 1e3,
                maxFiles: null,
                params: {},
                clickable: !0,
                ignoreHiddenFiles: !0,
                acceptedFiles: null,
                acceptedMimeTypes: null,
                autoProcessQueue: !0,
                autoQueue: !0,
                addRemoveLinks: !1,
                previewsContainer: null,
                hiddenInputContainer: "body",
                capture: null,
                renameFilename: null,
                dictDefaultMessage: "Drop files here to upload",
                dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.",
                dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
                dictFileTooBig: "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.",
                dictInvalidFileType: "You can't upload files of this type.",
                dictResponseError: "Server responded with {{statusCode}} code.",
                dictCancelUpload: "Cancel upload",
                dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?",
                dictRemoveFile: "Remove file",
                dictRemoveFileConfirmation: null,
                dictMaxFilesExceeded: "You can not upload any more files.",
                accept: function(t, e) {
                    return e()
                },
                init: function() {
                    return o
                },
                forceFallback: !1,
                fallback: function() {
                    var t, e, n, s, a, o;
                    for (this.element.className = this.element.className + " dz-browser-not-supported", o = this.element.getElementsByTagName("div"), s = 0, a = o.length; s < a; s++) t = o[s], /(^| )dz-message($| )/.test(t.className) && (e = t, t.className = "dz-message");
                    return e || (e = i.createElement('<div class="dz-message"><span></span></div>'), this.element.appendChild(e)), n = e.getElementsByTagName("span")[0], n && (null != n.textContent ? n.textContent = this.options.dictFallbackMessage : null != n.innerText && (n.innerText = this.options.dictFallbackMessage)), this.element.appendChild(this.getFallbackForm())
                },
                resize: function(t) {
                    var e, i, n;
                    return e = {
                        srcX: 0,
                        srcY: 0,
                        srcWidth: t.width,
                        srcHeight: t.height
                    }, i = t.width / t.height, e.optWidth = this.options.thumbnailWidth, e.optHeight = this.options.thumbnailHeight, null == e.optWidth && null == e.optHeight ? (e.optWidth = e.srcWidth, e.optHeight = e.srcHeight) : null == e.optWidth ? e.optWidth = i * e.optHeight : null == e.optHeight && (e.optHeight = 1 / i * e.optWidth), n = e.optWidth / e.optHeight, t.height < e.optHeight || t.width < e.optWidth ? (e.trgHeight = e.srcHeight, e.trgWidth = e.srcWidth) : i > n ? (e.srcHeight = t.height, e.srcWidth = e.srcHeight * n) : (e.srcWidth = t.width, e.srcHeight = e.srcWidth / n), e.srcX = (t.width - e.srcWidth) / 2, e.srcY = (t.height - e.srcHeight) / 2, e
                },
                drop: function() {
                    return this.element.classList.remove("dz-drag-hover")
                },
                dragstart: o,
                dragend: function() {
                    return this.element.classList.remove("dz-drag-hover")
                },
                dragenter: function() {
                    return this.element.classList.add("dz-drag-hover")
                },
                dragover: function() {
                    return this.element.classList.add("dz-drag-hover")
                },
                dragleave: function() {
                    return this.element.classList.remove("dz-drag-hover")
                },
                paste: o,
                reset: function() {
                    return this.element.classList.remove("dz-started")
                },
                addedfile: function(t) {
                    var e, n, s, a, o, r, l, c, u, d, h, p, f;
                    if (this.element === this.previewsContainer && this.element.classList.add("dz-started"), this.previewsContainer) {
                        for (t.previewElement = i.createElement(this.options.previewTemplate.trim()), t.previewTemplate = t.previewElement,
                            this.previewsContainer.appendChild(t.previewElement), d = t.previewElement.querySelectorAll("[data-dz-name]"), a = 0, l = d.length; a < l; a++) e = d[a], e.textContent = this._renameFilename(t.name);
                        for (h = t.previewElement.querySelectorAll("[data-dz-size]"), o = 0, c = h.length; o < c; o++) e = h[o], e.innerHTML = this.filesize(t.size);
                        for (this.options.addRemoveLinks && (t._removeLink = i.createElement('<a class="dz-remove" href="javascript:undefined;" data-dz-remove>' + this.options.dictRemoveFile + "</a>"), t.previewElement.appendChild(t._removeLink)), n = function(e) {
                                return function(n) {
                                    return n.preventDefault(), n.stopPropagation(), t.status === i.UPLOADING ? i.confirm(e.options.dictCancelUploadConfirmation, function() {
                                        return e.removeFile(t)
                                    }) : e.options.dictRemoveFileConfirmation ? i.confirm(e.options.dictRemoveFileConfirmation, function() {
                                        return e.removeFile(t)
                                    }) : e.removeFile(t)
                                }
                            }(this), p = t.previewElement.querySelectorAll("[data-dz-remove]"), f = [], r = 0, u = p.length; r < u; r++) s = p[r], f.push(s.addEventListener("click", n));
                        return f
                    }
                },
                removedfile: function(t) {
                    var e;
                    return t.previewElement && null != (e = t.previewElement) && e.parentNode.removeChild(t.previewElement), this._updateMaxFilesReachedClass()
                },
                thumbnail: function(t, e) {
                    var i, n, s, a;
                    if (t.previewElement) {
                        for (t.previewElement.classList.remove("dz-file-preview"), a = t.previewElement.querySelectorAll("[data-dz-thumbnail]"), n = 0, s = a.length; n < s; n++) i = a[n], i.alt = t.name, i.src = e;
                        return setTimeout(function() {
                            return function() {
                                return t.previewElement.classList.add("dz-image-preview")
                            }
                        }(), 1)
                    }
                },
                error: function(t, e) {
                    var i, n, s, a, o;
                    if (t.previewElement) {
                        for (t.previewElement.classList.add("dz-error"), "String" != typeof e && e.error && (e = e.error), a = t.previewElement.querySelectorAll("[data-dz-errormessage]"), o = [], n = 0, s = a.length; n < s; n++) i = a[n], o.push(i.textContent = e);
                        return o
                    }
                },
                errormultiple: o,
                processing: function(t) {
                    if (t.previewElement && (t.previewElement.classList.add("dz-processing"), t._removeLink)) return t._removeLink.textContent = this.options.dictCancelUpload
                },
                processingmultiple: o,
                uploadprogress: function(t, e) {
                    var i, n, s, a, o;
                    if (t.previewElement) {
                        for (a = t.previewElement.querySelectorAll("[data-dz-uploadprogress]"), o = [], n = 0, s = a.length; n < s; n++) i = a[n], "PROGRESS" === i.nodeName ? o.push(i.value = e) : o.push(i.style.width = e + "%");
                        return o
                    }
                },
                totaluploadprogress: o,
                sending: o,
                sendingmultiple: o,
                success: function(t) {
                    if (t.previewElement) return t.previewElement.classList.add("dz-success")
                },
                successmultiple: o,
                canceled: function(t) {
                    return this.emit("error", t, "Upload canceled.")
                },
                canceledmultiple: o,
                complete: function(t) {
                    if (t._removeLink && (t._removeLink.textContent = this.options.dictRemoveFile), t.previewElement) return t.previewElement.classList.add("dz-complete")
                },
                completemultiple: o,
                maxfilesexceeded: o,
                maxfilesreached: o,
                queuecomplete: o,
                addedfiles: o,
                previewTemplate: '<div class="dz-preview dz-file-preview">\n  <div class="dz-image"><img data-dz-thumbnail /></div>\n  <div class="dz-details">\n    <div class="dz-size"><span data-dz-size></span></div>\n    <div class="dz-filename"><span data-dz-name></span></div>\n  </div>\n  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n  <div class="dz-error-message"><span data-dz-errormessage></span></div>\n  <div class="dz-success-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Check</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>\n      </g>\n    </svg>\n  </div>\n  <div class="dz-error-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Error</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">\n          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>\n        </g>\n      </g>\n    </svg>\n  </div>\n</div>'
            }, n = function() {
                var t, e, i, n, s, a, o;
                for (n = arguments[0], i = 2 <= arguments.length ? l.call(arguments, 1) : [], a = 0, o = i.length; a < o; a++) {
                    e = i[a];
                    for (t in e) s = e[t], n[t] = s
                }
                return n
            }, i.prototype.getAcceptedFiles = function() {
                var t, e, i, n, s;
                for (n = this.files, s = [], e = 0, i = n.length; e < i; e++) t = n[e], t.accepted && s.push(t);
                return s
            }, i.prototype.getRejectedFiles = function() {
                var t, e, i, n, s;
                for (n = this.files, s = [], e = 0, i = n.length; e < i; e++) t = n[e], t.accepted || s.push(t);
                return s
            }, i.prototype.getFilesWithStatus = function(t) {
                var e, i, n, s, a;
                for (s = this.files, a = [], i = 0, n = s.length; i < n; i++) e = s[i], e.status === t && a.push(e);
                return a
            }, i.prototype.getQueuedFiles = function() {
                return this.getFilesWithStatus(i.QUEUED)
            }, i.prototype.getUploadingFiles = function() {
                return this.getFilesWithStatus(i.UPLOADING)
            }, i.prototype.getAddedFiles = function() {
                return this.getFilesWithStatus(i.ADDED)
            }, i.prototype.getActiveFiles = function() {
                var t, e, n, s, a;
                for (s = this.files, a = [], e = 0, n = s.length; e < n; e++) t = s[e], t.status !== i.UPLOADING && t.status !== i.QUEUED || a.push(t);
                return a
            }, i.prototype.init = function() {
                var t, e, n, s, a, o, r;
                for ("form" === this.element.tagName && this.element.setAttribute("enctype", "multipart/form-data"), this.element.classList.contains("dropzone") && !this.element.querySelector(".dz-message") && this.element.appendChild(i.createElement('<div class="dz-default dz-message"><span>' + this.options.dictDefaultMessage + "</span></div>")), this.clickableElements.length && (n = function(t) {
                        return function() {
                            return t.hiddenFileInput && t.hiddenFileInput.parentNode.removeChild(t.hiddenFileInput), t.hiddenFileInput = document.createElement("input"), t.hiddenFileInput.setAttribute("type", "file"), (null == t.options.maxFiles || t.options.maxFiles > 1) && t.hiddenFileInput.setAttribute("multiple", "multiple"), t.hiddenFileInput.className = "dz-hidden-input", null != t.options.acceptedFiles && t.hiddenFileInput.setAttribute("accept", t.options.acceptedFiles), null != t.options.capture && t.hiddenFileInput.setAttribute("capture", t.options.capture), t.hiddenFileInput.style.visibility = "hidden", t.hiddenFileInput.style.position = "absolute", t.hiddenFileInput.style.top = "0", t.hiddenFileInput.style.left = "0", t.hiddenFileInput.style.height = "0", t.hiddenFileInput.style.width = "0", document.querySelector(t.options.hiddenInputContainer).appendChild(t.hiddenFileInput), t.hiddenFileInput.addEventListener("change", function() {
                                var e, i, s, a;
                                if (i = t.hiddenFileInput.files, i.length)
                                    for (s = 0, a = i.length; s < a; s++) e = i[s], t.addFile(e);
                                return t.emit("addedfiles", i), n()
                            })
                        }
                    }(this))(), this.URL = null != (o = window.URL) ? o : window.webkitURL, r = this.events, s = 0, a = r.length; s < a; s++) t = r[s], this.on(t, this.options[t]);
                return this.on("uploadprogress", function(t) {
                    return function() {
                        return t.updateTotalUploadProgress()
                    }
                }(this)), this.on("removedfile", function(t) {
                    return function() {
                        return t.updateTotalUploadProgress()
                    }
                }(this)), this.on("canceled", function(t) {
                    return function(e) {
                        return t.emit("complete", e)
                    }
                }(this)), this.on("complete", function(t) {
                    return function() {
                        if (0 === t.getAddedFiles().length && 0 === t.getUploadingFiles().length && 0 === t.getQueuedFiles().length) return setTimeout(function() {
                            return t.emit("queuecomplete")
                        }, 0)
                    }
                }(this)), e = function(t) {
                    return t.stopPropagation(), t.preventDefault ? t.preventDefault() : t.returnValue = !1
                }, this.listeners = [{
                    element: this.element,
                    events: {
                        dragstart: function(t) {
                            return function(e) {
                                return t.emit("dragstart", e)
                            }
                        }(this),
                        dragenter: function(t) {
                            return function(i) {
                                return e(i), t.emit("dragenter", i)
                            }
                        }(this),
                        dragover: function(t) {
                            return function(i) {
                                var n;
                                try {
                                    n = i.dataTransfer.effectAllowed
                                } catch (t) {}
                                return i.dataTransfer.dropEffect = "move" === n || "linkMove" === n ? "move" : "copy", e(i), t.emit("dragover", i)
                            }
                        }(this),
                        dragleave: function(t) {
                            return function(e) {
                                return t.emit("dragleave", e)
                            }
                        }(this),
                        drop: function(t) {
                            return function(i) {
                                return e(i), t.drop(i)
                            }
                        }(this),
                        dragend: function(t) {
                            return function(e) {
                                return t.emit("dragend", e)
                            }
                        }(this)
                    }
                }], this.clickableElements.forEach(function(t) {
                    return function(e) {
                        return t.listeners.push({
                            element: e,
                            events: {
                                click: function(n) {
                                    return (e !== t.element || n.target === t.element || i.elementInside(n.target, t.element.querySelector(".dz-message"))) && t.hiddenFileInput.click(), !0
                                }
                            }
                        })
                    }
                }(this)), this.enable(), this.options.init.call(this)
            }, i.prototype.destroy = function() {
                var t;
                return this.disable(), this.removeAllFiles(!0), (null != (t = this.hiddenFileInput) ? t.parentNode : void 0) && (this.hiddenFileInput.parentNode.removeChild(this.hiddenFileInput), this.hiddenFileInput = null), delete this.element.dropzone, i.instances.splice(i.instances.indexOf(this), 1)
            }, i.prototype.updateTotalUploadProgress = function() {
                var t, e, i, n, s, a, o, r;
                if (n = 0, i = 0, t = this.getActiveFiles(), t.length) {
                    for (r = this.getActiveFiles(), a = 0, o = r.length; a < o; a++) e = r[a], n += e.upload.bytesSent, i += e.upload.total;
                    s = 100 * n / i
                } else s = 100;
                return this.emit("totaluploadprogress", s, i, n)
            }, i.prototype._getParamName = function(t) {
                return "function" == typeof this.options.paramName ? this.options.paramName(t) : this.options.paramName + (this.options.uploadMultiple ? "[" + t + "]" : "")
            }, i.prototype._renameFilename = function(t) {
                return "function" != typeof this.options.renameFilename ? t : this.options.renameFilename(t)
            }, i.prototype.getFallbackForm = function() {
                var t, e, n, s;
                return (t = this.getExistingFallback()) ? t : (n = '<div class="dz-fallback">', this.options.dictFallbackText && (n += "<p>" + this.options.dictFallbackText + "</p>"), n += '<input type="file" name="' + this._getParamName(0) + '" ' + (this.options.uploadMultiple ? 'multiple="multiple"' : void 0) + ' /><input type="submit" value="Upload!"></div>', e = i.createElement(n), "FORM" !== this.element.tagName ? (s = i.createElement('<form action="' + this.options.url + '" enctype="multipart/form-data" method="' + this.options.method + '"></form>'), s.appendChild(e)) : (this.element.setAttribute("enctype", "multipart/form-data"), this.element.setAttribute("method", this.options.method)), null != s ? s : e)
            }, i.prototype.getExistingFallback = function() {
                var t, e, i, n, s, a;
                for (e = function(t) {
                        var e, i, n;
                        for (i = 0, n = t.length; i < n; i++)
                            if (e = t[i], /(^| )fallback($| )/.test(e.className)) return e
                    }, a = ["div", "form"], n = 0, s = a.length; n < s; n++)
                    if (i = a[n], t = e(this.element.getElementsByTagName(i))) return t
            }, i.prototype.setupEventListeners = function() {
                var t, e, i, n, s, a, o;
                for (a = this.listeners, o = [], n = 0, s = a.length; n < s; n++) t = a[n], o.push(function() {
                    var n, s;
                    n = t.events, s = [];
                    for (e in n) i = n[e], s.push(t.element.addEventListener(e, i, !1));
                    return s
                }());
                return o
            }, i.prototype.removeEventListeners = function() {
                var t, e, i, n, s, a, o;
                for (a = this.listeners, o = [], n = 0, s = a.length; n < s; n++) t = a[n], o.push(function() {
                    var n, s;
                    n = t.events, s = [];
                    for (e in n) i = n[e], s.push(t.element.removeEventListener(e, i, !1));
                    return s
                }());
                return o
            }, i.prototype.disable = function() {
                var t, e, i, n, s;
                for (this.clickableElements.forEach(function(t) {
                        return t.classList.remove("dz-clickable")
                    }), this.removeEventListeners(), n = this.files, s = [], e = 0, i = n.length; e < i; e++) t = n[e], s.push(this.cancelUpload(t));
                return s
            }, i.prototype.enable = function() {
                return this.clickableElements.forEach(function(t) {
                    return t.classList.add("dz-clickable")
                }), this.setupEventListeners()
            }, i.prototype.filesize = function(t) {
                var e, i, n, s, a, o, r, l;
                if (n = 0, s = "b", t > 0) {
                    for (o = ["TB", "GB", "MB", "KB", "b"], i = r = 0, l = o.length; r < l; i = ++r)
                        if (a = o[i], e = Math.pow(this.options.filesizeBase, 4 - i) / 10, t >= e) {
                            n = t / Math.pow(this.options.filesizeBase, 4 - i), s = a;
                            break
                        }
                    n = Math.round(10 * n) / 10
                }
                return "<strong>" + n + "</strong> " + s
            }, i.prototype._updateMaxFilesReachedClass = function() {
                return null != this.options.maxFiles && this.getAcceptedFiles().length >= this.options.maxFiles ? (this.getAcceptedFiles().length === this.options.maxFiles && this.emit("maxfilesreached", this.files), this.element.classList.add("dz-max-files-reached")) : this.element.classList.remove("dz-max-files-reached")
            }, i.prototype.drop = function(t) {
                var e, i;
                t.dataTransfer && (this.emit("drop", t), e = t.dataTransfer.files, this.emit("addedfiles", e), e.length && (i = t.dataTransfer.items, i && i.length && null != i[0].webkitGetAsEntry ? this._addFilesFromItems(i) : this.handleFiles(e)))
            }, i.prototype.paste = function(t) {
                var e, i;
                if (null != (null != t && null != (i = t.clipboardData) ? i.items : void 0)) return this.emit("paste", t), e = t.clipboardData.items, e.length ? this._addFilesFromItems(e) : void 0
            }, i.prototype.handleFiles = function(t) {
                var e, i, n, s;
                for (s = [], i = 0, n = t.length; i < n; i++) e = t[i], s.push(this.addFile(e));
                return s
            }, i.prototype._addFilesFromItems = function(t) {
                var e, i, n, s, a;
                for (a = [], n = 0, s = t.length; n < s; n++) i = t[n], null != i.webkitGetAsEntry && (e = i.webkitGetAsEntry()) ? e.isFile ? a.push(this.addFile(i.getAsFile())) : e.isDirectory ? a.push(this._addFilesFromDirectory(e, e.name)) : a.push(void 0) : null != i.getAsFile && (null == i.kind || "file" === i.kind) ? a.push(this.addFile(i.getAsFile())) : a.push(void 0);
                return a
            }, i.prototype._addFilesFromDirectory = function(t, e) {
                var i, n, s;
                return i = t.createReader(), n = function(t) {
                    return "undefined" != typeof console && null !== console && "function" == typeof console.log ? console.log(t) : void 0
                }, (s = function(t) {
                    return function() {
                        return i.readEntries(function(i) {
                            var n, a, o;
                            if (i.length > 0) {
                                for (a = 0, o = i.length; a < o; a++) n = i[a], n.isFile ? n.file(function(i) {
                                    if (!t.options.ignoreHiddenFiles || "." !== i.name.substring(0, 1)) return i.fullPath = e + "/" + i.name, t.addFile(i)
                                }) : n.isDirectory && t._addFilesFromDirectory(n, e + "/" + n.name);
                                s()
                            }
                            return null
                        }, n)
                    }
                }(this))()
            }, i.prototype.accept = function(t, e) {
                return t.size > 1024 * this.options.maxFilesize * 1024 ? e(this.options.dictFileTooBig.replace("{{filesize}}", Math.round(t.size / 1024 / 10.24) / 100).replace("{{maxFilesize}}", this.options.maxFilesize)) : i.isValidFile(t, this.options.acceptedFiles) ? null != this.options.maxFiles && this.getAcceptedFiles().length >= this.options.maxFiles ? (e(this.options.dictMaxFilesExceeded.replace("{{maxFiles}}", this.options.maxFiles)), this.emit("maxfilesexceeded", t)) : this.options.accept.call(this, t, e) : e(this.options.dictInvalidFileType)
            }, i.prototype.addFile = function(t) {
                return t.upload = {
                    progress: 0,
                    total: t.size,
                    bytesSent: 0
                }, this.files.push(t), t.status = i.ADDED, this.emit("addedfile", t), this._enqueueThumbnail(t), this.accept(t, function(e) {
                    return function(i) {
                        return i ? (t.accepted = !1, e._errorProcessing([t], i)) : (t.accepted = !0, e.options.autoQueue && e.enqueueFile(t)), e._updateMaxFilesReachedClass()
                    }
                }(this))
            }, i.prototype.enqueueFiles = function(t) {
                var e, i, n;
                for (i = 0, n = t.length; i < n; i++) e = t[i], this.enqueueFile(e);
                return null
            }, i.prototype.enqueueFile = function(t) {
                if (t.status !== i.ADDED || !0 !== t.accepted) throw new Error("This file can't be queued because it has already been processed or was rejected.");
                if (t.status = i.QUEUED, this.options.autoProcessQueue) return setTimeout(function(t) {
                    return function() {
                        return t.processQueue()
                    }
                }(this), 0)
            }, i.prototype._thumbnailQueue = [], i.prototype._processingThumbnail = !1, i.prototype._enqueueThumbnail = function(t) {
                if (this.options.createImageThumbnails && t.type.match(/image.*/) && t.size <= 1024 * this.options.maxThumbnailFilesize * 1024) return this._thumbnailQueue.push(t), setTimeout(function(t) {
                    return function() {
                        return t._processThumbnailQueue()
                    }
                }(this), 0)
            }, i.prototype._processThumbnailQueue = function() {
                if (!this._processingThumbnail && 0 !== this._thumbnailQueue.length) return this._processingThumbnail = !0, this.createThumbnail(this._thumbnailQueue.shift(), function(t) {
                    return function() {
                        return t._processingThumbnail = !1, t._processThumbnailQueue()
                    }
                }(this))
            }, i.prototype.removeFile = function(t) {
                if (t.status === i.UPLOADING && this.cancelUpload(t), this.files = r(this.files, t), this.emit("removedfile", t), 0 === this.files.length) return this.emit("reset")
            }, i.prototype.removeAllFiles = function(t) {
                var e, n, s, a;
                for (null == t && (t = !1), a = this.files.slice(), n = 0, s = a.length; n < s; n++) e = a[n], (e.status !== i.UPLOADING || t) && this.removeFile(e);
                return null
            }, i.prototype.createThumbnail = function(t, e) {
                var i;
                return i = new FileReader, i.onload = function(n) {
                    return function() {
                        return "image/svg+xml" === t.type ? (n.emit("thumbnail", t, i.result), void(null != e && e())) : n.createThumbnailFromUrl(t, i.result, e)
                    }
                }(this), i.readAsDataURL(t)
            }, i.prototype.createThumbnailFromUrl = function(t, e, i, n) {
                var s;
                return s = document.createElement("img"), n && (s.crossOrigin = n), s.onload = function(e) {
                    return function() {
                        var n, o, r, l, c, u, d, h;
                        if (t.width = s.width, t.height = s.height, r = e.options.resize.call(e, t), null == r.trgWidth && (r.trgWidth = r.optWidth), null == r.trgHeight && (r.trgHeight = r.optHeight), n = document.createElement("canvas"), o = n.getContext("2d"), n.width = r.trgWidth, n.height = r.trgHeight, a(o, s, null != (c = r.srcX) ? c : 0, null != (u = r.srcY) ? u : 0, r.srcWidth, r.srcHeight, null != (d = r.trgX) ? d : 0, null != (h = r.trgY) ? h : 0, r.trgWidth, r.trgHeight), l = n.toDataURL("image/png"), e.emit("thumbnail", t, l), null != i) return i()
                    }
                }(this), null != i && (s.onerror = i), s.src = e
            }, i.prototype.processQueue = function() {
                var t, e, i, n;
                if (e = this.options.parallelUploads, i = this.getUploadingFiles().length, t = i, !(i >= e) && (n = this.getQueuedFiles(), n.length > 0)) {
                    if (this.options.uploadMultiple) return this.processFiles(n.slice(0, e - i));
                    for (; t < e;) {
                        if (!n.length) return;
                        this.processFile(n.shift()), t++
                    }
                }
            }, i.prototype.processFile = function(t) {
                return this.processFiles([t])
            }, i.prototype.processFiles = function(t) {
                var e, n, s;
                for (n = 0, s = t.length; n < s; n++) e = t[n], e.processing = !0, e.status = i.UPLOADING, this.emit("processing", e);
                return this.options.uploadMultiple && this.emit("processingmultiple", t), this.uploadFiles(t)
            }, i.prototype._getFilesWithXhr = function(t) {
                var e;
                return function() {
                    var i, n, s, a;
                    for (s = this.files, a = [], i = 0, n = s.length; i < n; i++) e = s[i], e.xhr === t && a.push(e);
                    return a
                }.call(this)
            }, i.prototype.cancelUpload = function(t) {
                var e, n, s, a, o, r, l;
                if (t.status === i.UPLOADING) {
                    for (n = this._getFilesWithXhr(t.xhr), s = 0, o = n.length; s < o; s++) e = n[s], e.status = i.CANCELED;
                    for (t.xhr.abort(), a = 0, r = n.length; a < r; a++) e = n[a], this.emit("canceled", e);
                    this.options.uploadMultiple && this.emit("canceledmultiple", n)
                } else(l = t.status) !== i.ADDED && l !== i.QUEUED || (t.status = i.CANCELED, this.emit("canceled", t), this.options.uploadMultiple && this.emit("canceledmultiple", [t]));
                if (this.options.autoProcessQueue) return this.processQueue()
            }, s = function() {
                var t, e;
                return e = arguments[0], t = 2 <= arguments.length ? l.call(arguments, 1) : [], "function" == typeof e ? e.apply(this, t) : e
            }, i.prototype.uploadFile = function(t) {
                return this.uploadFiles([t])
            }, i.prototype.uploadFiles = function(t) {
                var e, a, o, r, l, c, u, d, h, p, f, m, g, _, v, b, y, w, x, $, k, C, D, S, T, I, A, E, M, P, N, F, j, L;
                for (x = new XMLHttpRequest, $ = 0, S = t.length; $ < S; $++) e = t[$], e.xhr = x;
                m = s(this.options.method, t), y = s(this.options.url, t), x.open(m, y, !0), x.withCredentials = !!this.options.withCredentials, v = null, o = function(i) {
                    return function() {
                        var n, s, a;
                        for (a = [], n = 0, s = t.length; n < s; n++) e = t[n], a.push(i._errorProcessing(t, v || i.options.dictResponseError.replace("{{statusCode}}", x.status), x));
                        return a
                    }
                }(this), b = function(i) {
                    return function(n) {
                        var s, a, o, r, l, c, u, d, h;
                        if (null != n)
                            for (a = 100 * n.loaded / n.total, o = 0, c = t.length; o < c; o++) e = t[o], e.upload = {
                                progress: a,
                                total: n.total,
                                bytesSent: n.loaded
                            };
                        else {
                            for (s = !0, a = 100, r = 0, u = t.length; r < u; r++) e = t[r], 100 === e.upload.progress && e.upload.bytesSent === e.upload.total || (s = !1), e.upload.progress = a, e.upload.bytesSent = e.upload.total;
                            if (s) return
                        }
                        for (h = [], l = 0, d = t.length; l < d; l++) e = t[l], h.push(i.emit("uploadprogress", e, a, e.upload.bytesSent));
                        return h
                    }
                }(this), x.onload = function(e) {
                    return function(n) {
                        var s;
                        if (t[0].status !== i.CANCELED && 4 === x.readyState) {
                            if (v = x.responseText, x.getResponseHeader("content-type") && ~x.getResponseHeader("content-type").indexOf("application/json")) try {
                                v = JSON.parse(v)
                            } catch (t) {
                                n = t, v = "Invalid JSON response from server."
                            }
                            return b(), 200 <= (s = x.status) && s < 300 ? e._finished(t, v, n) : o()
                        }
                    }
                }(this), x.onerror = function() {
                    return function() {
                        if (t[0].status !== i.CANCELED) return o()
                    }
                }(), _ = null != (M = x.upload) ? M : x, _.onprogress = b, c = {
                    Accept: "application/json",
                    "Cache-Control": "no-cache",
                    "X-Requested-With": "XMLHttpRequest"
                }, this.options.headers && n(c, this.options.headers);
                for (r in c)(l = c[r]) && x.setRequestHeader(r, l);
                if (a = new FormData, this.options.params) {
                    P = this.options.params;
                    for (f in P) w = P[f], a.append(f, w)
                }
                for (k = 0, T = t.length; k < T; k++) e = t[k], this.emit("sending", e, x, a);
                if (this.options.uploadMultiple && this.emit("sendingmultiple", t, x, a), "FORM" === this.element.tagName)
                    for (N = this.element.querySelectorAll("input, textarea, select, button"), C = 0, I = N.length; C < I; C++)
                        if (d = N[C], h = d.getAttribute("name"), p = d.getAttribute("type"), "SELECT" === d.tagName && d.hasAttribute("multiple"))
                            for (F = d.options, D = 0, A = F.length; D < A; D++) g = F[D], g.selected && a.append(h, g.value);
                        else(!p || "checkbox" !== (j = p.toLowerCase()) && "radio" !== j || d.checked) && a.append(h, d.value);
                for (u = E = 0, L = t.length - 1; 0 <= L ? E <= L : E >= L; u = 0 <= L ? ++E : --E) a.append(this._getParamName(u), t[u], this._renameFilename(t[u].name));
                return this.submitRequest(x, a, t)
            }, i.prototype.submitRequest = function(t, e) {
                return t.send(e)
            }, i.prototype._finished = function(t, e, n) {
                var s, a, o;
                for (a = 0, o = t.length; a < o; a++) s = t[a], s.status = i.SUCCESS, this.emit("success", s, e, n), this.emit("complete", s);
                if (this.options.uploadMultiple && (this.emit("successmultiple", t, e, n), this.emit("completemultiple", t)), this.options.autoProcessQueue) return this.processQueue()
            }, i.prototype._errorProcessing = function(t, e, n) {
                var s, a, o;
                for (a = 0, o = t.length; a < o; a++) s = t[a], s.status = i.ERROR, this.emit("error", s, e, n), this.emit("complete", s);
                if (this.options.uploadMultiple && (this.emit("errormultiple", t, e, n), this.emit("completemultiple", t)), this.options.autoProcessQueue) return this.processQueue()
            }, i
        }(e), t.version = "4.3.0", t.options = {}, t.optionsForElement = function(e) {
            return e.getAttribute("id") ? t.options[i(e.getAttribute("id"))] : void 0
        }, t.instances = [], t.forElement = function(t) {
            if ("string" == typeof t && (t = document.querySelector(t)), null == (null != t ? t.dropzone : void 0)) throw new Error("No Dropzone found for given element. This is probably because you're trying to access it before Dropzone had the time to initialize. Use the `init` option to setup any additional observers on your Dropzone.");
            return t.dropzone
        }, t.autoDiscover = !0, t.discover = function() {
            var e, i, n, s, a, o;
            for (document.querySelectorAll ? n = document.querySelectorAll(".dropzone") : (n = [], e = function(t) {
                    var e, i, s, a;
                    for (a = [], i = 0, s = t.length; i < s; i++) e = t[i], /(^| )dropzone($| )/.test(e.className) ? a.push(n.push(e)) : a.push(void 0);
                    return a
                }, e(document.getElementsByTagName("div")), e(document.getElementsByTagName("form"))), o = [], s = 0, a = n.length; s < a; s++) i = n[s], !1 !== t.optionsForElement(i) ? o.push(new t(i)) : o.push(void 0);
            return o
        }, t.blacklistedBrowsers = [/opera.*Macintosh.*version\/12/i], t.isBrowserSupported = function() {
            var e, i, n, s, a;
            if (e = !0, window.File && window.FileReader && window.FileList && window.Blob && window.FormData && document.querySelector)
                if ("classList" in document.createElement("a"))
                    for (a = t.blacklistedBrowsers, n = 0, s = a.length; n < s; n++) i = a[n], i.test(navigator.userAgent) && (e = !1);
                else e = !1;
            else e = !1;
            return e
        }, r = function(t, e) {
            var i, n, s, a;
            for (a = [], n = 0, s = t.length; n < s; n++)(i = t[n]) !== e && a.push(i);
            return a
        }, i = function(t) {
            return t.replace(/[\-_](\w)/g, function(t) {
                return t.charAt(1).toUpperCase()
            })
        }, t.createElement = function(t) {
            var e;
            return e = document.createElement("div"), e.innerHTML = t, e.childNodes[0]
        }, t.elementInside = function(t, e) {
            if (t === e) return !0;
            for (; t = t.parentNode;)
                if (t === e) return !0;
            return !1
        }, t.getElement = function(t, e) {
            var i;
            if ("string" == typeof t ? i = document.querySelector(t) : null != t.nodeType && (i = t), null == i) throw new Error("Invalid `" + e + "` option provided. Please provide a CSS selector or a plain HTML element.");
            return i
        }, t.getElements = function(t, e) {
            var i, n, s, a, o, r, l;
            if (t instanceof Array) {
                n = [];
                try {
                    for (s = 0, o = t.length; s < o; s++) i = t[s], n.push(this.getElement(i, e))
                } catch (t) {
                    t,
                    n = null
                }
            } else if ("string" == typeof t)
                for (n = [], l = document.querySelectorAll(t), a = 0, r = l.length; a < r; a++) i = l[a], n.push(i);
            else null != t.nodeType && (n = [t]);
            if (null == n || !n.length) throw new Error("Invalid `" + e + "` option provided. Please provide a CSS selector, a plain HTML element or a list of those.");
            return n
        }, t.confirm = function(t, e, i) {
            return window.confirm(t) ? e() : null != i ? i() : void 0
        }, t.isValidFile = function(t, e) {
            var i, n, s, a, o;
            if (!e) return !0;
            for (e = e.split(","), n = t.type, i = n.replace(/\/.*$/, ""), a = 0, o = e.length; a < o; a++)
                if (s = e[a], s = s.trim(), "." === s.charAt(0)) {
                    if (-1 !== t.name.toLowerCase().indexOf(s.toLowerCase(), t.name.length - s.length)) return !0
                } else if (/\/\*$/.test(s)) {
                if (i === s.replace(/\/.*$/, "")) return !0
            } else if (n === s) return !0;
            return !1
        }, "undefined" != typeof jQuery && null !== jQuery && (jQuery.fn.dropzone = function(e) {
            return this.each(function() {
                return new t(this, e)
            })
        }), "undefined" != typeof module && null !== module ? module.exports = t : window.Dropzone = t, t.ADDED = "added", t.QUEUED = "queued", t.ACCEPTED = t.QUEUED, t.UPLOADING = "uploading", t.PROCESSING = t.UPLOADING, t.CANCELED = "canceled", t.ERROR = "error", t.SUCCESS = "success", s = function(t) {
            var e, i, n, s, a, o, r, l, c;
            for (t.naturalWidth, o = t.naturalHeight, i = document.createElement("canvas"), i.width = 1, i.height = o, n = i.getContext("2d"), n.drawImage(t, 0, 0), s = n.getImageData(0, 0, 1, o).data, c = 0, a = o, r = o; r > c;) e = s[4 * (r - 1) + 3], 0 === e ? a = r : c = r, r = a + c >> 1;
            return l = r / o, 0 === l ? 1 : l
        }, a = function(t, e, i, n, a, o, r, l, c, u) {
            var d;
            return d = s(e), t.drawImage(e, i, n, a, o, r, l, c, u / d)
        }, n = function(t, e) {
            var i, n, s, a, o, r, l, c, u;
            if (s = !1, u = !0, n = t.document, c = n.documentElement, i = n.addEventListener ? "addEventListener" : "attachEvent", l = n.addEventListener ? "removeEventListener" : "detachEvent", r = n.addEventListener ? "" : "on", a = function(i) {
                    if ("readystatechange" !== i.type || "complete" === n.readyState) return ("load" === i.type ? t : n)[l](r + i.type, a, !1), !s && (s = !0) ? e.call(t, i.type || i) : void 0
                }, o = function() {
                    try {
                        c.doScroll("left")
                    } catch (t) {
                        return t, void setTimeout(o, 50)
                    }
                    return a("poll")
                }, "complete" !== n.readyState) {
                if (n.createEventObject && c.doScroll) {
                    try {
                        u = !t.frameElement
                    } catch (t) {}
                    u && o()
                }
                return n[i](r + "DOMContentLoaded", a, !1), n[i](r + "readystatechange", a, !1), t[i](r + "load", a, !1)
            }
        }, t._autoDiscoverFunction = function() {
            if (t.autoDiscover) return t.discover()
        }, n(window, t._autoDiscoverFunction)
    }.call(this), $(document).ready(function() {
        $(document).on("click", ".add_edit_hours_option", function() {
            null != $(".item-selected").attr("id") ? $("#clienthours").modal() : toastr.info("Please create Business first.")
        }), $(document).on("click", ".delete_client", function() {
            null != $(".item-selected").attr("id") ? $.ajax({
                type: "get",
                url: "add_edit_clients/delete_selected_client",
                data: {
                    key: $(".item-selected").attr("id")
                },
                success: function() {}
            }) : toastr.info("Please select Business first.")
        }),$(document).on("click", ".client_day_time", function() {
            $("#clienthours").modal("hide")
        }), $(document).on("click", ".item_image", function() {
            $("#clientpicture").modal("hide")
        })
    }), $(document).ready(function() {
        $(document).on("click", ".make_user_active", function() {
                "true" == $(this).find("input").attr("data-id") ? ($(this).find("input").attr("value", "true"), $(this).find("input").attr("data-id", "false")) : ($(this).find("input").attr("value", "false"), $(this).find("input").attr("data-id", "true"))
            }),
            $(document).on("click", ".close-business_img", function() {
                $(this).parent().hide(), $.ajax({
                    type: "delete",
                    url: "/delete_profile_image",
                    data: {
                        i_id: this.id
                    },
                    success: function() {
                        toastr.success("Selected image deleted successfully.")
                    }
                })
            }), $(document).on("change", ".client_check_box", function() {
                "marijuana" == this.name ? ($(this).prop("checked", !0), $("[name=foodtruck]").prop("checked", !1), $("[name=restaurent]").prop("checked", !1)) : "foodtruck" == this.name ? ($(this).prop("checked", !0), $("[name=marijuana]").prop("checked", !1), $("[name=restaurent]").prop("checked", !1)) : "restaurent" == this.name && ($(this).prop("checked", !0), $("[name=marijuana]").prop("checked", !1), $("[name=foodtruck]").prop("checked", !1))
            })
    }), function(t) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof module && module.exports ? module.exports = t(require("jquery")) : t(window.jQuery)
    }(function(t) {
        "use strict";
        t.fn.ratingLocales = {}, t.fn.ratingThemes = {};
        var e, i;
        e = {
            NAMESPACE: ".rating",
            DEFAULT_MIN: 0,
            DEFAULT_MAX: 5,
            DEFAULT_STEP: .5,
            isEmpty: function(e, i) {
                return null === e || e === undefined || 0 === e.length || i && "" === t.trim(e)
            },
            getCss: function(t, e) {
                return t ? " " + e : ""
            },
            addCss: function(t, e) {
                t.removeClass(e).addClass(e)
            },
            getDecimalPlaces: function(t) {
                var e = ("" + t).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                return e ? Math.max(0, (e[1] ? e[1].length : 0) - (e[2] ? +e[2] : 0)) : 0
            },
            applyPrecision: function(t, e) {
                return parseFloat(t.toFixed(e))
            },
            handler: function(t, i, n, s, a) {
                var o = a ? i : i.split(" ").join(e.NAMESPACE + " ") + e.NAMESPACE;
                s || t.off(o), t.on(o, n)
            }
        }, i = function(e, i) {
            var n = this;
            n.$element = t(e), n._init(i)
        }, i.prototype = {
            constructor: i,
            _parseAttr: function(t, i) {
                var n, s, a, o, r = this,
                    l = r.$element,
                    c = l.attr("type");
                if ("range" === c || "number" === c) {
                    switch (s = i[t] || l.data(t) || l.attr(t), t) {
                        case "min":
                            a = e.DEFAULT_MIN;
                            break;
                        case "max":
                            a = e.DEFAULT_MAX;
                            break;
                        default:
                            a = e.DEFAULT_STEP
                    }
                    n = e.isEmpty(s) ? a : s, o = parseFloat(n)
                } else o = parseFloat(i[t]);
                return isNaN(o) ? a : o
            },
            _parseValue: function(t) {
                var e = this,
                    i = parseFloat(t);
                return isNaN(i) && (i = e.clearValue), !e.zeroAsNull || 0 !== i && "0" !== i ? i : null
            },
            _setDefault: function(t, i) {
                var n = this;
                e.isEmpty(n[t]) && (n[t] = i)
            },
            _initSlider: function(t) {
                var i = this,
                    n = i.$element.val();
                i.initialValue = e.isEmpty(n) ? 0 : n, i._setDefault("min", i._parseAttr("min", t)), i._setDefault("max", i._parseAttr("max", t)), i._setDefault("step", i._parseAttr("step", t)), (isNaN(i.min) || e.isEmpty(i.min)) && (i.min = e.DEFAULT_MIN), (isNaN(i.max) || e.isEmpty(i.max)) && (i.max = e.DEFAULT_MAX), (isNaN(i.step) || e.isEmpty(i.step) || 0 === i.step) && (i.step = e.DEFAULT_STEP), i.diff = i.max - i.min
            },
            _initHighlight: function(t) {
                var e, i = this,
                    n = i._getCaption();
                t || (t = i.$element.val()), e = i.getWidthFromValue(t) + "%", i.$filledStars.width(e), i.cache = {
                    caption: n,
                    width: e,
                    val: t
                }
            },
            _getContainerCss: function() {
                var t = this;
                return "rating-container" + e.getCss(t.theme, "theme-" + t.theme) + e.getCss(t.rtl, "rating-rtl") + e.getCss(t.size, "rating-" + t.size) + e.getCss(t.animate, "rating-animate") + e.getCss(t.disabled || t.readonly, "rating-disabled") + e.getCss(t.containerClass, t.containerClass)
            },
            _checkDisabled: function() {
                var t = this,
                    e = t.$element,
                    i = t.options;
                t.disabled = i.disabled === undefined ? e.attr("disabled") || !1 : i.disabled, t.readonly = i.readonly === undefined ? e.attr("readonly") || !1 : i.readonly, t.inactive = t.disabled || t.readonly, e.attr({
                    disabled: t.disabled,
                    readonly: t.readonly
                })
            },
            _addContent: function(t, e) {
                var i = this,
                    n = i.$container,
                    s = "clear" === t;
                return i.rtl ? s ? n.append(e) : n.prepend(e) : s ? n.prepend(e) : n.append(e)
            },
            _generateRating: function() {
                var i, n, s, a = this,
                    o = a.$element;
                n = a.$container = t(document.createElement("div")).insertBefore(o), e.addCss(n, a._getContainerCss()), a.$rating = i = t(document.createElement("div")).attr("class", "rating-stars").appendTo(n).append(a._getStars("empty")).append(a._getStars("filled")), a.$emptyStars = i.find(".empty-stars"), a.$filledStars = i.find(".filled-stars"), a._renderCaption(), a._renderClear(), a._initHighlight(), n.append(o), a.rtl && (s = Math.max(a.$emptyStars.outerWidth(), a.$filledStars.outerWidth()), a.$emptyStars.width(s)), o.appendTo(i)
            },
            _getCaption: function() {
                var t = this;
                return t.$caption && t.$caption.length ? t.$caption.html() : t.defaultCaption
            },
            _setCaption: function(t) {
                var e = this;
                e.$caption && e.$caption.length && e.$caption.html(t)
            },
            _renderCaption: function() {
                var i, n = this,
                    s = n.$element.val(),
                    a = n.captionElement ? t(n.captionElement) : "";
                if (n.showCaption) {
                    if (i = n.fetchCaption(s), a && a.length) return e.addCss(a, "caption"), a.html(i), void(n.$caption = a);
                    n._addContent("caption", '<div class="caption">' + i + "</div>"), n.$caption = n.$container.find(".caption")
                }
            },
            _renderClear: function() {
                var i, n = this,
                    s = n.clearElement ? t(n.clearElement) : "";
                if (n.showClear) {
                    if (i = n._getClearClass(), s.length) return e.addCss(s, i), s.attr({
                        title: n.clearButtonTitle
                    }).html(n.clearButton), void(n.$clear = s);
                    n._addContent("clear", '<div class="' + i + '" title="' + n.clearButtonTitle + '">' + n.clearButton + "</div>"), n.$clear = n.$container.find("." + n.clearButtonBaseClass)
                }
            },
            _getClearClass: function() {
                var t = this;
                return t.clearButtonBaseClass + " " + (t.inactive ? "" : t.clearButtonActiveClass)
            },
            _toggleHover: function() {},
            _init: function(e) {
                var i, n = this,
                    s = n.$element.addClass("rating-input");
                return n.options = e, t.each(e, function(t, e) {
                    n[t] = e
                }), (n.rtl || "rtl" === s.attr("dir")) && (n.rtl = !0, s.attr("dir", "rtl")), n.starClicked = !1, n.clearClicked = !1, n._initSlider(e), n._checkDisabled(), n.displayOnly && (n.inactive = !0, n.showClear = !1, n.showCaption = !1), n._generateRating(), n._initEvents(), n._listen(), i = n._parseValue(s.val()), s.val(i), s.removeClass("rating-loading")
            },
            _initEvents: function() {
                var t = this;
                t.events = {
                    _getTouchPosition: function(i) {
                        return (e.isEmpty(i.pageX) ? i.originalEvent.touches[0].pageX : i.pageX) - t.$rating.offset().left
                    },
                    _listenClick: function(t, e) {
                        if (t.stopPropagation(), t.preventDefault(), !0 === t.handled) return !1;
                        e(t), t.handled = !0
                    },
                    _noMouseAction: function(e) {
                        return !t.hoverEnabled || t.inactive || e && e.isDefaultPrevented()
                    },
                    initTouch: function(i) {
                        var n, s, a, o, r, l, c, u, d = t.clearValue || 0;
                        ("ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch) && !t.inactive && (n = i.originalEvent, s = e.isEmpty(n.touches) ? n.changedTouches : n.touches, a = t.events._getTouchPosition(s[0]), "touchend" === i.type ? (t._setStars(a), u = [t.$element.val(), t._getCaption()], t.$element.trigger("change").trigger("rating.change", u), t.starClicked = !0) : (o = t.calculate(a), r = o.val <= d ? t.fetchCaption(d) : o.caption, l = t.getWidthFromValue(d), c = o.val <= d ? l + "%" : o.width, t._setCaption(r), t.$filledStars.css("width", c)))
                    },
                    starClick: function(e) {
                        var i, n;
                        t.events._listenClick(e, function(e) {
                            if (t.inactive) return !1;
                            i = t.events._getTouchPosition(e), t._setStars(i), n = [t.$element.val(), t._getCaption()], t.$element.trigger("change").trigger("rating.change", n), t.starClicked = !0
                        })
                    },
                    clearClick: function(e) {
                        t.events._listenClick(e, function() {
                            t.inactive || (t.clear(), t.clearClicked = !0)
                        })
                    },
                    starMouseMove: function(e) {
                        var i, n;
                        t.events._noMouseAction(e)
                    },
                    starMouseLeave: function(e) {
                        var i;
                        t.events._noMouseAction(e)
                    },
                    clearMouseMove: function(e) {
                        var i, n, s, a;
                        !t.events._noMouseAction(e) && t.hoverOnClear && (t.clearClicked = !1, i = '<span class="' + t.clearCaptionClass + '">' + t.clearCaption + "</span>", n = t.clearValue, s = t.getWidthFromValue(n) || 0, a = {
                            caption: i,
                            width: s,
                            val: n
                        }, t._toggleHover(a), t.$element.trigger("rating.hover", [n, i, "clear"]))
                    },
                    clearMouseLeave: function(e) {
                        var i;
                        t.events._noMouseAction(e) || t.clearClicked || !t.hoverOnClear || (i = t.cache, t._toggleHover(i), t.$element.trigger("rating.hoverleave", ["clear"]))
                    },
                    resetForm: function(e) {
                        e && e.isDefaultPrevented() || t.inactive || t.reset()
                    }
                }
            },
            _listen: function() {
                var i = this,
                    n = i.$element,
                    s = n.closest("form"),
                    a = i.$rating,
                    o = i.$clear,
                    r = i.events;
                return e.handler(a, "touchstart touchmove touchend", t.proxy(r.initTouch, i)), e.handler(a, "click touchstart", t.proxy(r.starClick, i)), e.handler(a, "mousemove", t.proxy(r.starMouseMove, i)), e.handler(a, "mouseleave", t.proxy(r.starMouseLeave, i)), i.showClear && o.length && (e.handler(o, "click touchstart", t.proxy(r.clearClick, i)), e.handler(o, "mousemove", t.proxy(r.clearMouseMove, i)), e.handler(o, "mouseleave", t.proxy(r.clearMouseLeave, i))), s.length && e.handler(s, "reset", t.proxy(r.resetForm, i), !0), n
            },
            _getStars: function(t) {
                var e, i = this,
                    n = '<span class="' + t + '-stars">';
                for (e = 1; e <= i.stars; e++) n += '<span class="star">' + i[t + "Star"] + "</span>";
                return n + "</span>"
            },
            _setStars: function(t) {
                var e = this,
                    i = arguments.length ? e.calculate(t) : e.calculate(),
                    n = e.$element,
                    s = e._parseValue(i.val);
                return n.val(s), e.$filledStars.css("width", i.width), e._setCaption(i.caption), e.cache = i, n
            },
            showStars: function(t) {
                var e = this,
                    i = e._parseValue(t);
                return e.$element.val(i), e._setStars()
            },
            calculate: function(t) {
                var i = this,
                    n = e.isEmpty(i.$element.val()) ? 0 : i.$element.val(),
                    s = arguments.length ? i.getValueFromPosition(t) : n,
                    a = i.fetchCaption(s),
                    o = i.getWidthFromValue(s);
                return o += "%", {
                    caption: a,
                    width: o,
                    val: s
                }
            },
            getValueFromPosition: function(t) {
                var i, n, s = this,
                    a = e.getDecimalPlaces(s.step),
                    o = s.$rating.width();
                return n = s.diff * t / (o * s.step), n = s.rtl ? Math.floor(n) : Math.ceil(n), i = e.applyPrecision(parseFloat(s.min + n * s.step), a), i = Math.max(Math.min(i, s.max), s.min), s.rtl ? s.max - i : i
            },
            getWidthFromValue: function(t) {
                var e, i, n = this,
                    s = n.min,
                    a = n.max,
                    o = n.$emptyStars;
                return !t || t <= s || s === a ? 0 : (i = o.outerWidth(), e = i ? o.width() / i : 1, t >= a ? 100 : (t - s) * e * 100 / (a - s))
            },
            fetchCaption: function(t) {
                var i, n, s, a, o, r = this,
                    l = parseFloat(t) || r.clearValue,
                    c = r.starCaptions,
                    u = r.starCaptionClasses;
                return l && l !== r.clearValue && (l = e.applyPrecision(l, e.getDecimalPlaces(r.step))), a = "function" == typeof u ? u(l) : u[l], s = "function" == typeof c ? c(l) : c[l], n = e.isEmpty(s) ? r.defaultCaption.replace(/\{rating}/g, l) : s, i = e.isEmpty(a) ? r.clearCaptionClass : a, o = l === r.clearValue ? r.clearCaption : n, '<span class="' + i + '">' + o + "</span>"
            },
            destroy: function() {
                var i = this,
                    n = i.$element;
                return e.isEmpty(i.$container) || i.$container.before(n).remove(), t.removeData(n.get(0)), n.off("rating").removeClass("rating rating-input")
            },
            create: function(t) {
                var e = this,
                    i = t || e.options || {};
                return e.destroy().rating(i)
            },
            clear: function() {
                var t = this,
                    e = '<span class="' + t.clearCaptionClass + '">' + t.clearCaption + "</span>";
                return t.inactive || t._setCaption(e), t.showStars(t.clearValue).trigger("change").trigger("rating.clear")
            },
            reset: function() {
                var t = this;
                return t.showStars(t.initialValue).trigger("rating.reset")
            },
            update: function(t) {
                var e = this;
                return arguments.length ? e.showStars(t) : e.$element
            },
            refresh: function(e) {
                var i = this,
                    n = i.$element;
                return e ? i.destroy().rating(t.extend(!0, i.options, e)).trigger("rating.refresh") : n
            }
        }, t.fn.rating = function(n) {
            var s = Array.apply(null, arguments),
                a = [];
            switch (s.shift(), this.each(function() {
                var o, r = t(this),
                    l = r.data("rating"),
                    c = "object" == typeof n && n,
                    u = c.theme || r.data("theme"),
                    d = c.language || r.data("language") || "en",
                    h = {},
                    p = {};
                l || (u && (h = t.fn.ratingThemes[u] || {}), "en" === d || e.isEmpty(t.fn.ratingLocales[d]) || (p = t.fn.ratingLocales[d]), o = t.extend(!0, {}, t.fn.rating.defaults, h, t.fn.ratingLocales.en, p, c, r.data()), l = new i(this, o), r.data("rating", l)), "string" == typeof n && a.push(l[n].apply(l, s))
            }), a.length) {
                case 0:
                    return this;
                case 1:
                    return a[0] === undefined ? this : a[0];
                default:
                    return a
            }
        }, t.fn.rating.defaults = {
            theme: "",
            language: "en",
            stars: 5,
            filledStar: '<i class="glyphicon glyphicon-star"></i>',
            emptyStar: '<i class="glyphicon glyphicon-star-empty"></i>',
            containerClass: "",
            size: "md",
            animate: !0,
            displayOnly: !1,
            rtl: !1,
            showClear: !0,
            showCaption: !0,
            starCaptionClasses: {
                .5: "label label-success",
                1: "label label-success",
                1.5: "label label-success",
                2: "label label-success",
                2.5: "label label-success",
                3: "label label-success",
                3.5: "label label-success",
                4: "label label-success",
                4.5: "label label-success",
                5: "label label-success"
            },
            clearButton: '<i class="glyphicon glyphicon-minus-sign"></i>',
            clearButtonBaseClass: "clear-rating",
            clearButtonActiveClass: "clear-rating-active",
            clearCaptionClass: "label label-default",
            clearValue: null,
            captionElement: null,
            clearElement: null,
            hoverEnabled: !0,
            hoverChangeCaption: !0,
            hoverChangeStars: !0,
            hoverOnClear: !0,
            zeroAsNull: !0
        }, t.fn.ratingLocales.en = {
            defaultCaption: "{rating} Stars",
            starCaptions: {
                .5: "0.5",
                1: "1",
                1.5: "1.5",
                2: "2",
                2.5: "2.5",
                3: "3",
                3.5: "3.5",
                4: "4",
                4.5: "4.5",
                5: "5"
            },
            clearButtonTitle: "Clear",
            clearCaption: "0"
        }, t.fn.rating.Constructor = i, t(document).ready(function() {
            var e = t("input.rating");
            e.length && e.removeClass("rating-loading").addClass("rating-loading").rating()
        })
    }), function(t) {
        "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery)
    }(function(t) {
        return t.ui = t.ui || {}, t.ui.version = "1.12.1"
    }), function(t) {
        "function" == typeof define && define.amd ? define(["jquery", "./version"], t) : t(jQuery)
    }(function(t) {
        return t.ui.keyCode = {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    }), function(t) {
        "function" == typeof define && define.amd ? define(["jquery", "../version", "../keycode"], t) : t(jQuery)
    }(function(t) {
        function e(t) {
            for (var e, i; t.length && t[0] !== document;) {
                if (("absolute" === (e = t.css("position")) || "relative" === e || "fixed" === e) && (i = parseInt(t.css("zIndex"), 10), !isNaN(i) && 0 !== i)) return i;
                t = t.parent()
            }
            return 0
        }

        function i() {
            this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {
                closeText: "Done",
                prevText: "Prev",
                nextText: "Next",
                currentText: "Today",
                monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                weekHeader: "Wk",
                dateFormat: "mm/dd/yy",
                firstDay: 0,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, this._defaults = {
                showOn: "focus",
                showAnim: "fadeIn",
                showOptions: {},
                defaultDate: null,
                appendText: "",
                buttonText: "...",
                buttonImage: "",
                buttonImageOnly: !1,
                hideIfNoPrevNext: !1,
                navigationAsDateFormat: !1,
                gotoCurrent: !1,
                changeMonth: !1,
                changeYear: !1,
                yearRange: "c-10:c+10",
                showOtherMonths: !1,
                selectOtherMonths: !1,
                showWeek: !1,
                calculateWeek: this.iso8601Week,
                shortYearCutoff: "+10",
                minDate: null,
                maxDate: null,
                duration: "fast",
                beforeShowDay: null,
                beforeShow: null,
                onSelect: null,
                onChangeMonthYear: null,
                onClose: null,
                numberOfMonths: 1,
                showCurrentAtPos: 0,
                stepMonths: 1,
                stepBigMonths: 12,
                altField: "",
                altFormat: "",
                constrainInput: !0,
                showButtonPanel: !1,
                autoSize: !1,
                disabled: !1
            }, t.extend(this._defaults, this.regional[""]), this.regional.en = t.extend(!0, {}, this.regional[""]), this.regional["en-US"] = t.extend(!0, {}, this.regional.en), this.dpDiv = n(t("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))
        }

        function n(e) {
            var i = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
            return e.on("mouseout", i, function() {
                t(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).removeClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).removeClass("ui-datepicker-next-hover")
            }).on("mouseover", i, s)
        }

        function s() {
            t.datepicker._isDisabledDatepicker(o.inline ? o.dpDiv.parent()[0] : o.input[0]) || (t(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), t(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).addClass("ui-datepicker-next-hover"))
        }

        function a(e, i) {
            t.extend(e, i);
            for (var n in i) null == i[n] && (e[n] = i[n]);
            return e
        }
        t.extend(t.ui, {
            datepicker: {
                version: "1.12.1"
            }
        });
        var o;
        return t.extend(i.prototype, {
            markerClassName: "hasDatepicker",
            maxRows: 4,
            _widgetDatepicker: function() {
                return this.dpDiv
            },
            setDefaults: function(t) {
                return a(this._defaults, t || {}), this
            },
            _attachDatepicker: function(e, i) {
                var n, s, a;
                n = e.nodeName.toLowerCase(), s = "div" === n || "span" === n, e.id || (this.uuid += 1, e.id = "dp" + this.uuid), a = this._newInst(t(e), s), a.settings = t.extend({}, i || {}), "input" === n ? this._connectDatepicker(e, a) : s && this._inlineDatepicker(e, a)
            },
            _newInst: function(e, i) {
                return {
                    id: e[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1"),
                    input: e,
                    selectedDay: 0,
                    selectedMonth: 0,
                    selectedYear: 0,
                    drawMonth: 0,
                    drawYear: 0,
                    inline: i,
                    dpDiv: i ? n(t("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv
                }
            },
            _connectDatepicker: function(e, i) {
                var n = t(e);
                i.append = t([]), i.trigger = t([]), n.hasClass(this.markerClassName) || (this._attachments(n, i), n.addClass(this.markerClassName).on("keydown", this._doKeyDown).on("keypress", this._doKeyPress).on("keyup", this._doKeyUp), this._autoSize(i), t.data(e, "datepicker", i), i.settings.disabled && this._disableDatepicker(e))
            },
            _attachments: function(e, i) {
                var n, s, a, o = this._get(i, "appendText"),
                    r = this._get(i, "isRTL");
                i.append && i.append.remove(), o && (i.append = t("<span class='" + this._appendClass + "'>" + o + "</span>"), e[r ? "before" : "after"](i.append)), e.off("focus", this._showDatepicker), i.trigger && i.trigger.remove(), n = this._get(i, "showOn"), "focus" !== n && "both" !== n || e.on("focus", this._showDatepicker), "button" !== n && "both" !== n || (s = this._get(i, "buttonText"), a = this._get(i, "buttonImage"), i.trigger = t(this._get(i, "buttonImageOnly") ? t("<img/>").addClass(this._triggerClass).attr({
                    src: a,
                    alt: s,
                    title: s
                }) : t("<button type='button'></button>").addClass(this._triggerClass).html(a ? t("<img/>").attr({
                    src: a,
                    alt: s,
                    title: s
                }) : s)), e[r ? "before" : "after"](i.trigger), i.trigger.on("click", function() {
                    return t.datepicker._datepickerShowing && t.datepicker._lastInput === e[0] ? t.datepicker._hideDatepicker() : t.datepicker._datepickerShowing && t.datepicker._lastInput !== e[0] ? (t.datepicker._hideDatepicker(), t.datepicker._showDatepicker(e[0])) : t.datepicker._showDatepicker(e[0]), !1
                }))
            },
            _autoSize: function(t) {
                if (this._get(t, "autoSize") && !t.inline) {
                    var e, i, n, s, a = new Date(2009, 11, 20),
                        o = this._get(t, "dateFormat");
                    o.match(/[DM]/) && (e = function(t) {
                        for (i = 0, n = 0, s = 0; s < t.length; s++) t[s].length > i && (i = t[s].length, n = s);
                        return n
                    }, a.setMonth(e(this._get(t, o.match(/MM/) ? "monthNames" : "monthNamesShort"))), a.setDate(e(this._get(t, o.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - a.getDay())), t.input.attr("size", this._formatDate(t, a).length)
                }
            },
            _inlineDatepicker: function(e, i) {
                var n = t(e);
                n.hasClass(this.markerClassName) || (n.addClass(this.markerClassName).append(i.dpDiv), t.data(e, "datepicker", i), this._setDate(i, this._getDefaultDate(i), !0), this._updateDatepicker(i), this._updateAlternate(i), i.settings.disabled && this._disableDatepicker(e), i.dpDiv.css("display", "block"))
            },
            _dialogDatepicker: function(e, i, n, s, o) {
                var r, l, c, u, d, h = this._dialogInst;
                return h || (this.uuid += 1, r = "dp" + this.uuid, this._dialogInput = t("<input type='text' id='" + r + "' style='position: absolute; top: -100px; width: 0px;'/>"), this._dialogInput.on("keydown", this._doKeyDown), t("body").append(this._dialogInput), h = this._dialogInst = this._newInst(this._dialogInput, !1), h.settings = {}, t.data(this._dialogInput[0], "datepicker", h)), a(h.settings, s || {}), i = i && i.constructor === Date ? this._formatDate(h, i) : i, this._dialogInput.val(i), this._pos = o ? o.length ? o : [o.pageX, o.pageY] : null, this._pos || (l = document.documentElement.clientWidth, c = document.documentElement.clientHeight, u = document.documentElement.scrollLeft || document.body.scrollLeft, d = document.documentElement.scrollTop || document.body.scrollTop, this._pos = [l / 2 - 100 + u, c / 2 - 150 + d]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), h.settings.onSelect = n, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), t.blockUI && t.blockUI(this.dpDiv), t.data(this._dialogInput[0], "datepicker", h), this
            },
            _destroyDatepicker: function(e) {
                var i, n = t(e),
                    s = t.data(e, "datepicker");
                n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), t.removeData(e, "datepicker"), "input" === i ? (s.append.remove(), s.trigger.remove(), n.removeClass(this.markerClassName).off("focus", this._showDatepicker).off("keydown", this._doKeyDown).off("keypress", this._doKeyPress).off("keyup", this._doKeyUp)) : "div" !== i && "span" !== i || n.removeClass(this.markerClassName).empty(), o === s && (o = null))
            },
            _enableDatepicker: function(e) {
                var i, n, s = t(e),
                    a = t.data(e, "datepicker");
                s.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !1, a.trigger.filter("button").each(function() {
                    this.disabled = !1
                }).end().filter("img").css({
                    opacity: "1.0",
                    cursor: ""
                })) : "div" !== i && "span" !== i || (n = s.children("." + this._inlineClass), n.children().removeClass("ui-state-disabled"), n.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), this._disabledInputs = t.map(this._disabledInputs, function(t) {
                    return t === e ? null : t
                }))
            },
            _disableDatepicker: function(e) {
                var i, n, s = t(e),
                    a = t.data(e, "datepicker");
                s.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !0, a.trigger.filter("button").each(function() {
                    this.disabled = !0
                }).end().filter("img").css({
                    opacity: "0.5",
                    cursor: "default"
                })) : "div" !== i && "span" !== i || (n = s.children("." + this._inlineClass), n.children().addClass("ui-state-disabled"), n.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), this._disabledInputs = t.map(this._disabledInputs, function(t) {
                    return t === e ? null : t
                }), this._disabledInputs[this._disabledInputs.length] = e)
            },
            _isDisabledDatepicker: function(t) {
                if (!t) return !1;
                for (var e = 0; e < this._disabledInputs.length; e++)
                    if (this._disabledInputs[e] === t) return !0;
                return !1
            },
            _getInst: function(e) {
                try {
                    return t.data(e, "datepicker")
                } catch (t) {
                    throw "Missing instance data for this datepicker"
                }
            },
            _optionDatepicker: function(e, i, n) {
                var s, o, r, l, c = this._getInst(e);
                if (2 === arguments.length && "string" == typeof i) return "defaults" === i ? t.extend({}, t.datepicker._defaults) : c ? "all" === i ? t.extend({}, c.settings) : this._get(c, i) : null;
                s = i || {}, "string" == typeof i && (s = {}, s[i] = n), c && (this._curInst === c && this._hideDatepicker(), o = this._getDateDatepicker(e, !0), r = this._getMinMaxDate(c, "min"), l = this._getMinMaxDate(c, "max"), a(c.settings, s), null !== r && s.dateFormat !== undefined && s.minDate === undefined && (c.settings.minDate = this._formatDate(c, r)), null !== l && s.dateFormat !== undefined && s.maxDate === undefined && (c.settings.maxDate = this._formatDate(c, l)), "disabled" in s && (s.disabled ? this._disableDatepicker(e) : this._enableDatepicker(e)), this._attachments(t(e), c), this._autoSize(c), this._setDate(c, o), this._updateAlternate(c), this._updateDatepicker(c))
            },
            _changeDatepicker: function(t, e, i) {
                this._optionDatepicker(t, e, i)
            },
            _refreshDatepicker: function(t) {
                var e = this._getInst(t);
                e && this._updateDatepicker(e)
            },
            _setDateDatepicker: function(t, e) {
                var i = this._getInst(t);
                i && (this._setDate(i, e), this._updateDatepicker(i), this._updateAlternate(i))
            },
            _getDateDatepicker: function(t, e) {
                var i = this._getInst(t);
                return i && !i.inline && this._setDateFromField(i, e), i ? this._getDate(i) : null
            },
            _doKeyDown: function(e) {
                var i, n, s, a = t.datepicker._getInst(e.target),
                    o = !0,
                    r = a.dpDiv.is(".ui-datepicker-rtl");
                if (a._keyEvent = !0, t.datepicker._datepickerShowing) switch (e.keyCode) {
                    case 9:
                        t.datepicker._hideDatepicker(), o = !1;
                        break;
                    case 13:
                        return s = t("td." + t.datepicker._dayOverClass + ":not(." + t.datepicker._currentClass + ")", a.dpDiv), s[0] && t.datepicker._selectDay(e.target, a.selectedMonth, a.selectedYear, s[0]), i = t.datepicker._get(a, "onSelect"), i ? (n = t.datepicker._formatDate(a), i.apply(a.input ? a.input[0] : null, [n, a])) : t.datepicker._hideDatepicker(), !1;
                    case 27:
                        t.datepicker._hideDatepicker();
                        break;
                    case 33:
                        t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(a, "stepBigMonths") : -t.datepicker._get(a, "stepMonths"), "M");
                        break;
                    case 34:
                        t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(a, "stepBigMonths") : +t.datepicker._get(a, "stepMonths"), "M");
                        break;
                    case 35:
                        (e.ctrlKey || e.metaKey) && t.datepicker._clearDate(e.target), o = e.ctrlKey || e.metaKey;
                        break;
                    case 36:
                        (e.ctrlKey || e.metaKey) && t.datepicker._gotoToday(e.target), o = e.ctrlKey || e.metaKey;
                        break;
                    case 37:
                        (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, r ? 1 : -1, "D"), o = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(a, "stepBigMonths") : -t.datepicker._get(a, "stepMonths"), "M");
                        break;
                    case 38:
                        (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, -7, "D"), o = e.ctrlKey || e.metaKey;
                        break;
                    case 39:
                        (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, r ? -1 : 1, "D"), o = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(a, "stepBigMonths") : +t.datepicker._get(a, "stepMonths"), "M");
                        break;
                    case 40:
                        (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, 7, "D"), o = e.ctrlKey || e.metaKey;
                        break;
                    default:
                        o = !1
                } else 36 === e.keyCode && e.ctrlKey ? t.datepicker._showDatepicker(this) : o = !1;
                o && (e.preventDefault(), e.stopPropagation())
            },
            _doKeyPress: function(e) {
                var i, n, s = t.datepicker._getInst(e.target);
                if (t.datepicker._get(s, "constrainInput")) return i = t.datepicker._possibleChars(t.datepicker._get(s, "dateFormat")), n = String.fromCharCode(null == e.charCode ? e.keyCode : e.charCode), e.ctrlKey || e.metaKey || n < " " || !i || i.indexOf(n) > -1
            },
            _doKeyUp: function(e) {
                var i, n = t.datepicker._getInst(e.target);
                if (n.input.val() !== n.lastVal) try {
                    i = t.datepicker.parseDate(t.datepicker._get(n, "dateFormat"), n.input ? n.input.val() : null, t.datepicker._getFormatConfig(n)), i && (t.datepicker._setDateFromField(n), t.datepicker._updateAlternate(n), t.datepicker._updateDatepicker(n))
                } catch (t) {}
                return !0
            },
            _showDatepicker: function(i) {
                if (i = i.target || i, "input" !== i.nodeName.toLowerCase() && (i = t("input", i.parentNode)[0]), !t.datepicker._isDisabledDatepicker(i) && t.datepicker._lastInput !== i) {
                    var n, s, o, r, l, c, u;
                    n = t.datepicker._getInst(i), t.datepicker._curInst && t.datepicker._curInst !== n && (t.datepicker._curInst.dpDiv.stop(!0, !0), n && t.datepicker._datepickerShowing && t.datepicker._hideDatepicker(t.datepicker._curInst.input[0])), s = t.datepicker._get(n, "beforeShow"), o = s ? s.apply(i, [i, n]) : {}, !1 !== o && (a(n.settings, o), n.lastVal = null, t.datepicker._lastInput = i, t.datepicker._setDateFromField(n), t.datepicker._inDialog && (i.value = ""), t.datepicker._pos || (t.datepicker._pos = t.datepicker._findPos(i), t.datepicker._pos[1] += i.offsetHeight), r = !1, t(i).parents().each(function() {
                        return !(r |= "fixed" === t(this).css("position"))
                    }), l = {
                        left: t.datepicker._pos[0],
                        top: t.datepicker._pos[1]
                    }, t.datepicker._pos = null, n.dpDiv.empty(), n.dpDiv.css({
                        position: "absolute",
                        display: "block",
                        top: "-1000px"
                    }), t.datepicker._updateDatepicker(n), l = t.datepicker._checkOffset(n, l, r), n.dpDiv.css({
                        position: t.datepicker._inDialog && t.blockUI ? "static" : r ? "fixed" : "absolute",
                        display: "none",
                        left: l.left + "px",
                        top: l.top + "px"
                    }), n.inline || (c = t.datepicker._get(n, "showAnim"), u = t.datepicker._get(n, "duration"), n.dpDiv.css("z-index", e(t(i)) + 1), t.datepicker._datepickerShowing = !0, t.effects && t.effects.effect[c] ? n.dpDiv.show(c, t.datepicker._get(n, "showOptions"), u) : n.dpDiv[c || "show"](c ? u : null), t.datepicker._shouldFocusInput(n) && n.input.trigger("focus"), t.datepicker._curInst = n))
                }
            },
            _updateDatepicker: function(e) {
                this.maxRows = 4, o = e, e.dpDiv.empty().append(this._generateHTML(e)), this._attachHandlers(e);
                var i, n = this._getNumberOfMonths(e),
                    a = n[1],
                    r = e.dpDiv.find("." + this._dayOverClass + " a");
                r.length > 0 && s.apply(r.get(0)), e.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), a > 1 && e.dpDiv.addClass("ui-datepicker-multi-" + a).css("width", 17 * a + "em"), e.dpDiv[(1 !== n[0] || 1 !== n[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), e.dpDiv[(this._get(e, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), e === t.datepicker._curInst && t.datepicker._datepickerShowing && t.datepicker._shouldFocusInput(e) && e.input.trigger("focus"), e.yearshtml && (i = e.yearshtml, setTimeout(function() {
                    i === e.yearshtml && e.yearshtml && e.dpDiv.find("select.ui-datepicker-year:first").replaceWith(e.yearshtml), i = e.yearshtml = null
                }, 0))
            },
            _shouldFocusInput: function(t) {
                return t.input && t.input.is(":visible") && !t.input.is(":disabled") && !t.input.is(":focus")
            },
            _checkOffset: function(e, i, n) {
                var s = e.dpDiv.outerWidth(),
                    a = e.dpDiv.outerHeight(),
                    o = e.input ? e.input.outerWidth() : 0,
                    r = e.input ? e.input.outerHeight() : 0,
                    l = document.documentElement.clientWidth + (n ? 0 : t(document).scrollLeft()),
                    c = document.documentElement.clientHeight + (n ? 0 : t(document).scrollTop());
                return i.left -= this._get(e, "isRTL") ? s - o : 0, i.left -= n && i.left === e.input.offset().left ? t(document).scrollLeft() : 0, i.top -= n && i.top === e.input.offset().top + r ? t(document).scrollTop() : 0, i.left -= Math.min(i.left, i.left + s > l && l > s ? Math.abs(i.left + s - l) : 0), i.top -= Math.min(i.top, i.top + a > c && c > a ? Math.abs(a + r) : 0), i
            },
            _findPos: function(e) {
                for (var i, n = this._getInst(e), s = this._get(n, "isRTL"); e && ("hidden" === e.type || 1 !== e.nodeType || t.expr.filters.hidden(e));) e = e[s ? "previousSibling" : "nextSibling"];
                return i = t(e).offset(), [i.left, i.top]
            },
            _hideDatepicker: function(e) {
                var i, n, s, a, o = this._curInst;
                !o || e && o !== t.data(e, "datepicker") || this._datepickerShowing && (i = this._get(o, "showAnim"), n = this._get(o, "duration"), s = function() {
                    t.datepicker._tidyDialog(o)
                }, t.effects && (t.effects.effect[i] || t.effects[i]) ? o.dpDiv.hide(i, t.datepicker._get(o, "showOptions"), n, s) : o.dpDiv["slideDown" === i ? "slideUp" : "fadeIn" === i ? "fadeOut" : "hide"](i ? n : null, s), i || s(), this._datepickerShowing = !1, a = this._get(o, "onClose"), a && a.apply(o.input ? o.input[0] : null, [o.input ? o.input.val() : "", o]), this._lastInput = null, this._inDialog && (this._dialogInput.css({
                    position: "absolute",
                    left: "0",
                    top: "-100px"
                }), t.blockUI && (t.unblockUI(), t("body").append(this.dpDiv))), this._inDialog = !1)
            },
            _tidyDialog: function(t) {
                t.dpDiv.removeClass(this._dialogClass).off(".ui-datepicker-calendar")
            },
            _checkExternalClick: function(e) {
                if (t.datepicker._curInst) {
                    var i = t(e.target),
                        n = t.datepicker._getInst(i[0]);
                    (i[0].id === t.datepicker._mainDivId || 0 !== i.parents("#" + t.datepicker._mainDivId).length || i.hasClass(t.datepicker.markerClassName) || i.closest("." + t.datepicker._triggerClass).length || !t.datepicker._datepickerShowing || t.datepicker._inDialog && t.blockUI) && (!i.hasClass(t.datepicker.markerClassName) || t.datepicker._curInst === n) || t.datepicker._hideDatepicker()
                }
            },
            _adjustDate: function(e, i, n) {
                var s = t(e),
                    a = this._getInst(s[0]);
                this._isDisabledDatepicker(s[0]) || (this._adjustInstDate(a, i + ("M" === n ? this._get(a, "showCurrentAtPos") : 0), n), this._updateDatepicker(a))
            },
            _gotoToday: function(e) {
                var i, n = t(e),
                    s = this._getInst(n[0]);
                this._get(s, "gotoCurrent") && s.currentDay ? (s.selectedDay = s.currentDay, s.drawMonth = s.selectedMonth = s.currentMonth, s.drawYear = s.selectedYear = s.currentYear) : (i = new Date, s.selectedDay = i.getDate(), s.drawMonth = s.selectedMonth = i.getMonth(), s.drawYear = s.selectedYear = i.getFullYear()), this._notifyChange(s), this._adjustDate(n)
            },
            _selectMonthYear: function(e, i, n) {
                var s = t(e),
                    a = this._getInst(s[0]);
                a["selected" + ("M" === n ? "Month" : "Year")] = a["draw" + ("M" === n ? "Month" : "Year")] = parseInt(i.options[i.selectedIndex].value, 10), this._notifyChange(a), this._adjustDate(s)
            },
            _selectDay: function(e, i, n, s) {
                var a, o = t(e);
                t(s).hasClass(this._unselectableClass) || this._isDisabledDatepicker(o[0]) || (a = this._getInst(o[0]), a.selectedDay = a.currentDay = t("a", s).html(), a.selectedMonth = a.currentMonth = i, a.selectedYear = a.currentYear = n, this._selectDate(e, this._formatDate(a, a.currentDay, a.currentMonth, a.currentYear)))
            },
            _clearDate: function(e) {
                var i = t(e);
                this._selectDate(i, "")
            },
            _selectDate: function(e, i) {
                var n, s = t(e),
                    a = this._getInst(s[0]);
                i = null != i ? i : this._formatDate(a), a.input && a.input.val(i), this._updateAlternate(a), n = this._get(a, "onSelect"), n ? n.apply(a.input ? a.input[0] : null, [i, a]) : a.input && a.input.trigger("change"), a.inline ? this._updateDatepicker(a) : (this._hideDatepicker(), this._lastInput = a.input[0], "object" != typeof a.input[0] && a.input.trigger("focus"), this._lastInput = null)
            },
            _updateAlternate: function(e) {
                var i, n, s, a = this._get(e, "altField");
                a && (i = this._get(e, "altFormat") || this._get(e, "dateFormat"), n = this._getDate(e), s = this.formatDate(i, n, this._getFormatConfig(e)), t(a).val(s))
            },
            noWeekends: function(t) {
                var e = t.getDay();
                return [e > 0 && e < 6, ""]
            },
            iso8601Week: function(t) {
                var e, i = new Date(t.getTime());
                return i.setDate(i.getDate() + 4 - (i.getDay() || 7)), e = i.getTime(), i.setMonth(0), i.setDate(1), Math.floor(Math.round((e - i) / 864e5) / 7) + 1
            },
            parseDate: function(e, i, n) {
                if (null == e || null == i) throw "Invalid arguments";
                if ("" === (i = "object" == typeof i ? i.toString() : i + "")) return null;
                var s, a, o, r, l = 0,
                    c = (n ? n.shortYearCutoff : null) || this._defaults.shortYearCutoff,
                    u = "string" != typeof c ? c : (new Date).getFullYear() % 100 + parseInt(c, 10),
                    d = (n ? n.dayNamesShort : null) || this._defaults.dayNamesShort,
                    h = (n ? n.dayNames : null) || this._defaults.dayNames,
                    p = (n ? n.monthNamesShort : null) || this._defaults.monthNamesShort,
                    f = (n ? n.monthNames : null) || this._defaults.monthNames,
                    m = -1,
                    g = -1,
                    _ = -1,
                    v = -1,
                    b = !1,
                    y = function(t) {
                        var i = s + 1 < e.length && e.charAt(s + 1) === t;
                        return i && s++, i
                    },
                    w = function(t) {
                        var e = y(t),
                            n = "@" === t ? 14 : "!" === t ? 20 : "y" === t && e ? 4 : "o" === t ? 3 : 2,
                            s = "y" === t ? n : 1,
                            a = new RegExp("^\\d{" + s + "," + n + "}"),
                            o = i.substring(l).match(a);
                        if (!o) throw "Missing number at position " + l;
                        return l += o[0].length, parseInt(o[0], 10)
                    },
                    x = function(e, n, s) {
                        var a = -1,
                            o = t.map(y(e) ? s : n, function(t, e) {
                                return [
                                    [e, t]
                                ]
                            }).sort(function(t, e) {
                                return -(t[1].length - e[1].length)
                            });
                        if (t.each(o, function(t, e) {
                                var n = e[1];
                                if (i.substr(l, n.length).toLowerCase() === n.toLowerCase()) return a = e[0], l += n.length, !1
                            }), -1 !== a) return a + 1;
                        throw "Unknown name at position " + l
                    },
                    $ = function() {
                        if (i.charAt(l) !== e.charAt(s)) throw "Unexpected literal at position " + l;
                        l++
                    };
                for (s = 0; s < e.length; s++)
                    if (b) "'" !== e.charAt(s) || y("'") ? $() : b = !1;
                    else switch (e.charAt(s)) {
                        case "d":
                            _ = w("d");
                            break;
                        case "D":
                            x("D", d, h);
                            break;
                        case "o":
                            v = w("o");
                            break;
                        case "m":
                            g = w("m");
                            break;
                        case "M":
                            g = x("M", p, f);
                            break;
                        case "y":
                            m = w("y");
                            break;
                        case "@":
                            r = new Date(w("@")), m = r.getFullYear(), g = r.getMonth() + 1, _ = r.getDate();
                            break;
                        case "!":
                            r = new Date((w("!") - this._ticksTo1970) / 1e4), m = r.getFullYear(), g = r.getMonth() + 1, _ = r.getDate();
                            break;
                        case "'":
                            y("'") ? $() : b = !0;
                            break;
                        default:
                            $()
                    }
                if (l < i.length && (o = i.substr(l), !/^\s+/.test(o))) throw "Extra/unparsed characters found in date: " + o;
                if (-1 === m ? m = (new Date).getFullYear() : m < 100 && (m += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (m <= u ? 0 : -100)), v > -1)
                    for (g = 1, _ = v;;) {
                        if (a = this._getDaysInMonth(m, g - 1), _ <= a) break;
                        g++, _ -= a
                    }
                if (r = this._daylightSavingAdjust(new Date(m, g - 1, _)), r.getFullYear() !== m || r.getMonth() + 1 !== g || r.getDate() !== _) throw "Invalid date";
                return r
            },
            ATOM: "yy-mm-dd",
            COOKIE: "D, dd M yy",
            ISO_8601: "yy-mm-dd",
            RFC_822: "D, d M y",
            RFC_850: "DD, dd-M-y",
            RFC_1036: "D, d M y",
            RFC_1123: "D, d M yy",
            RFC_2822: "D, d M yy",
            RSS: "D, d M y",
            TICKS: "!",
            TIMESTAMP: "@",
            W3C: "yy-mm-dd",
            _ticksTo1970: 24 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)) * 60 * 60 * 1e7,
            formatDate: function(t, e, i) {
                if (!e) return "";
                var n, s = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort,
                    a = (i ? i.dayNames : null) || this._defaults.dayNames,
                    o = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort,
                    r = (i ? i.monthNames : null) || this._defaults.monthNames,
                    l = function(e) {
                        var i = n + 1 < t.length && t.charAt(n + 1) === e;
                        return i && n++, i
                    },
                    c = function(t, e, i) {
                        var n = "" + e;
                        if (l(t))
                            for (; n.length < i;) n = "0" + n;
                        return n
                    },
                    u = function(t, e, i, n) {
                        return l(t) ? n[e] : i[e]
                    },
                    d = "",
                    h = !1;
                if (e)
                    for (n = 0; n < t.length; n++)
                        if (h) "'" !== t.charAt(n) || l("'") ? d += t.charAt(n) : h = !1;
                        else switch (t.charAt(n)) {
                            case "d":
                                d += c("d", e.getDate(), 2);
                                break;
                            case "D":
                                d += u("D", e.getDay(), s, a);
                                break;
                            case "o":
                                d += c("o", Math.round((new Date(e.getFullYear(), e.getMonth(), e.getDate()).getTime() - new Date(e.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                                break;
                            case "m":
                                d += c("m", e.getMonth() + 1, 2);
                                break;
                            case "M":
                                d += u("M", e.getMonth(), o, r);
                                break;
                            case "y":
                                d += l("y") ? e.getFullYear() : (e.getFullYear() % 100 < 10 ? "0" : "") + e.getFullYear() % 100;
                                break;
                            case "@":
                                d += e.getTime();
                                break;
                            case "!":
                                d += 1e4 * e.getTime() + this._ticksTo1970;
                                break;
                            case "'":
                                l("'") ? d += "'" : h = !0;
                                break;
                            default:
                                d += t.charAt(n)
                        }
                return d
            },
            _possibleChars: function(t) {
                var e, i = "",
                    n = !1,
                    s = function(i) {
                        var n = e + 1 < t.length && t.charAt(e + 1) === i;
                        return n && e++, n
                    };
                for (e = 0; e < t.length; e++)
                    if (n) "'" !== t.charAt(e) || s("'") ? i += t.charAt(e) : n = !1;
                    else switch (t.charAt(e)) {
                        case "d":
                        case "m":
                        case "y":
                        case "@":
                            i += "0123456789";
                            break;
                        case "D":
                        case "M":
                            return null;
                        case "'":
                            s("'") ? i += "'" : n = !0;
                            break;
                        default:
                            i += t.charAt(e)
                    }
                return i
            },
            _get: function(t, e) {
                return t.settings[e] !== undefined ? t.settings[e] : this._defaults[e]
            },
            _setDateFromField: function(t, e) {
                if (t.input.val() !== t.lastVal) {
                    var i = this._get(t, "dateFormat"),
                        n = t.lastVal = t.input ? t.input.val() : null,
                        s = this._getDefaultDate(t),
                        a = s,
                        o = this._getFormatConfig(t);
                    try {
                        a = this.parseDate(i, n, o) || s
                    } catch (t) {
                        n = e ? "" : n
                    }
                    t.selectedDay = a.getDate(), t.drawMonth = t.selectedMonth = a.getMonth(), t.drawYear = t.selectedYear = a.getFullYear(), t.currentDay = n ? a.getDate() : 0, t.currentMonth = n ? a.getMonth() : 0, t.currentYear = n ? a.getFullYear() : 0, this._adjustInstDate(t)
                }
            },
            _getDefaultDate: function(t) {
                return this._restrictMinMax(t, this._determineDate(t, this._get(t, "defaultDate"), new Date))
            },
            _determineDate: function(e, i, n) {
                var s = function(t) {
                        var e = new Date;
                        return e.setDate(e.getDate() + t), e
                    },
                    a = function(i) {
                        try {
                            return t.datepicker.parseDate(t.datepicker._get(e, "dateFormat"), i, t.datepicker._getFormatConfig(e))
                        } catch (t) {}
                        for (var n = (i.toLowerCase().match(/^c/) ? t.datepicker._getDate(e) : null) || new Date, s = n.getFullYear(), a = n.getMonth(), o = n.getDate(), r = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, l = r.exec(i); l;) {
                            switch (l[2] || "d") {
                                case "d":
                                case "D":
                                    o += parseInt(l[1], 10);
                                    break;
                                case "w":
                                case "W":
                                    o += 7 * parseInt(l[1], 10);
                                    break;
                                case "m":
                                case "M":
                                    a += parseInt(l[1], 10), o = Math.min(o, t.datepicker._getDaysInMonth(s, a));
                                    break;
                                case "y":
                                case "Y":
                                    s += parseInt(l[1], 10), o = Math.min(o, t.datepicker._getDaysInMonth(s, a))
                            }
                            l = r.exec(i)
                        }
                        return new Date(s, a, o)
                    },
                    o = null == i || "" === i ? n : "string" == typeof i ? a(i) : "number" == typeof i ? isNaN(i) ? n : s(i) : new Date(i.getTime());
                return o = o && "Invalid Date" === o.toString() ? n : o, o && (o.setHours(0), o.setMinutes(0), o.setSeconds(0), o.setMilliseconds(0)), this._daylightSavingAdjust(o)
            },
            _daylightSavingAdjust: function(t) {
                return t ? (t.setHours(t.getHours() > 12 ? t.getHours() + 2 : 0), t) : null
            },
            _setDate: function(t, e, i) {
                var n = !e,
                    s = t.selectedMonth,
                    a = t.selectedYear,
                    o = this._restrictMinMax(t, this._determineDate(t, e, new Date));
                t.selectedDay = t.currentDay = o.getDate(), t.drawMonth = t.selectedMonth = t.currentMonth = o.getMonth(), t.drawYear = t.selectedYear = t.currentYear = o.getFullYear(), s === t.selectedMonth && a === t.selectedYear || i || this._notifyChange(t), this._adjustInstDate(t), t.input && t.input.val(n ? "" : this._formatDate(t))
            },
            _getDate: function(t) {
                return !t.currentYear || t.input && "" === t.input.val() ? null : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay))
            },
            _attachHandlers: function(e) {
                var i = this._get(e, "stepMonths"),
                    n = "#" + e.id.replace(/\\\\/g, "\\");
                e.dpDiv.find("[data-handler]").map(function() {
                    var e = {
                        prev: function() {
                            t.datepicker._adjustDate(n, -i, "M")
                        },
                        next: function() {
                            t.datepicker._adjustDate(n, +i, "M")
                        },
                        hide: function() {
                            t.datepicker._hideDatepicker()
                        },
                        today: function() {
                            t.datepicker._gotoToday(n)
                        },
                        selectDay: function() {
                            return t.datepicker._selectDay(n, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1
                        },
                        selectMonth: function() {
                            return t.datepicker._selectMonthYear(n, this, "M"), !1
                        },
                        selectYear: function() {
                            return t.datepicker._selectMonthYear(n, this, "Y"), !1
                        }
                    };
                    t(this).on(this.getAttribute("data-event"), e[this.getAttribute("data-handler")])
                })
            },
            _generateHTML: function(t) {
                var e, i, n, s, a, o, r, l, c, u, d, h, p, f, m, g, _, v, b, y, w, x, $, k, C, D, S, T, I, A, E, M, P, N, F, j, L, H, O, R = new Date,
                    z = this._daylightSavingAdjust(new Date(R.getFullYear(), R.getMonth(), R.getDate())),
                    W = this._get(t, "isRTL"),
                    B = this._get(t, "showButtonPanel"),
                    q = this._get(t, "hideIfNoPrevNext"),
                    U = this._get(t, "navigationAsDateFormat"),
                    Y = this._getNumberOfMonths(t),
                    V = this._get(t, "showCurrentAtPos"),
                    K = this._get(t, "stepMonths"),
                    X = 1 !== Y[0] || 1 !== Y[1],
                    Q = this._daylightSavingAdjust(t.currentDay ? new Date(t.currentYear, t.currentMonth, t.currentDay) : new Date(9999, 9, 9)),
                    G = this._getMinMaxDate(t, "min"),
                    J = this._getMinMaxDate(t, "max"),
                    Z = t.drawMonth - V,
                    tt = t.drawYear;
                if (Z < 0 && (Z += 12, tt--), J)
                    for (e = this._daylightSavingAdjust(new Date(J.getFullYear(), J.getMonth() - Y[0] * Y[1] + 1, J.getDate())), e = G && e < G ? G : e; this._daylightSavingAdjust(new Date(tt, Z, 1)) > e;) --Z < 0 && (Z = 11, tt--);
                for (t.drawMonth = Z, t.drawYear = tt, i = this._get(t, "prevText"), i = U ? this.formatDate(i, this._daylightSavingAdjust(new Date(tt, Z - K, 1)), this._getFormatConfig(t)) : i, n = this._canAdjustMonth(t, -1, tt, Z) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (W ? "e" : "w") + "'>" + i + "</span></a>" : q ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (W ? "e" : "w") + "'>" + i + "</span></a>", s = this._get(t, "nextText"), s = U ? this.formatDate(s, this._daylightSavingAdjust(new Date(tt, Z + K, 1)), this._getFormatConfig(t)) : s, a = this._canAdjustMonth(t, 1, tt, Z) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + s + "'><span class='ui-icon ui-icon-circle-triangle-" + (W ? "w" : "e") + "'>" + s + "</span></a>" : q ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + s + "'><span class='ui-icon ui-icon-circle-triangle-" + (W ? "w" : "e") + "'>" + s + "</span></a>", o = this._get(t, "currentText"), r = this._get(t, "gotoCurrent") && t.currentDay ? Q : z, o = U ? this.formatDate(o, r, this._getFormatConfig(t)) : o, l = t.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(t, "closeText") + "</button>", c = B ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (W ? l : "") + (this._isInRange(t, r) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + o + "</button>" : "") + (W ? "" : l) + "</div>" : "", u = parseInt(this._get(t, "firstDay"), 10), u = isNaN(u) ? 0 : u, d = this._get(t, "showWeek"), h = this._get(t, "dayNames"), p = this._get(t, "dayNamesMin"), f = this._get(t, "monthNames"), m = this._get(t, "monthNamesShort"), g = this._get(t, "beforeShowDay"), _ = this._get(t, "showOtherMonths"), v = this._get(t, "selectOtherMonths"), b = this._getDefaultDate(t), y = "", x = 0; x < Y[0]; x++) {
                    for ($ = "", this.maxRows = 4, k = 0; k < Y[1]; k++) {
                        if (C = this._daylightSavingAdjust(new Date(tt, Z, t.selectedDay)), D = " ui-corner-all", S = "", X) {
                            if (S += "<div class='ui-datepicker-group", Y[1] > 1) switch (k) {
                                case 0:
                                    S += " ui-datepicker-group-first", D = " ui-corner-" + (W ? "right" : "left");
                                    break;
                                case Y[1] - 1:
                                    S += " ui-datepicker-group-last", D = " ui-corner-" + (W ? "left" : "right");
                                    break;
                                default:
                                    S += " ui-datepicker-group-middle", D = ""
                            }
                            S += "'>"
                        }
                        for (S += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + D + "'>" + (/all|left/.test(D) && 0 === x ? W ? a : n : "") + (/all|right/.test(D) && 0 === x ? W ? n : a : "") + this._generateMonthYearHeader(t, Z, tt, G, J, x > 0 || k > 0, f, m) + "</div><table class='ui-datepicker-calendar'><thead><tr>", T = d ? "<th class='ui-datepicker-week-col'>" + this._get(t, "weekHeader") + "</th>" : "", w = 0; w < 7; w++) I = (w + u) % 7, T += "<th scope='col'" + ((w + u + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + "><span title='" + h[I] + "'>" + p[I] + "</span></th>";
                        for (S += T + "</tr></thead><tbody>", A = this._getDaysInMonth(tt, Z), tt === t.selectedYear && Z === t.selectedMonth && (t.selectedDay = Math.min(t.selectedDay, A)), E = (this._getFirstDayOfMonth(tt, Z) - u + 7) % 7, M = Math.ceil((E + A) / 7), P = X && this.maxRows > M ? this.maxRows : M, this.maxRows = P, N = this._daylightSavingAdjust(new Date(tt, Z, 1 - E)), F = 0; F < P; F++) {
                            for (S += "<tr>", j = d ? "<td class='ui-datepicker-week-col'>" + this._get(t, "calculateWeek")(N) + "</td>" : "", w = 0; w < 7; w++) L = g ? g.apply(t.input ? t.input[0] : null, [N]) : [!0, ""], H = N.getMonth() !== Z, O = H && !v || !L[0] || G && N < G || J && N > J, j += "<td class='" + ((w + u + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (H ? " ui-datepicker-other-month" : "") + (N.getTime() === C.getTime() && Z === t.selectedMonth && t._keyEvent || b.getTime() === N.getTime() && b.getTime() === C.getTime() ? " " + this._dayOverClass : "") + (O ? " " + this._unselectableClass + " ui-state-disabled" : "") + (H && !_ ? "" : " " + L[1] + (N.getTime() === Q.getTime() ? " " + this._currentClass : "") + (N.getTime() === z.getTime() ? " ui-datepicker-today" : "")) + "'" + (H && !_ || !L[2] ? "" : " title='" + L[2].replace(/'/g, "&#39;") + "'") + (O ? "" : " data-handler='selectDay' data-event='click' data-month='" + N.getMonth() + "' data-year='" + N.getFullYear() + "'") + ">" + (H && !_ ? "&#xa0;" : O ? "<span class='ui-state-default'>" + N.getDate() + "</span>" : "<a class='ui-state-default" + (N.getTime() === z.getTime() ? " ui-state-highlight" : "") + (N.getTime() === Q.getTime() ? " ui-state-active" : "") + (H ? " ui-priority-secondary" : "") + "' href='#'>" + N.getDate() + "</a>") + "</td>", N.setDate(N.getDate() + 1), N = this._daylightSavingAdjust(N);
                            S += j + "</tr>"
                        }
                        Z++, Z > 11 && (Z = 0, tt++), S += "</tbody></table>" + (X ? "</div>" + (Y[0] > 0 && k === Y[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : ""), $ += S
                    }
                    y += $
                }
                return y += c, t._keyEvent = !1, y
            },
            _generateMonthYearHeader: function(t, e, i, n, s, a, o, r) {
                var l, c, u, d, h, p, f, m, g = this._get(t, "changeMonth"),
                    _ = this._get(t, "changeYear"),
                    v = this._get(t, "showMonthAfterYear"),
                    b = "<div class='ui-datepicker-title'>",
                    y = "";
                if (a || !g) y += "<span class='ui-datepicker-month'>" + o[e] + "</span>";
                else {
                    for (l = n && n.getFullYear() === i, c = s && s.getFullYear() === i, y += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", u = 0; u < 12; u++)(!l || u >= n.getMonth()) && (!c || u <= s.getMonth()) && (y += "<option value='" + u + "'" + (u === e ? " selected='selected'" : "") + ">" + r[u] + "</option>");
                    y += "</select>"
                }
                if (v || (b += y + (!a && g && _ ? "" : "&#xa0;")), !t.yearshtml)
                    if (t.yearshtml = "", a || !_) b += "<span class='ui-datepicker-year'>" + i + "</span>";
                    else {
                        for (d = this._get(t, "yearRange").split(":"), h = (new Date).getFullYear(), p = function(t) {
                                var e = t.match(/c[+\-].*/) ? i + parseInt(t.substring(1), 10) : t.match(/[+\-].*/) ? h + parseInt(t, 10) : parseInt(t, 10);
                                return isNaN(e) ? h : e
                            }, f = p(d[0]), m = Math.max(f, p(d[1] || "")), f = n ? Math.max(f, n.getFullYear()) : f, m = s ? Math.min(m, s.getFullYear()) : m, t.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; f <= m; f++) t.yearshtml += "<option value='" + f + "'" + (f === i ? " selected='selected'" : "") + ">" + f + "</option>";
                        t.yearshtml += "</select>", b += t.yearshtml, t.yearshtml = null
                    }
                return b += this._get(t, "yearSuffix"), v && (b += (!a && g && _ ? "" : "&#xa0;") + y), b += "</div>"
            },
            _adjustInstDate: function(t, e, i) {
                var n = t.selectedYear + ("Y" === i ? e : 0),
                    s = t.selectedMonth + ("M" === i ? e : 0),
                    a = Math.min(t.selectedDay, this._getDaysInMonth(n, s)) + ("D" === i ? e : 0),
                    o = this._restrictMinMax(t, this._daylightSavingAdjust(new Date(n, s, a)));
                t.selectedDay = o.getDate(), t.drawMonth = t.selectedMonth = o.getMonth(), t.drawYear = t.selectedYear = o.getFullYear(), "M" !== i && "Y" !== i || this._notifyChange(t)
            },
            _restrictMinMax: function(t, e) {
                var i = this._getMinMaxDate(t, "min"),
                    n = this._getMinMaxDate(t, "max"),
                    s = i && e < i ? i : e;
                return n && s > n ? n : s
            },
            _notifyChange: function(t) {
                var e = this._get(t, "onChangeMonthYear");
                e && e.apply(t.input ? t.input[0] : null, [t.selectedYear, t.selectedMonth + 1, t])
            },
            _getNumberOfMonths: function(t) {
                var e = this._get(t, "numberOfMonths");
                return null == e ? [1, 1] : "number" == typeof e ? [1, e] : e
            },
            _getMinMaxDate: function(t, e) {
                return this._determineDate(t, this._get(t, e + "Date"), null)
            },
            _getDaysInMonth: function(t, e) {
                return 32 - this._daylightSavingAdjust(new Date(t, e, 32)).getDate()
            },
            _getFirstDayOfMonth: function(t, e) {
                return new Date(t, e, 1).getDay()
            },
            _canAdjustMonth: function(t, e, i, n) {
                var s = this._getNumberOfMonths(t),
                    a = this._daylightSavingAdjust(new Date(i, n + (e < 0 ? e : s[0] * s[1]), 1));
                return e < 0 && a.setDate(this._getDaysInMonth(a.getFullYear(), a.getMonth())), this._isInRange(t, a)
            },
            _isInRange: function(t, e) {
                var i, n, s = this._getMinMaxDate(t, "min"),
                    a = this._getMinMaxDate(t, "max"),
                    o = null,
                    r = null,
                    l = this._get(t, "yearRange");
                return l && (i = l.split(":"), n = (new Date).getFullYear(), o = parseInt(i[0], 10), r = parseInt(i[1], 10), i[0].match(/[+\-].*/) && (o += n), i[1].match(/[+\-].*/) && (r += n)), (!s || e.getTime() >= s.getTime()) && (!a || e.getTime() <= a.getTime()) && (!o || e.getFullYear() >= o) && (!r || e.getFullYear() <= r)
            },
            _getFormatConfig: function(t) {
                var e = this._get(t, "shortYearCutoff");
                return e = "string" != typeof e ? e : (new Date).getFullYear() % 100 + parseInt(e, 10), {
                    shortYearCutoff: e,
                    dayNamesShort: this._get(t, "dayNamesShort"),
                    dayNames: this._get(t, "dayNames"),
                    monthNamesShort: this._get(t, "monthNamesShort"),
                    monthNames: this._get(t, "monthNames")
                }
            },
            _formatDate: function(t, e, i, n) {
                e || (t.currentDay = t.selectedDay, t.currentMonth = t.selectedMonth, t.currentYear = t.selectedYear);
                var s = e ? "object" == typeof e ? e : this._daylightSavingAdjust(new Date(n, i, e)) : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
                return this.formatDate(this._get(t, "dateFormat"), s, this._getFormatConfig(t))
            }
        }), t.fn.datepicker = function(e) {
            if (!this.length) return this;
            t.datepicker.initialized || (t(document).on("mousedown", t.datepicker._checkExternalClick), t.datepicker.initialized = !0), 0 === t("#" + t.datepicker._mainDivId).length && t("body").append(t.datepicker.dpDiv);
            var i = Array.prototype.slice.call(arguments, 1);
            return "string" != typeof e || "isDisabled" !== e && "getDate" !== e && "widget" !== e ? "option" === e && 2 === arguments.length && "string" == typeof arguments[1] ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i)) : this.each(function() {
                "string" == typeof e ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this].concat(i)) : t.datepicker._attachDatepicker(this, e)
            }) : t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i))
        }, t.datepicker = new i, t.datepicker.initialized = !1, t.datepicker.uuid = (new Date).getTime(), t.datepicker.version = "1.12.1", t.datepicker
    }), $(document).ready(function() {
         $(document).on("click", "#add_category_msg", function() {
            "" != $(".category_message").val() ? $.ajax({
                type: "post",
                url: "/categories/add_category_message",
                data: {
                    category_id: $(".category_id").val(),
                    category_message: $(".category_message").val()
                },
                success: function(t) {
                    toastr.success(t[0]), $("#category_message").modal("hide"), $(".selected-ingrediends a").attr("class", "msg")
                }
            }) : $("#category_message").modal("hide")
        });
        var t = !1,
            e = $(".categry-sec :input");
        $(document).on("change", e, function() {
            t = !0
        }), $(document).on("focusout", e, function(e) {
            t && (t = !1, "" != $(".category_name").val() ? $(".category-button").trigger("click") : $(".category-button").trigger("click"), e.preventDefault()), e.preventDefault()
        })
    }), $("input,textarea").focus(function() {
        $(this).removeAttr("placeholder")
    }), $(function() {
        1 == $(".custom_radio").is(":checked") && $(".custom_value").val($("#special_discount_value").val())
    }), $(function() {
        1 == $(".modify_custom_radio").is(":checked") && $(".custom_modify_value").val($("#modify_discount_value").val())
    }), $(document).ready(function() {
          $(document).on("click", ".special_category", function() {
            if (null != $(".special_menu").val()) {
                var t = [];
                "special_category" == $(this).attr("class") ? $(this).attr("class", "special_category specials-selected") : $(this).attr("class", "special_category"), $(".special_cat_ids li.specials-selected").each(function() {
                    t.push(this.id)
                }), $(".selected_category_id").val(t)
            } else toastr.warning("Please select menu first")
        }), $(document).on("click", ".modify_category", function() {
            if (null != $(".modify_menu").val()) {
                var t = [];
                "modify_category" == $(this).attr("class") ? $(this).attr("class", "modify_category specials-selected") : $(this).attr("class", "modify_category"), $(".modify_cat_ids li.specials-selected").each(function() {
                    t.push(this.id)
                }), $(".modify_category_id").val(t)
            } else toastr.warning("Please select menu first")
        }), $(document).on("change", ".special_radio", function() {
            1 == $(".custom_radio").is(":checked") ? ($("#special_discount_value").prop("disabled", !1), $(".persent").prop("disabled", "disabled"), $(".dollar").prop("disabled", "disabled"), $(".dollar").val(""),
                $(".persent").val("")) : $("#special_discount_value").prop("disabled", "disabled"), 1 == $(".persent_radio").is(":checked") && ($(".dollar").val(""), $(".persent").attr("disabled", !1), $(".dollar").prop("disabled", "disabled")), 1 == $(".dollar_radio").is(":checked") && ($(".persent").val(""), $(".dollar").attr("disabled", !1), $(".persent").prop("disabled", "disabled"))
        }), $(document).on("change", ".modify_radio", function() {
            1 == $(".modify_custom_radio").is(":checked") ? ($("#modify_discount_value").prop("disabled", !1), $(".modify_persent").prop("disabled", "disabled"), $(".modify_dollar").prop("disabled", "disabled")) : $("#modify_discount_value").prop("disabled", "disabled"), 1 == $(".modify_persent_radio").is(":checked") && ($(".modify_persent").attr("disabled", !1), $(".modify_dollar").prop("disabled", "disabled")), 1 == $(".modify_dollar_radio").is(":checked") && ($(".modify_dollar").attr("disabled", !1), $(".modify_persent").prop("disabled", "disabled"))
        }), $(document).on("change", ".special_menu", function() {
            $(".special_menu_selected").val($(".special_menu").val())
        }), $(document).on("change", ".modify_menu", function() {
            $(".modify_menu_selected").val($(".modify_menu").val())
        }), $(document).on("click", ".hide_special", function() {
            "true" == $(this).find("input").attr("data-id") ? $(this).find("input").attr("value", "true") : $(this).find("input").attr("value", "false"), $(this).find("input").attr("data-id", "false")
        }), $(document).on("click", ".search-choice-close", function() {
            $(this).parent().hide()
        }), $(document).on("change", "#special_discount_value", function() {
            1 == $(".custom_radio").is(":checked") && $(".custom_value").val($("#special_discount_value").val())
        }), $(document).on("change", "#modify_discount_value", function() {
            1 == $(".custom_modify_radio").is(":checked") && $(".custom_modify_value").val($("#modify_discount_value").val())
        }), $(document).on("click", ".special_image", function() {
            toastr.info("Please create special item first.")
        })
    }), $(document).on("click", ".about_day_hours", function() {
        $(".close_about_day_time").unbind().on("click", function() {
            $("#aboutdaytime").modal("hide")
        }), "" != $(".user_id").val() && $.ajax({
            type: "get",
            url: "/get_about_day_hours",
            data: {
                user_id: $(".user_id").val()
            },
            success: function(t) {
                "" != t && $.each(t, function(t, e) {
                    "mon" == e.day_name ? "" != e.start_time || "" != e.end_time ? ($(".mon_start_value").val(e.start_time), $(".mon_last_value").val(e.end_time)) : ($(".cross_mon").attr("class", "about_closeicon disabled cross_mon"), $(".cross_mon i").attr("data-id", "false"), $(".menu_mon").hide(), $(".menu_mon").val(""), $(".arrow_mon").hide(), $(".mon").html("Closed")) : "tue" == e.day_name ? "" != e.start_time || "" != e.end_time ? ($(".tue_start_value").val(e.start_time), $(".tue_last_value").val(e.end_time)) : ($(".cross_tue").attr("class", "about_closeicon disabled cross_tue"), $(".cross_tue i").attr("data-id", "false"), $(".menu_tue").hide(), $(".menu_tue").val(""), $(".arrow_tue").hide(), $(".tue").html("Closed")) : "wed" == e.day_name ? "" != e.start_time || "" != e.end_time ? ($(".wed_start_value").val(e.start_time), $(".wed_last_value").val(e.end_time)) : ($(".cross_wed").attr("class", "about_closeicon disabled cross_wed"), $(".cross_wed i").attr("data-id", "false"), $(".menu_wed").hide(), $(".menu_wed").val(""), $(".arrow_wed").hide(), $(".wed").html("Closed")) : "thu" == e.day_name ? "" != e.start_time || "" != e.end_time ? ($(".thu_start_value").val(e.start_time), $(".thu_last_value").val(e.end_time)) : ($(".cross_thu").attr("class", "about_closeicon disabled cross_thu"), $(".cross_thu i").attr("data-id", "false"), $(".menu_thu").hide(), $(".menu_thu").val(""), $(".arrow_thu").hide(), $(".thu").html("Closed")) : "fri" == e.day_name ? "" != e.start_time || "" != e.end_time ? ($(".fri_start_value").val(e.start_time), $(".fri_last_value").val(e.end_time)) : ($(".cross_fri").attr("class", "about_closeicon disabled cross_fri"), $(".cross_fri i").attr("data-id", "false"), $(".menu_fri").hide(), $(".menu_fri").val(""), $(".arrow_fri").hide(), $(".fri").html("Closed")) : "sat" == e.day_name ? "" != e.start_time || "" != e.end_time ? ($(".sat_start_value").val(e.start_time), $(".sat_last_value").val(e.end_time)) : ($(".cross_sat").attr("class", "about_closeicon disabled cross_sat"), $(".cross_sat i").attr("data-id", "false"), $(".menu_sat").hide(), $(".menu_sat").val(""), $(".arrow_sat").hide(), $(".sat").html("Closed")) : "sun" == e.day_name && ("" != e.start_time || "" != e.end_time ? ($(".sun_start_value").val(e.start_time), $(".sun_last_value").val(e.end_time)) : ($(".cross_sun").attr("class", "about_closeicon disabled cross_sun"), $(".cross_sun i").attr("data-id", "false"), $(".menu_sun").hide(), $(".menu_sun").val(""), $(".arrow_sun").hide(), $(".sun").html("Closed")))
                })
            }
        })
    }), $(document).ready(function() {
        var t = !1,
            e = $("#media-dropzone :input");
        $(document).on("change", e, function() {
            t = !0
        }), $(document).on("focusout", e, function(e) {
            t && (t = !1, e.preventDefault(), $("#media-dropzone").trigger("submit")), e.preventDefault()
        })
    }), css_browser_selector(navigator.userAgent), $(document).ready(function() {
        $(document).on("click", ".select_sub_admin_people", function() {
            $(".select_sub_admin_people").removeClass("item-selected"), $(this).addClass("item-selected"), $(".sales_people_id").val($(this).attr("id")), $.ajax({
                type: "get",
                url: "/sub_admins/get_sub_admin",
                data: {
                    id: $(this).attr("id")
                },
                success: function(t) {
                    "" != t.user.name && ($(".first_name").val(t.user.name.split(" ")[0]), $(".last_name").val(t.user.name.split(" ")[1])), "" != t.user.email && $(".client_email").val(t.user.email), null != t.user ? ($(".sub_admin_id").val(t.user.id), 1 == t.user.is_active ? ($("input[name=active_user]").attr("checked", !1), $("input[name=active_user]").attr("checked", !0).click()) : ($(".make_active").attr("data-id", "true"), $(".make_active").attr("value", "false"), $("input[name=active_user]").attr("checked", !1))) : ($(".first_name").val(""), $(".last_name").val(""), $(".client_email").val("").attr("readonly", "false")), null != t.address ? ($(".client_address").val(t.address.address), $(".client_phone").val(t.address.phone_number), $(".client_city").val(t.address.city), $(".client_state").val(t.address.state), $(".client_zip").val(t.address.zip)) : ($(".client_address").val(""), $(".client_phone").val(""), $(".client_city").val(""), $(".client_state").val(""), $(".client_zip").val(""))
                }
            })
        }), $(document).on("click", ".make_sub_admin_active", function() {
            "true" == $(this).find("input").attr("data-id") ? ($(this).find("input").attr("value", "true"), $(this).find("input").attr("data-id", "false")) : ($(this).find("input").attr("value", "false"), $(this).find("input").attr("data-id", "true"))
        }), $(document).on("keyup", ".search_admin_people", function() {
            $.ajax({
                type: "get",
                url: "/sub_admins/search_admin_people",
                data: {
                    key: $(".search_admin_people").val()
                },
                success: function(t) {
                    $(".sub_admin-list ul").empty(), 0 != t.length && $.each(t, function(t, e) {
                        $(".sub_admin-list ul").append($("<li>").text(e.name).addClass("select_sub_admin_people").attr("id", e.id))
                    })
                }
            })
        })
    }), $(document).ready(function() {
        $(document).on("click", ".select_sales_people", function() {
            $(".select_sales_people").removeClass("item-selected"), $(this).addClass("item-selected"), $(".sales_people_id").val($(this).attr("id")), $.ajax({
                type: "get",
                url: "/sales_people/get_sales_people",
                data: {
                    id: $(this).attr("id")
                },
                success: function(t) {
                    "" != t.user.name && ($(".first_name").val(t.user.name.split(" ")[0]), $(".last_name").val(t.user.name.split(" ")[1])), "" != t.user.email && $(".client_email").val(t.user.email), null != t.user ? ($(".sales_people_id").val(t.user.id), 1 == t.user.is_active ? ($("input[name=active_user]").attr("checked", !1), $("input[name=active_user]").attr("checked", !0).click()) : ($(".make_active").attr("data-id", "true"), $(".make_active").attr("value", "false"), $("input[name=active_user]").attr("checked", !1))) : ($(".first_name").val(""), $(".last_name").val(""), $(".client_email").val("").attr("readonly", "false")), null != t.address ? ($(".client_address").val(t.address.address), $(".client_phone").val(t.address.phone_number), $(".client_city").val(t.address.city), $(".client_state").val(t.address.state), $(".client_zip").val(t.address.zip)) : ($(".client_address").val(""), $(".client_phone").val(""), $(".client_city").val(""), $(".client_state").val(""), $(".client_zip").val(""))
                }
            })
        }), $(document).on("click", ".make_sales_user_active", function() {
            "true" == $(this).find("input").attr("data-id") ? ($(this).find("input").attr("value", "true"), $(this).find("input").attr("data-id", "false")) : ($(this).find("input").attr("value", "false"), $(this).find("input").attr("data-id", "true"))
        }), $(document).on("keyup", ".search_sales_people", function() {
            $.ajax({
                type: "get",
                url: "/sales_people/search_sales_people",
                data: {
                    key: $(".search_sales_people").val()
                },
                success: function(t) {
                    $(".sales_people-list ul").empty(), 0 != t.length && $.each(t, function(t, e) {
                        $(".sales_people-list ul").append($("<li>").text(e.name).addClass("select_sales_people").attr("id", e.id))
                    })
                }
            })
        })
    }), /*$(function() {
        $(".datetimepicker").timepicki()
    }),*/ $(document).ready(function() {
        $(document).on("click", ".edit_client_menu_image", function() {
            "true" == $(this).attr("data-id") ? ("mon" == $(this).attr("data-day").value ? ($(".cross_mon").attr("class", "edit_client_closeicon disabled cross_mon"), $(".menu_mon").hide(), $(".menu_mon").val(""), $(".arrow_mon").hide(), $(".mon").html("Closed")) : "tue" == $(this).attr("data-day") ? ($(".cross_tue").attr("class", "edit_client_closeicon disabled cross_tue"), $(".menu_tue").hide(), $(".menu_tue").val(""), $(".arrow_tue").hide(), $(".tue").html("Closed")) : "wed" == $(this).attr("data-day") ? ($(".cross_wed").attr("class", "edit_client_closeicon disabled cross_wed"), $(".menu_wed").hide(), $(".menu_wed").val(""), $(".arrow_wed").hide(), $(".wed").html("Closed")) : "thu" == $(this).attr("data-day") ? ($(".cross_thu").attr("class", "edit_client_closeicon disabled cross_thu"), $(".menu_thu").hide(), $(".menu_thu").val(""), $(".arrow_thu").hide(), $(".thu").html("Closed")) : "fri" == $(this).attr("data-day") ? ($(".cross_fri").attr("class", "edit_client_closeicon disabled cross_fri"), $(".menu_fri").hide(), $(".menu_fri").val(""), $(".arrow_fri").hide(), $(".fri").html("Closed")) : "sat" == $(this).attr("data-day") ? ($(".cross_sat").attr("class", "edit_client_closeicon disabled cross_sat"), $(".menu_sat").hide(), $(".menu_sat").val(""), $(".arrow_sat").hide(), $(".sat").html("Closed")) : "sun" == $(this).attr("data-day") && ($(".cross_sun").attr("class", "edit_client_closeicon disabled cross_sun"), $(".menu_sun").hide(), $(".menu_sun").val(""), $(".arrow_sun").hide(), $(".sun").html("Closed")), $(this).attr("data-id", "false")) : ("mon" == $(this).attr("data-day") ? ($(".cross_mon").attr("class", "edit_client_closeicon cross_mon"), $(".menu_mon").show(), $(".arrow_mon").show(), $(".mon").html("")) : "tue" == $(this).attr("data-day") ? ($(".cross_tue").attr("class", "edit_client_closeicon cross_tue"), $(".menu_tue").show(), $(".arrow_tue").show(), $(".tue").html("")) : "wed" == $(this).attr("data-day") ? ($(".cross_wed").attr("class", "edit_client_closeicon cross_wed"), $(".menu_wed").show(), $(".arrow_wed").show(), $(".wed").html("")) : "thu" == $(this).attr("data-day") ? ($(".cross_thu").attr("class", "edit_client_closeicon cross_thu"), $(".menu_thu").show(), $(".arrow_thu").show(), $(".thu").html("")) : "fri" == $(this).attr("data-day") ? ($(".cross_fri").attr("class", "edit_client_closeicon cross_fri"), $(".menu_fri").show(), $(".arrow_fri").show(), $(".fri").html("")) : "sat" == $(this).attr("data-day") ? ($(".cross_sat").attr("class", "edit_client_closeicon cross_sat"), $(".menu_sat").show(), $(".arrow_sat").show(), $(".sat").html("")) : "sun" == $(this).attr("data-day") && ($(".cross_sun").attr("class", "edit_client_closeicon cross_sun"), $(".menu_sun").show(), $(".arrow_sun").show(), $(".sun").html("")), $(this).attr("data-id", "true"))
        })
    }), $(document).ready(function() {
        $(document).on("click", ".about_menu_image", function() {
            "true" == $(this).attr("data-id") ? ("mon" == $(this).attr("day_type") ? ($(".cross_mon").attr("class", "about_closeicon disabled cross_mon"), $(".menu_mon").hide(), $(".menu_mon").val(""), $(".arrow_mon").hide(), $(".mon").html("Closed")) : "tue" == $(this).attr("day_type") ? ($(".cross_tue").attr("class", "about_closeicon disabled cross_tue"), $(".menu_tue").hide(), $(".menu_tue").val(""), $(".arrow_tue").hide(), $(".tue").html("Closed")) : "wed" == $(this).attr("day_type") ? ($(".cross_wed").attr("class", "about_closeicon disabled cross_wed"), $(".menu_wed").hide(), $(".menu_wed").val(""), $(".arrow_wed").hide(), $(".wed").html("Closed")) : "thu" == $(this).attr("day_type") ? ($(".cross_thu").attr("class", "about_closeicon disabled cross_thu"), $(".menu_thu").hide(), $(".menu_thu").val(""), $(".arrow_thu").hide(), $(".thu").html("Closed")) : "fri" == $(this).attr("day_type") ? ($(".cross_fri").attr("class", "about_closeicon disabled cross_fri"), $(".menu_fri").hide(), $(".menu_fri").val(""), $(".arrow_fri").hide(), $(".fri").html("Closed")) : "sat" == $(this).attr("day_type") ? ($(".cross_sat").attr("class", "about_closeicon disabled cross_sat"), $(".menu_sat").hide(), $(".menu_sat").val(""), $(".arrow_sat").hide(), $(".sat").html("Closed")) : "sun" == $(this).attr("day_type") && ($(".cross_sun").attr("class", "about_closeicon disabled cross_sun"), $(".menu_sun").hide(), $(".menu_sun").val(""), $(".arrow_sun").hide(), $(".sun").html("Closed")), $(this).attr("data-id", "false")) : ("mon" == $(this).attr("day_type") ? ($(".cross_mon").attr("class", "about_closeicon cross_mon"), $(".menu_mon").show(), $(".arrow_mon").show(), $(".mon").html("")) : "tue" == $(this).attr("day_type") ? ($(".cross_tue").attr("class", "about_closeicon cross_tue"), $(".menu_tue").show(), $(".arrow_tue").show(), $(".tue").html("")) : "wed" == $(this).attr("day_type") ? ($(".cross_wed").attr("class", "about_closeicon cross_wed"), $(".menu_wed").show(), $(".arrow_wed").show(), $(".wed").html("")) : "thu" == $(this).attr("day_type") ? ($(".cross_thu").attr("class", "about_closeicon cross_thu"), $(".menu_thu").show(), $(".arrow_thu").show(), $(".thu").html("")) : "fri" == $(this).attr("day_type") ? ($(".cross_fri").attr("class", "about_closeicon cross_fri"), $(".menu_fri").show(), $(".arrow_fri").show(), $(".fri").html("")) : "sat" == $(this).attr("day_type") ? ($(".cross_sat").attr("class", "about_closeicon cross_sat"), $(".menu_sat").show(), $(".arrow_sat").show(), $(".sat").html("")) : "sun" == $(this).attr("day_type") && ($(".cross_sun").attr("class", "about_closeicon cross_sun"), $(".menu_sun").show(), $(".arrow_sun").show(), $(".sun").html("")), $(this).attr("data-id", "true"))
        })
    }), $("input,textarea").focus(function() {
        $(this).removeAttr("placeholder")
    }), $(document).ready(function() {
        var t = !1,
            e = $("#item_form :input").not(".search_item");
        $(document).on("change", e, function() {
            t = !0
        }), $(document).on("focusout", e, function(e) {
            t && (t = !1, e.preventDefault(), $("#item_form").trigger("submit")), e.preventDefault()
        }), $(document).on("click", ".c_option1, .c_option2, .c_option3, .c_option4", function(t) {
            t.preventDefault();
            var e = $(".item-selected").attr("id");
            $(".save_item_button:visible").trigger("click"), t.preventDefault(), setTimeout(function() {
                $("li#" + e).click()
            }, 1200)
        }), $(document).on("click", ".item_day_time", function() {
            $("#itemdaytime").modal("hide")
        }), $(document).on("click", ".item_ingredient", function() {
            $("#itemingredient").modal("hide")
        }), $(document).on("click", ".clear-item-btn", function() {
            $("#add_item").trigger("click")
        }), $(document).on("blur", ".price1, .price2, .price3, .price4, .price5, .price6", function() {
            value = $(this).val(), "" != value && (t = parseFloat(value).toFixed(2), "NaN" == t ? $(this).val("") : $(this).val(t))
        }), $(document).on("click", ".c_option1", function() {
            $("#combosoption1").modal("hide")
        }), $(document).on("click", ".c_option2", function() {
            $("#combosoption2").modal("hide")
        }), $(document).on("click", ".c_option3", function() {
            $("#combosoption3").modal("hide")
        }), $(document).on("click", ".c_option4", function() {
            $("#combosoption4").modal("hide")
        }), $(document).on("click", ".c_option5", function() {
            $("#combosoption5").modal("hide")
        }), $(document).on("click", ".c_option6", function() {
            $("#combosoption6").modal("hide")
        }), $(document).on("click", ".close-item_img", function() {
            $(this).parent().hide(), $.ajax({
                type: "delete",
                url: "/delete_profile_image",
                data: {
                    i_id: this.id
                },
                success: function() {
                    toastr.success("Selected image deleted successfully.")
                }
            })
        })
    }), "undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery"); + function(t) {
    "use strict";
    var e = t.fn.jquery.split(" ")[0].split(".");
    if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1 || e[0] > 3) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4")
}(jQuery),
function(t) {
    "use strict";

    function e() {
        var t = document.createElement("bootstrap"),
            e = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                transition: "transitionend"
            };
        for (var i in e)
            if (void 0 !== t.style[i]) return {
                end: e[i]
            };
        return !1
    }
    t.fn.emulateTransitionEnd = function(e) {
        var i = !1,
            n = this;
        t(this).one("bsTransitionEnd", function() {
            i = !0
        });
        var s = function() {
            i || t(n).trigger(t.support.transition.end)
        };
        return setTimeout(s, e), this
    }, t(function() {
        t.support.transition = e(), t.support.transition && (t.event.special.bsTransitionEnd = {
            bindType: t.support.transition.end,
            delegateType: t.support.transition.end,
            handle: function(e) {
                if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
            }
        })
    })
}(jQuery),
function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var i = t(this),
                s = i.data("bs.alert");
            s || i.data("bs.alert", s = new n(this)), "string" == typeof e && s[e].call(i)
        })
    }
    var i = '[data-dismiss="alert"]',
        n = function(e) {
            t(e).on("click", i, this.close)
        };
    n.VERSION = "3.3.7", n.TRANSITION_DURATION = 150, n.prototype.close = function(e) {
        function i() {
            o.detach().trigger("closed.bs.alert").remove()
        }
        var s = t(this),
            a = s.attr("data-target");
        a || (a = s.attr("href"), a = a && a.replace(/.*(?=#[^\s]*$)/, ""));
        var o = t("#" === a ? [] : a);
        e && e.preventDefault(), o.length || (o = s.closest(".alert")), o.trigger(e = t.Event("close.bs.alert")), e.isDefaultPrevented() || (o.removeClass("in"), t.support.transition && o.hasClass("fade") ? o.one("bsTransitionEnd", i).emulateTransitionEnd(n.TRANSITION_DURATION) : i())
    };
    var s = t.fn.alert;
    t.fn.alert = e, t.fn.alert.Constructor = n, t.fn.alert.noConflict = function() {
        return t.fn.alert = s, this
    }, t(document).on("click.bs.alert.data-api", i, n.prototype.close)
}(jQuery),
function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var n = t(this),
                s = n.data("bs.button"),
                a = "object" == typeof e && e;
            s || n.data("bs.button", s = new i(this, a)), "toggle" == e ? s.toggle() : e && s.setState(e)
        })
    }
    var i = function(e, n) {
        this.$element = t(e), this.options = t.extend({}, i.DEFAULTS, n), this.isLoading = !1
    };
    i.VERSION = "3.3.7", i.DEFAULTS = {
        loadingText: "loading..."
    }, i.prototype.setState = function(e) {
        var i = "disabled",
            n = this.$element,
            s = n.is("input") ? "val" : "html",
            a = n.data();
        e += "Text", null == a.resetText && n.data("resetText", n[s]()), setTimeout(t.proxy(function() {
            n[s](null == a[e] ? this.options[e] : a[e]), "loadingText" == e ? (this.isLoading = !0, n.addClass(i).attr(i, i).prop(i, !0)) : this.isLoading && (this.isLoading = !1, n.removeClass(i).removeAttr(i).prop(i, !1))
        }, this), 0)
    }, i.prototype.toggle = function() {
        var t = !0,
            e = this.$element.closest('[data-toggle="buttons"]');
        if (e.length) {
            var i = this.$element.find("input");
            "radio" == i.prop("type") ? (i.prop("checked") && (t = !1), e.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == i.prop("type") && (i.prop("checked") !== this.$element.hasClass("active") && (t = !1), this.$element.toggleClass("active")), i.prop("checked", this.$element.hasClass("active")), t && i.trigger("change")
        } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active")
    };
    var n = t.fn.button;
    t.fn.button = e, t.fn.button.Constructor = i, t.fn.button.noConflict = function() {
        return t.fn.button = n, this
    }, t(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function(i) {
        var n = t(i.target).closest(".btn");
        e.call(n, "toggle"), t(i.target).is('input[type="radio"], input[type="checkbox"]') || (i.preventDefault(), n.is("input,button") ? n.trigger("focus") : n.find("input:visible,button:visible").first().trigger("focus"))
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function(e) {
        t(e.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(e.type))
    })
}(jQuery),
function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var n = t(this),
                s = n.data("bs.carousel"),
                a = t.extend({}, i.DEFAULTS, n.data(), "object" == typeof e && e),
                o = "string" == typeof e ? e : a.slide;
            s || n.data("bs.carousel", s = new i(this, a)), "number" == typeof e ? s.to(e) : o ? s[o]() : a.interval && s.pause().cycle()
        })
    }
    var i = function(e, i) {
        this.$element = t(e), this.$indicators = this.$element.find(".carousel-indicators"), this.options = i, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", t.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", t.proxy(this.pause, this)).on("mouseleave.bs.carousel", t.proxy(this.cycle, this))
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 600, i.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
    }, i.prototype.keydown = function(t) {
        if (!/input|textarea/i.test(t.target.tagName)) {
            switch (t.which) {
                case 37:
                    this.prev();
                    break;
                case 39:
                    this.next();
                    break;
                default:
                    return
            }
            t.preventDefault()
        }
    }, i.prototype.cycle = function(e) {
        return e || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(t.proxy(this.next, this), this.options.interval)), this
    }, i.prototype.getItemIndex = function(t) {
        return this.$items = t.parent().children(".item"), this.$items.index(t || this.$active)
    }, i.prototype.getItemForDirection = function(t, e) {
        var i = this.getItemIndex(e);
        if (("prev" == t && 0 === i || "next" == t && i == this.$items.length - 1) && !this.options.wrap) return e;
        var n = "prev" == t ? -1 : 1,
            s = (i + n) % this.$items.length;
        return this.$items.eq(s)
    }, i.prototype.to = function(t) {
        var e = this,
            i = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        if (!(t > this.$items.length - 1 || t < 0)) return this.sliding ? this.$element.one("slid.bs.carousel", function() {
            e.to(t)
        }) : i == t ? this.pause().cycle() : this.slide(t > i ? "next" : "prev", this.$items.eq(t))
    }, i.prototype.pause = function(e) {
        return e || (this.paused = !0), this.$element.find(".next, .prev").length && t.support.transition && (this.$element.trigger(t.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
    }, i.prototype.next = function() {
        if (!this.sliding) return this.slide("next")
    }, i.prototype.prev = function() {
        if (!this.sliding) return this.slide("prev")
    }, i.prototype.slide = function(e, n) {
        var s = this.$element.find(".item.active"),
            a = n || this.getItemForDirection(e, s),
            o = this.interval,
            r = "next" == e ? "left" : "right",
            l = this;
        if (a.hasClass("active")) return this.sliding = !1;
        var c = a[0],
            u = t.Event("slide.bs.carousel", {
                relatedTarget: c,
                direction: r
            });
        if (this.$element.trigger(u), !u.isDefaultPrevented()) {
            if (this.sliding = !0, o && this.pause(), this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var d = t(this.$indicators.children()[this.getItemIndex(a)]);
                d && d.addClass("active")
            }
            var h = t.Event("slid.bs.carousel", {
                relatedTarget: c,
                direction: r
            });
            return t.support.transition && this.$element.hasClass("slide") ? (a.addClass(e), a[0].offsetWidth, s.addClass(r), a.addClass(r), s.one("bsTransitionEnd", function() {
                a.removeClass([e, r].join(" ")).addClass("active"), s.removeClass(["active", r].join(" ")), l.sliding = !1, setTimeout(function() {
                    l.$element.trigger(h)
                }, 0)
            }).emulateTransitionEnd(i.TRANSITION_DURATION)) : (s.removeClass("active"), a.addClass("active"), this.sliding = !1, this.$element.trigger(h)), o && this.cycle(), this
        }
    };
    var n = t.fn.carousel;
    t.fn.carousel = e, t.fn.carousel.Constructor = i, t.fn.carousel.noConflict = function() {
        return t.fn.carousel = n, this
    };
    var s = function(i) {
        var n, s = t(this),
            a = t(s.attr("data-target") || (n = s.attr("href")) && n.replace(/.*(?=#[^\s]+$)/, ""));
        if (a.hasClass("carousel")) {
            var o = t.extend({}, a.data(), s.data()),
                r = s.attr("data-slide-to");
            r && (o.interval = !1), e.call(a, o), r && a.data("bs.carousel").to(r), i.preventDefault()
        }
    };
    t(document).on("click.bs.carousel.data-api", "[data-slide]", s).on("click.bs.carousel.data-api", "[data-slide-to]", s), t(window).on("load", function() {
        t('[data-ride="carousel"]').each(function() {
            var i = t(this);
            e.call(i, i.data())
        })
    })
}(jQuery),
function(t) {
    "use strict";

    function e(e) {
        var i, n = e.attr("data-target") || (i = e.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, "");
        return t(n)
    }

    function i(e) {
        return this.each(function() {
            var i = t(this),
                s = i.data("bs.collapse"),
                a = t.extend({}, n.DEFAULTS, i.data(), "object" == typeof e && e);
            !s && a.toggle && /show|hide/.test(e) && (a.toggle = !1), s || i.data("bs.collapse", s = new n(this, a)), "string" == typeof e && s[e]()
        })
    }
    var n = function(e, i) {
        this.$element = t(e), this.options = t.extend({}, n.DEFAULTS, i), this.$trigger = t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
    };
    n.VERSION = "3.3.7", n.TRANSITION_DURATION = 350, n.DEFAULTS = {
        toggle: !0
    }, n.prototype.dimension = function() {
        return this.$element.hasClass("width") ? "width" : "height"
    }, n.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var e, s = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(s && s.length && (e = s.data("bs.collapse")) && e.transitioning)) {
                var a = t.Event("show.bs.collapse");
                if (this.$element.trigger(a), !a.isDefaultPrevented()) {
                    s && s.length && (i.call(s, "hide"), e || s.data("bs.collapse", null));
                    var o = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[o](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                    var r = function() {
                        this.$element.removeClass("collapsing").addClass("collapse in")[o](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                    };
                    if (!t.support.transition) return r.call(this);
                    var l = t.camelCase(["scroll", o].join("-"));
                    this.$element.one("bsTransitionEnd", t.proxy(r, this)).emulateTransitionEnd(n.TRANSITION_DURATION)[o](this.$element[0][l])
                }
            }
        }
    }, n.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var e = t.Event("hide.bs.collapse");
            if (this.$element.trigger(e), !e.isDefaultPrevented()) {
                var i = this.dimension();
                this.$element[i](this.$element[i]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                var s = function() {
                    this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                };
                return t.support.transition ? void this.$element[i](0).one("bsTransitionEnd", t.proxy(s, this)).emulateTransitionEnd(n.TRANSITION_DURATION) : s.call(this)
            }
        }
    }, n.prototype.toggle = function() {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    }, n.prototype.getParent = function() {
        return t(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(t.proxy(function(i, n) {
            var s = t(n);
            this.addAriaAndCollapsedClass(e(s), s)
        }, this)).end()
    }, n.prototype.addAriaAndCollapsedClass = function(t, e) {
        var i = t.hasClass("in");
        t.attr("aria-expanded", i), e.toggleClass("collapsed", !i).attr("aria-expanded", i)
    };
    var s = t.fn.collapse;
    t.fn.collapse = i, t.fn.collapse.Constructor = n, t.fn.collapse.noConflict = function() {
        return t.fn.collapse = s, this
    }, t(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function(n) {
        var s = t(this);
        s.attr("data-target") || n.preventDefault();
        var a = e(s),
            o = a.data("bs.collapse"),
            r = o ? "toggle" : s.data();
        i.call(a, r)
    })
}(jQuery),
function(t) {
    "use strict";

    function e(e) {
        var i = e.attr("data-target");
        i || (i = e.attr("href"), i = i && /#[A-Za-z]/.test(i) && i.replace(/.*(?=#[^\s]*$)/, ""));
        var n = i && t(i);
        return n && n.length ? n : e.parent()
    }

    function i(i) {
        i && 3 === i.which || (t(s).remove(), t(a).each(function() {
            var n = t(this),
                s = e(n),
                a = {
                    relatedTarget: this
                };
            s.hasClass("open") && (i && "click" == i.type && /input|textarea/i.test(i.target.tagName) && t.contains(s[0], i.target) || (s.trigger(i = t.Event("hide.bs.dropdown", a)), i.isDefaultPrevented() || (n.attr("aria-expanded", "false"), s.removeClass("open").trigger(t.Event("hidden.bs.dropdown", a)))))
        }))
    }

    function n(e) {
        return this.each(function() {
            var i = t(this),
                n = i.data("bs.dropdown");
            n || i.data("bs.dropdown", n = new o(this)), "string" == typeof e && n[e].call(i)
        })
    }
    var s = ".dropdown-backdrop",
        a = '[data-toggle="dropdown"]',
        o = function(e) {
            t(e).on("click.bs.dropdown", this.toggle)
        };
    o.VERSION = "3.3.7", o.prototype.toggle = function(n) {
        var s = t(this);
        if (!s.is(".disabled, :disabled")) {
            var a = e(s),
                o = a.hasClass("open");
            if (i(), !o) {
                "ontouchstart" in document.documentElement && !a.closest(".navbar-nav").length && t(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(t(this)).on("click", i);
                var r = {
                    relatedTarget: this
                };
                if (a.trigger(n = t.Event("show.bs.dropdown", r)), n.isDefaultPrevented()) return;
                s.trigger("focus").attr("aria-expanded", "true"), a.toggleClass("open").trigger(t.Event("shown.bs.dropdown", r))
            }
            return !1
        }
    }, o.prototype.keydown = function(i) {
        if (/(38|40|27|32)/.test(i.which) && !/input|textarea/i.test(i.target.tagName)) {
            var n = t(this);
            if (i.preventDefault(), i.stopPropagation(), !n.is(".disabled, :disabled")) {
                var s = e(n),
                    o = s.hasClass("open");
                if (!o && 27 != i.which || o && 27 == i.which) return 27 == i.which && s.find(a).trigger("focus"), n.trigger("click");
                var r = " li:not(.disabled):visible a",
                    l = s.find(".dropdown-menu" + r);
                if (l.length) {
                    var c = l.index(i.target);
                    38 == i.which && c > 0 && c--, 40 == i.which && c < l.length - 1 && c++, ~c || (c = 0), l.eq(c).trigger("focus")
                }
            }
        }
    };
    var r = t.fn.dropdown;
    t.fn.dropdown = n, t.fn.dropdown.Constructor = o, t.fn.dropdown.noConflict = function() {
        return t.fn.dropdown = r, this
    }, t(document).on("click.bs.dropdown.data-api", i).on("click.bs.dropdown.data-api", ".dropdown form", function(t) {
        t.stopPropagation()
    }).on("click.bs.dropdown.data-api", a, o.prototype.toggle).on("keydown.bs.dropdown.data-api", a, o.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", o.prototype.keydown)
}(jQuery),
function(t) {
    "use strict";

    function e(e, n) {
        return this.each(function() {
            var s = t(this),
                a = s.data("bs.modal"),
                o = t.extend({}, i.DEFAULTS, s.data(), "object" == typeof e && e);
            a || s.data("bs.modal", a = new i(this, o)), "string" == typeof e ? a[e](n) : o.show && a.show(n)
        })
    }
    var i = function(e, i) {
        this.options = i, this.$body = t(document.body), this.$element = t(e), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, t.proxy(function() {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 300, i.BACKDROP_TRANSITION_DURATION = 150, i.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, i.prototype.toggle = function(t) {
        return this.isShown ? this.hide() : this.show(t)
    }, i.prototype.show = function(e) {
        var n = this,
            s = t.Event("show.bs.modal", {
                relatedTarget: e
            });
        this.$element.trigger(s), this.isShown || s.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', t.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function() {
            n.$element.one("mouseup.dismiss.bs.modal", function(e) {
                t(e.target).is(n.$element) && (n.ignoreBackdropClick = !0)
            })
        }), this.backdrop(function() {
            var s = t.support.transition && n.$element.hasClass("fade");
            n.$element.parent().length || n.$element.appendTo(n.$body), n.$element.show().scrollTop(0), n.adjustDialog(), s && n.$element[0].offsetWidth, n.$element.addClass("in"), n.enforceFocus();
            var a = t.Event("shown.bs.modal", {
                relatedTarget: e
            });
            s ? n.$dialog.one("bsTransitionEnd", function() {
                n.$element.trigger("focus").trigger(a)
            }).emulateTransitionEnd(i.TRANSITION_DURATION) : n.$element.trigger("focus").trigger(a)
        }))
    }, i.prototype.hide = function(e) {
        e && e.preventDefault(), e = t.Event("hide.bs.modal"), this.$element.trigger(e), this.isShown && !e.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), t(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), t.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", t.proxy(this.hideModal, this)).emulateTransitionEnd(i.TRANSITION_DURATION) : this.hideModal())
    }, i.prototype.enforceFocus = function() {
        t(document).off("focusin.bs.modal").on("focusin.bs.modal", t.proxy(function(t) {
            document === t.target || this.$element[0] === t.target || this.$element.has(t.target).length || this.$element.trigger("focus")
        }, this))
    }, i.prototype.escape = function() {
        this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", t.proxy(function(t) {
            27 == t.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
    }, i.prototype.resize = function() {
        this.isShown ? t(window).on("resize.bs.modal", t.proxy(this.handleUpdate, this)) : t(window).off("resize.bs.modal")
    }, i.prototype.hideModal = function() {
        var t = this;
        this.$element.hide(), this.backdrop(function() {
            t.$body.removeClass("modal-open"), t.resetAdjustments(), t.resetScrollbar(), t.$element.trigger("hidden.bs.modal")
        })
    }, i.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, i.prototype.backdrop = function(e) {
        var n = this,
            s = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var a = t.support.transition && s;
            if (this.$backdrop = t(document.createElement("div")).addClass("modal-backdrop " + s).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", t.proxy(function(t) {
                    return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(t.target === t.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()))
                }, this)), a && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !e) return;
            a ? this.$backdrop.one("bsTransitionEnd", e).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : e()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var o = function() {
                n.removeBackdrop(), e && e()
            };
            t.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", o).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : o()
        } else e && e()
    }, i.prototype.handleUpdate = function() {
        this.adjustDialog()
    }, i.prototype.adjustDialog = function() {
        var t = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && t ? this.scrollbarWidth : "",
            paddingRight: this.bodyIsOverflowing && !t ? this.scrollbarWidth : ""
        })
    }, i.prototype.resetAdjustments = function() {
        this.$element.css({
            paddingLeft: "",
            paddingRight: ""
        })
    }, i.prototype.checkScrollbar = function() {
        var t = window.innerWidth;
        if (!t) {
            var e = document.documentElement.getBoundingClientRect();
            t = e.right - Math.abs(e.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < t, this.scrollbarWidth = this.measureScrollbar()
    }, i.prototype.setScrollbar = function() {
        var t = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", t + this.scrollbarWidth)
    }, i.prototype.resetScrollbar = function() {
        this.$body.css("padding-right", this.originalBodyPad)
    }, i.prototype.measureScrollbar = function() {
        var t = document.createElement("div");
        t.className = "modal-scrollbar-measure", this.$body.append(t);
        var e = t.offsetWidth - t.clientWidth;
        return this.$body[0].removeChild(t), e
    };
    var n = t.fn.modal;
    t.fn.modal = e, t.fn.modal.Constructor = i, t.fn.modal.noConflict = function() {
        return t.fn.modal = n, this
    }, t(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(i) {
        var n = t(this),
            s = n.attr("href"),
            a = t(n.attr("data-target") || s && s.replace(/.*(?=#[^\s]+$)/, "")),
            o = a.data("bs.modal") ? "toggle" : t.extend({
                remote: !/#/.test(s) && s
            }, a.data(), n.data());
        n.is("a") && i.preventDefault(), a.one("show.bs.modal", function(t) {
            t.isDefaultPrevented() || a.one("hidden.bs.modal", function() {
                n.is(":visible") && n.trigger("focus")
            })
        }), e.call(a, o, this)
    })
}(jQuery),
function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var n = t(this),
                s = n.data("bs.tooltip"),
                a = "object" == typeof e && e;
            !s && /destroy|hide/.test(e) || (s || n.data("bs.tooltip", s = new i(this, a)), "string" == typeof e && s[e]())
        })
    }
    var i = function(t, e) {
        this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", t, e)
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 150, i.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {
            selector: "body",
            padding: 0
        }
    }, i.prototype.init = function(e, i, n) {
        if (this.enabled = !0, this.type = e, this.$element = t(i), this.options = this.getOptions(n), this.$viewport = this.options.viewport && t(t.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = {
                click: !1,
                hover: !1,
                focus: !1
            }, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var s = this.options.trigger.split(" "), a = s.length; a--;) {
            var o = s[a];
            if ("click" == o) this.$element.on("click." + this.type, this.options.selector, t.proxy(this.toggle, this));
            else if ("manual" != o) {
                var r = "hover" == o ? "mouseenter" : "focusin",
                    l = "hover" == o ? "mouseleave" : "focusout";
                this.$element.on(r + "." + this.type, this.options.selector, t.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, t.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = t.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, i.prototype.getDefaults = function() {
        return i.DEFAULTS
    }, i.prototype.getOptions = function(e) {
        return e = t.extend({}, this.getDefaults(), this.$element.data(), e), e.delay && "number" == typeof e.delay && (e.delay = {
            show: e.delay,
            hide: e.delay
        }), e
    }, i.prototype.getDelegateOptions = function() {
        var e = {},
            i = this.getDefaults();
        return this._options && t.each(this._options, function(t, n) {
            i[t] != n && (e[t] = n)
        }), e
    }, i.prototype.enter = function(e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        return i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusin" == e.type ? "focus" : "hover"] = !0), i.tip().hasClass("in") || "in" == i.hoverState ? void(i.hoverState = "in") : (clearTimeout(i.timeout), i.hoverState = "in", i.options.delay && i.options.delay.show ? void(i.timeout = setTimeout(function() {
            "in" == i.hoverState && i.show()
        }, i.options.delay.show)) : i.show())
    }, i.prototype.isInStateTrue = function() {
        for (var t in this.inState)
            if (this.inState[t]) return !0;
        return !1
    }, i.prototype.leave = function(e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        if (i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusout" == e.type ? "focus" : "hover"] = !1), !i.isInStateTrue()) return clearTimeout(i.timeout), i.hoverState = "out", i.options.delay && i.options.delay.hide ? void(i.timeout = setTimeout(function() {
            "out" == i.hoverState && i.hide()
        }, i.options.delay.hide)) : i.hide()
    }, i.prototype.show = function() {
        var e = t.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(e);
            var n = t.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (e.isDefaultPrevented() || !n) return;
            var s = this,
                a = this.tip(),
                o = this.getUID(this.type);
            this.setContent(), a.attr("id", o), this.$element.attr("aria-describedby", o), this.options.animation && a.addClass("fade");
            var r = "function" == typeof this.options.placement ? this.options.placement.call(this, a[0], this.$element[0]) : this.options.placement,
                l = /\s?auto?\s?/i,
                c = l.test(r);
            c && (r = r.replace(l, "") || "top"), a.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(r).data("bs." + this.type, this), this.options.container ? a.appendTo(this.options.container) : a.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type);
            var u = this.getPosition(),
                d = a[0].offsetWidth,
                h = a[0].offsetHeight;
            if (c) {
                var p = r,
                    f = this.getPosition(this.$viewport);
                r = "bottom" == r && u.bottom + h > f.bottom ? "top" : "top" == r && u.top - h < f.top ? "bottom" : "right" == r && u.right + d > f.width ? "left" : "left" == r && u.left - d < f.left ? "right" : r, a.removeClass(p).addClass(r)
            }
            var m = this.getCalculatedOffset(r, u, d, h);
            this.applyPlacement(m, r);
            var g = function() {
                var t = s.hoverState;
                s.$element.trigger("shown.bs." + s.type), s.hoverState = null, "out" == t && s.leave(s)
            };
            t.support.transition && this.$tip.hasClass("fade") ? a.one("bsTransitionEnd", g).emulateTransitionEnd(i.TRANSITION_DURATION) : g()
        }
    }, i.prototype.applyPlacement = function(e, i) {
        var n = this.tip(),
            s = n[0].offsetWidth,
            a = n[0].offsetHeight,
            o = parseInt(n.css("margin-top"), 10),
            r = parseInt(n.css("margin-left"), 10);
        isNaN(o) && (o = 0), isNaN(r) && (r = 0), e.top += o, e.left += r, t.offset.setOffset(n[0], t.extend({
            using: function(t) {
                n.css({
                    top: Math.round(t.top),
                    left: Math.round(t.left)
                })
            }
        }, e), 0), n.addClass("in");
        var l = n[0].offsetWidth,
            c = n[0].offsetHeight;
        "top" == i && c != a && (e.top = e.top + a - c);
        var u = this.getViewportAdjustedDelta(i, e, l, c);
        u.left ? e.left += u.left : e.top += u.top;
        var d = /top|bottom/.test(i),
            h = d ? 2 * u.left - s + l : 2 * u.top - a + c,
            p = d ? "offsetWidth" : "offsetHeight";
        n.offset(e), this.replaceArrow(h, n[0][p], d)
    }, i.prototype.replaceArrow = function(t, e, i) {
        this.arrow().css(i ? "left" : "top", 50 * (1 - t / e) + "%").css(i ? "top" : "left", "")
    }, i.prototype.setContent = function() {
        var t = this.tip(),
            e = this.getTitle();
        t.find(".tooltip-inner")[this.options.html ? "html" : "text"](e), t.removeClass("fade in top bottom left right")
    }, i.prototype.hide = function(e) {
        function n() {
            "in" != s.hoverState && a.detach(), s.$element && s.$element.removeAttr("aria-describedby").trigger("hidden.bs." + s.type), e && e()
        }
        var s = this,
            a = t(this.$tip),
            o = t.Event("hide.bs." + this.type);
        if (this.$element.trigger(o), !o.isDefaultPrevented()) return a.removeClass("in"), t.support.transition && a.hasClass("fade") ? a.one("bsTransitionEnd", n).emulateTransitionEnd(i.TRANSITION_DURATION) : n(), this.hoverState = null, this
    }, i.prototype.fixTitle = function() {
        var t = this.$element;
        (t.attr("title") || "string" != typeof t.attr("data-original-title")) && t.attr("data-original-title", t.attr("title") || "").attr("title", "")
    }, i.prototype.hasContent = function() {
        return this.getTitle()
    }, i.prototype.getPosition = function(e) {
        e = e || this.$element;
        var i = e[0],
            n = "BODY" == i.tagName,
            s = i.getBoundingClientRect();
        null == s.width && (s = t.extend({}, s, {
            width: s.right - s.left,
            height: s.bottom - s.top
        }));
        var a = window.SVGElement && i instanceof window.SVGElement,
            o = n ? {
                top: 0,
                left: 0
            } : a ? null : e.offset(),
            r = {
                scroll: n ? document.documentElement.scrollTop || document.body.scrollTop : e.scrollTop()
            },
            l = n ? {
                width: t(window).width(),
                height: t(window).height()
            } : null;
        return t.extend({}, s, r, l, o)
    }, i.prototype.getCalculatedOffset = function(t, e, i, n) {
        return "bottom" == t ? {
            top: e.top + e.height,
            left: e.left + e.width / 2 - i / 2
        } : "top" == t ? {
            top: e.top - n,
            left: e.left + e.width / 2 - i / 2
        } : "left" == t ? {
            top: e.top + e.height / 2 - n / 2,
            left: e.left - i
        } : {
            top: e.top + e.height / 2 - n / 2,
            left: e.left + e.width
        }
    }, i.prototype.getViewportAdjustedDelta = function(t, e, i, n) {
        var s = {
            top: 0,
            left: 0
        };
        if (!this.$viewport) return s;
        var a = this.options.viewport && this.options.viewport.padding || 0,
            o = this.getPosition(this.$viewport);
        if (/right|left/.test(t)) {
            var r = e.top - a - o.scroll,
                l = e.top + a - o.scroll + n;
            r < o.top ? s.top = o.top - r : l > o.top + o.height && (s.top = o.top + o.height - l)
        } else {
            var c = e.left - a,
                u = e.left + a + i;
            c < o.left ? s.left = o.left - c : u > o.right && (s.left = o.left + o.width - u)
        }
        return s
    }, i.prototype.getTitle = function() {
        var t = this.$element,
            e = this.options;
        return t.attr("data-original-title") || ("function" == typeof e.title ? e.title.call(t[0]) : e.title)
    }, i.prototype.getUID = function(t) {
        do {
            t += ~~(1e6 * Math.random())
        } while (document.getElementById(t));
        return t
    }, i.prototype.tip = function() {
        if (!this.$tip && (this.$tip = t(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
        return this.$tip
    }, i.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, i.prototype.enable = function() {
        this.enabled = !0
    }, i.prototype.disable = function() {
        this.enabled = !1
    }, i.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled
    }, i.prototype.toggle = function(e) {
        var i = this;
        e && ((i = t(e.currentTarget).data("bs." + this.type)) || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i))), e ? (i.inState.click = !i.inState.click, i.isInStateTrue() ? i.enter(i) : i.leave(i)) : i.tip().hasClass("in") ? i.leave(i) : i.enter(i)
    }, i.prototype.destroy = function() {
        var t = this;
        clearTimeout(this.timeout), this.hide(function() {
            t.$element.off("." + t.type).removeData("bs." + t.type), t.$tip && t.$tip.detach(), t.$tip = null, t.$arrow = null, t.$viewport = null, t.$element = null
        })
    };
    var n = t.fn.tooltip;
    t.fn.tooltip = e, t.fn.tooltip.Constructor = i, t.fn.tooltip.noConflict = function() {
        return t.fn.tooltip = n, this
    }
}(jQuery),
function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var n = t(this),
                s = n.data("bs.popover"),
                a = "object" == typeof e && e;
            !s && /destroy|hide/.test(e) || (s || n.data("bs.popover", s = new i(this, a)), "string" == typeof e && s[e]())
        })
    }
    var i = function(t, e) {
        this.init("popover", t, e)
    };
    if (!t.fn.tooltip) throw new Error("Popover requires tooltip.js");
    i.VERSION = "3.3.7", i.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), i.prototype = t.extend({}, t.fn.tooltip.Constructor.prototype), i.prototype.constructor = i, i.prototype.getDefaults = function() {
        return i.DEFAULTS
    }, i.prototype.setContent = function() {
        var t = this.tip(),
            e = this.getTitle(),
            i = this.getContent();
        t.find(".popover-title")[this.options.html ? "html" : "text"](e), t.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof i ? "html" : "append" : "text"](i), t.removeClass("fade top bottom left right in"), t.find(".popover-title").html() || t.find(".popover-title").hide()
    }, i.prototype.hasContent = function() {
        return this.getTitle() || this.getContent()
    }, i.prototype.getContent = function() {
        var t = this.$element,
            e = this.options;
        return t.attr("data-content") || ("function" == typeof e.content ? e.content.call(t[0]) : e.content)
    }, i.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    };
    var n = t.fn.popover;
    t.fn.popover = e, t.fn.popover.Constructor = i, t.fn.popover.noConflict = function() {
        return t.fn.popover = n, this
    }
}(jQuery),
function(t) {
    "use strict";

    function e(i, n) {
        this.$body = t(document.body), this.$scrollElement = t(t(i).is(document.body) ? window : i), this.options = t.extend({}, e.DEFAULTS, n), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", t.proxy(this.process, this)), this.refresh(), this.process()
    }

    function i(i) {
        return this.each(function() {
            var n = t(this),
                s = n.data("bs.scrollspy"),
                a = "object" == typeof i && i;
            s || n.data("bs.scrollspy", s = new e(this, a)), "string" == typeof i && s[i]()
        })
    }
    e.VERSION = "3.3.7", e.DEFAULTS = {
        offset: 10
    }, e.prototype.getScrollHeight = function() {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }, e.prototype.refresh = function() {
        var e = this,
            i = "offset",
            n = 0;
        this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), t.isWindow(this.$scrollElement[0]) || (i = "position", n = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function() {
            var e = t(this),
                s = e.data("target") || e.attr("href"),
                a = /^#./.test(s) && t(s);
            return a && a.length && a.is(":visible") && [
                [a[i]().top + n, s]
            ] || null
        }).sort(function(t, e) {
            return t[0] - e[0]
        }).each(function() {
            e.offsets.push(this[0]), e.targets.push(this[1])
        })
    }, e.prototype.process = function() {
        var t, e = this.$scrollElement.scrollTop() + this.options.offset,
            i = this.getScrollHeight(),
            n = this.options.offset + i - this.$scrollElement.height(),
            s = this.offsets,
            a = this.targets,
            o = this.activeTarget;
        if (this.scrollHeight != i && this.refresh(), e >= n) return o != (t = a[a.length - 1]) && this.activate(t);
        if (o && e < s[0]) return this.activeTarget = null, this.clear();
        for (t = s.length; t--;) o != a[t] && e >= s[t] && (void 0 === s[t + 1] || e < s[t + 1]) && this.activate(a[t])
    }, e.prototype.activate = function(e) {
        this.activeTarget = e, this.clear();
        var i = this.selector + '[data-target="' + e + '"],' + this.selector + '[href="' + e + '"]',
            n = t(i).parents("li").addClass("active");
        n.parent(".dropdown-menu").length && (n = n.closest("li.dropdown").addClass("active")), n.trigger("activate.bs.scrollspy")
    }, e.prototype.clear = function() {
        t(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
    };
    var n = t.fn.scrollspy;
    t.fn.scrollspy = i, t.fn.scrollspy.Constructor = e, t.fn.scrollspy.noConflict = function() {
        return t.fn.scrollspy = n, this
    }, t(window).on("load.bs.scrollspy.data-api", function() {
        t('[data-spy="scroll"]').each(function() {
            var e = t(this);
            i.call(e, e.data())
        })
    })
}(jQuery),
function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var n = t(this),
                s = n.data("bs.tab");
            s || n.data("bs.tab", s = new i(this)), "string" == typeof e && s[e]()
        })
    }
    var i = function(e) {
        this.element = t(e)
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 150, i.prototype.show = function() {
        var e = this.element,
            i = e.closest("ul:not(.dropdown-menu)"),
            n = e.data("target");
        if (n || (n = e.attr("href"), n = n && n.replace(/.*(?=#[^\s]*$)/, "")), !e.parent("li").hasClass("active")) {
            var s = i.find(".active:last a"),
                a = t.Event("hide.bs.tab", {
                    relatedTarget: e[0]
                }),
                o = t.Event("show.bs.tab", {
                    relatedTarget: s[0]
                });
            if (s.trigger(a), e.trigger(o), !o.isDefaultPrevented() && !a.isDefaultPrevented()) {
                var r = t(n);
                this.activate(e.closest("li"), i), this.activate(r, r.parent(), function() {
                    s.trigger({
                        type: "hidden.bs.tab",
                        relatedTarget: e[0]
                    }), e.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: s[0]
                    })
                })
            }
        }
    }, i.prototype.activate = function(e, n, s) {
        function a() {
            o.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), e.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), r ? (e[0].offsetWidth, e.addClass("in")) : e.removeClass("fade"), e.parent(".dropdown-menu").length && e.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), s && s()
        }
        var o = n.find("> .active"),
            r = s && t.support.transition && (o.length && o.hasClass("fade") || !!n.find("> .fade").length);
        o.length && r ? o.one("bsTransitionEnd", a).emulateTransitionEnd(i.TRANSITION_DURATION) : a(), o.removeClass("in")
    };
    var n = t.fn.tab;
    t.fn.tab = e, t.fn.tab.Constructor = i, t.fn.tab.noConflict = function() {
        return t.fn.tab = n, this
    };
    var s = function(i) {
        i.preventDefault(), e.call(t(this), "show")
    };
    t(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', s).on("click.bs.tab.data-api", '[data-toggle="pill"]', s)
}(jQuery),
function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var n = t(this),
                s = n.data("bs.affix"),
                a = "object" == typeof e && e;
            s || n.data("bs.affix", s = new i(this, a)), "string" == typeof e && s[e]()
        })
    }
    var i = function(e, n) {
        this.options = t.extend({}, i.DEFAULTS, n), this.$target = t(this.options.target).on("scroll.bs.affix.data-api", t.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", t.proxy(this.checkPositionWithEventLoop, this)), this.$element = t(e), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()
    };
    i.VERSION = "3.3.7", i.RESET = "affix affix-top affix-bottom", i.DEFAULTS = {
        offset: 0,
        target: window
    }, i.prototype.getState = function(t, e, i, n) {
        var s = this.$target.scrollTop(),
            a = this.$element.offset(),
            o = this.$target.height();
        if (null != i && "top" == this.affixed) return s < i && "top";
        if ("bottom" == this.affixed) return null != i ? !(s + this.unpin <= a.top) && "bottom" : !(s + o <= t - n) && "bottom";
        var r = null == this.affixed,
            l = r ? s : a.top,
            c = r ? o : e;
        return null != i && s <= i ? "top" : null != n && l + c >= t - n && "bottom"
    }, i.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(i.RESET).addClass("affix");
        var t = this.$target.scrollTop(),
            e = this.$element.offset();
        return this.pinnedOffset = e.top - t
    }, i.prototype.checkPositionWithEventLoop = function() {
        setTimeout(t.proxy(this.checkPosition, this), 1)
    }, i.prototype.checkPosition = function() {
        if (this.$element.is(":visible")) {
            var e = this.$element.height(),
                n = this.options.offset,
                s = n.top,
                a = n.bottom,
                o = Math.max(t(document).height(), t(document.body).height());
            "object" != typeof n && (a = s = n), "function" == typeof s && (s = n.top(this.$element)), "function" == typeof a && (a = n.bottom(this.$element));
            var r = this.getState(o, e, s, a);
            if (this.affixed != r) {
                null != this.unpin && this.$element.css("top", "");
                var l = "affix" + (r ? "-" + r : ""),
                    c = t.Event(l + ".bs.affix");
                if (this.$element.trigger(c), c.isDefaultPrevented()) return;
                this.affixed = r, this.unpin = "bottom" == r ? this.getPinnedOffset() : null, this.$element.removeClass(i.RESET).addClass(l).trigger(l.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == r && this.$element.offset({
                top: o - e - a
            })
        }
    };
    var n = t.fn.affix;
    t.fn.affix = e, t.fn.affix.Constructor = i, t.fn.affix.noConflict = function() {
        return t.fn.affix = n, this
    }, t(window).on("load", function() {
        t('[data-spy="affix"]').each(function() {
            var i = t(this),
                n = i.data();
            n.offset = n.offset || {}, null != n.offsetBottom && (n.offset.bottom = n.offsetBottom), null != n.offsetTop && (n.offset.top = n.offsetTop), e.call(i, n)
        })
    })
}(jQuery);
var timoutWarning = 9e5,
    timoutNow = 96e4,
    logoutUrl = "http://localhost:3000/users/sign_out",
    warningTimer, timeoutTimer;
$(document).on("click", ".continue_loging", function() {
    $(".timeout_popup").val("true"), resetTimers()
}), $(document).ready(function() {
    $(".make_active").click(function() {
        $(".make_active").removeClass("active"), $(".active_add_edit").removeClass("active"), $(this).addClass("active")
    }), $(".add_menu").click(function() {
        $(".active_add_edit").removeClass("active"), $(".make_active").removeClass("active")
    }), $(".addeditclient").click(function() {
        $(".sales_people_active").removeClass("active"), $(".sub_admin_active").removeClass("active"), $(".active_add_edit").addClass("active")
    }), $(".addeditsales").click(function() {
        $(".active_add_edit").removeClass("active"), $(".sub_admin_active").removeClass("active"), $(".sales_people_active").addClass("active")
    }), $(".edit_menu").on("click", function() {
        $(".make_active").removeClass("active")
    }), $(".fa-close").click(function() {
        $(".loginbox").hide()
    }), $(document).on("click", ".close_menu", function(t) {
        var e = $(this).parent().parent().parent();
        $("<input>").attr({
            type: "hidden",
            name: "form-close",
            value: "true"
        }).appendTo("form:visible, .main-class"), e.find(".save-btn:visible").trigger("click"), t.preventDefault(), $(".main-class").html("")
    }), $(".addeditadmin").click(function() {
        $(".sales_people_active").removeClass("active"), $(".active_add_edit").removeClass("active"), $(".sub_admin_active").addClass("active")
    });
    var t = !1,
        e = $("#delivery_form :input");
    $(document).on("change", e, function() {
        t = !0
    }), $(document).on("focusout", e, function(e) {
        t && (t = !1, e.preventDefault(), $("#delivery_form").trigger("submit")), e.preventDefault()
    })
});