$(document).ready(function() {
	// code for vertical tab
	//$('#tabs').tabs().addClass('ui-tabs-vertical ui-helper-clearfix');

	// log in show hide

	$(".login-show").click(function() {
		$(".login").removeClass("no-display");
	});

	$(".login-hide").click(function() {
		$(".login").addClass("no-display");
	});

	// show hide add / modify client

	$(".edit-cient").click(function() {
		$(".add-client").removeClass("no-display");
	});

	$(".edit-cienthide").click(function() {
		$(".add-client").addClass("no-display");
	});

	// add menu
	$(".close_window").click(function() {
		$(".add-menu").addClass("no-display");
	});

	$(".add-menu-event").click(function() {
		$(".add-menu").removeClass("no-display");
	});

	// category message
	$(".caregory-message-close").click(function() {
		$(".caregory-message").addClass("no-display");
	});

	$(".edit-menu-tab-foodlist ul li").click(function() {
		$(this).toggleClass("food-selected");
	});

	$(".add-ingredients-content-box ul li").click(function() {
		$(this).toggleClass("add-greenback");
	});

	$("#sortable").sortable();
	$("#sortable").disableSelection();
	$("#sortable2").sortable();
	$("#sortable2").disableSelection();
	$("#sortable3").sortable();
	$("#sortable3").disableSelection();

	jQuery(document).ready(function() {
		jQuery('.tabs .tab-links a').on('click',function(e) {
			var currentAttrValue = jQuery(this).attr('href');

			// Show/Hide Tabs
			jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

			// Change/remove current tab to active
			jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
			
			e.preventDefault();
		});
	});
});