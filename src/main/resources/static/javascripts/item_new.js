$('input,textarea').focus(function(){
  $(this).removeAttr('placeholder');
});
$(document).ready(function(){
  var new_value = false;
  var all_inputs = $('#item_form :input').not('.search_item')
  $(document).on('change', all_inputs, function(e) {
    new_value = true
  });
  $(document).on('focusout', all_inputs, function(e) {
    if (new_value){
      new_value = false;
      e.preventDefault(); 
      $("#item_form").trigger('submit');
    }
    e.preventDefault(); 
  });
  $(document).on('click','.c_option1, .c_option2, .c_option3, .c_option4',function(e){
    e.preventDefault();
    var current_element = $('.item-selected').attr('id')
    $('.save_item_button:visible').trigger('click');
    e.preventDefault();  
    setTimeout(function(){
      $('li#' + current_element).click(); 
    }, 1200);
    
  });
  $(document).on('click','.item_ingredient_option',function(){
    if($('.item-selected').attr('id') != null){
      $('#itemingredient').modal();
    }else{
      toastr.info("Please create Item first.");
    }
  });

  $(document).on('click','.item_avalibility',function(){
    if($('.item-selected').attr('id') != null){
      $('#itemdaytime').modal();
    }else{
      toastr.info("Please create Item first.");
    }
  });
  // hide item
  $('.hide_item').click(function(){
    if($(this).find('input').attr('data-id') == "true"){
      $(this).find('input').attr('value','true');
    }else{
      $(this).find('input').attr('value','false'); 
    }
    $(this).find('input').attr('data-id','false');
  });

  // select item
  $(document).on('click', '.select_item', function(){
    $('.select_item').removeClass("item-selected");
    $(this).addClass("item-selected");
    $('.selected_item_id').val($('.item-selected').attr('id'));
    $.ajax({
      type:'post',
      url:'/item/get_selected_item',
      data: { 
        'i_id': $('.item-selected').attr('id') 
      },
      success:function(data){
        $('.item_description').removeAttr("disabled");
        $('.add-item-contents input').removeAttr("disabled");
        $('.additemfields input').removeAttr("disabled");
        $('.cbn').val("");
        $('.cbd').val("");
        $('.thc').val("");
        $('.image_picture').attr('data-id','true');
        // show selected item ingredient
        if(data.ingredient != ""){
          $('.ingredient_ids li').each(function(){$(this).attr('class','item_ingredient_select'); });
          $('.ingredient_ids li').each(function(){ 
            if(data.ingredient.includes(this.id)){
              $(this).attr('class','item_ingredient_select selected');
            }
          });
        }else{
          $('.ingredient_ids li').each(function(){$(this).attr('class','item_ingredient_select'); });  
        } 
        if(data.rating != null){
          if(data.rating.rating_value == "0.5"){
            $('.caption').find('span').text("0.5").attr('class','label label-success');
          }else if(data.rating.rating_value == "1"){
            $('.caption').find('span').text("1").attr('class','label label-success');
          }else if (data.rating.rating_value == "1.5"){
            $('.caption').find('span').text("1.5").attr('class','label label-success');
          }else if(data.rating.rating_value == "2"){
            $('.caption').find('span').text("2").attr('class','label label-success');
          }else if(data.rating.rating_value == "2.5"){
            $('.caption').find('span').text("2.5").attr('class','label label-success');
          }else if(data.rating.rating_value == "3"){
            $('.caption').find('span').text("3").attr('class','label label-success');
          }else if(data.rating.rating_value == "3.5"){
            $('.caption').find('span').text("3.5").attr('class','label label-success');
          }else if(data.rating.rating_value == "4"){
            $('.caption').find('span').text("4").attr('class','label label-success');
          }else if(data.rating.rating_value == "4.5"){
            $('.caption').find('span').text("4.5").attr('class','label label-success');
          }else if(data.rating.rating_value == "5"){
            $('.caption').find('span').text("5").attr('class','label label-success');
          }else{
            $('.caption').find('span').text("0").attr('class','label label-default');
          }
          $('.filled-stars').attr('style',data.rating.width);
        }else{
          $('.caption').find('span').text("0");
          $('.filled-stars').attr('style',"width: 0%;");
        }
        // show foodtruck details
        if(data.foodtruck_details != null){
          $('.cbn').val(data.foodtruck_details.cbn);
          $('.cbd').val(data.foodtruck_details.cbd);
          $('.thc').val(data.foodtruck_details.thc);
        }
        // show selected day and hours
        if(data.day_and_hour !=""){
          
          $.each(data.day_and_hour, function(k, v) {
            if(v.day_name =="mon"){
              if(v.start_time != "" || v.end_time !=""){
                $('.mon_start_value').val(v.start_time);
                $('.mon_last_value').val(v.end_time);
                $('.cross_mon').attr('class','item_closeicon cross_mon item_menu_image');
                $('.cross_mon i').attr('data-id','true')
                $('.menu_mon').show();
                $('.arrow_mon').show();
                $('.mon').html("");
              }else{
                $('.cross_mon').attr('class','item_closeicon disabled cross_mon');
                $('.cross_mon i').attr('data-id','false')
                $('.menu_mon').hide();
                $('.menu_mon').val("");
                $('.arrow_mon').hide();
                $('.mon').html("Closed");
              }
            }else if(v.day_name =="tue"){
              if(v.start_time != "" || v.end_time !=""){
                $('.tue_start_value').val(v.start_time);
                $('.tue_last_value').val(v.end_time);
                $('.cross_tue').attr('class','item_closeicon cross_tue item_menu_image');
                $('.cross_tue i').attr('data-id','true')
                $('.menu_tue').show();
                $('.arrow_tue').show();
                $('.tue').html("");
              }else{
                $('.cross_tue').attr('class','item_closeicon disabled cross_tue');
                $('.cross_tue i').attr('data-id','false')
                $('.menu_tue').hide();
                $('.menu_tue').val("");
                $('.arrow_tue').hide();
                $('.tue').html("Closed");
              }
            }else if(v.day_name =="wed"){
              if(v.start_time != "" || v.end_time !=""){
                $('.wed_start_value').val(v.start_time);
                $('.wed_last_value').val(v.end_time);
                $('.cross_wed').attr('class','item_closeicon cross_wed item_menu_image');
                $('.cross_wed i').attr('data-id','true')
                $('.menu_wed').show();
                $('.arrow_wed').show();
                $('.wed').html("");
              }else{
                $('.cross_wed').attr('class','item_closeicon disabled cross_wed');
                $('.cross_wed i').attr('data-id','false')
                $('.menu_wed').hide();
                $('.menu_wed').val("");
                $('.arrow_wed').hide();
                $('.wed').html("Closed");
              }
            }else if(v.day_name =="thu"){
              if(v.start_time != "" || v.end_time !=""){
                $('.thu_start_value').val(v.start_time);
                $('.thu_last_value').val(v.end_time);
                $('.cross_thu').attr('class','item_closeicon cross_thu item_menu_image');
                $('.cross_thu i').attr('data-id','true')
                $('.menu_thu').show();
                $('.arrow_thu').show();
                $('.thu').html("");
              }else{
                $('.cross_thu').attr('class','item_closeicon disabled cross_thu');
                $('.cross_thu i').attr('data-id','false')
                $('.menu_thu').hide();
                $('.menu_thu').val("");
                $('.arrow_thu').hide();
                $('.thu').html("Closed");
              }
            }else if(v.day_name =="fri"){
              if(v.start_time != "" || v.end_time !=""){
                $('.fri_start_value').val(v.start_time);
                $('.fri_last_value').val(v.end_time);
                $('.cross_fri').attr('class','item_closeicon cross_fri item_menu_image');
                $('.cross_fri i').attr('data-id','true')
                $('.menu_fri').show();
                $('.arrow_fri').show();
                $('.fri').html("");
              }else{
                $('.cross_fri').attr('class','item_closeicon disabled cross_fri');
                $('.cross_fri i').attr('data-id','false')
                $('.menu_fri').hide();
                $('.menu_fri').val("");
                $('.arrow_fri').hide();
                $('.fri').html("Closed");
              }
            }else if(v.day_name =="sat"){
              if(v.start_time != "" || v.end_time !=""){
                $('.sat_start_value').val(v.start_time);
                $('.sat_last_value').val(v.end_time);
                $('.cross_sat').attr('class','item_closeicon cross_sat item_menu_image');
                $('.cross_sat i').attr('data-id','true')
                $('.menu_sat').show();
                $('.arrow_sat').show();
                $('.sat').html("");
              }else{
                $('.cross_sat').attr('class','item_closeicon disabled cross_sat');
                $('.cross_sat i').attr('data-id','false')
                $('.menu_sat').hide();
                $('.menu_sat').val("");
                $('.arrow_sat').hide();
                $('.sat').html("Closed");
              }
            }else if(v.day_name =="sun"){
              if(v.start_time != "" || v.end_time !=""){
                $('.sun_start_value').val(v.start_time);
                $('.sun_last_value').val(v.end_time);
                $('.cross_sun').attr('class','item_closeicon cross_sun item_menu_image');
                $('.cross_sun i').attr('data-id','true')
                $('.menu_sun').show();
                $('.arrow_sun').show();
                $('.sun').html("");
              }else{
                $('.cross_sun').attr('class','item_closeicon disabled cross_sun');
                $('.cross_sun i').attr('data-id','false')
                $('.menu_sun').hide();
                $('.menu_sun').val("");
                $('.arrow_sun').hide();
                $('.sun').html("Closed");
              }
            }
          });
        }

        if (data.item.hide_item == true){
          $('input[name=prome-photo]').attr('checked', false);
          $('.make_checked').attr('data-idnum',false);
          $('.make_checked').attr('value','true');
          $('input[name=itemModel.hideItem]').attr('checked', true).click();
        }else{
          $('.make_checked').attr('data-idnum',true);
          $('.make_checked').attr('value','false');
          $('input[name=itemModel.hideItem]').attr('checked', false);
        }
        if (data.item !=""){
          $('.item_name').val(data.item.name);
          $('.item_id').val(data.item.id);
          $('.item_description').val(data.item.description);
        }
        if(data.price_and_combo != ""){
          if(data.price_and_combo[0] != "" && data.price_and_combo[0].record_id ==1){
            $('.name1').val(data.price_and_combo[0].name);
            $('.price1').val(data.price_and_combo[0].price);
            if(data.combo_options[0] != null && (data.combo_options[0].option1 != "" || data.combo_options[0].option2 != "" || data.combo_options[0].option3 != "" ||data.combo_options[0].option4 != "")){
              $('.combo_option1').addClass('make_button_active')
            }else{
              $('.combo_option1').removeClass('make_button_active')
            }
            if(data.combo_options[0] != null){
              var ids = [];
              $('.option1_selected li').each(function(){
                if((data.combo_options[0].option1.split(',')).includes(this.id)){
                  $(this).attr('class','option1_select selected');
                  ids.push(this.id); 
                  $(".first_hidden_option1").val(ids);
                }else{
                  $(this).attr('class','option1_select');
                }
              })
            }else{
              $('.option1_selected li').each(function(){
                var ids = [];
                $(".first_hidden_option1").val(ids);
                $(this).attr('class','option1_select');
              });
            }
            if(data.combo_options[0] != null){
              var ids = [];
              $('.option2_selected li').each(function(){
                if((data.combo_options[0].option2.split(',')).includes(this.id)){
                  $(this).attr('class','option2_select selected');
                  ids.push(this.id); 
                  $(".first_hidden_option2").val(ids);
                }else{
                  $(this).attr('class','option2_select');
                }
              })
            }else{
              var ids = [];
              $(".first_hidden_option2").val(ids);
              $(this).attr('class','option2_select');
            }
            if(data.combo_options[0] != null){
              var ids = [];
              $('.option3_selected li').each(function(){
                if((data.combo_options[0].option3.split(',')).includes(this.id)){
                  $(this).attr('class','option3_select selected');
                  ids.push(this.id); 
                  $(".first_hidden_option3").val(ids);
                }else{
                  $(this).attr('class','option3_select');
                }
              })
            }else{
              var ids = [];
              $(".first_hidden_option3").val(ids);
              $(this).attr('class','option3_select');
            }
            if(data.combo_options[0] != null){
              var ids = [];
              $('.option4_selected li').each(function(){
                if((data.combo_options[0].option4.split(',')).includes(this.id)){
                  $(this).attr('class','option4_select selected');
                  ids.push(this.id); 
                  $(".first_hidden_option4").val(ids);
                }else{
                  $(this).attr('class','option4_select');
                }
              })
            }else{
              var ids = [];
              $(".first_hidden_option4").val(ids);
              $(this).attr('class','option4_select');
            }
            // combo 2
            if(data.combo_options[1] != null && (data.combo_options[1].option1 != "" || data.combo_options[1].option2 != "" || data.combo_options[1].option3 != "" ||data.combo_options[1].option4 != "")){
              $('.combo_option2').addClass('make_button_active')
            }else{
              $('.combo_option2').removeClass('make_button_active')
            }

            if(data.combo_options[1] != null){
              var ids = [];
              $('.second_option1_selected li').each(function(){
                if((data.combo_options[1].option1.split(',')).includes(this.id)){
                  $(this).attr('class','second_option1_select selected');
                  ids.push(this.id); 
                  $(".second_hidden_option1").val(ids);
                }else{
                  $(this).attr('class','second_option1_select');
                }
              })
            }else{
              var ids = [];
              $(".second_hidden_option1").val(ids);
              $(this).attr('class','second_option1_select');
            }
            if(data.combo_options[1] != null){
              var ids = [];
              $('.second_option2_selected li').each(function(){
                if((data.combo_options[1].option2.split(',')).includes(this.id)){
                  $(this).attr('class','second_option2_select selected');
                  ids.push(this.id); 
                  $(".second_hidden_option2").val(ids);
                }else{
                  $(this).attr('class','second_option2_select');
                }
              })
            }else{
              var ids = [];
              $(".second_hidden_option2").val(ids);
              $(this).attr('class','second_option2_select');
            }
            if(data.combo_options[1] != null){
              var ids = [];
              $('.second_option3_selected li').each(function(){
                if((data.combo_options[1].option3.split(',')).includes(this.id)){
                  $(this).attr('class','second_option3_select selected');
                  ids.push(this.id); 
                  $(".second_hidden_option3").val(ids);
                }else{
                  $(this).attr('class','second_option3_select');
                }
              })
            }else{
              var ids = [];
              $(".second_hidden_option3").val(ids);
              $(this).attr('class','second_option3_select');
            }
            if(data.combo_options[1] != null){
              var ids = [];
              $('.second_option4_selected li').each(function(){
                if((data.combo_options[1].option4.split(',')).includes(this.id)){
                  $(this).attr('class','second_option4_select selected');
                  ids.push(this.id); 
                  $(".second_hidden_option4").val(ids);
                }else{
                  $(this).attr('class','second_option4_select');
                }
              })
            }else{
              var ids = [];
              $(".second_hidden_option4").val(ids);
              $(this).attr('class','second_option4_select');
            }
            //combo 3
            if(data.combo_options[2] != null && (data.combo_options[2].option1 != "" || data.combo_options[2].option2 != "" || data.combo_options[2].option3 != "" ||data.combo_options[2].option4 != "")){
              $('.combo_option3').addClass('make_button_active');
            }else{
              $('.combo_option3').removeClass('make_button_active');
            }

            if(data.combo_options[2] != null){
              var ids = [];
              $('.third_option1_selected li').each(function(){
                if((data.combo_options[2].option1.split(',')).includes(this.id)){
                  $(this).attr('class','third_option1_select selected');
                  ids.push(this.id); 
                  $(".third_hidden_option1").val(ids);
                }else{
                  $(this).attr('class','third_option1_select');
                }
              })
            }else{
              var ids = [];
              $(".third_hidden_option1").val(ids);
              $(this).attr('class','third_option1_select');
            }
            if(data.combo_options[2] != null){
              var ids = [];
              $('.third_option2_selected li').each(function(){
                if((data.combo_options[2].option2.split(',')).includes(this.id)){
                  $(this).attr('class','third_option2_select selected');
                  ids.push(this.id); 
                  $(".third_hidden_option2").val(ids);
                }else{
                  $(this).attr('class','third_option2_select');
                }
              })
            }else{
              var ids = [];
              $(".third_hidden_option2").val(ids);
              $(this).attr('class','third_option2_select');
            }
            if(data.combo_options[2] != null){
              var ids = [];
              $('.third_option3_selected li').each(function(){
                if((data.combo_options[2].option3.split(',')).includes(this.id)){
                  $(this).attr('class','third_option3_select selected');
                  ids.push(this.id); 
                  $(".third_hidden_option3").val(ids);
                }else{
                  $(this).attr('class','third_option3_select');
                }
              })
            }else{
              var ids = [];
              $(".third_hidden_option3").val(ids);
              $(this).attr('class','third_option3_select');
            }
            if(data.combo_options[2] != null){
              var ids = [];
              $('.third_option4_selected li').each(function(){
                if((data.combo_options[2].option4.split(',')).includes(this.id)){
                  $(this).attr('class','third_option4_select selected');
                  ids.push(this.id); 
                  $(".third_hidden_option4").val(ids);
                }else{
                  $(this).attr('class','third_option4_select');
                }
              })
            }else{
              var ids = [];
              $(".third_hidden_option4").val(ids);
              $(this).attr('class','third_option4_select');
            }

            // combo 4
            if(data.combo_options[3] != null && (data.combo_options[3].option1 != "" || data.combo_options[3].option2 != "" || data.combo_options[3].option3 != "" ||data.combo_options[3].option4 != "")){
              $('.combo_option4').addClass('make_button_active');
            }else{
              $('.combo_option4').removeClass('make_button_active');
            }

            if(data.combo_options[3] != null){
              var ids = [];
              $('.four_option1_selected li').each(function(){
                if((data.combo_options[3].option1.split(',')).includes(this.id)){
                  $(this).attr('class','four_option1_select selected');
                  ids.push(this.id); 
                  $(".four_hidden_option1").val(ids);
                }else{
                  $(this).attr('class','four_option1_select');
                }
              })
            }else{
              var ids = [];
              $(".four_hidden_option1").val(ids);
              $(this).attr('class','four_option1_select');
            }
            if(data.combo_options[3] != null){
              var ids = [];
              $('.four_option2_selected li').each(function(){
                if((data.combo_options[3].option2.split(',')).includes(this.id)){
                  $(this).attr('class','four_option2_select selected');
                  ids.push(this.id); 
                  $(".four_hidden_option2").val(ids);
                }else{
                  $(this).attr('class','four_option2_select');
                }
              })
            }else{
              var ids = [];
              $(".four_hidden_option2").val(ids);
              $(this).attr('class','four_option2_select');
            }
            if(data.combo_options[3] != null){
              var ids = [];
              $('.four_option3_selected li').each(function(){
                if((data.combo_options[3].option3.split(',')).includes(this.id)){
                  $(this).attr('class','four_option3_select selected');
                  ids.push(this.id); 
                  $(".four_hidden_option3").val(ids);
                }else{
                  $(this).attr('class','four_option3_select');
                }
              })
            }else{
              var ids = [];
              $(".four_hidden_option3").val(ids);
              $(this).attr('class','four_option3_select');
            }
            if(data.combo_options[3] != null){
              var ids = [];
              $('.four_option4_selected li').each(function(){
                if((data.combo_options[3].option4.split(',')).includes(this.id)){
                  $(this).attr('class','four_option4_select selected');
                  ids.push(this.id); 
                  $(".four_hidden_option4").val(ids);
                }else{
                  $(this).attr('class','four_option4_select');
                }
              })
            }else{
              var ids = [];
              $(".four_hidden_option4").val(ids);
              $(this).attr('class','four_option4_select');
            }
            // combo 5
            if(data.combo_options[4] != null && (data.combo_options[4].option1 != "" || data.combo_options[4].option2 != "" || data.combo_options[4].option3 != "" ||data.combo_options[4].option4 != "")){
              $('.combo_option5').addClass('make_button_active');
            }else{
              $('.combo_option5').removeClass('make_button_active');
            }

            if(data.combo_options[4] != null){
              var ids = [];
              $('.five_option1_selected li').each(function(){
                if((data.combo_options[4].option1.split(',')).includes(this.id)){
                  $(this).attr('class','five_option1_select selected');
                  ids.push(this.id); 
                  $(".five_hidden_option1").val(ids);
                }else{
                  $(this).attr('class','five_option1_select');
                }
              })
            }else{
              var ids = [];
              $(".five_hidden_option1").val(ids);
              $(this).attr('class','five_option1_select');
            }
            if(data.combo_options[4] != null){
              var ids = [];
              $('.five_option2_selected li').each(function(){
                if((data.combo_options[4].option2.split(',')).includes(this.id)){
                  $(this).attr('class','five_option2_select selected');
                  ids.push(this.id); 
                  $(".five_hidden_option2").val(ids);
                }else{
                  $(this).attr('class','five_option2_select');
                }
              })
            }else{
              var ids = [];
              $(".five_hidden_option2").val(ids);
              $(this).attr('class','five_option2_select');
            }
            if(data.combo_options[4] != null){
              var ids = [];
              $('.five_option3_selected li').each(function(){
                if((data.combo_options[0].option3.split(',')).includes(this.id)){
                  $(this).attr('class','five_option3_select selected');
                  ids.push(this.id); 
                  $(".five_hidden_option3").val(ids);
                }else{
                  $(this).attr('class','five_option3_select');
                }
              })
            }else{
              var ids = [];
              $(".five_hidden_option3").val(ids);
              $(this).attr('class','five_option3_select');
            }
            if(data.combo_options[4] != null){
              var ids = [];
              $('.five_option3_selected li').each(function(){
                if((data.combo_options[4].option4.split(',')).includes(this.id)){
                  $(this).attr('class','five_option4_select selected');
                  ids.push(this.id); 
                  $(".five_hidden_option4").val(ids);
                }else{
                  $(this).attr('class','five_option4_select');
                }
              })
            }else{
              var ids = [];
              $(".five_hidden_option4").val(ids);
              $(this).attr('class','five_option4_select');
            }

            // combo 6
            if(data.combo_options[5] != null && (data.combo_options[5].option1 != "" || data.combo_options[5].option2 != "" || data.combo_options[5].option3 != "" ||data.combo_options[5].option4 != "")){
              $('.combo_option6').addClass('make_button_active');
            }else{
              $('.combo_option6').removeClass('make_button_active');
            }
            if(data.combo_options[5] != null){
              var ids = [];
              $('.six_option1_selected li').each(function(){
                if((data.combo_options[5].option1.split(',')).includes(this.id)){
                  $(this).attr('class','six_option1_select selected');
                  ids.push(this.id); 
                  $(".six_hidden_option1").val(ids);
                }else{
                  $(this).attr('class','six_option1_select');
                }
              })
            }else{
              var ids = [];
              $(".six_hidden_option1").val(ids);
              $(this).attr('class','six_option1_select');
            }
            if(data.combo_options[5] != null){
              var ids = [];
              $('.six_option2_selected li').each(function(){
                if((data.combo_options[5].option2.split(',')).includes(this.id)){
                  $(this).attr('class','six_option2_select selected');
                  ids.push(this.id); 
                  $(".six_hidden_option2").val(ids);
                }else{
                  $(this).attr('class','six_option2_select');
                }
              })
            }else{
              var ids = [];
              $(".six_hidden_option2").val(ids);
              $(this).attr('class','six_option2_select');
            }
            if(data.combo_options[5] != null){
              var ids = [];
              $('.six_option3_selected li').each(function(){
                if((data.combo_options[5].option3.split(',')).includes(this.id)){
                  $(this).attr('class','six_option3_select selected');
                  ids.push(this.id); 
                  $(".six_hidden_option3").val(ids);
                }else{
                  $(this).attr('class','six_option3_select');
                }
              })
            }else{
              var ids = [];
              $(".six_hidden_option3").val(ids);
              $(this).attr('class','six_option3_select');
            }
            if(data.combo_options[5] != null){
              var ids = [];
              $('.six_option4_selected li').each(function(){
                if((data.combo_options[5].option4.split(',')).includes(this.id)){
                  $(this).attr('class','six_option4_select selected');
                  ids.push(this.id); 
                  $(".six_hidden_option4").val(ids);
                }else{
                  $(this).attr('class','six_option4_select');
                }
              })
            }else{
              var ids = [];
              $(".six_hidden_option4").val(ids);
              $(this).attr('class','six_option4_select');
            }
          }else{
            $('.name1').val("");
            $('.price1').val("");
          }
          if(data.price_and_combo[1] != "" && data.price_and_combo[1].record_id ==2){
            $('.name2').val(data.price_and_combo[1].name);
            $('.price2').val(data.price_and_combo[1].price);
          }else{
            $('.name2').val("");
            $('.price2').val("");
          }
          if(data.price_and_combo[2] != ""&& data.price_and_combo[2].record_id ==3){
            $('.name3').val(data.price_and_combo[2].name);
            $('.price3').val(data.price_and_combo[2].price);
          }else{
            $('.name3').val("");
            $('.price3').val("");
          }
          if(data.price_and_combo[3] != "" && data.price_and_combo[3].record_id ==4){
            $('.name4').val(data.price_and_combo[3].name);
            $('.price4').val(data.price_and_combo[3].price);
          }else{
            $('.name4').val("");
            $('.price4').val("");
          }
          if(data.price_and_combo[4] !="" && data.price_and_combo[4].record_id ==5){
            $('.name5').val(data.price_and_combo[4].name);
            $('.price5').val(data.price_and_combo[4].price);
          }else{
            $('.name5').val("");
            $('.price5').val("");
          }
          if(data.price_and_combo[5] != "" && data.price_and_combo[5].record_id == 6){
            $('.name6').val(data.price_and_combo[5].name);
            $('.price6').val(data.price_and_combo[5].price);
          }else{
            $('.name6').val("");
            $('.price6').val("");
          }
        }else{
          $('.name1 , .name2, .name3, .name4, .name5, .name6, .price1, .price2, .price3, .price4, .price5, .price6').val("");
        }
        //I assume you want to do something on controller action execution success?
        $(this).addClass('done');
      }
    });
  });

  // search items
  $(document).on('keyup','.search_item', function() {
    $.ajax({
      type:'get',
      url:'/item/search_item_value',
      data: { 
        key: $('.search_item').val()
      },
      success: function(data){
        $('.food-list ul').empty();
        if(data.length != 0){
          $.each(data, function(k, v) {
            $('.food-list ul').append($("<li>").text(v.name).addClass('select_item').attr('id',v.id));
          });
        }else{
          $('.food-list ul').append($("<li>").text("No Record Found"));
        }
        // response json to update the respective td
      }
    });
  });

  //hide item day time pop-up 
  $(document).on('click','.item_day_time',function(){
    $('#itemdaytime').modal('hide');
  })
  //hide item ingredient
  $(document).on('click','.item_ingredient',function(){
    $('#itemingredient').modal('hide');
  })
  $(document).on('click','.item_ingredient_select', function(){
    var ids = [];
    if($(this).attr('class') == "item_ingredient_select"){
      $(this).attr('class','item_ingredient_select selected');
    }else{
      $(this).attr('class','item_ingredient_select');
    }
    
    $('.ingredient_ids li.selected').each(function(){ ids.push(this.id); });
    $("#hidden_ingredient").val(ids);
  })



  $(document).ready(function(){

    // combo option 1
    $(document).on('click','.combo_option1',function(){
      $('.item_name1').html($('.item_name').val());
      if($('.name1').val() != "" && $('.price1').val() != ""){
        $('#combosoption1').modal();
        $('.option1_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "option1_select"){
            $(this).attr('class','option1_select selected');
          }else{
            $(this).attr('class','option1_select');
          }
          
          $('.option1_selected li.selected').each(function(){ ids.push(this.id); });
          $(".first_hidden_option1").val(ids);
        });

        $('.option2_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "option2_select"){
            $(this).attr('class','option2_select selected');
          }else{
            $(this).attr('class','option2_select');
          }
          
          $('.option2_selected li.selected').each(function(){ ids.push(this.id); });
          $(".first_hidden_option2").val(ids);
        });

        $('.option3_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "option3_select"){
            $(this).attr('class','option3_select selected');
          }else{
            $(this).attr('class','option3_select');
          }
          
          $('.option3_selected li.selected').each(function(){ ids.push(this.id); });
          $(".first_hidden_option3").val(ids);
        });

        $('.option4_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "option4_select"){
            $(this).attr('class','option4_select selected');
          }else{
            $(this).attr('class','option4_select');
          }
          
          $('.option4_selected li.selected').each(function(){ ids.push(this.id); });
          $(".first_hidden_option4").val(ids);
        });
      }else{
        toastr.error("Please enter name and price first.");
      }
    });

    // combo option 2
    $(document).on('click','.combo_option2',function(){
      $('.item_name1').html($('.item_name').val());
      if($('.name2').val() != "" && $('.price2').val() != ""){
        $('#combosoption2').modal();

        $('.second_option1_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "second_option1_select"){
            $(this).attr('class','second_option1_select selected');
          }else{
            $(this).attr('class','second_option1_select');
          }
          
          $('.second_option1_selected li.selected').each(function(){ ids.push(this.id); });
          $(".second_hidden_option1").val(ids);
        });

        $('.second_option2_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "second_option2_select"){
            $(this).attr('class','second_option2_select selected');
          }else{
            $(this).attr('class','second_option2_select');
          }
          
          $('.second_option2_selected li.selected').each(function(){ ids.push(this.id); });
          $(".second_hidden_option2").val(ids);
        });

        $('.second_option3_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "second_option3_select"){
            $(this).attr('class','second_option3_select selected');
          }else{
            $(this).attr('class','second_option3_select');
          }
          
          $('.second_option3_selected li.selected').each(function(){ ids.push(this.id); });
          $(".second_hidden_option3").val(ids);
        });

        $('.second_option4_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "second_option4_select"){
            $(this).attr('class','second_option4_select selected');
          }else{
            $(this).attr('class','second_option4_select');
          }
          
          $('.second_option4_selected li.selected').each(function(){ ids.push(this.id); });
          $(".second_hidden_option4").val(ids);
        });
      }else{
        toastr.error("Please enter name and price first.");
      }
    });

    // combo option 3
    $(document).on('click','.combo_option3',function(){
      $('.item_name1').html($('.item_name').val());
      if($('.name3').val() != "" && $('.price3').val() != ""){
        $('#combosoption3').modal();

        $('.third_option1_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "third_option1_select"){
            $(this).attr('class','third_option1_select selected');
          }else{
            $(this).attr('class','third_option1_select');
          }
          
          $('.third_option1_selected li.selected').each(function(){ ids.push(this.id); });
          $(".third_hidden_option1").val(ids);
        });

        $('.third_option2_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "third_option2_select"){
            $(this).attr('class','third_option2_select selected');
          }else{
            $(this).attr('class','third_option2_select');
          }
          
          $('.third_option2_selected li.selected').each(function(){ ids.push(this.id); });
          $(".third_hidden_option2").val(ids);
        });

        $('.third_option3_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "third_option3_select"){
            $(this).attr('class','third_option3_select selected');
          }else{
            $(this).attr('class','third_option3_select');
          }
          
          $('.third_option3_selected li.selected').each(function(){ ids.push(this.id); });
          $(".third_hidden_option3").val(ids);
        });

        $('.third_option4_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "third_option4_select"){
            $(this).attr('class','third_option4_select selected');
          }else{
            $(this).attr('class','third_option4_select');
          }
          
          $('.third_option4_selected li.selected').each(function(){ ids.push(this.id); });
          $(".third_hidden_option4").val(ids);
        });
      }else{
        toastr.error("Please enter name and price first.");
      }
    });

    // combo option 4
    $(document).on('click','.combo_option4',function(){
      $('.item_name1').html($('.item_name').val());
      if($('.name4').val() != "" && $('.price4').val() != ""){
        $('#combosoption4').modal();

        $('.four_option1_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "four_option1_select"){
            $(this).attr('class','four_option1_select selected');
          }else{
            $(this).attr('class','four_option1_select');
          }
          
          $('.four_option1_selected li.selected').each(function(){ ids.push(this.id); });
          $(".four_hidden_option1").val(ids);
        });

        $('.four_option2_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "four_option2_select"){
            $(this).attr('class','four_option2_select selected');
          }else{
            $(this).attr('class','four_option2_select');
          }
          
          $('.four_option2_selected li.selected').each(function(){ ids.push(this.id); });
          $(".four_hidden_option2").val(ids);
        });

        $('.four_option3_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "four_option3_select"){
            $(this).attr('class','four_option3_select selected');
          }else{
            $(this).attr('class','four_option3_select');
          }
          
          $('.four_option3_selected li.selected').each(function(){ ids.push(this.id); });
          $(".four_hidden_option3").val(ids);
        });

        $('.four_option4_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "four_option4_select"){
            $(this).attr('class','four_option4_select selected');
          }else{
            $(this).attr('class','four_option4_select');
          }
          
          $('.four_option4_selected li.selected').each(function(){ ids.push(this.id); });
          $(".four_hidden_option4").val(ids);
        });
      }else{
        toastr.error("Please enter name and price first.");
      }
    });

    // combo option 5
    $(document).on('click','.combo_option5',function(){
      $('.item_name1').html($('.item_name').val());
      if($('.name5').val() != "" && $('.price5').val() != ""){
        $('#combosoption5').modal();

        $('.five_option1_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "five_option1_select"){
            $(this).attr('class','five_option1_select selected');
          }else{
            $(this).attr('class','five_option1_select');
          }
          
          $('.five_option1_selected li.selected').each(function(){ ids.push(this.id); });
          $(".five_hidden_option1").val(ids);
        });

        $('.five_option2_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "five_option2_select"){
            $(this).attr('class','five_option2_select selected');
          }else{
            $(this).attr('class','five_option2_select');
          }
          
          $('.five_option2_selected li.selected').each(function(){ ids.push(this.id); });
          $(".five_hidden_option2").val(ids);
        });

        $('.five_option3_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "five_option3_select"){
            $(this).attr('class','five_option3_select selected');
          }else{
            $(this).attr('class','five_option3_select');
          }
          
          $('.five_option3_selected li.selected').each(function(){ ids.push(this.id); });
          $(".five_hidden_option3").val(ids);
        });

        $('.five_option4_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "five_option4_select"){
            $(this).attr('class','five_option4_select selected');
          }else{
            $(this).attr('class','five_option4_select');
          }
          
          $('.five_option4_selected li.selected').each(function(){ ids.push(this.id); });
          $(".five_hidden_option4").val(ids);
        });
      }else{
        toastr.error("Please enter name and price first.");
      }
    });

    // combo option 6
    $(document).on('click','.combo_option6',function(){
      $('.item_name1').html($('.item_name').val());
      if($('.name6').val() != "" && $('.price6').val() != ""){
        $('#combosoption6').modal();

        $('.six_option1_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "six_option1_select"){
            $(this).attr('class','six_option1_select selected');
          }else{
            $(this).attr('class','six_option1_select');
          }
          
          $('.six_option1_selected li.selected').each(function(){ ids.push(this.id); });
          $(".six_hidden_option1").val(ids);
        });

        $('.six_option2_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "six_option2_select"){
            $(this).attr('class','six_option2_select selected');
          }else{
            $(this).attr('class','six_option2_select');
          }
          
          $('.six_option2_selected li.selected').each(function(){ ids.push(this.id); });
          $(".six_hidden_option2").val(ids);
        });

        $('.six_option3_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "six_option3_select"){
            $(this).attr('class','six_option3_select selected');
          }else{
            $(this).attr('class','six_option3_select');
          }
          
          $('.six_option3_selected li.selected').each(function(){ ids.push(this.id); });
          $(".six_hidden_option3").val(ids);
        });

        $('.six_option4_select').unbind().on('click',function(){
          var ids = [];
          if($(this).attr('class') == "six_option4_select"){
            $(this).attr('class','six_option4_select selected');
          }else{
            $(this).attr('class','six_option4_select');
          }
          
          $('.six_option4_selected li.selected').each(function(){ ids.push(this.id); });
          $(".six_hidden_option4").val(ids);
        });
      }else{
        toastr.error("Please enter name and price first.");
      }
    });
  });
  $(document).on('click', '.clear-item-btn',function(){
    $("#add_item").trigger('click');
  });

  $(document).on('blur', ".price1, .price2, .price3, .price4, .price5, .price6", function(){
    value=$(this).val();
    if (value!=""){
      new_value=parseFloat(value).toFixed(2)
      if (new_value == 'NaN'){
        $(this).val("");
      }else{
        $(this).val(new_value);
      }
    }
  });


  //hide item day time pop-up 
  $(document).on('click','.c_option1',function(){
    $('#combosoption1').modal('hide');
  })
  $(document).on('click','.c_option2',function(){
    $('#combosoption2').modal('hide');
  })
  $(document).on('click','.c_option3',function(){
    $('#combosoption3').modal('hide');
  })
  $(document).on('click','.c_option4',function(){
    $('#combosoption4').modal('hide');
  })
  $(document).on('click','.c_option5',function(){
    $('#combosoption5').modal('hide');
  })
  $(document).on('click','.c_option6',function(){
    $('#combosoption6').modal('hide');
  })
  
  $(document).on('click','.close-item_img',function(){
    $(this).parent().hide();
    $.ajax({
      type:'delete',
      url:'/delete_profile_image',
      data: { 
        'i_id': this.id
      },
      success:function(data){
        toastr.success("Selected image deleted successfully.")
        //I assume you want to do something on controller action execution success?
      }
    });
  });
});