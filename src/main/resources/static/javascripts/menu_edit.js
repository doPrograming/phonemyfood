// $(function () {
//   $('.datetimepicker').timepicki();
// });

$(document).ready(function(){
  $(document).on('click','.menu_image1',function(){
    if ($(this).attr('data-id') == "true" ){
      if ($(this).attr('day_type') == "mon"){
        $(this).parent().parent().find('.cross_mon').attr('class','edit_menu_closeicon disabled cross_mon');
        $(this).parent().parent().find('.menu_mon').hide();
        $(this).parent().parent().find('.arrow_mon').hide();
        $(this).parent().parent().find('.mon_start_value').val("")
        $(this).parent().parent().find('.mon_last_value').val("")
        $(this).parent().parent().find('.mon').html("Closed");
      } 
      $(this).attr("data-id", "false");
    }else{
      if ($(this).attr('day_type') == "mon"){
        $(this).parent().parent().find('.cross_mon').attr('class','edit_menu_closeicon cross_mon');
        $(this).parent().parent().find('.menu_mon').show();
        $(this).parent().parent().find('.arrow_mon').show();
        $(this).parent().parent().find('.mon').html("");
        $(this).parent().parent().find('.mon_start_value').val($(this).parent().parent().find('.mon_start_value').attr('value'));
        $(this).parent().parent().find('.mon_last_value').val($(this).parent().parent().find('.mon_last_value').attr('value'));
        $(this).attr("data-id", "true");
      }else{
        $(this).parent().parent().toggle();
        $(this).parent().parent().siblings().toggle();
      }        
    }

  });

  // delete menu checkbox
  $(document).on('click','.delete_menu_checkbox',function(){
    if ($(this).attr('date-id') == 'false'){
      $('.delete_user_menu').removeAttr("disabled");
      $(this).attr('date-id','true');
    }else{
      $('.delete_user_menu').attr('disabled','disabled');
      $(this).attr('date-id','false');
    }
  })

  // delete seleted menu
  $(document).on('click','.delete_user_menu',function(){
    if(confirm("Are you sure to delete this Menu.")){
      $('.edit_menu_box').hide();
      $.ajax({
        type:'delete',
        url:'menus',
        data: { 
          'i_id' : $('.delete_menu_button').val() 
        },
        success:function(){
          //I assume you want to do something on controller action execution success?
          $(this).addClass('done');
        }
      });
    }
  });
});