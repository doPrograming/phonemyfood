$(document).ready(function(){
  $(document).on('click','.about_menu_image',function(){
    if ($(this).attr('data-id') == "true" ){
      if ($(this).attr('day_type') == "mon"){
        $('.cross_mon').attr('class','about_closeicon disabled cross_mon');
        $('.menu_mon').hide();
        $('.menu_mon').val("");
        $('.arrow_mon').hide();
        $('.mon').html("Closed");
        $('.mon_row').attr('class','row disabled mon_row');
        $('.mon_day').attr('class','col2 disabled mon_day');
      }else if ($(this).attr('day_type') == "tue"){
        $('.cross_tue').attr('class','about_closeicon disabled cross_tue');
        $('.menu_tue').hide();
        $('.menu_tue').val("");
        $('.arrow_tue').hide();
        $('.tue').html("Closed");
        $('.tue_row').attr('class','row disabled tue_row');
        $('.tue_day').attr('class','col2 disabled tue_day');
      }else if ($(this).attr('day_type') == "wed"){
        $('.cross_wed').attr('class','about_closeicon disabled cross_wed');
        $('.menu_wed').hide();
        $('.menu_wed').val("");
        $('.arrow_wed').hide();
        $('.wed').html("Closed");
        $('.wed_row').attr('class','row disabled wed_row');
        $('.wed_day').attr('class','col2 disabled wed_day');
      }else if ($(this).attr('day_type') == "thu"){
        $('.cross_thu').attr('class','about_closeicon disabled cross_thu');
        $('.menu_thu').hide();
        $('.menu_thu').val("");
        $('.arrow_thu').hide();
        $('.thu').html("Closed");
        $('.thu_row').attr('class','row disabled thu_row');
        $('.thu_day').attr('class','col2 disabled thu_day');
      }else if ($(this).attr('day_type') == "fri"){
        $('.cross_fri').attr('class','about_closeicon disabled cross_fri');
        $('.menu_fri').hide();
        $('.menu_fri').val("");
        $('.arrow_fri').hide();
        $('.fri').html("Closed");
        $('.fri_row').attr('class','row disabled fri_row');
        $('.fri_day').attr('class','col2 disabled fri_day');
      }else if ($(this).attr('day_type') == "sat"){
        $('.cross_sat').attr('class','about_closeicon disabled cross_sat');
        $('.menu_sat').hide();
        $('.menu_sat').val("");
        $('.arrow_sat').hide();
        $('.sat').html("Closed");
        $('.sat_row').attr('class','row disabled sat_row');
        $('.sat_day').attr('class','col2 disabled sat_day');
      } else if ($(this).attr('day_type') == "sun"){
        $('.cross_sun').attr('class','about_closeicon disabled cross_sun');
        $('.menu_sun').hide();
        $('.menu_sun').val("");
        $('.arrow_sun').hide();
        $('.sun').html("Closed");
        $('.sun_row').attr('class','row disabled sun_row');
        $('.sun_day').attr('class','col2 disabled sun_day');
      } 
      $(this).attr("data-id", "false"); 
    }else{
      if ($(this).attr('day_type') == "mon"){
        $('.cross_mon').attr('class','about_closeicon cross_mon');
        $('.menu_mon').show();
        $('.arrow_mon').show();
        $('.mon').html("");
        $('.mon_row').attr('class','row mon_row');
        $('.mon_day').attr('class','col2 mon_day');
      }else if ($(this).attr('day_type') == "tue"){
        $('.cross_tue').attr('class','about_closeicon cross_tue');
        $('.menu_tue').show();
        $('.arrow_tue').show();
        $('.tue').html("");
        $('.tue_row').attr('class','row tue_row');
        $('.tue_day').attr('class','col2 tue_day');
      }else if ($(this).attr('day_type') == "wed"){
        $('.cross_wed').attr('class','about_closeicon cross_wed');
        $('.menu_wed').show();
        $('.arrow_wed').show();
        $('.wed').html("");
        $('.wed_row').attr('class','row wed_row');
        $('.wed_day').attr('class','col2 wed_day');
      }else if ($(this).attr('day_type') == "thu"){
        $('.cross_thu').attr('class','about_closeicon cross_thu');
        $('.menu_thu').show();
        $('.arrow_thu').show();
        $('.thu').html("");
        $('.thu_row').attr('class','row thu_row');
        $('.thu_day').attr('class','col2 thu_day');
      }else if ($(this).attr('day_type') == "fri"){
        $('.cross_fri').attr('class','about_closeicon cross_fri');
        $('.menu_fri').show();
        $('.arrow_fri').show();
        $('.fri').html("");
        $('.fri_row').attr('class','row fri_row');
        $('.fri_day').attr('class','col2 fri_day');
      }else if ($(this).attr('day_type') == "sat"){
        $('.cross_sat').attr('class','about_closeicon cross_sat');
        $('.menu_sat').show();
        $('.arrow_sat').show();
        $('.sat').html("");
        $('.sat_row').attr('class','row sat_row');
        $('.sat_day').attr('class','col2 sat_day');
      }else if ($(this).attr('day_type') == "sun"){
        $('.cross_sun').attr('class','about_closeicon cross_sun');
        $('.menu_sun').show();
        $('.arrow_sun').show();
        $('.sun').html("");
        $('.sun_row').attr('class','row sun_row');
        $('.sun_day').attr('class','col2 sun_day');
      }  
      $(this).attr("data-id", "true");
    }
    
  });
  
});