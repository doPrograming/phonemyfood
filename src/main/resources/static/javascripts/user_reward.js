$(document).ready(function(){
  $(document).on('click','.closeicon',function(){
    if ($(this).parent().find('i').attr('data-id') == "true"){
      if ($(this).find('i').attr('reward-type') == 'Recommend a friend'){
        $(this).attr('class','closeicon selected');
        $('.user_reward0').find('input').attr('disabled','disabled');
        $('.user_reward0').find('input').val("");
      }else if($(this).find('i').attr('reward-type') == '${value1} Free Food!'){
        $(this).attr('class','closeicon selected');
        $('.user_reward1').find('input').attr('disabled','disabled');
        $('.user_reward1').find('input').val("");
      }else if($(this).find('i').attr('reward-type') == 'Wheel of food (Great Prizes)'){
        $(this).attr('class','closeicon selected');
        $('.user_reward2').find('input').attr('disabled','disabled');
        $('.user_reward2').find('input').val("");
      }else if($(this).find('i').attr('reward-type') == 'First Social Review'){
        $(this).attr('class','closeicon selected');
        $('.user_reward3').find('input').attr('disabled','disabled');
        $('.user_reward3').find('input').val("");
      }else if($(this).find('i').attr('reward-type') == 'Free Gift'){
        $(this).attr('class','closeicon selected');
        $('.user_reward4').find('input').attr('disabled','disabled');
        $('.user_reward4').find('input').val("");
      }
      $(this).find('i').attr('data-id','false');
    }else{
      if ($(this).parent().find('i').attr('reward-type') == "Recommend a friend"){
        $(this).attr('class','closeicon');
        $('.user_reward0').find('input').removeAttr("disabled");
      }else if ($(this).parent().find('i').attr('reward-type') == "${value1} Free Food!"){
        $(this).attr('class','closeicon');
        $('.user_reward1').find('input').removeAttr("disabled");
      }else if($(this).parent().find('i').attr('reward-type') == "Wheel of food (Great Prizes)"){
        $(this).attr('class','closeicon');
        $('.user_reward2').find('input').removeAttr("disabled");
      }else if($(this).parent().find('i').attr('reward-type') == "First Social Review"){
        $(this).attr('class','closeicon');
        $('.user_reward3').find('input').removeAttr("disabled");
      }else if($(this).parent().find('i').attr('reward-type') == "Free Gift"){
        $(this).attr('class','closeicon');
        $('.user_reward4').find('input').removeAttr("disabled");
      }
      $(this).parent().find('i').attr('data-id','true');
    }
  });
  $(document).on('click','#submit_reward',function(){
    $('.user_reward').off('click');
  });
  var new_value = false;
  var all_inputs = $('#reward_form :input')
  $(document).on('change', all_inputs, function(e) {
    new_value = true
  });
  $(document).on('focusout', all_inputs, function(e) {
    if (new_value){
      new_value = false;
      e.preventDefault(); 
      $("#reward_form").trigger('submit');
    }
    e.preventDefault(); 
  });
});

$("#reward-contents input").keypress(function(e){
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))
        return false;
});