
// get day and hours for about
$(document).on('click','.about_day_hours',function(){
  //hide item day time pop-up 
  $('.close_about_day_time').unbind().on('click',function(){
    $('#aboutdaytime').modal('hide');
  })
  if($('.user_id').val() != ""){
    $.ajax({
      type:'get',
      url:'/get_about_day_hours',
      data: {
        'user_id' : $('.user_id').val()
      },
      success: function(data){
        if(data !=""){
          $.each(data, function(k, v) {
            if(v.day_name =="mon"){
              if(v.start_time != "" || v.end_time !=""){
                $('.mon_start_value').val(v.start_time);
                $('.mon_last_value').val(v.end_time);
              }else{
                $('.cross_mon').attr('class','about_closeicon disabled cross_mon');
                $('.cross_mon i').attr('data-id','false');
                $('.menu_mon').hide();
                $('.menu_mon').val("");
                $('.arrow_mon').hide();
                $('.mon').html("Closed");
              }
            }else if(v.day_name =="tue"){
              if(v.start_time != "" || v.end_time !=""){
                $('.tue_start_value').val(v.start_time);
                $('.tue_last_value').val(v.end_time);
              }else{
                $('.cross_tue').attr('class','about_closeicon disabled cross_tue');
                $('.cross_tue i').attr('data-id','false');
                $('.menu_tue').hide();
                $('.menu_tue').val("");
                $('.arrow_tue').hide();
                $('.tue').html("Closed");
              }
            }else if(v.day_name =="wed"){
              if(v.start_time != "" || v.end_time !=""){
                $('.wed_start_value').val(v.start_time);
                $('.wed_last_value').val(v.end_time);
              }else{
                $('.cross_wed').attr('class','about_closeicon disabled cross_wed');
                $('.cross_wed i').attr('data-id','false')
                $('.menu_wed').hide();
                $('.menu_wed').val("");
                $('.arrow_wed').hide();
                $('.wed').html("Closed");
              }
            }else if(v.day_name =="thu"){
              if(v.start_time != "" || v.end_time !=""){
                $('.thu_start_value').val(v.start_time);
                $('.thu_last_value').val(v.end_time);
              }else{
                $('.cross_thu').attr('class','about_closeicon disabled cross_thu');
                $('.cross_thu i').attr('data-id','false')
                $('.menu_thu').hide();
                $('.menu_thu').val("");
                $('.arrow_thu').hide();
                $('.thu').html("Closed");
              }
            }else if(v.day_name =="fri"){
              if(v.start_time != "" || v.end_time !=""){
                $('.fri_start_value').val(v.start_time);
                $('.fri_last_value').val(v.end_time);
              }else{
                $('.cross_fri').attr('class','about_closeicon disabled cross_fri');
                $('.cross_fri i').attr('data-id','false')
                $('.menu_fri').hide();
                $('.menu_fri').val("");
                $('.arrow_fri').hide();
                $('.fri').html("Closed");
              }
            }else if(v.day_name =="sat"){
              if(v.start_time != "" || v.end_time !=""){
                $('.sat_start_value').val(v.start_time);
                $('.sat_last_value').val(v.end_time);
              }else{
                $('.cross_sat').attr('class','about_closeicon disabled cross_sat');
                $('.cross_sat i').attr('data-id','false')
                $('.menu_sat').hide();
                $('.menu_sat').val("");
                $('.arrow_sat').hide();
                $('.sat').html("Closed");
              }
            }else if(v.day_name =="sun"){
              if(v.start_time != "" || v.end_time !=""){
                $('.sun_start_value').val(v.start_time);
                $('.sun_last_value').val(v.end_time);
              }else{
                $('.cross_sun').attr('class','about_closeicon disabled cross_sun');
                $('.cross_sun i').attr('data-id','false')
                $('.menu_sun').hide();
                $('.menu_sun').val("");
                $('.arrow_sun').hide();
                $('.sun').html("Closed");
              }
            }
          });
        }
      }
    });
  }
  
})

$(document).ready(function(){
  var new_value = false;
  var all_inputs = $('#media-dropzone :input')
  $(document).on('change', all_inputs, function(e) {
    new_value = true
  });
  $(document).on('focusout', all_inputs, function(e) {
    if (new_value){
      new_value = false;
      e.preventDefault(); 
      $("#media-dropzone").trigger('submit');
    }
    e.preventDefault(); 
  });
});