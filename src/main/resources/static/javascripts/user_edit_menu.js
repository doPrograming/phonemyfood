$('input,textarea').focus(function(){
  $(this).removeAttr('placeholder');
});

$(document).ready(function(){

  $( "#sortable" ).sortable();
  $( "#sortable" ).disableSelection();       
  
  // menu builder form
  $(document).on('change','#select_menu', function() {
    $('.builder_menu').val($(this).find("option:selected").attr("id"));
  })

  $(document).on('change','#select_category', function() {
    $('.builder_category').val($(this).find("option:selected").attr('id'));
    if($(this).find("option:selected").val() == "Add Category"){
      $('#category').modal();
    }
  })
 
  $(document).on('click','.menu_builder',function(){
    var ids = [];
    $('.items li.selected').each(function(){ ids.push(this.id); });
    if($('.builder_menu').val() !="" && $('.builder_category').val() != "" && ids !=""){ 
      $.ajax({
        type:'post',
        url:'/menus/add_menu_builder',
        data: { 
          menu_id: $('.builder_menu').val(), 
          c_id: $('.builder_category').val(),
          i_ids: ids
        },
        success:function(data){
          var ids = [];
          $('.select_menu_builder').val("Please select menu");
          $('#select_category').val("Please select category");
          $('.items li').attr('class',"");
          toastr.success(data[0]);
          //I assume you want to do something on controller action execution success?
          $(this).addClass('done');
        }
      });
    }else if($('.builder_menu').val() == "") {
      toastr.warning("Please select Menu first");
    }else if ($('.builder_category').val() == ""){
      toastr.warning("Please select Category");
    }else if (ids ==""){
      toastr.warning("Please select Items");
    }
  });
  
  // select selected items
  $(document).on('change',".select_menu_builder",function(){
    
    if ($('.select_menu_builder')[0].value != "Please select menu" && $('.select_menu_builder')[1].value != "Please select category"){
      $.ajax({
        type:'get',
        url:'/menus/get_already_selected_items',
        data: { 
          menu_id: $($('.select_menu_builder')[0]).find("option:selected").attr('id'),
          category_id: $($('.select_menu_builder')[1]).find("option:selected").attr('id')
        },
        success:function(data){
          $('ul.items li').each(function(){
            var condition = data.includes($(this).attr('id'));
            if (condition){
              $(this).attr('class','selected')
            }
            else{
              $(this).attr('class','')
            }
          });
        }
      });
    }
  });

  //menu order form
  $(document).on('change','.select_menu_order', function() {
    if ($('.select_menu_order')[0].value != "Please select menu" && $('.select_menu_order')[1].value != "Please select category"){
      $.ajax({
        type:'get',
        url:'/menus/get_category_item',
        data: { 
          menu_id: $($('.select_menu_order')[0]).find("option:selected").attr('id'),
          category_id: $($('.select_menu_order')[1]).find("option:selected").attr('id') 
        },
        success: function(data){
          $('.menuorderlist ul').empty();
          if(data.length != 0){
            $.each(data, function(k, v) {
              $('.menuorderlist ul').append($("<li>").text(v.name).val(v.id));
            });
          }
          
          // response json to update the respective td
        }
      });
    }
  })


  // save menu order 
  $(document).on('click','.menu_order',function(){
    if ($('.select_menu_order')[0].value != "Please select menu" && $('.select_menu_order')[1].value != "Please select category"){
      var ids = [];
      $('.menuorderlist ul li').each(function(){ ids.push(this.value); });

      $.ajax({
        type:'post',
        url:'/menus/save_menu_order',
        data: { 
          menu_id: $($('.select_menu_order')[0]).find("option:selected").attr('id'),
          category_id: $($('.select_menu_order')[1]).find("option:selected").attr('id'),
          i_ids: ids 
        },
        success: function(data){
          toastr.success(data[0]);
        }
      });
    }else{
      toastr.warning("Please select Menu and Category first.");
    }
  });

  //add new category 
$(document).on("click", ".category_button", function() {
	if($(".new_category").val() != ""){
		$.ajax({
	        type: "get",
	        url: "/phonemyfood/user/category/add_new_category",
	        data: {
	            c_name: $(".new_category").val()
	        },
	        success: function(t) {
	        	if(t=="exist"){
	        		toastr.warning("Category name already exist");
	        	}
	        	else{
	        		var div_data =  "";
	        		$.ajax({
	                    type: "get",
	                    url: "/phonemyfood/user/category/add_new_category",
	                    data: {
	                        c_name: $(".new_category").val()
	                    },
	                    success: function(t) {
	                    	if(t!=null){
	                    		toastr.success("category added"),
	                    		$("select#select_category option").eq(-1).before("<option value="+t.id+">"+t.name+"</option>").attr("id", t.id).text(t.name).attr("class", ""),
	                    		$("select#select_category option").eq(-1).attr("class", "add_category_color"),
	                    		$(".new_category").val(""),
	                    		$(".new_category").attr("placeholder", "Add Category name")
	                    	}
	                    }
	                })
	        	}
	        }
	    })
	}
	else{
		toastr.error("Please enter category name first.");
		}
});
	    
  
	$(document).on("click", ".close-add-cat-popup", function() {
	    $("#category").modal("hide")
	})

});