$(document).ready(function(){
	//var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-##############"}];
	var phoneMask = "(###) ###-####";
	$('.client_phone').mask(phoneMask, {placeholder: "(xxx) xxx-xxxx"});
  $(document).on('click','.add_edit_hours_option',function(){
    if($('.item-selected').attr('id') != null){
      $('#clienthours').modal();
    }else{
      toastr.info("Please create Business first.");
    }
  });
  
  $(document).on('click','.delete_client',function(){
	  if($('.item-selected').attr('id') != null){
	  $.ajax({
		  type : "get",
		  url : contextURL + "admin/deleteClientModal",
		  data : {
			  "id" : $('.item-selected').attr('id')
		  },
	  }).done(function(data){
		  $("#deleteConfirm").html(data);
		  $("#deleteConfirm").modal();
	  });
	  }else{
		  toastr.info("Please select Business first.");
	  }
	  
  });
  // delete selected client
  $(document).on('click','#delete_client_yes',function(){
      $.ajax({
        type:'get',
        url: contextURL + 'admin/deleteSelectedClient',
        data: { 
          key: $('.item-selected').attr('id')
        },
        success: function(){
        	location.reload();
        }
      });
  });

  //hide item day time pop-up 
  $(document).on('click','.client_day_time',function(){
    $('#clienthours').modal('hide');
  })
  //hide item day time pop-up 
  $(document).on('click','.item_image', function(){
    $('#clientpicture').modal('hide');
  })

  //search client
  $(document).on('keyup','.search_business', function() {
    $.ajax({
      type:'get',
      url:'searchBusinessValue',
      data: { 
        key: $('.search_business').val()
      },
      success: function(data){
        $('.business-list ul').empty();
        if(data.length != 0){
          $.each(data, function(k, v) {
            $('.business-list ul').append($("<li>").text(v.name).addClass('select_business').attr('id',v.id));
          });
        }else{
          $('.business-list ul').append($("<li>").text("No Record Found"));
        }
        // response json to update the respective td
      }
    });
  });
});

$(document).ready(function() {

  // hide item
  $(document).on('click','.make_user_active',function(){
    if($(this).find('input').attr('data-id') == "true"){
      $(this).find('input').attr('value','true');
      $(this).find('input').attr('data-id','false');
    }else{
      $(this).find('input').attr('value','false'); 
      $(this).find('input').attr('data-id','true'); 
    }
  });

  

  $(document).on('click','.close-business_img',function(){
    $(this).parent().hide();
    //image_ids.splice(image_ids.indexOf(this.id),1);
    $.ajax({
      type:'delete',
      url:'/delete_profile_image',
      data: { 
        'i_id': this.id
      },
      success:function(data){
        toastr.success("Selected image deleted successfully.")
        //I assume you want to do something on controller action execution success?
      }
    });
  });

  $(document).on('change','.client_check_box',function(){
    if(this.name == "marijuana"){
      $(this).prop('checked', true);
      $('[name=foodtruck]').prop('checked', false);
      $('[name=restaurent]').prop('checked', false);
    }else if(this.name == "foodtruck") {
      $(this).prop('checked', true);
      $('[name=marijuana]').prop('checked', false);
      $('[name=restaurent]').prop('checked', false);
    }else if(this.name == "restaurent"){
      $(this).prop('checked', true);
      $('[name=marijuana]').prop('checked', false);
      $('[name=foodtruck]').prop('checked', false);
    }
  });

  $(document).on("click", ".select_business", function() {
		$(".select_business").removeClass("item-selected"), $(this).addClass("item-selected"), $(".business_id").val($(this).attr("id")), $.ajax({
			type : "get",
			url : "getClientDetails",
			data : {
				b_id : $(this).attr("id")
			},
			success : function(data) {
				$('.business').attr('data-id','true')
				$('button').removeAttr('disabled')
				if(data.businesses != null){
		        	$(".business_id").val(data.businesses.id);
		        	$('.business_name').val(data.businesses.name);
		        }else{
		        	$('.business_name').val("");
		        	$(".business_id").val("");
		        }
		        if(data.user != null){
		        	$(".user_id").val(data.user.id)
		          if (data.user.active == true){
		        	  $('input[id=active_user]').attr('checked', false);
		        	  $('input[id=active_user]').attr('checked', true).click();
		          }else{
		        	  $('.make_active').attr('data-id','true');
		        	  $('.make_active').attr('value','false');
		        	  $('input[id=active_user]').attr('checked', false);
		          }        
		          //$("#add_goto_button").html('<a href="/login_with_normal_user?user_id='+ data.user.id +'">Go to</a>');
		          $("#add_goto_button").html('<a href="/phonemyfood/admin/normalLogin?user_id=' + data.user.id + '">Go to</a>');
		          $("#reset_password_button").show();
		          $("#reset_password_button").attr("onclick", "resetPassword(" + data.user.id + ")");
		          if(data.user.name != ""){
		          $('.first_name').val(data.user.name.split(' ')[0]);
		            $('.last_name').val(data.user.name.split(' ')[1]);
		          }
		          if(data.user.email != ""){
		            $('.client_email').val(data.user.email);
		          }
		        }else{
		        	$(".user_id").val("")
		        	$('.first_name').val("");
		        	$('.last_name').val("");
		        	$('.client_email').val("").attr('readonly','false');
		        }
		        if(data.userAddresses != null){
		        	$(".address_id").val(data.userAddresses.id)
		        	$('.client_address').val(data.userAddresses.address);
		        	$('.client_phone').val(data.userAddresses.phoneNumber);
		        	$('.client_city').val(data.userAddresses.city);
		        	$('.client_state').val(data.userAddresses.state);
		          	$('.client_zip').val(data.userAddresses.zip);
		        }else{
		        	$(".address_id").val("");
		        	$('.client_address').val("");
		        	$('.client_phone').val("");
		        	$('.client_city').val("");
		        	$('.client_state').val("");
		        	$('.client_zip').val("");
		        }
		        if(data.role != ""){
		          if(data.businesses.businessType =="restaurent"){
		            $('[value=restaurent]').prop('checked', true);
		            $('[value=marijuana]').prop('checked', false);
		            $('[value=foodtruck]').prop('checked', false);
		          }else if(data.businesses.businessType =="marijuana"){
		            $('[value=restaurent]').prop('checked', false);
		            $('[value=marijuana]').prop('checked', true);
		            $('[value=foodtruck]').prop('checked', false);
		          }else if(data.businesses.businessType =="foodtruck"){
		            $('[value=restaurent]').prop('checked', false);
		            $('[value=marijuana]').prop('checked', false);
		            $('[value=foodtruck]').prop('checked', true);
		          }
		        }else{
		          $('[value=restaurent]').prop('checked', true);
		          $('[value=marijuana]').prop('checked', false);
		          $('[value=foodtruck]').prop('checked', false);
		        }
		        $("input[name='user_type'][value='client']").prop('checked', true);
		        // show selected day and hours
		        if(data.daysAndHours !=""){
		          $.each(data.daysAndHours, function(k, v) {
		            if(v.dayName =="mon"){		            	
		              if(v.startTime != "" || v.endTime !=""){
		            	$('.mon_id').val(v.id);  
		                $('.mon_start_value').val(v.startTime);
		                $('.mon_last_value').val(v.endTime);
		                $('.cross_mon').attr('class','edit_client_closeicon cross_mon');
		                $('.cross_mon').attr('data-id','true');
		                $('.menu_mon').show();
		                $('.arrow_mon').show();
		                $('.mon').html("");
		                $('.mon_row').attr('class','row mon_row');
		                $('.mon_day').attr('class','col2 mon_day');
		              }else{
		            	 $('.mon_id').val(v.id);
		                 $('.cross_mon').attr('class','edit_client_closeicon disabled cross_mon');
		                 $('.cross_mon i').attr('data-id','false');
		                 $('.menu_mon').hide();
		                 $('.menu_mon').val("");
		                 $('.arrow_mon').hide();
		                 $('.mon').html("Closed");
		                 $('.mon_row').attr('class','row disabled mon_row');
		                 $('.mon_day').attr('class','col2 disabled mon_day');
		              }
		            }else if(v.dayName =="tue"){
		              if(v.startTime != "" || v.endTime !=""){
		            	$('.tue_id').val(v.id);
		                $('.tue_start_value').val(v.startTime);
		                $('.tue_last_value').val(v.endTime);
		                $('.cross_tue').attr('class','edit_client_closeicon cross_tue');
		                $('.cross_tue').attr('data-id','true');
		                $('.menu_tue').show();
		                $('.arrow_tue').show();
		                $('.tue').html("");
		                $('.tue_row').attr('class','row tue_row');
		                $('.tue_day').attr('class','col2 tue_day');
		              }else{
		            	 $('.tue_id').val(v.id);
		                 $('.cross_tue').attr('class','edit_client_closeicon disabled cross_tue');
		                 $('.cross_tue i').attr('data-id','false');
		                 $('.menu_tue').hide();
		                 $('.menu_tue').val("");
		                 $('.arrow_tue').hide();
		                 $('.tue').html("Closed");
		                 $('.tue_row').attr('class','row disabled tue_row');
		                 $('.tue_day').attr('class','col2 disabled tue_day');
		              }
		            }else if(v.dayName =="wed"){
		              if(v.startTime != "" || v.endTime !=""){
		            	$('.wed_id').val(v.id);
		                $('.wed_start_value').val(v.startTime);
		                $('.wed_last_value').val(v.endTime);
		                $('.cross_wed').attr('class','edit_client_closeicon cross_wed');
		                $('.cross_wed').attr('data-id','true');
		                $('.menu_wed').show();
		                $('.arrow_wed').show();
		                $('.wed').html("");
		                $('.wed_row').attr('class','row wed_row');
		                $('.wed_day').attr('class','col2 wed_day');
		              }else{
		            	 $('.wed_id').val(v.id);
		                 $('.cross_wed').attr('class','edit_client_closeicon disabled cross_wed');
		                 $('.cross_wed i').attr('data-id','false');
		                 $('.menu_wed').hide();
		                 $('.menu_wed').val("");
		                 $('.arrow_wed').hide();
		                 $('.wed').html("Closed");
		                 $('.wed_row').attr('class','row disabled wed_row');
		                 $('.wed_day').attr('class','col2 disabled wed_day');
		              }
		            }else if(v.dayName =="thu"){
		              if(v.startTime != "" || v.endTime !=""){
		            	$('.thu_id').val(v.id);
		                $('.thu_start_value').val(v.startTime);
		                $('.thu_last_value').val(v.endTime);
		                $('.cross_thu').attr('class','edit_client_closeicon cross_thu');
		                $('.cross_thu').attr('data-id','true');
		                $('.menu_thu').show();
		                $('.arrow_thu').show();
		                $('.thu').html("");
		                $('.thu_row').attr('class','row thu_row');
		                $('.thu_day').attr('class','col2 thu_day');
		              }else{
		            	 $('.thu_id').val(v.id);
		                 $('.cross_thu').attr('class','edit_client_closeicon disabled cross_thu');
		                 $('.cross_thu i').attr('data-id','false');
		                 $('.menu_thu').hide();
		                 $('.menu_thu').val("");
		                 $('.arrow_thu').hide();
		                 $('.thu').html("Closed");
		                 $('.thu_row').attr('class','row disabled thu_row');
		                 $('.thu_day').attr('class','col2 disabled thu_day');
		              }
		            }else if(v.dayName =="fri"){
		              if(v.startTime != "" || v.endTime !=""){
		            	 $('.fri_id').val(v.id);
		                $('.fri_start_value').val(v.startTime);
		                $('.fri_last_value').val(v.endTime);
		                $('.cross_fri').attr('class','edit_client_closeicon cross_fri');
		                $('.cross_fri').attr('data-id','true');
		                $('.menu_fri').show();
		                $('.arrow_fri').show();
		                $('.fri').html("");
		                $('.fri_row').attr('class','row fri_row');
		                $('.fri_day').attr('class','col2 fri_day');
		              }else{
		            	 $('.fri_id').val(v.id);
		                 $('.cross_fri').attr('class','edit_client_closeicon disabled cross_fri');
		                 $('.cross_fri i').attr('data-id','false');
		                 $('.menu_fri').hide();
		                 $('.menu_fri').val("");
		                 $('.arrow_fri').hide();
		                 $('.fri').html("Closed");
		                 $('.fri_row').attr('class','row disabled fri_row');
		                 $('.fri_day').attr('class','col2 disabled fri_day');
		              }
		            }else if(v.dayName =="sat"){
		              if(v.startTime != "" || v.endTime !=""){
		            	 $('.sat_id').val(v.id);
		                $('.sat_start_value').val(v.startTime);
		                $('.sat_last_value').val(v.endTime);
		                $('.cross_sat').attr('class','edit_client_closeicon cross_sat');
		                $('.cross_sat').attr('data-id','true');
		                $('.menu_sat').show();
		                $('.arrow_sat').show();
		                $('.sat').html("");
		                $('.sat_row').attr('class','row sat_row');
		                $('.sat_day').attr('class','col2 sat_day');
		              }else{
		            	 $('.sat_id').val(v.id);
		                 $('.cross_sat').attr('class','edit_client_closeicon disabled cross_sat');
		                 $('.cross_sat i').attr('data-id','false');
		                 $('.menu_sat').hide();
		                 $('.menu_sat').val("");
		                 $('.arrow_sat').hide();
		                 $('.sat').html("Closed");
		                 $('.sat_row').attr('class','row disabled sat_row');
		                 $('.sat_day').attr('class','col2 disabled sat_day');
		              }
		            }else if(v.dayName =="sun"){
		              if(v.startTime != "" || v.endTime !=""){
		            	$('.sun_id').val(v.id);
		                $('.sun_start_value').val(v.startTime);
		                $('.sun_last_value').val(v.endTime);
		                $('.cross_sun').attr('class','edit_client_closeicon cross_sun');
		                $('.cross_sun').attr('data-id','true');
		                $('.menu_sun').show();
		                $('.arrow_sun').show();
		                $('.sun').html("");
		                $('.sun_row').attr('class','row sun_row');
		                $('.sun_day').attr('class','col2 sun_day');
		              }else{
		            	 $('.sun_id').val(v.id);
		                 $('.cross_sun').attr('class','edit_client_closeicon disabled cross_sun');
		                 $('.cross_sun i').attr('data-id','false');
		                 $('.menu_sun').hide();
		                 $('.menu_sun').val("");
		                 $('.arrow_sun').hide();
		                 $('.sun').html("Closed");
		                 $('.sun_row').attr('class','row disabled sun_row');
		                 $('.sun_day').attr('class','col2 disabled sun_day');
		              }
		            }
		          });
		        }
			}
		})
	});


});

$(window).load(function (){
	var url=window.location.pathname.split('/phonemyfood');
	var child=$('.sidebar ul').children();
	for(var i=0;i<child.length;i++){
		var link = ($(child[i]).find("a").attr("href").split('/phonemyfood'));
		if(JSON.stringify(url) === JSON.stringify(link)){
			$(child[i]).attr("class","selected");
		}
	}
});		

function resetPassword(userId){
	toastr.success("Please wait....");
	$("#reset_password_button").attr("disabled", "disabled");
	$.ajax({
		type : "post",
		url : contextURL + "admin/resendPassword",
		data : { id : userId },
		success : function(data) {
			toastr.success("Password Reset email sent.");
			$("#reset_password_button").removeAttr("disabled");
		}
	});
}