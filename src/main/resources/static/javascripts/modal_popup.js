// Set timeout variables.
var timoutWarning = 900000; // Display warning in 1Mins.
var timoutNow = 960000; // Timeout in 2 mins.
var logoutUrl = 'http://localhost:3000/users/sign_out'; // URL to logout page.

var warningTimer;
var timeoutTimer;

// Start timers.
function startTimers() {
  if($('.active_popup_window').val() == "true"){
    warningTimer = setTimeout("idleWarning()", timoutWarning);
    timeoutTimer = setTimeout("idleTimeout()", timoutNow);
  }
}


// Reset timers.
function resetTimers() {
  if(($('.active_popup_window').val() == "true") && $('.timeout_popup').val() =="true"){
    clearTimeout(warningTimer);
    clearTimeout(timeoutTimer);
    startTimers();
    $("#timeout").modal('hide');
  }
}

// Show idle timeout warning dialog.
function idleWarning() {
  $("#timeout").modal();
  $('.timeout_popup').val("false");
}

//continue login
$(document).on('click','.continue_loging',function(){
  $('.timeout_popup').val("true");
  resetTimers();
})
// Logout the user.
function idleTimeout() {
  window.location = logoutUrl;
}