$(document).ready(function() {
  $(document).on('click','.select_sales_people',function(){
    $('.select_sales_people').removeClass("item-selected");
    $(this).addClass("item-selected");
    $('.sales_people_id').val($(this).attr('id'));
    $.ajax({
      type:'get',
      url:'/sales_people/get_sales_people',
      data: { 
        'id': $(this).attr('id')
      },
      success: function(data){
        if(data.user.name != ""){
        $('.first_name').val(data.user.name.split(' ')[0]);
          $('.last_name').val(data.user.name.split(' ')[1]);
        }
        if(data.user.email != ""){
          $('.client_email').val(data.user.email);
        }
        
        if(data.user != null){
          $('.sales_people_id').val(data.user.id)
          if (data.user.is_active == true){
            $('input[name=active_user]').attr('checked', false);
            $('input[name=active_user]').attr('checked', true).click();
          }else{
            $('.make_active').attr('data-id','true');
            $('.make_active').attr('value','false');
            $('input[name=active_user]').attr('checked', false);
          }
        }else{
          $('.first_name').val("");
          $('.last_name').val("");
          $('.client_email').val("").attr('readonly','false');
        }
        if(data.address != null){
          $('.client_address').val(data.address.address);
          $('.client_phone').val(data.address.phone_number);
          $('.client_city').val(data.address.city);
          $('.client_state').val(data.address.state);
          $('.client_zip').val(data.address.zip);
        }else{
          $('.client_address').val("");
          $('.client_phone').val("");
          $('.client_city').val("");
          $('.client_state').val("");
          $('.client_zip').val("");
        }
      }
    });
  }); 

  // make active
  $(document).on('click','.make_sales_user_active',function(){
    if($(this).find('input').attr('data-id') == "true"){
      $(this).find('input').attr('value','true');
      $(this).find('input').attr('data-id','false');
    }else{
      $(this).find('input').attr('value','false'); 
      $(this).find('input').attr('data-id','true');
    }
  });

  //search sales people 
  $(document).on('keyup','.search_sales_people', function() {
    $.ajax({
      type:'get',
      url:'/sales_people/search_sales_people',
      data: { 
        key: $('.search_sales_people').val()
      },
      success: function(data){
        $('.sales_people-list ul').empty();
        if(data.length != 0){
          $.each(data, function(k, v) {
            $('.sales_people-list ul').append($("<li>").text(v.name).addClass('select_sales_people').attr('id',v.id));
          });
        }
      }
    });
  });

});