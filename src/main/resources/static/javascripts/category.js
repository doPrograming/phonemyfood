$(document).ready(function(){
  $(document).on('click','.cat_select',function(){
    $('.cat_select').removeClass("selected-ingrediends");
    $(this).addClass("selected-ingrediends");
    $('.category_name').val($(this).context.firstElementChild.innerText);
    $('.category_id').val($(this).attr('id'))
    $('.category_name').focus();
  });

  $(document).on('click','.add_category',function(){
    var confirmed = true
    $('.category_name').attr('disabled','disabled');
    $('span').each(function(){
      if($(this).text() == $('.category_name').val()){
        confirmed = confirm("Category Name Already Exists, Add ( Y / N)");
      }
    });
    if(confirmed){
      $.ajax({
        type:'post',
        url:'/categories',
        data: { 
          'c_name': $('.category_name').val(),
          'c_id': $('.category_id').val()
        },
        success:function(){
          //I assume you want to do something on controller action execution success?
          $(this).addClass('done');
        }
      });
    }
  });

  $(document).on('click','.category-button',function(){
    $('.category_name').attr('disabled','disabled');
    $('.cat_select').off('click');
    $.ajax({
      type:'patch',
      url:'/categories',
      data: { 
        'c_name': $('.category_name').val(),
        'c_id': $('.category_id').val(),
        'form-close' : $('input[name="form-close"]').val()
      },
      success:function(){
        //I assume you want to do something on controller action execution success?
        $(this).addClass('done');
      }
    });
  });

  $(document).on('click','.delete_category',function(){
    if($('.category_id').val() != ""){
      if(confirm("Are you sure to delete this category.")){
        $('.category_name').attr('disabled','disabled');
        $('.cat_select').off('click');
        $.ajax({
          type:'delete',
          url:'/categories',
          data: { 
            'c_id' : $('.category_id').val() 
          },
          success:function(){
            //I assume you want to do something on controller action execution success?
            $(this).addClass('done');
          }
        });
      }else{
        $('.cat_select').removeClass("selected-ingrediends");
        $('.category_name').val("");
        $('.category_id').val("");
      }
    }else{
      confirm("Please select Category for delete.");
    } 
  });

  //category message
  $(document).on('click','.category_msg',function(){
    $.ajax({
      type:'get',
      url:'/categories/get_category_message',
      data: { 
        category_id: $(this).parent().parent().attr('id')
      },
      success:function(data){
        if(data != null){
          $('.category_message').val(data.message);
          $('.name').html(data.name);
          $('#category_message').modal();
        }
      }
    });
    
  });

  //category message
  $(document).on('click','#add_category_msg',function(e){
    if($('.category_message').val() != ""){
      $.ajax({
        type:'post',
        url:'/categories/add_category_message',
        data: { 
          category_id: $('.category_id').val(),
          category_message: $('.category_message').val()
        },
        success:function(data){
          toastr.success(data[0]);
          $('#category_message').modal('hide');
          $('.selected-ingrediends a').attr('class','msg');
        }
      });
    }else{
      $('#category_message').modal('hide');
    }
  });
  var new_value = false;
  var all_inputs = $('.categry-sec :input')
  $(document).on('change', all_inputs, function(e) {
    new_value = true
  });
  $(document).on('focusout', all_inputs, function(e) {
    if (new_value){
      new_value = false;
      if($(".category_name").val()!=""){
        $(".add_category").trigger('click');
        $(".category-button").trigger('click');
      }else{        
        $(".category-button").trigger('click');
      }
      e.preventDefault(); 
    }
    e.preventDefault(); 
  });
});