$(function(){
  if(($('.custom_radio').is(':checked') == true)){
    $('.custom_value').val($('#special_discount_value').val());
  }
});

//modify
$(function(){
  if(($('.modify_custom_radio').is(':checked') == true)){
    $('.custom_modify_value').val($('#modify_discount_value').val());
  }
});
$(document).ready(function(){

  //make active
  $(document).on('click','.make_checkeddata',function(){
	  var a = $(this).attr('data-idnum');
    if(a == 'true'){
      $(this).val("true");
      $(this).attr('data-idnum','false');
    }else{
      $(this).val("false");
      $(this).attr('data-idnum','true');
    }
  });

  //make active
  $(document).on('click','.modify_checked',function(){
	  var a = $(this).attr('data-idnum');
	if(a == "true"){
      $(this).val("true");
      $(this).attr('data-idnum','false');
    }else{
      $(this).val("false");
      $(this).attr('data-idnum','true')
    }
  });
  //select special category
  $(document).on('click','.special_category',function(){
    if($('.special_menu').val() != null){
      var ids = [];
      if($(this).attr('class') == "special_category"){
        $(this).attr('class','special_category specials-selected');
      }else{
        $(this).attr('class','special_category');
      }
      $('.special_cat_ids li.specials-selected').each(function(){ ids.push(this.id); });
      $('.selected_category_id').val(ids);
    }else{
      toastr.warning("Please select menu first");
    }
  });

  //select modify category
  $(document).on('click','.modify_category',function(){
    if($('.modify_menu').val() != null){
      var ids = [];
      if($(this).attr('class') == "modify_category"){
        $(this).attr('class','modify_category specials-selected');
      }else{
        $(this).attr('class','modify_category');
      }
      $('.modify_cat_ids li.specials-selected').each(function(){ ids.push(this.id); });
      $('.modify_category_id').val(ids);
    }else{
      toastr.warning("Please select menu first");
    }
  });

  //select special type
  $(document).on('change','.special_radio',function(){
    if($('.custom_radio').is(':checked') == true){
      $('#special_discount_value').prop('disabled', false);
      $('#special_items').prop('disabled', false);
      $('.persent').prop('disabled', 'disabled');
      $('.dollar').prop('disabled', 'disabled');
      $('.dollar').val("");
      $('.persent').val("")
    }else{
      $('#special_discount_value').prop('disabled', 'disabled');
    }
    if($('.persent_radio').is(':checked') == true){
      $('.dollar').val("");
      $('.persent').attr('disabled', false);
      $('.dollar').prop('disabled', 'disabled');
    }
    if($('.dollar_radio').is(':checked') == true){
      $('.persent').val("");
      $('.dollar').attr('disabled', false);
      $('.persent').prop('disabled', 'disabled');
    }
  });

  //modify special type
  $(document).on('change','.modify_radio',function(){
    if($('.modify_custom_radio').is(':checked') == true){
      $('#modify_discount_value').prop('disabled', false);
    $('#modify_special_items').prop('disabled', false);
      $('.modify_persent').prop('disabled', 'disabled');
      $('.modify_dollar').prop('disabled', 'disabled');
    }else{
      $('#modify_discount_value').prop('disabled', 'disabled');
    }
    if($('.modify_persent_radio').is(':checked') == true){
      $('.modify_persent').attr('disabled', false);
      $('.modify_dollar').prop('disabled', 'disabled');
    }
    if($('.modify_dollar_radio').is(':checked') == true){
      $('.modify_dollar').attr('disabled', false);
      $('.modify_persent').prop('disabled', 'disabled');
    }
  });

  // menu builder form
  $(document).on('change','.special_menu', function() {
    var ids = [];
    $('.special_menu_selected').val($('.special_menu').val());
  })
  
  // menu builder form
  $(document).on('change','.modify_menu', function() {
    var ids = [];
    $('.modify_menu_selected').val($('.modify_menu').val());
  })
  //modify menu page
  $(document).on('change','.modify_special_page',function(){
    $('.modify_id').val($('.modify_special_page').find("option:selected").attr('id'));
    $.ajax({
      type:'get',
      url:'./specials/get_special_record',
      data: { 
        'special_id': $('.modify_special_page').find("option:selected").attr('id') 
      },
      success:function(data){
        if(data != null){
        	if(data.specials.item != ""){
        		$('#modify_special_items option').each(function(){
                if(data.specials.item == this.value){
                  $(this).attr('selected', 'selected');
                }
              });
        	}else{
        		$(this).text('Select Item');
        	}
        }
          if(data.specials.discountType != null){
            if(data.specials.discountType =="custom"){
              $('.modify_dollar').val("");
              $('.modify_persent').val("");
              $('.modify_custom_radio').click(); 
              $('#modify_discount_value option').each(function(){
                if(data.specials.discountValue == this.text){
                  $(this).attr('selected', 'selected');
                  $('.custom_modify_value').val(this.text);
                }
              });
            }else if(data.specials.discountType =="dollar"){
              $('#modify_discount_value').val("Buy 1 Get 1 free!");
              $('#modify_discount_value').prop('disabled', 'disabled');
              $('.modify_dollar').attr('disabled', false);
              $('.modify_persent').prop('disabled', 'disabled');
              $('.modify_persent_radio').attr('checked',false);
              $('.modify_custom_radio').attr('checked',false);
              $('.modify_dollar_radio').click();
              $('.modify_persent').val("");
              $('.modify_dollar').val(data.specials.discountValue);
            }else if(data.specials.discountType == "percent"){
              $('#modify_discount_value').val("Buy 1 Get 1 free!");
              $('#modify_discount_value').prop('disabled', 'disabled');
              $('.modify_dollar').attr('disabled', 'disabled');
              $('.modify_persent').prop('disabled', false);
              $('.modify_dollar_radio').attr('checked',false);
              $('.modify_custom_radio').attr('checked',false);
              $('.modify_persent_radio').click();
              $('.modify_persent').val(data.specials.discountValue);
              $('.modify_dollar').val("");
            }
          }else{
            $('#modify_discount_value').val("");
          }
          /*var e = [];*/
          var f = [];
          var ids = [];
          if(data.specials.userMenu != ""){
            $('.search-field').hide();
            $.each(data.specials.userMenu.split(','), function(k, v) {
            	f.push(v);
              $(".modify_chosen-select").val(f).trigger("chosen:updated");
              //$(".modify_menu_selected").val(f.join(","));
            });
            $(".modify_menu_selected").val(f.join(","));
          } 
          $('.modify_min_buy').val("");
          if(data.specials.minBuy != ""){
            $('.modify_min_buy').val(data.specials.minBuy);
          }
          $('.modify_start_date').val("");
          $('.modify_end_date').val("");
          if(data.specials.startDate != ""){
            $('.modify_start_date').val(data.specials.startDate);
          }
          if(data.specials.endDate != ""){
            $('.modify_end_date').val(data.specials.endDate);
          }
          $('.modify_persent').val();
          if(data.specials.selectedCategory != ""){
            
            $('.modify_cat_ids li').attr('class','modify_category');
            $('.modify_cat_ids li').each(function(){
              if((data.specials.selectedCategory.split(',')).includes(this.id)){
                $(this).attr('class','modify_category specials-selected');
                ids.push(this.id); 
                
              }
            })
            $('.modify_category_id').val(ids);
          }else{
            
            $('.modify_cat_ids li').each(function(){
              $(this).attr('class','modify_category');
              ids.push(this.id); 
            });
            $('.modify_category_id').val(ids);
          }
          $("#specId").val(data.specials.id);
          $("#specTitle").val(data.specials.title);
          if (data.specials.active == true){
            $('input[name=active]').prop('checked', true);
            $('.modify_checked').attr('data-idnum','false');
            $('.modify_checked').attr('value','true');
           /* $('input[name=active]').click();*/
          }else{
        	$('input[name=active]').prop('checked', false);
            $('.modify_checked').attr('data-idnum','true');
            $('.modify_checked').attr('value','false');
            /*$('input[name=active]').click();*/
          }
          $('.modify_description').val(data.specials.descriptionText);
          //I assume you want to do something on controller action execution success?
          $(this).addClass('done');
        }
    	
    });
  });
  // hide item
  /*$(document).on('click','.hide_special',function(){
    if($(this).find('input').attr('data-idnum') == "true"){
      $(this).find('input').attr('value','true');
      $(this).find('input').attr('data-idnum','false');
    }else{
      $(this).find('input').attr('value','false');
      $(this).find('input').attr('data-idnum','true');
    }
    
  });*/

  $(document).on('click','.search-choice-close',function(){
    $(this).parent().hide();
  })
  //special type
  $(document).on('change','#special_discount_value', function() {
    if($('.custom_radio').is(':checked') == true){
      $('.custom_value').val($('#special_discount_value').val());
    }
  })

  //modify type
  $(document).on('change','#modify_discount_value', function() {
    if($('.custom_modify_radio').is(':checked') == true){
      $('.custom_modify_value').val($('#modify_discount_value').val());
    }
  })

  //upload special image
  $(document).on('click','.special_image',function(){
    toastr.info("Please create special item first.");
  });

});
