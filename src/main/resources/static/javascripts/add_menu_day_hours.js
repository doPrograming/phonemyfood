$(document).ready(function(){
  $(document).on('click','.menu_image',function(){
	  var dataDay = $(this).attr('day_type');
	  var allDay = $("div.all_row").find("i").attr('data-id');
    if ($(this).attr('data-id') == "true" ){
      if (dataDay == "mon"){
    	$('.cross_mon').attr('class','menu_closeicon disabled cross_mon');
        $('.menu_mon').hide();
        $('.menu_mon').val("");
        $('.arrow_mon').hide();
        $('.mon').html("Closed");
        $('.mon_row').attr('class','row disabled mon_row');
        $('.mon_day').attr('class','col2 disabled mon_day');
      }else if (dataDay == "tue"){
        $('.cross_tue').attr('class','menu_closeicon disabled cross_tue');
        $('.menu_tue').hide();
        $('.menu_tue').val("");
        $('.arrow_tue').hide();
        $('.menu_tue').removeAttr('required');
        $('.tue').html("Closed");
        $('.tue_row').attr('class','row disabled tue_row');
        $('.tue_day').attr('class','col2 disabled tue_day');
      }else if (dataDay == "wed"){
        $('.cross_wed').attr('class','menu_closeicon disabled cross_wed');
        $('.menu_wed').hide();
        $('.menu_wed').val("");
        $('.arrow_wed').hide();
        $('.menu_wed').removeAttr('required');
        $('.wed').html("Closed");
        $('.wed_row').attr('class','row disabled wed_row');
        $('.wed_day').attr('class','col2 disabled wed_day');
      }else if (dataDay == "thu"){
        $('.cross_thu').attr('class','menu_closeicon disabled cross_thu');
        $('.menu_thu').hide();
        $('.menu_thu').val("");
        $('.arrow_thu').hide();
        $('.menu_thu').removeAttr('required');
        $('.thu').html("Closed");
        $('.thu_row').attr('class','row disabled thu_row');
        $('.thu_day').attr('class','col2 disabled thu_day');
      }else if (dataDay == "fri"){
        $('.cross_fri').attr('class','menu_closeicon disabled cross_fri');
        $('.menu_fri').hide();
        $('.menu_fri').val("");
        $('.arrow_fri').hide();
        $('.menu_fri').removeAttr('required');
        $('.fri').html("Closed");
        $('.fri_row').attr('class','row disabled fri_row');
        $('.fri_day').attr('class','col2 disabled fri_day');
      }else if (dataDay == "sat"){
        $('.cross_sat').attr('class','menu_closeicon disabled cross_sat');
        $('.menu_sat').hide();
        $('.menu_sat').val("");
        $('.arrow_sat').hide();
        $('.menu_sat').removeAttr('required');
        $('.sat').html("Closed");
        $('.sat_row').attr('class','row disabled sat_row');
        $('.sat_day').attr('class','col2 disabled sat_day');
      } else if (dataDay == "sun"){
        $('.cross_sun').attr('class','menu_closeicon disabled cross_sun');
        $('.menu_sun').hide();
        $('.menu_sun').val("");
        $('.arrow_sun').hide();
        $('.menu_sun').removeAttr('required');
        $('.sun').html("Closed");
        $('.sun_row').attr('class','row disabled sun_row');
        $('.sun_day').attr('class','col2 disabled sun_day');
      } else if (dataDay == "all"){
    	$('.cross_all').attr('class','menu_closeicon disabled cross_all');
    	$('.menu_all').hide();
    	$('.menu_all').val("");
    	$('.arrow_all').hide();
    	$('.menu_all').removeAttr('required');
    	$('.all').html("Closed");
    	$('.all_row').attr('class','row disabled all_row');
    	$('.all_day').attr('class','col2 disabled all_day');
      }
      $(this).attr("data-id", "false");
    }else{
      if (dataDay == "mon" && allDay == "false"){
        $('.cross_mon').attr('class','menu_closeicon cross_mon');
        $('.menu_mon').show();
        $('.arrow_mon').show();
        $('.menu_mon').attr('required','required');
        $('.mon').html("");
        $('.mon_row').attr('class','row mon_row');
        $('.mon_day').attr('class','col2 mon_day');
      }else if (dataDay == "tue" && allDay == "false"){
        $('.cross_tue').attr('class','menu_closeicon cross_tue');
        $('.menu_tue').show();
        $('.arrow_tue').show();
        $('.menu_tue').attr('required','required');
        $('.tue').html("");
        $('.tue_row').attr('class','row tue_row');
        $('.tue_day').attr('class','col2 tue_day');
      }else if (dataDay == "wed" && allDay == "false"){
        $('.cross_wed').attr('class','menu_closeicon cross_wed');
        $('.menu_wed').show();
        $('.arrow_wed').show();
        $('.menu_wed').attr('required','required');
        $('.wed').html("");
        $('.wed_row').attr('class','row wed_row');
        $('.wed_day').attr('class','col2 wed_day');
      }else if (dataDay == "thu" && allDay == "false"){
        $('.cross_thu').attr('class','menu_closeicon cross_thu');
        $('.menu_thu').show();
        $('.arrow_thu').show();
        $('.menu_thu').attr('required','required');
        $('.thu').html("");
        $('.thu_row').attr('class','row thu_row');
        $('.thu_day').attr('class','col2 thu_day');
      }else if (dataDay == "fri" && allDay == "false"){
        $('.cross_fri').attr('class','menu_closeicon cross_fri');
        $('.menu_fri').show();
        $('.arrow_fri').show();
        $('.menu_fri').attr('required','required');
        $('.fri').html("");
        $('.fri_row').attr('class','row fri_row');
        $('.fri_day').attr('class','col2 fri_day');
      }else if (dataDay == "sat" && allDay == "false"){
        $('.cross_sat').attr('class','menu_closeicon cross_sat');
        $('.menu_sat').show();
        $('.arrow_sat').show();
        $('.menu_sat').attr('required','required');
        $('.sat').html("");
        $('.sat_row').attr('class','row sat_row');
        $('.sat_day').attr('class','col2 sat_day');
      }else if (dataDay == "sun" && allDay == "false"){
        $('.cross_sun').attr('class','menu_closeicon cross_sun');
        $('.menu_sun').show();
        $('.arrow_sun').show();
        $('.menu_sun').attr('required','required');
        $('.sun').html("");
        $('.sun_row').attr('class','row sun_row');
        $('.sun_day').attr('class','col2 sun_day');
      }else if (dataDay == "all"){
        $('.cross_all').attr('class','menu_closeicon cross_all');
        $('.menu_all').show();
        $('.arrow_all').show();
        $('.menu_all').attr('required','required');
        $('.all').html("");
        $('.all_row').attr('class','row all_row');
        $('.all_day').attr('class','col2 all_day');
        if($("div.mon_row").find("i").attr('data-id') == "true"){
        	$("div.mon_row").find("i").click();	
        }
        if($("div.tue_row").find("i").attr('data-id') == "true"){
        	$("div.tue_row").find("i").click();	
        }
        if($("div.wed_row").find("i").attr('data-id') == "true"){
        	$("div.wed_row").find("i").click();	
        }
        if($("div.thu_row").find("i").attr('data-id') == "true"){
        	$("div.thu_row").find("i").click();	
        }
        if($("div.fri_row").find("i").attr('data-id') == "true"){
        	$("div.fri_row").find("i").click();	
        }
        if($("div.sat_row").find("i").attr('data-id') == "true"){
        	$("div.sat_row").find("i").click();	
        }
        if($("div.sun_row").find("i").attr('data-id') == "true"){
        	$("div.sun_row").find("i").click();	
        }
      }  
      $(this).attr("data-id", "true");
    }
    
  });
  $(document).on('click','.close_window',function(){
    $('.add-menu').hide();
  });
  //show loader
  // $('#add_menu_button').click(function(){
  //   $('.loader').attr('class','show_loader');
  // });

  //hide add menu page 

});