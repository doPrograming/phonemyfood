$(document).ready(function(){
  $(document).on('click','.ingredient_select',function(){
    $('.ingredient_select').removeClass("selected-ingrediends");
    $(this).addClass("selected-ingrediends");
    $('.ingredient_name').val($(this).context.firstElementChild.innerText);
    $('.ingredient_price').val($(this).context.lastElementChild.innerHTML);
    $('.ingredient_id').val($(this).attr('id'))
    $('.ingredient_name').focus();
    $(".delete_ingredient").css("background", "linear-gradient(to bottom, #ce5051 50%, #860404 50%)");
    $(".plus").find("i").attr("class","fa fa-pencil");
  });
  $(document).on('blur', ".ingredient_price", function(){
    value=$(this).val();
    if (value!=""){
      new_value=parseFloat(value).toFixed(2)
      $(this).val(new_value);
    }
  });
  var new_value = false;
  var all_inputs = $('.ingredients-sec :input')
  $(document).on('change', all_inputs, function(e) {
    new_value = true
  });
  $(document).on('focusout', all_inputs, function(e) {
    if (new_value){
      new_value = false;
      e.preventDefault(); 
      $(".ingrediends-button").trigger('click');
    }
    e.preventDefault(); 
  });
  $(document).on('click','.add_ingredient',function(){
    //$('.ingredient_name, .ingredient_price').attr('disabled','disabled');
    $('.ingredient_select').off('click');
    if($('.ingredient_name').val() != "" && $('.ingredient_price').val() != "NaN"){
      $.ajax({
        type:'post',
        url:'/add_ingredient',
        data: { 
          'i_name': $('.ingredient_name').val(), 
          'i_price': $('.ingredient_price').val() 
        },
        success:function(){
          //I assume you want to do something on controller action execution success?
          $(this).addClass('done');
        }
      });
    }else if($('.ingredient_price').val() == "NaN"){
      $('.ingredient_price').val("");
      toastr.warning("Please enter valid price.");
    }else{
      toastr.warning("Please add Ingredient name first.");
    }
  });

  $(document).on('click','.save_ingrediends-button',function(){
    if(($('.ingredient_price').val() != "") && ($('.ingredient_name').val() != "")){
      $.ajax({
        type:'post',
        url:'/phonemyfood/user/ingredients/add_ingredients',
        data: { 
          'i_name': $('.ingredient_name').val(), 
          'i_price': $('.ingredient_price').val(),
          'i_id' : $('.ingredient_id').val(),
          'form-close' : $('input[name="form-close"]').val()
        },
        success:function(i){
          //I assume you want to do something on controller action execution success?
        	toastr.success("Ingrediend saved");
        	$(this).addClass("done"), $("#ingredientData").empty(),$("#ingredientData").html(i);
        }
      });
    }else if($('.ingredient_name').val() == ""){
      toastr.warning("Please add Ingredient name first.");
    }
  });


  $(".delete_ingredient").on('click',function() {
  	if('0'!=$(".ingredient_id").val())	{
  		$.ajax({
  	        type: "get",	
  	        url: "/phonemyfood/user/ingredients/ingredientDeleteModal",
  	        data:{
  	        "ingredient_id" : $(".ingredient_id").val()
  	        }
  		})
  		.done(function(data){
  			$("#deleteConfirm").html(data);
  			$("#deleteConfirm").modal();
  		});
  	}
  	else{
  		toastr.warning("Please select Category for delete.")
  	}
  }),  
  
  $(document).on('click','#delete_ingredient_yes',function() {	
        $('.ingredient_name, .ingredient_price').attr('disabled','disabled');
        $('.ingredient_select').off('click');
        $.ajax({
          type:'post',
          url:'/phonemyfood/user/ingredients/delete_ingredient',
          data: { 
            'i_id' : $('.ingredient_id').val() 
          },
          success:function(i){
            //I assume you want to do something on controller action execution success?
        	  $(this).addClass("done"), $("#ingredientData").empty();
        	  $("#ingredientData").html(i);
        	  $('.ingredient_id').val("0");
              $('.ingredient_name').val("");
              $('.ingredient_name').removeAttr("disabled");
              $('.ingredient_price').val("");
              $('.ingredient_price').removeAttr("disabled");
        	  toastr.success("Ingrediend deleted");
          }
        });

  });

	$(document).on('keyup','.search_ingredient',function(){
		$.ajax({
			type:"get",
			url:"/phonemyfood/user/searchIngredient",
			data:{key:$('.search_ingredient').val()}
		}).done(function(data){
			$("#ingredientData ul").empty();
			if(data.length!=0){
				$.each(data,function(k,v){
					$("#ingredientData ul").append("<li class=ingredient_select id="+v.id+" value=true><span>"+v.name+"</span><span>"+v.price+"</span>");
				})
			}else{
				$("#ingredientData ul").append($("<li>").text("No record found"));
			}
		});
	});  
});