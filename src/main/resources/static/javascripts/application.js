// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require timepicki
//= require datatable
//= require chosen.jquery.min
//= require toastr_rails
//= require jquery.remotipart
//= require binaryajax.js
//= require canvasResize.js
//= require exif.js
//= require dropzone
//= require client_new.js
//= require star-rating.js
//= require datepicker.js
//= require category.js
//= require item_day_hour.js
//= require menu_new.js
//= require menu_edit.js
//= require user_edit_menu.js
//= require special_new.js
//= require user_about.js
//= require css_browser_selector.js
//= require user_ingrident.js
//= require user_reward.js
//= require sub_admin_new.js
//= require sales_people.js
//= require edit_client_day_hours.js
//= require about_day_and_hours.js
//= require item_new.js
//= require bootstrap.js
//= require modal_popup.js
//= require_self


$(document).ready(function(){
// code for vertical tab
 
  // make active tab
  $('.make_active').click(function(){
    $('.make_active').removeClass('active');
    $('.active_add_edit').removeClass('active');
    $(this).addClass('active');
  });

  $('.add_menu').click(function(){
    $('.active_add_edit').removeClass('active');
    $('.make_active').removeClass('active');
  });

  $('.addeditclient').click(function(){
    $('.sales_people_active').removeClass('active');
    $('.sub_admin_active').removeClass('active');
    $('.active_add_edit').addClass('active');
  });

  $('.addeditsales').click(function(){
    $('.active_add_edit').removeClass('active');
    $('.sub_admin_active').removeClass('active');
    $('.sales_people_active').addClass('active');
  });

  $('.edit_menu').on('click',function(){
    $('.make_active').removeClass('active');
  })
  //hide login form
  $('.fa-close').click(function(){
    $('.loginbox').hide();
  });

  $(document).on('click', '.close_menu',function(e){ 
    var super_parent = $(this).parent().parent().parent()
    $('<input>').attr({
    type: 'hidden',
    name: 'form-close',
    value: 'true'
     }).appendTo('form:visible, .main-class');
    super_parent.find('.save-btn:visible').trigger('click');
    e.preventDefault()
    $('.main-class').html('');
  });


  $('.addeditadmin').click(function(){
    $('.sales_people_active').removeClass('active');
    $('.active_add_edit').removeClass('active');
    $('.sub_admin_active').addClass('active');
  })
  
  var new_value = false;
  var all_inputs = $('#delivery_form :input')
  $(document).on('change', all_inputs, function(e) {
    new_value = true
  });
  $(document).on('focusout', all_inputs, function(e) {
    if (new_value){
      new_value = false;
      e.preventDefault(); 
      $("#delivery_form").trigger('submit');
    }
    e.preventDefault(); 
  });
});