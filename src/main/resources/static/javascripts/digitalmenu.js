var activeTab = ".tab-pane.fade.active.in";
var inActiveTab = ".tab-pane.fade";
var linkedTabs = "";
var nextscreen = null;

function addScreen(tabs){
	$(".add_screen").attr("disabled",true);
	if(nextscreen == null)
		nextscreen = tabs;
	nextscreen = nextscreen+1;
	if(nextscreen>6)
		toastr.error("Maximum 6 screens allowed");
	else{
		$("#tabElements li").each(function(index){
			if($(this).hasClass("active"))
				$(this).removeClass("active");
			if(!$(this).hasClass("active"))
				$(this).find("img").attr("src","/phonemyfood/assets/screen-off.png");
		});
		$.ajax({
			type:"get",
			url:contextURL+"user/digitalMenu/addScreen?screenNumber="+nextscreen
		}).done(function(data){
			$("#digitalmenu-tabcontent").find("div.active.in").removeClass("active in");
			$("ul#tabElements").append("<li role='presentation' class='active'><a href=#tab"+nextscreen+" aria-controls='home' role='tab' data-toggle='tab' onclick='onOffImage(this)'><span>"+nextscreen+"</span> <img alt='' src='/phonemyfood/assets/screen-on.png'></a></li>");
			$("div#digitalmenu-tabcontent").append("<div id=tab"+nextscreen+" class='tab-pane fade active in'><div id=digitalMenuTabPage"+nextscreen+ "></div></div>");
			$('#digitalMenuTabPage'+nextscreen).html(data);
			$(".add_screen").attr("disabled",false);
			
			initializeSelectpicker();
			layoutClickBgChange();
			orientationClickBgChange();
		})
	}
}

function onOffImage(aHref){
	$("#tabElements li").each(function(index){
		if($(this).hasClass("active"))
			$(this).find("img").attr("src","/phonemyfood/assets/screen-off.png");
	})
 	if(!$(aHref).parent().hasClass("active") || $(aHref).parent().hasClass("active"))
		$(aHref).find("img").attr("src","/phonemyfood/assets/screen-on.png");
}

function saveDigitalmenu(buttonelement){
	var linkedDigitalMenu = $(activeTab+" .linkedDigitalMenu").val(); 
	if(linkedDigitalMenu != ''){
		$.ajax({
			type : "POST",
			url: contextURL + "user/digitalMenu/saveLinkedDigitalMenu",
			data: {
				'digitalMenuId' : $(activeTab+" .digitalMenuId").val(),
				'layout' : $(activeTab+" input[name=layout]:checked").val() 
			}
		}).done(function(data){
			toastr.success("DigitalMenu "+$(activeTab+" .screennumber").val()+" successfully saved");
		});
	} else{
		var form = buttonelement.closest("form")
		var fields = validateForm(form);
		if(fields){
			fields = fields.replace(/,\s*$/, "");
			toastr.error("Please Add "+fields)
			return;
		} else{
			var menus = [];
			$(activeTab+" .digitalmenu_menus ul.dropdown-menu").find("li.selected").each(function(){
				menus.push($('.digitalmenu_menus option').eq($(this).attr("data-original-index")).val());
			})
			var digitalmenu = $(form).serialize()+"&menus="+menus;
			$.ajax({
				type : "POST",
				url: contextURL + "user/digitalMenu/saveDigitalMenu",
				data: digitalmenu
			}).done(function(data){
				$('#digitalMenuTabPage'+form.elements[1].value).html(data);
				$(inActiveTab+".linkedWith"+form.elements[0].value).each(function(){
					updateLinkedMenus($(this).find("form .digitalMenuId").val(),$(this).find("input[name=screenNumber]").val(),form);
				});
				$("#tabElements .thisDigitalMenuId").each(function(index){
		 			var thisTabId = ($(this).attr("class").split(" "))[0];
		 			if($(form).find("input[name=id]").val()==thisTabId){
	 					$("#"+$(activeTab).attr('id')+" :input:not([name=layout],[name=id],[name=save-button],[name=delete-button])").attr("disabled", true);
		 			}
				});
				toastr.success("DigitalMenu "+$(activeTab+" .screennumber").val()+" successfully saved");
				initializeSelectpicker();
				selectedMenusOfDigitalMenu();
				layoutOrientationBgChange();
				layoutClickBgChange();
				orientationClickBgChange();
			}).error(function(error){
				toastr.error(error)
			});
		}
	}
}

function updateLinkedMenus(id, screennumber, form){
	$.ajax({
		type : "get",
		url: contextURL + "user/digitalMenu/getUpdatedLinkedMenus",
		data: {'id' : id},
		success : function(data){
			$("#digitalMenuTabPage"+screennumber).html(data);
			$("#tab"+screennumber+" :input:not([name=layout],[name=id],[name=save-button],[name=delete-button])").attr("disabled", true);
			
			initializeSelectpicker();
			selectedMenusOfDigitalMenu();
			layoutOrientationBgChange();
			layoutClickBgChange();
			orientationClickBgChange();
		}
	})
}
function validateForm(form){
	var menus = [];
	$(activeTab+" .digitalmenu_menus ul.dropdown-menu").find("li.selected").each(function(){
		menus.push($('.digitalmenu_menus option').eq($(this).attr("data-original-index")).val());
	})
	var str = "";
	if ($(form).find("[name='menuType']").val() == "")
		str = str + " 'Menu Type',";
	if ($(form).find("[name='menuDesign']").val() == "")
		str = str + " 'Menu Design',"
	if(menus.length==0)
		str = str + " 'User Menu',";
	if ($(form).find("[name='userMenu']").val() == "")
		str = str + " 'Menu',";
	if ($(form).find("[name='orientation']:checked").val() == undefined)
		str = str + " 'Orientation',";
	if ($(form).find("[name='layout']:checked").val() == undefined)
		str = str + " 'Layout',";
	if ($(form).find("[name='colorPicker']:checked").val() == undefined)
		str = str + " 'ColorPicker',";
	return str;
}

function openDigitalMenuPhotoModal(){
	var digitalMenuId = $(activeTab).find(".digitalMenuId").val();
	if(digitalMenuId == '0'){
		toastr.error("Please Save This DigitalMenu To Add Image");
	}
	else{
		var menus = [];
		$(activeTab+" .digitalmenu_menus ul.dropdown-menu").find("li.selected").each(function(){
			menus.push($('.digitalmenu_menus option').eq($(this).attr("data-original-index")).val());
		})
		if(menus.length != 0){
		 	var menu = $(activeTab+" #tabForm").find("[name='menuType']").find('option:selected').val();
		 	var design = $(activeTab+" #tabForm").find("[name='menuDesign']").find('option:selected').val();
		 	design = design.match(/\d+/)+'';
			var orientation = $(activeTab+" #tabForm").find("[name='orientation']:checked").val();
			var screenNumber = $(activeTab+" .screennumber").val();
			$.ajax({
				type : 'get',
				url : '/phonemyfood/user/digitalMenu/digitalMenuPhotoModal',
				data: { 'menus' : menus,
						'menu' : menu,
						'design' : design,
						'orientation' : orientation
					},
				success : function(data) {
					$("#digitalMenuImage").html(data);
					$("#digitalMenuImage").modal();
				}
			});
		}
		else
			toastr.error("Please Select Menu");
	}
}

$(document).on("keyup", ".search-digitalmenu-items", function() {
	canvas.remove(object);
	var menus = [];
	$(activeTab+" .digitalmenu_menus ul.dropdown-menu").find("li.selected").each(function(){
		menus.push($('.digitalmenu_menus option').eq($(this).attr("data-original-index")).val());
	})
	var menuid = $(activeTab+" .digitalmenu_menus").find("option:selected").attr("id");
    $.ajax({
        type: "get",
        url: "/phonemyfood/user/digitalMenu/search_item_value",
        data: {
        		key: $(".search-digitalmenu-items").val() ,
        		'menus' : menus
        	}
	}).done(function(t){
       $(".food-list ul").empty(),
       0 != t.length ? $.each(t, function(t, e) {
            $(".food-list ul").append("<li id='"+e.items.id+"' class='select_digitalmenu_item' onclick=select_digitalmenu_item(this)><span>"+e.items.name+"</span><span class='hidden'>"+e.imageId+"</span></li>");
        }) : $(".food-list ul").append($("<li>").text("No Record Found"))
    }); 
})

$("#link-button").click(function(){
	if($(activeTab).find(".digitalMenuId").val() == 0)
		toastr.error("Please Save This DigitalMenu For Link");
	else{
		var openedTabId = $(activeTab).find("form > input[name=id]").val();
		var temp = "data";
		$("#tabElements .thisDigitalMenuId").each(function(index){
	 		var linkedTab = ($(this).attr("class").split(" "))[0];
	 		if(linkedTab == openedTabId){
	 			temp = null;
	 			return;
	 		}
	 	});
		if(temp==null)
			toastr.error("release this");
		else{
			$.ajax({
				type: "get",
				url: contextURL + "user/digitalMenu/digitalMenuLinkModal",
				data:{
					"screen_num":  $(activeTab+" .screennumber").val()
				},
				success : function(data) {
					$("#digitalMenuLinkModal").html(data);
					$("#digitalMenuLinkModal").modal();
				}
			})
		}
	}
})

$(document).on('click','.make_all_screens',function(){
	if($(this).find('input').attr('data-id') == "true"){
		$(this).find('input').attr('value','true');
		$(this).find('input').attr('data-id','false');
	} else {
		$(this).find('input').attr('value','false'); 
		$(this).find('input').attr('data-id','true'); 
	}
});

$(document).on('click','img.img-check',function(){
	$(this).toggleClass("img-checked");	
});

$(document).on('click','.getselected',function(){
	var mvar = "";
	var nvar = new Array();
	$(".link-with").find("[name='link-tab']:checked").each(function() {
		nvar.push($(this).attr("value"));
	});
	if(nvar.length==0){
		nvar = [0];
	}
	$.ajax({
		type: "get",
		url: contextURL + "user/digitalMenu/linkDigitalMenus",
		data:{
			"digitalMenuIds":  nvar,
			"currentDigitalMenuId" : $(".activated-tab").find("li").attr("id")
		}
	}).done(function(data){
		toastr.success("done");
		location.reload();
	});
})

function layoutClickBgChange(){
	$(".layouts").click(function(){
		$(activeTab+" .layout-row").find("input[type=radio]:checked").parent().css("background", "#2d2d2d");
		$(this).css("background", "#01431f");
	})
}

function orientationClickBgChange(){
	$(".orientations").click(function(){
		if(!($(this).find("input[type=radio]").attr("disabled"))){
			$(activeTab+" .orientation-row").find("input[type=radio]:checked").parent().css("background", "#2d2d2d");
			$(this).css("background", "#01431f");
		}
	})
}

function layoutOrientationBgChange(){
	$(".layout-row").find("input[name=layout]:checked").parent().css("background", "#01431f");
	$(".orientation-row").find("input[name=orientation]:checked").parent().css("background", "#01431f");
}

function initializeSelectpicker(){
	$('.selectpicker').selectpicker({
		noneSelectedText: 'Select Menu',
		width: '182px',
		template: { caret: '' }
	});	
}

function selectedMenusOfDigitalMenu(){
	var menus = [];
	$(".selected-menus").each(function(){
		$(this).find("option").each(function(){
			menus.push($(this).val());
		})
	$(this).parent().find('.selectpicker').selectpicker('val', menus);
	menus = [];
	})
}

function deleteDigitalmenuModal(){
	var screenNumber = $(activeTab+" .screennumber").val();
	$.ajax({
        type: "get",	
        url: contextURL + "user/digitalMenu/digitalMenuDeleteModal",
        data:{
        	"tab":screenNumber
        }
	})
	.done(function(data){
		$("#digitalMenuDeleteModal").html(data);
		$("#digitalMenuDeleteModal").modal();
	});
}

$(document).on("click", "#deleteDigitalMenu", function() {
	var mid=$(activeTab+" form").find("input[name=id]").val();
	$.ajax({
        type: "get",
        url: contextURL + "user/digitalMenu/deleteDigitalMenu",
        data: {
        	"mid" : mid
        },
        success: function(t) {
        	location.href = contextURL + "user/digitalMenu/digitalMenuPage?action="+t;
        },
        error: function(t) {
        	toastr.warning("Something went wrong, unable to delete");
        }
    })
})