create table oauth_client_details (
  client_id VARCHAR(256) PRIMARY KEY,
  resource_ids VARCHAR(256),
  client_secret VARCHAR(256),
  scope VARCHAR(256),
  authorized_grant_types VARCHAR(256),
  web_server_redirect_uri VARCHAR(256),
  authorities VARCHAR(256),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additional_information VARCHAR(4096),
  autoapprove VARCHAR(256)
);

create table oauth_client_token (
  token_id VARCHAR(256),
  token bytea,
  authentication_id VARCHAR(256),
  user_name VARCHAR(256),
  client_id VARCHAR(256)
);

create table oauth_access_token (
  token_id VARCHAR(256),
  token bytea,
  authentication_id VARCHAR(256),
  user_name VARCHAR(256),
  client_id VARCHAR(256),
  authentication bytea,
  refresh_token VARCHAR(256)
);

create table oauth_refresh_token (
  token_id VARCHAR(256),
  token bytea,
  authentication bytea
);

create table oauth_code (
  code VARCHAR(256), authentication bytea
);

create table oauth_approvals (
  userId VARCHAR(256),
  clientId VARCHAR(256),
  scope VARCHAR(256),
  status VARCHAR(10),
  expiresAt TIMESTAMP,
  lastModifiedAt TIMESTAMP
);

INSERT INTO public.users(
            created_at, created_by_id, update_at, update_by_id, is_active, 
            email, name, encrypted_password, remember_created_at, reset_password_sent_at, 
            reset_password_token)
    VALUES (null, null, null, null, true, 
            'bhautikd@exitosystems.com','Bhautik Dobariya' ,'{bcrypt}$2a$10$dXJ3SW6G7P50lGmMkkmwe.20cQQubK3.HZWzG3YB1tlRy.fqvM/BG', null, null, 
            null);
INSERT INTO public.role(
            created_at, created_by_id, update_at, update_by_id, name, 
            resource_type)
    VALUES (null, null, null, null, 'ROLE_ADMIN', 
            null);
            
INSERT INTO public.role(
            created_at, created_by_id, update_at, update_by_id, name, 
            resource_type)
    VALUES (null, null, null, null, 'ROLE_USER', 
            null);
            
INSERT INTO public.users_role(
            user_id, role_id)
    VALUES (1, 1);
INSERT INTO public.users_role(
            user_id, role_id)
    VALUES (1, 2);

--    OAUTH TOKEN
INSERT INTO public.oauth_client_details(
            client_id, resource_ids, client_secret, scope, authorized_grant_types, 
            web_server_redirect_uri, authorities, access_token_validity, 
            refresh_token_validity, additional_information, autoapprove)
    VALUES ('account-services', null, '{bcrypt}$2a$10$dXJ3SW6G7P50lGmMkkmwe.20cQQubK3.HZWzG3YB1tlRy.fqvM/BG', 'read', 'authorization_code,password,refresh_token,implicit', 
            null, null, 3600, 
            null, null, null);