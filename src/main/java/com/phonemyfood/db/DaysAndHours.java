package com.phonemyfood.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="day_and_hours")
public class DaysAndHours extends Auditable<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6984591757342112848L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="timeable_type", columnDefinition="TEXT")
	private String timeableType;
	
	@Column(name="timeable_id")
	private long timeableId;
	
	@Column(name="day_name", columnDefinition="TEXT")
	private String dayName;
	
	@Column(name="start_time", columnDefinition="TEXT")
	private String startTime="";
	
	@Column(name="end_time", columnDefinition="TEXT")
	private String endTime="";
	
	@Column(name="active")
	private boolean active = false;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTimeableType() {
		return timeableType;
	}

	public void setTimeableType(String timeableType) {
		this.timeableType = timeableType;
	}

	public long getTimeableId() {
		return timeableId;
	}

	public void setTimeableId(long timeableId) {
		this.timeableId = timeableId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getDayName() {
		return dayName;
	}

	public void setDayName(String dayName) {
		this.dayName = dayName;
	}
	
}
