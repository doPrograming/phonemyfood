package com.phonemyfood.db;

public enum ColorPicker {
	Red("Red"), Green("Green"), Blue("Blue");
	
	private String colorName;
	
	private ColorPicker(String colorName) {
		this.colorName = colorName;
	}
	
	public String getColorName() {
		return colorName;
	}
}
