package com.phonemyfood.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_addresses")
public class UserAddresses extends Auditable<Long> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7875634615400467614L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="address", columnDefinition="TEXT")
	private String address;
	
	@Column(name="phone_number", columnDefinition="TEXT")
	private String phoneNumber;
	
	@Column(name="city", columnDefinition="TEXT")
	private String city;
	
	@Column(name="state", columnDefinition="TEXT")
	private String state;
	
	@Column(name="zip", columnDefinition="TEXT")
	private String zip;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="description", columnDefinition="TEXT")
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
