package com.phonemyfood.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_specials")
public class UserSpecials extends Auditable<Long> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6136890804487981447L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="title", columnDefinition="TEXT")
	private String title;
	
	@Column(name="descreption_text", columnDefinition="TEXT")
	private String descriptionText;

	/*@Column(name="description_type", columnDefinition="TEXT")
	private String descriptionType;*/
	
	@Column(name="discount_type", columnDefinition="TEXT")
	private String discountType;
	
	@Column(name="discount_value", columnDefinition="TEXT")
	private String discountValue;
	
	@Column(name="min_buy", columnDefinition="TEXT")
	private String minBuy;
	
	@Column(name="start_date", columnDefinition="TEXT")
	private String startDate;
	
	@Column(name="end_date", columnDefinition="TEXT")
	private String endDate;
	
	@Column(name="user_menu", columnDefinition="TEXT")
	private String userMenu;
	
	@Column(name="selected_category", columnDefinition="TEXT")
	private String selectedCategory;
		
	@Column(name = "item_id",columnDefinition="TEXT")
	private String item;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@Column(name="active")
	private boolean active;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescriptionText() {
		return descriptionText;
	}

	public void setDescriptionText(String descriptionText) {
		this.descriptionText = descriptionText;
	}

	/*public String getDescriptionType() {
		return descriptionType;
	}

	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}*/

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(String discountValue) {
		this.discountValue = discountValue;
	}

	public String getMinBuy() {
		return minBuy;
	}

	public void setMinBuy(String minBuy) {
		this.minBuy = minBuy;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getUserMenu() {
		return userMenu;
	}

	public void setUserMenu(String userMenu) {
		this.userMenu = userMenu;
	}

	public String getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(String selectedCategory) {
		this.selectedCategory = selectedCategory;
	}
}
