package com.phonemyfood.db;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="digital_menu")
public class DigitalMenu extends Auditable<Long> implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="businesses_id")
	private Businesses businesses;
	
	@Column(name="screen")
	private String screen;
	
	@Enumerated(EnumType.STRING)
	private MenuType menuType;
	
	@Enumerated(EnumType.STRING)
	private MenuDesign menuDesign;
	
	@ManyToMany
	@JoinTable(name = "digital_menu_user_menu", joinColumns = {
			@JoinColumn(name = "digital_menu_id", referencedColumnName = "ID") }, inverseJoinColumns = {
					@JoinColumn(name = "user_menu_id", referencedColumnName = "ID") })
	private Collection<UserMenu> userMenus;

	@Enumerated(EnumType.STRING)
	private Orientation orientation;
	
	@Column(name="layout",nullable=true)
	private int layout;
	
	@Enumerated(EnumType.STRING)
	private ColorPicker colorPicker;
	
	@Column(name="page_duration",nullable=true)
	private int pageDuration;
	
	@Column(name="photo_duration",nullable=true)
	private int photoDuration;
	
	@Column(name="promo_message1")
	private String promomsg1;
	
	@Column(name="promo_message2")
	private String promomsg2;
	
	@Column(name="promo_message3")
	private String promomsg3;
	
	@ManyToOne
	@JoinColumn
	private DigitalMenu linkedWithMenu;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public Businesses getBusinesses() {
		return businesses;
	}

	public void setBusinesses(Businesses businesses) {
		this.businesses = businesses;
	}

	public MenuType getMenuType() {
		return menuType;
	}

	public void setMenuType(MenuType menuType) {
		this.menuType = menuType;
	}
	
	public int getLayout() {
		return layout;
	}

	public void setLayout(int layout) {
		this.layout = layout;
	}

	public int getPageDuration() {
		return pageDuration;
	}

	public void setPageDuration(int pageDuration) {
		this.pageDuration = pageDuration;
	}

	public int getPhotoDuration() {
		return photoDuration;
	}

	public void setPhotoDuration(int photoDuration) {
		this.photoDuration = photoDuration;
	}

	public String getPromomsg1() {
		return promomsg1;
	}

	public void setPromomsg1(String promomsg1) {
		this.promomsg1 = promomsg1;
	}

	public String getPromomsg2() {
		return promomsg2;
	}

	public void setPromomsg2(String promomsg2) {
		this.promomsg2 = promomsg2;
	}

	public String getPromomsg3() {
		return promomsg3;
	}

	public void setPromomsg3(String promomsg3) {
		this.promomsg3 = promomsg3;
	}
	
	public String getScreen() {
		return screen;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}

	public MenuDesign getMenuDesign() {
		return menuDesign;
	}

	public void setMenuDesign(MenuDesign menuDesign) {
		this.menuDesign = menuDesign;
	}
	
	public Collection<UserMenu> getUserMenus() {
		return userMenus;
	}

	public void setUserMenus(Collection<UserMenu> userMenus) {
		this.userMenus = userMenus;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	public ColorPicker getColorPicker() {
		return colorPicker;
	}

	public void setColorPicker(ColorPicker colorPicker) {
		this.colorPicker = colorPicker;
	}

	public DigitalMenu getLinkedWithMenu() {
		return linkedWithMenu;
	}

	public void setLinkedWithMenu(DigitalMenu linkedWithMenu) {
		this.linkedWithMenu = linkedWithMenu;
	}	
}