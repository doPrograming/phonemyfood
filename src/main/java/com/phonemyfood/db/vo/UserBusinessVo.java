package com.phonemyfood.db.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.DaysAndHours;
import com.phonemyfood.db.User;
import com.phonemyfood.db.UserAddresses;

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class UserBusinessVo {
	private User user;
	private Businesses businesses;
	private UserAddresses userAddresses;
	private List<DaysAndHours> daysAndHours;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Businesses getBusinesses() {
		return businesses;
	}

	public void setBusinesses(Businesses businesses) {
		this.businesses = businesses;
	}

	public UserAddresses getUserAddresses() {
		return userAddresses;
	}

	public void setUserAddresses(UserAddresses userAddresses) {
		this.userAddresses = userAddresses;
	}
	
	public void addDaysAndHours(DaysAndHours daysAndHours) {
        this.daysAndHours.add(daysAndHours);
    }

	public List<DaysAndHours> getDaysAndHours() {
		return daysAndHours;
	}

	public void setDaysAndHours(List<DaysAndHours> daysAndHours) {
		this.daysAndHours = daysAndHours;
	}
	
	

}
