package com.phonemyfood.db.vo;

import java.util.List;

import com.phonemyfood.db.Rewards;

public class RewardsVO {
	
	private List<Rewards> rewards;

	public List<Rewards> getRewards() {
		return rewards;
	}

	public void setRewards(List<Rewards> rewards) {
		this.rewards = rewards;
	}

}
