package com.phonemyfood.db.vo;

import java.util.List;

import com.phonemyfood.db.ComboOptions;
import com.phonemyfood.db.Items;

public class ComboOptionVO {
	private ComboOptions comboOptions;
	private long priceAndComboId;
	private List<Items> option1;
	private List<Items> option2;
	private List<Items> option3;
	private List<Items> option4;

	
	public ComboOptions getComboOptions() {
		return comboOptions;
	}
	public void setComboOptions(ComboOptions comboOptions) {
		this.comboOptions = comboOptions;
	}
	public long getPriceAndComboId() {
		return priceAndComboId;
	}

	public void setPriceAndComboId(long priceAndComboId) {
		this.priceAndComboId = priceAndComboId;
	}

	public List<Items> getOption1() {
		return option1;
	}

	public void setOption1(List<Items> option1) {
		this.option1 = option1;
	}

	public List<Items> getOption2() {
		return option2;
	}

	public void setOption2(List<Items> option2) {
		this.option2 = option2;
	}

	public List<Items> getOption3() {
		return option3;
	}

	public void setOption3(List<Items> option3) {
		this.option3 = option3;
	}

	public List<Items> getOption4() {
		return option4;
	}

	public void setOption4(List<Items> option4) {
		this.option4 = option4;
	}
}
