package com.phonemyfood.db.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.Items;
import com.phonemyfood.db.PriceAndCombos;

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ItemVo {
	
	private Items itemModel;
	
	private Businesses businesses;
	
	private List<PriceAndCombos> priceAndCombos;

	public Items getItemModel() {
		return itemModel;
	}

	public void setItemModel(Items itemModel) {
		this.itemModel = itemModel;
	}

	public Businesses getBusinesses() {
		return businesses;
	}

	public void setBusinesses(Businesses businesses) {
		this.businesses = businesses;
	}
	
	public List<PriceAndCombos> getPriceAndCombos() {
		return priceAndCombos;
	}
	
	public void setPriceAndCombos(List<PriceAndCombos> priceAndCombos) {
		this.priceAndCombos = priceAndCombos;
	}
	
	public void addPriceAndCombos(PriceAndCombos priceAndCombos){
		this.priceAndCombos.add(priceAndCombos);
	}
	
}
