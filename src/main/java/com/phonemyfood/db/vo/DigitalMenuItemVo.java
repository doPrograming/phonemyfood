package com.phonemyfood.db.vo;

public class DigitalMenuItemVo {

	private long itemId;

	private String itemName;

	private String itemImage;

	private String digitalMenuItemImage;

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemImage() {
		return itemImage;
	}

	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}

	public String getDigitalMenuItemImage() {
		return digitalMenuItemImage;
	}

	public void setDigitalMenuItemImage(String digitalMenuItemImage) {
		this.digitalMenuItemImage = digitalMenuItemImage;
	}

	
}
