package com.phonemyfood.db.vo;

import java.util.List;

import com.phonemyfood.db.Ingredients;

public class IngredientsVO {
	List<Ingredients> ingredients;
	
	private long itemId;

	public List<Ingredients> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredients> ingredients) {
		this.ingredients = ingredients;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	
}
