package com.phonemyfood.db.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.phonemyfood.db.DaysAndHours;
import com.phonemyfood.db.UserMenu;

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class UserMenuVO {

	private UserMenu userMenu;
	private List<DaysAndHours> daysAndHours;

	public UserMenu getUserMenu() {
		return userMenu;
	}

	public void setUserMenu(UserMenu userMenu) {
		this.userMenu = userMenu;
	}

	public List<DaysAndHours> getDaysAndHours() {
		return daysAndHours;
	}

	public void setDaysAndHours(List<DaysAndHours> daysAndHours) {
		this.daysAndHours = daysAndHours;
	}
	
	public void addDaysAndHours(DaysAndHours daysAndHours) {
        this.daysAndHours.add(daysAndHours);
    }
}
