package com.phonemyfood.db.vo;

import com.phonemyfood.db.Items;

public class ItemImageVO {
	
	private Items items;
	
	private long imageId;

	public Items getItems() {
		return items;
	}

	public void setItems(Items items) {
		this.items = items;
	}

	public long getImageId() {
		return imageId;
	}

	public void setImageId(long imageId) {
		this.imageId = imageId;
	}
}