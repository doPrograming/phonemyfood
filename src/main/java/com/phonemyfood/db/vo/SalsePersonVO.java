package com.phonemyfood.db.vo;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserAddresses;

public class SalsePersonVO {

	private User user;
	private UserAddresses userAddresses;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public UserAddresses getUserAddresses() {
		return userAddresses;
	}
	public void setUserAddresses(UserAddresses userAddresses) {
		this.userAddresses = userAddresses;
	}
	
}
