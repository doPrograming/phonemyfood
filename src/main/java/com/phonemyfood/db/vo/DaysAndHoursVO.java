package com.phonemyfood.db.vo;

import java.util.List;

import com.phonemyfood.db.DaysAndHours;

public class DaysAndHoursVO {
	
	private List<DaysAndHours> daysAndHours;

	public List<DaysAndHours> getDaysAndHours() {
		return daysAndHours;
	}

	public void setDaysAndHours(List<DaysAndHours> daysAndHours) {
		this.daysAndHours = daysAndHours;
	}

	public void addDaysAndHours(DaysAndHours daysAndhours2) {
		this.daysAndHours.add(daysAndhours2);
	}
	
	
	
}
