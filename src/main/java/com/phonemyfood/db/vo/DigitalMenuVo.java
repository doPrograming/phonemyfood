package com.phonemyfood.db.vo;

import java.util.List;

public class DigitalMenuVo {

	private String businesses;

	private int layout;

	private String screen;

	private String menuDesign;

	private String menuType;

	private String userMenuName;

	private long userMenuId;

	private String orientation;

	private String colorPicker;

	private int pageDuration;
	
	private int photoDuration;
	
	private String promomsg1;
	
	private String promomsg2;
	
	private String promomsg3;
	
	private List<DigitalMenuItemVo> digitalMenuItemVos;

	public String getBusinesses() {
		return businesses;
	}

	public void setBusinesses(String businesses) {
		this.businesses = businesses;
	}

	public int getLayout() {
		return layout;
	}

	public void setLayout(int layout) {
		this.layout = layout;
	}

	public String getScreen() {
		return screen;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}

	public String getMenuDesign() {
		return menuDesign;
	}

	public void setMenuDesign(String menuDesign) {
		this.menuDesign = menuDesign;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getUserMenuName() {
		return userMenuName;
	}

	public void setUserMenuName(String userMenuName) {
		this.userMenuName = userMenuName;
	}

	public long getUserMenuId() {
		return userMenuId;
	}

	public void setUserMenuId(long userMenuId) {
		this.userMenuId = userMenuId;
	}

	public String getOrientation() {
		return orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	public String getColorPicker() {
		return colorPicker;
	}

	public void setColorPicker(String colorPicker) {
		this.colorPicker = colorPicker;
	}

	public int getPageDuration() {
		return pageDuration;
	}

	public void setPageDuration(int pageDuration) {
		this.pageDuration = pageDuration;
	}

	public int getPhotoDuration() {
		return photoDuration;
	}

	public void setPhotoDuration(int photoDuration) {
		this.photoDuration = photoDuration;
	}

	public String getPromomsg1() {
		return promomsg1;
	}

	public void setPromomsg1(String promomsg1) {
		this.promomsg1 = promomsg1;
	}

	public String getPromomsg2() {
		return promomsg2;
	}

	public void setPromomsg2(String promomsg2) {
		this.promomsg2 = promomsg2;
	}

	public String getPromomsg3() {
		return promomsg3;
	}

	public void setPromomsg3(String promomsg3) {
		this.promomsg3 = promomsg3;
	}

	public List<DigitalMenuItemVo> getDigitalMenuItemVos() {
		return digitalMenuItemVos;
	}

	public void setDigitalMenuItemVos(List<DigitalMenuItemVo> digitalMenuItemVos) {
		this.digitalMenuItemVos = digitalMenuItemVos;
	}

}
