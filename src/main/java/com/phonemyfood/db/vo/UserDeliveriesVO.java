package com.phonemyfood.db.vo;

import java.util.List;

import com.phonemyfood.db.UserDeliveries;

public class UserDeliveriesVO {
	private List<UserDeliveries> userDeliveries;

	public List<UserDeliveries> getUserDeliveries() {
		return userDeliveries;
	}

	public void setUserDeliveries(List<UserDeliveries> userDeliveries) {
		this.userDeliveries = userDeliveries;
	}

}
