package com.phonemyfood.db.vo;

public enum RewardText {
	
	RECOMFRI{
        @Override
        public String getText() {
            return "Recommend a friend";
        }
        
        @Override
        public String getValue() {
			return "";
		}
    }, 
	FREEFOOD{
        @Override
        public String getText() {
            return "${value1} Free Food!";
        }
        
        @Override
        public String getValue() {
			return "";
		}
    },
	WHEELOFFOOD{
        @Override
        public String getText() {
            return "Wheel of food (Great Prizes)";
        }
        
        @Override
        public String getValue() {
			return "";
		}
    },
	FIRSTSOCIALREVIEW{
        @Override
        public String getText() {
            return "First Social Review";
        }
        
        @Override
        public String getValue() {
			return "";
		}
    },
	FREEGIFT{
        @Override
        public String getText() {
            return "Free Gift";
        }
        
        @Override
        public String getValue() {
			return "";
		}
    };

	public String getText() {
		return null;
	}

	public String getValue() {
		return null;
	}

}
