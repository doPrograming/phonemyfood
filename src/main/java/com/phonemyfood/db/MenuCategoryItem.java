package com.phonemyfood.db;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="menu_category_items")
public class MenuCategoryItem extends Auditable<Long> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8591418421733079302L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "user_menu_id")
	private UserMenu userMenu; 
	
	@ManyToOne
	@JoinColumn(name= "category_id")
	private Categories categories;
	
	/*@ManyToOne
	@JoinColumn(name= "item_id")
	private Items items;*/
	/*
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Items.class)
	@CollectionTable(name = "mci_item")
	@Column(name = "item_id",unique=false, nullable = true)*/
	
	@ManyToMany(cascade = { 
	        CascadeType.PERSIST, 
	        CascadeType.MERGE
	    })
	@JoinTable(name = "mci_item", joinColumns = {
			@JoinColumn(name = "menu_category_item_id", referencedColumnName = "ID") }, inverseJoinColumns = {
					@JoinColumn(name = "items_id", referencedColumnName = "ID") })
	Collection<Items> items;
	
	@Column(name="sortable_item_id")
	private long shortableId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserMenu getUserMenu() {
		return userMenu;
	}

	public void setUserMenu(UserMenu userMenu) {
		this.userMenu = userMenu;
	}

	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	public Collection<Items> getItems() {
		return items;
	}

	public void setItems(Collection<Items> items) {
		this.items = items;
	}

	public long getShortableId() {
		return shortableId;
	}

	public void setShortableId(long shortableId) {
		this.shortableId = shortableId;
	}
}
