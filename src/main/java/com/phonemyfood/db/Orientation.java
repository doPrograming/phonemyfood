package com.phonemyfood.db;

public enum Orientation {
	
	Vertical("Vertical"),Horizontal("Horizontal");
	
	private String orientationType;
	
	private Orientation(String orientationType) {
		this.orientationType = orientationType;
	}

	public String getOrientationType() {
		return orientationType;
	}
}
