package com.phonemyfood.db;

public enum MenuType {

	Menu("Menu"),Variety("Variety"),Feature("Feature");
	
	private String menuName;
	 
	private MenuType(String menuName) {
		this.menuName = menuName;
	}
	
	public String getMenuName() {
		return menuName;
	}
}