package com.phonemyfood.db;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
public abstract class Auditable<U> {
	
	
	@CreatedBy
	protected U createdById;
	
	@CreatedDate
	@Temporal(TIMESTAMP)
	protected Date createdAt;
	
	@LastModifiedBy
	protected U updateById;

	@LastModifiedDate
	@Temporal(TIMESTAMP)
	protected Date updateAt;
	
	
	
}
