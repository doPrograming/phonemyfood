package com.phonemyfood.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_deliveries")
public class UserDeliveries extends Auditable<Long> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1222444429500369314L;
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="deliveries_miles", columnDefinition="TEXT")
	private String deliveriesMiles;
	
	@Column(name="price", columnDefinition="TEXT")
	private String price;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDeliveriesMiles() {
		return deliveriesMiles;
	}

	public void setDeliveriesMiles(String deliveriesMiles) {
		this.deliveriesMiles = deliveriesMiles;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	
}
