package com.phonemyfood.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_menus")
public class UserMenu extends Auditable<Long> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6173796881635573318L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@Column(name="menu_name", columnDefinition="TEXT")
	private String menuName;
	
	@Column(name="app_only")
	private boolean appOnly;
	
	@Column(name="digitalmenu_only")
	private boolean digitalmenuOnly;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public boolean isAppOnly() {
		return appOnly;
	}

	public void setAppOnly(boolean appOnly) {
		this.appOnly = appOnly;
	}

	public boolean isDigitalmenuOnly() {
		return digitalmenuOnly;
	}

	public void setDigitalmenuOnly(boolean digitalmenuOnly) {
		this.digitalmenuOnly = digitalmenuOnly;
	}
}