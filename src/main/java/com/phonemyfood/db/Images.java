package com.phonemyfood.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="images")
public class Images extends Auditable<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2528289668625064642L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="imageable_id")
	private Long imageableId;
	
	@Column(name="imageable_type", columnDefinition="TEXT")
	private String imageableType;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="picture", columnDefinition="TEXT")
	private String picture;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getImageableId() {
		return imageableId;
	}

	public void setImageableId(Long imageableId) {
		this.imageableId = imageableId;
	}

	public String getImageableType() {
		return imageableType;
	}

	public void setImageableType(String imageableType) {
		this.imageableType = imageableType;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
}
