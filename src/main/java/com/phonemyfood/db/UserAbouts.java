package com.phonemyfood.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_abouts")
public class UserAbouts extends Auditable<Long> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5109898195772646656L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="description", columnDefinition="TEXT")
	private String description;
	
	@Column(name="address", columnDefinition="TEXT")
	private String address;
	
	@Column(name="city", columnDefinition="TEXT")
	private String city;
	
	@Column(name="state", columnDefinition="TEXT")
	private String state;
	
	@Column(name="zip", columnDefinition="TEXT")
	private String zip;
	
	@Column(name="phone", columnDefinition="TEXT")
	private String phone;
	
	@Column(name="email", columnDefinition="TEXT")
	private String email;
	
	@Column(name="facebook", columnDefinition="TEXT")
	private String facebook;
	
	@Column(name="twitter", columnDefinition="TEXT")
	private String twitter;
	
	@Column(name="foursquare", columnDefinition="TEXT")
	private String fourSquare;
	
	@Column(name="fac_password", columnDefinition="TEXT")
	private String fbPassword;
	
	@Column(name="twi_password", columnDefinition="TEXT")
	private String twtPassword;
	
	@Column(name="fou_password", columnDefinition="TEXT")
	private String fourPassword;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getFourSquare() {
		return fourSquare;
	}

	public void setFourSquare(String fourSquare) {
		this.fourSquare = fourSquare;
	}

	public String getFbPassword() {
		return fbPassword;
	}

	public void setFbPassword(String fbPassword) {
		this.fbPassword = fbPassword;
	}

	public String getTwtPassword() {
		return twtPassword;
	}

	public void setTwtPassword(String twtPassword) {
		this.twtPassword = twtPassword;
	}

	public String getFourPassword() {
		return fourPassword;
	}

	public void setFourPassword(String fourPassword) {
		this.fourPassword = fourPassword;
	}
	
	
	
}
