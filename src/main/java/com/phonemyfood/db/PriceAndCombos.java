package com.phonemyfood.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="price_and_combos")
public class PriceAndCombos extends Auditable<Long> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6642136673786511853L;
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;
	
	@Column(name="name", columnDefinition="TEXT")
	private String name;
	
	@Column(name="price", columnDefinition="TEXT")
	private String price;
	
	@ManyToOne
	@JoinColumn(name="item_id")
	private Items item;
	
	@Column(name="record_id")
	private int record;
	
	@Column(name="user_menu_id",nullable=true)
	private Long userMenuId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Items getItem() {
		return item;
	}
	
	public void setItem(Items item) {
		this.item = item;
	}

	public int getRecord() {
		return record;
	}

	public void setRecord(int record) {
		this.record = record;
	}

	public Long getUserMenuId() {
		return userMenuId;
	}

	public void setUserMenuId(Long userMenuId) {
		this.userMenuId = userMenuId;
	}
}