package com.phonemyfood.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "combo_options")
public class ComboOptions extends Auditable<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1160291189336580244L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "option1", columnDefinition = "TEXT")
	private String option1 = "";

	@Column(name = "option2", columnDefinition = "TEXT")
	private String option2 = "";

	@Column(name = "option3", columnDefinition = "TEXT")
	private String option3 = "";

	@Column(name = "option4", columnDefinition = "TEXT")
	private String option4 = "";

	@ManyToOne
	@JoinColumn(name = "price_and_combo_id")
	private PriceAndCombos priceCombos;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOption3() {
		return option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}

	public String getOption4() {
		return option4;
	}

	public void setOption4(String option4) {
		this.option4 = option4;
	}

	public PriceAndCombos getPriceCombos() {
		return priceCombos;
	}

	public void setPriceCombos(PriceAndCombos priceCombos) {
		this.priceCombos = priceCombos;
	}

}
