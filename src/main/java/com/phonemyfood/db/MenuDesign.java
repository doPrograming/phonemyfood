package com.phonemyfood.db;

public enum MenuDesign {

	Design1("Design1"), Design2("Design2"), Design3("Design3"), Design4("Design4"), Design5("Design5"),
	Design6("Design6"), Design7("Design7"), Design8("Design8"), Design9("Design9"), Design10("Design10");

	private String designName;

	private MenuDesign(String designName) {
		this.designName = designName;
	}

	public String getDesignName() {
		return designName;
	}
}