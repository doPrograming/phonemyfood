package com.phonemyfood.user.controller;

import java.util.Comparator;

import org.springframework.beans.factory.annotation.Autowired;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.DigitalMenu;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.BusinessesService;

public class BaseController {

	@Autowired
	private BusinessesService businessesService;
	
	@Autowired
	private LoginUserDetail loginUserDetail;
	
	 protected Businesses findBusinessesByLoginUser() {
		return businessesService.findByUser(loginUserDetail.getCurrentLoginUser());
	}
}

class DigitalMenuComparator implements Comparator<DigitalMenu>{
	@Override
	public int compare(DigitalMenu o1, DigitalMenu o2) {
		return (o1.getScreen()).compareTo((o2.getScreen()));
	}
}
