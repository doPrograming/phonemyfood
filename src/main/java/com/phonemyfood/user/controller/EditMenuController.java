package com.phonemyfood.user.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.MenuCategoryItem;
import com.phonemyfood.service.ItemsService;
import com.phonemyfood.service.MenuCategoryItemsService;

@Controller
@RequestMapping(value = "/user")
public class EditMenuController {
	
	@Autowired
	private MenuCategoryItemsService menuCategoryItemsService;
	
	@Autowired
	private ItemsService itemsService;

	@RequestMapping(value = "/menu/update_editmenu", method = RequestMethod.POST)
	public String EditMenuByCategoryItem(@ModelAttribute("menuCategoryItem") MenuCategoryItem menuCategoryItem, Errors Errors,HttpServletRequest request) {
		request.getSession().setAttribute("selectedItems",menuCategoryItem);
		menuCategoryItemsService.save(menuCategoryItem);
		return "redirect:/user/editMenu?updated='true'";
	}
	
	@RequestMapping(value="/menu/get_already_selected_items")
	public ResponseEntity<?> get_already_selected_items(@RequestParam(value ="menu_id") long menuId, @RequestParam(value ="category_id") long categoryId,HttpServletRequest request) {
		MenuCategoryItem menuCategoryItem = menuCategoryItemsService.findAllByMenuIdAndCategoryId(menuId,categoryId);
		if(request.getSession().getAttribute("selectedItems")!=null && menuCategoryItem==null)
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		if (request.getSession().getAttribute("selectedItems") != null && menuCategoryItem.getItems().size() == 0)
			menuCategoryItem.setItems(((MenuCategoryItem) request.getSession().getAttribute("selectedItems")).getItems());
		return ResponseEntity.ok(menuCategoryItem);
	}
	
	@GetMapping(value="/menu/select_unselected_items")
	public ResponseEntity<?> select_unselected_items(HttpServletRequest request) {
		MenuCategoryItem categoryItem = (MenuCategoryItem) request.getSession().getAttribute("selectedItems"); 
		request.getSession().removeAttribute("selectedItems");
		return ResponseEntity.ok(categoryItem);
	}
	
	@RequestMapping(value="/menus/get_category_item")
	public ResponseEntity<?> get_category_item(@RequestParam(value ="menu_id") long menuId, @RequestParam(value ="category_id") long categoryId) {
		MenuCategoryItem menuCategoryItem = menuCategoryItemsService.findAllByMenuIdAndCategoryId(menuId, categoryId);
		if (menuCategoryItem != null) {
			Set<BigInteger> itemIds = menuCategoryItemsService.findByMenuCategoryItem(menuCategoryItem.getId());
			Collection<Items> collection = new ArrayList<Items>();
			for (BigInteger items : itemIds) {
				collection.add(itemsService.getOne(items.longValue()));
			}
			return ResponseEntity.ok(collection);
		} else {
			return ResponseEntity.ok("");
		}
	}
	
	@RequestMapping(value="/menus/save_menu_order")
	public ResponseEntity<?> save_menu_order(@RequestParam(value ="menu_id") long menuId, @RequestParam(value ="category_id") long categoryId, @RequestParam(value="i_ids") String itemString){
		MenuCategoryItem menuCategoryItem = menuCategoryItemsService.findAllByMenuIdAndCategoryId(menuId,categoryId);
		String[] strings = itemString.split(",");
		Collection<Items> collection = new ArrayList<Items>();
		for(String string : strings){
			for(Items items : menuCategoryItem.getItems()){
				if(Long.valueOf(string) == items.getId()){
					collection.add(items);
				}
			}
		}
		menuCategoryItem.setItems(collection);
		menuCategoryItemsService.save(menuCategoryItem);
		return ResponseEntity.ok("Success");
	}
}
