package com.phonemyfood.user.controller;

import java.util.Comparator;

import com.phonemyfood.db.Items;

class SortItemsComparator implements Comparator<Items>{
	@Override
	public int compare(Items o1, Items o2) {
		return o1.getName().compareToIgnoreCase(o2.getName());
	}
}