package com.phonemyfood.user.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.phonemyfood.db.Categories;
import com.phonemyfood.db.Images;
import com.phonemyfood.db.Items;
import com.phonemyfood.db.User;
import com.phonemyfood.db.UserMenu;
import com.phonemyfood.db.UserSpecials;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.CategoriesService;
import com.phonemyfood.service.ImagesService;
import com.phonemyfood.service.ItemsService;
import com.phonemyfood.service.UserMenusService;
import com.phonemyfood.service.UserSpecialsService;
import com.phonemyfood.util.ImageUtils;

@Controller
@RequestMapping(value="/user")
public class SpecialsController {

	@Autowired
	private UserMenusService userMenusService;
	
	@Autowired
	private CategoriesService categoriesService;
	
	@Autowired
	private LoginUserDetail loginUserDetail;
	
	@Autowired
	private UserSpecialsService userSpecialsService;
	
	@Autowired
	private ImageUtils imageUtils;
	
	@Autowired
	private ImagesService imagesService;
	
	@Autowired
	private ItemsService itemsService;
	
	@RequestMapping({"/specials"})
	public String specials(Model model) {
		User user = loginUserDetail.getCurrentLoginUser();
		List<UserMenu> userMenus = userMenusService.findAllByUser(user);
		List<Categories> categories = categoriesService.getAllCategoryByUser(user);
		List<UserSpecials> userSpecials = userSpecialsService.getAllSpecialsByUser();
		List<Items> items = itemsService.findAllByUser(user);
		model.addAttribute("userSpecials", userSpecials);
		model.addAttribute("userMenus", userMenus);
		model.addAttribute("categories", categories);
		model.addAttribute("items", items);
		model.addAttribute("specials", new UserSpecials());
		return "site/specials";
	}
	
	@RequestMapping(value="/specials/add_specials", method=RequestMethod.POST)
	public String addSpecials(@ModelAttribute("specials") UserSpecials userSpecials, RedirectAttributes redirectAttributes, Model model){
		try {
			userSpecialsService.saveSpecials(userSpecials);
			redirectAttributes.addFlashAttribute("successMsg", "Successfull");
		} catch (Exception e) {
			// TODO: handle exception
			redirectAttributes.addFlashAttribute("errorMsg", "Something went wrong.");
		}
		
		return "redirect:/user/specials";
	}
	
	@RequestMapping(value="/specials/get_special_record", method=RequestMethod.GET)
	public ResponseEntity<?> get_special_record(Model model, @RequestParam("special_id") Long specialId){
		
		User user = loginUserDetail.getCurrentLoginUser();
		List<UserMenu> userMenus = userMenusService.findAllByUser(user);
		List<Categories> categories = categoriesService.getAllCategoryByUser(user);
		UserSpecials userSpecials = userSpecialsService.getSpecialsById(specialId);
		List<Items> items = itemsService.findAllByUser(user);
		model.addAttribute("userMenus", userMenus);
		model.addAttribute("categories", categories);
		model.addAttribute("items", items);
		/*model.addAttribute("userSpecials", userSpecials);*/
		model.addAttribute("specials", userSpecials);
		return ResponseEntity.ok(model);
	}
	
	@RequestMapping(value="/specials/get_special_image")
	public String get_about_image(Model model, @RequestParam(value = "special_id") Long Id){
		User user = loginUserDetail.getCurrentLoginUser(); 
		List<Images> images = imagesService.getImagesByImageableTypeAndImageableIdAndUser("UserSpecials",Id,user);
		model.addAttribute("images", images);
		return "site/modal/specialImageModal";
	}
	
	@RequestMapping(value="/specials/create_item_image", method=RequestMethod.POST)
	public ResponseEntity<?> specialsImage(@RequestParam("picture") MultipartFile file, @RequestParam("id") Long id, HttpServletRequest request){
		Images images = new Images();
		images.setImageableId(id);
		images.setImageableType("UserSpecials");
		User user = loginUserDetail.getCurrentLoginUser();
		images.setUser(user);
		images =  imageUtils.generateURL(images);
		Long persistId=imagesService.insert(images);
		images.setId(persistId);
		try {
			images = imageUtils.uploadImage(file.getBytes(), file.getOriginalFilename(), images);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		imagesService.saveImages(images);
		return ResponseEntity.ok("success");	
	}
}
