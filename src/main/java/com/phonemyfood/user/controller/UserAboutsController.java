package com.phonemyfood.user.controller;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.phonemyfood.db.DaysAndHours;
import com.phonemyfood.db.Images;
import com.phonemyfood.db.User;
import com.phonemyfood.db.UserAbouts;
import com.phonemyfood.db.UserAddresses;
import com.phonemyfood.db.UserMenu;
import com.phonemyfood.db.vo.DaysAndHoursVO;
import com.phonemyfood.repository.DayAndHoursRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.DaysAndHoursVOService;
import com.phonemyfood.service.ImagesService;
import com.phonemyfood.service.UserAboutsService;
import com.phonemyfood.service.UserAddressService;
import com.phonemyfood.service.UserMenusService;
import com.phonemyfood.util.ImageUtils;

@Controller
@RequestMapping(value="/user")
public class UserAboutsController extends BaseController{
	@Autowired
	private HttpSession httpSession;
	
	@Value("${phonemyfood.images.common.path}")
	private String commonPath;
	
	@Autowired
	private UserAboutsService userAboutsService;
	
	@Autowired
	private LoginUserDetail loginUserDetail;
	
	@Autowired
	UserAddressService userAddressService;
	
	@Autowired
	private DayAndHoursRepository dayAndHoursRepository;
	
	@Autowired
	private DaysAndHoursVOService daysAndHoursVOService;
	
	@Autowired
	private ImageUtils imageUtils;
	
	@Autowired
	private ImagesService imagesService;
	
	@Autowired
	private UserMenusService userMenusService;
	
	@RequestMapping(value="/abouts")
	public String userAbout(Model model){
		UserAbouts userAbouts = userAboutsService.findByUser();
		userAbouts = userAbouts != null ? userAbouts : new UserAbouts();
		DaysAndHoursVO daysAndHoursVO = new DaysAndHoursVO();
		List<DaysAndHours> daysAndHours = dayAndHoursRepository.findAllByTimeableIdAndTimeableType(findBusinessesByLoginUser().getId(), "Businesses");
		if(daysAndHours.isEmpty()){
			String day[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };
			for (int i = 0; i < 7; i++) {
				DaysAndHours daysAndhours = new DaysAndHours();
				daysAndhours.setDayName(day[i]);
				daysAndhours.setActive(false);
				daysAndHours.add(daysAndhours);
			}
		}
		daysAndHoursVO.setDaysAndHours(daysAndHours);
		model.addAttribute("daysAndHoursVO", daysAndHoursVO);
		model.addAttribute("UserAbouts", userAbouts);
		return "site/UserAbout";
	}
	
	@RequestMapping(value="/abouts/updateAbouts")
	public String updateUserAbouts(@ModelAttribute("UserAbouts") UserAbouts userAbouts){
		User user = loginUserDetail.getCurrentLoginUser();
		userAbouts.setUser(user);
		userAboutsService.saveUserAbout(userAbouts);
		UserAddresses userAddresses=userAddressService.findByUser(userAbouts.getUser());
		userAddressService.updateUserAddress(userAbouts, userAddresses);
		return "redirect:/user/abouts";
	}
	
	@RequestMapping(value="/abouts/updateDayAndHoursAbouts")
	public String updateDayAndHoursAbouts(@ModelAttribute("daysAndHoursVO") DaysAndHoursVO daysAndHoursVO){
		daysAndHoursVOService.saveDaysAndHoursAbout(daysAndHoursVO); 
		List<UserMenu> arrayList = userMenusService.findAllByUser(loginUserDetail.getCurrentLoginUser());
		Collections.reverse(arrayList);
		if (daysAndHoursVOService.isBusinessHours()) {
			httpSession.setAttribute("MenuList", arrayList);
			httpSession.setAttribute("AddMenuFlag",true);
		}else {
			httpSession.removeAttribute("MenuList");
			httpSession.removeAttribute("AddMenuFlag");
		}
		return "redirect:/user/abouts";	
	}
	
	@RequestMapping(value="/abouts/get_item_image")
	public String get_about_image(Model model, @RequestParam(value = "about_id") Long Id){
		User user = loginUserDetail.getCurrentLoginUser(); 
		List<Images> images = imagesService.getImagesByImageableTypeAndImageableIdAndUser("Business",Id,user);
		model.addAttribute("images", images);
		return "site/modal/aboutImageModal";
	}
	
	@RequestMapping(value="/abouts/create_item_image", method=RequestMethod.POST)
	public ResponseEntity<?> specialsImage(@RequestParam("picture") MultipartFile file, @RequestParam("id") Long id, HttpServletRequest request){
		Images images;
		Long persistId = 0l;
		User user = loginUserDetail.getCurrentLoginUser();
		images = imagesService.getImagesByImageableTypeAndImageableId("About",id,user);
		if(images!=null) {
			String[] strings = images.getPicture().split("[.]");
			persistId = images.getId();
			File f = new File(commonPath + File.separator + "images" + File.separator + user.getId() + File.separator + "About" +File.separator + images.getPicture());
			File f1 = new File(commonPath + File.separator + "images" + File.separator + user.getId() + File.separator + "About" +File.separator + strings[0]+"-500x500."+strings[1]);
			f.delete();
			f1.delete();
		}
		else {
			images=new Images();
			persistId=imagesService.insert(images);
		}
		images.setImageableId(id);
		images.setImageableType("Business");
		images.setUser(user);
		images =  imageUtils.generateURL(images);
		persistId=imagesService.insert(images);
		images.setId(persistId);
		try {
			images = imageUtils.uploadImage(file.getBytes(), file.getOriginalFilename(), images);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		imagesService.saveImages(images);
		return ResponseEntity.ok("success");	
	}

}
