package com.phonemyfood.user.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.phonemyfood.db.Images;
import com.phonemyfood.db.User;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.ImagesService;
import com.phonemyfood.util.ImageUtils;

@Controller
@RequestMapping(value = "/user/")
public class ImageController {

	@Value("${phonemyfood.images.common.path}")
	private String commonPath;
	
	@Autowired
	private ImageUtils imageUtils;
	
	@Autowired
	LoginUserDetail loginUserDetail;

	@Autowired
	private ImagesService imagesService;

	@RequestMapping(value = "Image/image/{id}")
	public ResponseEntity<byte[]> getImage1(@PathVariable("id") Long id) throws IOException {
		Images images = imagesService.findOneByid(id);
		File f = null;
		File info = imageUtils.readFile(images);
		byte[] imageContent = null;
		try {
			f = new File(info.getPath() + File.separator + images.getPicture());
			InputStream in1 = new FileInputStream(f);
			imageContent = IOUtils.toByteArray(in1);
			in1.close();
		} catch (IOException e) {
			System.out.println("Error: " + e);
		} catch (NullPointerException e) {
			System.out.println("Error: " + e + " File Not Found From Device");
		}
		final HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<byte[]>(imageContent, headers, HttpStatus.OK);
	}

	@RequestMapping(value = "Image/delete_profile_image", method = RequestMethod.POST)
	public ResponseEntity<?> delete_profile_image(Model model, @RequestParam(value = "i_id") Long Id) {
		Images images = imagesService.findOneByid(Id);
		User user = loginUserDetail.getCurrentLoginUser();
		imageUtils.deleteFile(images);
		String[] strings = images.getPicture().split("[.]");
		File f = new File(commonPath + File.separator + "images" + File.separator + user.getId() + File.separator + images.getImageableType() +File.separator + images.getPicture());
		File f1 = new File(commonPath + File.separator + "images" + File.separator + user.getId() + File.separator + images.getImageableType()+File.separator + strings[0]+"-500x500."+strings[1]);
		f.delete();
		f1.delete();
		if (true) {
			imagesService.deleteImage(images);
		}
		return ResponseEntity.ok(true);
	}
}
