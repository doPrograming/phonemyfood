package com.phonemyfood.user.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.phonemyfood.db.Images;
import com.phonemyfood.db.User;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.ImagesService;
import com.phonemyfood.util.ImageUtils;

@Controller
@RequestMapping(value = "/user")
public class ClientUploadController {

	@Value("${phonemyfood.images.common.path}")
	private String commonPath;
	
	@Autowired
	private ImageUtils imageUtils;

	@Autowired
	private ImagesService imagesService;

	@Autowired
	private LoginUserDetail loginUserDetail;

	@RequestMapping(value = "/clientUpload")
	public String clientUpload(Model model) {
		User user = loginUserDetail.getCurrentLoginUser();
		List<Images> images = imagesService.getImagesByTypeAndUser("ClientUpload", user);
		model.addAttribute("images", images);
		return "site/clientUpload";
	}
	
	@PostMapping(value = "/clientUpload/create_image")
	public ResponseEntity<?> upload(@RequestParam("picture") MultipartFile file, HttpServletRequest request) {
		User user = loginUserDetail.getCurrentLoginUser();
		Images images;
		Long persistId;
		images = new Images();
		persistId = imagesService.insert(images);
		images.setImageableType("ClientUpload");
		images.setUser(user);
		images = imageUtils.generateURL(images);
		images.setId(persistId);
		try {
			images = imageUtils.uploadFile(file.getBytes(), file.getOriginalFilename(), images);
		} catch (IOException e) {
			e.printStackTrace();
		}
		imagesService.saveImages(images);
		return new ResponseEntity<>("Success", HttpStatus.OK);
	}

/*	@RequestMapping(value = "/clientUpload/download_image")
	public ResponseEntity<?> downloadImage(@RequestParam("id") Long id) {
		Images image = imagesService.findOneByid(id);
		String inputFile = commonPath+"\\images\\2\\ClientUpload\\" + image.getPicture();
		File file=new File(inputFile);
		Path out=Paths.get(System.getProperty("user.home")+"\\Downloads\\downloaded.jpg");
		try {
			FileInputStream fis=new FileInputStream(file);
			if(fis != null)
				
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			BufferedImage bi = (BufferedImage) ImageIO.read(new File(inputFile));
			ImageIO.write(bi, "jpg", new File(System.getProperty("user.home")+"\\Downloads\\downloaded.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>("success", HttpStatus.OK);
	}*/
	
	@RequestMapping(value = "/clientUpload/download_image")
	public ResponseEntity<?> downloadImage(@RequestParam("id") Long id) {
		User user = loginUserDetail.getCurrentLoginUser();
		Images image = imagesService.findOneByid(id);
		OutputStream os;
		InputStream is;
		try {
			String str = new String("file:///" + commonPath + "\\images\\" + user.getId() + "\\ClientUpload\\" + image.getPicture());
			URL url = new URL(str);
			String out = new String(System.getProperty("user.home") + "\\Downloads\\" + image.getPicture());
			is = url.openStream();
			os = new FileOutputStream(out);
			byte[] buffer = new byte[2048];
			int length = 0;
			while ((length = is.read(buffer)) != -1) {
				os.write(buffer, 0, length);
			}
			is.close();
			os.close();
		} catch (IOException e) {
			System.out.println(e);
		}
		String str = commonPath + File.separator + "images" + File.separator + user.getId() + File.separator
				+ "ClientUpload" + File.separator + image.getPicture();
		return new ResponseEntity<>(str, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/clientUpload/download", method = RequestMethod.GET, produces = "application/image")
	public @ResponseBody void download(@RequestParam("id") Long id, HttpServletResponse response) throws Exception {
		User user = loginUserDetail.getCurrentLoginUser();
		Images image = imagesService.findOneByid(id);
		String filePath = new String(commonPath + File.separator + "images" + File.separator + user.getId()
				+ File.separator + "ClientUpload" + File.separator + image.getPicture());
		File file = new File(filePath);
		InputStream in = new FileInputStream(file);
		response.setContentType("application/image");
		response.setHeader("Content-Disposition", "attachment; filename=" + image.getPicture());
		response.setHeader("Content-Length", String.valueOf(file.length()));
		FileCopyUtils.copy(in, response.getOutputStream());
	}
}
