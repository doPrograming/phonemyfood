package com.phonemyfood.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.phonemyfood.db.Ingredients;
import com.phonemyfood.service.IngredientService;

@Controller
@RequestMapping(value="/user/")
public class IngredientController {
	
	@Autowired
	private IngredientService ingredientService;
	
	@RequestMapping(value="ingredients")
	public String ingredients(Model model) {		
		List<Ingredients> ingredients = ingredientService.getAllIngredientsByUser();
		Ingredients ingredientModal = new Ingredients();
		model.addAttribute("IngredientsList", ingredients);
		model.addAttribute("ingredientModal", ingredientModal);
		return "site/ingredient";
	}
	
	@RequestMapping(value = "/ingredients/ingredientDeleteModal")
	public String getIngredientToDelete(@RequestParam(value="ingredient_id")Long id, Model model) {	
		Ingredients ingredients=ingredientService.findOne(id);
		model.addAttribute("ingredients",ingredients);
		return "site/modal/ingredientDeleteModal";
	}
	
	@RequestMapping(value="ingredients/add_edit_items", method=RequestMethod.POST)
	public String addEditIngredients(@Valid @ModelAttribute("ingredientModal") Ingredients ingredients, Model model){
		ingredientService.saveIngredient(ingredients);
		return "redirect:/user/ingredients";
	}
	
	@RequestMapping(value="ingredients/add_ingredients", method=RequestMethod.POST)
	public String addIngredients(@RequestParam("i_name") String i_name,@RequestParam("i_price") String i_price,@RequestParam("i_id") long i_id,Model model){
		Ingredients ingredients = new Ingredients();
		ingredients.setId(i_id);
		ingredients.setPrice(i_price);
		ingredients.setName(i_name);
		ingredientService.saveIngredient(ingredients);
		List<Ingredients> ingredientsList = ingredientService.getAllIngredientsByUser();
		model.addAttribute("IngredientsList", ingredientsList);
		return "site/ingredientLoad";
	}
	
	@RequestMapping(value="ingredients/delete_ingredient", method=RequestMethod.POST)
	public String deleteIngredients(Model model, @RequestParam(value = "i_id") Long id){
		ingredientService.deleteIngredient(id);
		List<Ingredients> ingredients = ingredientService.getAllIngredientsByUser();
		model.addAttribute("IngredientsList", ingredients);
		return "site/ingredientLoad";
	}
	
	@RequestMapping(value="searchIngredient", method=RequestMethod.GET)
	public ResponseEntity<?> searchIngredient(@RequestParam(value = "key") String key) {
		List<Ingredients> ingredients;
		if(key.isEmpty())
			ingredients = ingredientService.getAllIngredientsByUser();
		else
			ingredients = ingredientService.findIngredientByUserAndName(key);
		return ResponseEntity.ok(ingredients);
	}
}
