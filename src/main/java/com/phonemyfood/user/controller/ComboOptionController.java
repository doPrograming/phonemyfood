package com.phonemyfood.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.phonemyfood.db.ComboOptions;
import com.phonemyfood.db.Items;
import com.phonemyfood.db.vo.ComboOptionVO;
import com.phonemyfood.repository.PriceAndCombosRepository;
import com.phonemyfood.service.ComboOptionsService;
import com.phonemyfood.service.ComboOptionsVOService;

@Controller
@RequestMapping(value="/user/")
public class ComboOptionController {
	
	@Autowired
	private ComboOptionsService comboOptionsService;
	
	@Autowired
	private ComboOptionsVOService comboOptionsVOService;
	
	@Autowired
	private PriceAndCombosRepository priceAndCombosRepository;
	
	@RequestMapping(value="combo/getComboById")
	public String getComboModal(Model model,@RequestParam(value = "c_id") Long id){
		/*ComboOptions comboOptions = comboOptionsService.findOneByPriceAndCombos(id);*/
		String[] strings = {"0"};
		ComboOptionVO comboOptionVO = comboOptionsVOService.getAllComboDetails(id);
		model.addAttribute("comboOptionVO", comboOptionVO);
		model.addAttribute("selectedOption1", comboOptionVO.getComboOptions()==null? strings : comboOptionVO.getComboOptions().getOption1().split(","));
		model.addAttribute("selectedOption2", comboOptionVO.getComboOptions()==null? strings : comboOptionVO.getComboOptions().getOption2().split(","));
		model.addAttribute("selectedOption3", comboOptionVO.getComboOptions()==null? strings : comboOptionVO.getComboOptions().getOption3().split(","));
		model.addAttribute("selectedOption4", comboOptionVO.getComboOptions()==null? strings : comboOptionVO.getComboOptions().getOption4().split(","));
		return "site/modal/comboOptionModal";		
	}
	
	@RequestMapping(value="combo/saveCombo")
	public String saveComboOptions(Model model, @ModelAttribute("comboOptionVO") @Valid ComboOptionVO comboOptionVO){
		ComboOptions comboOptions = comboOptionVO.getComboOptions();
		comboOptions.setPriceCombos(priceAndCombosRepository.getOne(comboOptionVO.getPriceAndComboId()));
		List<Items> option1 = comboOptionVO.getOption1();
		List<Items> option2 = comboOptionVO.getOption2();
		List<Items> option3 = comboOptionVO.getOption3();
		List<Items> option4 = comboOptionVO.getOption4();
		StringBuilder string1 = new StringBuilder("");
		StringBuilder string2 = new StringBuilder("");
		StringBuilder string3 = new StringBuilder("");
		StringBuilder string4 = new StringBuilder("");
		if(option1!=null){
			for(Items items: option1){
				string1.append(String.valueOf(items.getId()));
				string1.append(",");
			}
			string1.deleteCharAt(string1.length()-1);
		}
		if(option2!=null){
			string2 = new StringBuilder("");
			for(Items items: option2){
				string2.append(String.valueOf(items.getId()));
				string2.append(",");
			}
			string2.deleteCharAt(string2.length()-1);
		}
		if(option3!=null){
			string3 = new StringBuilder("");
			for(Items items: option3){
				string3.append(String.valueOf(items.getId()));
				string3.append(",");
			}
			string3.deleteCharAt(string3.length()-1);
		}
		if(option4!=null){
			string4 = new StringBuilder("");
			for(Items items: option4){
				string4.append(String.valueOf(items.getId()));
				string4.append(",");
			}
			string4.deleteCharAt(string4.length()-1);
		}
		comboOptions.setOption1(string1.toString());
		comboOptions.setOption2(string2.toString());
		comboOptions.setOption3(string3.toString());
		comboOptions.setOption4(string4.toString());
		comboOptionsService.saveCombo(comboOptions);
		return "redirect:/user/item";
	}
}
