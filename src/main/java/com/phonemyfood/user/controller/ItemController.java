package com.phonemyfood.user.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.phonemyfood.db.Images;
import com.phonemyfood.db.Ingredients;
import com.phonemyfood.db.Items;
import com.phonemyfood.db.PriceAndCombos;
import com.phonemyfood.db.User;
import com.phonemyfood.db.UserMenu;
import com.phonemyfood.db.vo.DaysAndHoursVO;
import com.phonemyfood.db.vo.IngredientsVO;
import com.phonemyfood.db.vo.ItemVo;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.ImagesService;
import com.phonemyfood.service.IngredientService;
import com.phonemyfood.service.ItemVOService;
import com.phonemyfood.service.ItemsService;
import com.phonemyfood.service.PriceAndCombosService;
import com.phonemyfood.service.UserMenusService;
import com.phonemyfood.util.ImageUtils;

@Controller
@RequestMapping(value="/user/")
public class ItemController extends BaseController{
	
	@Value("${phonemyfood.images.common.path}")
	private String commonPath;
	
	@Autowired
	private ItemVOService itemVOService;
	
	@Autowired
	private IngredientService ingredientService;
	
	@Autowired
	private ItemsService itemsService;
	
	@Autowired
	private ImageUtils imageUtils;
	
	@Autowired
	private ImagesService imagesService;
	
	@Autowired
	private LoginUserDetail loginUserDetail;
	
	@Autowired
	private UserMenusService userMenusService;
	
	@Autowired
	private PriceAndCombosService priceAndCombosService;
	
	@RequestMapping(value="item")
	public String category(Model model) {		
		ItemVo itemVos = itemVOService.getAllItemDetail();
		List<Items> items = itemVOService.getAllItemByUser();
		Collections.sort(items,new SortItemsComparator());
		List<UserMenu> userMenus = userMenusService.findAllByUser(loginUserDetail.getCurrentLoginUser());
		model.addAttribute("userMenus", userMenus);
		model.addAttribute("itemDetails", itemVos);
		model.addAttribute("itemList", items);
		return "site/item";
	}
	
	@RequestMapping(value="item/add_edit_items", method=RequestMethod.POST)
	public String add_edit_items(@ModelAttribute("itemDetails") ItemVo itemVo){
		itemVOService.saveAllItemsDetails(itemVo);
		return "redirect:/user/item";
	}
	
	@RequestMapping(value="item/get_selected_item")
	public String get_selected_item(Model model, @ModelAttribute(value = "i_id") Long id){
		ItemVo itemVos = itemVOService.getSelectedItemDetail(id);
		DaysAndHoursVO daysAndHoursVO = itemVOService.prepareDHModal(id);
		List<UserMenu> userMenus = userMenusService.findAllByUser(loginUserDetail.getCurrentLoginUser());
		model.addAttribute("itemDetails", itemVos);
		model.addAttribute("userMenus", userMenus);
		model.addAttribute("daysAndHoursVO", daysAndHoursVO);
		return "site/UpdateItem";
	}
	
	@RequestMapping(value="/item/update_items")
	public String updateItemsByIngredients(Model model, @ModelAttribute("ingredientsVO") @Valid IngredientsVO ingredientsVO){
		Items items = itemsService.getSelectedItemDetail(ingredientsVO.getItemId());
		List<Ingredients> ingredients = ingredientsVO.getIngredients();
		StringBuilder string = new StringBuilder("");
		if(ingredients!=null){
			for(Ingredients ingredients2: ingredients){
				string.append(String.valueOf(ingredients2.getId()));
				string.append(",");
			}
			string.deleteCharAt(string.length()-1);
		}
		items.setIngridientItem(string.toString());
		itemsService.saveitem(items);
		return "redirect:/user/item";	
	}
	
	@RequestMapping(value="item/ingredientsbyuser", method=RequestMethod.POST)
	public String ingredientByUser(Model model, @RequestParam(value = "i_id") Long id){
		IngredientsVO ingredientsVO = new IngredientsVO();
		List<Ingredients> ingredients = ingredientService.getAllIngredientsByUser();
		Items items = itemsService.getSelectedItemDetail(id);
		ingredientsVO.setIngredients(ingredients);
		ingredientsVO.setItemId(id);
		model.addAttribute("ingredientsVO", ingredientsVO);
		model.addAttribute("selectedItems", items.getIngridientItem().split(","));
		return "site/itemIngredients";
	}
	
	@RequestMapping(value="item/search_item_value")
	public ResponseEntity<?> search_item_value(@RequestParam(value = "key") String key) {
		List<Items> items ;
		if (key.isEmpty()) {
			items = itemsService.findAllByUser(loginUserDetail.getCurrentLoginUser());
		} else {
			items = itemsService.findAllByUserAndName(loginUserDetail.getCurrentLoginUser(),key.toUpperCase());
		}
		return ResponseEntity.ok(items);
	}
	
	
	@RequestMapping(value="Item/add_update_DH")
	public String openDayAndHours(Model model,@ModelAttribute("daysAndHoursVO") @Valid DaysAndHoursVO daysAndHoursVO, RedirectAttributes redirectAttributes) {		
		try {
			itemVOService.saveAllDHItem(daysAndHoursVO);
			redirectAttributes.addFlashAttribute("successMsg", "Successfull");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("errorMsg", "Something went wrong.");
		}
		return "redirect:/user/item";
	}
	
	@RequestMapping(value="item/get_item_image")
	public String itemImageModal(Model model, @RequestParam(value = "item_id") Long Id){
		User user = loginUserDetail.getCurrentLoginUser(); 
		List<Images> images = imagesService.getImagesByImageableTypeAndImageableIdAndUser("Item",Id,user);
		model.addAttribute("images", images);
		return "site/modal/itemImageModal";
	}
	 
	@RequestMapping(value="item/create_item_image", method=RequestMethod.POST)
	public ResponseEntity<?> specialsImage(@RequestParam("picture") MultipartFile file, @RequestParam("id") Long id, HttpServletRequest request){
		BufferedImage image=null;
		User user = loginUserDetail.getCurrentLoginUser();
		Integer height=0,width=0;
		try {
			image = ImageIO.read(file.getInputStream());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Images image1 = imagesService.getImagesByImageableTypeAndImageableId("Item",id,user);
		
		width = image.getWidth();
		height = image.getHeight();
		if ((width < 1500 || height < 1000) || (width > 1500 && height > 1000 && (2 * width != 3 * height))) {
			return new ResponseEntity<>(width + "----" + height + " not Accepted",HttpStatus.BAD_REQUEST);
		}
		Images images = new Images();
		Long persistId = 0l;
		if(image1!=null) {
			String[] strings = image1.getPicture().split("[.]");
			images.setId(image1.getId());
			File f = new File(commonPath + File.separator + "images" + File.separator + user.getId() + File.separator + "Item" +File.separator + image1.getPicture());
			File f1 = new File(commonPath + File.separator + "images" + File.separator + user.getId() + File.separator + "Item" +File.separator + strings[0]+"-500x500."+strings[1]);
			f.delete();
			f1.delete();
		}
		else {
			persistId=imagesService.insert(images);
			images.setId(persistId);
		}
	 
		images.setImageableId(id);
		images.setImageableType("Item");
		images.setUser(user);
		images =  imageUtils.generateURL(images);
		
		try {
			images = imageUtils.uploadImage(file.getBytes(), file.getOriginalFilename(), images);
		} catch (IOException e) {
			e.printStackTrace();
		}
		imagesService.saveImages(images);
		return ResponseEntity.ok("success");	
	}
	
	@RequestMapping(value="item/find_price_and_combos", method=RequestMethod.GET)
	public ResponseEntity<?> find_price_and_combos(@RequestParam("menuid") long menuid, @RequestParam("itemid") long itemid) {
		List<PriceAndCombos> priceAndCombos = priceAndCombosService.findAllByItemAndUserMenuId(itemsService.getOne(itemid),menuid);
		return new ResponseEntity<>(priceAndCombos,HttpStatus.OK);
	}
	
	@RequestMapping(value="item/itemDeleteModal", method=RequestMethod.GET)
	public String itemDeleteModal(@RequestParam("itemId") long itemId, Model model) {
		model.addAttribute("item",itemsService.getOne(itemId));
		return "site/modal/itemDeleteModal";
	}
	
	@RequestMapping(value="item/deleteItem", method=RequestMethod.GET)
	public ResponseEntity<?> deleteItem(@RequestParam("itemId") long itemId) {
		itemsService.deleteItem(itemsService.getOne(itemId));
		return new ResponseEntity<>("done",HttpStatus.OK);
	}
}