package com.phonemyfood.user.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.phonemyfood.db.UserDeliveries;
import com.phonemyfood.db.vo.UserDeliveriesVO;
import com.phonemyfood.service.UserDeliveryService;

@Controller
@RequestMapping(value="/user")
public class UserDeliveryController {
	
	@Autowired
	private UserDeliveryService userDeliveryService;
	
	@RequestMapping({"/delivery"})
	public String delivery(Model model) {
		UserDeliveriesVO userDeliveriesVO = new UserDeliveriesVO();
		List<UserDeliveries> userDeliveries = userDeliveryService.getAllUserDelivery();
		for(int i = 0;i<3;i++){
			try {
				userDeliveries.get(i);
			} catch (IndexOutOfBoundsException e) {
				// TODO: handle exception
				userDeliveries.add(new UserDeliveries());
			}
		}
		userDeliveriesVO.setUserDeliveries(userDeliveries);
		model.addAttribute("userDeliveries", userDeliveries);
		model.addAttribute("userDeliveriesVO", userDeliveriesVO);
		return "site/delivery";
	}
	
	@RequestMapping(value="/delivery/save_userdeliveries", method= RequestMethod.POST)
	public String save_userdeliveries(@RequestParam(name="userDeliveries[0]?.deliveriesMiles")String miles0,
			@RequestParam(name="userDeliveries[0]?.price")String price0,
			@RequestParam(name="userDeliveries[0]?.id")Long id0,
			@RequestParam(name="userDeliveries[1]?.deliveriesMiles")String miles1,
			@RequestParam(name="userDeliveries[1]?.price")String price1,
			@RequestParam(name="userDeliveries[1]?.id")Long id1,
			@RequestParam(name="userDeliveries[2]?.deliveriesMiles")String miles2,
			@RequestParam(name="userDeliveries[2]?.price")String price2,
			@RequestParam(name="userDeliveries[2]?.id")Long id2){
		List<UserDeliveries> userDeliveries = new ArrayList<UserDeliveries>();
		UserDeliveries deliveries;
		if( id0 != 0){
			if(miles0 != "" && price0 != "") {
				deliveries=userDeliveryService.getOne(id0);
				deliveries.setDeliveriesMiles(miles0);
				deliveries.setPrice(price0);
				userDeliveries.add(deliveries);
			}
			else {
				deliveries=userDeliveryService.getOne(id0);
				deliveries.setDeliveriesMiles("");
				deliveries.setPrice("");
			}
		}
		else if(id0 == 0) {
			if(miles0 != "" && price0 != "") {
				deliveries = new UserDeliveries();
				deliveries.setDeliveriesMiles(miles0);
				deliveries.setPrice(price0);
				userDeliveries.add(deliveries);
			}
		}
		if( id1 != 0){
			if(miles1 != "" && price1 != "") {
				deliveries=userDeliveryService.getOne(id1);
				deliveries.setDeliveriesMiles(miles1);
				deliveries.setPrice(price1);
				userDeliveries.add(deliveries);
			}
			else {
				deliveries=userDeliveryService.getOne(id1);
				deliveries.setDeliveriesMiles("");
				deliveries.setPrice("");
			}
		}
		else if(id1 == 0) {
			if(miles1 != "" && price1 != "") {
				deliveries = new UserDeliveries();
				deliveries.setDeliveriesMiles(miles1);
				deliveries.setPrice(price1);
				userDeliveries.add(deliveries);
			}
		}
		if( id2 != 0){
			if(miles2 != "" && price2 != "") {
				deliveries=userDeliveryService.getOne(id2);
				deliveries.setDeliveriesMiles(miles2);
				deliveries.setPrice(price2);
				userDeliveries.add(deliveries);
			}
			else {
				deliveries=userDeliveryService.getOne(id2);
				deliveries.setDeliveriesMiles("");
				deliveries.setPrice("");
			}
		}
		else if(id2 == 0) {
			if(miles2 != "" && price1 != "") {
				deliveries = new UserDeliveries();
				deliveries.setDeliveriesMiles(miles2);
				deliveries.setPrice(price2);
				userDeliveries.add(deliveries);
			}
		}
				
		userDeliveryService.saveAll(userDeliveries);
		return "redirect:/user/delivery";
	}
}
