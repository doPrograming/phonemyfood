package com.phonemyfood.user.controller;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.phonemyfood.db.Categories;
import com.phonemyfood.db.Items;
import com.phonemyfood.db.MenuCategoryItem;
import com.phonemyfood.db.User;
import com.phonemyfood.db.UserMenu;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.CategoriesService;
import com.phonemyfood.service.DaysAndHoursVOService;
import com.phonemyfood.service.ItemsService;
import com.phonemyfood.service.UserMenusService;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	@Autowired
	private LoginUserDetail loginUserDetail;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private UserMenusService userMenusService;

	@Autowired
	private CategoriesService categoriesService;

	@Autowired
	private ItemsService itemsService;
	
	@Autowired
	private DaysAndHoursVOService daysAndHoursVOService;

	@RequestMapping({ "/" })
	public String login(@PageableDefault(direction = Direction.DESC, value = 3, sort = "id") Pageable pageable) {
		//if (((ArrayList<UserMenu>) httpSession.getAttribute("MenuList")).isEmpty()) {
			//Page<UserMenu> userMenus = userMenusService.findLast3ByUser(loginUserDetail.getCurrentLoginUser(), pageable);
		List<UserMenu> arrayList = userMenusService.findAllByUser(loginUserDetail.getCurrentLoginUser());
		Collections.reverse(arrayList);
		if (daysAndHoursVOService.isBusinessHours()) {
			httpSession.setAttribute("AddMenuFlag",true);
			httpSession.setAttribute("MenuList",arrayList);
		}else {
			httpSession.removeAttribute("AddMenuFlag");
			httpSession.removeAttribute("MenuList");
		}
		return "site/index";
	}

	@RequestMapping({ "/editMenu" })
	public String editMenu(Model model) {
		User user = loginUserDetail.getCurrentLoginUser();
		List<UserMenu> userMenus = userMenusService.findAllByUser(user);
		List<Categories> categories = categoriesService.getAllCategoryByUser(user);
		List<Items> items = itemsService.getAllItems(user);
		Collections.sort(items,new SortItemsComparator());
		model.addAttribute("menuCategoryItem", new MenuCategoryItem());
		model.addAttribute("userMenus", userMenus);
		model.addAttribute("categories", categories);
		model.addAttribute("items", items);
		return "site/EditMenu";
	}
}
