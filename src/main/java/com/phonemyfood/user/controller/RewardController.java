package com.phonemyfood.user.controller;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.phonemyfood.db.Rewards;
import com.phonemyfood.db.vo.RewardText;
import com.phonemyfood.db.vo.RewardsVO;
import com.phonemyfood.service.RewardsService;

@Controller
@RequestMapping(value = "/user")
public class RewardController {

	@Autowired
	private RewardsService rewardsService;

	@RequestMapping(value = "/rewards")
	public String rewards(Model model) {
		RewardsVO rewardsVO = new RewardsVO();
		List<Rewards> rewards = rewardsService.getAllRewards();
		List<Rewards> rewardsEnum = new ArrayList<Rewards>();
		Set<RewardText> name = EnumSet.allOf(RewardText.class);
		for (int i = 0; i < name.size(); i++) {
			try {
				rewards.get(i);
			} catch (IndexOutOfBoundsException e) {
				rewards.add(new Rewards());
			}
		}
		for (RewardText rewardText : name) {
			Rewards rewards2 = new Rewards();
			for (Rewards rewards3 : rewards) {
				if (rewardText.getText().equals(rewards3.getRewardText())) {
					rewards2.setId(rewards3.getId());
					rewards2.setActive(rewards3.isActive());
					rewards2.setRewardText(rewards3.getRewardText());
					rewards2.setRewardValue(rewards3.getRewardValue());
					rewards2.setValue1(rewards3.getValue1());
					break;
				} else {
					rewards2.setActive(false);
					rewards2.setRewardText(rewardText.getText());
					rewards2.setRewardValue(rewardText.getValue());
					rewards2.setValue1(rewards3.getValue1());
				}
			}
			rewardsEnum.add(rewards2);
		}
		rewardsVO.setRewards(rewardsEnum);
		model.addAttribute("Rewards", rewardsVO);
		model.addAttribute("RewardsList", rewards);
		return "site/rewards";
	}

	@RequestMapping(value = "/rewards/update_rewards")
	public String updateRewards(@RequestParam(name="rewards[0].id",required=false)Long reward0,
			@RequestParam(name="rewards[0].rewardText",required=false)String rewardText0,
			@RequestParam(name="rewards[0].rewardValue",required=false)String rewardValue0,
			@RequestParam(name="rewards[1].id",required=false)Long reward1,
			@RequestParam(name="rewards[1].rewardText",required=false)String rewardText1,
			@RequestParam(name="rewards[1].rewardValue",required=false)String rewardValue1,
			@RequestParam(name="rewards[1].rewardValue1",required=false)String rewardValue12,
			@RequestParam(name="rewards[2].id",required=false)Long reward2,
			@RequestParam(name="rewards[2].rewardText",required=false)String rewardText2,
			@RequestParam(name="rewards[2].rewardValue",required=false)String rewardValue2,
			@RequestParam(name="rewards[3].id",required=false)Long reward3,
			@RequestParam(name="rewards[3].rewardText",required=false)String rewardText3,
			@RequestParam(name="rewards[3].rewardValue",required=false)String rewardValue3,
			@RequestParam(name="rewards[4].id",required=false)Long reward4,
			@RequestParam(name="rewards[4].rewardText",required=false)String rewardText4,
			@RequestParam(name="rewards[4].rewardValue",required=false)String rewardValue4,Model model) {
		RewardsVO rewardsVO = new RewardsVO();
		Rewards reward;
		List<Rewards> rewards=new ArrayList<>();
		if(reward0 != null && rewardValue0 != "") {
			reward=rewardsService.getOne(reward0);
			reward.setRewardValue(rewardValue0);
			rewards.add(reward);
		}
		if(reward1 != null && rewardValue1 != "") {
			reward=rewardsService.getOne(reward1);
			reward.setValue1(rewardValue12);
			reward.setRewardValue(rewardValue1);
			rewards.add(reward);
		}
		if(reward2 != null && rewardValue2 != "") {
			reward=rewardsService.getOne(reward2);
			reward.setRewardValue(rewardValue2);
			rewards.add(reward);
		}
		if(reward3 != null && rewardValue3 != "") {
			reward=rewardsService.getOne(reward3);
			reward.setRewardValue(rewardValue3);
			rewards.add(reward);
		}
		if(reward4 != null && rewardValue4 != "") {
			reward=rewardsService.getOne(reward4);
			reward.setRewardValue(rewardValue4);
			rewards.add(reward);
		}
		rewardsVO.setRewards(rewards);
		rewardsService.updateRewards(rewardsVO);
		return "redirect:/user/rewards";
	}
}