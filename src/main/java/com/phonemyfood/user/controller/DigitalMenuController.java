package com.phonemyfood.user.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.phonemyfood.db.ColorPicker;
import com.phonemyfood.db.DigitalMenu;
import com.phonemyfood.db.Images;
import com.phonemyfood.db.Items;
import com.phonemyfood.db.MenuDesign;
import com.phonemyfood.db.MenuType;
import com.phonemyfood.db.Orientation;
import com.phonemyfood.db.User;
import com.phonemyfood.db.UserMenu;
import com.phonemyfood.db.vo.ItemImageVO;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.DigitalMenuService;
import com.phonemyfood.service.ImagesService;
import com.phonemyfood.service.MenuCategoryItemsService;
import com.phonemyfood.service.UserBusinessVOService;
import com.phonemyfood.service.UserMenusService;
import com.phonemyfood.util.ImageUtils;

@Component
@RequestMapping(value="/user/digitalMenu")
public class DigitalMenuController extends BaseController{
	
	@Value("${phonemyfood.images.common.path}")
	private String commonPath;
	 
	@Autowired
	private DigitalMenuService digitalMenuService;
	
	@Autowired
	private LoginUserDetail loginUserDetail;
	
	@Autowired
	UserMenusService userMenusService;
		
	@Autowired
	private MenuCategoryItemsService menuCategoryItemsService;
	
	@Autowired
	private ImagesService imagesService;
	
	@Autowired
	UserBusinessVOService userBusinessVOService;
	
	@Autowired
	private ImageUtils imageUtils;
	
	DecimalFormat df = new DecimalFormat("000");
		
	@RequestMapping(value = "/digitalMenuPage")
	public String digitalmenu(Model model, @RequestParam(name = "action", required = false) String action,HttpServletRequest request) {
		List<DigitalMenu> digitalMenus = digitalMenuService.findAllByBusinesses(findBusinessesByLoginUser());
		List<UserMenu> userMenus = userMenusService.findAllByUser(loginUserDetail.getCurrentLoginUser());
		if(digitalMenus.size()==0) {
			DigitalMenu digitalMenu = new DigitalMenu();
			digitalMenu.setScreen("1");
			digitalMenus.add(digitalMenu);
		}
		model.addAttribute("digitalMenus", digitalMenus);
		model.addAttribute("userMenus", userMenus);
		model.addAttribute("action", action);
		return "admin/digitalMenu";
	}
	
	@PostMapping(value = "/saveDigitalMenu")
	public String save(Model model, @RequestParam(name = "id", required = false) String id,
			@RequestParam(name = "menuType", required = false) String menuType,
			@RequestParam(name = "menuDesign", required = false) String menuDesign,
			@RequestParam(name = "userMenu", required = false) String usermenuid,
			@RequestParam(name = "orientation", required = false) String orientation,
			@RequestParam(name = "layout", required = false) String layout,
			@RequestParam(name = "colorPicker", required = false) String colorPicker,
			@RequestParam(name = "pageDuration", required = false) String pageDuration,
			@RequestParam(name = "photoDuration", required = false) String photoDuration,
			@RequestParam(name = "promomsg1", required = false) String promomsg1,
			@RequestParam(name = "promomsg2", required = false) String promomsg2,
			@RequestParam(name = "promomsg3", required = false) String promomsg3,
			@RequestParam(name = "screenNumber", required = false) String screenNumber,
			@RequestParam(name = "menus", required = false) String userMenuList) {
		List<UserMenu> allUserMenus = userMenusService.findAllByUser(loginUserDetail.getCurrentLoginUser());
		List<UserMenu> userMenus = new ArrayList<>();
		String[] strings = userMenuList.split(",");
		for (int i = 0; i < strings.length; i++) {
		  userMenus.add(userMenusService.getOne(Long.valueOf(strings[i])));
		}
		DigitalMenu digitalMenu = new DigitalMenu();
		digitalMenu.setBusinesses(findBusinessesByLoginUser());
		if (Long.parseLong(id) != 0) {
			digitalMenu.setId(Long.parseLong(id));
		}
		if (Long.parseLong(id) != 0 && menuType == null) {
			digitalMenu = digitalMenuService.getOne(Long.parseLong(id));
			digitalMenu.setLayout(Integer.parseInt(layout));
			digitalMenuService.save(digitalMenu);
			
			model.addAttribute("userMenus", allUserMenus);
			model.addAttribute(digitalMenu);
			model.addAttribute("screenNumber", digitalMenu.getScreen());
			return "site/fragments/tab";
		}
		digitalMenu.setUserMenus(userMenus);
		digitalMenu.setScreen(screenNumber);
		digitalMenu.setMenuType(MenuType.valueOf(menuType));
		digitalMenu.setMenuDesign(MenuDesign.valueOf(menuDesign));
		digitalMenu.setOrientation(Orientation.valueOf(orientation));
		digitalMenu.setLayout(Integer.parseInt(layout));
		digitalMenu.setColorPicker(ColorPicker.valueOf(colorPicker));
		digitalMenu.setPageDuration(Integer.parseInt(pageDuration));
		digitalMenu.setPhotoDuration(Integer.parseInt(photoDuration));
		digitalMenu.setPromomsg1(promomsg1);
		digitalMenu.setPromomsg2(promomsg2);
		digitalMenu.setPromomsg3(promomsg3);
		digitalMenuService.save(digitalMenu);
		//digitalMenuService.setLinkedMenus(digitalMenu);
		model.addAttribute("userMenus", allUserMenus);
		model.addAttribute(digitalMenu);
		model.addAttribute("screenNumber", digitalMenu.getScreen());
		return "site/fragments/tab";
	}
	
	@PostMapping(value = "/saveLinkedDigitalMenu")
	public ResponseEntity<?> saveLinkedDigitalMenu(@RequestParam("digitalMenuId") long digitalMenuId,@RequestParam("layout") int layout) {
		DigitalMenu digitalMenu = digitalMenuService.getOne(digitalMenuId);
		digitalMenu.setLayout(layout);
		digitalMenuService.save(digitalMenu);
		return new ResponseEntity<>("done",HttpStatus.OK);
	}
	
	@PostMapping(value = "/saveDigitalMenuImage")
	public ResponseEntity<?> saveDigitalMenuImage(
			@RequestParam(name = "imageString", required = false) String imageString, @RequestParam(name = "screenNumber", required = false) String screenNumber, 
			@RequestParam(name = "digitalMenuId", required = false) String digitalMenuId, @RequestParam(name = "digitalmenuItemId", required = false) String digitalmenuItemId,
			@RequestParam(name = "menuType", required = false) String menuType, @RequestParam(name = "design", required = false) String design,
			@RequestParam(name = "croppingFrame", required = false) String croppingFrame) {
				design = df.format(Integer.parseInt(design));
				String croppingFramePath = commonPath+ File.separator + "frame" + File.separator + findBusinessesByLoginUser().getBusinessType() + File.separator + menuType  
						+ File.separator + design + File.separator + croppingFrame;
				File file = new File(croppingFramePath);
				if(!file.exists()) {
					croppingFramePath = commonPath + File.separator + "defaultFrame" + File.separator + croppingFrame;
				}
				digitalMenuService.saveDigitalMenuImage(imageString, screenNumber, Long.parseLong(digitalMenuId),digitalmenuItemId,croppingFramePath);
		return new ResponseEntity<>("Success", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addScreen")
	public String addScreen(@RequestParam("screenNumber") String screenNumber,Model model) { 
		DigitalMenu digitalMenu = new DigitalMenu();
		digitalMenu.setScreen(screenNumber);
		digitalMenu.setBusinesses(findBusinessesByLoginUser());
		List<UserMenu> userMenus = userMenusService.findAllByUser(loginUserDetail.getCurrentLoginUser());
		model.addAttribute("digitalMenu",digitalMenu);
		model.addAttribute("screenNumber",digitalMenu.getScreen());
		model.addAttribute("userMenus", userMenus);
		return "site/fragments/tab"; 
	}
	
	@RequestMapping(value = "/digitalMenuPhotoModal")
	public String getDigitalMenuImage(@RequestParam(name = "menus[]", required = false) List<String> userMenuList,
			@RequestParam("menu") String menu, @RequestParam("design") String design,
			@RequestParam("orientation") String orientation, Model model) {
		List<UserMenu> userMenus = new ArrayList<>();
		for (int i = 0; i < userMenuList.size(); i++) {
			userMenus.add(userMenusService.getOne(Long.valueOf(userMenuList.get(i))));
		}
		Set<Items> items = menuCategoryItemsService.findItemsByUserMenus(userMenus);
		List<ItemImageVO> itemImageVOs = new ArrayList<>();
		Images img = new Images();
		for(Items item : items) {
			img = imagesService.getImagesByImageableTypeAndImageableId("Item",item.getId(), loginUserDetail.getCurrentLoginUser());
			ItemImageVO itemImageVO = new ItemImageVO();
			itemImageVO.setItems(item);
			if(img==null||img.equals(null)) {
				itemImageVO.setImageId(0);
			}
			else {
				itemImageVO.setImageId(img.getId());
			}
			itemImageVOs.add(itemImageVO);
		}
		design = df.format(Integer.parseInt(design));
		byte[] imageContent = imageUtils.getInitialFrame(menu, design, orientation,findBusinessesByLoginUser().getBusinessType());
		int numOfFrames = imageUtils.getNumberOfFramesInFolder(menu, design, orientation,findBusinessesByLoginUser().getBusinessType());
		int width = 0, height = 0;
		BufferedImage imageProof = null;
		try {
			imageProof = ImageIO.read(new ByteArrayInputStream(imageContent));
		} catch (IOException e) {
			System.out.println("Error: " + e);
		}
		width = imageProof.getWidth();
		height = imageProof.getHeight();
		model.addAttribute("numOfFrames", numOfFrames);
		model.addAttribute("itemImageVOs", itemImageVOs);
		model.addAttribute("canvasWidth", width);
		model.addAttribute("canvasHeight", height);
		return "site/modal/digitalMenuPhotoModal";
	}

	@RequestMapping(value = "/search_item_value")
	public ResponseEntity<?> search_item_value(@RequestParam(value = "key") String key,
			@RequestParam(name = "menus[]", required = false) List<String> userMenuList) {
		User user = loginUserDetail.getCurrentLoginUser();
		List<UserMenu> userMenus = new ArrayList<>();
		for (int i = 0; i < userMenuList.size(); i++)
			userMenus.add(userMenusService.getOne(Long.valueOf(userMenuList.get(i))));
		Set<Items> items = menuCategoryItemsService.findItemsByUserMenus(userMenus);
		List<Items> result = items.stream().filter(line -> (line.getName().toLowerCase()).contains(key.toLowerCase()))
				.collect(Collectors.toList());
		List<ItemImageVO> itemImageVOS = new ArrayList<>();
		Images images;
		ItemImageVO itemImageVO;
		for (Items item : result) {
			itemImageVO = new ItemImageVO();
			itemImageVO.setItems(item);
			images = imagesService.getImagesByImageableTypeAndImageableId("Item", item.getId(), user);
			if (images != null) {
				itemImageVO.setImageId(images.getId());
			} else {
				itemImageVO.setImageId(0);
			}
			itemImageVOS.add(itemImageVO);
		}
		return new ResponseEntity<>(itemImageVOS, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get_item_images")
	public ResponseEntity<?> get_item_images(@RequestParam("item_id") String item_id, Model model) {
		Images image = imagesService.getImagesByImageableTypeAndImageableId("Item",Long.parseLong(item_id), loginUserDetail.getCurrentLoginUser());
		if (image == null || image.equals(null))
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		//System.out.println("get_item_images = "+ image.getId() +" --- "+image.getPicture());
		return new ResponseEntity<>(image.getId(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/isDigitalMenuImageAvailable")
	public ResponseEntity<?> isDigitalMenuImageAvailable(@RequestParam("picture") String picture,
			@RequestParam("digitalmenuId") String digitalmenuId) {
		Images images = imagesService.getImageByPicturAndImageableId(picture, Long.parseLong(digitalmenuId));
		if (images == null || images.equals(null)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>("available",HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get_image")
	public ResponseEntity<?> get_image(@RequestParam("picture") String picture, @RequestParam("digitalmenuId") String digitalmenuId) {
		Images images = imagesService.getImageByPicturAndImageableId(picture, Long.parseLong(digitalmenuId));
		File info = imageUtils.readFile(images);
		byte[] imageContent = null;
		try {
			File f = null;
			f = new File(info.getPath() + File.separator + images.getPicture());
			InputStream in1 = new FileInputStream(f);
			imageContent = IOUtils.toByteArray(in1);
			in1.close();
		} catch (IOException e) {
			System.out.println("Error: " + e);
		}
		final HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<byte[]>(imageContent, headers, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/digitalMenuLinkModal")
	public String digitalMenuLinkModal(Model model, @RequestParam("screen_num") String screenNumber) {
		List<DigitalMenu> dgmenus = digitalMenuService.getValidDigitalMenusForScreen(screenNumber, findBusinessesByLoginUser());
		Collections.sort(dgmenus, new DigitalMenuComparator());
		model.addAttribute("digitalMenus", dgmenus);
		model.addAttribute("screenNumber", screenNumber);
		return "site/modal/digitalMenuLinkModal";
	}
	
	@RequestMapping(value = "/linkDigitalMenus")
	public String linkDigitalMenus(Model model, @RequestParam(value = "digitalMenuIds[]") List<Integer> ids,
			@RequestParam("currentDigitalMenuId") long c_id) {
		List<Long> long_ids = ids.stream().mapToLong(Integer::intValue).boxed().collect(Collectors.toList());
		digitalMenuService.linkDigitalMenus(long_ids, c_id);
		return "redirect:/user/digitalMenu/digitalMenuPage";
	}
	
	@RequestMapping(value = "/getUpdatedLinkedMenus")
	public String getUpdatedLinkedMenus(Model model,@RequestParam("id")String id) {
		model.addAttribute("userMenus", userMenusService.findAllByUser(loginUserDetail.getCurrentLoginUser()));
		model.addAttribute("digitalMenu",digitalMenuService.getOne(Long.parseLong(id)));
		model.addAttribute("screenNumber", digitalMenuService.getOne(Long.parseLong(id)).getScreen());
		return "site/fragments/tab";
	}
	
	@RequestMapping(value = "/digitalMenuDeleteModal")
	public String deleteTab(@RequestParam("tab")String str,Model model) {
		model.addAttribute("tab", str);
		return "site/modal/digitalMenuDeleteModal";
	}
	
	@RequestMapping(value = "/deleteDigitalMenu")
	public ResponseEntity<?> deleteDigitalMenu(@RequestParam(value = "mid") Long id) {
		digitalMenuService.deleteDigitalMenu(id);
		return ResponseEntity.ok("Deleted successfully.");
	}

	@RequestMapping(value = "/getFrame")
	public ResponseEntity<byte[]> getFrame(@RequestParam("orientation") String orientation,
			@RequestParam("menu") String menu, @RequestParam("design") String design, @RequestParam("name") String name) throws IOException {
		design = df.format(Integer.parseInt(design));
		byte[] imageContent = imageUtils.getFrame(menu, design, orientation, name);
		final HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<byte[]>(imageContent, headers, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getInitialFrame")
	public ResponseEntity<byte[]> getInitialFrame(@RequestParam("orientation") String orientation,
			@RequestParam("menu") String menu, @RequestParam("design") String design) throws IOException {
		design = df.format(Integer.parseInt(design));
		byte[] imageContent = imageUtils.getInitialFrame(menu, design, orientation,findBusinessesByLoginUser().getBusinessType());
		final HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<byte[]>(imageContent, headers, HttpStatus.OK);
	}
}