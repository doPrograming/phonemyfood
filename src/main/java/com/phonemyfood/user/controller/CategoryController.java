package com.phonemyfood.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.phonemyfood.db.Categories;
import com.phonemyfood.db.User;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.CategoriesService;

@Controller
@RequestMapping(value = "/user")
public class CategoryController {

	@Autowired
	private CategoriesService categoriesService;

	@Autowired
	private LoginUserDetail loginUserDetail;

	@RequestMapping({ "/category" })
	public String category(Model model) {
		User user = loginUserDetail.getCurrentLoginUser();
		List<Categories> categories = categoriesService.getAllCategoryByUser(user);
		model.addAttribute("categories", categories);
		return "site/category";
	}
	
	@RequestMapping(value = "/category/categoryDeleteModal")
	public String getCategoryToDelete(@RequestParam(value="category_id")long id,Model model) {
		Categories categories = categoriesService.getOne(id);
		model.addAttribute("categories", categories);
		return "site/modal/categoryDeleteModal";
	}

	@RequestMapping(value = "/category/get_category_byId")
	public String getCategoryByid(Model model, @RequestParam(value = "category_id") Long id) {
		Categories categories = categoriesService.getOne(id);
		model.addAttribute("CategoryMsg", categories);
		return "site/modal/categoryMsgModal";
	}

	@RequestMapping(value = "/category/edit_category")
	public String editCategory(@ModelAttribute("categoryMsg") @Valid Categories categories, Model model) {
		User user = loginUserDetail.getCurrentLoginUser();
		categories.setUser(user);
		categoriesService.save(categories);
		return "redirect:/user/category";
	}

	@RequestMapping(value = "/category/delete", method = RequestMethod.GET)
	public ResponseEntity<?> deleteCategory(@RequestParam(value = "cid") Integer id) {
		String message = categoriesService.deleteCategory(Long.valueOf(id));
		return ResponseEntity.ok(message);
	}

	@RequestMapping(value = "/category/addCategory", method = RequestMethod.POST)
	public ResponseEntity<?> addCategory(@RequestParam(value = "id", defaultValue = "0") Long id,
			@RequestParam(value = "name") String name) {
		User user = loginUserDetail.getCurrentLoginUser();
		List<Categories> categories1 = categoriesService.categoryExist(user, name);
		if (categoriesService.categoryExist(user, name).size()!=0) {
			for(int i = 0;i<categories1.size();i++) {
				if(id!=0 && categories1.get(i).getName().equalsIgnoreCase(name))
					categoriesService.save(new Categories(),name,id,user);
				else
					return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}
		categoriesService.save(new Categories(),name,id,user);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping(value="/category/add_new_category")
	public ResponseEntity<?> add_new_category(@RequestParam("c_name") String categoryName){
		User user = loginUserDetail.getCurrentLoginUser();
		System.out.println("size is "+categoriesService.categoryExist(user, categoryName).size());
		if (categoriesService.categoryExist(user, categoryName).size()!=0) {
			System.out.println("category name = " + categoryName);
			return new ResponseEntity<>("exist", HttpStatus.OK);
		}
		Categories categories = new Categories();
		categories.setName(categoryName);
		categories.setUser(user);
		categoriesService.save(categories);
		return new ResponseEntity<>(categories,HttpStatus.OK);		
	}

}
