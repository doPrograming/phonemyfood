package com.phonemyfood.user.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.phonemyfood.db.DaysAndHours;
import com.phonemyfood.db.UserMenu;
import com.phonemyfood.db.vo.UserMenuVO;
import com.phonemyfood.repository.DayAndHoursRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.DaysAndHoursVOService;
import com.phonemyfood.service.UserMenuVOService;
import com.phonemyfood.service.UserMenusService;
import com.phonemyfood.util.Utility;

@Controller
@RequestMapping(value = "/user")
public class UserMenuController {
	@Autowired
	private LoginUserDetail loginUserDetail;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private UserMenuVOService userMenuVOService;

	@Autowired
	DayAndHoursRepository dayAndHoursRepository;

	@Autowired
	private UserMenusService userMenusService;

	@Autowired
	private DaysAndHoursVOService daysAndHoursVOService;

	@RequestMapping({ "/addMenu" })
	public String addMenu(Model model) {
		UserMenuVO userMenuVO = userMenuVOService.prepareUserMenuVO();
		model.addAttribute("userMenuVO", userMenuVO);
		return "site/menu";
	}

	@RequestMapping(value = "/menu/add_menu", method = RequestMethod.POST)
	public ResponseEntity<?> saveMenu(@RequestParam(name = "userMenu.id", required = false) String id,
			@RequestParam(name = "userMenu.menuName") String menuName, @RequestParam(name = "daysAndHours[0].startTime") String monStart,
			@RequestParam(name = "daysAndHours[0].endTime") String monEnd, @RequestParam(name = "daysAndHours[1].startTime") String tueStart,
			@RequestParam(name = "daysAndHours[1].endTime") String tueEnd, @RequestParam(name = "daysAndHours[2].startTime") String wedStart,
			@RequestParam(name = "daysAndHours[2].endTime") String wedEnd, @RequestParam(name = "daysAndHours[3].startTime") String thuStart,
			@RequestParam(name = "daysAndHours[3].endTime") String thuEnd, @RequestParam(name = "daysAndHours[4].startTime") String friStart,
			@RequestParam(name = "daysAndHours[4].endTime") String friEnd, @RequestParam(name = "daysAndHours[5].startTime") String satStart,
			@RequestParam(name = "daysAndHours[5].endTime") String satEnd, @RequestParam(name = "daysAndHours[6].startTime") String sunStart,
			@RequestParam(name = "daysAndHours[6].endTime") String sunEnd, @RequestParam(name = "daysAndHours[7].startTime") String allStart,
			@RequestParam(name = "daysAndHours[7].endTime") String allEnd) {
		String status = "";
		String startTimes[] = { monStart, tueStart, wedStart, thuStart, friStart, satStart, sunStart, allStart };
		String endTimes[] = { monEnd, tueEnd, wedEnd, thuEnd, friEnd, satEnd, sunEnd, allEnd };
		DaysAndHours daysAndHour = null;
		UserMenuVO userMenuVO = new UserMenuVO();
		List<DaysAndHours> daysAndHours = new ArrayList<>();
		UserMenu userMenu = Long.parseLong(id) == 0 ? new UserMenu() : userMenusService.getOne(Long.parseLong(id));
		if (userMenu != null) {
			userMenu.setMenuName(menuName);
			userMenuVO.setUserMenu(userMenu);

			for (int i = 0; i <= 7; i++) {
				if (startTimes[i] != "" && endTimes[i] != "") {
					if (Utility.validateTime(startTimes[i], endTimes[i])) {
						if (daysAndHoursVOService.isEqualToBusinessHours(startTimes[i], endTimes[i], i)) {
							daysAndHour = new DaysAndHours();
							daysAndHour.setStartTime(startTimes[i]);
							daysAndHour.setEndTime(endTimes[i]);
							daysAndHours.add(daysAndHour);
							userMenuVO.setDaysAndHours(daysAndHours);
						} else {
							return new ResponseEntity<>("Please select a Time when you are open", HttpStatus.BAD_REQUEST);
						}
					} else {
						return new ResponseEntity<>("Time Should Be Between 9:00 AM To 9:00 PM", HttpStatus.BAD_REQUEST);
					}
				} else if (startTimes[i] == "" && endTimes[i] == "") {
					daysAndHour = new DaysAndHours();
					daysAndHour.setStartTime("");
					daysAndHour.setEndTime("");
					daysAndHours.add(daysAndHour);
					userMenuVO.setDaysAndHours(daysAndHours);
				}
			}
			status = userMenuVOService.saveAllUserMenu(userMenuVO);
			List<UserMenu> arrayList = userMenusService.findAllByUser(loginUserDetail.getCurrentLoginUser());
			Collections.reverse(arrayList);
			if (daysAndHoursVOService.isBusinessHours()) {
				httpSession.setAttribute("MenuList", arrayList);
				httpSession.setAttribute("AddMenuFlag", true);
			}
		}
		if (status.equals("success")) {
			return new ResponseEntity<>("success", HttpStatus.OK);
		}
		return new ResponseEntity<>("Something Went Wrong", HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/menu/{id}/{name}")
	public String loadMenuByIdAndName(@PathVariable("id") Long id, @PathVariable("name") String menuName, Model model) {
		UserMenuVO userMenuVO = userMenuVOService.LoadUserMenuVO(id, menuName);
		model.addAttribute("userMenuVO", userMenuVO);
		return "site/menu";
	}
}
