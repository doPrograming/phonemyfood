package com.phonemyfood.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserAddresses;
import com.phonemyfood.security.UserService;

@Controller
@RequestMapping({ "/admin" })
public class SalespersonController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/addSalesperson",method=RequestMethod.GET)
	public String addSalesperson(){
		User user = new User();
		UserAddresses userAddresses;
		List<User> salsePeople = userService.findAllSalsePeople();
		return "admin/addSalesperson";
	}
}
