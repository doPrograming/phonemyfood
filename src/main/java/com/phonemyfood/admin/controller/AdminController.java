package com.phonemyfood.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.User;
import com.phonemyfood.db.vo.UserBusinessVo;
import com.phonemyfood.security.UserService;
import com.phonemyfood.service.BusinessesService;
import com.phonemyfood.service.UserBusinessVOService;

@Controller
public class AdminController{
	
	@Autowired
	private UserBusinessVOService userBusinessVOService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	HttpSession httpSession;
	
	@Autowired
	BusinessesService businessesService;
	
	@RequestMapping({"/admin"})
	public String login() {
      return "admin/index";
	}
	
	@RequestMapping({"/admin/addClient"})
	public ModelAndView addClient(ModelMap modelMap) {
		ModelAndView modelAndView=new ModelAndView();
		UserBusinessVo userBusinessVo = new UserBusinessVo();
		List<Businesses> businesses = userBusinessVOService.findAllBusiness();
		modelAndView.addObject("businessList", businesses);
		modelAndView.addObject("user_business_vo", userBusinessVo);
		modelAndView.addAllObjects(modelMap);
		modelAndView.setViewName("admin/addClient");
		return modelAndView;
	}
	
	@RequestMapping({"/admin/normalLogin"})
	public String normalLogin(@RequestParam("user_id") Long id, @PageableDefault(direction= Direction.DESC, value = 3, sort="id") Pageable pageable){
		User user = userService.get(id);
		
		/*Page<UserMenu> userMenus = userMenusService.findLast3ByUser(loginUserDetail.getCurrentLoginUser(), pageable);
		ArrayList<UserMenu> arrayList = new ArrayList<>(userMenus.getContent());
		Collections.reverse(arrayList);
		httpSession.setAttribute("MenuList", arrayList);*/
		httpSession.setAttribute("USER_SESSION", user);
		httpSession.setAttribute("BUSINESS_NAME_SESSION", userBusinessVOService.findBusinessByUser(user).getName());
		return "redirect:/user/";
	}
	
	@RequestMapping({"/admin/backToAdmin"})
	public String backToAdmin(HttpServletRequest request, HttpServletResponse response){
		request.getSession().removeAttribute("USER_SESSION");
		request.getSession().removeAttribute("MenuList");
		request.getSession().removeAttribute("AddMenuFlag");
		request.getSession().removeAttribute("BUSINESS_NAME_SESSION");
		return "redirect:/admin/addClient";
	}
	
	@RequestMapping({"/admin/deleteClientModal"})
	public String deleteClientModal(@RequestParam("id") Long id,Model model) {
		Businesses businesses=businessesService.getOne(id);
		model.addAttribute(businesses);
		return "site/modal/deleteClientModal";
	}
	
	@RequestMapping({ "/admin/deleteSelectedClient" })
	public String deleteSelectedBusinesses(@RequestParam("key") Long id) {
		businessesService.deleteBusinesses(id);
		return "redirect:/admin/addClient";
	}
}
