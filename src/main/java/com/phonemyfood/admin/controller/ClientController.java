package com.phonemyfood.admin.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.Images;
import com.phonemyfood.db.User;
import com.phonemyfood.db.vo.UserBusinessVo;
import com.phonemyfood.security.UserService;
import com.phonemyfood.service.EmailService;
import com.phonemyfood.service.ImagesService;
import com.phonemyfood.service.UserBusinessVOService;
import com.phonemyfood.util.ImageUtils;

@Controller
@RequestMapping(path = "/admin")
public class ClientController {

	@Autowired
	private UserBusinessVOService userBusinessVOService;
	
	@Autowired
	private ImageUtils imageUtils;
	
	@Autowired
	private ImagesService imagesService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	AuthenticationManager authenticationManager;

	@RequestMapping(value = "/addEditClients", method = RequestMethod.POST)
	public String addClient(@ModelAttribute("user_business_vo") @Valid UserBusinessVo userBusinessVo, Model model,
			BindingResult result, HttpServletRequest request) {
		List<Businesses> businesses = userBusinessVOService.findAllBusiness();
		try {
			userBusinessVOService.saveAll(userBusinessVo, request);
		} catch (Exception e) {
			model.addAttribute("businessList", businesses);
			model.addAttribute("user_business_vo", userBusinessVo);
			if (e.getMessage().equals("Email Already exist")) {
				model.addAttribute("alreadyExist", true);
				return "admin/addClient";
			}
		}
		return "redirect:/admin/addClient";
	}

	@RequestMapping(value = "/searchBusinessValue")
	public ResponseEntity<?> searchBusiness(@RequestParam(value = "key") String key) {
		List<Businesses> businesses ;
		if (key.isEmpty()) {
			businesses =  userBusinessVOService.findAllBusiness();
		} else {
			businesses = userBusinessVOService.findBusinessByName(key);
		}
		return ResponseEntity.ok(businesses);
	}

	@RequestMapping(value = "/getClientDetails")
	public ResponseEntity<?> getClientDetails(@RequestParam(value = "b_id") Long id) {
		UserBusinessVo userBusinessVo = userBusinessVOService.findBusinessById(id);
		return ResponseEntity.ok(userBusinessVo);
	}

	@RequestMapping(value="/getBusinessImage")
	public String get_about_image(Model model, @RequestParam(value = "business_id") Long Id){
		UserBusinessVo userBusinessVo = userBusinessVOService.findBusinessById(Id);
		List<Images> images = imagesService.getImagesByImageableTypeAndImageableIdAndUser("Business",Id,userBusinessVo.getUser());
		model.addAttribute("images", images);
		return "site/modal/businessImageModal";
	}
	
	@RequestMapping(value="/createBusinessImage", method=RequestMethod.POST)
	public ResponseEntity<?> specialsImage(@RequestParam("id") Long id, MultipartHttpServletRequest request){
		
		Map<String, MultipartFile> fileMap = request.getFileMap();
		try {
			for(MultipartFile multipartFile : fileMap.values()){
				Images images = new Images();
				images.setImageableId(id);
				images.setImageableType("Business");
				User user = userBusinessVOService.findBusinessById(id).getUser();
				images.setUser(user);
				images =  imageUtils.generateURL(images);
				Long persistId=imagesService.insert(images);
				images.setId(persistId);
				images = imageUtils.uploadImage(multipartFile.getBytes(), multipartFile.getOriginalFilename(), images);
				imagesService.saveImages(images);
			}
			return ResponseEntity.ok("success");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			/*e.printStackTrace();*/
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		/*imagesService.saveImages(images);*/
			
	}
	
	@RequestMapping(value="/resetPassword", method = RequestMethod.GET)
	public String resetPassword(Model model,RedirectAttributes redirectAttributes, HttpServletRequest request) {
//		model.addAttribute("resetPassForm",new User());
		User user = userService.findByPasswordResetAndResetToken(true, request.getParameter("token"));
		if(user == null){
			redirectAttributes.addFlashAttribute("errorMsg", "Link is expired or no longer active");
			return "redirect:/";
		}
		model.addAttribute("token",request.getParameter("token"));
		return "resetPassword";
	} 
	
	@RequestMapping(value="/updatePassword", method = RequestMethod.POST)
	public String resetToken(@RequestParam("token") String token , Model model,RedirectAttributes redirectAttributes ,HttpServletRequest request) {
		String password = request.getParameter("password").toString();
		String confirmPass = request.getParameter("confirmPassword").toString();
		
		if(!password.equals(confirmPass)){
			model.addAttribute("errorMsg", "Please enter same password.");
			model.addAttribute("token",token);
			return "resetPassword";
		}
		
		User user = userService.findByPasswordResetAndResetToken(true, token);
		try{
			if(user == null){
				System.out.print("token not valid");
//				model.addAttribute("resetPassForm",new User());
				model.addAttribute("errorMsg", "Link is expired or no longer active");
				model.addAttribute("token",token);
				return "resetPassword";
			}
			user.setResetToken(UUID.randomUUID().toString());
			user.setPasswordReset(false);
			user.setPassword(passwordEncoder.encode(password));
			emailService.sendLoginUserDetailsEmail(user.getName(),user.getEmail(),password);
			userService.saveResetToken(user);
			redirectAttributes.addFlashAttribute("successMsg", "Password updated successfully. Login with new credintial.");
		}catch(Exception e){
			System.out.println("---------> " +e.getMessage());
			redirectAttributes.addFlashAttribute("errorMsg", "Password is not updated successfully. Try Again");
		}
		userService.autoLoginAfterResetPassword(request,user.getEmail(),password);
		return "redirect:/";
	}
	
	
	@RequestMapping(value="/forgotPassword", method = RequestMethod.GET)
	public String forgotPassword(Model model, HttpServletRequest request) {
		return "forgotPassword";
	} 
	
	@RequestMapping(value = "/resendPassword", method = RequestMethod.POST)
	public String resendPassword(@RequestParam(name = "id", required = false) Long id, @RequestParam(name = "email", required = false) String email,
			Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		User user = id == null ? userService.findByEmailAndActive(email) : userService.get(id);
		if (user == null) {
			model.addAttribute("errorMsg", "No user is available with this username.");
			return "forgotPassword";
		}
		user.setPasswordReset(true);
		user.setResetToken(UUID.randomUUID().toString());
		userService.saveResetToken(user);
		emailService.sendResetPasswordEmail(request, user);
		redirectAttributes.addFlashAttribute("successMsg", "Before login go through mail sent on " + user.getEmail());
		return "redirect:/";
	}
}
