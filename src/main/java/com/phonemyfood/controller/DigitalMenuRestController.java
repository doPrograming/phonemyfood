package com.phonemyfood.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.phonemyfood.db.DigitalMenu;
import com.phonemyfood.db.MenuCategoryItem;
import com.phonemyfood.db.vo.DigitalMenuVo;
import com.phonemyfood.service.DigitalMenuService;
import com.phonemyfood.service.MenuCategoryItemsService;
import com.phonemyfood.user.controller.BaseController;

@RestController
public class DigitalMenuRestController extends BaseController{
	@Autowired
	DigitalMenuService digitalMenuService;
	
	@Autowired
	MenuCategoryItemsService menuCategoryItemsService;
		
	@RequestMapping(value="/user/api/menus")
	public ResponseEntity<Object> menuData(){
		List<MenuCategoryItem> menuCategoryItems=menuCategoryItemsService.findAll();
		return new ResponseEntity<>(menuCategoryItems,HttpStatus.OK);
	}
	
	@RequestMapping(value="/user/api/digitalMenuData")
	public ResponseEntity<Object> digitalMenuData(){
		List<DigitalMenu> digitalMenu=digitalMenuService.findAllByBusinesses(findBusinessesByLoginUser());
		return new ResponseEntity<>(digitalMenu,HttpStatus.OK);
	}
	
	@RequestMapping(value="/user/api/digitalMenu")
	public ResponseEntity<Object> digitalMenu(){
		List<DigitalMenuVo> digitalMenuVos=digitalMenuService.getDigitalMenuVos();
		return new ResponseEntity<>(digitalMenuVos,HttpStatus.OK);
	}

}
