package com.phonemyfood.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ViewController {

	@RequestMapping({ "/" })
	public String login(HttpServletRequest request) {
		if (request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_USER")) {
			return "redirect:/default";
		} else
			return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView loginPage() {
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/default")
	public String defaultPage(HttpServletRequest request) {
		if (request.isUserInRole("ROLE_ADMIN")) {
			return "redirect:/admin/addClient";
		}
		if (request.isUserInRole("ROLE_USER")) {
			return "redirect:/user/";
		}
		return "redirect:/";
	}

	@RequestMapping(value="/pmf/{shortCode}")
	public String login(@PathVariable("shortCode") String shortCode) {
		if (shortCode != null) {
			return "redirect:/admin/resetPassword?token=" + shortCode;
		}
		return "redirect:/";
	}
}