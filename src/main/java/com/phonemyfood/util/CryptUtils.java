package com.phonemyfood.util;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class CryptUtils {    
    private final static String publicKeyEncoded = "308201b83082012c06072a8648ce3804013082011f028" + 
            "18100fd7f53811d75122952df4a9c2eece4e7f611b7523cef4400c31e3f80b6512669455d402251fb593d8" + 
            "d58fabfc5f5ba30f6cb9b556cd7813b801d346ff26660b76b9950a5a49f9fe8047b1022c24fbba9d7feb7c" + 
            "61bf83b57e7c6a8a6150f04fb83f6d3c51ec3023554135a169132f675f3ae2b61d72aeff22203199dd1480" + 
            "1c70215009760508f15230bccb292b982a2eb840bf0581cf502818100f7e1a085d69b3ddecbbcab5c36b85" + 
            "7b97994afbbfa3aea82f9574c0b3d0782675159578ebad4594fe67107108180b449167123e84c281613b7c" + 
            "f09328cc8a6e13c167a8b547c8d28e0a3ae1e2bb3a675916ea37f0bfa213562f1fb627a01243bcca4f1bea" + 
            "8519089a883dfe15ae59f06928b665e807b552564014c3bfecf492a0381850002818100893bedb9c9c53ee" + 
            "92636d28c50ccc095dcef0d43d2866d2eaca08e6f7877bbf2ccabb9a4e70d22515d0d0d7383fe435956b11" + 
            "641d5a21d619f5a9070cd382cbe845cde3e2bdcc6ba19207f61a908a313ea38c7107071441712c710eb257" + 
            "43a64477948575967b3cdc2cbe44ef4cdf1608c61f1f58c79306af992dd60c0bdaa2a";

    private static String aesKeyEncoded = "JRXCOvNowrBucPJjIO5Ypg==";

    public static boolean verifyMessage(String message, String signature) {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("DSA");
            EncodedKeySpec publicKeySpec = 
                    new X509EncodedKeySpec(new BigInteger(publicKeyEncoded, 16).toByteArray());
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
            Signature verifying = Signature.getInstance("DSA");
            verifying.initVerify(publicKey);
            verifying.update(message.getBytes());
            return verifying.verify(Base64.getDecoder().decode(signature));
        } catch (Exception ex) {
            ;
        }
        return false;
    }

    public static String obfuscate (String cleartext) throws Exception {
        byte[] rawKey = Base64.getDecoder().decode(aesKeyEncoded);
        SecretKeySpec skeySpec = new SecretKeySpec(rawKey, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(cleartext.getBytes());
        return Base64.getEncoder().encodeToString(encrypted);
    }

    public static String deobfuscate (String obfuscated) throws Exception {
        byte[] rawKey = Base64.getDecoder().decode(aesKeyEncoded);
        SecretKeySpec skeySpec = new SecretKeySpec(rawKey, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] obfuscateBytes = Base64.getDecoder().decode(obfuscated);
        byte[] decrypted = cipher.doFinal(obfuscateBytes);
        return new String(decrypted);
    }

    public static String bytesToHex (byte[] in) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < in.length; ++i) {
            if ((in[i] & 0xf0) == 0)
                result.append('0');
            result.append(Integer.toHexString( (int)(in[i] & 0xff)));
        }
        return result.toString();
    }

    public static byte[] hexToBytes (String in) {
        byte[] result = new byte[in.length() / 2];
        for (int i = 0, j = 0; i < result.length; ++i, j += 2)
            result[i] = (byte) Integer.parseInt(in.substring(j, j+2), 16);
        return result;
    }

    public static long obfuscateLong(Long x) {
        return (x * 387420489 % 1000000000);
    }
    public static long deobfuscateLong(long x) {
        return (x * 513180409 % 1000000000);
    }

 // Function to generate a short url from intger ID
    static String  map = "abcdefghijklmnopqrstuvwxyzABCDEF" +
            "GHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String idToShortURL(int n) {
        // Map to store 62 possible characters
        StringBuilder shortUrl = new StringBuilder();
     
        // Convert given integer id to a base 62 number
        while (n > 0) {
            // use above map to store actual character
            // in short url
            shortUrl.append(map.charAt(n % 62));
            n = n / 62;
        }
     
        // Reverse shortURL to complete base conversion
        //reverse(shorturl.begin(), shorturl.end());
     
        return shortUrl.reverse().toString();
    }
     
    // Function to get integer ID back from a short url
    public static int shortURLtoID(String shortUrl) {
        int id = 0; // initialize result
     
        // A simple base conversion logic
        for (int i = 0; i < shortUrl.length(); i++) {
            char ch = shortUrl.charAt(i);
            id *= 62;
            if ('a' <= ch && ch <= 'z')
              id += ch - 'a';
            if ('A' <= ch && ch <= 'Z')
              id += ch - 'A' + 26;
            if ('0' <= ch && ch <= '9')
              id += ch - '0' + 52;
        }
        return id;
    }
    
    public static void main (String[] args) throws Exception{
        /*int n = 1234567;
        String shorturl = idToShortURL(n);
        System.out.println("Generated short url is " + shorturl);
        System.out.println("Id from url is " + Integer.toString(shortURLtoID(shorturl)));
         
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128); // 192 and 256 bits may not be available
        SecretKey skey = kgen.generateKey();
        @SuppressWarnings("unused") byte[] raw = skey.getEncoded();*/
        // aesKeyEncoded = new BASE64Encoder().encode(raw);
        /*long id = 3000;
        long obfuscatedInt = obfuscateLong(id);
        long deobfuscatedInt = deobfuscateLong(obfuscatedInt);
        System.out.println(String.valueOf(id) + ' ' + String.valueOf(obfuscatedInt) + " " + String.valueOf(deobfuscatedInt));*/
        String obfuscated = obfuscate("http://icicibank.com");
        System.out.println("obfuscated:>"+obfuscated);
        String original = deobfuscate(obfuscated);
        System.out.println("original:>"+original);
    }
}