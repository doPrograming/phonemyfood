package com.phonemyfood.util;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public abstract class Utility {
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");

	public static void main(String arg[]) {

	}

	public static boolean validateTime(String fromTime, String toTime) {
		fromTime = fromTime.replace(",", "");
		toTime = toTime.replace(",", "");
		LocalTime localFromTime = LocalTime.parse(fromTime, formatter);
		LocalTime localToTime = LocalTime.parse(toTime, formatter);
		return localToTime.compareTo(localFromTime) > 0;
	}

	public static boolean isTimeEquals(String newStartTime, String newEndTime, String oldStartTime, String oldEndTime) {
		LocalTime bussinessStartTime = LocalTime.parse(oldStartTime, formatter);
		LocalTime bussinessEndTime = LocalTime.parse(oldEndTime, formatter);
		// Bussiness Time Start
		LocalTime startNewTime = LocalTime.parse(newStartTime, formatter);
		LocalTime endNewTime = LocalTime.parse(newEndTime, formatter);
		// New Time Start		
		if (bussinessStartTime.compareTo(startNewTime) <= 0 && bussinessEndTime.compareTo(endNewTime) >= 0) {
			return true;
		}
		return false;
	}
}
