package com.phonemyfood.util;

public class SignupBean {
	private String fullName;
	private String confirmationUrl;
	private String businessType;

	public SignupBean(String fullName, String confirmationUrl, String businessType) {
		super();
		this.fullName = fullName;
		this.confirmationUrl = confirmationUrl;
		this.businessType = businessType;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getConfirmationUrl() {
		return confirmationUrl;
	}

	public void setConfirmationUrl(String confirmationUrl) {
		this.confirmationUrl = confirmationUrl;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
}
