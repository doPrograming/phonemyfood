package com.phonemyfood.util;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class CropImage {
	public static void main(String[] args) {
		CropImage cropImage = new CropImage();
		cropImage.crop();
	}

	public void crop() {
		try {
			BufferedImage originalImgage = ImageIO.read(new File("C:\\Users\\exito\\Pictures\\102.jpg"));
			GeneralPath clip = new GeneralPath();
			clip.moveTo(0, 0);//TOP 
			clip.lineTo(780, 0);//TOP 
			clip.lineTo(960, 365);//Arrow
			clip.lineTo(800, 725);//Bottom 
			clip.lineTo(0, 725);//Bottom 
			Rectangle bounds = clip.getBounds();
			BufferedImage img = new BufferedImage(bounds.width, bounds.height, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2d = img.createGraphics();
			g2d.setClip(clip);								
			g2d.drawImage(originalImgage, 0, 0, null);
			final Image img1= makeColorTransparent(img, new Color(img.getRGB(0, 0)));
			BufferedImage bi=imageToBufferedImage(img1);
			// Swap Image
			File f;
	        int width = bi.getWidth(); 
	        int height = bi.getHeight();
	        BufferedImage mimg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB); 
	        for (int y = 0; y < height; y++) { 
	            for (int lx = 0, rx = width - 1; lx < width; lx++, rx--){ 
	                int p = bi.getRGB(lx, y); 
	                mimg.setRGB(rx, y, p); 
	            } 
	        }
	        try { 
	            f = new File("C:\\Users\\exito\\Pictures\\swapped.png"); 
	            ImageIO.write(mimg, "png", f); 
	        } 
	        catch(IOException e){ 
	            System.out.println("Error: " + e); 
	        } 	
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}	
	} 
	
	//Image to BuuferedImage
	private BufferedImage imageToBufferedImage(final Image image) {
       final BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
       final Graphics2D g2 = bufferedImage.createGraphics();
       g2.drawImage(image, 0, 0, null);
       g2.dispose();
       return bufferedImage;
     }
	
	//Make Color Transperent
	private Image makeColorTransparent(final BufferedImage im, final Color color) {
	  final ImageFilter filter = new RGBImageFilter() {
	     // the color we are looking for (white)... Alpha bits are set to opaque
	     public int markerRGB = color.getRGB() | 0x00FF00; 
	     public final int filterRGB(final int x, final int y, final int rgb)
	     {
	    	 // Mark the alpha bits as zero - transparent
	        if ((rgb | 0x00FF00) == markerRGB) 
               return 0x00FF00 & rgb;
            else 
               return rgb;
         }
	  };
      final ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
      return Toolkit.getDefaultToolkit().createImage(ip);
	}
}