package com.phonemyfood.util;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import com.phonemyfood.user.controller.BaseController;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.phonemyfood.db.Images;
import com.phonemyfood.service.ImagesService;

@Component
public class ImageUtils extends BaseController{

	@Value("${phonemyfood.images.common.path}")
	private String commonPath;
	
	private final Logger LOG = LoggerFactory.getLogger(ImageUtils.class);
	
	@Autowired
	ImagesService imagesService;

	public Images generateURL(Images images) {
		File commonFile = new File(commonPath + File.separator + "images" + File.separator + images.getUser().getId()
				+ File.separator + images.getImageableType());
		if (!commonFile.exists()) {
			commonFile.mkdirs();
		}
		images.setPicture(commonFile.getPath());
		return images;
	}

	public Images uploadImage(byte[] imageFile, String originalFilename, Images images) {
		String[] strings = originalFilename.split("[.]");
		String nameFile;
		if(images.getImageableType()=="DigitalMenuItem")
			nameFile = strings[0].replaceAll(" ", "") + "_" + images.getId();
		else
			nameFile = strings[0].replaceAll(" ", "") + "_" + images.getImageableId() + "_" + images.getId();
		String fileName = nameFile + "." + strings[1];
		try {
			File commonFile = new File(images.getPicture());
			try {
				BufferedImage bufferedImage = null;
				BufferedImage imageProof = null;
				if (imageFile != null) {
					imageProof = ImageIO.read(new ByteArrayInputStream(imageFile));
					int width = imageProof.getWidth();
					int height = imageProof.getHeight();
					int[] pixels = imageProof.getRGB(0, 0, width, height, null, 0, width);
					imageProof = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
					imageProof.setRGB(0, 0, width, height, pixels, 0, width);
				}
				if (!commonFile.exists())
					commonFile.mkdirs();

				if (imageProof != null) {
					ImageIO.write(imageProof, strings[1], new File(commonFile.getPath() + File.separator + fileName));
					Integer integer = imageProof.getWidth() / imageProof.getHeight();
					try {
						bufferedImage = ImageIO.read(new ByteArrayInputStream(imageFile));
						bufferedImage = resize(bufferedImage, 500 / integer, 500);
						if (bufferedImage != null && !images.getImageableType().equals("ClientUpload") && !images.getImageableType().equals("DigitalMenuItem")) {
							ImageIO.write(bufferedImage, strings[1], new File(commonFile.getPath() + File.separator + nameFile + "-500x500." + strings[1]));
						}
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
					} finally {
						bufferedImage.flush();
						imageProof.flush();
					}
				}
				images.setPicture(fileName);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return images;
	}

	public File readFile(Images images) {
		File kycDetailFile = new File(commonPath + File.separator + "images");
		if (kycDetailFile.exists()) {
			File fileById = new File(kycDetailFile.getPath() + File.separator + images.getUser().getId()
					+ File.separator + images.getImageableType());
			if (fileById.exists()) {
				return fileById;
			}
		}
		return null;
	}

	public Boolean deleteFile(Images images) {
		File file = new File(commonPath + File.separator + "images" + File.separator + images.getUser().getId()
				+ File.separator + images.getImageableType() + File.separator + images.getPicture());
		try {
			return file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private static BufferedImage resize(BufferedImage img, int height, int width) {
		Image tmp = img.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
		BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		return resized;
	}

	public static BufferedImage loadImage(String ref) {
		BufferedImage bimg = null;
		try {
			bimg = ImageIO.read(new File(ref));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bimg;
	}
	
	public static BufferedImage makeColorTransparent(BufferedImage img, Color color) {
		BufferedImage dimg = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = dimg.createGraphics();
		g.setComposite(AlphaComposite.Src);
		g.drawImage(img, null, 0, 0);
		g.dispose();
		for (int i = 0; i < dimg.getHeight(); i++) {
			for (int j = 0; j < dimg.getWidth(); j++) {
				if (dimg.getRGB(j, i) == color.getRGB()) {
					dimg.setRGB(j, i, 0x8F1C1C);
				}
			}
		}
		return dimg;
	}

	public static void saveImage(BufferedImage img, String ref, String croppingFramePath) {
		try {
			BufferedImage frame = ImageIO.read(new File(croppingFramePath));
			img=overlapImage(img, frame);
			img = makeColorTransparent(img,new Color(0,0,0)); 
			String format = (ref.endsWith(".png")) ? "png" : "jpg";
			ImageIO.write(img, format, new File(ref));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public byte[] getFrame(String menu, String design, String orientation,String name) {
		File f = new File(commonPath+ File.separator + "frame" + File.separator + findBusinessesByLoginUser().getBusinessType() + File.separator + menu  
				+ File.separator + design + File.separator + name);
		byte[] imageContent = null;
		try {
			InputStream in1 = new FileInputStream(f);
			imageContent = IOUtils.toByteArray(in1);
			in1.close();
		} catch (IOException e) {
			System.out.println("Error: " + e);
		}
		return imageContent;
	}
	
	public byte[] getInitialFrame(String menu, String design, String orientation, String bussinessType) {
		String path = commonPath + File.separator + "frame" + File.separator + bussinessType + File.separator + menu
				+ File.separator + design + File.separator;
		LOG.info(path);
		File dir = new File(path);
		if (dir.listFiles() == null) {
			path = commonPath + File.separator + "defaultFrame" + File.separator;
			dir = new File(path);
		}
		File f = null;
		byte[] imageContent = null;
		for (File file : dir.listFiles()) {
			if (file.getName().startsWith(orientation) && !file.getName().contains("mask")) {
				if(file.isDirectory()){
					for (File maskFile : file.listFiles()) {
						f = maskFile;
					}
				}
				break;
			}
		}
		if (f == null) {
			path = commonPath + File.separator + "defaultFrame" + File.separator;
			LOG.info(path);
			dir = new File(path);
			for (File file : dir.listFiles()) {
				if (file.getName().startsWith(orientation) && !file.getName().contains("mask")) {
					f = file;
					break;
				}
			}
		}
		LOG.info(path);
		try {
			if(!f.canRead())
				f.setReadable(true);
			InputStream in1 = new FileInputStream(f);
			imageContent = IOUtils.toByteArray(in1);
			in1.close();
		} catch (IOException e) {
			LOG.error("File is missing '{}'!", f.getName());
		}
		return imageContent;
	}

	public int getNumberOfFramesInFolder(String menu, String design, String orientation, String bussinessType) {
		File dir = new File(commonPath + File.separator + "frame" + File.separator + bussinessType + File.separator
				+ menu + File.separator + design + File.separator);
		if(dir.listFiles()==null) {
			dir = new File(commonPath + File.separator + "defaultFrame" + File.separator);
		}
		int count = 0;
		for (File file : dir.listFiles()) {
			if (file.getName().startsWith(orientation) && !file.getName().contains("mask")) {
				count++;
			}
		}
		return count;
	}

	public Images uploadFile(byte[] file, String originalFilename, Images images) {
		String[] strings = originalFilename.split("\\.(?=[^\\.]+$)");
		String nameFile = strings[0].replaceAll(" ", "") + "_" + images.getId();
		String fileName = nameFile + "." + strings[strings.length-1];
		try {
			File commonFile = new File(images.getPicture());
			try {
				if (!commonFile.exists())
					commonFile.mkdirs();
				Path data=Paths.get(commonFile+File.separator+fileName);
				Files.write(data,file);
				images.setPicture(fileName);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return images;
	}
	
	public static BufferedImage overlapImage(BufferedImage bgImage,BufferedImage frame) {
		Graphics2D g2Source = bgImage.createGraphics();
		g2Source.drawImage(frame, 0, 0, null);
		g2Source.dispose();
        return bgImage;
    }
}
