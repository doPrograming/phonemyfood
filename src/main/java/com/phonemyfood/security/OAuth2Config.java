package com.phonemyfood.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class OAuth2Config {
	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		@Autowired
		private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

		@Autowired
		private CustomLogoutSuccessHandler customLogoutSuccessHandler;

		@Override
		public void configure(HttpSecurity http) throws Exception {
			 http
	             .exceptionHandling()
	             .authenticationEntryPoint(customAuthenticationEntryPoint)
	             .and()
	             .logout()
	             .logoutUrl("/oauth/logout")
	             .logoutSuccessHandler(customLogoutSuccessHandler)
	             .and()
	             .csrf()
	             .requireCsrfProtectionMatcher(new AntPathRequestMatcher("/oauth/authorize"))
	             .disable()
	             .headers()
	             .frameOptions().disable()
	             /*.sessionManagement()
	             .sessionCreationPolicy(SessionCreationPolicy.STATELESS)*/
	             .and()
	             .authorizeRequests()
	             .antMatchers("/api/signup").permitAll()
	             .antMatchers("/api/**").fullyAuthenticated();
		}
	}

	@Configuration
	@EnableAuthorizationServer
	protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {
		private static final String ENV_OAUTH = "authentication.oauth.";
		private static final String PROP_CLIENTID = "clientid";
		private static final String PROP_SECRET = "secret";
		//private RelaxedPropertyResolver propertyResolver;
		@Autowired
		private Environment env;
		
		@Autowired
		private DataSource dataSource;
		
		@Autowired
		private PasswordEncoder passwordEncoder;

		@Bean
		public TokenStore tokenStore() {
			return new CustomJdbcTokenStore(dataSource);
		}

		@Autowired
		@Qualifier("authenticationManagerBean")
		private AuthenticationManager authenticationManager;

		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
			endpoints.tokenStore(tokenStore()).tokenEnhancer(tokenEnhancer()).authenticationManager(authenticationManager);
		}
		
		@Override
	    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
			oauthServer.allowFormAuthenticationForClients().tokenKeyAccess("permitAll()");
	    }

		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			//clients.jdbc(dataSource).passwordEncoder(passwordEncoder);
			clients.inMemory().withClient(env.getProperty(ENV_OAUTH + PROP_CLIENTID)).scopes("read", "write")
					.authorities("ROLE_USER").authorizedGrantTypes("password", "refresh_token")
					.secret("{bcrypt}"+env.getProperty(ENV_OAUTH + PROP_SECRET)).accessTokenValiditySeconds(3600);
		}
		
		@Bean
	    public TokenEnhancer tokenEnhancer() {
	        return new CustomTokenEnhancer();
	    }

	}
}
