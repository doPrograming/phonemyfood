package com.phonemyfood.security;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Role;
import com.phonemyfood.db.User;
import com.phonemyfood.repository.RoleRepository;
import com.phonemyfood.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = findByEmailAndActive(username);
		if (user == null) {
			throw new UsernameNotFoundException("Email " + username + " not found");
		}
		return new AuthenticatedUser(user);
	}
	
	public void save(User user) {
		userRepository.save(user);
	}

	public void delete(Long id) {
		User user = userRepository.getOne(id);
		user.setActive(false);
		userRepository.save(user);
	}

	public User findByEmailAndActive(String email) {
		return userRepository.findByEmailAndActive(email, true);
	}
	
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

//	public List<User> listAll() {
//		return userRepository.findAllActive();
//	}

	public User get(Long id) {
		return userRepository.getOne(id);
	}

	/*public User findByResetToken(String token) {
		return userRepository.findByResetToken(token);
	}*/

	public void saveResetToken(User user) {
		System.out.print("REPO...." +user);
		userRepository.save(user);
	}

	public User findByPasswordResetAndResetToken(boolean b, String parameter) {
		return userRepository.findByPasswordResetAndResetToken(b,parameter);
	}
	
	
	/*public List<User> findAllClientByUser(String userName) {
		return userRepository.findAllByRolesAndUSerName(roleRepository.findAllByName("ROLE_USER"),userName);
	}*/

	public boolean emailAlreadyExist(User user) {
		User existUser = findByEmailAndActive(user.getEmail());
		if (user.getId() == 0 && existUser != null)
			return true;
		else if(existUser != null && user.getId() != existUser.getId())
			return true;
		return false;
	}
	
	public List<User> findAllSalsePeople(){
		List<Role> roles = roleRepository.findAllByName("ROLE_SALSEPERSON");
		return userRepository.findAllByRoles(roles);
	}
	
	public void autoLoginAfterResetPassword(HttpServletRequest request, String username, String password) {
	    try {
	        request.login(username, password);
	    } catch (Exception e) {
	        System.out.println(e);
	    }
	}
}
