package com.phonemyfood.security;

import javax.sql.DataSource;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

public class CustomJdbcTokenStore extends JdbcTokenStore {

	public CustomJdbcTokenStore(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
		//AuthenticatedUser user = (AuthenticatedUser) authentication.getPrincipal();
		OAuth2AccessToken accessToken = super.getAccessToken(authentication);
	//	final Map<String, Object> additionalInfo = new HashMap<>();

        //additionalInfo.put("loginPin", user.getUser().getLoginPin() == null ? false : true);
	//	if (accessToken != null)
	//		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
		return accessToken;
	}

}
