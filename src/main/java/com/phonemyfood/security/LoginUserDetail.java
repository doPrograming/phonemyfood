package com.phonemyfood.security;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.phonemyfood.db.User;

@Component
public class LoginUserDetail {
	
	@Autowired
	private HttpSession httpSession;

	public User getCurrentLoginUser() {
		User clientUser = (User) httpSession.getAttribute("USER_SESSION");
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		AuthenticatedUser customUserDetail = (AuthenticatedUser) authentication.getPrincipal();
		return clientUser != null ? clientUser : customUserDetail.getUser();
	}
	
	public User getActualLoginUser(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		AuthenticatedUser customUserDetail = (AuthenticatedUser) authentication.getPrincipal();
		return customUserDetail.getUser();
	}
}
