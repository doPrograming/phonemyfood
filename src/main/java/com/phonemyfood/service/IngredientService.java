package com.phonemyfood.service;

import java.util.List;

import javax.validation.Valid;

import com.phonemyfood.db.Ingredients;

public interface IngredientService {

	List<Ingredients> getAllIngredientsByUser();

	void saveIngredient(@Valid Ingredients ingredients);

	void deleteIngredient(Long id);

	List<Ingredients> findIngredientByUserAndName(String key);
	
	Ingredients findOne(Long id);
}
