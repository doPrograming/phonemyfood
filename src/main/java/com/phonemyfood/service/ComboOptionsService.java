package com.phonemyfood.service;

import com.phonemyfood.db.ComboOptions;

public interface ComboOptionsService {

	ComboOptions findOneByPriceAndCombos(Long id);

	void saveCombo(ComboOptions comboOptions);

}
