package com.phonemyfood.service;

import java.util.List;

import com.phonemyfood.db.Categories;
import com.phonemyfood.db.User;

public interface CategoriesService {

	List<Categories> getAllCategoryByUser(User user);

	Categories getOne(Long id);

	void save(Categories categories);

	void delete(Long id);

	List<Categories> categoryExist(User user, String name);

	String deleteCategory(Long id);

	void save(Categories categories, String name, Long id, User user);
}
