package com.phonemyfood.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.User;
import com.phonemyfood.db.vo.UserBusinessVo;

public interface UserBusinessVOService {

	void saveAll(@Valid UserBusinessVo userBusinessVo, HttpServletRequest request) throws Exception;

	List<Businesses> findAllBusiness();

	/* List<Businesses> findBusinessById(String key); */

	UserBusinessVo findBusinessById(Long id);

	List<Businesses> findBusinessByName(String key);

	Businesses findBusinessByUser(User user);
}
