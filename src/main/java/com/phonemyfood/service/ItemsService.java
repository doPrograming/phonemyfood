package com.phonemyfood.service;

import java.util.List;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.User;

public interface ItemsService {
	
	Items getOne(long itemid);

	Items getSelectedItemDetail(Long id);

	void saveitem(Items items);

	List<Items> getAllItems(User user);

	Items findByIdAndKey(long id, String key);

	List<Items> findAllByUserAndName(User currentLoginUser,String key);

	List<Items> findAllByUser(User currentLoginUser);

	void deleteItem(Items item);
}
