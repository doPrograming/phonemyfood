package com.phonemyfood.service;

import java.util.List;

import com.phonemyfood.db.UserSpecials;

public interface UserSpecialsService {

	void saveSpecials(UserSpecials userSpecials);

	List<UserSpecials> getAllSpecialsByUser();

	UserSpecials getSpecialsById(Long specialId);

}
