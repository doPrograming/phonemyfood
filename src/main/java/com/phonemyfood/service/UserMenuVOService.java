package com.phonemyfood.service;

import com.phonemyfood.db.vo.UserMenuVO;

public interface UserMenuVOService {

	UserMenuVO prepareUserMenuVO();

	String saveAllUserMenu(UserMenuVO userMenuVO);

	UserMenuVO LoadUserMenuVO(Long id, String menuName);

}
