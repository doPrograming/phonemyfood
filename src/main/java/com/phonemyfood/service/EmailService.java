package com.phonemyfood.service;

import javax.servlet.http.HttpServletRequest;

import com.phonemyfood.db.User;
import com.phonemyfood.db.vo.UserBusinessVo;

public interface EmailService {
	public void sendSignupEmail(HttpServletRequest request, UserBusinessVo userBusinessVo, String token);

	public void sendResetPasswordEmail(HttpServletRequest request, User user);
	
	public void sendLoginUserDetailsEmail(String username,String email,String Password);
}
