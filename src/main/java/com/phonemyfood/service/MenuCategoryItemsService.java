package com.phonemyfood.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.MenuCategoryItem;
import com.phonemyfood.db.UserMenu;

public interface MenuCategoryItemsService {

	List<MenuCategoryItem> findAll();

	void save(MenuCategoryItem menuCategoryItem);

	MenuCategoryItem findAllByMenuIdAndCategoryId(long menuId, long categoryId);

	void delete(MenuCategoryItem menuCategoryItem);

	Set<Items> findItemsByUserMenus(List<UserMenu> userMenus);

	Set<BigInteger> findByMenuCategoryItem(long menuCategoryItemId);

	List<MenuCategoryItem> findAllByCategoryId(Long id);
}