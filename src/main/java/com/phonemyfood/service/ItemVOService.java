package com.phonemyfood.service;

import java.util.List;

import javax.validation.Valid;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.vo.DaysAndHoursVO;
import com.phonemyfood.db.vo.ItemVo;

public interface ItemVOService {

	ItemVo getAllItemDetail();

	void saveAllItemsDetails(@Valid ItemVo itemVo);

	ItemVo getSelectedItemDetail(Long id);

	List<Items> getAllItemByUser();

	DaysAndHoursVO prepareDHModal(Long id);

	void saveAllDHItem(@Valid DaysAndHoursVO daysAndHoursVO);

}
