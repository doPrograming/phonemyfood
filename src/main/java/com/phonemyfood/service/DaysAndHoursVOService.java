package com.phonemyfood.service;

import java.util.List;

import com.phonemyfood.db.DaysAndHours;
import com.phonemyfood.db.vo.DaysAndHoursVO;

public interface DaysAndHoursVOService {

	void saveDaysAndHoursAbout(DaysAndHoursVO daysAndHoursVO);

	void deleteAll(List<DaysAndHours> daysAndHours);

	void deleteAllByTimeableIdAndTimeableType(long id, String string);

	boolean isBusinessHours();

	public boolean isEqualToBusinessHours(String startTime, String endTime, int index);
}
