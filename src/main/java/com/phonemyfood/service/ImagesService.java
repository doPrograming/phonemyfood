package com.phonemyfood.service;

import java.util.List;

import com.phonemyfood.db.Images;
import com.phonemyfood.db.User;

public interface ImagesService {

	void saveImages(Images images);

	List<Images> getImagesByTypeAndUser(String string, User user);

	Images findOneByid(Long id);

	List<Images> getImagesByImageableTypeAndImageableIdAndUser(String string, Long id, User user);
	
	Images getImagesByImageableTypeAndImageableId(String string, Long id, User user);

	void deleteImage(Images images);

	Long insert(Images images);
	
	Images findByImageableTypeAndUser(String string,User user);
	
	public Images getImageByPicturAndImageableId(String picture, long digitalmenuId);
}
