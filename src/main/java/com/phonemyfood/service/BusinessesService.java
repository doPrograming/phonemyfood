package com.phonemyfood.service;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.User;

public interface BusinessesService {
	
	Businesses findByUser(User user);
	
	Businesses getOne(long id);

	void save(Businesses businesses);
	
	void deleteBusinesses(long id);
}
