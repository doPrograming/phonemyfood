package com.phonemyfood.service;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserAbouts;
import com.phonemyfood.db.UserAddresses;

public interface UserAddressService {

	public UserAddresses findByUser(User user);
	
	public void updateUserAddress(UserAbouts userAbouts,UserAddresses userAddresses);
	
	public UserAddresses save(UserAddresses userAddresses);
}