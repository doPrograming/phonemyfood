package com.phonemyfood.service;

import com.phonemyfood.db.vo.ComboOptionVO;

public interface ComboOptionsVOService {

	ComboOptionVO getAllComboDetails(Long id);

}
