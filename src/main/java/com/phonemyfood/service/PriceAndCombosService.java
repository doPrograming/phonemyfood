package com.phonemyfood.service;

import java.util.List;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.PriceAndCombos;

public interface PriceAndCombosService {

	List<PriceAndCombos> findAllByItemAndUserMenuId(Items itemModel, long userMenuId);

	List<PriceAndCombos> findAllByItemOrderById(Items itemsModal);

	void saveAll(List<PriceAndCombos> priceAndCombos);

	void deleteAllByItem(Items item);

}
