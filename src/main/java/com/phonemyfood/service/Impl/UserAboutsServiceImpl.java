package com.phonemyfood.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.UserAbouts;
import com.phonemyfood.repository.UserAboutsRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.security.UserService;
import com.phonemyfood.service.UserAboutsService;

@Service
public class UserAboutsServiceImpl implements UserAboutsService {
	
	@Autowired
	private UserAboutsRepository userAboutsRepository;
	
	@Autowired
	private LoginUserDetail loginUserDetail;
	
	@Autowired
	private UserService userService;
	
	@Override
	public void saveUserAbout(UserAbouts userAbouts) {
		userAboutsRepository.save(userAbouts);
	}

	@Override
	public UserAbouts findByUser() {
		return userAboutsRepository.findByUser(loginUserDetail.getCurrentLoginUser());
	}

	@Override
	public UserAbouts findById(long id) {
		return userAboutsRepository.findByUser(userService.get(id));
	}

}
