package com.phonemyfood.service.Impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.DaysAndHours;
import com.phonemyfood.db.Items;
import com.phonemyfood.db.PriceAndCombos;
import com.phonemyfood.db.User;
import com.phonemyfood.db.vo.DaysAndHoursVO;
import com.phonemyfood.db.vo.ItemVo;
import com.phonemyfood.repository.BusinessesRepository;
import com.phonemyfood.repository.DayAndHoursRepository;
import com.phonemyfood.repository.ItemRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.ItemVOService;
import com.phonemyfood.service.PriceAndCombosService;

@Service
public class ItemVOServiceImpl implements ItemVOService {

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private LoginUserDetail loginUserDetail;

	@Autowired
	private BusinessesRepository businessesRepository;

	@Autowired
	private DayAndHoursRepository dayAndHoursRepository;

	@Autowired
	private PriceAndCombosService priceAndCombosService;
	
	@Override
	public ItemVo getAllItemDetail() {
		ItemVo itemVo = new ItemVo();
		User user = loginUserDetail.getCurrentLoginUser();
		// List<Items> items = itemRepository.findAllByUser(user);
		Businesses businesses = businessesRepository.findByUser(user);
		List<PriceAndCombos> priceAndCombos = new ArrayList<>();
		int count = 6;
		if (businesses.getBusinessType() == "marijuana") {
			count = 5;
		}
		itemVo.setPriceAndCombos(priceAndCombos);
		for (int i = 0; i < count; i++) {
			PriceAndCombos combos = new PriceAndCombos();
			combos.setRecord(i + 1);
			itemVo.addPriceAndCombos(combos);
		}
		priceAndCombos = itemVo.getPriceAndCombos();
		// itemVo.setItems(items); // pass list of items for current user
		itemVo.setItemModel(new Items()); // pass new Created Item
		itemVo.setBusinesses(businesses); // pass data for business
		itemVo.setPriceAndCombos(priceAndCombos); // pass 6 or 5 model class as
													// per business type
		return itemVo;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveAllItemsDetails(@Valid ItemVo itemVo) {
		User user = loginUserDetail.getCurrentLoginUser();
		itemVo.getItemModel().setUser(user);
		Items items = itemRepository.save(itemVo.getItemModel());
		if (itemVo.getPriceAndCombos() != null) {
			itemVo.getPriceAndCombos().stream().filter(p -> p.getName() != null && p.getPrice() != null ).forEach(p -> {				
				if(p.getPrice()!="") {
					try {
					if(p.getPrice().contains(".") && p.getPrice().substring(p.getPrice().indexOf(".")).length()<3)
					{
						p.setPrice(p.getPrice().concat("0"));
					}
					else if(! p.getPrice().contains(".")) {
					p.setPrice(p.getPrice().concat(".00"));
				}	
					}
					catch (Exception e) {
						System.err.println(e);
					}
				p.setItem(items);
				}
			});
			priceAndCombosService.saveAll(itemVo.getPriceAndCombos());
		}
		}
	

	@Override
	public ItemVo getSelectedItemDetail(Long id) {
		ItemVo itemVo = new ItemVo();
		Items itemsModal = itemRepository.getOne(id);
		User user = loginUserDetail.getCurrentLoginUser();
		// List<Items> items = itemRepository.findAllByUser(user);
		Businesses businesses = businessesRepository.findByUser(user);
		List<PriceAndCombos> priceAndCombos = priceAndCombosService.findAllByItemOrderById(itemsModal);
		if (priceAndCombos.isEmpty()) {
			int count = 6;
			if (businesses.getBusinessType() == "marijuana") {
				count = 5;
			}
			itemVo.setPriceAndCombos(priceAndCombos);
			for (int i = 0; i < count; i++) {
				PriceAndCombos combos = new PriceAndCombos();
				combos.setRecord(i + 1);
				itemVo.addPriceAndCombos(combos);
			}
			priceAndCombos = itemVo.getPriceAndCombos();
		}

		// itemVo.setItems(items); // pass list of items for current user
		itemVo.setItemModel(itemsModal); // pass new Created Item
		itemVo.setBusinesses(businesses); // pass data for business
		itemVo.setPriceAndCombos(priceAndCombos); // pass 6 or 5 model class as
													// per business type
		return itemVo;
	}

	@Override
	public List<Items> getAllItemByUser() {
		User user = loginUserDetail.getCurrentLoginUser();
		List<Items> items = itemRepository.findAllByUser(user);
		return items;
	}

	@Override
	public DaysAndHoursVO prepareDHModal(Long id) {
		DaysAndHoursVO daysAndHoursVO = new DaysAndHoursVO();
		List<DaysAndHours> daysAndHours = dayAndHoursRepository.findAllByTimeableIdAndTimeableType(id, "Item");
		daysAndHoursVO.setDaysAndHours(daysAndHours);
		if (daysAndHours.isEmpty()) {
			String day[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };
			for (int i = 0; i < 7; i++) {
				DaysAndHours daysAndhours = new DaysAndHours();
				daysAndhours.setDayName(day[i]);
				daysAndhours.setActive(false);
				daysAndhours.setTimeableId(id);
				daysAndhours.setTimeableType("Item");
				daysAndHoursVO.addDaysAndHours(daysAndhours);
			}
		}
		return daysAndHoursVO;
	}

	@Override
	public void saveAllDHItem(@Valid DaysAndHoursVO daysAndHoursVO) {
		User actualLoginUser = loginUserDetail.getActualLoginUser();
		String day[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };
		AtomicInteger atomicInteger = new AtomicInteger(0);
		daysAndHoursVO.getDaysAndHours().stream().filter(dh -> dh != null).forEach(u -> {
			u.setDayName(day[atomicInteger.getAndIncrement()]);
			u.setActive(true);
			u.setCreatedAt(new Date());
			u.setUpdateAt(new Date());
			u.setCreatedById(actualLoginUser.getId());
			u.setUpdateById(actualLoginUser.getId());
		});
		dayAndHoursRepository.saveAll(daysAndHoursVO.getDaysAndHours());
	}

}
