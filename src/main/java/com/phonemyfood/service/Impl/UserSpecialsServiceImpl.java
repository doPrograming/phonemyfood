package com.phonemyfood.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserSpecials;
import com.phonemyfood.repository.UserSpecialsRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.UserSpecialsService;

@Service
public class UserSpecialsServiceImpl implements UserSpecialsService {
	
	@Autowired
	private UserSpecialsRepository userSpecialsRepository;
	
	@Autowired
	private LoginUserDetail loginUserDetail;

	@Override
	public void saveSpecials(UserSpecials userSpecials) {
		User user = loginUserDetail.getCurrentLoginUser();
		userSpecials.setUser(user);
		userSpecialsRepository.save(userSpecials);
	}

	@Override
	public List<UserSpecials> getAllSpecialsByUser() {

		User user = loginUserDetail.getCurrentLoginUser();
		List<UserSpecials> userSpecials = userSpecialsRepository.findAllByUser(user);
		return userSpecials;
	}

	@Override
	public UserSpecials getSpecialsById(Long specialId) {
		return userSpecialsRepository.getOne(specialId);
	}
	
}
