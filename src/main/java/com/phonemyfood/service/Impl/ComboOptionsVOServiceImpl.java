package com.phonemyfood.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.ComboOptions;
import com.phonemyfood.db.Items;
import com.phonemyfood.db.vo.ComboOptionVO;
import com.phonemyfood.repository.ItemRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.ComboOptionsService;
import com.phonemyfood.service.ComboOptionsVOService;

@Service
public class ComboOptionsVOServiceImpl implements ComboOptionsVOService {
	@Autowired
	private ComboOptionsService comboOptionsService;
	
	@Autowired
	private ItemRepository ItemRepository;
	
	@Autowired
	private LoginUserDetail loginUserDetail;
	@Override
	public ComboOptionVO getAllComboDetails(Long id) {
		ComboOptionVO comboOptionVO = new ComboOptionVO();
		ComboOptions comboOptions = comboOptionsService.findOneByPriceAndCombos(id);
		comboOptions = comboOptions==null?new ComboOptions():comboOptions;
		List<Items> items = ItemRepository.findAllByUser(loginUserDetail.getCurrentLoginUser());
		comboOptionVO.setComboOptions(comboOptions);
		comboOptionVO.setPriceAndComboId(id);
		comboOptionVO.setOption1(items);
		comboOptionVO.setOption2(items);
		comboOptionVO.setOption3(items);
		comboOptionVO.setOption4(items);
		return comboOptionVO;
	}	
}
