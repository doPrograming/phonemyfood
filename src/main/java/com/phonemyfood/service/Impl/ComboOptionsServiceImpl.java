package com.phonemyfood.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.ComboOptions;
import com.phonemyfood.db.PriceAndCombos;
import com.phonemyfood.repository.ComboOptionsRepository;
import com.phonemyfood.repository.PriceAndCombosRepository;
import com.phonemyfood.service.ComboOptionsService;

@Service
public class ComboOptionsServiceImpl implements ComboOptionsService {

	@Autowired
	private ComboOptionsRepository comboOptionsRepository;
	
	@Autowired
	private PriceAndCombosRepository priceAndCombosRepository;
	
	@Override
	public ComboOptions findOneByPriceAndCombos(Long id) {
		PriceAndCombos priceAndCombos = priceAndCombosRepository.getOne(id);
		ComboOptions comboOptions = comboOptionsRepository.findByPriceCombos(priceAndCombos);
		return comboOptions;
	}

	@Override
	public void saveCombo(ComboOptions comboOptions) {
		comboOptionsRepository.save(comboOptions);
	}
	
}
