package com.phonemyfood.service.Impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Ingredients;
import com.phonemyfood.db.User;
import com.phonemyfood.repository.IngredientRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.IngredientService;

@Service
public class IngredientServiceImpl implements IngredientService {
	
	@Autowired
	private IngredientRepository ingredientRepository;
	
	@Autowired
	private LoginUserDetail loginUserDetail;
	
	@Override
	public List<Ingredients> getAllIngredientsByUser() {
		User user = loginUserDetail.getCurrentLoginUser();
		return ingredientRepository.findAllByUser(user);
	}

	@Override
	public void saveIngredient(@Valid Ingredients ingredients) {
		User user = loginUserDetail.getCurrentLoginUser();
		ingredients.setUser(user);
		ingredientRepository.save(ingredients);
	}

	@Override
	public void deleteIngredient(Long id) {
		try {
			ingredientRepository.deleteById(id);
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}

	@Override
	public List<Ingredients> findIngredientByUserAndName(String key) {
		User user = loginUserDetail.getCurrentLoginUser();
		return ingredientRepository.findIngredientByUserAndNameIgnoringCase(user,key.toUpperCase());
	}

	@Override
	public Ingredients findOne(Long id) {
		Ingredients ingredients=ingredientRepository.getOne(id);
		return ingredients;
	}
}
