package com.phonemyfood.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserMenu;
import com.phonemyfood.repository.UserMenusRepository;
import com.phonemyfood.service.UserMenusService;

@Service
public class UserMenusServiceImpl implements UserMenusService{
	
	@Autowired
	UserMenusRepository userMenusRepository;

	@Override
	public Page<UserMenu> findLast3ByUser(User actualLoginUser, Pageable pageable) {
		return userMenusRepository.findLast3ByUser(actualLoginUser, pageable);
	}

	@Override
	public List<UserMenu> findAllByUser(User user) {
		return userMenusRepository.findAllByUser(user);
	}

	@Override
	public UserMenu getOne(long id) {
		return userMenusRepository.getOne(id);
	}

	@Override
	public UserMenu save(UserMenu userMenu) {
		return userMenusRepository.save(userMenu);
	}
}