package com.phonemyfood.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Categories;
import com.phonemyfood.db.MenuCategoryItem;
import com.phonemyfood.db.User;
import com.phonemyfood.repository.CategoriesRepository;
import com.phonemyfood.service.CategoriesService;
import com.phonemyfood.service.MenuCategoryItemsService;

@Service
public class CategoriesServiceImpl implements CategoriesService {

	@Autowired
	private CategoriesRepository categoiesRepository;
	
	@Autowired
	private MenuCategoryItemsService menuCategoryItemsService;
	
	@Override
	public List<Categories> getAllCategoryByUser(User user) {
		return categoiesRepository.findAllByUser(user);
	}

	@Override
	public Categories getOne(Long id) {
		return categoiesRepository.getOne(id);
	}

	@Override
	public void save(Categories categories) {
		categoiesRepository.save(categories);
	}
	
	@Override
	public void save(Categories categories, String name, Long id, User user) {
		categories.setName(name);
		categories.setId(id);
		categories.setUser(user);
		categories.setMessage("");
		categoiesRepository.save(categories);
	}

	@Override
	public void delete(Long id) {
		categoiesRepository.deleteById(id);
	}

	@Override
	public List<Categories> categoryExist(User user, String name) {
		return categoiesRepository.categoryExist(name, user);
	}

	@Override
	public String deleteCategory(Long id) {
		List<MenuCategoryItem> menus = menuCategoryItemsService.findAllByCategoryId(id);
		if(menus.size()!=0) {
			for(int i=0;i<menus.size();i++) {
				if(!menus.get(i).getItems().isEmpty()) {
					return "This category is already in use in items";
				}
				else if(menus.get(i).getItems().isEmpty())
					menuCategoryItemsService.delete(menus.get(i));
			}
		}
		categoiesRepository.deleteById(id);
		return "Deleted Successfully";
	}
}
