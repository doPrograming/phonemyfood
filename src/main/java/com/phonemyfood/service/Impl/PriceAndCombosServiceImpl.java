package com.phonemyfood.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.PriceAndCombos;
import com.phonemyfood.repository.PriceAndCombosRepository;
import com.phonemyfood.service.PriceAndCombosService;

@Service
public class PriceAndCombosServiceImpl implements PriceAndCombosService {
	
	@Autowired
	PriceAndCombosRepository priceAndCombosRepository;

	@Override
	public List<PriceAndCombos> findAllByItemAndUserMenuId(Items itemModel, long userMenuId) {
		return priceAndCombosRepository.findAllByItemAndUserMenuId(itemModel, userMenuId);
	}

	@Override
	public List<PriceAndCombos> findAllByItemOrderById(Items itemsModal) {
		return priceAndCombosRepository.findAllByItemOrderById(itemsModal);
	}

	@Override
	public void saveAll(List<PriceAndCombos> priceAndCombos) {
		priceAndCombosRepository.saveAll(priceAndCombos);
	}

	@Override
	public void deleteAllByItem(Items item) {
		priceAndCombosRepository.removeByItem(item);
	}

}
