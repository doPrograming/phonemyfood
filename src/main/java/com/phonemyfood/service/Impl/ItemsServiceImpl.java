package com.phonemyfood.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.User;
import com.phonemyfood.repository.ItemRepository;
import com.phonemyfood.service.DaysAndHoursVOService;
import com.phonemyfood.service.ItemsService;
import com.phonemyfood.service.PriceAndCombosService;

@Service
public class ItemsServiceImpl implements ItemsService {
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private PriceAndCombosService priceAndCombosService;
	
	@Autowired
	private DaysAndHoursVOService daysAndHoursVOService;

	@Override
	public Items getSelectedItemDetail(Long id) {
		return itemRepository.getOne(id);
	}

	@Override
	public void saveitem(Items items) {
		itemRepository.save(items);
	}

	@Override
	public List<Items> getAllItems(User user) {
		return itemRepository.findAllByUserAndHideItem(user, false);
	}

	@Override
	public Items findByIdAndKey(long id, String key) {
		return itemRepository.findByIdAndKey(id, key);
	}

	@Override
	public Items getOne(long itemid) {
		return itemRepository.getOne(itemid);
	}

	@Override
	public List<Items> findAllByUserAndName(User currentLoginUser, String key) {
		return itemRepository.findAllByUserAndName(currentLoginUser, key);
	}

	@Override
	public List<Items> findAllByUser(User currentLoginUser) {
		return itemRepository.findAllByUser(currentLoginUser);
	}

	@Override
	public void deleteItem(Items item) {
		daysAndHoursVOService.deleteAllByTimeableIdAndTimeableType(item.getId(),"Item");
		priceAndCombosService.deleteAllByItem(item);
		itemRepository.deleteById(item.getId());
	}
}
