package com.phonemyfood.service.Impl;

import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.MenuCategoryItem;
import com.phonemyfood.db.UserMenu;
import com.phonemyfood.repository.MenuCategoryItemsRepository;
import com.phonemyfood.service.MenuCategoryItemsService;

@Service
public class MenuCategoryItemsServiceImpl implements MenuCategoryItemsService {

	@Autowired
	private MenuCategoryItemsRepository menuCategoryItemsRepository;

	@Override
	public List<MenuCategoryItem> findAll() {
		return menuCategoryItemsRepository.findAll();
	}

	@Override
	public void save(MenuCategoryItem menuCategoryItem) {
		menuCategoryItemsRepository.save(menuCategoryItem);
	}

	@Override
	public MenuCategoryItem findAllByMenuIdAndCategoryId(long menuId, long categoryId) {
		return menuCategoryItemsRepository.findAllByMenuIdAndCategoryId(menuId, categoryId);
	}

	@Override
	public void delete(MenuCategoryItem menuCategoryItem) {
		menuCategoryItemsRepository.delete(menuCategoryItem);
	}

	@Override
	public Set<Items> findItemsByUserMenus(List<UserMenu> userMenus) {
		Set<Items> items = new TreeSet<Items>(new Comparator<Items>() {
			@Override
			public int compare(Items i1, Items i2) {
				return (i1.getName()).compareTo(i2.getName());
			}
		});
		for(int i = 0;i<userMenus.size();i++) {
			items.addAll(menuCategoryItemsRepository.findItemByUserMenus(userMenus.get(i)));
		}
		return items;
	}

	@Override
	public Set<BigInteger> findByMenuCategoryItem(long menuCategoryItemId) {
		return menuCategoryItemsRepository.findByMenuCategoryItem(menuCategoryItemId);
	}

	@Override
	public List<MenuCategoryItem> findAllByCategoryId(Long id) {
		return menuCategoryItemsRepository.findAllByCategoryId(id);
	}
}