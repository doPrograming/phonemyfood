package com.phonemyfood.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserDeliveries;
import com.phonemyfood.repository.UserDeliveryRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.UserDeliveryService;

@Service
public class UserDeliveryServiceImpl implements UserDeliveryService {
	@Autowired
	private UserDeliveryRepository userDeliveryRepository;
	
	@Autowired
	private LoginUserDetail loginUserDetail;

	@Override
	public List<UserDeliveries> getAllUserDelivery() {
		User user = loginUserDetail.getCurrentLoginUser();
				
		return userDeliveryRepository.findAllByUser(user);
	}

	@Override
	public void delete(UserDeliveries deliveries) {
		userDeliveryRepository.delete(deliveries);
		
	}

	@Override
	public void saveAll(List<UserDeliveries> userDeliveries) {
		User user = loginUserDetail.getCurrentLoginUser();
		userDeliveries.stream()
			.forEach(uD -> {
				uD.setUser(user);
			});
		userDeliveryRepository.saveAll(userDeliveries);
	}

	@Override
	public UserDeliveries getOne(Long id) {
		return userDeliveryRepository.getOne(id);
	}
	
	
}
