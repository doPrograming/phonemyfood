package com.phonemyfood.service.Impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Rewards;
import com.phonemyfood.db.User;
import com.phonemyfood.db.vo.RewardsVO;
import com.phonemyfood.repository.RewardsRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.RewardsService;

@Service
public class RewardsServiceImpl implements RewardsService{
	
	@Autowired
	private RewardsRepository rewardsRepository;
	
	@Autowired
	private LoginUserDetail loginUserDetail;
	
	@Override
	public List<Rewards> getAllRewards() {
		User user = loginUserDetail.getCurrentLoginUser();
		return rewardsRepository.findAllByUser(user);
	}

	@Override
	public void updateRewards(RewardsVO rewardsVO) {
		User user = loginUserDetail.getCurrentLoginUser();
		User actualUser = loginUserDetail.getActualLoginUser();
		for (int i = 0; i < rewardsVO.getRewards().size(); i++) {
			if (rewardsVO.getRewards().get(i).getRewardValue() == null || rewardsVO.getRewards().get(i).getRewardValue().equals(null)
					|| rewardsVO.getRewards().get(i).getRewardValue().equals("")) {
				rewardsVO.getRewards().get(i).setRewardValue("");
				rewardsVO.getRewards().get(i).setValue1("");
				rewardsVO.getRewards().get(i).setUser(user);
				rewardsVO.getRewards().get(i).setCreatedAt(new Date());
				rewardsVO.getRewards().get(i).setUpdateAt(new Date());
				rewardsVO.getRewards().get(i).setCreatedById(actualUser.getId());
				rewardsVO.getRewards().get(i).setUpdateById(actualUser.getId());
			} else if (rewardsVO.getRewards().get(i).getRewardValue() != "") {
				rewardsVO.getRewards().get(i).setActive(true);
				rewardsVO.getRewards().get(i).setUser(user);
				rewardsVO.getRewards().get(i).setCreatedAt(new Date());
				rewardsVO.getRewards().get(i).setUpdateAt(new Date());
				rewardsVO.getRewards().get(i).setCreatedById(actualUser.getId());
				rewardsVO.getRewards().get(i).setUpdateById(actualUser.getId());
			}
		}
		rewardsRepository.saveAll(rewardsVO.getRewards());
	}

	@Override
	public Rewards getOne(Long id) {
		return rewardsRepository.getOne(id);
	}
}