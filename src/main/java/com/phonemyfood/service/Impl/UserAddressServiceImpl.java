package com.phonemyfood.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserAbouts;
import com.phonemyfood.db.UserAddresses;
import com.phonemyfood.repository.UserAddressesRepository;
import com.phonemyfood.service.UserAddressService;
@Service
public class UserAddressServiceImpl implements UserAddressService{
	
	@Autowired
	private UserAddressesRepository userAddressesRepository;

	@Override
	public UserAddresses findByUser(User user) {
		return userAddressesRepository.findByUser(user);
	}

	@Override
	public void updateUserAddress(UserAbouts userAbouts, UserAddresses userAddresses) {
		userAddresses.setAddress(userAbouts.getAddress());
		userAddresses.setCity(userAbouts.getCity());
		userAddresses.setPhoneNumber(userAbouts.getPhone());
		userAddresses.setState(userAbouts.getState());
		userAddresses.setZip(userAbouts.getZip());
		userAddresses.setUser(userAbouts.getUser());
		userAddresses.setDescription(userAbouts.getDescription());
		userAddressesRepository.save(userAddresses);
	}

	@Override
	public UserAddresses save(UserAddresses userAddresses) {
		return userAddressesRepository.save(userAddresses);
	}

}
