package com.phonemyfood.service.Impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.phonemyfood.db.User;
import com.phonemyfood.db.vo.UserBusinessVo;
import com.phonemyfood.service.EmailService;
import com.phonemyfood.util.SignupBean;

@Service
public class EmailServiceImpl implements EmailService {

	private final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private TemplateEngine templateEngine;

	@Override
	public void sendSignupEmail(HttpServletRequest request, UserBusinessVo userBusinessVo, String token) {
		final String appUrl = request.getRequestURL().toString().replace("/admin/addEditClients", "");
		final String recipientAddress = userBusinessVo.getUser().getEmail();
		final String subject = "Welcome to Phone My Food";
		final String confirmationUrl = appUrl + "/pmf/" + token;
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom("support@phonemyfood.com");
			messageHelper.setTo(recipientAddress);
			messageHelper.setSubject(subject);
			messageHelper.setText(buildSignupEmail(new SignupBean(userBusinessVo.getUser().getName(), confirmationUrl, userBusinessVo.getBusinesses().getBusinessType())), true);
		};

		userBusinessVo.getUser().setPasswordReset(true);
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			LOG.error("Error while sending signUp email to '{}' !", recipientAddress);
		}
	}

	@Override
	public void sendResetPasswordEmail(HttpServletRequest request, User user) {
		String appUrl = request.getRequestURL().toString().replace("/admin/resendPassword", "");
		String confirmationUrl = appUrl + "/pmf/" + user.getResetToken();
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom("support@itraino.com");
			messageHelper.setTo(user.getEmail());
			messageHelper.setSubject("Reset your password!!");
			messageHelper.setText(buildSignupEmail(new SignupBean(user.getName(), confirmationUrl, "")), true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			LOG.error("Error while sending reset password email to '{}' !", user.getEmail());
		}
	}

	private String buildSignupEmail(SignupBean signupBean) {
		Context context = new Context();
		context.setVariable("fullName", signupBean.getFullName());
		context.setVariable("firstName", signupBean.getFullName().split(" ")[0]);
		context.setVariable("confirmationUrl", signupBean.getConfirmationUrl());
		context.setVariable("businessType", signupBean.getBusinessType());
		return templateEngine.process("SignUpMailTemplate", context);
	}

	@Override
	public void sendLoginUserDetailsEmail(String userName,String email,String password) {
		String subject = "Phone My Food - Your Login Details";
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom("support@phonemyfood.com");
			messageHelper.setTo(email);
			messageHelper.setSubject(subject);
			messageHelper.setText(buildLoginUserDatailsEmail(userName,email,password), true);
		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {
			LOG.error("Error while sending 'login details' email to '{}' !", email);
		}
	}
	
	private String buildLoginUserDatailsEmail(String userName,String email,String password) {
		Context context = new Context();
		context.setVariable("firstName", userName.split(" ")[0]);
		context.setVariable("email", email);
		context.setVariable("password", password);
		return templateEngine.process("loginUserDetailsEmailTemplate", context);
	}
	
}
