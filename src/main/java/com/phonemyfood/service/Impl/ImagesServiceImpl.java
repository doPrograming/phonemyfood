package com.phonemyfood.service.Impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Images;
import com.phonemyfood.db.User;
import com.phonemyfood.repository.ImagesRepository;
import com.phonemyfood.service.ImagesService;

@Service
@Repository
@Transactional
public class ImagesServiceImpl implements ImagesService {
	
	@Autowired
	private ImagesRepository imagesRepository;
	
	@Autowired
	private EntityManager entityManager;
	
	public Long insert(Images images){
		entityManager.persist(images);
		return images.getId();
	}
	
	@Override
	public void saveImages(Images images) {
		imagesRepository.saveAndFlush(images);
	}

	/*@Override
	public List<Images> getImagesByImageableTypeAndImageableId(String string, Long id) {
		return imagesRepository.findAllByImageableTypeAndImageableId(string,id);
	}*/

	@Override
	public Images findOneByid(Long id) {
		return imagesRepository.getOne(id);
	}

	@Override
	public List<Images> getImagesByImageableTypeAndImageableIdAndUser(String string, Long id, User user) {
		return imagesRepository.findAllByImageableTypeAndImageableIdAndUser(string,id,user);
	}
	
	@Override
	public Images getImagesByImageableTypeAndImageableId(String string, Long id, User user) {
		List<Images> images = imagesRepository.getImagesByImageableTypeAndImageableId(string,id,user);
		if(images.size()==0) {
			return null;
		}
		return images.get(0);
	}

	@Override
	public void deleteImage(Images images) {
		imagesRepository.delete(images);
	}

	@Override
	public Images findByImageableTypeAndUser(String string, User user) {
		return imagesRepository.findByImageableTypeAndUser(string, user);
	}

	@Override
	public Images getImageByPicturAndImageableId(String picture ,long digitalmenuId) {
		return imagesRepository.getImageByPicturAndImageableId(picture,digitalmenuId);
	}

	@Override
	public List<Images> getImagesByTypeAndUser(String string, User user) {
		return imagesRepository.findByTypeAndUser(string, user);
	}
}
