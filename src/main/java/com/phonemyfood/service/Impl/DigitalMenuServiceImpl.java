package com.phonemyfood.service.Impl;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.DigitalMenu;
import com.phonemyfood.db.Images;
import com.phonemyfood.db.Items;
import com.phonemyfood.db.User;
import com.phonemyfood.db.vo.DigitalMenuItemVo;
import com.phonemyfood.db.vo.DigitalMenuVo;
import com.phonemyfood.repository.DigitalMenuRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.DigitalMenuService;
import com.phonemyfood.service.ImagesService;
import com.phonemyfood.service.ItemsService;
import com.phonemyfood.user.controller.BaseController;
import com.phonemyfood.util.ImageUtils;

@Service
public class DigitalMenuServiceImpl extends BaseController implements DigitalMenuService{
	
	@Value("${phonemyfood.images.common.path}")
	private String commonPath;

	@Autowired
	private LoginUserDetail loginUserDetail;
	
	@Autowired
	private DigitalMenuRepository digitalMenuRepository;
	
	@Autowired
	private ImageUtils imageUtils;
	
	@Autowired
	private ImagesService imagesService;
	
	@Autowired
	private ItemsService itemsService;
		
	@Override
	public void save(DigitalMenu digitalMenu) {
		digitalMenuRepository.save(digitalMenu);
	}

	@Override
	public List<DigitalMenu> findAllByBusinesses(Businesses businesses) {
		return digitalMenuRepository.findAllByBusinesses(businesses);
	}

	@Override
	public DigitalMenu getOne(long id) {
		return digitalMenuRepository.getOne(id);
	}

	@Override
	public void saveDigitalMenuImage(String imageString, String screennumber, long digitalmenuId, String digitalmenuItemId, String croppingFramePath) {
		String pathImage;
		byte[] imageByte = null;
		try {
			imageByte = org.apache.commons.codec.binary.Base64.decodeBase64(imageString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Images images1 = imagesService.getImageByPicturAndImageableId("screen" + screennumber + "_" + digitalmenuItemId, digitalmenuId);
		Images images = new Images();
		images.setImageableType("DigitalMenuItem");
		User user = loginUserDetail.getCurrentLoginUser();
		images.setUser(user);
		images = imageUtils.generateURL(images);
		if (images1 != null) {
			images.setId(images1.getId());
		} else {
			Long persistId = imagesService.insert(images);
			images.setId(persistId);
		}
		images.setImageableId(digitalmenuId);
		images = imageUtils.uploadImage(imageByte, "screen" + screennumber + "_" + digitalmenuItemId + ".png", images);
		pathImage = commonPath + "//images//" + images.getUser().getId() + "//DigitalMenuItem//" + images.getPicture();
		BufferedImage img = ImageUtils.loadImage(pathImage);
		ImageUtils.saveImage(img, pathImage, croppingFramePath);
		imagesService.saveImages(images);
	}

	@Override
	public void saveLinkedMenus(DigitalMenu digitalMenu, List<DigitalMenu> digitalMenus) {
		for(int i = 0; i < digitalMenus.size(); i++) {
			digitalMenus.get(i).setMenuType(digitalMenu.getMenuType());
			digitalMenus.get(i).setMenuDesign(digitalMenu.getMenuDesign());
			digitalMenus.get(i).setUserMenus(digitalMenu.getUserMenus());
			digitalMenus.get(i).setOrientation(digitalMenu.getOrientation());
			//digitalMenus.get(i).setLayout(digitalMenu.getLayout());
			digitalMenus.get(i).setColorPicker(digitalMenu.getColorPicker());
			digitalMenus.get(i).setPageDuration(digitalMenu.getPageDuration());
			digitalMenus.get(i).setPhotoDuration(digitalMenu.getPhotoDuration());
			digitalMenus.get(i).setPromomsg1(digitalMenu.getPromomsg1());
			digitalMenus.get(i).setPromomsg2(digitalMenu.getPromomsg2());
			digitalMenus.get(i).setPromomsg3(digitalMenu.getPromomsg3());
			digitalMenuRepository.save(digitalMenus.get(i));
		}
	}

	@Override
	public void linkDigitalMenus(List<Long> ids, long c_id) {
		DigitalMenu parentDigitalMenu = getOne(c_id);
		List<DigitalMenu> linkedMenuList = digitalMenuRepository.findAllByLinkedWithMenu(parentDigitalMenu);
		for (DigitalMenu digiMenu : linkedMenuList) {
			digiMenu.setLinkedWithMenu(null);
		}
		digitalMenuRepository.saveAll(linkedMenuList);
		if (linkedMenuList != null && ids.get(0) != 0) {
			List<DigitalMenu> digitalmenus = digitalMenuRepository.findAllDigitalMenusByIds(ids);
			for (DigitalMenu digiMenu : digitalmenus) {
				digiMenu.setLinkedWithMenu(parentDigitalMenu);
			}
			digitalMenuRepository.saveAll(digitalmenus);
		}
	}

	@Override
	public List<DigitalMenu> findAllDigitalMenusByIds(List<Long> ids){
		return digitalMenuRepository.findAllDigitalMenusByIds(ids);
	}
	
	@Override
	public List<DigitalMenu> findAllByLinkedWithMenu(DigitalMenu digitalMenu) {
		return digitalMenuRepository.findAllByLinkedWithMenu(digitalMenu);
	}
	
	@Override
	public DigitalMenu findByBusinessesAndScreen(Businesses businesses, String screenNumber) {
		return digitalMenuRepository.findByBusinessesAndScreen(businesses, screenNumber);
	}

	@Override
	public List<DigitalMenu> findAllByBusinessesAndNotLinkedWithMenu(Businesses businesses) {
		return digitalMenuRepository.findAllByBusinessesAndNotLinkedWithMenu(businesses);
	}

	@Override
	public void setLinkedMenus(DigitalMenu digitalMenu) {
		List<DigitalMenu> digitalMenus = digitalMenuRepository.findAllByBusinesses(findBusinessesByLoginUser());
		List<DigitalMenu> digitalMenus2 = new ArrayList<>();
		for(DigitalMenu digiMenu : digitalMenus) {
			if(digiMenu.getLinkedWithMenu()!=null && digiMenu.getLinkedWithMenu().getId() == digitalMenu.getId())
				digitalMenus2.add(digiMenu);
		}
		if(digitalMenus2.size()!=0)
			saveLinkedMenus(digitalMenu, digitalMenus2);
	}

	@Override
	public List<DigitalMenuVo> getDigitalMenuVos() {
		List<DigitalMenu> digitalMenus=digitalMenuRepository.findAllByBusinesses(findBusinessesByLoginUser());
		List<DigitalMenuVo> digitalMenuVos=new ArrayList<>();
		DigitalMenuVo digitalMenuVo;
		for(DigitalMenu digitalMenu:digitalMenus) {   
			digitalMenuVo = new DigitalMenuVo();
			digitalMenuVo.setBusinesses(digitalMenu.getBusinesses().getName());
			digitalMenuVo.setColorPicker(digitalMenu.getColorPicker().getColorName());
			digitalMenuVo.setLayout(digitalMenu.getLayout());
			digitalMenuVo.setMenuType(digitalMenu.getMenuType().toString());
			digitalMenuVo.setMenuDesign(digitalMenu.getMenuDesign().toString());
			digitalMenuVo.setOrientation(digitalMenu.getOrientation().toString());
			digitalMenuVo.setPageDuration(digitalMenu.getPageDuration());
			digitalMenuVo.setPhotoDuration(digitalMenu.getPhotoDuration());
			digitalMenuVo.setPromomsg1(digitalMenuVo.getPromomsg1());
			digitalMenuVo.setPromomsg2(digitalMenuVo.getPromomsg2());
			digitalMenuVo.setPromomsg3(digitalMenuVo.getPromomsg3());
		//	digitalMenuVo.setUserMenuName(digitalMenu.getUserMenu().getMenuName());
		//	digitalMenuVo.setUserMenuId(digitalMenu.getUserMenu().getId());
			digitalMenuVo.setDigitalMenuItemVos(getDigitalMenuItemVo(digitalMenu.getId()));
			digitalMenuVos.add(digitalMenuVo);
		}
		return digitalMenuVos;
	}

	@Override
	public List<DigitalMenuItemVo> getDigitalMenuItemVo(Long id) {
		DigitalMenuItemVo digitalMenuItemVo = null;
		Images itemImage;
		Items items;
		List<DigitalMenuItemVo> digitalMenuItemVos = new ArrayList<>();
		User user = loginUserDetail.getCurrentLoginUser();
		List<Images> digitalMenuItemImages = imagesService.getImagesByImageableTypeAndImageableIdAndUser("DigitalMenuItem", id, user);
		for (Images images : digitalMenuItemImages) {
			digitalMenuItemVo = new DigitalMenuItemVo();
			digitalMenuItemVo.setDigitalMenuItemImage(images.getPicture());
			String itemData[] = images.getPicture().split("_");
			items = itemsService.getSelectedItemDetail(Long.parseLong(itemData[1]));
			itemImage = imagesService.getImagesByImageableTypeAndImageableId("Item", items.getId(), user);
			if (itemImage != null) {
				digitalMenuItemVo.setItemId(itemImage.getImageableId());
				digitalMenuItemVo.setItemImage(itemImage.getPicture());
				digitalMenuItemVo.setItemName(items.getName());
			}
			for(Images img:digitalMenuItemImages) {
				if(img !=null) {
					digitalMenuItemVo.setDigitalMenuItemImage(img.getPicture());
				}
			}
			digitalMenuItemVos.add(digitalMenuItemVo);
		}
		return digitalMenuItemVos;
	}

	@Override
	public List<DigitalMenu> getValidDigitalMenusForScreen(String screenNumber,Businesses businesses) {
		List<DigitalMenu> digitalMenus = digitalMenuRepository.findAllByBusinesses(businesses);
		DigitalMenu currentDigitalMenu = digitalMenuRepository.findByBusinessesAndScreen(businesses, screenNumber);
		List<DigitalMenu> validDigitalMenus = digitalMenuRepository.findAllByBusinessesAndNotLinkedWithMenu(businesses);
		validDigitalMenus.addAll(digitalMenuRepository.findAllByLinkedWithMenu(currentDigitalMenu));
		for (int i = 0; i < digitalMenus.size(); i++) {
			for (int x = 0; x < validDigitalMenus.size(); x++) {
				if (digitalMenus.get(i).getLinkedWithMenu() != null && digitalMenus.get(i).getLinkedWithMenu() == validDigitalMenus.get(x)) {
					validDigitalMenus.remove(validDigitalMenus.get(x));
				}
			}
		}
		if (!validDigitalMenus.contains(currentDigitalMenu)) {
			validDigitalMenus.add(currentDigitalMenu);
		}
		return validDigitalMenus;
	}
	
	public void deleteDigitalMenu(Long id) {
		DigitalMenu digitalMenu=digitalMenuRepository.getOne(id);
		List<DigitalMenu> digitalMenus=digitalMenuRepository.findAllByBusinesses(findBusinessesByLoginUser());
		int screenNumber=Integer.parseInt(digitalMenu.getScreen());
		for(int i=0;i<digitalMenus.size();i++) {
			if(digitalMenu.equals(digitalMenus.get(i).getLinkedWithMenu())) { 
				digitalMenus.get(i).setLinkedWithMenu(null);
			}
		}
		digitalMenuRepository.deleteById(id);
		for(int i=0;i<digitalMenus.size();i++) {
			int j=Integer.parseInt(digitalMenus.get(i).getScreen());
			if(screenNumber<j) {
				digitalMenus.get(i).setScreen(String.valueOf(j-1));
				digitalMenuRepository.save(digitalMenus.get(i));
			}
		}
	}
}