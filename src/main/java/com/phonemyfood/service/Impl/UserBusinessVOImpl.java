package com.phonemyfood.service.Impl;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.DaysAndHours;
import com.phonemyfood.db.User;
import com.phonemyfood.db.UserAbouts;
import com.phonemyfood.db.UserAddresses;
import com.phonemyfood.db.vo.UserBusinessVo;
import com.phonemyfood.repository.BusinessesRepository;
import com.phonemyfood.repository.DayAndHoursRepository;
import com.phonemyfood.repository.RoleRepository;
import com.phonemyfood.security.UserService;
import com.phonemyfood.service.EmailService;
import com.phonemyfood.service.UserAboutsService;
import com.phonemyfood.service.UserAddressService;
import com.phonemyfood.service.UserBusinessVOService;

@Service
public class UserBusinessVOImpl implements UserBusinessVOService {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserAddressService userAddressService;

	@Autowired
	private BusinessesRepository businessesRepository;

	@Autowired
	private DayAndHoursRepository dayAndHoursRepository;
	
	@Autowired
	private UserAboutsService userAboutsService;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveAll(@Valid UserBusinessVo userBusinessVo, HttpServletRequest request) throws Exception {
		String token = UUID.randomUUID().toString();
		
		User user = userBusinessVo.getUser();
		if (userService.emailAlreadyExist(user))
			throw new Exception("Email Already exist");
			
		user.setName(user.getName() + " " + user.getLastName());
		user.setRoles(roleRepository.findAllByName("ROLE_USER"));
		if (user.getId() == 0) {	
			user.setPassword(passwordEncoder.encode("password"));
			user.setResetToken(token);
			emailService.sendSignupEmail(request, userBusinessVo, token);
		}else {
			User user1 = userService.get(user.getId());
			if(!(user1.getEmail().equals(user.getEmail())) ) {
				user.setResetToken(token);
				emailService.sendSignupEmail(request, userBusinessVo, token);
			}
			user.setPassword(user1.getPassword());
		}
		userService.save(user);

		userBusinessVo.getBusinesses().setUser(user);
		userBusinessVo.getUserAddresses().setUser(user);
		UserAddresses  userAddresses =userAddressService.save(userBusinessVo.getUserAddresses());
		
		UserAbouts userAbouts = userAboutsService.findById(user.getId());
		
		userAbouts = userAbouts != null ? userAbouts : new UserAbouts();
		userAbouts.setId(userAbouts.getId());
		userAbouts.setAddress(userAddresses.getAddress());
		userAbouts.setCity(userAddresses.getCity());
		userAbouts.setState(userAddresses.getState());
		userAbouts.setPhone(userAddresses.getPhoneNumber());
		userAbouts.setUser(user);
		userAbouts.setEmail(user.getEmail());
		userAbouts.setZip(userAddresses.getZip());
		
		userAboutsService.saveUserAbout(userAbouts);
		
		Businesses businesses = businessesRepository.save(userBusinessVo.getBusinesses());
		String day[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };
		AtomicInteger atomicInteger = new AtomicInteger(0);
		userBusinessVo.getDaysAndHours().stream()
		.filter(u -> u != null)
		.forEach(u -> {
			u.setDayName(day[atomicInteger.getAndIncrement()]);
			u.setActive(true);
			u.setTimeableType("Businesses");
			u.setTimeableId(businesses.getId());
			
		});
		dayAndHoursRepository.saveAll(userBusinessVo.getDaysAndHours());
	}	
	
	@Override
	public List<Businesses> findAllBusiness() {
		return businessesRepository.findAll();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public UserBusinessVo findBusinessById(Long id) {
		UserBusinessVo userBusinessVo = new UserBusinessVo();
		Businesses businesses = businessesRepository.getOne(id);
		User user = userService.get(businesses.getUser().getId());
		UserAddresses userAddresses = userAddressService.findByUser(user);	
		List<DaysAndHours> daysAndHours = dayAndHoursRepository.findAllByTimeableIdAndTimeableType(businesses.getId(),"Businesses");
		if (daysAndHours.isEmpty()) {
			String day[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };
			userBusinessVo.setDaysAndHours(daysAndHours);
			for (int i = 0; i < 7; i++) {
				DaysAndHours daysAndhours = new DaysAndHours();
				daysAndhours.setDayName(day[i]);
				daysAndhours.setActive(false);
				userBusinessVo.addDaysAndHours(daysAndhours);
			}
			daysAndHours = userBusinessVo.getDaysAndHours();
		}
		userBusinessVo.setBusinesses(businesses);
		userBusinessVo.setUser(user);
		userBusinessVo.setUserAddresses(userAddresses);
		userBusinessVo.setDaysAndHours(daysAndHours);
		return userBusinessVo;
	}

	@Override
	public List<Businesses> findBusinessByName(String key) {
		return businessesRepository.findAllByNameIgnoringCase(key.toUpperCase());
	}

	@Override
	public Businesses findBusinessByUser(User user) {
		return businessesRepository.findByUser(user);
	}
}
