package com.phonemyfood.service.Impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.User;
import com.phonemyfood.repository.BusinessesRepository;
import com.phonemyfood.service.BusinessesService;

@Service
public class BusinessesServiceImpl implements BusinessesService{

	private final Logger LOG = LoggerFactory.getLogger(BusinessesServiceImpl.class);

	@Autowired
	BusinessesRepository businessesRepository;

	@Autowired
	BusinessesService businessesService;
	
	@Override
	public Businesses findByUser(User user) {
		return businessesRepository.findByUser(user);
	}

	@Override
	public Businesses getOne(long id) {
		return businessesRepository.getOne(id);
	}

	@Override
	public void save(Businesses businesses) {
		businessesRepository.save(businesses);
	}

	@Override
	public void deleteBusinesses(long id) {
		Businesses businesses = businessesService.getOne(id);
		businesses.setDeleted(true);
		businesses.getUser().setActive(false);
		LOG.info("User " + businesses.getName() + " got logically deleted");
		businessesService.save(businesses);
	}
}
