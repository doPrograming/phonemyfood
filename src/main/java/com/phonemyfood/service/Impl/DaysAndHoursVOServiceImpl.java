package com.phonemyfood.service.Impl;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.DaysAndHours;
import com.phonemyfood.db.User;
import com.phonemyfood.db.vo.DaysAndHoursVO;
import com.phonemyfood.repository.DayAndHoursRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.BusinessesService;
import com.phonemyfood.service.DaysAndHoursVOService;
import com.phonemyfood.util.Utility;

@Service
public class DaysAndHoursVOServiceImpl implements DaysAndHoursVOService {

	@Autowired
	private LoginUserDetail loginUserDetail;

	@Autowired
	private DayAndHoursRepository dayAndHoursRepository;

	@Autowired
	BusinessesService businessesService;

	@Override
	public void saveDaysAndHoursAbout(DaysAndHoursVO daysAndHoursVO) {
		String day[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };
		AtomicInteger atomicInteger = new AtomicInteger(0);
		User user = loginUserDetail.getCurrentLoginUser();
		daysAndHoursVO.getDaysAndHours().stream().forEach(Dah -> {
			Businesses businesses = businessesService.findByUser(user);
			Dah.setTimeableType("Businesses");
			Dah.setTimeableId(businesses.getId());
			Dah.setDayName(day[atomicInteger.getAndIncrement()]);
			Dah.setActive(true);
			Dah.setCreatedAt(new Date());
			Dah.setUpdateAt(new Date());
			Dah.setCreatedById(user.getId());
			Dah.setUpdateById(user.getId());
		});
		dayAndHoursRepository.saveAll(daysAndHoursVO.getDaysAndHours());
	}

	@Override
	public void deleteAll(List<DaysAndHours> daysAndHours) {
		dayAndHoursRepository.deleteAll(daysAndHours);
	}

	@Override
	public void deleteAllByTimeableIdAndTimeableType(long id, String string) {
		dayAndHoursRepository.removeByTimeableIdAndTimeableType(id, string);
	}

	@Override
	public boolean isBusinessHours() {
		User user = loginUserDetail.getCurrentLoginUser();
		Businesses businesses = businessesService.findByUser(user);
		List<DaysAndHours> daysAndHours = dayAndHoursRepository.findAllByTimeableIdAndTimeableType(businesses.getId(), "Businesses");
		return daysAndHours.stream().anyMatch(dayHour -> !dayHour.getStartTime().isEmpty() && !dayHour.getEndTime().isEmpty());
	}

	@Override
	public boolean isEqualToBusinessHours(String startTime, String endTime, int index) {
		boolean flag = false;
		User user = loginUserDetail.getCurrentLoginUser();
		Businesses businesses = businessesService.findByUser(user);
		List<DaysAndHours> daysAndHours = dayAndHoursRepository.findAllByTimeableIdAndTimeableType(businesses.getId(), "Businesses");
		if (index != 7) {
			if (daysAndHours.get(index).getStartTime() != null && !daysAndHours.get(index).getStartTime().trim().isEmpty()
					&& daysAndHours.get(index).getEndTime() != null && !daysAndHours.get(index).getEndTime().trim().isEmpty()) {
				if (Utility.isTimeEquals(startTime, endTime, daysAndHours.get(index).getStartTime(), daysAndHours.get(index).getEndTime())) {
					flag = true;
				}
			}
		} else {
			for (int i = 0; i <= daysAndHours.size() - 1; i++) {
				if (daysAndHours.get(i).getStartTime() != null && !daysAndHours.get(i).getStartTime().trim().isEmpty()
						&& daysAndHours.get(i).getEndTime() != null && !daysAndHours.get(i).getEndTime().trim().isEmpty()) {
					if (Utility.isTimeEquals(startTime, endTime, daysAndHours.get(i).getStartTime(), daysAndHours.get(i).getEndTime())) {
						flag = true;
					} else {
						flag = false;
					}
				} else {
					flag = false;
				}
			}
		}
		return flag;
	}
}
