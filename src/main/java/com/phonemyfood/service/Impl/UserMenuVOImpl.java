package com.phonemyfood.service.Impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.phonemyfood.db.DaysAndHours;
import com.phonemyfood.db.User;
import com.phonemyfood.db.UserMenu;
import com.phonemyfood.db.vo.UserMenuVO;
import com.phonemyfood.repository.DayAndHoursRepository;
import com.phonemyfood.security.LoginUserDetail;
import com.phonemyfood.service.UserMenuVOService;
import com.phonemyfood.service.UserMenusService;

@Service
public class UserMenuVOImpl implements UserMenuVOService {

	@Autowired
	private LoginUserDetail loginUserDetails;

	@Autowired
	private UserMenusService userMenusService;

	@Autowired
	private DayAndHoursRepository dayAndHoursRepository;

	@Override
	public UserMenuVO prepareUserMenuVO() {
		UserMenuVO userMenuVO = new UserMenuVO();
		List<DaysAndHours> daysAndHours = new ArrayList<DaysAndHours>();
		userMenuVO.setDaysAndHours(daysAndHours);
		String day[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun", "all" };
		for (int i = 0; i <= 7; i++) {
			DaysAndHours daysAndhours = new DaysAndHours();
			daysAndhours.setDayName(day[i]);
			daysAndhours.setActive(false);
			userMenuVO.addDaysAndHours(daysAndhours);
		}
		userMenuVO.setUserMenu(new UserMenu());
		return userMenuVO;
	}

	@Override
	public UserMenuVO LoadUserMenuVO(Long id, String menuName) {
		UserMenu userMenu = userMenusService.getOne(id);
		UserMenuVO userMenuVO = new UserMenuVO();
		List<DaysAndHours> daysAndHoursList = dayAndHoursRepository.findAllByTimeableIdAndTimeableType(id, "UserMenu");
		if (daysAndHoursList.isEmpty()) {
			String day[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun", "all" };
			for (int i = 0; i <= 7; i++) {
				DaysAndHours daysAndHours = new DaysAndHours();
				daysAndHours.setDayName(day[i]);
				daysAndHours.setActive(false);
				daysAndHoursList.add(daysAndHours);
			}
		}
		userMenuVO.setDaysAndHours(daysAndHoursList);
		userMenuVO.setUserMenu(userMenu);
		return userMenuVO;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public String saveAllUserMenu(UserMenuVO userMenuVO) {
		User actualLoginUser = loginUserDetails.getActualLoginUser();
		String status = "error";
		Long id = userMenuVO.getUserMenu().getId();
		List<DaysAndHours> daysAndHours = dayAndHoursRepository.findAllByTimeableIdAndTimeableType(id, "UserMenu");
		User user = loginUserDetails.getCurrentLoginUser();
		UserMenu userMenu = userMenuVO.getUserMenu();
		userMenu.setUser(user);
		userMenu.setCreatedAt(new Date());
		userMenu.setUpdateAt(new Date());
		userMenu.setCreatedById(actualLoginUser.getId());
		userMenu.setUpdateById(actualLoginUser.getId());
		try {
			final UserMenu userMenu2 = userMenusService.save(userMenu);
			String day[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun", "all" };
			List<DaysAndHours> daysAndHours2 = userMenuVO.getDaysAndHours();
			for (int i = 0; i < daysAndHours2.size(); i++) {
				daysAndHours2.get(i).setDayName(day[i]);
				daysAndHours2.get(i).setActive(true);
				daysAndHours2.get(i).setCreatedAt(new Date());
				daysAndHours2.get(i).setUpdateAt(new Date());
				daysAndHours2.get(i).setCreatedById(actualLoginUser.getId());
				daysAndHours2.get(i).setUpdateById(actualLoginUser.getId());
				daysAndHours2.get(i).setTimeableId(userMenu2.getId());
				daysAndHours2.get(i).setTimeableType("UserMenu");
				daysAndHours2.get(i).setId(daysAndHours.size() > 0 ? daysAndHours.get(i).getId() : 0);
				userMenuVO.setDaysAndHours(daysAndHours2);
			}
			dayAndHoursRepository.saveAll(userMenuVO.getDaysAndHours());
			status = "success";
		} catch (Exception e) {
			e.printStackTrace();
			status = "error";
		}
		return status;
	}
}
