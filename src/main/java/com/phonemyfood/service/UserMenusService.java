package com.phonemyfood.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.phonemyfood.db.User;
import com.phonemyfood.db.UserMenu;

public interface UserMenusService {

	Page<UserMenu> findLast3ByUser(User actualLoginUser, Pageable pageable);
	
	List<UserMenu> findAllByUser(User user);
	
	UserMenu getOne(long id);
	
	UserMenu save(UserMenu userMenu);
}