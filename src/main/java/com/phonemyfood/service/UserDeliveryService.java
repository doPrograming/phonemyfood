package com.phonemyfood.service;

import java.util.List;

import com.phonemyfood.db.UserDeliveries;

public interface UserDeliveryService {

	List<UserDeliveries> getAllUserDelivery();

	void delete(UserDeliveries deliveries);

	void saveAll(List<UserDeliveries> userDeliveries);
	
	UserDeliveries getOne(Long id);
	
}
