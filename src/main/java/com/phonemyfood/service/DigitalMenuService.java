package com.phonemyfood.service;

import java.util.List;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.DigitalMenu;
import com.phonemyfood.db.vo.DigitalMenuItemVo;
import com.phonemyfood.db.vo.DigitalMenuVo;

public interface DigitalMenuService{

	void save(DigitalMenu digitalMenu);

	List<DigitalMenu> findAllByBusinesses(Businesses findBusinessesByLoginUser);

	DigitalMenu getOne(long id);
	
	public void saveDigitalMenuImage(String imageString, String screennumber, long digitalmenuId, String digitalmenuItemId, String croppingFramePath);

	void saveLinkedMenus(DigitalMenu digitalMenu, List<DigitalMenu> digitalmenus);

	void linkDigitalMenus(List<Long> ids, long c_id);

	void setLinkedMenus(DigitalMenu digitalMenu);
	
	List<DigitalMenuVo> getDigitalMenuVos();
	
	List<DigitalMenuItemVo> getDigitalMenuItemVo(Long id);

	List<DigitalMenu> getValidDigitalMenusForScreen(String screenNumber, Businesses businesses);
	
	void deleteDigitalMenu(Long id);

	List<DigitalMenu> findAllByLinkedWithMenu(DigitalMenu digitalMenu);
	
	public List<DigitalMenu> findAllDigitalMenusByIds(List<Long> ids);
	
	public DigitalMenu findByBusinessesAndScreen(Businesses businesses,String screenNumber);
	
	public List<DigitalMenu> findAllByBusinessesAndNotLinkedWithMenu(Businesses businesses);
}