package com.phonemyfood.service;

import java.util.List;

import com.phonemyfood.db.Rewards;
import com.phonemyfood.db.vo.RewardsVO;

public interface RewardsService {

	List<Rewards> getAllRewards();

	void updateRewards(RewardsVO rewardsVO);
	
	Rewards getOne(Long id);

}
