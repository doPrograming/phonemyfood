package com.phonemyfood.service;

import com.phonemyfood.db.UserAbouts;

public interface UserAboutsService {

	void saveUserAbout(UserAbouts userAbouts);

	UserAbouts findByUser();

	UserAbouts findById(long id);
	
}
