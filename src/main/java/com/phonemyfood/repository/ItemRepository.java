package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.User;

public interface ItemRepository extends JpaRepository<Items, Long>{

	@Query("Select I from Items I where I.user =?1 Order by I.name")
	List<Items> findAllByUser(User user);

	List<Items> findAllByUserAndHideItem(User user, boolean b);
	
	@Query("Select I from Items I where I.user =:user And upper(I.name) like %:name% order by I.name")
	List<Items> findAllByUserAndName(@Param("user") User user,@Param("name") String upperCase);

	@Query("Select I from Items I where I.id =:id And upper(I.name) like %:key%")
	Items findByIdAndKey(@Param("id") long id,@Param("key") String key);

}
