package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phonemyfood.db.Role;
import com.phonemyfood.db.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String username);

	User findByEmailAndActive(String email, boolean b);

	User findByPasswordResetAndResetToken(boolean b, String parameter);
	
	List<User> findAllByRoles(List<Role> role);

	//List<User> findAllByRolesAndUSerName(List<Role> findAllByName, String userName);

	//List<User> findAllActive();

}