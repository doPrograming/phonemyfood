package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phonemyfood.db.Rewards;
import com.phonemyfood.db.User;

public interface RewardsRepository extends JpaRepository<Rewards, Long> {

	List<Rewards> findAllByUser(User user);
	
	Rewards getOne(Long id);

}
