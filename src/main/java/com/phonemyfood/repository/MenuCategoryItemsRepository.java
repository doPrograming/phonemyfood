package com.phonemyfood.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.MenuCategoryItem;
import com.phonemyfood.db.UserMenu;

public interface MenuCategoryItemsRepository extends JpaRepository<MenuCategoryItem, Long> {
	
	@Query("select mci from MenuCategoryItem mci where mci.userMenu.id=:menuId and mci.categories.id=:categoryId")
	MenuCategoryItem findAllByMenuIdAndCategoryId(@Param("menuId") long menuId,@Param("categoryId") long categoryId);

	@Query("select mci from MenuCategoryItem mci where mci.categories.id=:categoryId")
	List<MenuCategoryItem> findAllByCategoryId(@Param("categoryId") Long categoryId);
	
	@Query("select mci.items from MenuCategoryItem mci where mci.userMenu = ?1 order by mci.id")
	Set<Items> findItemByUserMenus(UserMenu userMenu);
	
	@Query(value = "SELECT items_id FROM public.mci_item where menu_category_item_id = ?1", nativeQuery = true)
	Set<BigInteger> findByMenuCategoryItem(long menuCategoryItemId);
}