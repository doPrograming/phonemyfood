package com.phonemyfood.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phonemyfood.db.ComboOptions;
import com.phonemyfood.db.PriceAndCombos;

public interface ComboOptionsRepository extends JpaRepository<ComboOptions, Long> {

	ComboOptions findByPriceCombos(PriceAndCombos priceAndCombos);

}
