package com.phonemyfood.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserAddresses;

public interface UserAddressesRepository extends JpaRepository<UserAddresses, Long> {

	UserAddresses findByUser(User user);
}