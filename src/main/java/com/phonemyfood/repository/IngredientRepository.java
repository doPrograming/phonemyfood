package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.phonemyfood.db.Ingredients;
import com.phonemyfood.db.User;

public interface IngredientRepository extends JpaRepository<Ingredients, Long>{

	List<Ingredients> findAllByUser(User user);

	@Query("Select I from Ingredients I where I.user = :user and upper(I.name) like %:key%")
	List<Ingredients> findIngredientByUserAndNameIgnoringCase(@Param("user") User user, @Param("key") String key);
}