package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.phonemyfood.db.Categories;
import com.phonemyfood.db.User;

public interface CategoriesRepository extends JpaRepository<Categories, Long> {

	@Query("Select c from Categories c where c.user = ?1 order by c.id")
	List<Categories> findAllByUser(User user);
	
	@Query("Select c from Categories c where upper(c.name) = upper(:name) AND c.user = :user")
	List<Categories> categoryExist(@Param("name") String name, @Param("user") User user);
}
