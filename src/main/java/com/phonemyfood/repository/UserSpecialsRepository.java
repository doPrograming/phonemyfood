package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserSpecials;

public interface UserSpecialsRepository extends JpaRepository<UserSpecials, Long>{

	List<UserSpecials> findAllByUser(User user);

}
