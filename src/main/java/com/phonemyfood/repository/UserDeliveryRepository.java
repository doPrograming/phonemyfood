package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserDeliveries;

public interface UserDeliveryRepository extends JpaRepository<UserDeliveries, Long> {

	List<UserDeliveries> findAllByUser(User user);

	UserDeliveries getOne(Long id);
}
