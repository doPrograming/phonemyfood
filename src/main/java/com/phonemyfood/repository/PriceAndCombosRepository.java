package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.phonemyfood.db.Items;
import com.phonemyfood.db.PriceAndCombos;

public interface PriceAndCombosRepository extends JpaRepository<PriceAndCombos, Long> {

	List<PriceAndCombos> findAllByItemOrderById(Items itemsModal);

	@Query("select pac from PriceAndCombos pac where pac.item=?1 AND pac.userMenuId=?2 order by pac.id")
	List<PriceAndCombos> findAllByItemAndUserMenuId(Items itemModel, Long userMenuId);
	
	@Transactional
	void removeByItem(Items item);
}
