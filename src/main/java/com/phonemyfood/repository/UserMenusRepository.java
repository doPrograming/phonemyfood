package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserMenu;

public interface UserMenusRepository extends JpaRepository<UserMenu, Long>, PagingAndSortingRepository<UserMenu, Long> {

	//List<UserMenu> findTop3ByUser(User actualLoginUser, Pageable pageable);

	/*List<UserMenu> findTop3ByUser(User actualLoginUser, Pageable pageable);*/
	
	
	//@Query(value ="SELECT * FROM user_menus u where u.user_id= :user ORDER BY u.id DESC Limit 3", nativeQuery=true)
	/*@Query("SELECT u From UserMenu u where u.user= :user ORDER By u.id DESC Limit 3")
	List<UserMenu> findLast3ByUser(@Param("user") User actualLoginUser);*/

	@Query("SELECT u From UserMenu u where u.user= :user")
	Page<UserMenu> findLast3ByUser(@Param("user") User actualLoginUser, Pageable pageable);

	List<UserMenu> findAllByUser(User user);

}
