package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.DigitalMenu;

public interface DigitalMenuRepository extends JpaRepository<DigitalMenu, Long>{
	
	@Query("select dm from DigitalMenu dm where dm.businesses=?1 order by dm.screen")
	List<DigitalMenu> findAllByBusinesses(Businesses businesses);

	@Query("select dm from DigitalMenu dm where dm.linkedWithMenu=?1 order by dm.screen")
	List<DigitalMenu> findAllByLinkedWithMenu(DigitalMenu digitalMenu);

	@Query("select dm from DigitalMenu dm where dm.id IN ?1 order by dm.screen")
	List<DigitalMenu> findAllDigitalMenusByIds(List<Long> ids);

	@Query("select dm from DigitalMenu dm where dm.businesses=?1 And dm.screen=?2")
	DigitalMenu findByBusinessesAndScreen(Businesses businesses, String screenNumber);

	@Query("select dm from DigitalMenu dm where dm.businesses=?1 AND dm.linkedWithMenu=null")
	List<DigitalMenu> findAllByBusinessesAndNotLinkedWithMenu(Businesses businesses);
}