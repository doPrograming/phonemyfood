package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.phonemyfood.db.Businesses;
import com.phonemyfood.db.User;

public interface BusinessesRepository extends JpaRepository<Businesses, Long> {

	// @Query("Select B from Businesses B where B.name like %:name%")
	// List<Businesses> findAllByName(@Param("name") String name);

	Businesses findByUser(User user);

	@Query("Select B from Businesses B where upper(B.name) like %:name%")
	List<Businesses> findAllByNameIgnoringCase(@Param("name") String key);

}
