package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.phonemyfood.db.Images;
import com.phonemyfood.db.User;

public interface ImagesRepository extends JpaRepository<Images, Long> {

	/*List<Images> findAllByImageableTypeAndImageableId(String string, Long id);*/

	List<Images> findAllByImageableTypeAndImageableIdAndUser(String string, Long id, User user);
	
	@Query("select i from Images i where i.imageableType = ?1 and i.user = ?2")
	Images findByImageableTypeAndUser(String string, User user);

	@Query("select i from Images i where i.imageableType = ?1 and i.user = ?2")
	List<Images> findByTypeAndUser(String string, User user);
	
	@Query("select i from Images i where i.imageableType=?1 and i.imageableId=?2 and i.user=?3 order by i.id desc")
	List<Images> getImagesByImageableTypeAndImageableId(String string, Long id, User user);
	
	@Query("select i from Images i where i.picture LIKE %?1% AND i.imageableId = ?2")
	Images getImageByPicturAndImageableId(String picture,long digitalmenuId);
}
