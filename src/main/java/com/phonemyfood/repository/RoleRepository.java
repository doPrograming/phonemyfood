package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phonemyfood.db.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

	List<Role> findAllByName(String string);

}
