package com.phonemyfood.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phonemyfood.db.User;
import com.phonemyfood.db.UserAbouts;

public interface UserAboutsRepository extends JpaRepository<UserAbouts, Long>{

	UserAbouts findByUser(User user);

}
