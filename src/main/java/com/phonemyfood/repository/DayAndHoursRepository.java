package com.phonemyfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.phonemyfood.db.DaysAndHours;

public interface DayAndHoursRepository extends JpaRepository<DaysAndHours, Long> {

	/*List<DaysAndHours> findAllByTimeableId(long id);*/
	
	@Query("Select da from DaysAndHours da where da.timeableId = ?1 and da.timeableType = ?2 ORDER BY da.id")
	List<DaysAndHours> findAllByTimeableIdAndTimeableType(long id, String string);

	@Transactional
	void removeByTimeableIdAndTimeableType(long id, String string);
}
